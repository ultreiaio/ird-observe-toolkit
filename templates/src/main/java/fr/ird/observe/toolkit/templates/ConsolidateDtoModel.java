package fr.ird.observe.toolkit.templates;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerChildWithDataFileDto;
import fr.ird.observe.dto.data.ContainerChildWithOptionalProportionDto;
import fr.ird.observe.dto.data.ContainerChildWithProportionDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.AbstractMetaTransformer;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.java.BeanTransformerTagValues;
import org.nuiton.eugene.java.EugeneJavaTagValues;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static fr.ird.observe.toolkit.templates.TemplateContract.isReferentialFromPackageName;

/**
 * To check and consolidate dto model.
 * <p>
 * This will generate missing tagValues for you.
 * <p>
 * Created on 06/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
@Component(role = Template.class, hint = "fr.ird.observe.toolkit.templates.ConsolidateDtoModel")
public class ConsolidateDtoModel extends AbstractMetaTransformer<ObjectModel> {

    private Set<String> i18nLabels;

    public static List<String> loadContainerChildProperties(ToolkitTagValues observeTagValues,
                                                            ObjectModelClassifier input,
                                                            boolean containerChildDto,
                                                            boolean containerChildWithDataFileDto,
                                                            boolean containerChildWithProportionDto,
                                                            boolean containerChildWithOptionalProportionDto,
                                                            Set<String> i18nLabels) {
        Set<String> expressions = new LinkedHashSet<>();
        if (containerChildDto || containerChildWithDataFileDto || containerChildWithProportionDto || containerChildWithOptionalProportionDto) {
            String tagValue = observeTagValues.getContainerChildDataDtoPropertiesTagValue(input);
            if (tagValue == null) {
                if (containerChildWithDataFileDto) {
                    expressions.add("dataLabel");
                    if (i18nLabels != null) {
                        i18nLabels.add("dataLabel");
                    }
                }
                if (containerChildWithProportionDto || containerChildWithOptionalProportionDto) {
                    expressions.add("proportion");
                    if (i18nLabels != null) {
                        i18nLabels.add("proportion");
                    }
                }
            } else {
                if (i18nLabels == null) {
                    expressions.addAll(List.of(tagValue.split("\\s*,\\s*")));
                }
            }
        }
        return new LinkedList<>(expressions);
    }

    @Override
    protected boolean validateModel(ObjectModel model) {
        ToolkitTagValues observeTagValues = new ToolkitTagValues();
        BeanTransformerContext all = new BeanTransformerContext(model, new EugeneCoreTagValues(), new EugeneJavaTagValues(), new BeanTransformerTagValues(), false, false, t -> true, getLog());
        String defaultPackageName = getConfiguration().getProperty(PROP_DEFAULT_PACKAGE);
        List<String> missingI18nLabelsProperties = new LinkedList<>();
//        List<String> missingInlineDataDtoProperties = new LinkedList<>();
        List<String> missingContainerChildDataDtoProperties = new LinkedList<>();

        for (ObjectModelClass aClass : all.selectedClasses) {
//            boolean inlineDto = aClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(InlineDataDto.class.getName()));
            boolean containerDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(ContainerDto.class.getName()));
            if (containerDto) {
                continue;
            }
            boolean openableDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(OpenableDto.class.getName()));
            boolean editableDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(EditableDto.class.getName()));
            boolean containerChildDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(ContainerChildDto.class.getName()));
            boolean containerChildWithDataFileDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(ContainerChildWithDataFileDto.class.getName()));
            boolean containerChildWithProportionDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(ContainerChildWithProportionDto.class.getName()));
            boolean containerChildWithOptionalProportionDto = aClass.getSuperclasses().stream().anyMatch(i -> (i.getQualifiedName() + "Dto").equals(ContainerChildWithOptionalProportionDto.class.getName()));
//            boolean notToEntity = aClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(NotEntityDto.class.getName()));
            List<String> oldI18nLabels = getOldI18nLabels(observeTagValues.getI18nLabelsTagValue(aClass));
            i18nLabels = getI18nLabels(aClass);

            boolean referential = isReferentialFromPackageName(aClass.getPackageName());
//            if (inlineDto) {
//                String tagValue = observeTagValues.getInlineDataDtoPropertiesTagValue(aClass);
//                if (tagValue == null) {
//                    List<String> expressions = new LinkedList<>();
//                    if (notToEntity) {
//                        // special case for notToEntity (we had the id, this is very weak...)
//                        expressions.add("id");
//                    }
//                    addDefaultAttributes(defaultPackageName, missingInlineDataDtoProperties, aClass, expressions);
//                }
//            }
            List<String> expressions = loadContainerChildProperties(observeTagValues, aClass, containerChildDto, containerChildWithDataFileDto, containerChildWithProportionDto,containerChildWithOptionalProportionDto, i18nLabels);
            if (!expressions.isEmpty()) {
                addDefaultAttributes(defaultPackageName, missingContainerChildDataDtoProperties, aClass, expressions);
            }
//            if (!(referential || openableDto || editableDto || containerChildDto || inlineDto || containerChildWithDataFileDto || containerChildWithProportionDto || containerChildWithOptionalProportionDto)) {
            if (!(referential || openableDto || editableDto || containerChildDto || containerChildWithDataFileDto || containerChildWithProportionDto || containerChildWithOptionalProportionDto)) {
                continue;
            }
            consolidateI18nLabels(defaultPackageName, aClass, oldI18nLabels, missingI18nLabelsProperties);
        }
        boolean withError = false;
//        if (!missingInlineDataDtoProperties.isEmpty()) {
//            withError = true;
//            Collections.sort(missingInlineDataDtoProperties);
//            getLog().error(String.format("\n\n\nFound missing inlineDataDtoProperties tagValues, please add this to add dto/class/inlineDataDtoProperties.properties\n%s\n", String.join("\n", missingInlineDataDtoProperties)));
//        }
        if (!missingContainerChildDataDtoProperties.isEmpty()) {
            withError = true;
            Collections.sort(missingContainerChildDataDtoProperties);
            getLog().error(String.format("\n\n\nFound missing containerChildDataDtoProperties tagValues, please add this to add dto/class/containerChildDataDtoProperties.properties\n%s\n", String.join("\n", missingContainerChildDataDtoProperties)));
        }
        if (!missingI18nLabelsProperties.isEmpty()) {
            withError = true;
            Collections.sort(missingI18nLabelsProperties);
            getLog().error(String.format("\n\n\nFound missing i18nLabels tagValues, please add this to add dto/class/i18nLabels.properties\n%s\n", String.join("\n", missingI18nLabelsProperties)));
        }
        if (withError) {
            throw new IllegalStateException("Found some error(s), please read logs.");
        }
        return true;
    }

    private List<String> getOldI18nLabels(String tagValue) {
        List<String> i18nLabels = new LinkedList<>();
        if (tagValue != null) {
            i18nLabels.addAll(List.of(tagValue.split("\\s*,\\s*")));
        }
        return i18nLabels;
    }

    private Set<String> getI18nLabels(ObjectModelClass aClass) {
        Set<String> i18nLabels = new TreeSet<>();
        Collection<ObjectModelAttribute> attributes = new LinkedList<>(aClass.getAttributes());
//        attributes.addAll(aClass.getAllOtherAttributes());
        for (ObjectModelAttribute attribute : attributes) {
            if (!attribute.isNavigable()) {
                continue;
            }
            i18nLabels.add(attribute.getName());
        }
        return i18nLabels;
    }

    private void consolidateI18nLabels(String defaultPackageName, ObjectModelClass aClass, List<String> oldI18nLabels, List<String> missingI18nLabelsProperties) {
        if (oldI18nLabels.isEmpty()) {
            List<String> missingI18nLabels = new LinkedList<>(i18nLabels);
            missingI18nLabels.removeAll(oldI18nLabels);
            if (!missingI18nLabels.isEmpty()) {
                Collections.sort(missingI18nLabels);
                String newTagValue = String.join(",", missingI18nLabels);
                missingI18nLabelsProperties.add(String.format("%s=%s", aClass.getQualifiedName().replace(defaultPackageName + ".", ""), newTagValue));
            }
        }
    }

    private void addDefaultAttributes(String defaultPackageName, List<String> missingTagValues, ObjectModelClass aClass, List<String> expressions) {
        for (ObjectModelAttribute attribute : aClass.getAttributes()) {
            if (!attribute.isNavigable()) {
                continue;
            }
            String name = attribute.getName();
            if (!expressions.contains(name)) {
                expressions.add(name);
            }
        }
        String tagValue = String.join(",", expressions);
        missingTagValues.add(String.format("%s=%s", aClass.getQualifiedName().replace(defaultPackageName + ".", ""), tagValue));
    }
}
