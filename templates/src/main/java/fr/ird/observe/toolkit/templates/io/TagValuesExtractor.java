package fr.ird.observe.toolkit.templates.io;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithNoCodeAware;
import io.ultreia.java4all.util.SortedProperties;
import org.nuiton.eugene.LogProxy;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;

import static fr.ird.observe.toolkit.templates.TemplateContract.isReferentialFromPackageName;

/**
 * To extract tag values from model, and then store them as short tag values.
 * <p>
 * Created on 28/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.5
 */
public class TagValuesExtractor {

    public static final String packagePrefix = "package-tagValue-";
    public static final String classPrefix = "class-tagValue-";
    public static final String attributePrefix = "attribute-tagValue-";
    private final ObjectModel model;
    private final String classifier;
    private final LogProxy log;
    private final String packageName;
    private final SortedProperties properties;
    private final ArrayListMultimap<String, String> tagValueKeysByTagValue;
    private final Map<String, String> allTagValues;
    private final TagValuesList list;

    public static Path getModelPath(Path resourcePath, ObjectModel model, String classifier) {
        return getModelPath(resourcePath, model.getName(), classifier);
    }

    public static Path getModelPath(Path resourcePath, String modelName, String classifier) {
        return resourcePath.getParent().resolve("models").resolve(modelName).resolve(classifier);
    }

    public TagValuesExtractor(ObjectModel model, String packageName, String classifier, LogProxy log) {
        this.model = model;
        this.packageName = packageName;
        this.classifier = classifier;
        this.log = log;
        this.list = new TagValuesList(classifier, log);

        this.properties = new SortedProperties();
        this.tagValueKeysByTagValue = ArrayListMultimap.create();
        this.allTagValues = new TreeMap<>();
    }

    public Set<String> getModelTagValueKeys() {
        return tagValueKeysByTagValue.keySet();
    }

    public void load(ClassLoader classLoader, Path resourceDirectory) throws IOException {

        list.load(resourceDirectory);
        properties.clear();
        tagValueKeysByTagValue.clear();
        allTagValues.clear();
        load0(classLoader, classifier, properties);
        log.info(String.format("[%s] Load %d tag-value(s).", classifier, properties.size()));

        Collection<ObjectModelPackage> packages = model.getPackages();
        log.info(String.format("[%s] Extract tag values from %d package(s).", classifier, packages.size()));
        packages.forEach(this::extractPackageTagValues);

        List<ObjectModelClass> classes = new ArrayList<>(model.getClasses());
        log.info(String.format("[%s] Extract tag values from %d class(es).", classifier, classes.size()));
        classes.forEach(this::extractClassTagValues);
    }

    public Map<String, String> loadFromModel(String tagValuesKey) {
        String[] tagParts = tagValuesKey.split("-");
        String tagType = tagParts[0];
        String middle = tagParts[1];

        int beginIndex = packageName.length() + 1;
        Map<String, String> currentTagValues = new TreeMap<>();
        for (String k : tagValueKeysByTagValue.get(tagValuesKey)) {
            String value = allTagValues.get(k);
            String key;
            switch (tagType) {
                case "package":
                    // package.fr.ird.observe.dto.data.ll.common.tagValue.xxx  → data.ll.common
                    // package.fr.ird.observe.dto.tagValue.xxx  → @@
                    key = k.substring(tagType.length() + beginIndex, k.indexOf(middle) - 1);
                    // package.fr.ird.observe.dto.data.ll.common.tagValue.xxx  → .data.ll.common
                    // package.fr.ird.observe.dto.tagValue.xxx  → .
                    if (key.equals("")) {
                        // empty  → @@
                        key = "@@";
                    } else {
                        // .data.ll.common → data.ll.common
                        key = key.substring(1);
                    }
                    break;
                case "class":
                    // fr.ird.observe.dto.referential.Referential.class.tagValue.skip=true → referential.Referential
                    key = k.substring(beginIndex, k.indexOf(tagType) - 1);
                    break;
                case "attribute":
                    // fr.ird.observe.dto.referential.Referential.attribute.test.tagValue.skip=true → referential.Referential.attribute.test
                    key = k.substring(beginIndex, k.indexOf(middle) - 1);
                    break;
                default:
                    throw new IllegalStateException("Can't manage value of type: " + tagType);
            }

            currentTagValues.put(key, value);
            properties.setProperty(k, value);
        }
        String index = tagValuesKey.replace("-" + middle, "").replaceAll("-", ".");
        list.add(index);

        return currentTagValues;
    }

    public void store(Path resourceDirectory) throws IOException {
        String resourcesPath = TagValues.getResourceFilePath(model, Objects.requireNonNull(classifier));
        Path indexPath = resourceDirectory.resolve(resourcesPath);
        log.info(String.format("[%s] Store %d tag-value(s) at %s", classifier, properties.size(), indexPath));
        List<String> lines = new LinkedList<>();
        for (String key : properties.stringPropertyNames()) {
            lines.add(key + "=" + properties.getProperty(key));
        }
        TagValuesList.store(indexPath, lines);
    }

    private void load0(ClassLoader classLoader, String classifier, SortedProperties result) throws IOException {
        String resourcesPath = TagValues.getResourcePath(model, Objects.requireNonNull(classifier));

        List<String> tagValueList = list.getList();
        for (String tagValue : tagValueList) {
            int i = tagValue.indexOf(".");
            String type = tagValue.substring(0, i);
            String suffix = tagValue.substring(i + 1);
            String tagValueName = suffix.replace("tagValue.", "");
            String tagValuePath = resourcesPath + type + "/" + tagValueName + ".properties";
            String tagValueStoreName = type + "-tagValue-" + tagValueName;

            SortedProperties current = new SortedProperties();
            Function<String, String> keyExtractor;
            switch (type) {
                case "package":
                    keyExtractor = key -> {
                        if (key.equals("@@")) {
                            return "package." + packageName + "." + suffix;
                        }
                        return "package." + packageName + "." + key + "." + suffix;
                    };
                    break;
                case "attribute":
                    keyExtractor = key -> packageName + "." + key + "." + suffix;
                    break;
                case "class":
                    keyExtractor = key -> packageName + "." + key + ".class." + suffix;
                    break;
                default:
                    throw new IllegalStateException(String.format("Can't manage tag value type: %s", type));
            }
            URL tagValueURL = classLoader.getResource(tagValuePath);
            TagValues.loadTagValuesFile(current, Objects.requireNonNull(tagValueURL), keyExtractor);
            for (String propertyName : current.stringPropertyNames()) {
                String property = current.getProperty(propertyName);
                result.setProperty(propertyName, property);
                tagValueKeysByTagValue.put(tagValueStoreName, propertyName);
                allTagValues.put(propertyName, property);
            }

        }
    }

    private void extractPackageTagValues(ObjectModelPackage aPackage) {
        String prefix = "package." + aPackage.getName() + ".tagValue.";
        aPackage.getTagValues().forEach((k, v) -> {
            String key = prefix + k;
            allTagValues.put(key, v);
            tagValueKeysByTagValue.put(packagePrefix + k, key);
        });
    }

    protected void extractClassTagValues(ObjectModelClass aClass) {
        String prefix = aClass.getQualifiedName() + ".class.tagValue.";
        aClass.getTagValues().forEach((k, v) -> {
            String key = prefix + k;
            allTagValues.put(key, v);
            tagValueKeysByTagValue.put(classPrefix + k, key);
        });
        for (ObjectModelAttribute attribute : aClass.getAttributes()) {
            extractAttributeTagValues(aClass, attribute);
        }
        boolean referential = isReferentialFromPackageName(aClass.getPackageName());
        if (referential && !aClass.isAbstract()) {
            boolean withCodeDto = aClass.getInterfaces().stream().noneMatch(i -> i.getQualifiedName().equals(ReferentialDtoReferenceWithNoCodeAware.class.getName()));
            addReferentialWithCode(aClass, withCodeDto);
        }
    }

    protected void addReferentialWithCode(ObjectModelClass aClass, boolean withCodeDto) {
        if (withCodeDto) {
            addAttributeTagValue(aClass, "code", "notNull", "true");
        } else {
            log.info("Skip WithNoCode referential: " + aClass.getQualifiedName());
        }
    }

    protected void extractAttributeTagValues(ObjectModelClass aClass, ObjectModelAttribute attribute) {
        String prefix = aClass.getQualifiedName() + ".attribute." + attribute.getName() + ".tagValue.";
        attribute.getTagValues().forEach((k, v) -> {
            String key = prefix + k;
            allTagValues.put(key, v);
            tagValueKeysByTagValue.put(attributePrefix + k, key);
        });
    }

    protected boolean containsAttributeTagValue(ObjectModelClass aClass, String attributeName, String tagValueName) {
        String prefix = aClass.getQualifiedName() + ".attribute." + attributeName + ".tagValue.";
        String key = prefix + tagValueName;
        return allTagValues.containsKey(key);
    }

    protected void addAttributeTagValue(ObjectModelClass aClass, String attributeName, String tagValueName, String value) {
        String prefix = aClass.getQualifiedName() + ".attribute." + attributeName + ".tagValue.";
        String key = prefix + tagValueName;
        if (!allTagValues.containsKey(key)) {
            allTagValues.put(key, value);
            tagValueKeysByTagValue.put(attributePrefix + tagValueName, key);
        }
    }

    protected boolean containsClassTagValue(ObjectModelClass aClass, String tagValueName) {
        String prefix = aClass.getQualifiedName() + ".class.tagValue.";
        String key = prefix + tagValueName;
        return allTagValues.containsKey(key);
    }

    protected String getClassTagValue(ObjectModelClass aClass, String tagValueName) {
        String prefix = aClass.getQualifiedName() + ".class.tagValue.";
        String key = prefix + tagValueName;
        return allTagValues.get(key);
    }

    protected void addClassTagValue(ObjectModelClass aClass, String tagValueName, String value,boolean force) {
        String prefix = aClass.getQualifiedName() + ".class.tagValue.";
        String key = prefix + tagValueName;
        if (force || !allTagValues.containsKey(key)) {
            allTagValues.put(key, value);
            tagValueKeysByTagValue.put(classPrefix + tagValueName, key);
        }
    }

    public SortedProperties createOrLoadFromFile(Path outputFile, String filePrefix) throws IOException {
        SortedProperties existingProperties = new SortedProperties();
        if (Files.exists(outputFile)) {
            try (BufferedReader reader = Files.newBufferedReader(outputFile)) {
                existingProperties.load(reader);
            }
            log.debug(String.format("[%s] %s - Load existing %d element(s) from %s.", classifier, filePrefix, existingProperties.size(), outputFile));
        }
        return existingProperties;
    }

    public void mergeAndStore(SortedProperties fromFile, Map<String, String> fromModel, Path outputFile, Writer output, String filePrefix) throws IOException {

        // add new one from model
        int size = fromFile.size();
        fromModel.forEach(fromFile::setProperty);

        log.debug(String.format("[%s] %s - Load from model new %d element(s).", classifier, filePrefix, fromFile.stringPropertyNames().size() - size));

        log.debug(String.format("[%s] %s - Store %d element(s) to %s.", classifier, filePrefix, fromFile.size(), outputFile));

        for (String key : fromFile.stringPropertyNames()) {
            String value = fromFile.getProperty(key);
            output.write(String.format("%s=%s", key, value));
            output.write('\n');
        }
    }
}
