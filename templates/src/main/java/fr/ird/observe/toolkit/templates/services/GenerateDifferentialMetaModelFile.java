package fr.ird.observe.toolkit.templates.services;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.WithAtLeastOneSelected;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.DifferentialMetaModel;
import fr.ird.observe.toolkit.templates.navigation.GenerateNavigationMetaModel;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.ObjectModelType;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.topia.templates.sql.TopiaMetadataModelGeneratorSupport;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 24/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class GenerateDifferentialMetaModelFile extends ObjectModelGenerator {
    private static final Set<String> SKIP_PROPERTIES = new LinkedHashSet<>(Arrays.asList(
            ReferentialDto.PROPERTY_LAST_UPDATE_DATE,
            ReferentialDto.PROPERTY_ID,
            ReferentialDto.PROPERTY_CREATE_DATE,
            ReferentialDto.PROPERTY_VERSION,
            ReferentialDto.PROPERTY_STATUS,
            ToolkitId.PROPERTY_TOOLKIT_ID,
            ToolkitId.PROPERTY_TOOLKIT_VERSION,
            ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE,
            WithAtLeastOneSelected.PROPERTY_AT_LEAST_ONE_SELECTED));

    @Override
    public void applyTemplate(ObjectModel model, File destDir) {
        this.model = model;

        File realTarget = TopiaMetadataModelGeneratorSupport.getNotGeneratedResourceDirector(destDir);
        String filename = getFilenameForModel(model);
        generateFromElement(model, realTarget, filename, ObjectModelType.OBJECT_MODEL);
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return DifferentialMetaModel.toLocation(getFilenameForModel0(model));
    }

    @Override
    protected void generateFromElement(Object element, File destDir, String filename, ObjectModelType type) {
        if (ObjectModelType.OBJECT_MODEL != type) {
            // only generate on model
            return;
        }
        super.generateFromElement(element, destDir, filename, type);
    }

    @Override
    public void generateFromModel(Writer output, ObjectModel input) {
        List<String> replicationOrderWithStandalone = loadReplicationOrderWithStandalone(getClassLoader(), model.getName());

        LinkedHashMap<String, List<String>> referentialBusinessProperties = new LinkedHashMap<>();
        for (String entityName : replicationOrderWithStandalone) {
            if (!entityName.contains(".referential.")) {
                continue;
            }
            String dtoName = entityName.replace(".entities.", ".dto.") + "Dto";
            ObjectModelClass aClass = model.getClass(dtoName.replace("Dto", ""));
            List<String> propertyNames = getBusinessProperties0(aClass);
            getLog().debug(String.format("%s - Detected %s business properties: %s.", dtoName, propertyNames.size(), propertyNames));
            referentialBusinessProperties.put(dtoName, propertyNames);
        }
        DifferentialMetaModel.newGson().toJson(referentialBusinessProperties, output);
    }

    public static List<String> loadReplicationOrderWithStandalone(ClassLoader loader, String modelName) {
        List<String> replicationOrderWithStandalone;
        URL resource = loader.getResource(GenerateNavigationMetaModel.getStandaloneReplicationOrderLocation(modelName));
        try (Reader reader = new InputStreamReader(Objects.requireNonNull(resource).openStream())) {
            replicationOrderWithStandalone = new GsonBuilder().create().fromJson(reader, new TypeToken<List<String>>() {
            }.getType());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return replicationOrderWithStandalone;
    }

    protected List<String> getBusinessProperties0(ObjectModelClass dtoType) {
        List<ObjectModelAttribute> attributes = new LinkedList<>();
        attributes.addAll(dtoType.getAttributes());
        attributes.addAll(dtoType.getAllOtherAttributes());
        ObjectModelAttributeImpl enabledAttribute = new ObjectModelAttributeImpl();
        enabledAttribute.setName(ReferentialDto.PROPERTY_ENABLED);
        enabledAttribute.setType(boolean.class.getName());
        attributes.add(enabledAttribute);
        return attributes.stream()
                .filter(p -> !SKIP_PROPERTIES.contains(p.getName()) && !(p.getType().equals(boolean.class.getName()) && p.getName().endsWith("Valid")))
                .map(ObjectModelElement::getName)
                .sorted()
                .collect(Collectors.toList());
    }

    protected String getFilenameForModel0(ObjectModel model) {
        return super.getFilenameForModel(model);
    }

    @Override
    protected File getDestinationFile(File destDir, String filename) {
        return destDir.toPath().resolve(filename).toFile();
    }
}
