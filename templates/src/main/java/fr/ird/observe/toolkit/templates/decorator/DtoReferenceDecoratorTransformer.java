package fr.ird.observe.toolkit.templates.decorator;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import fr.ird.observe.decoration.DtoReferenceDecoratorRenderer;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.dto.DtoReferenceTransformer;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticDefinitionModel;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticsHelper;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 21/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public class DtoReferenceDecoratorTransformer extends DecoratorTransformerSupport {
    private Map<ObjectModelClass, List<StatisticDefinitionModel>> statistics;
    private BeanTransformerContext context;

    @Override
    public void transformFromModel(ObjectModel model) {
        context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, false, false, input -> {
            ObjectModelPackage aPackage = model.getPackage(input.getPackageName());
            boolean referential = TemplateContract.isReferentialFromPackageName(aPackage.getName());
            String referencesTagValue = toolkitTagValues.getReferencesTagValue(input);
            if (referencesTagValue == null && referential) {
                referencesTagValue = "code,label,uri";
            }
            if (referencesTagValue == null) {
                return false;
            }
            String decoratorTagValue = getDecoratorTagValue(aPackage, input);
            if (decoratorTagValue != null) {
                return true;
            }
            String decoratorWithClassifierTagValue = getDecoratorWithClassifierTagValue(aPackage, input);
            return decoratorWithClassifierTagValue != null;
        }, getLog());

        context.report();
        statistics = StatisticsHelper.toModel(model, ProjectPackagesDefinition.of(getClassLoader()));
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Class<? extends DtoReferenceDecoratorRenderer> rendererType(ObjectModelClassifier input) {
        return DtoReferenceDecoratorRenderer.class;
    }

    @Override
    protected boolean acceptClass(ObjectModelClass input) {
        return input != null && context.selectedClasses.contains(input);
    }

    @Override
    protected String getClassName(ObjectModelClassifier input) {
        return input.getName() + "Reference";
    }

    @Override
    protected List<StatisticDefinitionModel> getStatistics(ObjectModelClassifier input) {
        return input instanceof ObjectModelClass ? statistics.get(input) : null;
    }

    @Override
    protected String getDecoratorTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return toolkitTagValues.getDecoratorTagValue(aPackage, input);
    }

    @Override
    protected String getDecoratorWithClassifierTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return toolkitTagValues.getDecoratorWithClassifierTagValue(aPackage, input);
    }

    @Override
    protected String translatePackageName(String packageName) {
        return packageName.replace(".decoration", ".dto.decoration");
    }

    @Override
    protected void generateDecoratorDefinition(ObjectModelPackage aPackage, ObjectModelClassifier input, String className, String decoratorTagValue, String decoratorWithClassifierTagValue, List<StatisticDefinitionModel> statisticDefinitions) {
        if (!(input instanceof ObjectModelClass)) {
            return;
        }
        boolean referential = TemplateContract.isReferentialFromPackageName(aPackage.getName());
        String referencesTagValue = toolkitTagValues.getReferencesTagValue(input);
        if (referencesTagValue == null && referential) {
            referencesTagValue = "code,label,uri";
        }
        Set<String> availableProperties = new LinkedHashSet<>(Arrays.asList(Objects.requireNonNull(referencesTagValue).split(",")));
        Map<ObjectModelAttribute, ObjectModelAttribute> binderProperties = DtoReferenceTransformer.getReferenceProperties(context.selectedClassesFqn, (ObjectModelClass) input, availableProperties, this::getAttributeType);
        if (decoratorTagValue != null) {
            decoratorTagValue = encodeDecoratorTagValue(binderProperties, decoratorTagValue);
        }
        if (decoratorWithClassifierTagValue != null) {
            decoratorWithClassifierTagValue = encodeDecoratorTagValue(binderProperties, decoratorWithClassifierTagValue);
        }
        super.generateDecoratorDefinition(aPackage, input, className, decoratorTagValue, decoratorWithClassifierTagValue, statisticDefinitions);
    }

    protected String encodeDecoratorTagValue(Map<ObjectModelAttribute, ObjectModelAttribute> binderProperties, String decoratorTagValue) {
        for (Map.Entry<ObjectModelAttribute, ObjectModelAttribute> entry : binderProperties.entrySet()) {
            ObjectModelAttribute property = entry.getKey();
            ObjectModelAttribute dtoProperty = entry.getValue();
            if (dtoProperty == null) {
                continue;
            }
            String propertyName = property.getName();
            if (propertyName.endsWith("Label")) {
                String dtoPropertyName = dtoProperty.getName();
                if (!dtoPropertyName.equals(propertyName)) {
                    // this means a XXXLabel reference for a XXX::label
                    decoratorTagValue = decoratorTagValue.replaceAll(dtoPropertyName + "::labelOrUnknown", propertyName);
                    decoratorTagValue = decoratorTagValue.replaceAll(dtoPropertyName + "::label", propertyName);
                    decoratorTagValue = decoratorTagValue.replaceAll(dtoPropertyName + "::scientificLabel", propertyName);
                }
            } else if (propertyName.endsWith("Code")) {
                String dtoPropertyName = dtoProperty.getName();
                if (!dtoPropertyName.equals(propertyName)) {
                    // this means a XXXCode reference for a XXX::code
                    decoratorTagValue = decoratorTagValue.replaceAll(dtoPropertyName + "::code", propertyName);
                }
            }
        }
        return decoratorTagValue;
    }

    private String getAttributeType(String attrType) {
        return DtoReferenceTransformer.getAttributeType(model, context, attrType);
    }

}
