package fr.ird.observe.toolkit.templates.decorator;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticDefinitionModel;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticsHelper;
import io.ultreia.java4all.decoration.DecoratorRenderer;
import io.ultreia.java4all.decoration.spi.DecoratorDefinitionJavaFileBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.TemplateConfiguration;
import org.nuiton.eugene.java.BeanTransformerTagValues;
import org.nuiton.eugene.java.EugeneJavaTagValues;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.templates.sql.TopiaMetadataModelGeneratorSupport;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

/**
 * Created on 21/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public abstract class DecoratorTransformerSupport extends ObjectModelTransformerToJava implements TemplateContract {
    private static final Logger log = LogManager.getLogger(DecoratorTransformerSupport.class);

    protected final EugeneCoreTagValues coreTagValues;
    protected final EugeneJavaTagValues javaTemplatesTagValues;
    protected final BeanTransformerTagValues beanTagValues;
    protected final ToolkitTagValues toolkitTagValues;

    private Path targetDirectory;


    public DecoratorTransformerSupport() {
        coreTagValues = new EugeneCoreTagValues();
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
        toolkitTagValues = new ToolkitTagValues();
    }

    @SuppressWarnings("rawtypes")
    protected abstract Class<? extends DecoratorRenderer> rendererType(ObjectModelClassifier input);

    protected abstract boolean acceptClass(ObjectModelClass input);

    protected abstract String getClassName(ObjectModelClassifier input);

    protected abstract List<StatisticDefinitionModel> getStatistics(ObjectModelClassifier input);

    protected abstract String getDecoratorTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input);

    protected abstract String getDecoratorWithClassifierTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input);

    @Override
    public final void transformFromClass(ObjectModelClass input) {
        if (acceptClass(input)) {
            generateClassifier(input);
        }
    }

    protected void generateClassifier(ObjectModelClassifier input) {
        if (log.isDebugEnabled()) {
            log.debug("Will use classLoader " + getClassLoader());
        }
        ObjectModelPackage aPackage = getPackage(input);

        String className = Objects.requireNonNull(getClassName(input));
        log.info("for entity : " + input.getQualifiedName());

        String decoratorTagValue = getDecoratorTagValue(aPackage, input);
        String decoratorWithClassifierTagValue = getDecoratorWithClassifierTagValue(aPackage, input);

        List<StatisticDefinitionModel> statisticDefinitions = getStatistics(input);

        generateDecoratorDefinition(aPackage, input, className, decoratorTagValue, decoratorWithClassifierTagValue, statisticDefinitions);
    }

    protected void generateDecoratorDefinition(ObjectModelPackage aPackage,
                                               ObjectModelClassifier input,
                                               String className,
                                               String decoratorTagValue,
                                               String decoratorWithClassifierTagValue,
                                               List<StatisticDefinitionModel> statisticDefinitions) {
        Path targetDirectory = getTargetDirectory();
        String modelName = model.getName();
        @SuppressWarnings("rawtypes") Class<? extends DecoratorRenderer> decoratorHelper = rendererType(input);

        String packageName = aPackage.getName();

        if (decoratorTagValue != null) {
            String decoratorHelperImpl = getDecoratorHelperImpl(modelName, decoratorHelper);
            DecoratorDefinitionJavaFileBuilder.create(packageName,
                                                      className,
                                                      decoratorTagValue,
                                                      decoratorHelperImpl).generateJavaFile(targetDirectory, false);
            if (statisticDefinitions != null) {
                String expression = StatisticsHelper.generateDecoratorWithStatsExpression(decoratorTagValue, statisticDefinitions);
                DecoratorDefinitionJavaFileBuilder.create(packageName,
                                                          className,
                                                          DecoratorService.WITH_STATS_CLASSIFIER,
                                                          false,
                                                          expression,
                                                          decoratorHelperImpl,
                                                          null,
                                                          null).generateJavaFile(targetDirectory, false);
            }
            if (decoratorWithClassifierTagValue != null) {
                String[] split = decoratorWithClassifierTagValue.split("\\s*~\\s*");
                for (String s : split) {
                    int classifierIndex = s.indexOf("|");
                    if (classifierIndex == -1) {
                        throw new IllegalStateException("Can't find classifier name from " + s);
                    }
                    String classifier = s.substring(0, classifierIndex);
                    String expression = s.substring(classifierIndex + 1);
                    DecoratorDefinitionJavaFileBuilder.create(packageName,
                                                              className,
                                                              classifier,
                                                              false,
                                                              expression,
                                                              decoratorHelperImpl,
                                                              null,
                                                              null).generateJavaFile(targetDirectory, false);
                }

            }
        }
    }

    protected String getDecoratorHelperImpl(String modelName, @SuppressWarnings("rawtypes") Class<? extends DecoratorRenderer> decoratorHelper) {
        return String.format("%s.%s%s", translatePackageName(decoratorHelper.getPackageName()), modelName, decoratorHelper.getSimpleName());
    }

    protected String translatePackageName(String packageName) {
        return packageName;
    }

    protected Path getTargetDirectory() {
        if (targetDirectory == null) {
            targetDirectory = TopiaMetadataModelGeneratorSupport.getNotGeneratedSourceDirector(configuration.getProperty(TemplateConfiguration.PROP_OUTPUT_DIRECTORY, File.class)).toPath();
        }
        return targetDirectory;
    }
}
