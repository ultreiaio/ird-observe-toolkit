package fr.ird.observe.toolkit.templates.validation;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Place here all validation tag-values.
 * <p>
 * Created on 15/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
@Component(role = TagValueMetadatasProvider.class, hint = "toolkit-validation")
@AutoService(TagValueMetadatasProvider.class)
public class ValidationTagValues extends DefaultTagValueMetadatasProvider {

    public ValidationTagValues() {
        super(Store.values());
    }

    @Override
    public String getDescription() {
        return "Tag values Toolkit - validation";
    }

    /**
     * Obtain the value of the {@link Store#positiveNumber} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#positiveNumber
     */
    public boolean isPositiveNumber(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.positiveNumber, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#strictlyPositiveNumber} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#strictlyPositiveNumber
     */
    public boolean isStrictlyPositiveNumber(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.strictlyPositiveNumber, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#notNull} tag value on the given classifier.
     * <p>
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#notNull
     */
    public boolean isNotNull(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.notNull, clazz, attribute);
    }
    /**
     * Obtain the value of the {@link Store#notBlank} tag value on the given classifier.
     * <p>
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#notBlank
     */
    public boolean isNotBlank(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.notBlank, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#mayNotNull} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#mayNotNull
     */
    public boolean isMayNotNull(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.mayNotNull, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#unique} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#unique
     */
    public boolean isUnique(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.unique, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#stringMaxLength} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#stringMaxLength
     */
    public Integer getStringMaxLength(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeIntegerTagValue(Store.stringMaxLength, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#commentNeeded} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#commentNeeded
     */
    public String getCommentNeeded(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.commentNeeded, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#notNullIf} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#notNullIf
     */
    public String getNotNullIf(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.notNullIf, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#mayNotNullIf} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#mayNotNullIf
     */
    public String getMayNotNullIf(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.mayNotNullIf, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#boundNumber} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#boundNumber
     */
    public String getBoundNumber(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.boundNumber, clazz, attribute);
    }

    public String getDayBefore(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.dayBefore, clazz, attribute);
    }

    public String getDayAfter(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.dayAfter, clazz, attribute);
    }
    public String getDayAfterWarning(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.dayAfterWarning, clazz, attribute);
    }

    public String getTimeAfter(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.timeAfter, clazz, attribute);
    }
    public String getTimestampAfterWarning(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.timestampAfterWarning, clazz, attribute);
    }

    public String getTimestampAfter(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.timestampAfter, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#notNullCoordinate} tag value on the given classifier.
     *
     * @param store tag-values store
     * @param clazz attribute classifier
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#notNullCoordinate
     */
    public String getNotNullCoordinate(ObjectModelTagValuesStore store, ObjectModelClass clazz) {
        return store.findClassifierTagValue(Store.notNullCoordinate, clazz);
    }

    /**
     * Obtain the value of the {@link Store#mayNotNullCoordinate} tag value on the given classifier.
     *
     * @param store tag-values store
     * @param clazz attribute classifier
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#mayNotNullCoordinate
     */
    public String getMayNotNullCoordinate(ObjectModelTagValuesStore store, ObjectModelClass clazz) {
        return store.findClassifierTagValue(Store.mayNotNullCoordinate, clazz);
    }

    /**
     * Obtain the value of the {@link Store#proportionTotal} tag value on the given classifier.
     *
     * @param store tag-values store
     * @param clazz attribute classifier
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#proportionTotal
     */
    public String getProportionTotal(ObjectModelTagValuesStore store, ObjectModelClass clazz) {
        return store.findClassifierTagValue(Store.proportionTotal, clazz);
    }
    public String getAtLeastOneSelected(ObjectModelTagValuesStore store, ObjectModelClass clazz) {
        return store.findClassifierTagValue(Store.atLeastOneSelected, clazz);
    }

    public String getAtLeastOneSelected( ObjectModelClass clazz) {
        return TagValueUtil.findTagValue(Store.atLeastOneSelected,clazz);
    }

    /**
     * Obtain the value of the {@link Store#collectionUniqueKey} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#collectionUniqueKey
     */
    public String getCollectionUniqueKey(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeTagValue(Store.collectionUniqueKey, clazz, attribute);
    }

    public enum Store implements TagValueMetadata {

        mayNotNull("Pour qualifier un attribut demandé non null (mais qui peut rester null)", boolean.class, null, ObjectModelAttribute.class),
        mayNotNullIf("Pour qualifier un attribut demandé non null (mais qui peut rester null) avec une condition", String.class, null, ObjectModelAttribute.class),
        notNull("Pour qualifier un attribut non null", boolean.class, null, ObjectModelAttribute.class),
        notBlank("Pour qualifier un attribut texte dont la valeur ne peut pas etre vide (une fois un auto-trim effectué à gauche et à droite)", boolean.class, null, ObjectModelAttribute.class),
        dayAfter("Pour qualifier un attribut date avec validation", String.class, null, ObjectModelAttribute.class),
        dayAfterWarning("Pour qualifier un attribut date avec validation warning", String.class, null, ObjectModelAttribute.class),
        dayBefore("Pour qualifier un attribut date avec validation", String.class, null, ObjectModelAttribute.class),
        timeAfter("Pour qualifier un attribut time avec validation", String.class, null, ObjectModelAttribute.class),
        timestampAfterWarning("Pour qualifier un attribut time avec validation warning", String.class, null, ObjectModelAttribute.class),
        timestampAfter("Pour qualifier un attribut timestamp avec validation", String.class, null, ObjectModelAttribute.class),
        notNullIf("Pour qualifier un attribut non null avec une condition pour ne pas traiter la validation", boolean.class, null, ObjectModelAttribute.class),
        unique("Pour qualifier un attribut qui doit être unique", boolean.class, null, ObjectModelAttribute.class),
        positiveNumber("Pour qualifier un attribut positif", boolean.class, null, ObjectModelAttribute.class),
        strictlyPositiveNumber("Pour qualifier un attribut strictement positif", boolean.class, null, ObjectModelAttribute.class),
        stringMaxLength("Pour assurer une taille maximum sur un attribut de type texte", int.class, null, ObjectModelAttribute.class),
        commentNeeded("Pour demander un commentaire si les référentiels sont désactivés pu requièrent un commentaire sur un attribut de type texte, format: property1,property2,...", String.class, null, ObjectModelAttribute.class),
        boundNumber("Pour qualifier un intervalle autorisé sur un attribut de type nombre (avec une condition optionnelle), format: (:)min:max(:skipCondition (:) pour mettre en warning", String.class, null, ObjectModelAttribute.class),
        notNullCoordinate("Pour qualifier les coordonnées obligatoires sur la classe, format: name1,name2,... (name1 peut valoir default, dans ce cas pas de préfixe (latitude, longitude, quadrant), si commence par : alors (latitudeX,...) sinon (XLatitude,...)", String.class, null, ObjectModelClass.class),
        mayNotNullCoordinate("Pour qualifier les coordonnées optionnelles sur la classe, format: name1,name2,... (name1 peut valoir default, dans ce cas pas de préfixe (latitude, longitude, quadrant), si commence par : alors (latitudeX,...) sinon (XLatitude,...)", String.class, null, ObjectModelClass.class),
        proportionTotal("Pour qualifier les attributs somme de proportions sur une classe format: name1,name2,...", String.class, null, ObjectModelClass.class),
        atLeastOneSelected("Pour qualifier les attributs booléens dont au moins un doit etre sélectionné sur une classe format: name1,name2,...", String.class, null, ObjectModelClass.class),
        collectionUniqueKey("Pour vérifier l'unicité sur une collection, format: property1,property2,... (la clef d'unicité)", String.class, null, ObjectModelAttribute.class),
        ;

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = new LinkedHashSet<>(Arrays.asList(targets));
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }
    }
}

