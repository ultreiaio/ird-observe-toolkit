package fr.ird.observe.toolkit.templates;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.WithAtLeastOneSelected;
import fr.ird.observe.dto.data.NoValidationDto;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.io.TagValuesExtractor;
import fr.ird.observe.toolkit.templates.validation.ValidationTagValues;
import io.ultreia.java4all.util.SortedProperties;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.ObjectModelType;
import org.nuiton.topia.templates.TopiaExtensionTagValues;
import org.nuiton.topia.templates.TopiaHibernateTagValues;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 26/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.5
 */
@Component(role = Template.class, hint = "fr.ird.observe.toolkit.templates.ExtractTagValues")
public class ExtractTagValues extends ObjectModelGenerator {

    private static final Set<String> DECIMAL_TYPES = Set.of(
            Float.class.getSimpleName(), Float.class.getName(),
            Double.class.getSimpleName(), Double.class.getName()
    );

    private String filePrefix;
    private Path outputFile;
    private Path resourceDirectory;
    private TagValuesExtractor extractor;

    @Override
    public void applyTemplate(ObjectModel model, File destDir) throws IOException {
        String packageName = getConfiguration().getProperty(PROP_DEFAULT_PACKAGE);

        Path rootResourcePath = getConfiguration().getProperty(PROP_RESOURCE_DIRECTORY, File.class).toPath();
        Path rootOutputPath = getConfiguration().getProperty(PROP_OUTPUT_DIRECTORY, File.class).toPath().getParent().resolve("java");


        String classifier;
        if (packageName.contains(".dto")) {
            classifier = "dto";
        } else if (packageName.contains(".entities")) {
            classifier = "persistence";
        } else {
            throw new IllegalStateException("Can't manage packageName: " + packageName);
        }
        resourceDirectory = TagValuesExtractor.getModelPath(rootResourcePath, model, classifier);
        getLog().info(String.format("[%s] Will generate tag values at %s", classifier, resourceDirectory));

        ProjectPackagesDefinition projectPackagesDefinition;
        try (URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{rootResourcePath.toUri().toURL()}, getClassLoader())) {
            projectPackagesDefinition = ProjectPackagesDefinition.of(urlClassLoader);
        }
        extractor = createExtractor(model, packageName, classifier, projectPackagesDefinition);
        extractor.load(getClassLoader(), resourceDirectory);

        Set<String> tagValueKeys = extractor.getModelTagValueKeys();
        for (String tagValuesKey : tagValueKeys) {
            filePrefix = tagValuesKey;
            super.applyTemplate(model, destDir);
        }
        extractor.store(rootOutputPath);
    }
    protected TagValuesExtractor createExtractor(ObjectModel model, String packageName, String classifier, ProjectPackagesDefinition projectPackagesDefinition) {
        if ("persistence".equals(classifier)) {
            return new TagValuesExtractor(model, packageName, classifier, getLog()) {

                @Override
                protected void extractAttributeTagValues(ObjectModelClass aClass, ObjectModelAttribute attribute) {
                    super.extractAttributeTagValues(aClass, attribute);
                    checkDateTagValue(aClass, attribute);
                    checkDecimalAttribute(aClass, attribute);
                }

                private void checkDateTagValue(ObjectModelClass aClass, ObjectModelAttribute attribute) {
                    if (attribute.getType().equals("java.util.Date")) {
                        // must check if the attribute is defined by hibernateAttributeType.java.util.Date
                        String attributeName = attribute.getName();
                        boolean booleanTagValue = containsAttributeTagValue(aClass, attributeName, TopiaHibernateTagValues.Store.hibernateAttributeType.name() + ".java.util.Date");
                        if (booleanTagValue) {
                            return;
                        }
                        throw new IllegalStateException(String.format("Missing hibernateAttributeType.java.util.Date tag-value on %s#%s (you can use time, date or timestamp)", aClass.getQualifiedName(), attributeName));
                    }
                }

                private void checkDecimalAttribute( ObjectModelClass aClass, ObjectModelAttribute attribute) {
                    if (DECIMAL_TYPES.contains(attribute.getType())) {
                        // check if digits tag value is set
                        String attributeName = attribute.getName();
                        boolean booleanTagValue = containsAttributeTagValue(aClass, attributeName, TopiaExtensionTagValues.Store.digits.name());
                        if (booleanTagValue) {
                            return;
                        }
                        throw new IllegalStateException(String.format("Missing digits tag-value on %1$s#%2$s (you need to set it on %1$s.attribute.%2$s)", projectPackagesDefinition.getRelativeEntityPackage(aClass.getQualifiedName()).substring(1), attributeName));
                    }
                }
            };
        }
        if ("dto".equals(classifier)) {
            return new TagValuesExtractor(model, packageName, classifier, getLog()) {
                final Set<String> skipNotBlankAttributeNames = Set.of("id", "code", "parentId");

                @Override
                protected void extractAttributeTagValues(ObjectModelClass aClass, ObjectModelAttribute attribute) {
                    super.extractAttributeTagValues(aClass, attribute);
                    addNotBlankTagValue(aClass, attribute);
                }

                @Override
                protected void extractClassTagValues(ObjectModelClass aClass) {
                    super.extractClassTagValues(aClass);
                    processAtLeastOneTagValue(aClass);
                }

                private void processAtLeastOneTagValue(ObjectModelClass aClass) {
                    String atLeastOneSelectedTagValue = getClassTagValue(aClass, ValidationTagValues.Store.atLeastOneSelected.name());
                    if (atLeastOneSelectedTagValue != null) {
                        if (atLeastOneSelectedTagValue.startsWith(":")) {
                            atLeastOneSelectedTagValue = atLeastOneSelectedTagValue.substring(1);
                        }
                        String i18nTagValue = getClassTagValue(aClass, ToolkitTagValues.Store.i18nLabels.name());
                        List<String> i18nParts;
                        if (i18nTagValue == null) {
                            i18nParts = new ArrayList<>();
                        } else {
                            i18nParts = new ArrayList<>(List.of(i18nTagValue.split("\\s*,\\s*")));
                        }
                        List<String> i18nPartsMissing = new ArrayList<>();
                        List<String> atLeastOneSelectedParts = new ArrayList<>(List.of(atLeastOneSelectedTagValue.split("\\s*,\\s*")));
                        for (String atLeastOneSelectedPart : atLeastOneSelectedParts) {
                            augments(i18nParts, i18nPartsMissing, atLeastOneSelectedPart);
                        }
                        augments(i18nParts, i18nPartsMissing, WithAtLeastOneSelected.PROPERTY_AT_LEAST_ONE_SELECTED);
                        augments(i18nParts, i18nPartsMissing, "validation." + WithAtLeastOneSelected.PROPERTY_AT_LEAST_ONE_SELECTED);
                        if (i18nPartsMissing.size() > 0) {
                            i18nParts.addAll(i18nPartsMissing);
                            getLog().warn(String.format("Add missing i18nLabel tag-value parts on %s: %s", aClass.getQualifiedName(), String.join(",", i18nPartsMissing)));
                            addClassTagValue(aClass, ToolkitTagValues.Store.i18nLabels.name(), String.join(",", i18nParts), true);
                        }
                    }
                }

                private void augments(List<String> parts, List<String> missingParts, String key) {
                    if (!parts.contains(key)) {
                        missingParts.add(key);
                    }
                }

                private void addNotBlankTagValue(ObjectModelClass aClass, ObjectModelAttribute attribute) {
                    if (aClass.getName().equals("Business")) {
                        return;
                    }
                    if (aClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(NoValidationDto.class.getName()))) {
                        return;
                    }
                    if (!attribute.getType().equals("java.lang.String")) {
                        return;
                    }
                    if (GeneratorUtil.isNMultiplicity(attribute)) {
                        return;
                    }
                    String attributeName = attribute.getName();
                    if (skipNotBlankAttributeNames.contains(attributeName)) {
                        return;
                    }
                    if (attributeName.startsWith("computedWhen")) {
                        return;
                    }
                    boolean booleanTagValue = containsAttributeTagValue(aClass, attributeName, ValidationTagValues.Store.notBlank.name());
                    if (booleanTagValue) {
                        return;
                    }
                    getLog().warn(String.format("Add missing notBlank tag-value on %s#%s", aClass.getQualifiedName(), attributeName));
                    addAttributeTagValue(aClass, attributeName, ValidationTagValues.Store.notBlank.name(), "true");
                }

                @Override
                protected void addReferentialWithCode(ObjectModelClass aClass, boolean withCodeDto) {
                    super.addReferentialWithCode(aClass, withCodeDto);
                    if (withCodeDto) {
                        //FIXME Make this also for persistence
                        addAttributeTagValue(aClass, "code", "unique", "true");
                    }
                }
            };
        }
        throw new IllegalStateException("Can't manage classifier " + classifier);
    }

    @Override
    protected void generateFromElement(Object element, File destDir, String filename, ObjectModelType type) {
        if (ObjectModelType.OBJECT_MODEL != type) {
            // only generate on model
            return;
        }
        super.generateFromElement(element, destDir, filename, type);
    }

    @Override
    public void generateFromModel(Writer output, ObjectModel input) throws IOException {
        // get existing tag values
        SortedProperties fromFile = extractor.createOrLoadFromFile(outputFile, filePrefix);

        // get tag values from model
        Map<String, String> fromModel = extractor.loadFromModel(filePrefix);

        // merge and store
        extractor.mergeAndStore(fromFile, fromModel, outputFile, output, filePrefix);
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return filePrefix + ".properties";
    }

    @Override
    protected File getDestinationFile(File destDir, String filename) {
        int endIndex = filename.indexOf("-");
        outputFile = resourceDirectory.resolve(filename.substring(0, endIndex)).resolve(filename.substring(filename.lastIndexOf("-") + 1));
        return outputFile.toFile();
    }
}
