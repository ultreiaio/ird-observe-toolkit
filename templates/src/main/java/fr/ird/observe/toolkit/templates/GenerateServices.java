package fr.ird.observe.toolkit.templates;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.templates.services.GenerateDifferentialMetaModelClass;
import fr.ird.observe.toolkit.templates.services.GenerateDifferentialMetaModelFile;
import fr.ird.observe.toolkit.templates.services.GenerateReferentialSecondLevelDeepBehaviourClass;
import fr.ird.observe.toolkit.templates.services.GenerateReferentialSecondLevelDeepBehaviourFile;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4
 */
@Component(role = Template.class, hint = "fr.ird.observe.toolkit.templates.GenerateServices")
public class GenerateServices extends DtoMetaTransformer {

    @SuppressWarnings("unchecked")
    public GenerateServices() {
        setTemplateTypes(
                GenerateDifferentialMetaModelFile.class,
                GenerateDifferentialMetaModelClass.class,
                GenerateReferentialSecondLevelDeepBehaviourFile.class,
                GenerateReferentialSecondLevelDeepBehaviourClass.class
        );
    }
}
