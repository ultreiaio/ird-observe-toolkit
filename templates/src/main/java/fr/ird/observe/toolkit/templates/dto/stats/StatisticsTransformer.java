package fr.ird.observe.toolkit.templates.dto.stats;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonObject;
import fr.ird.observe.dto.stats.StatisticDefinition;
import fr.ird.observe.dto.stats.StatisticValue;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.dto.stats.WithStatisticsBuilder;
import fr.ird.observe.dto.stats.WithStatisticsHolder;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.json.toolkit.stats.StatisticsHolderAdapter;
import fr.ird.observe.toolkit.templates.TemplateContract;
import io.ultreia.java4all.util.json.JsonAdapter;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created on 31/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.1.4
 */
public class StatisticsTransformer extends ObjectModelTransformerToJava implements TemplateContract {


    @Override
    public void transformFromModel(ObjectModel model) {
        Map<ObjectModelClass, List<StatisticDefinitionModel>> statistics = StatisticsHelper.toModel(model, ProjectPackagesDefinition.of(getClassLoader()));
        I18nKeySet i18nGetterFile = getI18nGetterFile();
        for (Map.Entry<ObjectModelClass, List<StatisticDefinitionModel>> entry : statistics.entrySet()) {
            ObjectModelClass input = entry.getKey();
            List<StatisticDefinitionModel> definitions = entry.getValue();
            i18nGetterFile.addKeys(definitions.stream().map(StatisticDefinitionModel::label).collect(Collectors.toCollection(TreeSet::new)));
            generateEnum(input, definitions);
            generateContract(input, definitions);
            generateBuilder(input, definitions);
            generateHolder(input, definitions);
            generateHolderHolderAdapter(input, definitions);
        }
    }

    private void generateEnum(ObjectModelClass input, List<StatisticDefinitionModel> definitions) {

        String name = StatisticsHelper.getStatTypeName(input);
        ObjectModelEnumeration enumeration = createEnumeration(name, input.getPackageName());
        addInterface(enumeration, StatisticDefinition.class);

        addAttribute(enumeration, "label", String.class.getSimpleName(), null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);
        addAttribute(enumeration, "symbol", String.class.getSimpleName(), null, ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);

        ObjectModelOperation constructor = addConstructor(enumeration, ObjectModelJavaModifier.PRIVATE);
        addParameter(constructor, String.class.getSimpleName(), "symbol");
        addParameter(constructor, String.class.getSimpleName(), "label");
        setOperationBody(constructor, ""/*{
        this.symbol = symbol;
        this.label = label;
        }*/);

        ObjectModelOperation labelOperation = addOperation(enumeration, "label", String.class.getSimpleName());
        addAnnotation(enumeration, labelOperation, Override.class);
        setOperationBody(labelOperation, ""/*{
        return label;
        }*/);
        ObjectModelOperation symbolOperation = addOperation(enumeration, "symbol", String.class.getSimpleName());
        addAnnotation(enumeration, symbolOperation, Override.class);
        setOperationBody(symbolOperation, ""/*{
        return symbol;
        }*/);

        for (StatisticDefinition statisticDefinition : definitions) {
            addLiteral(enumeration, ""/*{<%=statisticDefinition.name()%>("<%=statisticDefinition.symbol()%>", "<%=statisticDefinition.label()%>")}*/);
        }
    }


    public void generateContract(ObjectModelClass input, List<StatisticDefinitionModel> statisticDefinitions) {
        if (statisticDefinitions == null) {
            return;
        }
        String contractName = StatisticsHelper.getWithStatisticsName(input);
        String holderName = StatisticsHelper.getWithStatisticsHolderName(input);
        String enumName = StatisticsHelper.getStatTypeName(input);
        String newHolderInstance = String.format("new %s()", holderName);
        ObjectModelInterface output = createInterface(contractName, input.getPackageName());
        addImport(output, List.class);
        addImport(output, List.class);

        addInterface(output, WithStatistics.class.getName() + "<" + contractName + ">");

        addImport(output, StatisticValue.class);

        ObjectModelOperation statisticsOperation = addOperation(output, "statistics", holderName);
        addAnnotation(output, statisticsOperation, Override.class);

        ObjectModelOperation setStatisticsOperation = addOperation(output, "setStatistics", void.class);
        addParameter(setStatisticsOperation, holderName, "statistics");

        ObjectModelOperation copyToOperation = addOperation(output, "copyTo", void.class.getSimpleName(), ObjectModelJavaModifier.DEFAULT);
        addAnnotation(output, copyToOperation, Override.class);
        addParameter(copyToOperation, contractName, "target");
        setOperationBody(copyToOperation, ""/*{
        statistics().copyTo(target);
    }*/);

        ObjectModelOperation addStatisticsOperation = addOperation(output, "addStatistics", "void", ObjectModelJavaModifier.DEFAULT);
        addAnnotation(output, addStatisticsOperation, Override.class);
        setOperationBody(addStatisticsOperation, ""/*{
        setStatistics(<%=newHolderInstance%>);
    }*/);

        ObjectModelOperation removeStatisticsOperation = addOperation(output, "removeStatistics", "void", ObjectModelJavaModifier.DEFAULT);
        addAnnotation(output, removeStatisticsOperation, Override.class);
        setOperationBody(removeStatisticsOperation, ""/*{
        setStatistics(null);
    }*/);

        for (StatisticDefinitionModel statisticDefinition : statisticDefinitions) {

            String name = statisticDefinition.name();
            String fieldName = name + "Stat";
            String getName = "get" + Strings.capitalize(fieldName);
            String getValueName = getName + "Value";
            String setValueName = "s" + getValueName.substring(1);

            ObjectModelOperation getStatOperation = addOperation(output, getName, StatisticValue.class.getSimpleName(), ObjectModelJavaModifier.DEFAULT);
            setOperationBody(getStatOperation, ""/*{
        return withStatistics() ? statistics().<%=getName%>() : null;
    }*/);
            ObjectModelOperation getStatValueOperation = addOperation(output, getValueName, long.class.getSimpleName(), ObjectModelJavaModifier.DEFAULT);
            setOperationBody(getStatValueOperation, ""/*{
        return withStatistics() ? statistics().<%=getValueName%>() : -1L;
    }*/);
            ObjectModelOperation setValueOperation = addOperation(output, setValueName, void.class.getSimpleName(), ObjectModelJavaModifier.DEFAULT);
            addParameter(setValueOperation, long.class.getSimpleName(), "value");
            setOperationBody(setValueOperation, ""/*{
        if (withStatistics()) {
            statistics().<%=setValueName%>(value);
        }
    }*/);
        }
    }

    public void generateBuilder(ObjectModelClass input, List<StatisticDefinitionModel> statisticDefinitions) {
        if (statisticDefinitions == null) {
            return;
        }
        String contractName = StatisticsHelper.getWithStatisticsName(input);
        String builderName = StatisticsHelper.getWithStatisticsBuilderName(input);
        String holderName = StatisticsHelper.getWithStatisticsHolderName(input);

        ObjectModelInterface output = createInterface(builderName, input.getPackageName());

        addInterface(output, WithStatisticsBuilder.class.getName() + "<" + contractName + ">");

        String enumName = StatisticsHelper.getStatTypeName(input);
        List<String> buildStatisticsStatements = new LinkedList<>();
        for (StatisticDefinitionModel statisticDefinition : statisticDefinitions) {

            String name = statisticDefinition.name();
            String fieldName = name + "Stat";
            String getName = "get" + Strings.capitalize(fieldName);
            String getValueName = getName + "Value";
            String setValueName = "s" + getValueName.substring(1);
            String buildValueName = "build" + getValueName.substring(3);

            addOperation(output, buildValueName, long.class.getSimpleName());
            buildStatisticsStatements.add(""/*{statistics.<%=setValueName%>(<%=buildValueName%>());
}*/);
        }

        ObjectModelOperation buildStatisticsOperation = addOperation(output, "buildStatistics", void.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.DEFAULT);
        addParameter(buildStatisticsOperation, contractName, "target");
        addAnnotation(output, buildStatisticsOperation, Override.class);
        String buildContent = ""/*{
        <%=holderName%> statistics = target.statistics();
        }*/ + String.join("        ", buildStatisticsStatements) + "    ";
        setOperationBody(buildStatisticsOperation, buildContent);
    }

    public void generateHolder(ObjectModelClass input, List<StatisticDefinitionModel> statisticDefinitions) {
        if (statisticDefinitions == null) {
            return;
        }
        String contractName = StatisticsHelper.getWithStatisticsName(input);
        String builderName = StatisticsHelper.getWithStatisticsBuilderName(input);
        String holderName = StatisticsHelper.getWithStatisticsHolderName(input);

        ObjectModelClass output = createClass(holderName, input.getPackageName());

        addImport(output, StatisticValue.class);
        addImport(output, List.class);
        setSuperClass(output, WithStatisticsHolder.class.getName() + "<" + contractName + ">");
        addInterface(output, contractName);
        addInterface(output, builderName);

        ObjectModelOperation statisticsOperation = addOperation(output, "statistics", holderName, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, statisticsOperation, Override.class);
        setOperationBody(statisticsOperation, ""/*{
        return this;
    }*/);

        ObjectModelOperation setStatisticsOperation = addOperation(output, "setStatistics", void.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, setStatisticsOperation, Override.class);
        addParameter(setStatisticsOperation, holderName, "statistics");
        setOperationBody(setStatisticsOperation, ""/*{
        // in holder, can't set another statisticsHolder
    }*/);
        String enumName = StatisticsHelper.getStatTypeName(input);
        for (StatisticDefinitionModel statisticDefinition : statisticDefinitions) {

            String name = statisticDefinition.name();
            String fieldName = name + "Stat";
            String getName = "get" + Strings.capitalize(fieldName);
            String getValueName = getName + "Value";
            String setValueName = "s" + getValueName.substring(1);
            String buildValueName = "build" + getValueName.substring(3);

            addAttribute(output, fieldName, StatisticValue.class.getSimpleName(), String.format("new %s(%s.%s)", StatisticValue.class.getSimpleName(), enumName, name), ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.FINAL);

            ObjectModelOperation getOperation = addOperation(output, getName, StatisticValue.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
            addAnnotation(output, getOperation, Override.class);
            setOperationBody(getOperation, ""/*{
        return <%=fieldName%>;
    }*/);
            ObjectModelOperation getValueOperation = addOperation(output, getValueName, long.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
            addAnnotation(output, getValueOperation, Override.class);
            setOperationBody(getValueOperation, ""/*{
        return <%=fieldName%>.getValue();
    }*/);
            ObjectModelOperation setValueOperation = addOperation(output, setValueName, void.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
            addAnnotation(output, setValueOperation, Override.class);
            addParameter(setValueOperation, long.class.getSimpleName(), "value");
            setOperationBody(setValueOperation, ""/*{
        <%=fieldName%>.setValue(value);
    }*/);
            ObjectModelOperation buildOperation = addOperation(output, buildValueName, long.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
            addAnnotation(output, buildOperation, Override.class);
            setOperationBody(buildOperation, ""/*{
        return <%=getValueName%>();
    }*/);

        }
    }

    public void generateHolderHolderAdapter(ObjectModelClass input, List<StatisticDefinitionModel> statisticDefinitions) {
        if (statisticDefinitions == null) {
            return;
        }
        String contractName = StatisticsHelper.getWithStatisticsName(input);
        String holderName = StatisticsHelper.getWithStatisticsHolderName(input);
        String holderAdapterName = StatisticsHelper.getWithStatisticsHolderAdapterName(input);
        ObjectModelClass output = createClass(holderAdapterName, input.getPackageName());

        addImport(output, JsonObject.class);
        addImport(output, StatisticsHolderAdapter.class);
        setSuperClass(output, StatisticsHolderAdapter.class.getName() + "<" + contractName + ", " + holderName + ">");

        ImportsManager importManager = getImportManager(output);
        ObjectModelAnnotation annotation = addAnnotation(output, output, AutoService.class);
        addAnnotationClassParameter(importManager, output, annotation, "value", JsonAdapter.class);

        List<String> serializeStatements = new LinkedList<>();
        List<String> deserializeStatements = new LinkedList<>();
        for (StatisticDefinitionModel statisticDefinition : statisticDefinitions) {

            String name = statisticDefinition.name();
            String fieldName = name + "Stat";
            String getName = "get" + Strings.capitalize(fieldName);
            String getValueName = getName + "Value";
            String setValueName = "s" + getValueName.substring(1);
            serializeStatements.add(""/*{setStaticsValue(target, source.<%=getName%>());
}*/);
            deserializeStatements.add(""/*{target.<%=setValueName%>(getStaticsValue(source, "<%=fieldName%>"));
}*/);
        }
        ObjectModelOperation serializeOperation = addOperation(output, "serialize", JsonObject.class.getSimpleName(), ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.FINAL);
        addParameter(serializeOperation, holderName, "source");
        addAnnotation(output, serializeOperation, Override.class);
        String serializeContent = ""/*{
        JsonObject target = new JsonObject();
        }*/ + String.join("        ", serializeStatements) + ""/*{        return target;
    }*/;
        setOperationBody(serializeOperation, serializeContent);

        ObjectModelOperation deserializeOperation = addOperation(output, "deserialize", holderName, ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.FINAL);
        addParameter(deserializeOperation, JsonObject.class.getSimpleName(), "source");
        addAnnotation(output, deserializeOperation, Override.class);
        String deserializeContent = ""/*{
        <%=holderName%> target = new <%=holderName%>();
        }*/ + String.join("        ", deserializeStatements) + ""/*{        return target;
    }*/;
        setOperationBody(deserializeOperation, deserializeContent);

        ObjectModelOperation typeOperation = addOperation(output, "type", Class.class.getSimpleName() + "<" + holderName + ">", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, typeOperation, Override.class);
        setOperationBody(typeOperation, ""/*{
        return <%=holderName%>.class;
    }*/);
    }
}
