package fr.ird.observe.toolkit.templates.dto.stats;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.stats.StatisticValue;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created on 31/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.1.4
 */
public class StatisticsHelper {

    public static Map<ObjectModelClass, List<StatisticDefinitionModel>> toModel(ObjectModel objectModel, ProjectPackagesDefinition projectPackagesDefinition) {
        Map<ObjectModelClass, List<StatisticDefinitionModel>> statistics = new LinkedHashMap<>();
        ToolkitTagValues tagValues = new ToolkitTagValues();
        for (ObjectModelClass objectModelClass : Objects.requireNonNull(objectModel).getClasses()) {
            String[] statisticsTagValue = tagValues.getStatisticsTagValue(objectModelClass);
            if (statisticsTagValue != null) {
                List<StatisticDefinitionModel> list = toModel(objectModel, objectModelClass, projectPackagesDefinition, statisticsTagValue);
                statistics.put(objectModelClass, list);
            }
        }
        return Collections.unmodifiableMap(statistics);
    }

    public static List<StatisticDefinitionModel> toModel(ObjectModel objectModel, ObjectModelClassifier objectModelClass, ProjectPackagesDefinition projectPackagesDefinition, String[] statisticsTagValue) {
        String labelPrefix = Introspector.decapitalize(objectModel.getName());
        List<StatisticDefinitionModel> list = new ArrayList<>(statisticsTagValue.length);
        for (String value : statisticsTagValue) {
            String[] s = value.split("\\s*:\\s*");
            String symbol = s[0];
            String name = s[1];
            String labelPackage = labelPrefix + projectPackagesDefinition.getRelativeDtoPackage(objectModelClass.getQualifiedName());
            String label = String.format("%s.%sStat", labelPackage, name);
            list.add(new StatisticDefinitionModel(name, label, symbol));
        }
        return list;
    }

    public static String getStatTypeName(ObjectModelClass input) {
        return input.getName() + "StatType";
    }

    public static String getWithStatisticsName(ObjectModelClass input) {
        return input.getName() + "Stat";
    }

    public static String getWithStatisticsBuilderName(ObjectModelClass input) {
        return input.getName() + "StatBuilder";
    }

    public static String getWithStatisticsHolderName(ObjectModelClass input) {
        return input.getName() + "StatHolder";
    }

    public static String getWithStatisticsHolderAdapterName(ObjectModelClass input) {
        return input.getName() + "StatHolderAdapter";
    }

    public static void addWithStatistics(TemplateContract template, String packageName, ObjectModelClass input, ObjectModelClass output, ObjectModelInterface outputInterface, List<StatisticDefinitionModel> statisticDefinitions) {
        if (statisticDefinitions == null) {
            return;
        }
        String holderName = getWithStatisticsHolderName(input);
        boolean withInterface = outputInterface != null;
        String enumName = getStatTypeName(input);
        template.addImport(output, packageName + "." + enumName);
        template.addImport(output, packageName + "." + holderName);

        String newHolderInstance = String.format("new %s()", holderName);
        ObjectModelAttribute statisticsAttribute = template.addAttribute(output, "statistics", holderName, newHolderInstance, ObjectModelJavaModifier.PRIVATE);
        if (withInterface) {
            ((ObjectModelAttributeImpl) statisticsAttribute).setTransient(true);
        }
        ObjectModelOperation statisticsOperation = template.addOperation(output, "statistics", holderName, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        template.addAnnotation(output, statisticsOperation, Override.class);
        template.setOperationBody(statisticsOperation, ""/*{
        return statistics;
    }*/);

        ObjectModelOperation setStatisticsOperation = template.addOperation(output, "setStatistics", void.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        template.addAnnotation(output, setStatisticsOperation, Override.class);
        template.addParameter(setStatisticsOperation, holderName, "statistics");
        template.setOperationBody(setStatisticsOperation, ""/*{
        this.statistics = statistics;
    }*/);

        for (StatisticDefinitionModel statisticDefinition : statisticDefinitions) {

            String name = statisticDefinition.name();
            String fieldName = name + "Stat";
            String getName = "get" + Strings.capitalize(fieldName);

            if (withInterface) {
                // add buildXXXValue default implementation (based on input attribute)
                String buildValueName = "build" + getName.substring(3) + "Value";
                if (input.getAttribute(name) != null) {
                    String getterName = "get" + Strings.capitalize(name) + "Size";
                    ObjectModelOperation buildStatValueOperation = template.addOperation(output, buildValueName, long.class.getSimpleName());
                    template.addAnnotation(output, buildStatValueOperation, Override.class);
                    template.setOperationBody(buildStatValueOperation, ""/*{
        return (long) <%=getterName%>();
    }*/);
                }
            }
        }
        template.addImport(output, StatisticValue.class);
        template.addImport(output, List.class);
        if (withInterface) {
            template.addInterface(outputInterface, packageName + "." + getWithStatisticsName(input));
            template.addInterface(outputInterface, packageName + "." + getWithStatisticsBuilderName(input));
        } else {
            template.addInterface(output, packageName + "." + getWithStatisticsName(input));
        }
    }

    public static String generateDecoratorWithStatsExpression(String decoratorTagValue, List<StatisticDefinitionModel> statisticDefinitions) {
        Iterator<StatisticDefinitionModel> iterator = statisticDefinitions.iterator();
        StatisticDefinitionModel firstStat = iterator.next();
        StringBuilder expressionBuilder = new StringBuilder(decoratorTagValue);
        expressionBuilder.append(String.format("##( ${%sStat::stat}", firstStat.name()));
        while (iterator.hasNext()) {
            StatisticDefinitionModel nextStat = iterator.next();
            expressionBuilder.append(String.format("##${%sStat::stat}", nextStat.name()));
        }
        expressionBuilder.append(" )");
        return expressionBuilder.toString();
    }
}
