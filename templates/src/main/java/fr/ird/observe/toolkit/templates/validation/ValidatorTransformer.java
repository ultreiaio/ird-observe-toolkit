package fr.ird.observe.toolkit.templates.validation;

/*
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.io.MoreFiles;
import com.google.common.io.RecursiveDeleteOption;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.WithAtLeastOneSelected;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.InlineDataDto;
import fr.ird.observe.dto.data.LayoutAware;
import fr.ird.observe.dto.data.NoValidationDto;
import fr.ird.observe.dto.data.WithProportion;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithNoCodeAware;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import fr.ird.observe.toolkit.templates.dto.DtoTransformer;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducer;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.validation.api.NuitonValidatorFileInfo;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModelEntry;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviders;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.api.SimpleNuitonValidationContext;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorDefinitionBuilder;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorFileDefinitionHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.TemplateConfiguration;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.java.BeanTransformerTagValues;
import org.nuiton.eugene.java.EugeneJavaTagValues;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ValidatorTransformer extends ObjectModelTransformerToJava {

    private static final Logger log = LogManager.getLogger(ValidatorTransformer.class);
    public static final String PREFIX = "default";
    public static final String SUFFIX = ".json";

    public static void main(String[] args) throws IllegalAccessException, IOException {
        Path rootPath = Path.of("/tmp").resolve(PREFIX);
        if (Files.notExists(rootPath)) {
            Files.createDirectories(rootPath);
        }
        for (Field field : ValidatorTransformer.class.getDeclaredFields()) {
            String fieldName = field.getName();
            log.debug("Test field: {}", fieldName);
            if (Modifier.isStatic(field.getModifiers()) && Objects.equals(field.getType(), String.class)&& !fieldName.startsWith("PROP_")&& fieldName.contains("_TEMPLATE")) {
                Path path = rootPath.resolve(fieldName + SUFFIX);
                if (Files.notExists(path)) {
                    log.info("Create file: {}", path);
                    String content = (String) field.get(ValidatorTransformer.class);
                    Files.write(path, content.getBytes(StandardCharsets.UTF_8));
                }
            }
        }
    }
    private static final String FILE_FORMAT = "{\n" +
            "%1$s" +
            "\n" +
            "}";
    private static final String FIELD_FORMAT =
            "  \"%1$s\": [\n" +
                    "%2$s\n" +
                    "  ]";
    private static final String DISABLED_ERRORS_FIELD_TEMPLATE = loadTemplate("DISABLED_ERRORS_FIELD_TEMPLATE");
    private static final String DISABLED_WARNING_FIELD_TEMPLATE = loadTemplate("DISABLED_WARNING_FIELD_TEMPLATE");
    private static final String COMMENT_NEED_FIELD_TEMPLATE = loadTemplate("COMMENT_NEED_FIELD_TEMPLATE");
    private static final String MANDATORY_FIELD_TEMPLATE = loadTemplate("MANDATORY_FIELD_TEMPLATE");
    private static final String MANDATORY_LATITUDE_FIELD_TEMPLATE = loadTemplate("MANDATORY_LATITUDE_FIELD_TEMPLATE");
    private static final String MANDATORY_LONGITUDE_FIELD_TEMPLATE = loadTemplate("MANDATORY_LONGITUDE_FIELD_TEMPLATE");
    private static final String MANDATORY_QUADRANT_FIELD_TEMPLATE = loadTemplate("MANDATORY_QUADRANT_FIELD_TEMPLATE");
    private static final String OPTIONAL_LATITUDE_FIELD_TEMPLATE = loadTemplate("OPTIONAL_LATITUDE_FIELD_TEMPLATE");
    private static final String OPTIONAL_COORDINATE_FIELD_TEMPLATE = loadTemplate("OPTIONAL_COORDINATE_FIELD_TEMPLATE");
    private static final String OPTIONAL_LONGITUDE_FIELD_TEMPLATE = loadTemplate("OPTIONAL_LONGITUDE_FIELD_TEMPLATE");
    private static final String OPTIONAL_QUADRANT_FIELD_TEMPLATE = loadTemplate("OPTIONAL_QUADRANT_FIELD_TEMPLATE");
    private static final String MANDATORY_IF_FIELD_TEMPLATE = loadTemplate("MANDATORY_IF_FIELD_TEMPLATE");
    private static final String MANDATORY_IF_FIELD_TEMPLATE2 = loadTemplate("MANDATORY_IF_FIELD_TEMPLATE2");
    private static final String PROPORTION_TOTAL_FIELD_TEMPLATE = loadTemplate("PROPORTION_TOTAL_FIELD_TEMPLATE");
    private static final String AT_LEAST_ONE_SELECTED_FIELD_TEMPLATE = loadTemplate("AT_LEAST_ONE_SELECTED_FIELD_TEMPLATE");
    private static final String BOUND_NUMBER_FIELD_TEMPLATE = loadTemplate("BOUND_NUMBER_FIELD_TEMPLATE");
    private static final String BOUND_NUMBER_IF_FIELD_TEMPLATE = loadTemplate("BOUND_NUMBER_IF_FIELD_TEMPLATE");
    private static final String MANDATORY_STRING_FIELD_TEMPLATE = loadTemplate("MANDATORY_STRING_FIELD_TEMPLATE");
    private static final String NOT_BLANK_STRING_FIELD_TEMPLATE = loadTemplate("NOT_BLANK_STRING_FIELD_TEMPLATE");
    private static final String MANDATORY_IF_STRING_FIELD_TEMPLATE = loadTemplate("MANDATORY_IF_STRING_FIELD_TEMPLATE");
    private static final String MANDATORY_IF_STRING_FIELD_TEMPLATE2 = loadTemplate("MANDATORY_IF_STRING_FIELD_TEMPLATE2");
    private static final String DATA_EMPTY_FIELD_TEMPLATE2 = loadTemplate("DATA_EMPTY_FIELD_TEMPLATE2");
    private static final String MANDATORY_COLLECTION_FIELD_TEMPLATE = loadTemplate("MANDATORY_COLLECTION_FIELD_TEMPLATE");
    private static final String NOT_FILLED_FIELD_TEMPLATE = loadTemplate("NOT_FILLED_FIELD_TEMPLATE");
    private static final String NOT_FILLED_IF_FIELD_TEMPLATE = loadTemplate("NOT_FILLED_IF_FIELD_TEMPLATE");
    private static final String NOT_FILLED_IF_WITH_I18N_FIELD_TEMPLATE = loadTemplate("NOT_FILLED_IF_WITH_I18N_FIELD_TEMPLATE");
    private static final String NOT_FILLED_STRING_FIELD_TEMPLATE = loadTemplate("NOT_FILLED_STRING_FIELD_TEMPLATE");
    private static final String NOT_FILLED_COLLECTION_FIELD_TEMPLATE = loadTemplate("NOT_FILLED_COLLECTION_FIELD_TEMPLATE");
    private static final String REFERENTIAL_UNIQUE_FIELD_TEMPLATE = loadTemplate("REFERENTIAL_UNIQUE_FIELD_TEMPLATE");
    private static final String POSITIVE_NUMBER_FIELD_TEMPLATE = loadTemplate("POSITIVE_NUMBER_FIELD_TEMPLATE");
    private static final String STRICTLY_POSITIVE_NUMBER_FIELD_TEMPLATE = loadTemplate("STRICTLY_POSITIVE_NUMBER_FIELD_TEMPLATE");
    private static final String PROPORTION_FIELD_TEMPLATE = loadTemplate("PROPORTION_FIELD_TEMPLATE");
    private static final String STRING_MAX_LENGTH_FIELD_TEMPLATE = loadTemplate("STRING_MAX_LENGTH_FIELD_TEMPLATE");
    private static final String COLLECTION_UNIQUE_KEY_FIELD_TEMPLATE = loadTemplate("COLLECTION_UNIQUE_KEY_FIELD_TEMPLATE");
    private static final String DAY_AFTER_DEFAULT_FIELD_TEMPLATE = loadTemplate("DAY_AFTER_DEFAULT_FIELD_TEMPLATE");
    private static final String DAY_AFTER_FIELD_TEMPLATE = loadTemplate("DAY_AFTER_FIELD_TEMPLATE");
    private static final String TIME_AFTER_FIELD_TEMPLATE = loadTemplate("TIME_AFTER_FIELD_TEMPLATE");
    private static final String TIMESTAMP_AFTER_FIELD_TEMPLATE = loadTemplate("TIMESTAMP_AFTER_FIELD_TEMPLATE");
    private static final String TIMESTAMP_STRICT_AFTER_FIELD_TEMPLATE = loadTemplate("TIMESTAMP_STRICT_AFTER_FIELD_TEMPLATE");
    private static final String DAY_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE = loadTemplate("DAY_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE");
    private static final String DAY_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE = loadTemplate("DAY_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE");
    private static final String TIME_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE = loadTemplate("TIME_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE");
    private static final String TIME_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE = loadTemplate("TIME_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE");
    private static final String TIMESTAMP_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE = loadTemplate("TIMESTAMP_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE");
    private static final String TIMESTAMP_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE = loadTemplate("TIMESTAMP_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE");
    private static final String DAY_BEFORE_FIELD_TEMPLATE = loadTemplate("DAY_BEFORE_FIELD_TEMPLATE");
    private final EugeneCoreTagValues coreTagValues;
    private final EugeneJavaTagValues javaTemplatesTagValues;
    private final BeanTransformerTagValues beanTagValues;
    private final ToolkitTagValues toolkitTagValues;
    private final ValidationTagValues validationTagValues;
    /**
     * Store all fields for any type found in a validator (used to generate i18n keys)
     */
    private final Multimap<Class<?>, String> allFieldsByType;
    private final Map<String, ContextMap> contexts;
    Function<String, String> dayFunction = f -> f.replace("timeStamp", "date")
                                                 .replace("endTimeStamp", "endDate")
                                                 .replace("EndTimeStamp", "EndDate")
                                                 .replace("startTimeStamp", "startDate")
                                                 .replace("StartTimeStamp", "StartDate");
    Function<String, String> timeFunction = f -> f.replace("timeStamp", "time")
                                                  .replace("endTimeStamp", "endTime")
                                                  .replace("EndTimeStamp", "EndTime")
                                                  .replace("startTimeStamp", "startTime")
                                                  .replace("StartTimeStamp", "StartTime");
    private Class<?> dtoClazz;
    private BeanPropertyI18nKeyProducer labelsBuilder;
    private ProjectValidatorDefinition projectValidatorDefinition;

    public ValidatorTransformer() {
        coreTagValues = new EugeneCoreTagValues();
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
        toolkitTagValues = new ToolkitTagValues();
        validationTagValues = new ValidationTagValues();
        allFieldsByType = MultimapBuilder.hashKeys().hashSetValues().build();
        contexts = new TreeMap<>();
    }

    public static String loadTemplate(String templateName) {
        return loadTemplate(ValidatorTransformer.class, ValidatorTransformer.PREFIX +"/"+ templateName);
    }

    public static String loadTemplate(Class<?> origin, String templateName) {
        String content = ObserveUtil.loadResourceContent(origin.getResource( templateName + SUFFIX));
        return Arrays.stream(content.split("\n")).map(s->"    "+s).collect(Collectors.joining("\n"));
    }

    public static String removeVariables(String tagValue) {
        return tagValue.replaceAll("\\$o\\.", "").replaceAll("\\$c\\.", "");
    }

    static List<NuitonValidatorFileInfo> loadUserValidators(Path userPath, Multimap<Class<?>, String> allFieldsByType, Consumer<String> infoLogger, Consumer<String> debugLogger) {
        try {
            NuitonValidatorProvider provider = NuitonValidatorProviders.newProvider(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME, new SimpleNuitonValidationContext(Locale.FRANCE));
            List<NuitonValidatorFileInfo> userValidators = new LinkedList<>();
            NuitonValidatorModelHelper helper = new NuitonValidatorModelHelper();
            List<NuitonValidatorModel<?>> validatorModels = helper.read(provider.getName(), userPath);
            for (NuitonValidatorModel<?> validatorModel : validatorModels) {
                NuitonValidatorModelEntry<?> key = validatorModel.getKey();
                Class<?> type = key.getType();
                String context = key.getContext();
                for (NuitonValidatorScope scope : key.getScopes()) {
                    Path file = provider.toPath(key, userPath, scope);
                    debugLogger.accept("Detect user validator: " + file);
                    validatorModel.getFieldNames(scope).ifPresent(fieldNames -> {
                        NuitonValidatorFileInfo validatorFileInfo = provider.toFileInfo(file, type, context, scope, fieldNames);
                        userValidators.add(validatorFileInfo);
                        debugLogger.accept("Detect user validator: " + validatorFileInfo);
                        allFieldsByType.putAll(type, fieldNames);
                    });
                }
            }
            infoLogger.accept(String.format("%d user validator(s) detected.", userValidators.size()));
            return userValidators;
        } catch (IOException e) {
            throw new IllegalStateException("Can't get validators cache", e);
        }
    }

    public static Map<String, String> getStringProperties(ObjectModelClass beanClass, Collection<ObjectModelAttribute> attributes, BiFunction<ObjectModelClass, ObjectModelAttribute, String> filter) {
        Map<String, String> properties = new LinkedHashMap<>();
        for (ObjectModelAttribute attr : attributes) {
            if (attr.isNavigable()) {
                String tagValue = filter.apply(beanClass, attr);
                if (tagValue != null) {
                    properties.put(attr.getName(), tagValue);
                }
            }
        }
        return properties;
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);
        Path sourceRootPath = getSourceRootPath();
        Path userValidationRootPath = sourceRootPath.getParent().resolve("validation");
        //FIXME Should remove only json files
        // remove all generated files in this path
        try {
            //noinspection UnstableApiUsage
            MoreFiles.deleteDirectoryContents(sourceRootPath, RecursiveDeleteOption.ALLOW_INSECURE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<NuitonValidatorFileInfo> userValidators = loadUserValidators(userValidationRootPath);
        BeanTransformerContext context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, false, false, input -> {
            ObjectModelPackage aPackage = model.getPackage(input.getPackageName());
            Collection<ObjectModelAttribute> attributes = new LinkedList<>(input.getAttributes());
            attributes.addAll(input.getAllOtherAttributes());
            boolean needComment = attributes.stream().anyMatch(e -> e.getName().equals("comment"));
            String formTagValue = toolkitTagValues.getFormTagValue(input, aPackage);
            return needComment || toolkitTagValues.notSkip(formTagValue) != null;
        }, getLog());

        context.report();
        labelsBuilder = I18nDecoratorHelper.get().getDefaultLabelsBuilder();

        List<ValidationTransformerCallback> callbacks = new LinkedList<>();
        for (ValidationTransformerCallback callback : ServiceLoader.load(ValidationTransformerCallback.class, getClassLoader())) {
            callbacks.add(callback);
        }

        ProjectPackagesDefinition def = ProjectPackagesDefinition.of(getClassLoader());

        for (ObjectModelClass beanClass : context.selectedClasses.stream().sorted(Comparator.comparing(ObjectModelClassifier::getQualifiedName)).collect(Collectors.toList())) {

            if (beanClass.isStatic()) {
                // do not generate validators for static dto
                log.warn("Skip static bean {}", beanClass.getQualifiedName());
                continue;
            }
            if (beanClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(NoValidationDto.class.getName()))) {
                // do not generate validators for NoValidationDto
                continue;
            }

            contexts.values().forEach(ContextMap::clear);
            contexts.put("create", new ContextMap("create"));
            contexts.put("update", new ContextMap("update"));

            prepareBean(context, def, beanClass, userValidators, callbacks);
            processBean(context, beanClass, sourceRootPath);
        }
        generateI18n(getI18nGetterFile());
    }

    protected void generateI18n(I18nKeySet i18nGetterFile) {
        BeanPropertyI18nKeyProducer labelsBuilder = I18nDecoratorHelper.get().getDefaultLabelsBuilder();
        int typeCount = 0;
        int fieldCount = 0;
        for (Map.Entry<Class<?>, Collection<String>> entry : allFieldsByType.asMap().entrySet()) {
            dtoClazz = entry.getKey();
            typeCount++;
            Collection<String> allFields = entry.getValue();
            getLog().debug(String.format("%s - Detected %d fields for type: %s", this, fieldCount += allFields.size(), dtoClazz.getName()));
            getLog().debug(String.format("\n\t%s", String.join("\n\t", allFields)));
            for (String field : allFields) {
                String key = labelsBuilder.getI18nPropertyKey(dtoClazz, field);
                i18nGetterFile.addKey(key);
            }
        }
        getLog().info(String.format("%s - Detected %d type(s) and %d field(s).", this, typeCount, fieldCount));
    }

    protected void prepareBean(BeanTransformerContext context, ProjectPackagesDefinition def, ObjectModelClass beanClass, List<NuitonValidatorFileInfo> userValidators, List<ValidationTransformerCallback> callbacks) {

        String dtoType = context.classesNameTranslation.get(beanClass);
        ObjectModelPackage thisPackage = getPackage(beanClass);
        boolean referential = TemplateContract.isReferentialFromPackageName(thisPackage.getName());

        dtoClazz = Objects2.forName(thisPackage.getName() + "." + dtoType);

        String formTagValue = toolkitTagValues.getFormTagValue(beanClass, thisPackage).trim();

        Collection<ObjectModelAttribute> attributes = new LinkedList<>(beanClass.getAttributes());
        attributes.addAll(beanClass.getAllOtherAttributes());

        ObjectModelTagValuesStore tagValuesStore = model.getTagValuesStore();
        Set<String> skipCopyToEntity = new TreeSet<>();
        for (ObjectModelAttribute attribute : attributes) {
            if (toolkitTagValues.isSkipCopyToEntity(tagValuesStore, beanClass, attribute)) {
                skipCopyToEntity.add(attribute.getName());
            }
        }
        String atLeastOneSelected = getStringProperties(beanClass, c -> validationTagValues.getAtLeastOneSelected(tagValuesStore, c));
        if (atLeastOneSelected != null) {
            boolean warning = atLeastOneSelected.startsWith(":");
            if (warning) {
                atLeastOneSelected = atLeastOneSelected.substring(1);
            }
            String mainFiled = WithAtLeastOneSelected.PROPERTY_AT_LEAST_ONE_SELECTED;

            String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, "validation." + mainFiled);
            String content = String.format(AT_LEAST_ONE_SELECTED_FIELD_TEMPLATE, atLeastOneSelected, i18n);
            if (warning) {
                addWarningValidator(mainFiled, content);
            } else {
                addErrorValidator(mainFiled, content);
            }
        }
        boolean needComment = attributes.stream().anyMatch(e -> e.getName().equals("comment"));
        if (needComment || formTagValue.equals(thisPackage.getName() + "." + dtoType)) {

            String mandatoryCoordinate = getStringProperties(beanClass, c -> validationTagValues.getNotNullCoordinate(tagValuesStore, c));
            String mayNotNullCoordinate = getStringProperties(beanClass, c -> validationTagValues.getMayNotNullCoordinate(tagValuesStore, c));
            String proportionTotal = getStringProperties(beanClass, c -> validationTagValues.getProportionTotal(tagValuesStore, c));
            Map<String, String> boundNumber = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getBoundNumber(tagValuesStore, c, a));
            Map<String, String> mandatoryIf = getStringProperties(beanClass, attributes, (c, a) -> {
                if ("String".equals(GeneratorUtil.getSimpleName(a.getType()))) {
                    return null;
                }
                return validationTagValues.getNotNullIf(tagValuesStore, c, a);
            });
            Map<String, String> mandatoryStringIf = getStringProperties(beanClass, attributes, (c, a) -> {
                if (!"String".equals(GeneratorUtil.getSimpleName(a.getType()))) {
                    return null;
                }
                return validationTagValues.getNotNullIf(tagValuesStore, c, a);
            });

            Map<String, String> mayNotNullIf = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getMayNotNullIf(tagValuesStore, c, a));
            Map<String, String> commentNeeded = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getCommentNeeded(tagValuesStore, c, a));
            Map<String, String> collectionUniqueKey = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getCollectionUniqueKey(tagValuesStore, c, a));
            Map<String, Integer> stringMaxLength = getIntegerProperties(beanClass, attributes, (c, a) -> validationTagValues.getStringMaxLength(tagValuesStore, c, a));
            for (String attributeName : skipCopyToEntity) {
                stringMaxLength.remove(attributeName);
            }
            Map<String, String> dayBefore = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getDayBefore(tagValuesStore, c, a));
            Map<String, String> dayAfter = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getDayAfter(tagValuesStore, c, a));
            Map<String, String> dayAfterWarning = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getDayAfterWarning(tagValuesStore, c, a));
            Map<String, String> timeAfter = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getTimeAfter(tagValuesStore, c, a));
            Map<String, String> timestampAfter = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getTimestampAfter(tagValuesStore, c, a));
            Map<String, String> timestampAfterWarning = getStringProperties(beanClass, attributes, (c, a) -> validationTagValues.getTimestampAfterWarning(tagValuesStore, c, a));
            List<String> mandatory = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isNotNull(tagValuesStore, c, a) && !"String".equals(GeneratorUtil.getSimpleName(a.getType())));
            List<String> mandatoryString = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isNotNull(tagValuesStore, c, a) && "String".equals(GeneratorUtil.getSimpleName(a.getType())));
            List<String> mandatoryCollection = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isNotNull(tagValuesStore, c, a) && GeneratorUtil.isNMultiplicity(a));
            List<String> notBlankString = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isNotBlank(tagValuesStore, c, a) && "String".equals(GeneratorUtil.getSimpleName(a.getType())));
            if (referential && beanClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(ReferentialDtoReferenceWithNoCodeAware.class.getName()))) {
                notBlankString.remove("code");
            }
            if (!referential) {
                if (beanClass.getSuperclasses().stream().anyMatch(i -> i.getQualifiedName().equals(Strings.removeEnd(ContainerDto.class.getName(), "Dto")))
                        || beanClass.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(LayoutAware.class.getName()))) {
                    notBlankString.remove("homeId");
                }
            }
            for (String attributeName : skipCopyToEntity) {
                notBlankString.remove(attributeName);
            }
            mandatory.removeAll(mandatoryString);
            mandatory.removeAll(mandatoryCollection);
            mandatoryString.removeAll(mandatoryCollection);
            //FIXME We are still using InlineDataDto on LonglineElementAware and does not have the full support of inline stuff
            //FIXME We should remove InlineDataDto on LonglineElementAware
            if (InlineDataDto.class.isAssignableFrom(dtoClazz) && toolkitTagValues.getInlineDataDtoPropertiesTagValue(beanClass) != null) {
                // all mandatory fields except if isDataEmpty
                mandatory.forEach(f -> {
                    addErrorValidator(f, String.format(MANDATORY_IF_FIELD_TEMPLATE2, f, "$o.dataEmpty", "dataEmpty"));
                    addWarningValidator(f, DATA_EMPTY_FIELD_TEMPLATE2);
                });
                mandatory.clear();
                // all mandatory string fields except if isDataEmpty
                mandatoryString.forEach(f -> {
                    addErrorValidator(f, String.format(MANDATORY_IF_STRING_FIELD_TEMPLATE2, f, "$o.dataEmpty", "dataEmpty"));
                    addWarningValidator(f, DATA_EMPTY_FIELD_TEMPLATE2);
                });
                mandatoryString.clear();
            }

            List<String> mayNotNull = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isMayNotNull(tagValuesStore, c, a) && !"String".equals(GeneratorUtil.getSimpleName(a.getType())));
            List<String> mayNotNullString = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isMayNotNull(tagValuesStore, c, a) && "String".equals(GeneratorUtil.getSimpleName(a.getType())));
            List<String> mayNotNullCollection = TemplateContract.getProperties(beanClass, attributes, (c, a) -> validationTagValues.isMayNotNull(tagValuesStore, c, a) && GeneratorUtil.isNMultiplicity(a));
            mayNotNull.removeAll(mayNotNullString);
            mayNotNull.removeAll(mayNotNullCollection);
            mayNotNullString.removeAll(mayNotNullCollection);

            List<String> positiveNumber = TemplateContract.getProperties(beanClass, attributes, (c, a) -> isNumber(a) && validationTagValues.isPositiveNumber(tagValuesStore, c, a));
            List<String> strictlyPositiveNumber = TemplateContract.getProperties(beanClass, attributes, (c, a) -> isNumber(a) && validationTagValues.isStrictlyPositiveNumber(tagValuesStore, c, a));
            List<String> referentialUnique = TemplateContract.getProperties(beanClass, attributes, (c, a) -> referential && validationTagValues.isUnique(tagValuesStore, c, a));
            List<String> proportion = TemplateContract.getProperties(beanClass, attributes, (c, a) -> !referential && a.getName().equals(WithProportion.PROPERTY_PROPORTION));

            Map<String, String> properties = getProperties(attributes);
            Map<String, String> multipleProperties = getMultipleProperties(attributes);

            getLog().debug(String.format("form: %s, found %d referential(s) and %s referential collection(s).", dtoType, properties.size(), multipleProperties.size()));
            addErrorValidators(mandatory, MANDATORY_FIELD_TEMPLATE);
            addErrorValidators(mandatoryString, MANDATORY_STRING_FIELD_TEMPLATE);
            addErrorValidators(notBlankString, NOT_BLANK_STRING_FIELD_TEMPLATE);
            addErrorValidators(mandatoryCollection, MANDATORY_COLLECTION_FIELD_TEMPLATE);
            addErrorValidators(mandatoryIf, (f, v) -> {
                String value = (String) v;
                String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, f + ".validation.required");
                return String.format(MANDATORY_IF_FIELD_TEMPLATE, f, value, i18n, removeVariables(value));
            });
            addErrorValidators(mandatoryStringIf, (f, v) -> {
                String value = (String) v;
                String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, f);
                return String.format(MANDATORY_IF_STRING_FIELD_TEMPLATE, f, value, i18n, removeVariables(value));
            });

            addTemporalValidators(dayBefore, dayAfter, dayAfterWarning, timeAfter, timestampAfter, timestampAfterWarning);

            addErrorValidators(positiveNumber, POSITIVE_NUMBER_FIELD_TEMPLATE);
            addErrorValidators(strictlyPositiveNumber, STRICTLY_POSITIVE_NUMBER_FIELD_TEMPLATE);
            addErrorValidators(referentialUnique, REFERENTIAL_UNIQUE_FIELD_TEMPLATE);
            addErrorValidators(proportion, PROPORTION_FIELD_TEMPLATE);
            addErrorValidators(stringMaxLength, STRING_MAX_LENGTH_FIELD_TEMPLATE);
            addErrorValidators(commentNeeded, COMMENT_NEED_FIELD_TEMPLATE);

            addBoundNumbers(boundNumber);

            addErrorValidators(collectionUniqueKey, (f, v) -> {
                String value = (String) v;
                String validatorType = GenerateCollectionUniqueKeyValidators.dtoToValidationPackageName(dtoClazz) + "." + GenerateCollectionUniqueKeyValidators.dtoToValidationClassName(dtoClazz, f);
                return String.format(COLLECTION_UNIQUE_KEY_FIELD_TEMPLATE, f, value, validatorType);
            });

            Set<String> quadrantToAdd = new LinkedHashSet<>();
            if (mandatoryCoordinate != null) {
                String[] split = mandatoryCoordinate.split("\\s*,\\s*");
                for (String coordinatePrefix : split) {
                    boolean before = coordinatePrefix.startsWith(":");
                    Pair<String, String> pair = DtoTransformer.getCoordinateMainAndPrefix(before, coordinatePrefix);
                    String main = pair.getLeft();
                    coordinatePrefix = pair.getRight();

                    String latitude = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Latitude");
                    String longitude = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Longitude");
                    String quadrant = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Quadrant");

                    addLatitude(main, latitude, null);
                    addLongitude(main, longitude, null);
                    if (!referential) {
                        quadrantToAdd.add(quadrant);
                    }
                }
            }
            addWarningValidators(mayNotNullIf, (f, v) -> {
                String value = (String) v;
                boolean withI18n = value.startsWith(":");
                if (withI18n) {
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, f);
                    return String.format(NOT_FILLED_IF_WITH_I18N_FIELD_TEMPLATE, f, value.substring(1), i18n, removeVariables(value.substring(1)));
                }
                return String.format(NOT_FILLED_IF_FIELD_TEMPLATE, f, value, removeVariables(value));
            });
            if (mayNotNullCoordinate != null) {
                boolean warning = mayNotNullCoordinate.startsWith("+");
                if (warning) {
                    mayNotNullCoordinate = mayNotNullCoordinate.substring(1);
                }
                String[] split = mayNotNullCoordinate.split("\\s*,\\s*");
                for (String coordinatePrefix : split) {
                    boolean before = coordinatePrefix.startsWith(":");
                    Pair<String, String> pair = DtoTransformer.getCoordinateMainAndPrefix(before, coordinatePrefix);
                    String main = pair.getLeft();
                    coordinatePrefix = pair.getRight();

                    String latitude = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Latitude");
                    String longitude = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Longitude");
                    String quadrant = DtoTransformer.getCoordinateField(before, coordinatePrefix, "Quadrant");

                    addLatitude(main, latitude, String.format(OPTIONAL_LATITUDE_FIELD_TEMPLATE, latitude, longitude, quadrant));
                    addLongitude(main, longitude, String.format(OPTIONAL_LONGITUDE_FIELD_TEMPLATE, longitude, latitude, quadrant));
                    addErrorValidator(quadrant, String.format(OPTIONAL_QUADRANT_FIELD_TEMPLATE, quadrant, latitude, longitude));
                    if (warning) {
                        String content = String.format(OPTIONAL_COORDINATE_FIELD_TEMPLATE, quadrant, latitude, longitude);
                        addWarningValidator(quadrant, content);
                        addWarningValidator(latitude, content);
                        addWarningValidator(longitude, content);
                    }
                    if (!referential) {
                        quadrantToAdd.add(quadrant);
                    }
                }
            }
            if (proportionTotal != null) {
                String[] split = proportionTotal.split("\\s*,\\s*");
                for (String field : split) {
                    field += "ProportionSum";
                    String content = String.format(PROPORTION_TOTAL_FIELD_TEMPLATE, field);
                    addErrorValidator(field, content);
                }
            }

            addWarningValidators(mayNotNull, NOT_FILLED_FIELD_TEMPLATE);
            addWarningValidators(mayNotNullString, NOT_FILLED_STRING_FIELD_TEMPLATE);
            addWarningValidators(mayNotNullCollection, NOT_FILLED_COLLECTION_FIELD_TEMPLATE);

            quadrantToAdd.forEach(this::addDataQuadrant);

            for (Map.Entry<String, String> entry : properties.entrySet()) {
                String field = entry.getKey();
                String checkDisabledReferentialOnErrorScope = String.format(DISABLED_ERRORS_FIELD_TEMPLATE, field, "fr.ird.observe.validation.validator.referential.CheckReferentialIsEnabledOnErrorScope");
                addErrorValidator(field, checkDisabledReferentialOnErrorScope);
                String checkDisabledReferentialOnWarningScope = String.format(DISABLED_WARNING_FIELD_TEMPLATE, field, "fr.ird.observe.validation.validator.referential.CheckReferentialIsEnabledOnWarningScope");
                addWarningValidator(field, checkDisabledReferentialOnWarningScope);
            }
            for (Map.Entry<String, String> entry : multipleProperties.entrySet()) {
                String field = entry.getKey();
                String checkDisabledReferentialOnErrorScope = String.format(DISABLED_ERRORS_FIELD_TEMPLATE, field, "fr.ird.observe.validation.validator.referential.CheckReferentialCollectionIsEnabledOnErrorScope");
                addErrorValidator(field, checkDisabledReferentialOnErrorScope);
                String checkDisabledReferentialOnWarningScope = String.format(DISABLED_WARNING_FIELD_TEMPLATE, field, "fr.ird.observe.validation.validator.referential.CheckReferentialCollectionIsEnabledOnWarningScope");
                addWarningValidator(field, checkDisabledReferentialOnWarningScope);
            }
            properties.putAll(multipleProperties);
            if (needComment && !properties.isEmpty()) {
                String content = String.format(COMMENT_NEED_FIELD_TEMPLATE, "comment", String.join(",", properties.keySet()));
                addErrorValidator("comment", content);
            }
        }

        callbacks.forEach(callback-> {
            callback.prepareBean(this, context, def, model, beanClass, attributes, userValidators, dtoClazz);
        });
        addUserValidators(userValidators, dtoClazz);
    }

    private void addTemporalValidators(Map<String, String> dayBefore, Map<String, String> dayAfter, Map<String, String> dayAfterWarning, Map<String, String> timeAfter, Map<String, String> timestampAfter, Map<String, String> timestampAfterWarning) {
        addErrorValidators(dayBefore, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();
            for (String value : values.split("\\s*,\\s*")) {
                value = dayFunction.apply(value);
                String cleanValue = removeVariables(value);
                String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.before.%s", f, cleanValue));
                result.add(String.format(DAY_BEFORE_FIELD_TEMPLATE, f, value, i18n, cleanValue));
            }
            return String.join(",\n", result);
        }, dayFunction);
        addErrorValidators(dayAfter, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();

            for (String value : values.split("\\s*,\\s*")) {
                value = dayFunction.apply(value);
                String cleanValue = removeVariables(value);

                if (cleanValue.equals("startDate")) {
                    result.add(String.format(DAY_AFTER_DEFAULT_FIELD_TEMPLATE, f));
                    continue;
                }
                int index = value.indexOf(":");
                if (index > -1) {
                    String delay = value.substring(index + 1);
                    value = value.substring(0, index);
                    cleanValue = removeVariables(value);
                    String template;
                    String i18n2;
                    if (delay.startsWith("-")) {
                        delay = delay.substring(1);
                        template = DAY_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.short", f));
                    } else {
                        template = DAY_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.long", f));
                    }
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(template, f, value, i18n, delay, i18n2, cleanValue));

                } else {
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(DAY_AFTER_FIELD_TEMPLATE, f, value, i18n, cleanValue));
                }
            }
            return String.join(",\n", result);
        }, dayFunction);
        addWarningValidators(dayAfterWarning, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();

            for (String value : values.split("\\s*,\\s*")) {
                value = dayFunction.apply(value);
                String cleanValue = removeVariables(value);
                if (cleanValue.equals("startDate")) {
                    result.add(String.format(DAY_AFTER_DEFAULT_FIELD_TEMPLATE, f));
                    continue;
                }
                int index = value.indexOf(":");
                if (index > -1) {
                    String delay = value.substring(index + 1);
                    value = value.substring(0, index);
                    cleanValue = removeVariables(value);
                    String template;
                    String i18n2;
                    if (delay.startsWith("-")) {
                        delay = delay.substring(1);
                        template = DAY_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.short", f));
                    } else {
                        template = DAY_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.long", f));
                    }
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(template, f, value, i18n, delay, i18n2, cleanValue));

                } else {
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(DAY_AFTER_FIELD_TEMPLATE, f, value, i18n, cleanValue));
                }
            }
            return String.join(",\n", result);
        }, dayFunction);

        addErrorValidators(timeAfter, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();

            for (String value : values.split("\\s*,\\s*")) {
                value = timeFunction.apply(value);
                String cleanValue = removeVariables(value);
                int index = value.indexOf(":");
                if (index > -1) {
                    String delay = value.substring(index + 1);
                    String template;
                    String i18n2;
                    if (delay.startsWith("-")) {
                        delay = delay.substring(1);
                        template = TIME_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.short", f));
                    } else {
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.long", f));
                        template = TIME_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE;
                    }
                    value = value.substring(0, index);
                    cleanValue = removeVariables(value);
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(template, f, value, i18n, delay, i18n2, cleanValue));

                } else {
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(TIME_AFTER_FIELD_TEMPLATE, f, value, i18n, cleanValue));
                }
            }
            return String.join(",\n", result);
        }, timeFunction);

        addErrorValidators(timestampAfter, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();

            for (String value : values.split("\\s*,\\s*")) {
                String cleanValue = removeVariables(value);
                int index = value.indexOf(":");
                if (index > -1) {
                    String delay = value.substring(index + 1);
                    String template;
                    String i18n2;
                    if (delay.startsWith("-")) {
                        delay = delay.substring(1);
                        template = TIMESTAMP_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.short", f));
                    } else {
                        template = TIMESTAMP_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.long", f));
                    }
                    value = value.substring(0, index);
                    cleanValue = removeVariables(value);
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));

                    result.add(String.format(template, f, value, i18n, delay, i18n2, cleanValue));

                } else {
                    boolean strict = value.startsWith("!");
                    String template;
                    if (strict) {
                        template = TIMESTAMP_STRICT_AFTER_FIELD_TEMPLATE;
                        value = value.substring(1);
                        cleanValue = removeVariables(value);
                    } else {
                        template = TIMESTAMP_AFTER_FIELD_TEMPLATE;
                    }
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(template, f, value, i18n, cleanValue));
                }
            }
            return String.join(",\n", result);
        });
        addWarningValidators(timestampAfterWarning, (f, v) -> {
            String values = (String) v;
            List<String> result = new LinkedList<>();

            for (String value : values.split("\\s*,\\s*")) {
                value = timeFunction.apply(value);
                String cleanValue = removeVariables(value);
                int index = value.indexOf(":");
                if (index > -1) {
                    String delay = value.substring(index + 1);
                    String template;
                    String i18n2;
                    if (delay.startsWith("-")) {
                        delay = delay.substring(1);
                        template = TIMESTAMP_AFTER_WITH_MIN_DELAY_FIELD_TEMPLATE;
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.short", f));
                    } else {
                        i18n2 = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.delay.too.long", f));
                        template = TIMESTAMP_AFTER_WITH_MAX_DELAY_FIELD_TEMPLATE;
                    }
                    value = value.substring(0, index);
                    cleanValue = removeVariables(value);
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(template, f, value, i18n, delay, i18n2, cleanValue));

                } else {
                    String i18n = labelsBuilder.getI18nPropertyKey(dtoClazz, String.format("validation.%s.after.%s", f, cleanValue));
                    result.add(String.format(TIMESTAMP_AFTER_FIELD_TEMPLATE, f, value, i18n, cleanValue));
                }
            }
            return String.join(",\n", result);
        });
    }

    private void addBoundNumbers(Map<String, String> boundNumber) {
        for (Map.Entry<String, String> entry : boundNumber.entrySet()) {
            String value = entry.getValue();
            boolean warning = value.startsWith(":");
            if (warning) {
                value = value.substring(1);
            }
            String field = entry.getKey();
            String[] split = value.split(":");
            String content;
            if (split.length == 2) {
                content = String.format(BOUND_NUMBER_FIELD_TEMPLATE, field, split[0], split[1]);
            } else {
                content = String.format(BOUND_NUMBER_IF_FIELD_TEMPLATE, field, split[0], split[1], split[2], removeVariables(split[2]));
            }
            if (warning) {
                addWarningValidator(field, content);
            } else {
                addErrorValidator(field, content);
            }
        }
    }

    protected void addDataQuadrant(String field) {
        String packageName = dtoClazz.getPackageName();

        int i = packageName.indexOf(".data");
        String packagePart = packageName.substring(i + 6, packageName.indexOf('.', i + 7));
        String ocean = "$c.current" + Strings.capitalize(packagePart) + "CommonTrip.ocean";
        String content = String.format(MANDATORY_QUADRANT_FIELD_TEMPLATE, field, ocean);
        addErrorValidator(field, content);
    }

    protected void addLatitude(String main, String field, String extra) {
        String content = (extra == null ? "" : extra + ",\n") + String.format(MANDATORY_LATITUDE_FIELD_TEMPLATE, field, main);
        addErrorValidator(field, content);

    }

    protected void addLongitude(String main, String field, String extra) {
        String content = (extra == null ? "" : extra + ",\n") + String.format(MANDATORY_LONGITUDE_FIELD_TEMPLATE, field, main);
        addErrorValidator(field, content);
    }

    private ContextMap contextMap(String context) {
        return contexts.computeIfAbsent(context, ContextMap::new);
    }

    protected void processBean(BeanTransformerContext beanTransformerContext, ObjectModelClass beanClass, Path sourceRootPath) {

        String packageName = beanClass.getPackageName();
        String dtoType = beanTransformerContext.classesNameTranslation.get(beanClass);

        Path file = sourceRootPath;
        for (String dir : packageName.split("\\.")) {
            file = file.resolve(dir);
        }

        Path finalFile = file;
        contexts.forEach((context, map) -> {
            map.ifWarning(m -> generateGeneratedValidatorFile(finalFile, context, NuitonValidatorScope.WARNING, dtoType, m));
            map.ifError(m -> generateGeneratedValidatorFile(finalFile, context, NuitonValidatorScope.ERROR, dtoType, m));
        });
    }

    private void generateGeneratedValidatorFile(Path dir, String context, NuitonValidatorScope scope, String fileName, Map<String, List<String>> fragmentEntry) {
        fileName = String.format("%s-%s-%s-validation.json", fileName, context, scope.name().toLowerCase());
        List<String> fields = new LinkedList<>();
        for (Map.Entry<String, List<String>> entry : fragmentEntry.entrySet()) {
            String propertyName = entry.getKey();
            List<String> validators = entry.getValue();
            fields.add(String.format(FIELD_FORMAT, propertyName, String.join(",\n", validators)));
            allFieldsByType.put(dtoClazz, propertyName);
        }
        String fileContent = String.format(FILE_FORMAT, String.join(",\n", fields));
        Path target = dir.resolve(fileName);
        generateFile(target, fileContent);
    }

    private Map<String, String> getProperties(Collection<ObjectModelAttribute> attributes) {
        Map<String, String> properties = new TreeMap<>();
        for (ObjectModelAttribute attr : attributes) {
            if (!attr.isNavigable()) {
                continue;
            }
            if (GeneratorUtil.isNMultiplicity(attr)) {
                continue;
            }
            String type = attr.getType();
            if (!type.endsWith("Reference") || !type.contains("referential")) {
                continue;
            }
            properties.put(attr.getName(), JavaGeneratorUtil.getSimpleName(type));
        }
        return properties;
    }

    private Map<String, String> getMultipleProperties(Collection<ObjectModelAttribute> attributes) {
        Map<String, String> properties = new TreeMap<>();
        for (ObjectModelAttribute attr : attributes) {
            if (!attr.isNavigable()) {
                continue;
            }
            if (!GeneratorUtil.isNMultiplicity(attr)) {
                continue;
            }
            String type = attr.getType();
            if (!type.endsWith("Reference") || !type.contains("referential")) {
                continue;
            }
            properties.put(attr.getName(), JavaGeneratorUtil.getSimpleName(type));
        }
        return properties;
    }

    private Map<String, Integer> getIntegerProperties(ObjectModelClass beanClass, Collection<ObjectModelAttribute> attributes, BiFunction<ObjectModelClass, ObjectModelAttribute, Integer> filter) {
        Map<String, Integer> properties = new LinkedHashMap<>();
        for (ObjectModelAttribute attr : attributes) {
            if (attr.isNavigable()) {
                Integer tagValue = filter.apply(beanClass, attr);
                if (tagValue != null) {
                    properties.put(attr.getName(), tagValue);
                }
            }
        }
        return properties;
    }

    public String getStringProperties(ObjectModelClass beanClass, Function<ObjectModelClass, String> filter) {
        return filter.apply(beanClass);
    }

    private boolean isNumber(ObjectModelAttribute attribute) {
        String type = GeneratorUtil.getSimpleName(attribute.getType());
        return "byte".equals(type) || "short".equals(type)
                || "Byte".equals(type) || "Short".equals(type)
                || "int".equals(type) || "long".equals(type)
                || "Integer".equals(type) || "Long".equals(type)
                || "float".equals(type) || "double".equals(type)
                || "Float".equals(type) || "Double".equals(type);
    }

    public void addErrorValidators(List<String> fields, String template) {
        for (String field : fields) {
            String content = String.format(template, field);
            addErrorValidator(field, content);
        }
    }

    public void addErrorValidators(Map<String, ?> fields, BiFunction<String, Object, String> toContent, Function<String, String> keyFunction) {
        for (Map.Entry<String, ?> entry : fields.entrySet()) {
            String field = entry.getKey();
            if (keyFunction != null) {
                field = keyFunction.apply(field);
            }
            Object value = entry.getValue();
            String content = toContent.apply(field, value);
            addErrorValidator(field, content);
        }
    }

    public void addErrorValidators(Map<String, ?> fields, BiFunction<String, Object, String> toContent) {
        for (Map.Entry<String, ?> entry : fields.entrySet()) {
            String field = entry.getKey();
            Object value = entry.getValue();
            String content = toContent.apply(field, value);
            addErrorValidator(field, content);
        }
    }

    public void addWarningValidators(Map<String, ?> fields, BiFunction<String, Object, String> toContent, Function<String, String> keyFunction) {
        for (Map.Entry<String, ?> entry : fields.entrySet()) {
            String field = entry.getKey();
            if (keyFunction != null) {
                field = keyFunction.apply(field);
            }
            Object value = entry.getValue();
            String content = toContent.apply(field, value);
            addWarningValidator(field, content);
        }
    }

    public void addWarningValidators(Map<String, ?> fields, BiFunction<String, Object, String> toContent) {
        for (Map.Entry<String, ?> entry : fields.entrySet()) {
            String field = entry.getKey();
            Object value = entry.getValue();
            String content = toContent.apply(field, value);
            addWarningValidator(field, content);
        }
    }

    public void addErrorValidators(Map<String, ?> fields, String template) {
        addErrorValidators(fields, (f, v) -> String.format(template, f, v));
    }

    public void addWarningValidators(List<String> fields, String template) {
        for (String field : fields) {
            String content = String.format(template, field);
            addWarningValidator(field, content);
        }
    }

    public void addErrorValidator(String field, String content) {
        contextMap("update").onError(m -> addValidator(m, field, content));
        contextMap("create").onError(m -> addValidator(m, field, content));
    }

    public void addWarningValidator(String field, String content) {
        contextMap("update").onWarning(m -> addValidator(m, field, content));
        contextMap("create").onWarning(m -> addValidator(m, field, content));
    }

    public void addValidator(Map<String, List<String>> map, String field, String template) {
        map.computeIfAbsent(field, e -> new LinkedList<>()).add(String.format(template, field));
    }

    private void generateFile(Path target, String content) {
        try {
            if (!Files.exists(target.getParent())) {
                Files.createDirectories(target.getParent());
            }
            log.info("Generate validation file: " + target);
            Files.write(target, content.getBytes());
        } catch (IOException e) {
            throw new IllegalStateException("Could not create file: " + target);
        }
    }

    public Path getSourceRootPath() {
        return configuration.getProperty(TemplateConfiguration.PROP_RESOURCE_DIRECTORY, File.class).toPath();
    }

    private List<NuitonValidatorFileInfo> loadUserValidators(Path userPath) {
        try (URLClassLoader loader = new URLClassLoader(new URL[]{userPath.toUri().toURL()}, configuration.getClassLoader())) {

            ProjectValidatorDefinitionBuilder helper = new ProjectValidatorDefinitionBuilder(loader);
            URL userValidatorsLocation = loader.getResource(ProjectValidatorFileDefinitionHelper.LOCATION);
            projectValidatorDefinition = helper.build(Objects.requireNonNull(userValidatorsLocation));

            getLog().info(String.format("%d user validator definition(s) detected.", projectValidatorDefinition.getFilesByEntry().size()));

        } catch (IOException e) {
            throw new IllegalStateException("Can't get validators cache", e);
        }
        return ValidatorTransformer.loadUserValidators(userPath, allFieldsByType, getLog()::info, getLog()::debug);

    }

    protected void addUserValidators(List<NuitonValidatorFileInfo> userValidators, Class<?> dtoClazz) {
        getLog().debug(dtoClazz.getName() + " - try to add user validators");
        List<NuitonValidatorFileInfo> validators = userValidators.stream().filter(v -> v.getType().isAssignableFrom(dtoClazz)).collect(Collectors.toList());
        for (NuitonValidatorFileInfo userValidator : validators) {
            NuitonValidatorScope scope = userValidator.getScope();
            ContextMap contextMap = contextMap(userValidator.getContext());
            Map<String, List<String>> map = contextMap.get(scope.name().toLowerCase());
            if (map == null) {
                return;
            }
            getLog().debug(dtoClazz.getName() + " add user validator: " + userValidator);
            FileValidatorEntryDefinition key = new FileValidatorEntryDefinition(userValidator.getType(), userValidator.getContext(), userValidator.getScope().name().toLowerCase());
            FileValidatorDefinition fileValidatorDefinition = projectValidatorDefinition.getFilesByEntry().get(key);
            for (String field : userValidator.getFields()) {
                getLog().debug(String.format("Add user validation on field %s-%s for context: %s/%s", dtoClazz.getName(), field, userValidator.getContext(), scope));
                fileValidatorDefinition.getFields().get(field).forEach(f -> {
                    String content = getFieldFragment(f);
                    if (content != null) {
                        addValidator(map, field, content);
                    }
                });
            }
        }
    }

    protected String getFieldFragment(FieldValidatorDefinition f) {
        String parametersStr = "";
        Map<String, String> parameters = f.getParameters();
        getLog().debug("getFieldFragment: " + f);
        if (parameters != null && !parameters.isEmpty()) {
            parametersStr = String.format(",\n      \"parameters\": {\n%s\n      }", parameters.entrySet().stream().map(e -> String.format("        \"%s\": \"%s\"", e.getKey(), e.getValue())).collect(Collectors.joining(",\n")));
        }
        String messageStr = "";
        String message = f.getMessage();
        if (message != null && !message.isEmpty()) {
            messageStr = String.format(",\n      \"message\": \"%s\"", message);
        }
        return String.format("    {\n" +
                                     "      \"validator\": \"%1$s\",\n" +
                                     "      \"comment\": \"%2$s\"%3$s\n" +
                                     "    }", f.getValidator().getName(), f.getComment(), parametersStr + messageStr);
    }


    static class ContextMap {
        private final String context;
        private final Map<String, List<String>> error;
        private final Map<String, List<String>> warning;

        ContextMap(String context) {
            this.context = Objects.requireNonNull(context);
            warning = new TreeMap<>();
            error = new TreeMap<>();
        }

        public Map<String, List<String>> get(String scope) {
            switch (scope) {
                case "fatal":
                case "error":
                    return error;
                case "warning":
                    return warning;
                default:
                    return null;
            }
        }

        public void clear() {
            error.clear();
            warning.clear();
        }

        public boolean ifWarning(Consumer<Map<String, List<String>>> o) {
            if (!warning.isEmpty()) {
                o.accept(warning);
                return true;
            }
            return false;
        }

        public boolean ifError(Consumer<Map<String, List<String>>> o) {
            if (!error.isEmpty()) {
                o.accept(error);
                return true;
            }
            return false;
        }

        public void onWarning(Consumer<Map<String, List<String>>> o) {
            o.accept(warning);
        }

        public void onError(Consumer<Map<String, List<String>>> o) {
            o.accept(error);
        }
    }
}

