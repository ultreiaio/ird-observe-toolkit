package fr.ird.observe.toolkit.templates.entity.query;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created at 14/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class SqlQueryDefinitions {

    protected static final String VARIABLE_PREFIX = ".parameter.";
    protected static final Pattern VARIABLE_NAME_PATTERN = Pattern.compile("(.+)\\.parameter\\.(\\d+)$");
    private final Set<SqlQueryDefinition> queries;

    public SqlQueryDefinitions(Set<SqlQueryDefinition> queries) {
        this.queries = queries;
    }

    public static Optional<SqlQueryDefinitions> load(String type) {
        URL url = Thread.currentThread().getContextClassLoader().getResource(String.format("META-INF/persistence/%s-sql-queries.properties", type.replaceAll("\\.", "/")));
        return url == null ? Optional.empty() : Optional.of(load(type, url));
    }

    public static SqlQueryDefinitions load(String type, URL url) {
        Map<String, String> properties = PropertiesParser.load(Objects.requireNonNull(url));
        List<SqlQueryDefinition> result = new LinkedList<>();

        Collection<String> queryNameKeys = properties.keySet().stream().filter(key -> !key.contains(".")).collect(Collectors.toCollection(LinkedHashSet::new));

        for (String queryName : queryNameKeys) {
            String value = properties.remove(queryName);
            String commentKey = String.format("%s.comment", queryName);
            String commentValue = properties.remove(commentKey);
            String callableKey = String.format("%s.callable", queryName);
            String callableValue = properties.remove(callableKey);
            boolean callable = false;
            if (callableValue != null) {
                callable = Boolean.parseBoolean(callableValue);
            }
            Map<Integer, String> parametersNamesByOrder = filterParameters(properties, queryName + VARIABLE_PREFIX);
            Map<String, String> parameters = new LinkedHashMap<>(parametersNamesByOrder.size());
            parametersNamesByOrder.forEach((k, location) -> {
                String[] coords = location.split("\\s*\\|\\s*");
                String parameterName = coords[0];
                String parameterType = coords[1];
                parameters.put(parameterName, parameterType);
            });
            result.add(new SqlQueryDefinition(type, queryName, value, commentValue, callable, parameters));
        }
        if (!properties.isEmpty()) {
            throw new IllegalStateException("There is still some not consumed properties " + properties);
        }

        result.sort(Comparator.comparing(SqlQueryDefinition::getFullyQualifiedName));
        return new SqlQueryDefinitions(Collections.unmodifiableSet(new LinkedHashSet<>(result)));
    }

    protected static Map<Integer, String> filterParameters(Map<String, String> reportProperties, String prefixKey) {

        Map<Integer, String> namesByOrder = new TreeMap<>();
        Set<String> propertiesToRemove = new TreeSet<>();
        reportProperties.forEach((key, name) -> {
            if (!key.startsWith(prefixKey)) {
                // prefix does not match
                return;
            }
            Matcher matcher = SqlQueryDefinitions.VARIABLE_NAME_PATTERN.matcher(key);
            if (!matcher.matches()) {
                // name property does not match
                return;
            }
            Integer number = Integer.parseInt(matcher.group(2));
            namesByOrder.put(number, name);
            propertiesToRemove.add(key);
        });
        propertiesToRemove.forEach(reportProperties::remove);
        return namesByOrder;
    }

    public Set<SqlQueryDefinition> getQueries() {
        return queries;
    }

    public Set<SqlQueryDefinition> forType(String qualifiedName) {
        return getQueries().stream().filter(q -> q.getType().equals(qualifiedName)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

}
