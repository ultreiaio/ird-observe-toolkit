package fr.ird.observe.toolkit.templates;

/*
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Defines all tag values managed by Java templates.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
@Component(role = TagValueMetadatasProvider.class, hint = "toolkit")
@AutoService(TagValueMetadatasProvider.class)
public class ToolkitTagValues extends DefaultTagValueMetadatasProvider {

    public ToolkitTagValues() {
        super(Store.values());
    }

    @Override
    public String getDescription() {
        return "Tag values Toolkit";
    }

    public String getReferencesTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.references, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getMainDtoTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.mainDto, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getDecomposeTimestamp(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.decomposeTimestamp, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getInlineDataDtoPropertiesTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.inlineDataDtoProperties, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getContainerChildDataDtoPropertiesTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.containerChildDataDtoProperties, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getNavigationNodeTypeTagValue(ObjectModelClassifier classifier, ObjectModelAttribute attribute) {
        String tagValue = TagValueUtil.findTagValue(Store.navigationNodeType, attribute, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getNavigationNodeExtraTypeTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.navigationNodeExtraType, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getComparatorTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.comparator, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getDecoratorTagValue(ObjectModelPackage aPackage, ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.decorator, classifier, aPackage);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getDecoratorWithClassifierTagValue(ObjectModelPackage aPackage, ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.decoratorWithClassifier, classifier, aPackage);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getI18nLabelsTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.i18nLabels, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String[] getNaturalOrderTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.naturalOrder, classifier);
        return tagValue == null ? null : tagValue.trim().split("\\s*,\\s*");
    }

    public String[] getStatisticsTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.statistics, classifier);
        return tagValue == null ? null : tagValue.trim().split("\\s*,\\s*");
    }

    public String getI18nOverrideLabelsTagValue(ObjectModelClassifier classifier) {
        String tagValue = TagValueUtil.findTagValue(Store.i18nOverrideLabels, classifier);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getFormTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage) {
        String tagValue = TagValueUtil.findTagValue(Store.form, classifier, aPackage);
        return tagValue == null ? null : tagValue.trim();
    }

    public String getPackagePriorityTagValue(ObjectModelPackage aPackage) {
        String tagValue = TagValueUtil.findTagValue(Store.packagePriority, aPackage);
        return tagValue == null ? null : tagValue.trim();
    }

    public boolean isSkipCopyToEntity(ObjectModelTagValuesStore store, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        return store.findAttributeBooleanTagValue(Store.skipCopyToEntity, clazz, attribute);
    }

    /**
     * Obtain the value of the {@link Store#referentialSecondLevelDeepBehaviour} tag value on the given classifier.
     *
     * @param store     tag-values store
     * @param clazz     attribute classifier
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#referentialSecondLevelDeepBehaviour
     * @see <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2846">issue 2846</a>
     */
    public boolean isReferentialSecondLevelDeepBehaviour(ObjectModelTagValuesStore store, ObjectModelClass clazz) {
        return store.findClassifierBooleanTagValue(Store.referentialSecondLevelDeepBehaviour, clazz);
    }

    public String notSkip(String tagValue) {
        return Objects.equals("skip", tagValue) ? null : tagValue;
    }

    public enum Store implements TagValueMetadata {

        references("Pour qualifier les propriété à générer", String.class, null, ObjectModelClassifier.class),

        mainDto("Pour qualifier le dto principal", String.class, null, ObjectModelClassifier.class),

        decomposeTimestamp("Pour decomposer un timestamp en date-time : indiquer la liste des attributs séparés par virgule, les champs supplementaires seront générés", String.class, null, ObjectModelClassifier.class),

        inlineDataDtoProperties("Pour qualifier les propriétés d'un InlineDataDto", String.class, null, ObjectModelClassifier.class),

        containerChildDataDtoProperties("Pour qualifier les propriétés d'un ContainerChildDataDto", String.class, null, ObjectModelClassifier.class),

        navigationNodeExtraType("Pour indiquer des nœuds supplémentaires de type SIMPLE au tout au début", String.class, null, ObjectModelClassifier.class),

        navigationNodeType("Pour indiquer quelle est le type de nœud associé dans la navigation", String.class, null, ObjectModelClassifier.class, ObjectModelAttribute.class),

        skipCopyToEntity("Pour indiquer que l'on ne recopie pas d'un dto vers l'entité", String.class, null, ObjectModelAttribute.class),

        comparator("Pour qualifier l'attribut de comparaison", String.class, null, ObjectModelClassifier.class),
        statistics("Pour qualifier les statistiques d'un object", String.class, null, ObjectModelClassifier.class),

        decorator("Pour qualifier la décoration d'un dto", String.class, null, ObjectModelPackage.class, ObjectModelClassifier.class), decoratorWithClassifier("Pour qualifier la décoration avec un classifier", String.class, null, ObjectModelPackage.class, ObjectModelClassifier.class), i18nLabels("Pour qualifier les propriétés à traduire d'un dto", String.class, null, ObjectModelClassifier.class), naturalOrder("Pour qualifier l'ordre naturel de retour d'une liste de référentiels (plusieurs valeurs possibles: lastName,firstName)", String.class, null, ObjectModelClassifier.class), i18nOverrideLabels("Pour qualifier les propriétés à traduire qui sont surchargé pour ce dto", String.class, null, ObjectModelClassifier.class), Static("Pour indiquer si une classe est static (ici c'est un hack pour ne pas generer la persistence de celle-ci", Boolean.class, "false", ObjectModelClassifier.class),

        form("Pour qualifier les propriétés à générer", String.class, null, ObjectModelClassifier.class, ObjectModelPackage.class),

        /**
         * @see <a href="https://gitlab.com/ultreiaio/ird-observe/-/issues/2846">issue 2846</a>
         */
        referentialSecondLevelDeepBehaviour("Pour qualifier le comportement à utiliser pour les référentiels lors d'une suppression, désactivation ou remplacement (false = comportement par défaut, true = pour effectuer une opération en profondeur sur les référentiels utilisés de second niveau", boolean.class, "false", ObjectModelClassifier.class),

        packagePriority("Pour prioriser les paquetages", int.class, null, ObjectModelPackage.class);

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = new LinkedHashSet<>(Arrays.asList(targets));
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }
}
