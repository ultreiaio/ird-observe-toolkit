package fr.ird.observe.toolkit.templates.dto;

/*
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.DtoAndReferenceAware;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceDefinition;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceDefinition;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithCodeAware;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithNoCodeAware;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.mapping.DtoToReferenceDtoMapping;
import fr.ird.observe.spi.mapping.ReferenceDtoToDtoClassMapping;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticDefinitionModel;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticsHelper;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.java.BeanTransformerTagValues;
import org.nuiton.eugene.java.EugeneJavaTagValues;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Generates a reference of a bean.
 * <p>
 * For example:
 * <pre>
 *     GeneratedBoatReference
 *     BoatReference
 * </pre>
 *
 * @author Tony Chemit - dev@tchemit.Fr
 * @since 1.04
 */
@SuppressWarnings({"unused", "StringOperationCanBeSimplified"})
public class DtoReferenceTransformer extends ObjectModelTransformerToJava implements TemplateContract {

    private final EugeneCoreTagValues coreTagValues;
    private final EugeneJavaTagValues javaTemplatesTagValues;
    private final BeanTransformerTagValues beanTagValues;
    private final ToolkitTagValues observeTagValues;

    private BeanTransformerContext context;
    private Map<ObjectModelClass, List<StatisticDefinitionModel>> statistics;

    public static Map<ObjectModelAttribute, ObjectModelAttribute> getReferenceProperties(List<String> selectedClassesFqn, ObjectModelClass input, Set<String> availableProperties, Function<String, String> classMapping) {
        Collection<ObjectModelAttribute> attributes = new LinkedList<>(input.getAttributes());
        attributes.addAll(input.getAllOtherAttributes());
        Map<String, ObjectModelAttribute> properties = new TreeMap<>();
        Map<ObjectModelAttribute, ObjectModelAttribute> resultMap = new LinkedHashMap<>();
        for (ObjectModelAttribute attr : attributes) {

            if (attr.isNavigable()) {

                String attrName = attr.getName();
                String type = attr.getType();

                if (selectedClassesFqn.contains(ProjectPackagesDefinition.cleanType(type))) {

                    // Dto

                    if (availableProperties.contains(attrName + "Label")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Label");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                        resultMap.put(newAttr, attr);
                    }

                    if (availableProperties.contains(attrName + "Id")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Id");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                        resultMap.put(newAttr, attr);
                    }
                    if (availableProperties.contains(attrName + "Code")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Code");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                        resultMap.put(newAttr, attr);
                    }

                    if (availableProperties.contains(attrName + "Size")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Size");
                        newAttr.setType("int");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                        resultMap.put(newAttr, attr);
                    }

                    // Get a ref

                    if (availableProperties.contains(attrName)) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName);
                        if (!type.endsWith("Reference")) {
                            // not a ref, get a ref
                            type = ProjectPackagesDefinition.cleanType(type) + "Reference";
                        }
                        newAttr.setType(classMapping.apply(type));
                        properties.put(attrName, newAttr);
                        resultMap.put(newAttr, attr);
                    }
                    continue;
                }

                if (availableProperties.contains(attrName + "Size")) {
                    ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                    newAttr.setName(attrName + "Size");
                    newAttr.setType("int");
                    // only keep navigable attributes
                    properties.put(newAttr.getName(), newAttr);
                    resultMap.put(newAttr, attr);
                    continue;
                }

                // Simple type

                if (availableProperties.contains(attrName)) {
                    ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                    newAttr.setName(attrName);
                    newAttr.setType(type);
                    properties.put(attrName, newAttr);
                    resultMap.put(newAttr, attr);
                }

            }
        }
        if (availableProperties.contains("label") && properties.values().stream().noneMatch(p -> p.getName().equals("label"))) {

            ObjectModelAttributeImpl attr = new ObjectModelAttributeImpl();
            attr.setName("label");
            attr.setType("String");
            properties.put("label", attr);
        }
        Map<ObjectModelAttribute, ObjectModelAttribute> result = new LinkedHashMap<>();
        for (String availableProperty : availableProperties) {
            ObjectModelAttribute key = properties.get(availableProperty);
            ObjectModelAttribute value;
            if (key == null) {
                ObjectModelAttributeImpl ee = new ObjectModelAttributeImpl();
                ee.setName(availableProperty);
                ee.setType("String");
                key = ee;
                value = null;
            } else {
                value = resultMap.get(key);
            }
            Objects.requireNonNull(key, "Cant' find property " + availableProperty + " on " + input.getQualifiedName());
            result.put(key, value);
        }
        return result;
    }

    public static String getAttributeType(ObjectModel model, BeanTransformerContext context, String attrType) {
        if (!JavaGeneratorUtil.isPrimitiveType(attrType)) {
            boolean hasClass = model.hasClass(attrType);
            if (hasClass) {
                ObjectModelClass attributeClass = model.getClass(attrType);
                String attributeType = context.classesNameTranslation.get(attributeClass);
                if (attributeType != null) {
                    attrType = attributeClass.getPackageName() + "." + attributeType;
                }
            }
        }
        return attrType;
    }

    public DtoReferenceTransformer() {
        coreTagValues = new EugeneCoreTagValues();
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
        observeTagValues = new ToolkitTagValues();
    }

    private void generateClass(ObjectModelClass input, String className, String abstractClassName, String dtoName, String generatedDtoName, Set<ObjectModelAttribute> properties, boolean referential) {

        ObjectModelClass output = createClass(className, input.getPackageName());
        addAnnotation(output, output, GenerateJavaBeanDefinition.class);
        setSuperClass(output, abstractClassName);

        getLog().debug("will generate " + output.getQualifiedName());

    }

    private void generateGeneratedClass(ObjectModelClass input, String className, String abstractClassName, String dtoName, String generatedDtoName, String superClass, Set<ObjectModelAttribute> properties, Map<ObjectModelAttribute, ObjectModelAttribute> binderProperties, boolean referential, Set<String> extractInterfaces) {
        ObjectModelClass output = createAbstractClass(abstractClassName, input.getPackageName());
        ObjectModelPackage aPackage = model.getPackage(input);
        List<StatisticDefinitionModel> statisticDefinitions = statistics.get(input);


        ImportsManager importManager = getImportManager(output);
        importManager.addExcludedPattern(".+\\." + dtoName);
        importManager.addExcludedPattern(".+\\." + className);
        setSuperClass(output, superClass);
        boolean foundReferentialDtoReferenceWithNoCodeAware = false;
        for (String extractInterface : extractInterfaces) {
            addInterface(output, extractInterface);
            if (ReferentialDtoReferenceWithNoCodeAware.class.getName().equals(extractInterface)) {
                foundReferentialDtoReferenceWithNoCodeAware = true;
            }
        }
        if (referential && !foundReferentialDtoReferenceWithNoCodeAware) {
            addInterface(output, ReferentialDtoReferenceWithCodeAware.class.getName());
        }
        @SuppressWarnings("rawtypes") Class builderMethodName = referential ? ReferentialDtoReferenceDefinition.class : DataDtoReferenceDefinition.class;
        addImport(output, builderMethodName);
        addImport(output, Comparator.class);
        String comparator = observeTagValues.getComparatorTagValue(input);
        if (comparator == null) {
            comparator = TopiaEntity.PROPERTY_TOPIA_CREATE_DATE;
        }
        String comparatorGetter = getJavaBeanMethodName("get", comparator);
        StringBuilder definition = new StringBuilder("" /*{
            <%=builderMethodName.getSimpleName()%>
                    .builder(<%=dtoName%>.class, <%=className%>.class, Comparator.comparing(<%=className%>::<%=comparatorGetter%>)) }*/
        );
        for (ObjectModelAttribute attr : properties) {
            String type = importManager.importAndSimplify(attr.getType());
            if (type.endsWith("Stub")) {
                continue;
            }
            String name = attr.getName();
            String getterName;
            boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attr);

            definition.append("" /*{
                    .addProperty(<%=type%>.class, "<%=name%>")}*/
            );
        }
        definition.append(""/*{
                    .build()}*/
        );
        String definitionType = String.format("%s<%s, %s>", builderMethodName.getName(), dtoName, className);
        addAttribute(output, "DEFINITION", definitionType, definition.toString(), ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC, ObjectModelJavaModifier.FINAL);

        getLog().debug("will generate " + output.getQualifiedName());
        String importReferentialLocale = importAndSimplify(output, ReferentialLocale.class.getName());

        ObjectModelOperation factoryMethod = addOperation(output, "of", className, ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.STATIC);
        addParameter(factoryMethod, importReferentialLocale, "referentialLocale");
        addParameter(factoryMethod, dtoName, "dto");
        setOperationBody(factoryMethod, ""/*{
        <%=className%> reference = new <%=className%>();
        reference.fromDto(referentialLocale, dto);
        return reference;
    }*/);

        ObjectModelOperation getDefinition = addOperation(output, "getDefinition", definitionType, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDefinition, Override.class);
        setOperationBody(getDefinition, ""/*{
        return DEFINITION;
    }*/
        );
        ObjectModelOperation getDtoType = addOperation(output, "getDtoType", "Class<" + dtoName + ">", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getDtoType, Override.class);
        setOperationBody(getDtoType, ""/*{
        return <%=dtoName%>.class;
    }*/
        );
        ObjectModelOperation getReferenceType = addOperation(output, "getReferenceType", "Class<" + className + ">", ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, getReferenceType, Override.class);
        setOperationBody(getReferenceType, ""/*{
        return <%=className%>.class;
    }*/
        );
        addImport(output, ToolkitIdDtoBean.class);
        ObjectModelOperation toShortDto = addOperation(output, "toShortDto", ToolkitIdDtoBean.class.getSimpleName(), ObjectModelJavaModifier.PUBLIC);
        addAnnotation(output, toShortDto, Override.class);
        setOperationBody(toShortDto, ""/*{
        return ToolkitIdDtoBean.of(<%=dtoName%>.class, getId(), getLastUpdateDate());
    }*/
        );
        TemplateContract.addToLabel(this, output, dtoName);

        ObjectModelOperation fromDto = addOperation(output, "fromDto", "void", ObjectModelJavaModifier.PUBLIC);
//        addAnnotation(output, fromDto, Override.class);
        addParameter(fromDto, importReferentialLocale, "referentialLocale");
        addParameter(fromDto, dtoName, "dto");
        StringBuilder body2;
        if (referential) {
            body2 = new StringBuilder(""/*{
        fromReferentialDtoReferenceAware(dto);}*/);
        } else {
            body2 = new StringBuilder(""/*{
        fromDtoReferenceAware(dto);}*/);
        }
        Collection<String> allOtherAttributes = input.getAllOtherAttributes().stream().map(ObjectModelAttribute::getName).collect(Collectors.toList());
        if (referential) {
            allOtherAttributes.remove("code");
        } else {
            if (input.getName().equals("Program")) {
                allOtherAttributes.clear();
            }
        }
        allOtherAttributes.remove("proportion");
        allOtherAttributes.remove("startDate");
        allOtherAttributes.remove("endDate");
        allOtherAttributes.remove("species");

        for (Map.Entry<ObjectModelAttribute, ObjectModelAttribute> entry : binderProperties.entrySet()) {
            ObjectModelAttribute attr = entry.getKey();
            String name = attr.getName();
            if (allOtherAttributes.contains(name)) {
                continue;
            }
            createProperty(output, attr);
            ObjectModelAttribute dtoAttr = entry.getValue();
            String assignment = getReferencePropertyAssignment(output, dtoName, dtoAttr, attr);
            String getterName = "set" + JavaGeneratorUtil.capitalizeJavaBeanPropertyName(name);
            body2.append(""/*{
        <%=getterName%>(<%=assignment%>);}*/
            );
        }
        if (statisticDefinitions != null) {
            body2.append(""/*{
        dto.copyTo(this);}*/
            );
        }
        body2.append(""/*{
    }*/);
        setOperationBody(fromDto, body2.toString());
        StatisticsHelper.addWithStatistics(this, input.getPackageName(), input, output, null, statisticDefinitions);
    }

    private String getReferencePropertyAssignment(ObjectModelClass output, String dtoName, ObjectModelAttribute dtoAttr, ObjectModelAttribute attr) {
        String content;
        if (dtoAttr != null) {
            String getterName;
            String dtoAttrName = dtoAttr.getName();
            String type = getAttributeType(dtoAttr);
            boolean isSizeMethod = attr.getName().equals(dtoAttrName + "Size");
            boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(dtoAttr);

            if (booleanProperty) {
                getterName = JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
            } else {
                getterName = JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
            }
            getterName += JavaGeneratorUtil.capitalizeJavaBeanPropertyName(dtoAttrName);
            if (isSizeMethod) {
                getterName += "Size";
            }
            content = "dto." + getterName + "()";

            if (isSizeMethod) {
                return content;
            }
            if (context.selectedClassesFqn.contains(ProjectPackagesDefinition.cleanType(dtoAttr.getType()))) {

                // Dto

                if (!type.endsWith("Reference")) {

                    // not a ref, get a ref
                    type = ProjectPackagesDefinition.cleanType(type) + "Reference";
                }

                if (attr.getName().equals(dtoAttrName + "Id")) {
                    addImport(output, Optional.class);
                    addImport(output, BusinessDto.class);
                    type = importAndSimplify(output, type);
                    content = String.format("Optional.ofNullable(%s).map(%s::getId).orElse(null)", content, type);
                } else if (attr.getName().equals(dtoAttrName + "Label")) {
                    addImport(output, Optional.class);
                    addImport(output, BusinessDto.class);
                    String method = "getLabel";
                    if (type.equals("SpeciesReference")) {
                        method = "getScientificLabel";
                    }
                    type = importAndSimplify(output, type);
                    content = String.format("Optional.ofNullable(%s).map(%s::%s).orElse(null)", content, type, method);
                } else if (attr.getName().equals(dtoAttrName + "Code")) {
                    addImport(output, Optional.class);
                    addImport(output, BusinessDto.class);
                    type = importAndSimplify(output, type);
                    content = String.format("Optional.ofNullable(%s).map(%s::getCode).orElse(null)", content, type);
                }
            }
        } else {

            if (attr.getName().equals("label")) {
                content = "dto.getLabel(referentialLocale)";
            } else {
                throw new IllegalStateException(String.format("Can't come here (class: %s, property: %s)", output.getQualifiedName(), attr));
            }
        }
        return content;
    }

    private String getAttributeType(ObjectModelAttribute attr) {
        String attrType = attr.getType();
        return getAttributeType(attrType);
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);

        context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, false, false, input -> {

            ObjectModelPackage aPackage = model.getPackage(input.getPackageName());

            boolean referential = TemplateContract.isReferentialFromPackageName(aPackage.getName());

            String referencesTagValue = observeTagValues.getReferencesTagValue(input);
            return referencesTagValue != null || referential;
        }, getLog());

        context.report();
        statistics = StatisticsHelper.toModel(model, ProjectPackagesDefinition.of(getClassLoader()));

        Map<String, String> dtoToReferenceMapping = new TreeMap<>();
        Map<String, String> referenceToDtoMapping = new TreeMap<>();
        for (ObjectModelClass input : context.selectedClasses) {

            ObjectModelPackage aPackage = getPackage(input);

            String packageName = aPackage.getName();

            boolean referential = TemplateContract.isReferentialFromPackageName(packageName);
            String referencesTagValue = observeTagValues.getReferencesTagValue(input);
            if (referencesTagValue == null && referential) {
                referencesTagValue = "code,label,uri";
            }

            Set<String> availableProperties = new LinkedHashSet<>(Arrays.asList(Objects.requireNonNull(referencesTagValue).split(",")));

            Map<ObjectModelAttribute, ObjectModelAttribute> binderProperties = getReferenceProperties(context.selectedClassesFqn, input, availableProperties, this::getAttributeType);

            Set<ObjectModelAttribute> properties = getProperties(model, context, input, availableProperties);

            String prefix = getConstantPrefix(input);
            setConstantPrefix(prefix);

            String dtoName = context.classesNameTranslation.get(input);
            String generatedDtoName = "Generated" + dtoName;
//            i18nGetterFile.addKey("observe." + TemplateContract.cleanDtoType(referential, packageName + "." + dtoName) + ".type");
            String className = ProjectPackagesDefinition.cleanType(dtoName) + "Reference";
            String generatedClassName = "Generated" + className;

            dtoToReferenceMapping.put(packageName + "." + dtoName, packageName + "." + className + ".DEFINITION");
            referenceToDtoMapping.put(packageName + "." + className, packageName + "." + dtoName + ".class");

            Class<?> superClass = referential ? ReferentialDtoReference.class : DataDtoReference.class;

            Set<String> extractInterfaces = getInterfaces(input,
                                                          model.getInterface(DtoAndReferenceAware.class.getName()),
                                                          model.getInterface("fr.ird.observe.dto.data.ll.common.TripCommon"),
                                                          model.getInterface("fr.ird.observe.dto.data.ps.common.TripCommon"),
                                                          model.getInterface(ReferentialDtoReferenceWithNoCodeAware.class.getName()));

            generateGeneratedClass(input, className, generatedClassName, dtoName, generatedDtoName, superClass.getName(), properties, binderProperties, referential, extractInterfaces);

            boolean generateClass = notFoundInClassPath(input.getPackageName(), className);
            if (generateClass) {
                generateClass(input, className, generatedClassName, dtoName, generatedDtoName, properties, referential);
            }
        }

        generateClassMapping(true, DtoToReferenceDtoMapping.class, "Class<? extends " + BusinessDto.class.getName() + ">", DtoReferenceDefinition.class.getName() + "<?,?>", ImmutableMap.class, "build", dtoToReferenceMapping);
        generateClassMapping(true, ReferenceDtoToDtoClassMapping.class, DtoReference.class.getName(), BusinessDto.class.getName(), ImmutableClassMapping.class, "getMappingBuilder", referenceToDtoMapping);

    }

    private String getAttributeType(String attrType) {
        return getAttributeType(model, context, attrType);
    }

    private boolean notFoundInClassPath(String input, String className) {
        String fqn = input + "." + className;
        boolean inClassPath = getResourcesHelper().isJavaFileInClassPath(fqn);
        return !inClassPath;
    }

    private void createProperty(ObjectModelClass output, ObjectModelAttribute attr) {

        String attrName = attr.getName();
        String attrType = getAttributeTypeWithGeneric(attr);

        boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attr);
        if (booleanProperty) {
            createGetMethod(output, attrName, attrType, JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX);
        } else {
            createGetMethod(output, attrName, attrType, JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX);
        }
        createSetMethod(output, attrName, attrType);
        addAttribute(output, attrName, attrType, "", ObjectModelJavaModifier.PRIVATE);

    }

    private Set<ObjectModelAttribute> getProperties(ObjectModel model, BeanTransformerContext context, ObjectModelClass input, Set<String> availableProperties) {
        Collection<ObjectModelAttribute> attributes = new LinkedList<>(input.getAttributes());
        attributes.addAll(input.getAllOtherAttributes());
        Map<String, ObjectModelAttribute> properties = new TreeMap<>();
        for (ObjectModelAttribute attr : attributes) {

            if (attr.isNavigable()) {

                String attrName = attr.getName();
                String type = attr.getType();

                if (context.selectedClassesFqn.contains(ProjectPackagesDefinition.cleanType(type))) {

                    // Dto

                    if (availableProperties.contains(attrName + "Label")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Label");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                    }

                    if (availableProperties.contains(attrName + "Id")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Id");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                    }
                    if (availableProperties.contains(attrName + "Code")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Code");
                        newAttr.setType("String");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                    }
                    if (availableProperties.contains(attrName + "Size")) {
                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                        newAttr.setName(attrName + "Size");
                        newAttr.setType("int");
                        // only keep navigable attributes
                        properties.put(newAttr.getName(), newAttr);
                        continue;
                    }

                    // Get a ref

                    if (availableProperties.contains(attrName)) {

                        ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();

                        newAttr.setName(attrName);

                        if (!type.endsWith("Reference")) {

                            // not a ref, get a ref
                            type = ProjectPackagesDefinition.cleanType(type) + "Reference";
                        }

                        newAttr.setType(getAttributeType(model, context, type));

                        properties.put(attrName, newAttr);
                    }

                    continue;
                }

                if (availableProperties.contains(attrName + "Size")) {
                    ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                    newAttr.setName(attrName + "Size");
                    newAttr.setType("int");
                    // only keep navigable attributes
                    properties.put(newAttr.getName(), newAttr);
                    continue;
                }

                // Simple type

                if (availableProperties.contains(attrName)) {
                    ObjectModelAttributeImpl newAttr = new ObjectModelAttributeImpl();
                    newAttr.setName(attrName);
                    newAttr.setType(type);
                    properties.put(attrName, newAttr);
                }

            }
        }
        if (availableProperties.contains("label") && properties.values().stream().noneMatch(p -> p.getName().equals("label"))) {

            ObjectModelAttributeImpl attr = new ObjectModelAttributeImpl();
            attr.setName("label");
            attr.setType("String");
            properties.put("label", attr);
        }
        Set<ObjectModelAttribute> result = new LinkedHashSet<>();
        for (String availableProperty : availableProperties) {
            ObjectModelAttribute e = properties.get(availableProperty);
            if (e == null) {
                ObjectModelAttributeImpl ee = new ObjectModelAttributeImpl();
                ee.setName(availableProperty);
                ee.setType("String");
                e = ee;
            }
            Objects.requireNonNull(e, "Cant' find property " + availableProperty + " on " + input.getQualifiedName());
            result.add(e);
        }
        return result;
    }

    private void createGetMethod(ObjectModelClass output, String attrName, String attrType, String methodPrefix) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%>;
    }*/
        );
    }

    private void createSetMethod(ObjectModelClass output, String attrName, String attrType) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("set", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        setOperationBody(operation, ""
    /*{
        this.<%=attrName%> = <%=attrName%>;
    }*/
        );
    }

    private String getAttributeTypeWithGeneric(ObjectModelAttribute attr) {
        String attrType = getAttributeType(attr);
        String generic = eugeneTagValues.getAttributeGenericTagValue(attr);
        if (generic != null) {
            attrType += "<" + getAttributeType(generic) + ">";
        }
        return attrType;
    }
}
