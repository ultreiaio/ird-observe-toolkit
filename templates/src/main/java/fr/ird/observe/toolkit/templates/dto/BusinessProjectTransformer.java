package fr.ird.observe.toolkit.templates.dto;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.NoValidationDto;
import fr.ird.observe.dto.data.NotEntityDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.mapping.DataGroupByDtoToDefinitionMapping;
import fr.ird.observe.spi.mapping.DtoToFormDtoMapping;
import fr.ird.observe.spi.mapping.DtoToMainDtoClassMapping;
import fr.ird.observe.spi.mapping.DtoToReferenceDtoMapping;
import fr.ird.observe.spi.mapping.ReferenceDtoToDtoClassMapping;
import fr.ird.observe.spi.module.BusinessDataPackage;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessPackage;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessProjectI18nHelper;
import fr.ird.observe.spi.module.BusinessProjectMapping;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.java.BeanTransformerTagValues;
import org.nuiton.eugene.java.EugeneJavaTagValues;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings({"StringOperationCanBeSimplified", "unused"})
public class BusinessProjectTransformer extends ObjectModelTransformerToJava implements TemplateContract {

    private final EugeneCoreTagValues coreTagValues;
    private final EugeneJavaTagValues javaTemplatesTagValues;
    private final BeanTransformerTagValues beanTagValues;
    private final ToolkitTagValues observeTagValues;
    private ProjectPackagesDefinition def;
    private BeanTransformerContext context;
    private String generatedPackageName;
    private String projectClassName;

    public BusinessProjectTransformer() {
        coreTagValues = new EugeneCoreTagValues();
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
        observeTagValues = new ToolkitTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);

        LinkedHashMultimap<String, String> subModuleByModule = LinkedHashMultimap.create();
        LinkedHashMultimap<Pair<String, String>, String> packageBySubModule = LinkedHashMultimap.create();
        Multimap<String, ObjectModelClass> dtoByPackage = LinkedHashMultimap.create();
        Map<String, Integer> priorityByPackage = new LinkedHashMap<>();
        def = ProjectPackagesDefinition.of(getClassLoader());
        String dtoDataPackage = def.getDtoDataPackage();
        String dtoReferentialPackage = def.getDtoReferentialPackage();

        context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, false, false, input -> {

            ObjectModelPackage aPackage = model.getPackage(input.getPackageName());
            String packageName = aPackage.getName();
            String packageSuffix;
            if (packageName.startsWith(dtoReferentialPackage)) {
                packageSuffix = packageName.substring(dtoReferentialPackage.length() + 1);
            } else if (packageName.startsWith(dtoDataPackage)) {
                packageSuffix = packageName.substring(dtoDataPackage.length() + 1);
            } else {
                return false;
            }
            String[] parts = packageSuffix.split("\\.");
            String moduleName = parts[0];
            String subModuleName = parts.length == 1 ? moduleName : parts[1];
            boolean skip = input.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(NotEntityDto.class.getName()))
                    || input.getInterfaces().stream().anyMatch(i -> i.getQualifiedName().equals(NoValidationDto.class.getName()));
            if (skip) {
                return false;
            }
            dtoByPackage.put(packageName, input);
            return true;
        }, getLog());

        for (ObjectModelPackage aPackage : model.getPackages()) {
            String priority = observeTagValues.getPackagePriorityTagValue(aPackage);
            if (priority != null) {
                priorityByPackage.put(aPackage.getName(), Integer.valueOf(priority));
                String packageName = aPackage.getName();
                String packageSuffix;
                if (packageName.startsWith(dtoReferentialPackage)) {
                    packageSuffix = packageName.substring(dtoReferentialPackage.length() + 1);
                } else {
                    packageSuffix = packageName.substring(dtoDataPackage.length() + 1);
                }
                String[] parts = packageSuffix.split("\\.");
                String moduleName = parts[0];
                String subModuleName = parts.length == 1 ? moduleName : parts[1];

                subModuleByModule.put(moduleName, subModuleName);
                Pair<String, String> pair = Pair.of(moduleName, subModuleName);
                packageBySubModule.put(pair, packageName);
            }
        }

        context.report();

        generatedPackageName = BusinessProject.class.getPackage().getName();

        // all packages ordered by their priority
        List<String> allPackages = priorityByPackage.keySet().stream().sorted(Comparator.comparing(priorityByPackage::get, Integer::compareTo)).collect(Collectors.toList());
        // all module/sub-module ordered by their packages priority
        List<Pair<String, String>> allModuleSubModules = computeModuleSubModuleOrder(packageBySubModule, allPackages);
        // all module ordered by their packages priority
        List<String> allModules = allModuleSubModules.stream().map(Pair::getKey).distinct().collect(Collectors.toList());

        generate(allModules, allModuleSubModules, allPackages, packageBySubModule, dtoByPackage.asMap(), priorityByPackage);
    }

    private void generate(List<String> allModules, List<Pair<String, String>> allModuleSubModules, List<String> allPackages, LinkedHashMultimap<Pair<String, String>, String> packageBySubModule, Map<String, Collection<ObjectModelClass>> dtoByPackage, Map<String, Integer> priorityByPackage) {
        if (isVerbose()) {
            getLog().debug(String.format("Found %d module(s) (%s).", allModules.size(), allModules));
            getLog().debug(String.format("Found %d package(s) (%s).", allPackages.size(), allPackages));
        }
        I18nKeySet i18nGetterFile = getI18nGetterFile();
        BusinessProjectI18nHelper.setI18nKeyPrefix(model.getName().toLowerCase().trim() + ".");
        List<String> moduleClassesNames = new LinkedList<>();

        projectClassName = String.format("%s%s", model.getName(), BusinessProject.class.getSimpleName());

        for (String moduleName : allModules) {

            List<String> subModules = allModuleSubModules.stream().filter(p -> p.getKey().equals(moduleName)).map(Pair::getValue).collect(Collectors.toList());
            if (isVerbose()) {
                getLog().debug(String.format("[module %s] Found %d sub-module(s) (%s).", moduleName, subModules.size(), subModules));
            }
            List<String> subModuleClassNames = new LinkedList<>();

            for (String subModule : subModules) {

                Pair<String, String> pair = Pair.of(moduleName, subModule);
                Set<String> packagesSet = packageBySubModule.get(pair);
                String logPrefix = moduleName + " → " + subModule;
                List<String> packages = allPackages.stream().filter(packagesSet::contains).sorted().collect(Collectors.toList());
                if (isVerbose()) {
                    getLog().debug(String.format("[module %s] Found %d package(s) (%s).", logPrefix, packages.size(), packages));
                }
                String dataPackage = null;
                String referentialPackage = null;
                for (String packageName : packages) {
                    Integer priority = priorityByPackage.get(packageName);
                    List<ObjectModelClass> classes = dtoByPackage.getOrDefault(packageName, Collections.emptyList()).stream().sorted(Comparator.comparing(ObjectModelClass::getQualifiedName, String::compareTo)).collect(Collectors.toList());
                    if (classes.isEmpty()) {
                        // do not generate package if no classes found
                        continue;
                    }
                    String className = generateBusinessPackage(i18nGetterFile, moduleName, subModule, packageName, logPrefix, priority, classes);
                    if (isVerbose()) {
                        getLog().info("Generated: " + className);
                    }
                    boolean referential = def.isReferentialFromPackageName(packageName);
                    if (referential) {
                        referentialPackage = className;
                    } else {
                        dataPackage = className;
                    }
                }
                String subModuleClassName = generateBusinessSubModule(i18nGetterFile, moduleName, subModule, referentialPackage, dataPackage);
                if (isVerbose()) {
                    getLog().info("Generated: " + subModuleClassName);
                }
                subModuleClassNames.add(subModuleClassName);
            }
            String moduleClasseName = generateBusinessModule(i18nGetterFile, moduleName, subModuleClassNames);
            if (isVerbose()) {
                getLog().info("Generated: " + moduleClasseName);
            }
            moduleClassesNames.add(moduleClasseName);
        }
        String projectMappingClassName = generateBusinessProjectMapping(model.getName());
        if (isVerbose()) {
            getLog().info("Generated: " + projectMappingClassName);
        }
        String projectClassName = generateBusinessProject(model.getName(), moduleClassesNames);
        if (isVerbose() && projectClassName != null) {
            getLog().info("Generated: " + projectClassName);
        }
    }

    private String generateBusinessProject(String name, List<String> moduleClasseNames) {

        String generatedBusinessProject = generateGeneratedBusinessProject(name, moduleClasseNames);
        if (isVerbose()) {
            getLog().info("Generated: " + generatedBusinessProject);
        }

        String className = String.format("%s%s", model.getName(), BusinessProject.class.getSimpleName());
        boolean skip = getResourcesHelper().isJavaFileInClassPath(generatedPackageName + "." + className);
        if (skip) {
            return null;
        }
        ObjectModelClass aClass = createClass(className, generatedPackageName);
        ObjectModelAnnotation annotation = addAnnotation(aClass, aClass, AutoService.class);
        ImportsManager importManager = getImportManager(aClass);
        addAnnotationClassParameter(importManager, aClass, annotation, "value", BusinessProject.class);

        setSuperClass(aClass, generatedBusinessProject);
        addImport(aClass, List.class);
        addImport(aClass, BusinessModule.class);
        ObjectModelOperation constructor = addConstructor(aClass, ObjectModelJavaModifier.PUBLIC);
        return aClass.getQualifiedName();
    }

    private String generateGeneratedBusinessProject(String name, List<String> moduleClasseNames) {
        String concreteClassName = String.format("%s%s", model.getName(), BusinessProject.class.getSimpleName());
        String className = String.format("Generated%s%s", model.getName(), BusinessProject.class.getSimpleName());
        String mappingClassName = String.format("%s%s", model.getName(), BusinessProjectMapping.class.getSimpleName());
        ObjectModelClass aClass = createClass(className, generatedPackageName);
        setSuperClass(aClass, BusinessProject.class);
        addImport(aClass, List.class);
        addImport(aClass, BusinessModule.class);
        ObjectModelOperation constructor = addConstructor(aClass, ObjectModelJavaModifier.PUBLIC);
        addStaticFactory(aClass, concreteClassName);
        StringBuilder content = new StringBuilder(""/*{
        super("<%=name%>", List.of(}*/);
        Iterator<String> iterator = moduleClasseNames.iterator();
        while (iterator.hasNext()) {
            String qualifiedName = iterator.next();
            content.append(""/*{
                <%=qualifiedName%>.get()}*/);
            if (iterator.hasNext()) {
                content.append(",");
            }
        }
        content.append(""/*{
                ),
              <%=mappingClassName%>.get());
    }*/);
        setOperationBody(constructor, content.toString());
        int index = 0;
        for (String qualifiedName : moduleClasseNames) {
            String[] split = qualifiedName.split("\\.");
            String simpleName = Strings.capitalize(split[split.length - 2]) + GeneratorUtil.getSimpleName(qualifiedName);
            ObjectModelOperation getter = addOperation(aClass, getJavaBeanMethodName("get", simpleName), qualifiedName, ObjectModelJavaModifier.PUBLIC);
            setOperationBody(getter, ""/*{
        return (<%=qualifiedName%>) getModules().get(<%=index++%>);
    }*/);
        }
        return aClass.getQualifiedName();
    }

    private void addGetProjectSingletonMethod(ObjectModelClass aClass) {
        ObjectModelOperation getProjectSupplier = addOperation(aClass, "getProjectSupplier", Supplier.class.getName() + "<" + projectClassName + ">", ObjectModelJavaModifier.PROTECTED);
        addAnnotation(aClass, getProjectSupplier, Override.class);
        setOperationBody(getProjectSupplier, ""/*{
        return <%=projectClassName%>::get;
    }*/);

    }

    private String generateBusinessProjectMapping(String name) {
        String className = name + BusinessProjectMapping.class.getSimpleName();
        ObjectModelClass aClass = createClass(className, BusinessProjectMapping.class.getPackage().getName());
        setSuperClass(aClass, BusinessProjectMapping.class);
        ObjectModelOperation constructor = addConstructor(aClass, ObjectModelJavaModifier.PUBLIC);
        addStaticFactory(aClass, null);

        String dtoToReferenceMapping = DtoToReferenceDtoMapping.class.getPackage().getName() + "." + name + DtoToReferenceDtoMapping.class.getSimpleName();
        String referenceToDtoMapping = ReferenceDtoToDtoClassMapping.class.getPackage().getName() + "." + name + ReferenceDtoToDtoClassMapping.class.getSimpleName();
        String dtoToFormMapping = DtoToFormDtoMapping.class.getPackage().getName() + "." + name + DtoToFormDtoMapping.class.getSimpleName();
        String dataGroupByDtoToDefinitionMapping = DataGroupByDtoToDefinitionMapping.class.getPackage().getName() + "." + name + DataGroupByDtoToDefinitionMapping.class.getSimpleName();
        String dtoToMainMapping = DtoToMainDtoClassMapping.class.getPackage().getName() + "." + name + DtoToMainDtoClassMapping.class.getSimpleName();

        setOperationBody(constructor, ""/*{
        super(<%=dtoToReferenceMapping%>.get(),
              <%=referenceToDtoMapping%>.get(),
              <%=dtoToFormMapping%>.get(),
              <%=dataGroupByDtoToDefinitionMapping%>.get(),
              <%=dtoToMainMapping%>.get());
    }*/);
        return aClass.getQualifiedName();
    }

    private String generateBusinessModule(I18nKeySet i18nGetterFile, String moduleName, List<String> subModuleClassNames) {
        String moduleKey = BusinessProjectI18nHelper.getModuleKey(moduleName);
        i18nGetterFile.addKey(moduleKey);
        String moduleShortKey = BusinessProjectI18nHelper.getModuleShortKey(moduleName);
        i18nGetterFile.addKey(moduleShortKey);
        Class<?> className = BusinessModule.class;
        String packageName = generatedPackageName + "." + moduleName;
        String concreteClassName = packageName + "." + className.getSimpleName();
        ObjectModelClass generatedClass = createAbstractClass("Generated" + className.getSimpleName(), packageName);

        getImportManager(generatedClass).addExcludedPattern(concreteClassName);
        setSuperClass(generatedClass, className);
        addImport(generatedClass, List.class);
        addImport(generatedClass, BusinessSubModule.class);
        addImport(generatedClass, I18n.class);
        ObjectModelOperation constructor = addConstructor(generatedClass, ObjectModelJavaModifier.PUBLIC);
        addStaticFactory(generatedClass, concreteClassName);
        StringBuilder content = new StringBuilder(""/*{
        super("<%=moduleName%>", List.of(}*/);
        Iterator<String> iterator = subModuleClassNames.iterator();
        while (iterator.hasNext()) {
            String qualifiedName = iterator.next();
            content.append(""/*{
                <%=qualifiedName%>.get()}*/);
            if (iterator.hasNext()) {
                content.append(",");
            }
        }
        content.append(""/*{
                ));
    }*/);
        setOperationBody(constructor, content.toString());
        ObjectModelOperation getLabelKey = addOperation(generatedClass, "getLabelKey", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(generatedClass, getLabelKey, Override.class);
        setOperationBody(getLabelKey, ""/*{
        return I18n.n("<%=moduleKey%>");
    }*/);

        ObjectModelOperation getShortLabelKey = addOperation(generatedClass, "getShortLabelKey", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(generatedClass, getShortLabelKey, Override.class);
        setOperationBody(getShortLabelKey, ""/*{
        return I18n.n("<%=moduleShortKey%>");
    }*/);

        int index = 0;
        for (String qualifiedName : subModuleClassNames) {
            String[] split = qualifiedName.split("\\.");
            String simpleName = Strings.capitalize(split[split.length - 2]) + GeneratorUtil.getSimpleName(qualifiedName);
            ObjectModelOperation getter = addOperation(generatedClass, getJavaBeanMethodName("get", simpleName), qualifiedName, ObjectModelJavaModifier.PUBLIC);
            setOperationBody(getter, ""/*{
        return (<%=qualifiedName%>) getSubModules().get(<%=index++%>);
    }*/);
        }
        addGetProjectSupplierMethod(generatedClass);

        if (!getResourcesHelper().isJavaFileInClassPath(concreteClassName)) {
            ObjectModelClass concreteClass = createClass(className.getSimpleName(), packageName);
            setSuperClass(concreteClass, generatedClass.getQualifiedName());
        }
        return concreteClassName;
    }

    private void addGetProjectSupplierMethod(ObjectModelClass generatedClass) {
        String projectFqn = generatedPackageName + "." + projectClassName;
        ObjectModelOperation getProjectSupplier = addOperation(generatedClass, "getProjectSupplier", String.format("%s<%s>", Supplier.class.getName(), projectFqn), ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.FINAL);
        addAnnotation(generatedClass, getProjectSupplier, Override.class);
        setOperationBody(getProjectSupplier, ""/*{
        return <%=projectFqn%>::get;
    }*/);
    }

    private String generateBusinessSubModule(I18nKeySet i18nGetterFile, String moduleName, String subModuleName, String referentialPackage, String dataPackage) {
        String subModuleKey = BusinessProjectI18nHelper.getSubModule(moduleName, subModuleName);
        i18nGetterFile.addKey(subModuleKey);
        Class<?> className = BusinessSubModule.class;
        ObjectModelClass aClass = createClass(className.getSimpleName(), generatedPackageName + "." + moduleName + "." + subModuleName);
        getImportManager(aClass).addExcludedPattern(className.getName());
        setSuperClass(aClass, className);
        addImport(aClass, BusinessPackage.class);
        addImport(aClass, BusinessDto.class);
        addImport(aClass, I18n.class);
        ObjectModelOperation constructor = addConstructor(aClass, ObjectModelJavaModifier.PUBLIC);
        addStaticFactory(aClass, null);
        String dataImpl = dataPackage == null ? "null" : dataPackage + ".get()";
        String referentialImpl = referentialPackage == null ? "null" : referentialPackage + ".get()";
        setOperationBody(constructor, ""/*{
        super("<%=subModuleName%>", <%=referentialImpl%>, <%=dataImpl%>);
    }*/);
        ObjectModelOperation getLabelKey = addOperation(aClass, "getLabelKey", String.class, ObjectModelJavaModifier.PUBLIC);
        addAnnotation(aClass, getLabelKey, Override.class);
        setOperationBody(getLabelKey, ""/*{
        return I18n.n("<%=subModuleKey%>");
    }*/);

        return aClass.getQualifiedName();
    }

    private String generateBusinessPackage(I18nKeySet i18nGetterFile, String moduleName, String subModuleName, String packageName, String logPrefix, Integer priority, List<ObjectModelClass> classes) {
        boolean referential = def.isReferentialFromPackageName(packageName);
        Class<?> className = referential ? BusinessReferentialPackage.class : BusinessDataPackage.class;
        Class<?> typeName = referential ? ReferentialDto.class : DataDto.class;
        if (isVerbose()) {
            getLog().debug(String.format("[module %s → %s] Found %d class(es) (%s).", logPrefix, packageName, classes.size(), classes.stream().map(ObjectModelClass::getQualifiedName).sorted().collect(Collectors.toList())));
        }
        ObjectModelClass aClass = createClass(className.getSimpleName(), generatedPackageName + "." + moduleName + "." + subModuleName);
        getImportManager(aClass).addExcludedPattern(className.getName());
        setSuperClass(aClass, className);
        addImport(aClass, List.class);
        addImport(aClass, LinkedHashSet.class);
        addImport(aClass, typeName);
        ObjectModelOperation constructor = addConstructor(aClass, ObjectModelJavaModifier.PUBLIC);
        addStaticFactory(aClass, null);
        StringBuilder content = new StringBuilder(""/*{
        super(<%=priority%>, "<%=packageName%>", new LinkedHashSet<>(List.of(}*/);
        Iterator<ObjectModelClass> iterator = classes.iterator();
        while (iterator.hasNext()) {
            ObjectModelClass objectModelClass = iterator.next();
            String qualifiedName = context.classesNameTranslation.get(objectModelClass);
            content.append(""/*{
                <%=packageName%>.<%=qualifiedName%>.class}*/);
            if (iterator.hasNext()) {
                content.append(",");
            }
        }
        content.append(""/*{
                )));
    }*/);
        setOperationBody(constructor, content.toString());
        return aClass.getQualifiedName();
    }

    private List<Pair<String, String>> computeModuleSubModuleOrder(Multimap<Pair<String, String>, String> packageBySubModule, List<String> allPackages) {
        Map<Pair<String, String>, Integer> priorityByModuleSubModule = new TreeMap<>();
        for (Map.Entry<Pair<String, String>, Collection<String>> entry : packageBySubModule.asMap().entrySet()) {
            Pair<String, String> pair = entry.getKey();
            int pairOrder = entry.getValue().stream().mapToInt(allPackages::indexOf).min().orElse(Integer.MAX_VALUE);
            priorityByModuleSubModule.put(pair, pairOrder);
        }
        return packageBySubModule.keySet().stream().sorted(Comparator.comparing(priorityByModuleSubModule::get, Integer::compareTo)).collect(Collectors.toList());
    }

}

