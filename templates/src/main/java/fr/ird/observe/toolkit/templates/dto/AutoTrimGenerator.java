package fr.ird.observe.toolkit.templates.dto;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.validation.ValidationTagValues;
import io.ultreia.java4all.lang.Strings;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * To generate autoTrim method on a class.
 * <p>
 * Created on 14/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.25
 */
public class AutoTrimGenerator {

    private final ValidationTagValues validationTagValues;
    private final TemplateContract template;
    private final ObjectModel model;

    private final BiPredicate<ObjectModelClass, ObjectModelAttribute> compositeAttributePredicate;

    public AutoTrimGenerator(ValidationTagValues validationTagValues,
                             TemplateContract template,
                             ObjectModel model,
                             BiPredicate<ObjectModelClass, ObjectModelAttribute> compositeAttributePredicate) {
        this.validationTagValues = validationTagValues;
        this.template = template;
        this.model = model;
        this.compositeAttributePredicate = compositeAttributePredicate;
    }

    public void generate(ObjectModelClass input, ObjectModelClass output) {
        Collection<ObjectModelAttribute> attributes = new LinkedList<>(input.getAttributes());

        List<String> stringPropertyNames = TemplateContract.getProperties(input, attributes, (c, a) -> "String".equals(GeneratorUtil.getSimpleName(a.getType())) && validationTagValues.isNotBlank(model.getTagValuesStore(), c, a));
        List<String> compositionPropertyNames = TemplateContract.getProperties(input, attributes, (c, a) -> !GeneratorUtil.isNMultiplicity(a) && compositeAttributePredicate.test(c, a));
        List<String> multipleCompositionPropertyNames = TemplateContract.getProperties(input, attributes, (c, a) -> GeneratorUtil.isNMultiplicity(a) && compositeAttributePredicate.test(c, a));
        if (stringPropertyNames.isEmpty()
                && compositionPropertyNames.isEmpty()
                && multipleCompositionPropertyNames.isEmpty()) {
            return;
        }

        ObjectModelOperation autoTrimMethod = template.addOperation(output, "autoTrim", void.class.getName(), ObjectModelJavaModifier.PUBLIC);
        template.addAnnotation(output, autoTrimMethod, Override.class);
        StringBuilder content = new StringBuilder(""/*{
        super.autoTrim();}*/);
        for (String property : stringPropertyNames) {
            String capitalize = Strings.capitalize(property);
            content.append(""/*{
        autoTrim(this::get<%=capitalize%>, this::set<%=capitalize%>);}*/);
        }
        for (String property : compositionPropertyNames) {
            String capitalize = Strings.capitalize(property);
            content.append(""/*{
        if (get<%=capitalize%>() != null) {
            get<%=capitalize%>().autoTrim();
        }}*/);
        }
        for (String property : multipleCompositionPropertyNames) {
            String capitalize = Strings.capitalize(property);
            content.append(""/*{
        if (isNot<%=capitalize%>Empty()) {
            get<%=capitalize%>().forEach(c -> c.autoTrim());
        }}*/);
        }
        template.setOperationBody(autoTrimMethod, ""/*{<%=content.toString()%>
    }*/);
    }
}
