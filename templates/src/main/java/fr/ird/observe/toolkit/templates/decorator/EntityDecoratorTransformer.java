package fr.ird.observe.toolkit.templates.decorator;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.decoration.EntityDecoratorRenderer;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticDefinitionModel;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticsHelper;
import fr.ird.observe.toolkit.templates.entity.ToolkitEntityToDtoMapping;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.topia.templates.EntityToDtoMapping;
import org.nuiton.topia.templates.TopiaTemplateHelper;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;

/**
 * Created on 21/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.33
 */
public class EntityDecoratorTransformer extends DecoratorTransformerSupport {
    protected EntityToDtoMapping entityToDtoMapping;
    protected TopiaTemplateHelper templateHelper;
    private ProjectPackagesDefinition def;
    private Class<? extends BusinessDto> dtoType;

    @Override
    public void applyTemplate(ObjectModel model, File destDir) throws IOException {
        templateHelper = new TopiaTemplateHelper(model);
        super.applyTemplate(model, destDir);
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        this.def = ProjectPackagesDefinition.of(getClassLoader());

        for (EntityToDtoMapping toDtoMapping : ServiceLoader.load(EntityToDtoMapping.class, getClassLoader())) {
            this.entityToDtoMapping = toDtoMapping;
            this.entityToDtoMapping.init(this);
            break;
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Class<? extends EntityDecoratorRenderer> rendererType(ObjectModelClassifier input) {
        return EntityDecoratorRenderer.class;
    }

    @Override
    protected boolean acceptClass(ObjectModelClass input) {
        if (!templateHelper.isEntity(input)) {
            // not an entity, skip class.
            return false;
        }

        boolean isAbstract = input.isAbstract() && !input.isStatic();
        if (isAbstract) {
            return false;
        }
        boolean doDto = entityToDtoMapping != null && entityToDtoMapping.accept(input);
        if (!doDto) {
            return false;
        }
        //noinspection unchecked
        dtoType = (Class<? extends BusinessDto>) entityToDtoMapping.getDtoType();
        return dtoType != null;
    }

    @Override
    protected String getClassName(ObjectModelClassifier input) {
        return input.getName();
    }

    @Override
    protected List<StatisticDefinitionModel> getStatistics(ObjectModelClassifier input) {
        String[] statisticsTagValue = ((ToolkitEntityToDtoMapping) entityToDtoMapping).getStatisticsTagValue(dtoType);
        return statisticsTagValue == null ? null : StatisticsHelper.toModel(model, input, def, Objects.requireNonNull(statisticsTagValue));
    }

    @Override
    protected String getDecoratorTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return ((ToolkitEntityToDtoMapping) entityToDtoMapping).getDecoratorDescription(dtoType);
    }

    @Override
    protected String getDecoratorWithClassifierTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return ((ToolkitEntityToDtoMapping) entityToDtoMapping).getDecoratorLongDescription(dtoType);
    }
}
