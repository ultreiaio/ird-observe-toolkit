package fr.ird.observe.toolkit.templates.entity;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.referential.SqlStatements;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataAssociation;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.script.DeleteReferentialScript;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Generate delete script for a referential that has no usage in any business data.
 * <p>
 * It will
 *     <ul>
 * <li> delete any many-to-many associations of this type. </li>
 *     <li>delete the referential it-self</li>
 *     </ul>
 *     <p>
 *         <b>Note:</b> we don not have to take care of any composition, since there is no one in referential world.
 *     </p>
 * Created on 29/06/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see DeleteReferentialScript
 */
public class DeleteReferentialScriptGenerator {

    /**
     * To delete a entity.
     */
    private static final String DELETE_STATEMENT = "DELETE FROM %s.%s WHERE topiaId = '%s';\n";
    private final Set<TopiaMetadataAssociation> associations;
    private final Set<TopiaMetadataAssociation> reverseAssociations;
    private final TopiaMetadataEntity metadataEntity;

    public DeleteReferentialScriptGenerator(TopiaMetadataModel topiaMetadataModel, TopiaMetadataEntity metadataEntity) {
        this.metadataEntity = Objects.requireNonNull(metadataEntity);
        this.associations = topiaMetadataModel.getAssociations(metadataEntity);
        this.reverseAssociations = topiaMetadataModel.getReverseManyToManyAssociations(metadataEntity);
    }

    public List<String> generateSql(String id) {
        List<String> result = new LinkedList<>();
        for (TopiaMetadataAssociation association : associations) {
            String sql = generateManyToManyAssociationDeleteStatement(association, id);
            result.add(sql.trim());
        }
        for (TopiaMetadataAssociation association : reverseAssociations) {
            if (association.getOwner().getFullyQualifiedName().contains(".referential")) {
                // always delete referential associations
                // this is the opposite of https://gitlab.com/ultreiaio/ird-observe/issues/1065
                // See https://gitlab.com/ultreiaio/ird-observe/issues/1270
                String sql = generateAssociationDeleteStatement(association, id);
                result.add(sql.trim());
            }
        }
        String sql = generateDeleteStatement(metadataEntity, id);
        result.add(sql.trim());
        return result;
    }

    private String generateAssociationDeleteStatement(TopiaMetadataAssociation association, String sourceId) {
        return String.format(SqlStatements.ASSOCIATION_DELETE_STATEMENT, association.getOwner().getDbSchemaName(), association.getTableName(), association.getTargetDbName(), sourceId);
    }

    private String generateDeleteStatement(TopiaMetadataEntity entity, String sourceId) {
        return String.format(DELETE_STATEMENT,
                             entity.getDbSchemaName(),
                             entity.getDbTableName(),
                             sourceId);
    }


    private String generateManyToManyAssociationDeleteStatement(TopiaMetadataAssociation association, String sourceId) {
        return String.format(SqlStatements.ASSOCIATION_DELETE_STATEMENT,
                             association.getOwner().getDbSchemaName(),
                             association.getTableName(),
                             association.getOwner().getDbTableName(),
                             sourceId);
    }

}
