package fr.ird.observe.toolkit.templates.decorator;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import fr.ird.observe.decoration.DefaultDecoratorRenderer;
import fr.ird.observe.decoration.DtoDecoratorRenderer;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticDefinitionModel;
import fr.ird.observe.toolkit.templates.dto.stats.StatisticsHelper;
import io.ultreia.java4all.decoration.DecoratorRenderer;
import org.nuiton.eugene.java.BeanTransformerContext;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.List;
import java.util.Map;

/**
 * Created on 21/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public class DtoDecoratorTransformer extends DecoratorTransformerSupport {

    private Map<ObjectModelClass, List<StatisticDefinitionModel>> statistics;
    protected BeanTransformerContext context;

    @Override
    public void transformFromModel(ObjectModel model) {

        context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, false, false, input -> {
            ObjectModelPackage aPackage = model.getPackage(input.getPackageName());
            String decoratorTagValue = getDecoratorTagValue(aPackage, input);
            if (decoratorTagValue != null) {
                return true;
            }
            String decoratorWithClassifierTagValue = getDecoratorWithClassifierTagValue(aPackage, input);
            return decoratorWithClassifierTagValue != null;
        }, getLog());
        context.report();
        statistics = StatisticsHelper.toModel(model, ProjectPackagesDefinition.of(getClassLoader()));
    }

    @Override
    public void transformFromInterface(ObjectModelInterface input) {
        if (input.getName().endsWith("Aware") || input.getName().startsWith("With")) {
            return;
        }
        generateClassifier(input);
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Class<? extends DecoratorRenderer> rendererType(ObjectModelClassifier input) {
        return input instanceof ObjectModelClass ? DtoDecoratorRenderer.class : DefaultDecoratorRenderer.class;
    }

    @Override
    protected boolean acceptClass(ObjectModelClass input) {
        return context.selectedClasses.contains(input);
    }

    @Override
    protected String getClassName(ObjectModelClassifier input) {
        return input.getName() + (input instanceof ObjectModelClass ? "Dto" : "");
    }

    @Override
    protected List<StatisticDefinitionModel> getStatistics(ObjectModelClassifier input) {
        return input instanceof ObjectModelClass ? statistics.get(input) : null;
    }

    @Override
    protected String getDecoratorTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return toolkitTagValues.getDecoratorTagValue(aPackage, input);
    }

    @Override
    protected String getDecoratorWithClassifierTagValue(ObjectModelPackage aPackage, ObjectModelClassifier input) {
        return toolkitTagValues.getDecoratorWithClassifierTagValue(aPackage, input);
    }

    @Override
    protected String getDecoratorHelperImpl(String modelName, @SuppressWarnings("rawtypes") Class<? extends DecoratorRenderer> decoratorHelper) {
        if (DefaultDecoratorRenderer.class.equals(decoratorHelper)) {
            return decoratorHelper.getName();
        }
        return super.getDecoratorHelperImpl(modelName, decoratorHelper);
    }

    @Override
    protected String translatePackageName(String packageName) {
        return packageName.replace(".decoration", ".dto.decoration");
    }
}
