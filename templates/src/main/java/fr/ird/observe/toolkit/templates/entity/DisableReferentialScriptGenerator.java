package fr.ird.observe.toolkit.templates.entity;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.referential.SqlStatements;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.script.DisableReferentialScript;

import java.util.Objects;

/**
 * To generate disable referential script.
 * Created on 04/08/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see DisableReferentialScript
 * @since 5.0
 */
public class DisableReferentialScriptGenerator {

    private final TopiaMetadataEntity metadataEntity;

    public DisableReferentialScriptGenerator(TopiaMetadataEntity metadataEntity) {
        this.metadataEntity = Objects.requireNonNull(metadataEntity);
    }

    public String generateSql() {
        return generateUpdateStatement(metadataEntity).trim();
    }

    private String generateUpdateStatement(TopiaMetadataEntity entity) {
        return String.format(SqlStatements.UPDATE_STATEMENT,
                             entity.getDbSchemaName(),
                             entity.getDbTableName(),
                             "status = 0, topiaVersion = topiaVersion + 1, lastUpdateDate = '%1$s'::timestamp",
                             "%2$s");
    }
}
