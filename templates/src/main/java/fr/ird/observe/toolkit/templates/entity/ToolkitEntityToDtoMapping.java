package fr.ird.observe.toolkit.templates.entity;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.DtoToReference;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataFileDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.I18nReferentialDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.toolkit.templates.TemplateContract;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;
import io.ultreia.java4all.util.ServiceLoaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.topia.templates.EntityToDtoMapping;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4
 */
@AutoService(EntityToDtoMapping.class)
public class ToolkitEntityToDtoMapping extends EntityToDtoMapping {

    private static final Logger log = LogManager.getLogger(ToolkitEntityToDtoMapping.class);
    private final Map<Class<?>, JavaBeanDefinition> definitions = new LinkedHashMap<>();
    private Class<? extends BusinessDto> dtoType;
    private BusinessProject businessProject;
    private Properties dtoTagValues;
    private Set<String> containerChildDataTypes;

    @Override
    public void init(ObjectModelTransformerToJava transformer) {
        super.init(transformer);
        String modelName = transformer.getModel().getName();
        dtoTagValues = TemplateContract.loadDtoTagValues(transformer.getClassLoader(), modelName);
        businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);
        containerChildDataTypes = businessProject.getContainerChildDataTypes().stream().map(c -> c.getName().replace(".dto.", ".entities.").replace("Dto", "")).collect(Collectors.toSet());
    }

    public boolean containerChildDataTypes(String type) {
        return containerChildDataTypes.contains(type);
    }

    @Override
    public void close() {
    }

    @Override
    public Set<Class<?>> getDtoTypes(ObjectModelClass entity) {

        @SuppressWarnings({"unchecked", "rawtypes"}) Set<Class<?>> subDtoTypes = (Set) businessProject.getMapping().getSubDtoTypes(Objects.requireNonNull(dtoType));
        for (Class<?> dtoType : subDtoTypes) {
            definitions.put(dtoType, JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow(() -> new RuntimeException("Can't find java bean definition for dtoType: " + dtoType.getName())));
        }
        if (!subDtoTypes.contains(dtoType)) {
            definitions.put(dtoType, JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow(() -> new RuntimeException("Can't find java bean definition for dtoType: " + dtoType.getName())));
            subDtoTypes = Collections.singleton(dtoType);
        }
        return subDtoTypes;
    }

    @Override
    public boolean accept(ObjectModelClass entity) {
        if (entity.getQualifiedName().equals(Entity.class.getName())) {
            return false;
        }
        if (entity.getQualifiedName().equals(RootOpenableEntity.class.getName())) {
            return false;
        }
        if (!entity.getQualifiedName().equals(ReferentialEntity.class.getName())
                && !entity.getQualifiedName().equals(DataEntity.class.getName())) {

            for (ObjectModelClass superclass : entity.getSuperclasses()) {
                String qualifiedName = superclass.getQualifiedName();
                if (qualifiedName.equals(Entity.class.getName())) {
                    return false;
                }
            }
        }
        String entityName = entity.getQualifiedName();
        String dtoName = entityName.replace("Entity", "").replace("entities", "dto") + "Dto";
        try {
            log.info(String.format("Looking for dtoType: %s from entity type: %s", dtoName, entityName));
            //noinspection unchecked
            dtoType = (Class<? extends BusinessDto>) Class.forName(dtoName, true, getTransformer().getClassLoader());
            JavaBeanDefinition javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(dtoType).orElseThrow(RuntimeException::new);
            definitions.put(dtoType, javaBeanDefinition);
        } catch (Exception e) {
            log.error(String.format("can't find dto type for entity type: %s", entityName), e);
            return false;
        }
        return true;
    }

    @Override
    public boolean acceptReference(ObjectModelClass entity) {
        return DtoToReference.class.isAssignableFrom(dtoType);
    }

    @Override
    public Class<?> getDtoType() {
        return dtoType;
    }

    @Override
    public boolean acceptToDtoProperty(Class<?> dtoType, String propertyName) {
        JavaBeanDefinition javaBeanDefinition = definitions.get(dtoType);
        Optional<JavaBeanPropertyDefinition<?, ?>> first = javaBeanDefinition.writeProperties().filter(p -> p.propertyName().equals(propertyName)).findFirst();
        return first.isPresent() && !(DataFileDto.class.equals(first.get().type()));
    }

    @Override
    public boolean acceptFromDtoProperty(Class<?> dtoType, String propertyName, boolean multiple, boolean isEntity) {
        if (isSkipCopyToEntity(dtoType, propertyName)) {
            return false;
        }
        if (!multiple || !isEntity) {
            return true;
        }
        if (!acceptToDtoProperty(dtoType, propertyName)) {
            return false;
        }
        // reject all relation from stubs dto
        Class<?> atomicType = getAtomicType(dtoType, propertyName);
        //FIXME Add a contract for this instead of this weak test
        return !atomicType.getName().contains("Stub");
    }

    private boolean isSkipCopyToEntity(Class<?> dtoType, String propertyName) {
        String dtoName = dtoType.getName().replace("Dto", "");
        String tagValue = String.format("%s.attribute.%s.tagValue.%s", dtoName, propertyName, ToolkitTagValues.Store.skipCopyToEntity);
        String property = dtoTagValues.getProperty(tagValue);
        return "true".equals(property);
    }

    @Override
    public String getToDtoSimpleMethod(Class<?> dtoType, ObjectModelAttribute attribute, String operationPrefix, boolean isEntity) {
        String call = String.format("entity.%s()", getTransformer().getJavaBeanMethodName(operationPrefix, attribute.getName()));
        if (isEntity) {
            String attributeType = attribute.getType();
            String attributeName = attribute.getName();
            Class<?> atomicType = getAtomicType(dtoType, attributeName);
            String spi;
            if (DtoReference.class.isAssignableFrom(atomicType)) {
                spi = String.format("%s.SPI", attributeType);
                call = String.format("%s.toReference(referentialLocale, %s)", spi, call);
            } else {
                String spiName = GeneratorUtil.convertVariableNameToConstantName(atomicType.getSimpleName().replace("Dto", "") + "Spi");
                spi = String.format("%s.%s", attributeType, spiName);
                call = String.format("%s.toDto(referentialLocale, %s)", spi, call);
            }
        }
        return call;
    }

    @Override
    public String getToDtoMultipleMethod(Class<?> dtoType, ObjectModelAttribute attribute, boolean isEntity) {
        String attributeName = attribute.getName();
        String entityCall = "entity." + getTransformer().getJavaBeanMethodName("get", attributeName);
        if (isEntity || attribute.getDeclaringElement().isStatic()) {
            JavaBeanDefinition javaBeanDefinition = definitions.get(dtoType);
            Class<?> atomicType = getAtomicType(dtoType, attributeName);

            String attributeType = attribute.getType();
            String spi = String.format("%s.SPI", attributeType);
            Class<?> type = javaBeanDefinition.writeProperty(attributeName).type();
            if (type.equals(LinkedHashSet.class)) {
                if (DataDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataReferenceLinkedHashSet(referentialLocale, %s())", spi, entityCall);
                }
                String attributeTypeName = attribute.getClassifier().getName();
                if ((attributeTypeName + "Dto").equals(atomicType.getSimpleName())) {
                    return String.format("%s.toDtoLinkedHashSet(referentialLocale, %s())", spi, entityCall);
                } else {
                    String spiName = GeneratorUtil.convertVariableNameToConstantName(atomicType.getSimpleName().replace("Dto", "") + "Spi");
                    spi = String.format("%s.%s", attributeType, spiName);
                    return String.format("%s.toDtoLinkedHashSet(referentialLocale, %s())", spi, entityCall);
                }
            }
            if (type.equals(List.class)) {
                if (ReferentialDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toReferentialReferenceList(referentialLocale, %s())", spi, entityCall);
                }
                if (DataDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataReferenceList(referentialLocale, %s())", spi, entityCall);
                }
                return String.format("%s.toDataDtoList(referentialLocale, %s())", spi, entityCall);
            }
            if (type.equals(Collection.class)) {
                if (ReferentialDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toReferentialReferenceList(referentialLocale, %s())", spi, entityCall);
                }
                if (DataDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataReferenceLinkedHashSet(referentialLocale, %s())", spi, entityCall);
                }
                return String.format("%s.toDataDtoList(referentialLocale, %s())", spi, entityCall);
            }
            return String.format("%s()", entityCall);
        }
        return String.format("%s()", entityCall);
    }

    @Override
    public String getFromDtoSimpleMethod(Class<?> dtoType, ObjectModelAttribute attribute, String operationPrefix, boolean isEntity) {
        String attributeName = attribute.getName();
        String dtoCall = String.format("dto.%s()", getTransformer().getJavaBeanMethodName(operationPrefix, attributeName));
        if (isEntity) {
            String attributeType = attribute.getType();
            Class<?> atomicType = getAtomicType(dtoType, attributeName);
            String spi;
            if (DtoReference.class.isAssignableFrom(atomicType)) {
                spi = String.format("%s.SPI", attributeType);
                return String.format("%s.toEntity(%s)", spi, dtoCall);
            } else {
                String spiName = GeneratorUtil.convertVariableNameToConstantName(atomicType.getSimpleName().replace("Dto", "") + "Spi");
                spi = String.format("%s.%s", attributeType, spiName);
                return String.format("%s.toEntity(%s)", spi, dtoCall);
            }
        }
        if ("java.sql.Blob".equals(attribute.getType())) {
            return "null";
        }
        return dtoCall;
    }

    @Override
    public String getFromDtoMultipleMethod(Class<?> dtoType, ObjectModelAttribute attribute, Class<?> collectionInterface, boolean isEntity) {
        String attributeName = attribute.getName();
        String dtoCall = String.format("dto.%s()", getTransformer().getJavaBeanMethodName("get", attributeName));
        String entityCall = String.format("entity.%s()", getTransformer().getJavaBeanMethodName("get", attributeName));

        if (isEntity || attribute.getDeclaringElement().isStatic()) {
            Class<?> atomicType = getAtomicType(dtoType, attributeName);
            String attributeType = attribute.getType();
            String spi = String.format("%s.SPI", attributeType);
            if (DataDtoReference.class.isAssignableFrom(atomicType)) {
                if (collectionInterface.equals(Collection.class)) {
                    return String.format("%s.toDataEntitySet(%s)", spi, dtoCall);
                }
                // never assign back data reference list to entity
                // FIXME We should use a transient modifier on attribute for this
                return null;
            }
            if (collectionInterface.equals(Set.class)) {
                if (DataDto.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataEntityTypedSet(referentialLocale, %s, %s)", spi, dtoCall, entityCall);
                } else if (ReferentialDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toReferentialEntitySet(%s)", spi, dtoCall);
                }
                return dtoCall;
            }
            if (collectionInterface.equals(List.class)) {
                if (DataDto.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataEntityList(referentialLocale, %s, %s)", spi, dtoCall, entityCall);
                } else if (ReferentialDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toReferentialEntityList(%s)", spi, dtoCall);
                }
                return dtoCall;
            }
            if (collectionInterface.equals(Collection.class)) {
                if (ReferentialDtoReference.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toReferentialEntitySet(%s)", spi, dtoCall);
                } else if (DataDto.class.isAssignableFrom(atomicType)) {
                    return String.format("%s.toDataEntityCollection(referentialLocale, %s, %s)", spi, dtoCall, entityCall);
                }
                return dtoCall;
            }
            return dtoCall;
        }
        return dtoCall;
    }

    @Override
    public Class<?> getDtoContract(ObjectModelClass input) {
        if (ReferentialDto.class.isAssignableFrom(dtoType)) {
            return ReferentialDto.class;
        }
        if (DataGroupByDto.class.isAssignableFrom(dtoType)) {
            return DataGroupByDto.class;
        }
        if (RootOpenableDto.class.isAssignableFrom(dtoType)) {
            return RootOpenableDto.class;
        }
        return DataDto.class;
    }

    public Class<? extends BusinessDto> getDtoContract2() {
        if (I18nReferentialDto.class.isAssignableFrom(dtoType)) {
            return I18nReferentialDto.class;
        }
        if (ReferentialDto.class.isAssignableFrom(dtoType)) {
            return ReferentialDto.class;
        }
        if (DataGroupByDto.class.isAssignableFrom(dtoType)) {
            return DataGroupByDto.class;
        }
        if (RootOpenableDto.class.isAssignableFrom(dtoType)) {
            return RootOpenableDto.class;
        }
        return DataDto.class;
    }

    Class<?> getAtomicType(Class<?> dtoType, String propertyName) {
        try {
            return getAtomicType0(dtoType, propertyName);
        } catch (NoSuchFieldException e) {
            if (dtoType.getSuperclass() != null) {
                return getAtomicType(dtoType.getSuperclass(), propertyName);
            }
            throw new IllegalArgumentException(e);
        }
    }

    Class<?> getAtomicType0(Class<?> dtoType, String propertyName) throws NoSuchFieldException {
        Field declaredField = dtoType.getSuperclass().getDeclaredField(propertyName);
        Type genericType = declaredField.getGenericType();
        if (genericType instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) genericType).getActualTypeArguments()[0];
        }
        return (Class<?>) genericType;
    }

    public Class<? extends DtoReference> getReferenceType(Class<? extends BusinessDto> dtoType) {
        return businessProject.getMapping().getReferenceType(dtoType);
    }

    public Class<? extends DataDto> getMainDtoType(Class<? extends DataDto> dtoType) {
        return businessProject.getMapping().getMainDtoType(dtoType);
    }

    public String[] getStatisticsTagValue(Class<? extends BusinessDto> dtoType) {
        String dtoName = dtoType.getName().replace("Dto", "");
        String tagValue = String.format("%s.class.tagValue.%s", dtoName, ToolkitTagValues.Store.statistics);
        String result = dtoTagValues.getProperty(tagValue);
        return result == null ? null : result.trim().split("\\s*,\\s*");
    }

    public String getDecoratorDescription(Class<? extends BusinessDto> dtoType) {
        String dtoName = dtoType.getName().replace("Dto", "");
        String tagValue = String.format("%s.class.tagValue.%s", dtoName, ToolkitTagValues.Store.decorator);
        String result = dtoTagValues.getProperty(tagValue);
        if (result == null && ReferentialDto.class.isAssignableFrom(dtoType)) {
            // try on package
            tagValue = String.format("package.%s.tagValue.%s", ReferentialDto.class.getPackageName(), ToolkitTagValues.Store.decorator);
            result = dtoTagValues.getProperty(tagValue);
        }
        return result;
    }

    public String getDecoratorLongDescription(Class<? extends BusinessDto> dtoType) {
        String dtoName = dtoType.getName().replace("Dto", "");
        String tagValue = String.format("%s.class.tagValue.%s", dtoName, ToolkitTagValues.Store.decoratorWithClassifier);
        String result = dtoTagValues.getProperty(tagValue);
        if (result == null && ReferentialDto.class.isAssignableFrom(dtoType)) {
            // try on package
            tagValue = String.format("package.%s.tagValue.%s", ReferentialDto.class.getPackageName(), ToolkitTagValues.Store.decoratorWithClassifier);
            result = dtoTagValues.getProperty(tagValue);
        }
        return result;
    }
}
