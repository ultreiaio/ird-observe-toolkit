package fr.ird.observe.toolkit.templates;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.templates.decorator.EntityDecoratorTransformer;
import fr.ird.observe.toolkit.templates.entity.ReferentialExtraScriptsGenerator;
import fr.ird.observe.toolkit.templates.entity.ToolkitEntityHibernateMappingTransformer;
import fr.ird.observe.toolkit.templates.entity.ToolkitEntityScriptModelGenerator;
import fr.ird.observe.toolkit.templates.entity.ToolkitEntityTransformer;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.topia.templates.ApplicationContextTransformer;
import org.nuiton.topia.templates.EntityDaoTransformer;
import org.nuiton.topia.templates.PersistenceContextTransformer;
import org.nuiton.topia.templates.TopiaMetaTransformer;
import org.nuiton.topia.templates.sql.EntitySqlDescriptorGenerator;
import org.nuiton.topia.templates.sql.EntitySqlModelClassGenerator;

/**
 * Created: 20 déc. 2009
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Component(role = Template.class, hint = "fr.ird.observe.toolkit.templates.GeneratePersistence")
public class GeneratePersistence extends TopiaMetaTransformer {

    @SuppressWarnings("unchecked")
    public GeneratePersistence() {
        setTemplateTypes(
                ReferentialExtraScriptsGenerator.class,
                ToolkitEntityTransformer.class,
                ToolkitEntityHibernateMappingTransformer.class,
                ApplicationContextTransformer.class,
                PersistenceContextTransformer.class,
                EntityDaoTransformer.class,
                EntitySqlDescriptorGenerator.class,
                ToolkitEntityScriptModelGenerator.class,
                EntitySqlModelClassGenerator.class
        );
    }
}
