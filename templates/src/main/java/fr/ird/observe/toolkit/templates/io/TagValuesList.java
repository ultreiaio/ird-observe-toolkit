package fr.ird.observe.toolkit.templates.io;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import org.nuiton.eugene.LogProxy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a list of tag values used in model.
 * <p>
 * Created on 28/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.5
 */
public class TagValuesList {

    private static final String PROPERTIES = ".properties";

    private final String classifier;
    private final LogProxy log;
    private final List<String> list;

    public static void store(Path target, List<String> list) throws IOException {
        list.sort(String::compareTo);
        if (Files.notExists(target.getParent())) {
            Files.createDirectories(target.getParent());
        }
        Files.write(target, list);
    }

    public TagValuesList(String classifier, LogProxy log) {
        this.classifier = classifier;
        this.log = log;
        this.list = new LinkedList<>();
    }

    public void load(Path path) throws IOException {
        list.clear();
        load0(path, "class");
        load0(path, "attribute");
        load0(path, "package");
        log.info(String.format("[%s] Found %d existing tag-value(s) list.", classifier, list.size()));
    }

    public void add(String tagValue) {
        if (!list.contains(tagValue)) {
            list.add(tagValue);
        }
    }

    public List<String> getList() {
        return list;
    }

    private void load0(Path path, String type) throws IOException {
        Path resolve = path.resolve(type);
        if (Files.notExists(resolve)) {
            return;
        }
        String prefix = type + ".tagValue.";
        Files.walk(resolve, 1)
                .filter(p -> p.toFile().getName().endsWith(PROPERTIES))
                .map(p -> prefix + Strings.removeEnd(p.toFile().getName(), PROPERTIES))
                .forEach(list::add);
    }
}
