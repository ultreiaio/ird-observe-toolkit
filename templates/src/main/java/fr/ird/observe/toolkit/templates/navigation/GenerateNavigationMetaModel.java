package fr.ird.observe.toolkit.templates.navigation;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.navigation.model.MetaModel;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.id.IdProjectModel;
import fr.ird.observe.spi.navigation.model.id.IdProjectModelBuilder;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModelBuilder;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;
import fr.ird.observe.toolkit.templates.TemplateContract;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataEntity;
import org.nuiton.topia.service.sql.metadata.TopiaMetadataModel;
import org.nuiton.topia.templates.TopiaTemplateHelper;
import org.nuiton.topia.templates.sql.TopiaMetadataModelBuilder;
import org.nuiton.topia.templates.sql.order.ReplicationOrderBuilderWithStandalone;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateNavigationMetaModel extends ObjectModelTransformerToJava implements TemplateContract {

    @Override
    public void transformFromModel(ObjectModel model) {
        ProjectPackagesDefinition def = ProjectPackagesDefinition.of(getClassLoader());

        MetaModelBuilderFromModel metaModelBuilder = new MetaModelBuilderFromModel(getLog(), model, def);

        MetaModel metaModel = metaModelBuilder.build();

        getLog().info(String.format("Found %d root node(s) to generate.", metaModel.getRoot().size()));
        getLog().info(String.format("Found %d node(s) to generate.", metaModel.getNodes().size()));

        Path navigationTargetDirectory = getTargetDirectory();
        createDirectory(navigationTargetDirectory);

        generateMetaModelFile(metaModel, navigationTargetDirectory.resolve(String.format("%s-metamodel.json", model.getName())));
        generateIdModelFile(IdProjectModelBuilder.createIdModel(metaModel), navigationTargetDirectory.resolve(String.format("%s-id-model.json", model.getName())));
        generateTreeModelFile(TreeProjectModelBuilder.createSelectionModel(metaModel), navigationTargetDirectory.resolve(String.format("%s-tree-selection-model.json", model.getName())));
        generateTreeModelFile(TreeProjectModelBuilder.createNavigationModel(metaModel), navigationTargetDirectory.resolve(String.format("%s-tree-navigation-model.json", model.getName())));
        generateParentModelFile(new ParentProjectModel(metaModelBuilder.getParentLinks()), navigationTargetDirectory.resolve(String.format("%s-parent-model.json", model.getName())));
        generateReplicationOrderFile(model, getRootResourcePath().resolve(getStandaloneReplicationOrderLocation(model.getName())));

        generateModelSupport(model);
    }

    public static String getStandaloneReplicationOrderLocation(String modelName) {
        return String.format("META-INF/persistence/%s-standalone-replication-order.json", modelName);
    }

    private static void createDirectory(Path directory) {
        try {
            if (Files.notExists(directory)) {
                Files.createDirectories(directory);
            }
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't create directory %s", directory), e);
        }
    }

    private void generateTreeModelFile(TreeProjectModel metaModel, Path targetFile) {
        getWriterReport().addFile(targetFile.toFile().getName(), targetFile.toFile(), isVerbose());
        try {
            Files.writeString(targetFile, MetaModelSupport.getGson().toJson(metaModel));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write %s", targetFile), e);
        }
    }

    private void generateIdModelFile(IdProjectModel metaModel, Path targetFile) {
        getWriterReport().addFile(targetFile.toFile().getName(), targetFile.toFile(), isVerbose());
        try {
            Files.writeString(targetFile, MetaModelSupport.getGson().toJson(metaModel));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write %s", targetFile), e);
        }
    }

    private void generateMetaModelFile(MetaModel metaModel, Path destinationFile) {
        getWriterReport().addFile(destinationFile.toFile().getName(), destinationFile.toFile(), isVerbose());
        try {
            Files.writeString(destinationFile, MetaModelSupport.getGson().toJson(metaModel));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write %s", destinationFile), e);
        }
    }

    private void generateParentModelFile(ParentProjectModel metaModel, Path destinationFile) {
        getWriterReport().addFile(destinationFile.toFile().getName(), destinationFile.toFile(), isVerbose());
        try {
            Files.writeString(destinationFile, MetaModelSupport.getGson().toJson(metaModel));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write %s", destinationFile), e);
        }
    }

    private void generateReplicationOrderFile(ObjectModel model, Path destinationFile) {
        createDirectory(destinationFile.getParent());
        TopiaTemplateHelper templateHelper = new TopiaTemplateHelper(model);
        TopiaMetadataModel metadataModel = TopiaMetadataModelBuilder.build(isVerbose(), model, templateHelper);
        metadataModel.applyInheritance();

        Set<TopiaMetadataEntity> standaloneEntities = metadataModel.streamWithStandalone().collect(Collectors.toSet());
        List<TopiaMetadataEntity> entities = ReplicationOrderBuilderWithStandalone.build(metadataModel, standaloneEntities);
        List<String> replicationOrderWithStandalone = TopiaMetadataEntity.toFqn(entities);

        getWriterReport().addFile(destinationFile.toFile().getName(), destinationFile.toFile(), isVerbose());
        try {
            Files.writeString(destinationFile, MetaModelSupport.getGson().toJson(replicationOrderWithStandalone));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't write %s", destinationFile), e);
        }
    }

    private void generateModelSupport(ObjectModel model) {
        String packageName = getDefaultPackageName().replace(".entities", ".spi.navigation");
        String modelName = model.getName();
        ObjectModelClass modelSupport = createClass(modelName + "MetaModel", packageName);
        setSuperClass(modelSupport, MetaModelSupport.class);
        addStaticFactory(modelSupport, modelSupport.getQualifiedName());
        ObjectModelOperation constructor = addConstructor(modelSupport, ObjectModelJavaModifier.PUBLIC);
        setOperationBody(constructor, ""/*{
        super("<%=modelName%>");
    }*/);
    }

    protected Path getTargetDirectory() {
        Path rootResourcePath = getRootResourcePath();
        return rootResourcePath.resolve("META-INF").resolve("navigation");
    }

    private Path getRootResourcePath() {
        return getConfiguration().getProperty(PROP_RESOURCE_DIRECTORY, File.class).toPath();
    }
}
