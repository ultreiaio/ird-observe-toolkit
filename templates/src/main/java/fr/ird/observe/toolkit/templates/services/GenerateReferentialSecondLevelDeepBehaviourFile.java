package fr.ird.observe.toolkit.templates.services;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.referential.ReferentialSecondLevelDeepBehaviourModel;
import fr.ird.observe.toolkit.templates.ToolkitTagValues;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.ObjectModelType;
import org.nuiton.topia.templates.sql.TopiaMetadataModelGeneratorSupport;

import java.io.File;
import java.io.Writer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created at 30/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 10.0.0
 */
public class GenerateReferentialSecondLevelDeepBehaviourFile extends ObjectModelGenerator {

    @Override
    public void applyTemplate(ObjectModel model, File destDir) {
        this.model = model;

        File realTarget = TopiaMetadataModelGeneratorSupport.getNotGeneratedResourceDirector(destDir);
        String filename = getFilenameForModel(model);
        generateFromElement(model, realTarget, filename, ObjectModelType.OBJECT_MODEL);
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return ReferentialSecondLevelDeepBehaviourModel.toLocation(super.getFilenameForModel(model));
    }

    @Override
    protected void generateFromElement(Object element, File destDir, String filename, ObjectModelType type) {
        if (ObjectModelType.OBJECT_MODEL != type) {
            // only generate on model
            return;
        }
        super.generateFromElement(element, destDir, filename, type);
    }

    @Override
    public void generateFromModel(Writer output, ObjectModel input) {
        List<String> referentialTypes = new LinkedList<>();

        ToolkitTagValues tagValues = new ToolkitTagValues();
        for (ObjectModelClass classifier : model.getClasses()) {
            if (tagValues.isReferentialSecondLevelDeepBehaviour(model.getTagValuesStore(), classifier)) {
                referentialTypes.add(classifier.getQualifiedName() + "Dto");
            }
        }
        Collections.sort(referentialTypes);
        ReferentialSecondLevelDeepBehaviourModel.newGson().toJson(referentialTypes, output);
    }

    @Override
    protected File getDestinationFile(File destDir, String filename) {
        return destDir.toPath().resolve(filename).toFile();
    }
}
