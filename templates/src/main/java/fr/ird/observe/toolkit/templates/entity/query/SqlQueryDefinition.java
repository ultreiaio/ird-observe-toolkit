package fr.ird.observe.toolkit.templates.entity.query;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created at 14/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class SqlQueryDefinition {
    private final String type;
    private final String name;
    private final String query;
    private final String comment;
    private final boolean callable;
    private final Map<String, String> parameters;

    public SqlQueryDefinition(String type, String name, String query, String comment, boolean callable, Map<String, String> parameters) {
        this.type = Objects.requireNonNull(type);
        this.name = Objects.requireNonNull(name);
        this.query = Objects.requireNonNull(query);
        this.comment = comment;
        this.callable = callable;
        this.parameters = parameters;
    }

    public String getFullyQualifiedName() {
        return getType() + "::" + getName();
    }

    public String getName() {
        return name;
    }

    public String getQuery() {
        return query;
    }

    public boolean isCallable() {
        return callable;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getComment() {
        return comment;
    }

    public boolean withParameters() {
        return !parameters.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SqlQueryDefinition)) return false;
        SqlQueryDefinition that = (SqlQueryDefinition) o;
        return Objects.equals(type, that.type) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }

    @Override
    public String toString() {
        StringJoiner result = new StringJoiner(", ", SqlQueryDefinition.class.getSimpleName() + "[", "]")
                .add("type='" + type + "'")
                .add("name='" + name + "'")
                .add("fqn='" + getFullyQualifiedName() + "'")
                .add("query='" + query + "'");
        if (isCallable()) {
            result.add("callable= true");
        }
        if (getComment() != null) {
            result.add("comment=" + comment);
        }
        if (withParameters()) {
            result.add("parameters=" + parameters);
        }
        return result.toString();
    }
}
