package fr.ird.observe.toolkit.templates.services;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.referential.differential.DifferentialMetaModel;
import fr.ird.observe.toolkit.templates.TemplateContract;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/**
 * Created on 19/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.23
 */
public class GenerateDifferentialMetaModelClass extends ObjectModelTransformerToJava implements TemplateContract {

    @Override
    public void transformFromModel(ObjectModel model) {
        String packageName = getDefaultPackageName().replace(".dto", ".services.service.referential.differential");
        String modelName = model.getName();
        ObjectModelClass modelSupport = createClass(modelName + "DifferentialMetaModel", packageName);
        setSuperClass(modelSupport, DifferentialMetaModel.class);
        addStaticFactory(modelSupport, modelSupport.getQualifiedName());
        ObjectModelOperation constructor = addConstructor(modelSupport, ObjectModelJavaModifier.PUBLIC);
        setOperationBody(constructor, ""/*{
        super("<%=modelName%>");
    }*/);
    }

}
