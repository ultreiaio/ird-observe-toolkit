package org.nuiton.topia.templates;

/*
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/

import com.google.common.collect.ImmutableMap;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * To generate PersistenceHelper
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.ApplicationContextTransformer")
public class ApplicationContextTransformer extends ObjectModelTransformerToJava {

    protected TopiaTemplateHelper templateHelper;

    protected final TopiaCoreTagValues topiaCoreTagValues;

    public ApplicationContextTransformer() {
        this.topiaCoreTagValues = new TopiaCoreTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel input) {

        if (templateHelper == null) {
            templateHelper = new TopiaTemplateHelper(model);
        }

        String packageName = templateHelper.getApplicationContextPackage(this, model);

        String applicationContextAbstractName = templateHelper.getApplicationContextAbstractName(model);

        String applicationContextConcreteName = templateHelper.getApplicationContextConcreteName(model);

        boolean generateAbstract = !getResourcesHelper().isJavaFileInClassPath(packageName + "." + applicationContextAbstractName);

        boolean generateConcrete = !getResourcesHelper().isJavaFileInClassPath(packageName + "." + applicationContextConcreteName);

        if (generateAbstract) {

            generateAbstract(packageName,
                             applicationContextAbstractName);
        }

        if (generateConcrete) {

            generateImpl(packageName,
                         applicationContextAbstractName,
                         applicationContextConcreteName);
        }

    }

    protected void generateAbstract(String packageName,
                                    String className) {

        // try to find a super class by tag-value
        String superClass = topiaCoreTagValues.getApplicationContextSuperClassTagValue(model);

        if (superClass == null) {

            // no super-class, use default one
            superClass = AbstractTopiaApplicationContext.class.getName();
        } else {

            //TODO check that super class instance of ApplicationPersistenceContext

        }

        ObjectModelClass output = createAbstractClass(className, packageName);

        String persistenceContextConcreteName = templateHelper.getPersistenceContextConcreteName(model);
        setSuperClass(output, superClass + "<" + persistenceContextConcreteName + ">");

        // detect if there is a contract to set on abstract
        String contractName = templateHelper.getApplicationContextInterfaceName(model);

        boolean addPersistenceContextContract = getResourcesHelper().isJavaFileInClassPath(packageName + "." + contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        String modelName = model.getName();

        addImport(output, TopiaEntity.class);

        addConstructors(output, false);

        ObjectModelOperation op;

        // getModelVersion method
        String modelVersion = model.getVersion();
        op = addOperation(output, "getModelVersion", "String", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return "<%=modelVersion%>";
    }*/
        );

        // getModelName method
        op = addOperation(output, "getModelName", "String", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, op, Override.class);
        setOperationBody(op, ""
/*{
        return "<%=modelName%>";
    }*/
        );
        // getImplementationClasses method
        addImport(output, Set.class);
        ObjectModelOperation getImplementationClasses = addOperation(output, "getImplementationClasses", "Set<Class<? extends TopiaEntity>>", ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, getImplementationClasses, Override.class);
        StringBuilder getImplementationClassesBodyContent = new StringBuilder(""/*{
        return new LinkedHashSet<>(List.of(}*/);

        addImport(output, TopiaDao.class);
        addImport(output, ImmutableMap.class);
        addImport(output, LinkedHashSet.class);
        addImport(output, List.class);
        addImport(output, Supplier.class);
        addImport(output, Map.class);
        // createDaoMapping method
        op = addOperation(output, "createDaoMapping", "Map<String, Supplier<TopiaDao<?>>>", ObjectModelJavaModifier.PROTECTED, ObjectModelJavaModifier.FINAL);
        addAnnotation(output, op, Override.class);
        StringBuilder body = new StringBuilder(""/*{
        return ImmutableMap.<String, Supplier<TopiaDao<?>>>builder()}*/);
        TopiaTemplateHelper templateHelper = new TopiaTemplateHelper(model);
        for (ObjectModelClass entityClass : templateHelper.getEntityClasses(model, true)) {
            if (entityClass.isAbstract()) {
                continue;
            }
            String source = entityClass.getQualifiedName();
            String daoName = source + "TopiaDao";
            body.append(""/*{
                .put(<%=source%>.class.getName(), <%=daoName%>::new)}*/);
            getImplementationClassesBodyContent.append(""/*{
                <%=source%>Impl.class,}*/);
        }
        body.append(""/*{
                .build();
    }*/);
        setOperationBody(op, body.toString());
        setOperationBody(getImplementationClasses, getImplementationClassesBodyContent.substring(0, getImplementationClassesBodyContent.length() - 1) + ""/*{
                ));
    }*/);
    }

    protected ObjectModelClass generateImpl(String packageName,
                                            String applicationContextAbstractName,
                                            String applicationContextConcreteName) {

        ObjectModelClass output = createClass(applicationContextConcreteName, packageName);

        setSuperClass(output, applicationContextAbstractName);

        addConstructors(output, true);
        return output;
    }

    protected void addConstructors(ObjectModelClass output, boolean isPublic) {

        ObjectModelOperation constructor = addConstructor(output, isPublic ? ObjectModelJavaModifier.PUBLIC : ObjectModelJavaModifier.PROTECTED);
        addParameter(constructor, TopiaConfiguration.class, "topiaConfiguration");
        setOperationBody(constructor, ""/*{
        super(topiaConfiguration);
    }*/
        );
    }

}
