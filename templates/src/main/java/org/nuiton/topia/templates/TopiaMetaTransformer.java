package org.nuiton.topia.templates;

/*-
 * #%L
 * Toolkit :: Templates
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.toolkit.templates.io.TagValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.AbstractMetaTransformer;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.validator.AttributeNamesValidator;
import org.nuiton.eugene.models.object.validator.ClassNamesValidator;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;
import org.nuiton.topia.templates.sql.EntitySqlDescriptorGenerator;
import org.nuiton.topia.templates.sql.EntitySqlModelClassGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * Created: 20 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@Component(role = Template.class, hint = "org.nuiton.topia.templates.TopiaMetaTransformer")
public class TopiaMetaTransformer extends AbstractMetaTransformer<ObjectModel> {

    protected static final Set<String> FORBIDDEN_ATTRIBUTE_NAMES =
            Set.of("analyze",
                   "next",
                   "value",
                   "values",
                   "begin",
                   "end",
                   "authorization",
                   "order",
                   "user",
                   "when");
    protected static final Set<String> FORBIDDEN_CLASS_NAMES = Set.of("constraint", "user");
    private static final Logger log = LogManager.getLogger(TopiaMetaTransformer.class);

    @SuppressWarnings("unchecked")
    public TopiaMetaTransformer() {
        setTemplateTypes(
                EntityTransformer.class,
                EntityHibernateMappingTransformer.class,
                ApplicationContextTransformer.class,
                PersistenceContextTransformer.class,
                EntityDaoTransformer.class,
                EntitySqlDescriptorGenerator.class,
                EntitySqlModelClassGenerator.class);
    }

    @Override
    protected void applyTemplates(ObjectModel model, List<? extends Template<ObjectModel>> templates, File destDir) throws IOException {
        for (Template<ObjectModel> template : templates) {
            template.setLog(getLog());
        }
        super.applyTemplates(model, templates, destDir);
    }

    protected boolean validateModel(ObjectModel model) {
        Path outputDirectory = getConfiguration().getProperty(PROP_OUTPUT_DIRECTORY, File.class).toPath().getParent().getParent();
        TagValues tagValues = new TagValues(model, "persistence", getLog());
        try {
            tagValues.load(getClassLoader(), outputDirectory, isVerbose());
        } catch (Exception e) {
            throw new IllegalStateException("Can't load tag-values", e);
        }
        boolean validationSuccess = true;
        for (ObjectModelValidator validator : getValidators(model)) {
            if (!validator.validate()) {
                for (String error : validator.getErrors()) {
                    log.warn("[VALIDATION] " + error);
                }
                // TODO brendan 29/08/14 uncomment line below to resolve #3487
                // validationSuccess = false;
            }
        }
        TopiaTemplateHelper templateHelper = new TopiaTemplateHelper(model);
        // test before all if there is some entities to generate
        List<ObjectModelClass> classes = templateHelper.getEntityClasses(model, true);
        if (classes.isEmpty()) {
            // no entity to generate, can stop safely
            log.warn("No entity to generate, " + getClass().getName() + " is skipped");
            validationSuccess = false;
        }
        return validationSuccess;
    }

    protected ImmutableSet<ObjectModelValidator> getValidators(ObjectModel model) {
        AttributeNamesValidator attributeNamesValidator = new AttributeNamesValidator(model);
        for (String sqlKeyword : FORBIDDEN_ATTRIBUTE_NAMES) {
            attributeNamesValidator.addNameAndReason(sqlKeyword, "Le nom d'attribut \"" + sqlKeyword + "\" est incompatible avec certains SGBD");
        }
        ClassNamesValidator classNamesValidator = new ClassNamesValidator(model);
        for (String sqlKeyword : FORBIDDEN_CLASS_NAMES) {
            classNamesValidator.addNameAndReason(sqlKeyword, "Le nom de classe \"" + sqlKeyword + "\" est incompatible avec certains SGBD");
        }
        TopiaJavaValidator topiaJavaValidator = new TopiaJavaValidator(model);
        TopiaRelationValidator topiaRelationValidator = new TopiaRelationValidator(model);
        return ImmutableSet.of(attributeNamesValidator, classNamesValidator, topiaJavaValidator, topiaRelationValidator);
    }
}
