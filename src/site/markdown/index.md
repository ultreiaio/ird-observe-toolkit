# ObServe ToolKit

[![Maven Central status](https://img.shields.io/maven-central/v/fr.ird.observe/toolkit.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22fr.ird.observe%22%20AND%20a%3A%22toolkit%22)
![Build Status](https://gitlab.com/ultreiaio/ird-observe-toolkit/badges/develop/pipeline.svg)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/ird-observe-toolkit/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/ird-observe-toolkit)

# Community

* [Contact](mailto:dev+ird-observe-toolkit@tchemit.fr)
