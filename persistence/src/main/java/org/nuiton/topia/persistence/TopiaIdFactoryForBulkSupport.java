package org.nuiton.topia.persistence;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A special id factory where the timestamp part is fixed and the random part is performed by a sequence (by type) plus always a random part.
 * <p>
 * Created on 09/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class TopiaIdFactoryForBulkSupport implements TopiaIdFactory {

    private static final long serialVersionUID = 1L;
    private final String timestampPart;
    private final Map<String, AtomicLong> count;

    public TopiaIdFactoryForBulkSupport(long timestamp) {
        this.timestampPart = "" + timestamp;
        this.count = new TreeMap<>();
    }

    @Override
    public String newTopiaIdFromIdPrefix(String idPrefix) {
        return newTopiaId(idPrefix, timestampPart, getRandom(idPrefix));
    }

    protected String getRandom(String type) {
        return count.computeIfAbsent(type, s -> new AtomicLong()).incrementAndGet() + generateRandom().substring(1);
    }
}


