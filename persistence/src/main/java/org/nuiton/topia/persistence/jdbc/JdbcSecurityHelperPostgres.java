package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.security.SecurityScriptHelper;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobIdsIterator;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 11/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class JdbcSecurityHelperPostgres implements JdbcSecurityHelper {

    private static final Logger log = LogManager.getLogger(JdbcSecurityHelperPostgres.class);

    private static final Function<String, String> ESCAPE_STRING = input -> "\"" + input + "\"";
    private static final String REVOKE_ON_TABLE_ALL_PATTERN = "REVOKE ALL ON %s.%s FROM %s CASCADE;";
    private static final String REVOKE_ON_LARGE_OBJECT_ALL_PATTERN = "REVOKE ALL ON LARGE OBJECT %s FROM %s CASCADE;";
    private static final String SET_ON_TABLE_OWNER_PATTERN = "ALTER TABLE %s.%s OWNER TO %s;";
    private static final String SET_ON_LARGE_OBJECT_OWNER_PATTERN = "ALTER LARGE OBJECT %s OWNER TO %s;";
    private static final String GRANT_ON_LARGE_OBJECT_READ_PATTERN = "GRANT SELECT ON LARGE OBJECT %s TO %s;";
    private static final String GRANT_ON_LARGE_OBJECT_ALL_PATTERN = "GRANT ALL ON LARGE OBJECT %s TO %s;";
    private static final String GRANT_ON_TABLE_READ_PATTERN = "GRANT SELECT ON %s.%s TO %s;";
    private static final String GRANT_ON_TABLE_ALL_PATTERN = "GRANT ALL ON %s.%s TO %s;";
    private static final String GRANT_ON_FUNCTION_PATTERN = "GRANT EXECUTE ON FUNCTION %s TO %s;";
    private static final String REVOKE_ON_SCHEMA_ALL_PATTERN = "REVOKE ALL ON SCHEMA %s FROM %s CASCADE;";
    private static final String REVOKE_ON_FUNCTIONS_PATTERN = "REVOKE EXECUTE ON FUNCTION %s FROM %s CASCADE;";
    private static final String GRANT_ON_SCHEMA_ALL_PATTERN = "GRANT USAGE ON SCHEMA %s TO %s;";
    private static final List<String> POSTGIS_TABLES = List.of("geometry_columns", "spatial_ref_sys");

    private final JdbcHelperPostgres jdbcHelper;

    public JdbcSecurityHelperPostgres(JdbcHelperPostgres jdbcHelper) {
        this.jdbcHelper = jdbcHelper;
    }

    @Override
    public void createSecurityScript(SecurityScriptHelper.SecurityScriptBuilderContext context,
                                     SqlScriptWriter sqlScriptWriter) throws IOException {
        Set<String> schemas = context.schemas;
        List<Pair<String, String>> tables = context.tables;
        List<Pair<String, String>> referentielTables = context.referentielTables;
        List<Pair<String, String>> dataWithoutReferentialTables = context.dataWithoutReferentialTables;

        Set<String> allPostgisFunctions = new LinkedHashSet<>();
        for (String postgisFunction : context.functionNamesPrefixes) {
            Set<String> postgisFunctions = jdbcHelper.newSecurityHelper().getPostgisFunctions(postgisFunction);
            allPostgisFunctions.addAll(postgisFunctions);
        }

        String administratorName = escapedName(context.administratorName);
        List<String> technicalNames = escapedNames(context.technicalNames);
        List<String> usersNames = escapedNames(context.usersNames);
        List<String> dataEntryOperatorNames = escapedNames(context.dataEntryOperatorNames);
        List<String> referentialNames = escapedNames(context.referentialNames);
        List<String> unusedNames = escapedNames(context.unusedNames);

        TopiaEntitySqlBlobIdsIterator blobIdsIterator = context.blobIdsIteratorSupplier.get();

        // revoke any right
        {
            Set<String> privateRoles = new HashSet<>();
            privateRoles.add("public");
            privateRoles.addAll(referentialNames);
            privateRoles.addAll(usersNames);
            privateRoles.addAll(dataEntryOperatorNames);
            privateRoles.addAll(technicalNames);
            privateRoles.addAll(unusedNames);

            String roles = String.join(",", privateRoles);

            addOnTablesForRole(REVOKE_ON_TABLE_ALL_PATTERN, sqlScriptWriter, tables, roles);
            addOnSchemaForRole(REVOKE_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, roles);
            addOnFunctionForRole(REVOKE_ON_FUNCTIONS_PATTERN, sqlScriptWriter, allPostgisFunctions, roles);
            addOnLargeObjectForRole(REVOKE_ON_LARGE_OBJECT_ALL_PATTERN, sqlScriptWriter, blobIdsIterator, roles);
        }

        // add owner
        addOnTablesForRole(SET_ON_TABLE_OWNER_PATTERN, sqlScriptWriter, tables, administratorName);
        addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, administratorName);
        addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, sqlScriptWriter, allPostgisFunctions, administratorName);
        addOnLargeObjectForRole(SET_ON_LARGE_OBJECT_OWNER_PATTERN, sqlScriptWriter, blobIdsIterator, administratorName);
        addOnLargeObjectForRole(GRANT_ON_LARGE_OBJECT_ALL_PATTERN, sqlScriptWriter, blobIdsIterator, administratorName);

        // add technical
        if (!technicalNames.isEmpty()) {
            String roles = String.join(",", technicalNames);
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, tables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, sqlScriptWriter, allPostgisFunctions, roles);
            addOnLargeObjectForRole(GRANT_ON_LARGE_OBJECT_ALL_PATTERN, sqlScriptWriter, blobIdsIterator, roles);
        }

        // add user
        if (!usersNames.isEmpty()) {
            String roles = String.join(",", usersNames);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, tables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, sqlScriptWriter, allPostgisFunctions, roles);
            addOnLargeObjectForRole(GRANT_ON_LARGE_OBJECT_READ_PATTERN, sqlScriptWriter, blobIdsIterator, roles);
        }

        // add dataEntryOperator
        if (!dataEntryOperatorNames.isEmpty()) {
            String roles = String.join(",", dataEntryOperatorNames);
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, dataWithoutReferentialTables, roles);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, referentielTables, roles);
            // The lastUpdateDate table must have write access (to create data)
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2724
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, List.of(context.lastUpdateTable), roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, sqlScriptWriter, allPostgisFunctions, roles);
            addOnLargeObjectForRole(GRANT_ON_LARGE_OBJECT_ALL_PATTERN, sqlScriptWriter, blobIdsIterator, roles);
        }

        // add referential
        if (!referentialNames.isEmpty()) {
            String roles = String.join(",", referentialNames);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, referentielTables, roles);
            addOnSchemaForRole(GRANT_ON_SCHEMA_ALL_PATTERN, sqlScriptWriter, schemas, roles);
            addOnSchemaForRole(GRANT_ON_FUNCTION_PATTERN, sqlScriptWriter, allPostgisFunctions, roles);
        }
    }

    @Override
    public Set<String> getTablePrivileges(String schema, String tableName) {
        String jdbcConnectionUser = jdbcHelper.getJdbcConnectionUser();
        try {
            return jdbcHelper.runOnMetadata(metaData -> {
                Set<String> tablePrivileges = new LinkedHashSet<>();
                try (ResultSet resultSet = metaData.getTablePrivileges(null, schema, tableName)) {
                    while (resultSet.next()) {
                        String security = resultSet.getString("PRIVILEGE");
                        String grantor = resultSet.getString("GRANTOR");
                        String grantee = resultSet.getString("GRANTEE");
                        // Il se peut que le login soit echappe sous la forme \"login\"
                        grantor = grantor.replaceAll("\\\\\"", "");
                        grantee = grantee.replaceAll("\\\\\"", "");
                        log.debug(String.format("(security %s) - grantee (%s)", security, grantee));
                        if (grantor.equals(jdbcConnectionUser)) {
                            log.debug("for " + tableName + " table " + grantor + '/' + security);
                            tablePrivileges.add(security);
                        } else if (grantee.equals(jdbcConnectionUser)) {
                            log.debug("for " + tableName + " table " + grantee + '/' + security);
                            tablePrivileges.add(security);
                        }
                    }
                }
                return tablePrivileges;
            });

        } catch (Exception e) {
            throw new TopiaException("Can't get postgres roles", e);
        }
    }

    @Override
    public List<Pair<String, String>> getTables(Set<String> schemas, Set<String> extraTables) {
        try {
            return jdbcHelper.runOnMetadata(data -> {
                List<Pair<String, String>> result = new ArrayList<>();
                try (ResultSet tables = data.getTables(null, null, null, new String[]{"TABLE"})) {

                    int columnCount = tables.getMetaData().getColumnCount();

                    if (log.isDebugEnabled()) {
                        StringBuilder builder = new StringBuilder();
                        builder.append("\nheader");
                        for (int i = 1; i <= columnCount; i++) {
                            String columnName = tables.getMetaData().getColumnName(i);
                            builder.append("\n [").append(i).append("] :").append(columnName);
                        }
                        log.debug(builder.toString());
                        log.debug("fetchSize : " + tables.getFetchSize());
                    }
                    while (tables.next()) {
                        String schemaName = tables.getString(2);
                        String tableName = tables.getString(3);
                        log.debug(String.format("Discover table named %s", tables));
                        if (!extraTables.contains(tableName)) {
                            if (POSTGIS_TABLES.contains(tableName)) {
                                continue;
                            }
                            if (schemaName == null || !schemas.contains(schemaName.toLowerCase())) {
                                continue;
                            }
                        }
                        log.debug(String.format("Keep table: %s", tables));
                        result.add(Pair.of(schemaName, tableName));
                    }
                }
                Collections.sort(result);
                return result;
            });
        } catch (Exception e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public Set<String> getRoles() {
        try {
            return jdbcHelper.runSelect("SELECT rolname FROM pg_catalog.pg_roles;", resultSet -> {
                Set<String> users = new LinkedHashSet<>();
                while (resultSet.next()) {
                    String name = resultSet.getString(1);
                    users.add(name);
                }
                return users;
            });
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public boolean isOwner() {
        try {
            return jdbcHelper.runSelect("SELECT pg_catalog.pg_get_userbyid(d.datdba) FROM pg_catalog.pg_database d WHERE d.datname = (SELECT current_database())", resultSet -> Objects.equals(jdbcHelper.getJdbcConnectionUser(), resultSet.getString(1)));
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public boolean isSuperUser() {
        try {
            return jdbcHelper.runSelect("SELECT usesuper FROM pg_user WHERE usename = CURRENT_USER", resultSet -> resultSet.getBoolean(1));
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }


    @Override
    public Set<String> getPostgisFunctions(String functionPattern) {
        String sql = String.format("SELECT ns.nspname::text || '.' ||  p.proname::text || '(' || oidvectortypes(p.proargtypes)::text || ')'" +
                                           " FROM pg_proc p INNER JOIN pg_namespace ns ON (p.pronamespace = ns.oid)" +
                                           " WHERE ns.nspname = 'public' AND p.proname ILIKE '%s%%';", functionPattern);
        try {
            List<String> result = jdbcHelper.runMultipleSelect(sql, resultSet -> resultSet.getString(1));
            return Set.copyOf(result);
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public JdbcHelper jdbcHelper() {
        return jdbcHelper;
    }

    private String escapedName(String name) {
        return ESCAPE_STRING.apply(name);
    }

    private List<String> escapedNames(List<String> names) {
        return names.stream().map(ESCAPE_STRING).collect(Collectors.toList());
    }

    private void addOnTablesForRole(String pattern, SqlScriptWriter builder, Iterable<Pair<String, String>> tables, String role) {
        for (Pair<String, String> t : tables) {
            builder.writeSql(String.format(pattern, t.getLeft(), t.getRight(), role));
        }
    }

    private void addOnSchemaForRole(String pattern, SqlScriptWriter builder, Set<String> schemas, String role) {
        for (String t : schemas) {
            builder.writeSql(String.format(pattern, t, role));
        }
    }

    private void addOnLargeObjectForRole(String pattern, SqlScriptWriter builder, TopiaEntitySqlBlobIdsIterator blobIdsIterator, String role) throws IOException {
        try (blobIdsIterator) {
            while (blobIdsIterator.hasNext()) {
                String blobId = blobIdsIterator.next();
                builder.writeSql(String.format(pattern, blobId, role));
            }
        }
    }

    private void addOnFunctionForRole(@SuppressWarnings("SameParameterValue") String pattern, SqlScriptWriter builder, Set<String> functions, String role) {
        for (String function : functions) {
            builder.writeSql(String.format(pattern, function, role));
        }
    }
}
