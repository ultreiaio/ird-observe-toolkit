package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.datasource.security.model.DataSourceUserRole;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.security.SecurityScriptConfiguration;
import org.nuiton.topia.persistence.security.SecurityScriptHelper;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceProvider;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;
import org.nuiton.topia.service.migration.version.TMSVersion;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created on 08/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public interface JdbcSecurityHelper {

    void createSecurityScript(SecurityScriptHelper.SecurityScriptBuilderContext context, SqlScriptWriter sqlScriptWriter) throws IOException;

    Set<String> getTablePrivileges(String schema, String tableName);

    List<Pair<String, String>> getTables(Set<String> schemas, Set<String> extraTables);

    Set<String> getRoles();

    boolean isOwner();

    boolean isSuperUser();

    Set<String> getPostgisFunctions(String functionPattern);

    JdbcHelper jdbcHelper();

    default Set<DataSourceUserDto> getUsers() {
        Set<DataSourceUserDto> users = new LinkedHashSet<>();
        String jdbcConnectionUser = jdbcHelper().getJdbcConnectionUser();
        Set<String> userNames = getRoles();
        for (String userName : userNames) {
            DataSourceUserDto user = new DataSourceUserDto();
            user.setName(userName);
            if (jdbcConnectionUser.equalsIgnoreCase(userName)) {
                user.setRole(DataSourceUserRole.ADMINISTRATOR);
            } else {
                user.setRole(DataSourceUserRole.UNUSED);
            }
            users.add(user);
        }
        return users;
    }

    default ObserveDataSourceInformation getDataSourceInformation(SecurityScriptConfiguration model, boolean localDatabase, boolean canBeEmpty) throws SQLException {

        jdbcHelper().runSelectOnString("SELECT 1;");
        boolean owner = isOwner();
        boolean superUser = isSuperUser();

        List<Permission> permissionList = new LinkedList<>();
        // Everybody can read referential
        permissionList.add(Permission.READ_REFERENTIAL);
        if (localDatabase) {
            // On local database, permissions are fixed (read-write data)
            permissionList.add(Permission.READ_DATA);
            permissionList.add(Permission.WRITE_DATA);
        }
        Version version;
        try {
            version = getVersion();
        } catch (Exception e) {
            if (!canBeEmpty) {
                throw e;
            }
            return new ObserveDataSourceInformation(
                    Permission.valueOf(permissionList),
                    owner,
                    superUser,
                    model.getMinimumMigrationVersion(),
                    null,
                    null);
        }

        if (!localDatabase) {

            // on recherche les droits de l'utilisateur sur cette base
            //TODO chemit 2010-10-28 : il vaudrait mieux utiliser des fonctions postgres adéquates qui elle sont fiables...
            //FIXME la recuperation des meta-donnees n'est pas fiable!
            //FIXME en effet, sur un simple lecteur, on voit apparaître aussi
            //FIXME des privileges INSERT ou UPDATE...
            // on pourrait utiliser une requête spécifique postgres :
            // select count(*) from information_schema.table_privileges where
            // grantee='ROLE' and table_name='trip' and
            // privilege_type='INSERT';
            // cependant cela n'est pas mieux car la requête peut ne pas être
            // exacte si l'utilisateur n'a pas les bons droits....
            // la meilleure solution serait je pense de poser 2 fonctions
            // stockées dans pg canWriteData, canWrite pour être sur du résultat
            Set<String> dataPrivileges = getTablePrivileges(model.getDataTableSchemaName(), model.getDataTableTableName());
            boolean readData = SecurityScriptHelper.canRead(dataPrivileges);
            boolean writeData = SecurityScriptHelper.canWrite(dataPrivileges);
            // recherche des droits sur le referentiel
            Set<String> referentielPrivileges = getTablePrivileges(model.getReferentialTableSchemaName(), model.getReferentialTableTableName());
            // Sur une base PG, on regarde en base ce que l'utilisateur peut lire/écrire
            boolean writeReferential = SecurityScriptHelper.canWrite(referentielPrivileges);
            if (readData) {
                permissionList.add(Permission.READ_DATA);
            }
            if (writeData) {
                permissionList.add(Permission.WRITE_DATA);
            }
            if (writeReferential) {
                permissionList.add(Permission.WRITE_REFERENTIAL);
            }
        }
        Permission permission = Permission.valueOf(permissionList);
        MigrationVersionResourceProvider migrationConfigurationProvider = MigrationVersionResourceProvider.get();
        return new ObserveDataSourceInformation(
                permission,
                owner,
                superUser,
                model.getMinimumMigrationVersion(),
                version,
                migrationConfigurationProvider.getVersionsAfter(version));
    }

    private Version getVersion() {
        if (MigrationServiceSqlHelper.isLegacy()) {
            return getLegacyVersion();
        }
        try {
            return MigrationServiceSqlHelper.DEFAULT.getVersion(jdbcHelper()).map(TMSVersion::getVersion).orElse(Version.VZERO);
        } catch (Exception e) {
            // try on legacy (migration may not been applied)
            return getLegacyVersion();
        }
    }

    private Version getLegacyVersion() {
        return MigrationServiceSqlHelper.LEGACY.getVersion(jdbcHelper()).map(TMSVersion::getVersion).orElse(Version.VZERO);
    }
}
