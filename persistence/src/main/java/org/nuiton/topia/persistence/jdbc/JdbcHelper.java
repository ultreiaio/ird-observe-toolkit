package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlFunction;
import io.ultreia.java4all.util.sql.SqlSupplier;
import io.ultreia.java4all.util.sql.SqlWork;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * @since 3.0
 */
public abstract class JdbcHelper {

    protected final JdbcConfiguration jdbcConfiguration;

    protected final SqlSupplier<Connection> connectionProvider;

    public static JdbcHelper create(JdbcConfiguration jdbcConfiguration, SqlSupplier<Connection> connectionProvider) {
        String url = jdbcConfiguration.getJdbcConnectionUrl();
        if (JdbcConfigurationBuilder.isH2Url(url)) {
            return new JdbcHelperH2(jdbcConfiguration, connectionProvider);
        }
        if (JdbcConfigurationBuilder.isPostgreSqlUrl(url)) {
            return new JdbcHelperPostgres(jdbcConfiguration, connectionProvider);
        }
        throw new IllegalStateException("No jdbcHelper for url: " + url);
    }

    public static JdbcHelper create(JdbcConfiguration jdbcConfiguration) {
        String url = jdbcConfiguration.getJdbcConnectionUrl();
        if (JdbcConfigurationBuilder.isH2Url(url)) {
            return new JdbcHelperH2(jdbcConfiguration);
        }
        if (JdbcConfigurationBuilder.isPostgreSqlUrl(url)) {
            return new JdbcHelperPostgres(jdbcConfiguration);
        }
        throw new IllegalStateException("No jdbcHelper for url: " + url);
    }

    protected JdbcHelper(JdbcConfiguration jdbcConfiguration) {
        this(jdbcConfiguration, () -> DriverManager.getConnection(
                jdbcConfiguration.getJdbcConnectionUrl(),
                jdbcConfiguration.getJdbcConnectionUser(),
                jdbcConfiguration.getJdbcConnectionPassword()));
    }

    public JdbcHelper(JdbcConfiguration jdbcConfiguration, SqlSupplier<Connection> connectionProvider) {
        this.jdbcConfiguration = Objects.requireNonNull(jdbcConfiguration);
        this.connectionProvider = Objects.requireNonNull(connectionProvider);
    }

    /**
     * @param schemaOrTable schema or table name to set to correct case
     * @return given parameter in the correct case for the database type
     */
    public abstract String applyDatabaseNameCase(String schemaOrTable);

    public abstract JdbcSecurityHelper newSecurityHelper();

    public boolean isTableExist(String schema, String tableName) throws SQLException {
        return runOnMetadata(m -> {
            try (ResultSet tables = m.getTables(null, applyDatabaseNameCase(schema), null, null)) {
                while (tables.next()) {
                    String currentTableName = tables.getString("TABLE_NAME");
                    if (currentTableName.equalsIgnoreCase(tableName)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    public boolean isSchemaExist(String schema) throws SQLException {
        return runOnMetadata(m -> {
            try (ResultSet tables = m.getSchemas(null, applyDatabaseNameCase(schema))) {
                while (tables.next()) {
                    String currentTableName = tables.getString("TABLE_SCHEM");
                    if (currentTableName.equalsIgnoreCase(schema)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    public String runSelectOnString(String sql) throws SQLException {
        return runSelect(sql, resultSet -> resultSet.getString(1));
    }

    public <T> T runSelect(String sql, SqlFunction<ResultSet, T> function) throws SQLException {
        try (Connection connection = openConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return function.apply(resultSet);
                    }
                    return null;
                }
            }
        }
    }

    public <T> List<T> runMultipleSelect(String sql, SqlFunction<ResultSet, T> function) throws SQLException {
        List<T> result = new LinkedList<>();
        try (Connection connection = openConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {

                        T row = function.apply(resultSet);
                        if (row != null) {
                            result.add(row);
                        }
                    }
                }
            }
        }
        return result;
    }

    public <T> T runOnMetadata(SqlFunction<DatabaseMetaData, T> function) throws SQLException {
        try (Connection connection = openConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            return function.apply(metaData);
        }
    }

    public void runUpdate(String sql) {
        try {
            consume(connection -> {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                    preparedStatement.executeUpdate();
                }
            });
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void runQuery(String sql) {
        try {
            consume(connection -> {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                    preparedStatement.executeQuery();
                }
            });
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void consume(SqlWork consumer) throws SQLException {
        try (Connection connection = openConnection()) {
            consumer.execute(connection);
        }
    }

    protected Connection openConnection() throws SQLException {
        return connectionProvider.get();
    }

    public String getJdbcConnectionUser() {
        return jdbcConfiguration.getJdbcConnectionUser();
    }
}
