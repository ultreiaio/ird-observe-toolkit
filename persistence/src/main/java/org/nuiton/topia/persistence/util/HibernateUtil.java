package org.nuiton.topia.persistence.util;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Table;
import org.hibernate.service.Service;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;
import org.nuiton.topia.persistence.SchemaValidationTopiaException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Properties;

/**
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public class HibernateUtil {
    private static final Logger log = LogManager.getLogger(HibernateUtil.class);

    /**
     * Test si une entite donnee correspondant a une sessionFactory existe en base.
     *
     * @param sessionFactory la sessionFactory hibernate
     * @param entityName     le nom de l'entite a tester
     * @return <tt>true</tt> si le schema de la table existe
     * @deprecated since 9.0.7, will be remove soon
     */
    @Deprecated
    public static boolean isSchemaExist(SessionFactory sessionFactory, Metadata metadata, String entityName) {
        boolean exist = false;
        try {
            PersistentClass classMapping = metadata.getEntityBinding(entityName);
            if (classMapping == null) {
                throw new IllegalArgumentException("could not find entity with name " + entityName);
            }
            Table testTable = classMapping.getTable();

            if (testTable == null) {
                throw new IllegalArgumentException("could not find entity with name " + entityName);
            }
            HibernateConnectionSupplier hibernateConnectionSupplier = new HibernateConnectionSupplier(sessionFactory);
            try (Connection connection = hibernateConnectionSupplier.get()) {
                try (ResultSet tables = connection.getMetaData().getTables(testTable.getCatalog(), testTable.getSchema(), null, null)) {
                    while (tables.next()) {
                        String currentTableName = tables.getString("TABLE_NAME");
                        if (currentTableName.equalsIgnoreCase(testTable.getName())) {
                            exist = true;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            log.error("Cant connect to database", e);
        }
        return exist;
    }

    /**
     * Test if the db associated to the given {@code sessionFactory} contains any of
     * the dealed entities.
     *
     * @param sessionFactory hibernate db sessionFactory
     * @param metaData       hibernate metadata
     * @return {@code true} if there is no schema for any of the dealed entities,
     * {@code false} otherwise.
     * @since 2.5.3
     * @deprecated since 9.0.7, will be remove soon
     */
    @Deprecated
    public static boolean isSchemaEmpty(SessionFactory sessionFactory, Metadata metaData) {
        HibernateConnectionSupplier hibernateConnectionSupplier = new HibernateConnectionSupplier(sessionFactory);
        try (Connection connection = hibernateConnectionSupplier.get()) {
            for (PersistentClass persistentClass : metaData.getEntityBindings()) {
                Table testTable = persistentClass.getTable();
                try (ResultSet tables = connection.getMetaData().getTables(testTable.getCatalog(), testTable.getSchema(), null, null)) {
                    while (tables.next()) {
                        String currentTableName = tables.getString("TABLE_NAME");
                        log.trace("Scan table: " + currentTableName);
                        if (currentTableName.equalsIgnoreCase(testTable.getName())) {
                            log.debug(String.format("Existing table found %s for entity %s, db is not empty.", testTable.getName(), persistentClass.getClassName()));
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            log.error("Cant connect to database", e);
        }
        return true;
    }

    /**
     * Builds a new instance of Hibernate SessionFactory according to the given Hibernate Configuration
     *
     * @param hibernateConfiguration an initialized Hibernate Configuration
     * @return an instance of SessionFactory
     */
    public static SessionFactory newSessionFactory(Configuration hibernateConfiguration) {
        Properties properties = hibernateConfiguration.getProperties();
        // Use the next 2 lines if bootstrap customization is needed (classloader, autoclose, ...)
        // {@see org.hibernate.boot.registry.internal.BootstrapServiceRegistryImpl)
//            BootstrapServiceRegistry bootstrap = new BootstrapServiceRegistryBuilder().build();
//            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder(bootstrap);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        StandardServiceRegistry standardServiceRegistry = builder.applySettings(properties).build();
        SessionFactory result;
        try {
            result = hibernateConfiguration.buildSessionFactory(standardServiceRegistry);
        } catch (HibernateException e) {
            SchemaValidationTopiaException.throwIfHibernateExceptionIsAboutSchemaValidation(e);
            throw e;
        }
        return result;
    }

    /**
     * Method to extract from the given Hibernate SessionFactory a working instance of StandardServiceRegistry
     * <p>
     * IMPORTANT : As much as possible, prefer using the
     * {@link #getSessionFactoryServiceRegistry(SessionFactory)} method instead of the current one because
     * the SessionFactoryServiceRegistry is a child of the StandardServiceRegistry
     * <p>
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @return the StandardServiceRegistry instance used by the given SessionFactory
     */
    public static StandardServiceRegistry getStandardServiceRegistry(SessionFactory sessionFactory) {
        // AThimel 03/04/14 The next two lines are the good way to get the StandardServiceRegistry in Hibernate 4.3
        SessionFactoryOptions sessionFactoryOptions = sessionFactory.getSessionFactoryOptions();
        return sessionFactoryOptions.getServiceRegistry();
    }

    /**
     * Method to extract from the given Hibernate SessionFactory a working instance of SessionFactoryServiceRegistry
     * <p>
     * IMPORTANT : If possible, prefer using this method instead of
     * {@link #getStandardServiceRegistry(SessionFactory)} because the SessionFactoryServiceRegistry is a
     * child of the StandardServiceRegistry
     * <p>
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @return the SessionFactoryServiceRegistry instance used by the given SessionFactory
     */
    protected static SessionFactoryServiceRegistry getSessionFactoryServiceRegistry(SessionFactory sessionFactory) {

        // AThimel 03/04/14 The next two lines are the good way to get the SessionFactoryServiceRegistry in Hibernate 4.3
        SessionFactoryImplementor sessionFactoryImplementor = (SessionFactoryImplementor) sessionFactory;

        return (SessionFactoryServiceRegistry) sessionFactoryImplementor.getServiceRegistry();
    }

    /**
     * Method to get an Hibernate service instance from a given Hibernate SessionFactory
     * <p>
     * NB: This method is static to make sure it does not depend on the current instance
     *
     * @param sessionFactory the Hibernate's SessionFactory instance
     * @param serviceClass   the expected service class
     * @param <S>            type of service
     * @return the found service instance
     * @throws org.hibernate.service.UnknownServiceException Indicates the service was not known.
     * @see ServiceRegistry#getService(Class)
     */
    public static <S extends Service> S getHibernateService(SessionFactory sessionFactory, Class<S> serviceClass) {
        // Hibernate 4.3.x : prefer using the SessionFactoryServiceRegistry method instead of StandardServiceRegistry
        // because SessionFactoryServiceRegistry is a child of the StandardServiceRegistry
        ServiceRegistry serviceRegistry = getSessionFactoryServiceRegistry(sessionFactory);
        return serviceRegistry.getService(serviceClass);
    }

    public static Metadata newMetaData(Configuration configuration, SessionFactory sessionFactory, Collection<Class<?>> persistenceClasses) {
        StandardServiceRegistry standardServiceRegistry = getStandardServiceRegistry(sessionFactory);
        MetadataSources sources = new MetadataSources(standardServiceRegistry);
        for (Class<?> persistenceClass : persistenceClasses) {
            String hbmXmlFile = persistenceClass.getName().replace('.', '/') + ".hbm.xml";
            sources.addResource(hbmXmlFile);
            configuration.addClass(persistenceClass);
        }
        MetadataBuilder metadataBuilder = sources.getMetadataBuilder();
        return metadataBuilder.build();
    }
}
