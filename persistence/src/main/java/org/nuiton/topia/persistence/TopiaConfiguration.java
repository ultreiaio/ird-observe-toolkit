package org.nuiton.topia.persistence;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;
import org.hibernate.cfg.AvailableSettings;

import java.nio.file.Path;
import java.util.Map;

/**
 * Configuration needed to instantiate a {@link TopiaApplicationContext}.
 *
 * @author Brendan Le Ny (Code Lutin)
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaConfiguration extends JdbcConfiguration {

    /**
     * Configuration directive to change topia Ids generation strategy.
     *
     * @return the id factory
     * @since 3.0
     */
    TopiaIdFactory getTopiaIdFactory();

    /**
     * Topia will automatically generate an Hibernate configuration for you based
     * on the different element of this TopiaConfiguration. However, you can tune Hibernate
     * by adding Hibernate configuration directive to this Map.
     * <p>
     * We highly recommend you to use constants in {@link AvailableSettings} as keys of the map.
     * <p>
     * You <strong>MUST NOT</strong> pass {@link AvailableSettings#HBM2DDL_AUTO} since
     * ToPIA will use validate.
     *
     * @return a map containing hibernate configuration directives that ToPIA will use when it
     * will instantiate Hibernate.
     */
    Map<String, String> getHibernateExtraConfiguration();

    Map<String, Class<? extends TopiaService>> getDeclaredServices();

    Map<String, Map<String, String>> getDeclaredServicesConfiguration();

    Path getTemporaryDirectory();

    /**
     * Do nothing but throws {@link TopiaMisconfigurationException} if there is a problem in
     * given configuration.
     *
     * @throws TopiaMisconfigurationException if any problem found in given configuration
     */
    void check();

    default boolean isH2Configuration() {
        return JdbcConfigurationBuilder.isH2Url(getJdbcConnectionUrl());
    }

    default boolean isPostgresConfiguration() {
        return JdbcConfigurationBuilder.isPostgreSqlUrl(getJdbcConnectionUrl());
    }
}
