package org.nuiton.topia.persistence.jdbc;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.security.SecurityScriptHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 11/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class JdbcSecurityHelperH2 implements JdbcSecurityHelper {

    private static final String REVOKE_ON_TABLE_ALL_PATTERN = "REVOKE ALL ON %s.%s FROM %s;";
    private static final String GRANT_ON_TABLE_READ_PATTERN = "GRANT SELECT ON %s.%s TO %s;";
    private static final String GRANT_ON_TABLE_ALL_PATTERN = "GRANT ALL ON %s.%s TO %s;";

    private final JdbcHelperH2 jdbcHelper;

    public JdbcSecurityHelperH2(JdbcHelperH2 jdbcHelper) {
        this.jdbcHelper = jdbcHelper;
    }

    @Override
    public void createSecurityScript(SecurityScriptHelper.SecurityScriptBuilderContext context, SqlScriptWriter sqlScriptWriter) {

        String administratorName = context.administratorName;
        List<String> technicalNames = context.technicalNames;
        List<String> usersNames = context.usersNames;
        List<String> referentialNames = context.referentialNames;
        List<String> dataEntryOperatorNames = context.dataEntryOperatorNames;
        List<String> unusedNames = context.unusedNames;

        List<Pair<String, String>> tables = context.tables;
        List<Pair<String, String>> referentielTables = context.referentielTables;
        List<Pair<String, String>> dataWithoutReferentialTables = context.dataWithoutReferentialTables;

        // remove any rights
        {
            Set<String> privateRoles = new HashSet<>();
//                privateRoles.add("public");
            privateRoles.addAll(referentialNames);
            privateRoles.addAll(usersNames);
            privateRoles.addAll(technicalNames);
            privateRoles.addAll(dataEntryOperatorNames);
            privateRoles.addAll(unusedNames);

            addOnTablesForRole(REVOKE_ON_TABLE_ALL_PATTERN, sqlScriptWriter, tables, privateRoles);
        }

        // add owner
        addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, tables, Set.of(administratorName));

        // add technical
        if (!technicalNames.isEmpty()) {
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, tables, technicalNames);
        }

        // add user
        if (!usersNames.isEmpty()) {
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, tables, usersNames);
        }

        // add dataEntryOperator
        if (!dataEntryOperatorNames.isEmpty()) {
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, dataWithoutReferentialTables, dataEntryOperatorNames);
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, referentielTables, dataEntryOperatorNames);
            // The lastUpdateDate table must have write access (to create data)
            // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2724
            addOnTablesForRole(GRANT_ON_TABLE_ALL_PATTERN, sqlScriptWriter, List.of(context.lastUpdateTable), dataEntryOperatorNames);
        }

        // add referential
        if (!referentialNames.isEmpty()) {
            addOnTablesForRole(GRANT_ON_TABLE_READ_PATTERN, sqlScriptWriter, referentielTables, referentialNames);
        }
    }

    private static final Logger log = LogManager.getLogger(JdbcSecurityHelperH2.class);

    @Override
    public Set<String> getTablePrivileges(String schema, String tableName) {

        String jdbcConnectionUser = jdbcHelper.getJdbcConnectionUser().toUpperCase();
        try {
            return jdbcHelper.runOnMetadata(metaData -> {
                Set<String> tablePrivileges = new LinkedHashSet<>();
                try (ResultSet resultSet = metaData.getTablePrivileges(null, schema, tableName)) {
                    while (resultSet.next()) {
                        String security = resultSet.getString("PRIVILEGE");
                        String grantor = resultSet.getString("GRANTOR");
                        String grantee = resultSet.getString("GRANTEE");
                        // Il se peut que le login soit echappe sous la forme \"login\"
                        grantor = grantor == null ? null : grantor.replaceAll("\\\\\"", "").toUpperCase();
                        grantee = grantee.replaceAll("\\\\\"", "").toUpperCase();
                        log.debug(String.format("(security %s) - grantee (%s)", security, grantee));
                        if (Objects.equals(grantor, jdbcConnectionUser)) {
                            log.debug("for " + tableName + " table " + grantor + '/' + security);
                            tablePrivileges.add(security);
                        } else if (grantee.equals(jdbcConnectionUser)) {
                            log.debug("for " + tableName + " table " + grantee + '/' + security);
                            tablePrivileges.add(security);
                        }
                    }
                }
                return tablePrivileges;
            });

        } catch (Exception e) {
            throw new TopiaException("Can't get h2 roles", e);
        }
    }

    @Override
    public Set<String> getPostgisFunctions(String functionPattern) {
        return Set.of();
    }

    @Override
    public JdbcHelper jdbcHelper() {
        return jdbcHelper;
    }

    @Override
    public List<Pair<String, String>> getTables(Set<String> schemas, Set<String> extraTables) {
        Set<String> safeSchemas = schemas.stream().map(String::toLowerCase).collect(Collectors.toSet());
        Set<String> safeExtraTables = extraTables.stream().map(String::toLowerCase).collect(Collectors.toSet());
        try {

            List<Pair<String, String>> pairs = jdbcHelper.runMultipleSelect("SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'TABLE'", resultSet -> {
                String schemaName = resultSet.getString(1);
                String tableName = resultSet.getString(2);
                if (!safeExtraTables.contains(tableName.toLowerCase())) {
                    //                        if (POSTGIS_TABLES.contains(tableName)) {
                    //                            continue;
                    //                        }
                    if (schemaName == null || !safeSchemas.contains(schemaName.toLowerCase())) {
                        return null;
                    }
                }
                return Pair.of(schemaName, tableName);
            });
            Collections.sort(pairs);
            return pairs;
        } catch (Exception e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public Set<String> getRoles() {
        try {
            List<String> roles = jdbcHelper.runMultipleSelect("SELECT name FROM INFORMATION_SCHEMA.USERS;", resultSet -> resultSet.getString(1));
            return Set.copyOf(roles);
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public boolean isOwner() {
        try {
            return jdbcHelper.runSelect("SELECT t.* FROM INFORMATION_SCHEMA.SCHEMATA t WHERE SCHEMA_NAME = 'PUBLIC' AND SCHEMA_OWNER = CURRENT_USER", r-> true);
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public boolean isSuperUser() {
        try {
            return jdbcHelper.runSelect("SELECT admin FROM INFORMATION_SCHEMA.USERS WHERE name = CURRENT_USER", resultSet -> resultSet.getBoolean(1));
        } catch (SQLException e) {
            throw new TopiaException(e);
        }
    }

    private void addOnTablesForRole(String pattern, SqlScriptWriter builder, Iterable<Pair<String, String>> tables, Collection<String> roles) {
        for (String role : roles) {
            for (Pair<String, String> t : tables) {
                builder.writeSql(String.format(pattern, t.getLeft(), t.getRight(), role));
            }
        }
    }
}
