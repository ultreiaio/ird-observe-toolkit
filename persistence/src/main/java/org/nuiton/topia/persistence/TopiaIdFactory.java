package org.nuiton.topia.persistence;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This contract represents a topiaId generation strategy.
 * <p>
 * It can be used in both ways :
 * <ul>
 * <li>generate a new topiaId from a given class;</li>
 * <li>retrieves a class or random part from a given topiaId.</li>
 * </ul>
 * <p>
 * You can change the implementation used by ToPIA by changing configuration.
 * <p>
 * {@link Serializable} must be implemented because
 * {@link TopiaConfiguration} may hold some references.
 *
 * @author Brendan Le Ny - bleny@codelutin.com
 * @author Tony Chemit - tchemit@codelutin.com
 * @see TopiaConfiguration#getTopiaIdFactory()
 * @since 3.0
 */
public interface TopiaIdFactory extends Serializable {

    String DEFAULT_SEPARATOR = "#";

    /**
     * Generates a new topiaId with given id prefix.
     *
     * @param idPrefix id prefix to set
     * @return the new topiaId
     */
    String newTopiaIdFromIdPrefix(String idPrefix);

    /**
     * Generates a new topiaId for the given entity type.
     *
     * @param entityType type of entity (must be a not null interface)
     * @param <E>        type of entity
     * @return the new topiaId for the given entity
     */
    default <E extends TopiaEntity> String newTopiaId(Class<E> entityType) {
        if (!entityType.isInterface()) {
            throw new IllegalArgumentException("Only interface is permit to create id: " + entityType);
        }
        return newTopiaIdFromEntityType(entityType.getName());
    }

    /**
     * Generates a new topiaId for the given entity type and the id parts (timestamp and random).
     *
     * @param entityType type of entity (must be a not null interface)
     * @param timestamp  timestamp part to set
     * @param randomPart random part to use
     * @param <E>        type of entity
     * @return the new topiaId for the given entity
     */
    default <E extends TopiaEntity> String newTopiaId(Class<E> entityType, String timestamp, String randomPart) {
        if (!entityType.isInterface()) {
            throw new IllegalArgumentException("Only interface is permit to create id: " + entityType);
        }
        return newTopiaId(getIdClassNamePartFromClassName(entityType.getName()), timestamp, randomPart);
    }

    /**
     * Generates a new topiaId from fully qualified entity name.
     *
     * @param entityType fully qualified entity name
     * @return the new topiaId for the given entity
     */
    default String newTopiaIdFromEntityType(String entityType) {
        return newTopiaIdFromIdPrefix(getIdClassNamePartFromClassName(entityType));
    }

    /**
     * Generates a new topiaId from the id parts (prefix and random).
     *
     * @param idPrefix      id prefix to set
     * @param timestampPart time stamp part
     * @param randomPart    random part to use
     * @return the new topiaId for the given entity
     */
    default String newTopiaId(String idPrefix, String timestampPart, String randomPart) {
        return idPrefix + getSeparator() + timestampPart + getSeparator() + randomPart;
    }

    default String getEntityFullyQualifiedName(String topiaId) {
        return getClassNameFromIdClassNamePart(Strings.substringBefore(topiaId, getSeparator()));
    }

    /**
     * To get the class name part of an id from the entity class name
     * <p>
     * By default, class name is id class name part.
     *
     * @param entityTypeName entity class name
     * @return the class name part of the id
     */
    default String getIdClassNamePartFromClassName(String entityTypeName) {
        return entityTypeName;
    }

    /**
     * To get the entity class name part from an id.
     * <p>
     * By default, class name is id class name part.
     *
     * @param classNamePart class name part from id
     * @return the class name
     */
    default String getClassNameFromIdClassNamePart(String classNamePart) {
        return classNamePart;
    }

    /**
     * @param topiaId the topiaId to inspect
     * @param <E>     type of the entity
     * @return the FQN part of the topiaId
     */
    default <E extends TopiaEntity> Class<E> getClassName(String topiaId) {
        String className = getEntityFullyQualifiedName(topiaId);
        return Objects2.forName(className);
    }

    /**
     * @param topiaId the topiaId to inspect
     * @return the random part of the topiaId
     */
    default String getRandomPart(String topiaId) {
        return Strings.substringAfter(topiaId, getSeparator());
    }

    /**
     * @return the separator between the FQN and the random part of any topiaId.
     */
    default String getSeparator() {
        return DEFAULT_SEPARATOR;
    }

    default <E extends TopiaEntity> List<String> generateId(Class<E> entityType, int number) {
        String entityTypeName = entityType.getName();
        List<String> result = new ArrayList<>(number);
        for (int i = 0; i < number; i++) {
            String id = newTopiaIdFromEntityType(entityTypeName);
            result.add(id);
        }
        return result;
    }

    /**
     * Generates a random decimal (between 0 and 1) used as random part in a id.
     *
     * @return generate random as a string
     */
    default String generateRandom() {
        double random = Math.random();
        while (Double.toString(random).contains("E-")) {
            random = Math.random();
        }
        return random + "";
    }
}
