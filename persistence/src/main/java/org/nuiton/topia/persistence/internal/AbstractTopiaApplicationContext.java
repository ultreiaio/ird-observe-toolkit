package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.services.service.data.MissingReferentialRequest;
import fr.ird.observe.services.service.data.MissingReferentialResult;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.persistence.internal.support.TopiaServiceSupportImpl;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.jdbc.JdbcHelperH2;
import org.nuiton.topia.persistence.jdbc.JdbcHelperPostgres;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;
import org.nuiton.topia.persistence.util.HibernateConnectionSupplier;
import org.nuiton.topia.service.migration.TopiaMigrationService;
import org.nuiton.topia.service.sql.TopiaSqlService;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobIdsIterator;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * The application context is the main class in ToPIA usage. This class is a kind of equivalent of the RootTopiaContext.
 * It contains only high level methods and new contexts creation (transaction begin, ...). This class has to be extended
 * by the user, even if some default one could be automatically generated.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public abstract class AbstractTopiaApplicationContext<K extends TopiaPersistenceContext> implements TopiaApplicationContext<K> {
    protected static final TimeLog TIME_LOG = new TimeLog(AbstractTopiaApplicationContext.class, 500, 1000);
    private static final Logger log = LogManager.getLogger(AbstractTopiaApplicationContext.class);

    public static final String MIGRATION_SERVICE_NAME = "migration";
    public static final String SQL_SERVICE_NAME = "sql";

    protected TopiaConfiguration configuration;

    protected TopiaServiceSupport topiaServiceSupport;

    protected HibernateProvider hibernateProvider;

    /**
     * Dao mapping.
     */
    private transient Map<String, Supplier<TopiaDao<?>>> daoMapping;
    /**
     * Unique id associated with this application context.
     */
    protected final String authenticationToken = UUID.randomUUID().toString();
    /**
     * Set to {@code true} when at least one transaction has been done.
     */
    protected boolean open;

    protected boolean closed = false;

    /**
     * A set of known initialized opened persistence contexts. This set is mainly used to trigger shutdown on these
     * persistence contexts when the application context is closed.
     */
    protected Set<TopiaPersistenceContext> persistenceContexts = new LinkedHashSet<>();//Collections.newSetFromMap(new WeakHashMap<>());

    public AbstractTopiaApplicationContext(TopiaConfiguration configuration) {
        this.configuration = configuration;
        init();
    }

    public Map<String, Supplier<TopiaDao<?>>> getDaoMapping() {
        if (daoMapping == null) {
            daoMapping = createDaoMapping();
        }
        return daoMapping;
    }

    protected abstract Map<String, Supplier<TopiaDao<?>>> createDaoMapping();

    @Override
    public final String getAuthenticationToken() {
        return authenticationToken;
    }

    protected void init() {

        configuration.check();

        // First initialize all the services
        initServices();

        // AThimel 14/06/14 Make sure this method is called AFTER ToPIA's schema init, otherwise Hibernate may have created the schema itself
        // The next line will trigger the Configuration#buildMappings() method which really initializes Hibernate
        getHibernateProvider().getHibernateConfiguration();
    }

    protected void initServices() {
        TopiaServiceSupportImpl topiaServiceSupportImpl = new TopiaServiceSupportImpl();
        this.topiaServiceSupport = topiaServiceSupportImpl;
        topiaServiceSupportImpl.initServices(this);
    }

    @Override
    public final TopiaSqlService getSqlService() {
        return getServices(TopiaSqlService.class).get(SQL_SERVICE_NAME);
    }

    @Override
    public TopiaMigrationService getMigrationService() {
        return getServices(TopiaMigrationService.class).get(MIGRATION_SERVICE_NAME);
    }

    @Override
    public final TopiaConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public final Map<String, TopiaService> getServices() {
        return topiaServiceSupport.getServices();
    }

    @Override
    public final <T extends TopiaService> Map<String, T> getServices(Class<T> interfaceService) {
        return topiaServiceSupport.getServices(interfaceService);
    }

    @Override
    public void close() {

        // Throw exception if context is already closed
        Preconditions.checkState(!closed, "TopiaApplicationContext was already closed");

        log.debug("will close " + this);

        TopiaConfiguration topiaConfiguration = getConfiguration();
        if (topiaConfiguration.isH2Configuration()) {
            log.debug("Shutdown h2 database");

            //FIXME Should we still do this ?
            // Fermer proprement la base
            // 2022-08-11 We can't use the newJdbcHelper() method, since it will raise an error, Db is already closed...
            JdbcHelper.create(getConfiguration()).runUpdate("SHUTDOWN COMPACT;");
        }

        // Iterate over the children PersistenceContexts and close them
        for (TopiaPersistenceContext persistenceContext : List.copyOf(persistenceContexts)) {
            if (persistenceContext == null) {
                log.warn("null TopiaPersistenceContext found in #persistenceContexts");
            } else {
                // Avoid to have exception from checkNotClosed method on child
                try {
                    if (!persistenceContext.isClosed()) {
                        persistenceContext.close();
                    }
                } catch (Exception eee) {
                    // Don't let any exception stop the application closing
                    log.warn("unable to close TopiaPersistenceContext", eee);
                }
            }
        }

        hibernateProvider.close();

        // call TopiaService#close on all services
        for (TopiaService topiaService : getServices().values()) {
            topiaService.close();
        }

        closed = true;
        log.debug(this + " closed");
    }

    @Override
    public final List<String> getSchemaNames() {
        return getSqlService().getModel().getSchemaNames();
    }

    @Override
    public final TopiaEntitySqlBlobIdsIterator newBlobIdsIterator(Path blobIdsPath) {
        return new TopiaEntitySqlBlobIdsIterator(getBlobModel(), blobIdsPath, newJdbcHelper());
    }

    @Override
    public final boolean isClosed() {
        return closed;
    }

    @Override
    public final boolean isOpened() {
        return !isClosed();
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractTopiaApplicationContext)) return false;
        AbstractTopiaApplicationContext<?> that = (AbstractTopiaApplicationContext<?>) o;
        return Objects.equals(authenticationToken, that.authenticationToken);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(authenticationToken);
    }

    @Override
    public final void executeSqlStatements(SqlScript content) {
        try {
            SqlScriptReader scriptLocation = content.getLocation();
            try (K persistenceContext = newPersistenceContext()) {
                persistenceContext.executeSqlScript(scriptLocation);
                //FIXME-2022-08-22 this is a heart-break to force a commit, review this
                persistenceContext.commit();
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new TopiaException(e);
        }
    }

    @Override
    public final JdbcHelper newJdbcHelper() {
        HibernateConnectionSupplier hibernateConnectionSupplier = new HibernateConnectionSupplier(getHibernateProvider().getSessionFactory());
        return JdbcHelper.create(getConfiguration(), hibernateConnectionSupplier);
    }

    @Override
    public final JdbcHelperH2 newJdbcHelperH2() {
        HibernateConnectionSupplier hibernateConnectionSupplier = new HibernateConnectionSupplier(getHibernateProvider().getSessionFactory());
        return new JdbcHelperH2(getConfiguration(), hibernateConnectionSupplier);
    }

    @Override
    public final JdbcHelperPostgres newJdbcHelperPostgres() {
        HibernateConnectionSupplier hibernateConnectionSupplier = new HibernateConnectionSupplier(getHibernateProvider().getSessionFactory());
        return new JdbcHelperPostgres(getConfiguration(), hibernateConnectionSupplier);
    }

    @Override
    public final <E extends RootOpenableEntity> MissingReferentialResult computeMissingReferential(ServiceContext context, Class<E> entityType, MissingReferentialRequest request) {

        Path temporaryDirectoryRoot = context.getTemporaryDirectoryRoot();
        //FIXME Move this to persistence context
        long t0 = TimeLog.getTime();

        Map<Class<? extends ReferentialDto>, Set<String>> existingIds = request.getCentralSourceReferential();
        String[] dataIds = request.getDataIds();
        ReferentialIds requiredIds = getReferentialIds(entityType, dataIds);

        Map<Class<? extends ReferentialDto>, Set<String>> missingIds = requiredIds.without(existingIds);
        TIME_LOG.log(t0, "Compute missing referential", String.format("for data: %s", Arrays.asList(dataIds)));
        if (missingIds.isEmpty()) {
            return null;
        }
        OneSideSqlRequest sqlRequest = OneSideSqlRequest.builder().addTasks(SynchronizeTaskType.ADD, missingIds).build();
        OneSideSqlResult sqlResult = newOneSideSqlResultBuilder(context, context.now()).build(sqlRequest);

        Path scriptPath = temporaryDirectoryRoot.resolve(String.format("%s-%d.sql", getClass().getSimpleName(), System.nanoTime()));
        log.info(String.format("Generate script at: %s", scriptPath));

        Optional<SqlScript> topiaSqlScript = sqlResult.toSqlScript(scriptPath, sqlResult);
        return MissingReferentialResult.of(missingIds, topiaSqlScript.orElseThrow());
    }

    public final boolean isOpen() {
        return isOpened() && open;
    }

    public final void setOpen(boolean open) {
        this.open = open;
    }

    protected final HibernateProvider getHibernateProvider() {
        if (hibernateProvider == null) {
            List<Class<?>> persistenceClasses = getPersistenceClasses();
            hibernateProvider = new HibernateProvider(getConfiguration(), topiaServiceSupport, persistenceClasses);
        }
        return hibernateProvider;
    }

    protected final void registerPersistenceContext(TopiaPersistenceContext persistenceContext) {
        persistenceContexts.add(persistenceContext);
    }

    protected final void unregisterPersistenceContext(TopiaPersistenceContext persistenceContext) {
        persistenceContexts.add(persistenceContext);
    }
}
