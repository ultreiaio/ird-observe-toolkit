package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Parameter-object design pattern for the {@link AbstractTopiaApplicationContext}
 * constructor.
 * <p>
 * This parameter-object is useful because we can add/change/remove parameters without changing
 * the constructor signature which is necessary to ensure backward compatibility with XxxPersistenceContext
 * overridden by the user.
 */
public class AbstractTopiaPersistenceContextConstructorParameter {

    private final TopiaConfiguration configuration;
    protected final HibernateProvider hibernateProvider;
    protected final TopiaIdFactory topiaIdFactory;
    protected final Map<String, Supplier<TopiaDao<?>>> daoMapping;

    /**
     * @param configuration     topia configuration
     * @param hibernateProvider holds the Hibernate configuration and session factory
     * @param topiaIdFactory    the TopiaIdFactory instance created according to the application's configuration
     * @param daoMapping        daoMapping
     */
    public AbstractTopiaPersistenceContextConstructorParameter(TopiaConfiguration configuration,
                                                               HibernateProvider hibernateProvider,
                                                               TopiaIdFactory topiaIdFactory,
                                                               Map<String, Supplier<TopiaDao<?>>> daoMapping) {
        this.configuration = configuration;
        this.hibernateProvider = hibernateProvider;
        this.topiaIdFactory = topiaIdFactory;
        this.daoMapping = daoMapping;
    }

    public HibernateProvider getHibernateProvider() {
        return hibernateProvider;
    }

    public TopiaIdFactory getTopiaIdFactory() {
        return topiaIdFactory;
    }

    public Map<String, Supplier<TopiaDao<?>>> getDaoMapping() {
        return daoMapping;
    }

    public TopiaConfiguration getConfiguration() {
        return configuration;
    }
}
