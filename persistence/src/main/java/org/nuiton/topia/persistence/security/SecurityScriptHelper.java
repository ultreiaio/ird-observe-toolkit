package org.nuiton.topia.persistence.security;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import fr.ird.observe.datasource.security.model.DataSourceUserRole;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.jdbc.JdbcSecurityHelper;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobIdsIterator;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public class SecurityScriptHelper {

    public static final Set<String> WRITE_PRIVILEGES = Set.of("DELETE", "UPDATE", "INSERT");

    private static final Set<String> FUNCTION_NAMES_PREFIXES = new LinkedHashSet<>(List.of("ST_MakePoint",
                                                                                           "ST_SetSRID",
                                                                                           "sync_",
                                                                                           "tr_sync",
                                                                                           "ot_enhanced_school_type",
                                                                                           "observe_"));
    private static final Logger log = LogManager.getLogger(SecurityScriptHelper.class);
    private final JdbcSecurityHelper securityHelper;
    private final Path temporaryDirectory;
    private final TopiaApplicationContext<?> applicationContext;

    public static class SecurityScriptBuilderContext {

        private static final Logger log = LogManager.getLogger(SecurityScriptBuilderContext.class);

        public final List<Pair<String, String>> tables;
        public final List<Pair<String, String>> referentielTables;
        public final List<Pair<String, String>> dataWithoutReferentialTables;
        public final Pair<String, String> lastUpdateTable;
        public final String administratorName;
        public final List<String> technicalNames;
        public final List<String> usersNames;
        public final List<String> referentialNames;
        public final List<String> dataEntryOperatorNames;
        public final List<String> unusedNames;
        public final Set<String> schemas;
        public final Set<String> functionNamesPrefixes;
        public final Supplier<TopiaEntitySqlBlobIdsIterator> blobIdsIteratorSupplier;

        SecurityScriptBuilderContext(JdbcSecurityHelper jdbcSecurityHelper,
                                     TopiaEntitySqlDescriptors sqlDescriptors,
                                     Set<String> schemas,
                                     Set<String> extraTables,
                                     Set<String> functionNamesPrefixes,
                                     Set<DataSourceUserDto> users,
                                     Supplier<TopiaEntitySqlBlobIdsIterator> blobIdsIteratorSupplier) {
            JdbcHelper jdbcHelper = jdbcSecurityHelper.jdbcHelper();
            this.schemas = schemas.stream().map(jdbcHelper::applyDatabaseNameCase).collect(Collectors.toCollection(LinkedHashSet::new));
            this.functionNamesPrefixes = functionNamesPrefixes;
            this.blobIdsIteratorSupplier = blobIdsIteratorSupplier;

            Set<String> safeExtraTables = extraTables.stream().map(jdbcHelper::applyDatabaseNameCase).collect(Collectors.toCollection(LinkedHashSet::new));
            tables = jdbcSecurityHelper.getTables(this.schemas, safeExtraTables);

            administratorName = getUserNamesByRole(users, DataSourceUserRole.ADMINISTRATOR).get(0);
            technicalNames = getUserNamesByRole(users, DataSourceUserRole.TECHNICAL);
            usersNames = getUserNamesByRole(users, DataSourceUserRole.USER);
            referentialNames = getUserNamesByRole(users, DataSourceUserRole.REFERENTIAL);
            dataEntryOperatorNames = getUserNamesByRole(users, DataSourceUserRole.DATA_ENTRY_OPERATOR);
            unusedNames = getUserNamesByRole(users, DataSourceUserRole.UNUSED);

            if (log.isInfoEnabled()) {
                log.info(String.format("Will apply security on %d table(s).", tables.size()));
                log.info(" - administrator       : " + administratorName);
                log.info(" - technical           : " + technicalNames);
                log.info(" - user                : " + usersNames);
                log.info(" - referential         : " + referentialNames);
                log.info(" - dataEntryOperator   : " + dataEntryOperatorNames);
            }

            referentielTables = getReferentielTables(sqlDescriptors, tables, extraTables);
            lastUpdateTable = Pair.of(jdbcHelper.applyDatabaseNameCase("common"),jdbcHelper.applyDatabaseNameCase("lastUpdateDate"));
            dataWithoutReferentialTables = new ArrayList<>(tables);
            dataWithoutReferentialTables.removeAll(referentielTables);
        }

        private List<String> getUserNamesByRole(Set<DataSourceUserDto> users, DataSourceUserRole role) {
            return users.stream().filter(u -> u.getRole() == role).map(DataSourceUserDto::getName).collect(Collectors.toList());
        }

        private List<Pair<String, String>> getReferentielTables(TopiaEntitySqlDescriptors sqlDescriptors, Collection<Pair<String, String>> tables, Set<String> extraTables) {

            List<Pair<String, String>> result = sqlDescriptors.getTables(tables, extraTables);
            if (log.isInfoEnabled()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Detected ").append(result.size()).append(" referentiel tables.");
                for (Pair<String, String> s : result) {
                    sb.append("\n - ").append(s);
                }
                List<Pair<String, String>> dataTables = new ArrayList<>(tables);
                dataTables.removeAll(result);

                sb.append("\nDetected ").append(dataTables.size()).append(" data tables.");
                for (Pair<String, String> s : dataTables) {
                    sb.append("\n - ").append(s);
                }
                log.info(sb.toString());
            }
            return result;
        }
    }

    public static boolean canWrite(Set<String> privileges) {
        return privileges != null && privileges.containsAll(WRITE_PRIVILEGES);
    }

    public static boolean canRead(Set<String> privileges) {
        // seul les utilisateurs avec au moins un droit sur les donnes observer peut les lire
        return privileges != null && !privileges.isEmpty();
    }

    public SecurityScriptHelper(TopiaApplicationContext<?> applicationContext) {
        this.applicationContext = Objects.requireNonNull(applicationContext);
        this.securityHelper = applicationContext.newJdbcHelper().newSecurityHelper();
        this.temporaryDirectory = applicationContext.getConfiguration().getTemporaryDirectory();
    }

    public void applySecurity(Set<DataSourceUserDto> users) {
        if (users == null) {
            throw new NullPointerException("users can not be null");
        }
        Path scriptPath;
        try {
            scriptPath = Files.createTempFile(temporaryDirectory, "topia-sql-script-", ".sql");
        } catch (IOException e) {
            throw new IllegalStateException("Can't create temporary path", e);
        }

        try {
            createSecurityScript(scriptPath, users);
        } catch (IOException e) {
            throw new TopiaException(String.format("Can't generate security script at %s", scriptPath), e);
        }
        try {
            securityHelper.jdbcHelper().consume(SqlScriptConsumer.builder(scriptPath).commit().build());
        } catch (SQLException e) {
            throw new TopiaException(String.format("Can't consume security script at %s", scriptPath), e);
        }
    }

    protected void createSecurityScript(Path scriptPath, Set<DataSourceUserDto> users) throws IOException {
        Set<String> schemas = new LinkedHashSet<>();
        MigrationServiceSqlHelper migrationServiceSqlHelper = MigrationServiceSqlHelper.CURRENT;
        // We need to add this schema, postgis function are in it
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2757
        schemas.add("public");
        schemas.add(migrationServiceSqlHelper.schemaName());
        schemas.addAll(applicationContext.getSchemaNames());
        Path blobIdsPath = scriptPath.getParent().resolve(scriptPath.toFile().getName().replace(".sql", ".blob-ids"));
        try (SqlScriptWriter sqlScriptWriter = SqlScriptWriter.of(scriptPath)) {
            SecurityScriptBuilderContext context = new SecurityScriptBuilderContext(securityHelper,
                                                                                    applicationContext.getModel().getReplicationOrderWithStandaloneDescriptors(),
                                                                                    schemas,
                                                                                    Set.of(migrationServiceSqlHelper.tableName()),
                                                                                    FUNCTION_NAMES_PREFIXES,
                                                                                    users,
                                                                                    () -> applicationContext.newBlobIdsIterator(blobIdsPath));
            securityHelper.createSecurityScript(context, sqlScriptWriter);
            log.info(String.format("Generate security script %d statements(s) at %s", sqlScriptWriter.getStatementCount(), scriptPath));
        }
    }

    public Set<DataSourceUserDto> getUsers() {
        return securityHelper.getUsers();
    }
}
