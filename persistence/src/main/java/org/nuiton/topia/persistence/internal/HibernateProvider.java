package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.DB2Dialect;
import org.hibernate.dialect.DerbyTenSevenDialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgreSQL9Dialect;
import org.hibernate.dialect.SQLServer2012Dialect;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.Stoppable;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaMisconfigurationException;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;
import org.nuiton.topia.persistence.util.HibernateUtil;

import java.util.Properties;
import java.util.Set;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class HibernateProvider {

    private static final Logger log = LogManager.getLogger(HibernateProvider.class);

    protected TopiaConfiguration topiaConfiguration;
    protected TopiaServiceSupport topiaServiceSupport;
    protected Configuration hibernateConfiguration;
    protected SessionFactory hibernateSessionFactory;
    /**
     * List of persistent classes (TopiaEntity or not)
     */
    protected Set<Class<?>> persistenceClasses;
    protected Metadata metaData;

    public HibernateProvider(TopiaConfiguration topiaConfiguration,
                             TopiaServiceSupport topiaServiceSupport,
                             Iterable<Class<?>> persistenceClasses) {
        this.topiaConfiguration = topiaConfiguration;
        this.topiaServiceSupport = topiaServiceSupport;
        this.persistenceClasses = ImmutableSet.copyOf(persistenceClasses);
    }

    /**
     * Get the current Hibernate Configuration. The Configuration instance is lazy-initialized using the
     * {@link #newHibernateConfiguration()} method. The returned instance is always initialized and mapping are built.
     *
     * @return the Hibernate Configuration instance with built mappings.
     */
    public Configuration getHibernateConfiguration() {
        if (hibernateConfiguration == null) {
            hibernateConfiguration = newHibernateConfiguration();
        }
        return hibernateConfiguration;
    }

    /**
     * Creates a new Configuration instance. The instance is create but mappings are not built yet to avoid unwanted
     * database access.
     *
     * @return a new Hibernate Configuration instance without built mappings.
     */
    public Configuration newHibernateConfiguration() {

        Configuration newHibernateConfiguration = new Configuration();

        for (Class<?> entityClass : persistenceClasses) {
            newHibernateConfiguration.addClass(entityClass);
        }

        Properties properties = new Properties();

        // JDBC
        properties.put(AvailableSettings.URL, topiaConfiguration.getJdbcConnectionUrl());
        properties.put(AvailableSettings.DRIVER, topiaConfiguration.getJdbcDriverClass().getName());
        properties.put(AvailableSettings.USER, topiaConfiguration.getJdbcConnectionUser());
        properties.put(AvailableSettings.PASS, topiaConfiguration.getJdbcConnectionPassword());

        // dialect
        properties.put(AvailableSettings.DIALECT, getHibernateDialect(topiaConfiguration));

        // user specific configuration
        properties.putAll(topiaConfiguration.getHibernateExtraConfiguration());

        newHibernateConfiguration.setProperties(properties);
        return newHibernateConfiguration;
    }

    /**
     * Get Hibernate {@link org.hibernate.dialect.Dialect} to use for given {@link TopiaConfiguration}.
     * <p>
     * Prefer user defined dialect over dialect guessed by ToPIA;
     * Warn user if dialect declared seems wrong (H2 dialect for a PostgreSQL database)
     *
     * @throws TopiaMisconfigurationException if user must add dialect to its configuration (because it can not be guessed)
     */
    public static String getHibernateDialect(TopiaConfiguration topiaConfiguration) {
        String jdbcConnectionUrl = topiaConfiguration.getJdbcConnectionUrl();
        String guessedDialect = guessHibernateDialect(jdbcConnectionUrl);
        String userDefinedDialect = topiaConfiguration.getHibernateExtraConfiguration().get(AvailableSettings.DIALECT);
        String dialect;
        if (guessedDialect == null) {
            if (userDefinedDialect == null) {
                String message = String.format(
                        "unable to guess Hibernate dialect to use for JDBC URL %s please patch ToPIA or configure Hibernate manually using %s",
                        jdbcConnectionUrl,
                        AvailableSettings.DIALECT);
                throw new TopiaMisconfigurationException(message, topiaConfiguration);
            } else {
                dialect = userDefinedDialect;
            }
        } else {
            if (userDefinedDialect == null) {
                dialect = guessedDialect;
            } else {
                dialect = userDefinedDialect;
                if (guessedDialect.equals(userDefinedDialect)) {
                    log.info("configuration defined hibernate dialect " + userDefinedDialect + " but ToPIA could have guessed it (you can remove the configuration directive safely)");
                } else {
                    log.warn("not sure if " + dialect + " is suitable for " + jdbcConnectionUrl);
                }
            }
        }
        return dialect;
    }

    /**
     * TODO Hibernate can do a much better job at guessing the dialect by calling JDBC and discovering the actual DBMS version and the dialect to use.
     */
    public static String guessHibernateDialect(String jdbcConnectionUrl) {
        String guessedHibernateDialect = null;

        // DB2
        if (JdbcConfigurationBuilder.isDb2Url(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=DB2")) {
            guessedHibernateDialect = DB2Dialect.class.getName();

            // Derby
        } else if (JdbcConfigurationBuilder.isDerbyUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=Derby")) {
            guessedHibernateDialect = DerbyTenSevenDialect.class.getName();

            // HSQLDB
        } else if (JdbcConfigurationBuilder.isHsqlDbUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=HSQLDB")) {
            guessedHibernateDialect = HSQLDialect.class.getName();

            // MySQL
        } else if (JdbcConfigurationBuilder.isMysqlUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isMariaDbUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isGoogleAppEngineUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=MySQL")) {
            guessedHibernateDialect = MySQL5Dialect.class.getName();

            // Oracle
        } else if (JdbcConfigurationBuilder.isOracleUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=Oracle")) {
            guessedHibernateDialect = Oracle10gDialect.class.getName();

            // PostgreSQL
        } else if (JdbcConfigurationBuilder.isPostgreSqlUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=PostgreSQL")) {
            guessedHibernateDialect = PostgreSQL9Dialect.class.getName();

            // MS SQLServer
        } else if (JdbcConfigurationBuilder.isJtdsUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isSqlServerUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl) && jdbcConnectionUrl.contains("MODE=MSSQLServer")) {
            guessedHibernateDialect = SQLServer2012Dialect.class.getName();

            // SQLite
        } else if (JdbcConfigurationBuilder.isSqliteUrl(jdbcConnectionUrl)
                || JdbcConfigurationBuilder.isSqlDroidUrl(jdbcConnectionUrl)) {
            guessedHibernateDialect = null;

            // H2
        } else if (JdbcConfigurationBuilder.isH2Url(jdbcConnectionUrl)) {
            guessedHibernateDialect = H2Dialect.class.getName();

        } else {
            log.warn(String.format("unable to guess Hibernate dialect for JDBC URL %s. Please, submit a patch!", jdbcConnectionUrl));
        }
        return guessedHibernateDialect;
    }

    public void close() {
        metaData = null;
        if (hibernateSessionFactory != null) {
            Preconditions.checkState(!hibernateSessionFactory.isClosed());
            // close connection provider if possible (http://nuiton.org/issues/2757)
            ConnectionProvider service = HibernateUtil.getHibernateService(hibernateSessionFactory, ConnectionProvider.class);
            if (service instanceof Stoppable) {
                Stoppable stoppable = (Stoppable) service;
                stoppable.stop();
            }
            hibernateSessionFactory.close();
        }
    }

    public SessionFactory getSessionFactory() {
        if (hibernateSessionFactory == null) {
            Configuration effectiveHibernateConfiguration = getHibernateConfiguration();
            hibernateSessionFactory = HibernateUtil.newSessionFactory(effectiveHibernateConfiguration);
        }
        return hibernateSessionFactory;
    }

    public Metadata getMetaData() {
        if (metaData == null) {
            metaData = HibernateUtil.newMetaData(getHibernateConfiguration(), getSessionFactory(), persistenceClasses);
        }
        return metaData;
    }
}
