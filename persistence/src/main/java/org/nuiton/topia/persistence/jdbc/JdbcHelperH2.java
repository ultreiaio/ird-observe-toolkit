package org.nuiton.topia.persistence.jdbc;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import io.ultreia.java4all.util.sql.SqlSupplier;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import org.h2.util.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;

public class JdbcHelperH2 extends JdbcHelper {
    public static final String CREATE_BLOB_TABLE = "CREATE TABLE IF NOT EXISTS SYSTEM_LOB_STREAM(ID INT NOT NULL, PART INT NOT NULL, CDATA VARCHAR, BDATA BINARY, PRIMARY KEY (ID, PART));\n" +
            "CREATE ALIAS IF NOT EXISTS SYSTEM_COMBINE_CLOB FOR \"org.h2.command.dml.ScriptCommand.combineClob\";\n" +
            "CREATE ALIAS IF NOT EXISTS SYSTEM_COMBINE_BLOB FOR \"org.h2.command.dml.ScriptCommand.combineBlob\";";
    public static final String INSERT_BLOB = "INSERT INTO SYSTEM_LOB_STREAM VALUES(%s, %s, NULL, '%s');";
    public static final String UPDATE_BLOB = "UPDATE %s SET %s = SYSTEM_COMBINE_BLOB(%s) WHERE topiaId = '%s';";

    public JdbcHelperH2(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public JdbcHelperH2(JdbcConfiguration jdbcConfiguration, SqlSupplier<Connection> connectionProvider) {
        super(jdbcConfiguration, connectionProvider);
    }

    @Override
    public JdbcSecurityHelper newSecurityHelper() {
        return new JdbcSecurityHelperH2(this);
    }

    @Override
    public String applyDatabaseNameCase(String schemaOrTable) {
        return schemaOrTable.toUpperCase();
    }

    /**
     * Backup database in gzip compressed file.
     *
     * @param file     file to write backup
     * @param compress if true then use gzip to compress file
     */
    public void backup(File file, boolean compress) {
        String options = "";
        if (compress) {
            options += " COMPRESSION GZIP";
        }
        runQuery("SCRIPT NOPASSWORDS NOSETTINGS BLOCKSIZE 2048 TO '" + file.getAbsolutePath() + "'" + options);
    }

    /**
     * Read database from gzip compressed file
     *
     * @param file the source file to use for restore
     */
    public void restore(File file) {
        String options = "";
        try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
            in.mark(2);
            // read header to see if is compressed file
            int b = in.read();
            // redundant cast : int magic = ((int) in.read() << 8) | b;
            int magic = in.read() << 8 | b;
            in.reset();
            if (magic == GZIPInputStream.GZIP_MAGIC) {
                options += " COMPRESSION GZIP";
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        options += " CHARSET 'UTF8'";
        runUpdate("RUNSCRIPT FROM '" + file.getAbsolutePath() + "'" + options);
    }

    public void clear(boolean dropDatabase) {
        String sql = "DROP ALL OBJECTS";
        if (dropDatabase) {
            sql += " DELETE FILES";
        }
        runUpdate(sql);
    }

    public void copy(SqlScript reader, SqlScriptWriter scriptWriter) throws IOException {
        try (SqlScriptReader scriptReader = reader.getLocation()) {
            scriptWriter.writeScript(scriptReader);
        }
        Set<BlobsContainer> blobsContainers = reader.getBlobsContainers();
        storeBlobs(blobsContainers, scriptWriter);
    }

    public void storeBlobs(Set<BlobsContainer> blobsContainers, SqlScriptWriter scriptWriter) {
        if (blobsContainers.isEmpty()) {
            return;
        }
        // Always sort blobsContainer to make deterministic generated sql code
        List<BlobsContainer> sortedBlobsContainers = new ArrayList<>(blobsContainers);
        sortedBlobsContainers.sort(Comparator.comparing(b -> b.getTableName() + "." + b.getColumnName()));
        scriptWriter.writeSql(CREATE_BLOB_TABLE);
        final AtomicInteger idCount = new AtomicInteger();
        for (BlobsContainer blobsContainer : sortedBlobsContainers) {
            String tableName = blobsContainer.getTableName();
            String columnName = blobsContainer.getColumnName();
            for (Map.Entry<String, byte[]> entry : new TreeMap<>(blobsContainer.getBlobsById()).entrySet()) {
                String id = entry.getKey();
                AtomicInteger subIdCount = new AtomicInteger();
                String content = StringUtils.convertBytesToHex(entry.getValue());
                Splitter.fixedLength(4096).split(content).forEach(
                        sqlPart -> {
                            String insertSql = String.format(INSERT_BLOB, idCount.get(), subIdCount.getAndIncrement(), sqlPart);
                            scriptWriter.writeSql(insertSql);
                        });
                String updateSql = String.format(UPDATE_BLOB, tableName, columnName, idCount.getAndIncrement(), id);
                scriptWriter.writeSql(updateSql);
            }
        }
    }
}
