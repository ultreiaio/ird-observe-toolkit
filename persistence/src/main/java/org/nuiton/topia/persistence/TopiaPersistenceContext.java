package org.nuiton.topia.persistence;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.io.Closeable;

/**
 * This contract represents a persistence context, which lifecycle is linked to the transaction lifecycle.
 *
 * <p>It extends contracts such as :</p>
 * <ul>
 * <li>{@link TopiaTransaction} - to deal with transaction lifecycle</li>
 * <li>{@link TopiaDaoSupplier} - to let user get any Dao instance</li>
 * </ul>
 *
 * <p>In addition to these contracts, there is some other methods :</p>
 * <ul>
 * <li>Generic entity find</li>
 * <li>DAO factory</li>
 * <li>Schema management</li>
 * </ul>
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 3.0
 */
public interface TopiaPersistenceContext extends TopiaDaoSupplier, TopiaTransaction, Closeable {

    /**
     * Retrieve {@link TopiaEntity} using its unique {@code topiaId}.
     *
     * @param topiaId unique identifier of the entity in all the application.
     * @param <E>     type of entity
     * @return the entity found or null
     */
    <E extends TopiaEntity> E findByTopiaId(String topiaId);

    /**
     * Get the currently configured {@link TopiaIdFactory}.
     *
     * @return the {@link TopiaIdFactory} in use
     * @see TopiaIdFactory
     * @since 3.0
     */
    TopiaIdFactory getTopiaIdFactory();

    TopiaSqlSupport getSqlSupport();

    /**
     * Tells if the context is closed
     *
     * @return {@code true} if the context is closed, {@code false} otherwise
     */
    boolean isClosed();

    /**
     * Closes the PersistenceContext.
     * <p>
     * After calling close, it's impossible to make any operation on the database.
     */
    @Override
    void close();

    void executeSqlScript(SqlScript content);

    void executeSqlScript(SqlScriptReader content);

    void executeSqlScript(SqlScriptConsumer content);
}
