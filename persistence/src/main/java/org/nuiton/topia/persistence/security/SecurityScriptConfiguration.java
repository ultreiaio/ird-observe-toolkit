package org.nuiton.topia.persistence.security;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

/**
 * Created on 12/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public abstract class SecurityScriptConfiguration {
    /**
     * Used to compute security privileges.
     *
     * @return schema name of a data table
     */
    public abstract String getDataTableSchemaName();

    /**
     * Used to compute security privileges.
     *
     * @return table name of a data table
     */
    public abstract String getDataTableTableName();

    /**
     * Used to compute security privileges.
     *
     * @return schema name of a referential table
     */
    public abstract String getReferentialTableSchemaName();

    /**
     * Used to compute security privileges.
     *
     * @return table name of a referential table
     */
    public abstract String getReferentialTableTableName();

    /**
     * Used to compute data source information.
     *
     * @return the minimum version accepted for migration
     */
    public abstract Version getMinimumMigrationVersion();

}
