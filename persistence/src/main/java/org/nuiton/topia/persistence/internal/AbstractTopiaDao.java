package org.nuiton.topia.persistence.internal;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.nuiton.topia.persistence.HqlAndParametersBuilder;
import org.nuiton.topia.persistence.QueryMissingOrderException;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.TopiaNonUniqueResultException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.pager.FilterRuleGroupOperator;
import org.nuiton.topia.persistence.pager.PaginationOrder;
import org.nuiton.topia.persistence.pager.PaginationParameter;
import org.nuiton.topia.persistence.pager.PaginationResult;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * This class has the common methods usable for each Dao managed by Topia. It is not JPA implementation dependent, it
 * only relies on {@link TopiaJpaSupport}.
 * </p>
 * <p>
 * This class is directly extended by the GeneratedXyzTopiaDao which groups all the Xyz specific methods.
 * </p>
 * Instances are created bt the model's specific {@link TopiaPersistenceContext}, which implements the
 * {@link TopiaDaoSupplier} contract.
 *
 * @param <E> the managed entity type
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Arnaud Thimel (Code Lutin)
 * @author Tony Chemit - tchemit@codelutin.com
 */
public abstract class AbstractTopiaDao<E extends TopiaEntity> implements TopiaDao<E> {

    protected static final Function<PaginationOrder, String> PAGINATION_ORDER_TO_HQL = input -> String.format("%s %s", input.getClause(), input.isDesc() ? "DESC" : "ASC");

    protected TopiaJpaSupport topiaJpaSupport;

    protected TopiaHibernateSupport topiaHibernateSupport;

    protected TopiaSqlSupport topiaSqlSupport;

    protected TopiaIdFactory topiaIdFactory;

    protected TopiaDaoSupplier topiaDaoSupplier;

    /**
     * When AbstractTopiaContext create the TopiaDAOHibernate, it must call this
     * method just after.
     *
     * @param topiaJpaSupport       FIXME
     * @param topiaHibernateSupport FIXME
     * @param topiaSqlSupport       FIXME
     * @param topiaIdFactory        FIXME
     * @param topiaDaoSupplier      FIXME
     */
    public void init(
            TopiaJpaSupport topiaJpaSupport,
            TopiaHibernateSupport topiaHibernateSupport,
            TopiaSqlSupport topiaSqlSupport,
            TopiaIdFactory topiaIdFactory,
            TopiaDaoSupplier topiaDaoSupplier) {
        this.topiaJpaSupport = topiaJpaSupport;
        this.topiaHibernateSupport = topiaHibernateSupport;
        this.topiaSqlSupport = topiaSqlSupport;
        this.topiaIdFactory = topiaIdFactory;
        this.topiaDaoSupplier = topiaDaoSupplier;
    }

    @Override
    public E newInstance(Map<String, Object> properties) {
        E result = newInstance();

        try {
            for (Map.Entry<String, Object> e : properties.entrySet()) {
                String propertyName = e.getKey();
                Object value = e.getValue();
                PropertyUtils.setProperty(result, propertyName, value);
            }
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException eee) {
            throw new IllegalArgumentException(
                    "Can't put properties on new Object", eee);
        }
        return result;
    }

    @Override
    public E initId(E entity) {
        // first set topiaId
        if (!entity.isPersisted()) {
            // only set id if not already on
            String topiaId = topiaIdFactory.newTopiaId(entity.contractType());
            entity.setTopiaId(topiaId);
        }
        return entity;
    }

    @Override
    public E create(E entity) {
        // first set topiaId
        entity = initId(entity);
        // save entity
        topiaJpaSupport.save(entity);
        return entity;
    }

    @Override
    public E create(String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues) {
        Map<String, Object> properties =
                convertPropertiesArrayToMap(propertyName, propertyValue, otherPropertyNamesAndValues);
        return create(properties);
    }

    @Override
    public E create(Map<String, Object> properties) {
        E result = newInstance(properties);
        create(result);
        return result;
    }

    @Override
    public E create() {
        E result = newInstance();
        create(result);
        return result;
    }

    @Override
    public E update(E entity) {
        topiaJpaSupport.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public void delete(E entity) {
        topiaJpaSupport.delete(entity);
        entity.notifyDeleted();
    }

    protected HqlAndParametersBuilder<E> newHqlAndParametersBuilder(FilterRuleGroupOperator filterRuleGroupOperator) {
        return new HqlAndParametersBuilder<>(getEntityClass(), filterRuleGroupOperator);
    }

    protected HqlAndParametersBuilder<E> newHqlAndParametersBuilder() {
        return newHqlAndParametersBuilder(FilterRuleGroupOperator.AND);
    }

    protected HqlAndParametersBuilder<E> getHqlForProperties(String propertyName,
                                                             Object propertyValue,
                                                             Object... otherPropertyNamesAndValues) {
        Map<String, Object> properties = convertPropertiesArrayToMap(propertyName, propertyValue, otherPropertyNamesAndValues);
        return getHqlForProperties(properties);
    }

    protected HqlAndParametersBuilder<E> getHqlForProperties(Map<String, Object> properties) {
        HqlAndParametersBuilder<E> result = newHqlAndParametersBuilder();
        for (Map.Entry<String, Object> property : properties.entrySet()) {
            result.addEquals(property.getKey(), property.getValue());
        }
        return result;
    }

    protected AbstractTopiaDaoQueryBuilderRunQueryStep<E> forHql(String hql, Map<String, Object> hqlParameters) {
        //FIXME tchemit-2016-05-01 Should we scan in hql code if there is an orderBy ?
        boolean withOrderByClause = false;
        return new AbstractTopiaDaoQueryBuilderRunQueryStep<>(this, true, withOrderByClause, hql, hqlParameters);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forAll() {
        return newQueryBuilder();
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(Map<String, Object> properties) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = getHqlForProperties(properties);
        return new AbstractTopiaDaoQueryBuilderAddCriteriaOrRunQueryStep<>(this, hqlAndParametersBuilder);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forProperties(String propertyName,
                                                                       Object propertyValue,
                                                                       Object... otherPropertyNamesAndValues) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = getHqlForProperties(propertyName, propertyValue, otherPropertyNamesAndValues);
        return new AbstractTopiaDaoQueryBuilderAddCriteriaOrRunQueryStep<>(this, hqlAndParametersBuilder);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forContains(String propertyName, Object propertyValue) {
        return newQueryBuilder().addContains(propertyName, propertyValue);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forEquals(String propertyName, Object propertyValue) {
        return newQueryBuilder().addEquals(propertyName, propertyValue);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forIn(String propertyName, Collection<?> propertyValues) {
        return newQueryBuilder().addIn(propertyName, propertyValues);
    }

    @Override
    public <O> List<O> findAll(String hql, Map<String, Object> hqlParameters) {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        return topiaJpaSupport.findAll(hql, hqlParameters);
    }

    protected boolean exists(String hql, Map<String, Object> hqlParameters) {
        // TODO AThimel 12/09/14 Avoid loading entity, just count the results
        E entity = topiaJpaSupport.findAny(hql, hqlParameters);
        return entity != null;
    }

    protected long count(String hql, Map<String, Object> hqlParameters) {
        Preconditions.checkArgument(hql.toLowerCase().trim().startsWith("select count("), "Your HQL query must start with \"select count(\"");
        return findUnique(hql, hqlParameters);
    }

    protected <O> O findUnique(String hql, Map<String, Object> hqlParameters) throws TopiaNoResultException, TopiaNonUniqueResultException {
        O result = findUniqueOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindUnique(String hql, Map<String, Object> hqlParameters) throws TopiaNonUniqueResultException {
        O uniqueOrNull = findUniqueOrNull(hql, hqlParameters);
        return Optional.ofNullable(uniqueOrNull);
    }

    protected <O> O findUniqueOrNull(String hql, Map<String, Object> hqlParameters) throws TopiaNonUniqueResultException {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        return topiaJpaSupport.findUnique(hql, hqlParameters);
    }

    protected <O> O findFirst(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        O result = findFirstOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindFirst(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        O firstOrNull = findFirstOrNull(hql, hqlParameters);
        return Optional.ofNullable(firstOrNull);
    }

    protected <O> O findFirstOrNull(String hql, Map<String, Object> hqlParameters) throws QueryMissingOrderException {
        if (!hqlContainsOrderBy(hql)) {
            throw new QueryMissingOrderException(hql, hqlParameters);
        }
        return findAnyOrNull(hql, hqlParameters);
    }

    protected <O> O findAny(String hql, Map<String, Object> hqlParameters) throws TopiaNoResultException {
        O result = findAnyOrNull(hql, hqlParameters);
        if (result == null) {
            throw new TopiaNoResultException(hql, hqlParameters);
        }
        return result;
    }

    protected <O> Optional<O> tryFindAny(String hql, Map<String, Object> hqlParameters) {
        O anyOrNull = findAnyOrNull(hql, hqlParameters);
        return Optional.ofNullable(anyOrNull);
    }

    protected <O> O findAnyOrNull(String hql, Map<String, Object> hqlParameters) {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        return topiaJpaSupport.findAny(hql, hqlParameters);
    }

    protected <O> List<O> findAll(String hql) {
        Objects.requireNonNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        return findAll(hql, hqlParameters);
    }

    protected <O> Stream<O> stream(String hql) {
        Objects.requireNonNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        return stream(hql, hqlParameters);
    }

    public <O> Stream<O> stream(String hql, Map<String, Object> hqlParameters) {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        return topiaJpaSupport.stream(hql, hqlParameters);
    }

    protected <O> List<O> find(String hql, int startIndex, int endIndex) {
        Objects.requireNonNull(hql);
        Map<String, Object> hqlParameters = Collections.emptyMap();
        return find(hql, hqlParameters, startIndex, endIndex);
    }

    protected <O> List<O> find(String hql, Map<String, Object> hqlParameters, int startIndex, int endIndex) {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        return topiaJpaSupport.find(hql, startIndex, endIndex, hqlParameters);
    }

    protected <O> List<O> find(String hql, Map<String, Object> hqlParameters, PaginationParameter page) {
        Objects.requireNonNull(hql);
        Objects.requireNonNull(hqlParameters);
        Objects.requireNonNull(page);

        boolean hqlContainsOrderClause = hqlContainsOrderBy(hql);
        boolean pageContainsOrderClause = !page.getOrderClauses().isEmpty();

        if (!hqlContainsOrderClause && !pageContainsOrderClause) {
            throw new QueryMissingOrderException(hql, hqlParameters, page);
        }

        // Must have one (and only one) order by clause in query
        Preconditions.checkArgument(
                hqlContainsOrderClause ^ pageContainsOrderClause,
                String.format(
                        "One 'order by' clause (and only one) must be specified. [orderByInHql=%b] [orderByInPage=%b]",
                        hqlContainsOrderClause,
                        pageContainsOrderClause)
        );

        if (pageContainsOrderClause) {
            hql += " ORDER BY ";
            Iterable<String> orderClauses = page.getOrderClauses().stream().map(PAGINATION_ORDER_TO_HQL).collect(Collectors.toList());
            hql += Joiner.on(", ").join(orderClauses);
        }

        return topiaJpaSupport.find(hql, page.getStartIndex(), page.getEndIndex(), hqlParameters);
    }

    protected <O> PaginationResult<O> findPage(String hql, Map<String, Object> hqlParameters, PaginationParameter page) {
        List<O> elements = find(hql, hqlParameters, page);
        String countHql = "select count(topiaId) ";
        if (hqlStartsWithSelect(hql)) {
            // must remove the from clause, otherwise some sql queries won't work.
            countHql += hql.substring(hql.toLowerCase().indexOf(" from "));
        } else {
            countHql += hql;
        }
        if (hqlContainsOrderBy(countHql)) {
            // must remove the order by clause, otherwise some sql queries won't work.
            countHql = countHql.substring(0, countHql.toLowerCase().indexOf("order by"));
        }
        long count = count(countHql, hqlParameters);
        return PaginationResult.of(elements, count, page);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forTopiaIdEquals(String topiaId) {
        Preconditions.checkArgument(Strings.isNotEmpty(topiaId), "given topiaId is blank");
        return forEquals(TopiaEntity.PROPERTY_TOPIA_ID, topiaId);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> forTopiaIdIn(Collection<String> topiaIds) {
        Objects.requireNonNull(topiaIds, "given topiaIds is null");
        return forIn(TopiaEntity.PROPERTY_TOPIA_ID, topiaIds);
    }

    @Override
    public List<String> findAllIds() {
        return newQueryBuilder().findAllIds();
    }

    @Override
    public List<E> findAll() {
        return newQueryBuilder().findAll();
    }

    @Override
    public Stream<E> streamAll() {
        return newQueryBuilder().stream();
    }

    @Override
    public long count() {
        return newQueryBuilder().count();
    }

    protected boolean hqlContainsOrderBy(String hql) {
        return hql.toLowerCase().contains("order by");
    }

    protected boolean hqlStartsWithSelect(String hql) {
        return hql.toLowerCase().trim().startsWith("select ");
    }

    @Override
    public long findSingleResult(SqlQuery<Long> query) {
        return topiaSqlSupport.findSingleResult(query);
    }

    @Override
    public TopiaDaoSupplier topiaDaoSupplier() {
        return topiaDaoSupplier;
    }

    @Override
    public E newInstance() {
        return newInstance0();
    }

    @Override
    public <O> List<O> findMultipleResult(SqlQuery<O> query) {
        return topiaSqlSupport.findMultipleResult(query);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder() {
        return newQueryBuilder(FilterRuleGroupOperator.AND);
    }

    @Override
    public TopiaQueryBuilderAddCriteriaOrRunQueryStep<E> newQueryBuilder(FilterRuleGroupOperator filterRuleGroupOperator) {
        HqlAndParametersBuilder<E> hqlAndParametersBuilder = new HqlAndParametersBuilder<>(getEntityClass(), "main", filterRuleGroupOperator);
        return new AbstractTopiaDaoQueryBuilderAddCriteriaOrRunQueryStep<>(this, hqlAndParametersBuilder);
    }

    @Override
    public TopiaSqlSupport getTopiaSqlSupport() {
        return topiaSqlSupport;
    }

    @Override
    public final Query<Map<String, ?>> getMapQuery(String queryName) {
        return topiaHibernateSupport.getMapQuery(getEntityClass().getName() + "::" + queryName);
    }

    @Override
    public final <T> NativeQuery<T> getSqlQuery(String queryName) {
        return topiaHibernateSupport.getSqlQuery(getEntityClass().getName() + "::" + queryName);
    }

    @Override
    public final <T> Query<T> getQuery(String queryName) {
        return topiaHibernateSupport.getQuery(getEntityClass().getName() + "::" + queryName);
    }

    @Override
    public void init(AbstractTopiaPersistenceContext persistenceContext) {
        init(persistenceContext.getJpaSupport(),
             persistenceContext.getHibernateSupport(),
             persistenceContext.getSqlSupport(),
             persistenceContext.getTopiaIdFactory(),
             persistenceContext);
    }

    public abstract <T> NativeQuery<T> queryGetLastUpdateDate();

    public static Map<String, Object> convertPropertiesArrayToMap(
            String propertyName, Object propertyValue, Object... otherPropertyNamesAndValues) throws IllegalArgumentException {
        Map<String, Object> properties = new HashMap<>();
        properties.put(propertyName, propertyValue);
        int propertiesLength = otherPropertyNamesAndValues.length;
        Preconditions.checkArgument(propertiesLength % 2 == 0,
                                    "Wrong number of argument "
                                            + propertiesLength
                                            + ", you must have even number.");
        for (int i = 0; i < otherPropertyNamesAndValues.length; ) {
            Object aPropertyName = otherPropertyNamesAndValues[i++];
            Object value = otherPropertyNamesAndValues[i++];
            Preconditions.checkArgument(
                    aPropertyName instanceof String,
                    "Argument at position [" + (i - 1) + "] " +
                            "should be a property name (says a String) but was " +
                            aPropertyName);
            properties.put((String) aPropertyName, value);
        }
        return properties;
    }

}
