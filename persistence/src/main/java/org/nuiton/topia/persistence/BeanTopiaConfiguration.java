package org.nuiton.topia.persistence;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.sql.conf.BeanJdbcConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import org.hibernate.cfg.AvailableSettings;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

public class BeanTopiaConfiguration extends BeanJdbcConfiguration implements TopiaConfiguration {

    /**
     * Configuration that must not be in {@link TopiaConfiguration#getHibernateExtraConfiguration()}.
     */
    protected static final Set<String> MAIN_CONFIGURATION = Set.of(AvailableSettings.DRIVER,
                                                                   AvailableSettings.URL,
                                                                   AvailableSettings.USER,
                                                                   AvailableSettings.PASS);

    private static final Map<String, String> HIBERNATE_GLOBAL_PROPERTIES;

    static {
        URL resource = TopiaConfiguration.class.getClassLoader().getResource("/hibernate.properties");
        if (resource == null) {
            resource = Thread.currentThread().getContextClassLoader().getResource("hibernate.properties");
        }
        HIBERNATE_GLOBAL_PROPERTIES = new TreeMap<>();
        try (InputStream stream = Objects.requireNonNull(resource).openStream()) {
            Properties p = new Properties();
            p.load(stream);
            for (String s : p.stringPropertyNames()) {
                HIBERNATE_GLOBAL_PROPERTIES.put(s, p.getProperty(s));
            }
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't load hibernate.properties file from %s", resource), e);
        }
    }

    protected final Map<String, String> hibernateExtraConfiguration = new LinkedHashMap<>();

    protected final Map<String, Class<? extends TopiaService>> declaredServices = new LinkedHashMap<>();

    protected final Map<String, Map<String, String>> declaredServicesConfiguration = new LinkedHashMap<>();

    protected TopiaIdFactory topiaIdFactory;

    private final Path temporaryDirectory;

    public BeanTopiaConfiguration(JdbcConfiguration jdbcConfiguration, Path temporaryDirectory, boolean showSql) {
        setJdbcConnectionUrl(jdbcConfiguration.getJdbcConnectionUrl());
        setJdbcConnectionUser(jdbcConfiguration.getJdbcConnectionUser());
        setJdbcConnectionPassword(jdbcConfiguration.getJdbcConnectionPassword());
        setJdbcDriverClass(jdbcConfiguration.getJdbcDriverClass());
        this.temporaryDirectory = temporaryDirectory;
        if (showSql) {
            hibernateExtraConfiguration.put(AvailableSettings.SHOW_SQL, Boolean.TRUE.toString());
        }
        hibernateExtraConfiguration.putAll(HIBERNATE_GLOBAL_PROPERTIES);
    }

    @Override
    public TopiaIdFactory getTopiaIdFactory() {
        return topiaIdFactory;
    }

    @Override
    public Map<String, String> getHibernateExtraConfiguration() {
        return hibernateExtraConfiguration;
    }

    @Override
    public Path getTemporaryDirectory() {
        return temporaryDirectory;
    }

    @Override
    public Map<String, Class<? extends TopiaService>> getDeclaredServices() {
        return declaredServices;
    }

    @Override
    public Map<String, Map<String, String>> getDeclaredServicesConfiguration() {
        return declaredServicesConfiguration;
    }

    @Override
    public void check() throws TopiaMisconfigurationException {
        if (Strings.isEmpty(getJdbcConnectionUrl())) {
            throw new TopiaMisconfigurationException("you must provide JDBC connection URL", this);
        }
        if (Strings.isEmpty(getJdbcConnectionUser())) {
            throw new TopiaMisconfigurationException("you must provide JDBC connection user", this);
        }
        if (getJdbcConnectionPassword() == null) {
            throw new TopiaMisconfigurationException("you must provide JDBC connection password", this);
        }
        if (getJdbcDriverClass() == null) {
            throw new TopiaMisconfigurationException("you must provide JDBC connection driver", this);
        }
        Map<String, String> hibernateExtraConfiguration = getHibernateExtraConfiguration();
        for (Map.Entry<String, String> entry : hibernateExtraConfiguration.entrySet()) {
            String key = entry.getKey();
            boolean entryIsOk = (key.startsWith("hibernate.") || key.startsWith("javax.persistence.")) && !MAIN_CONFIGURATION.contains(key);
            if (!entryIsOk) {
                throw new TopiaMisconfigurationException(String.format("hibernate extra configuration is %s but it should not contains a key %s", hibernateExtraConfiguration, key), this);
            }
        }
        if (hibernateExtraConfiguration.get(AvailableSettings.CONNECTION_PROVIDER) == null) {
            throw new TopiaMisconfigurationException("you must provide hibernate connection provider in hibernate extra configuration", this);
        }
        String hbm2ddl = hibernateExtraConfiguration.get(AvailableSettings.HBM2DDL_AUTO);
        if (hbm2ddl != null && Set.of("update", "create", "create-drop").contains(hbm2ddl)) {
            throw new TopiaMisconfigurationException(String.format("Be careful, you are about to let Hibernate automatically create or update " +
                                                                           "your database schema. [%s=%s]", AvailableSettings.HBM2DDL_AUTO, hbm2ddl), this);
        }
    }

    public void setTopiaIdFactory(TopiaIdFactory topiaIdFactory) {
        this.topiaIdFactory = topiaIdFactory;
    }

    public void addDeclaredService(String serviceName, Class<? extends TopiaService> serviceClassName, Map<String, String> serviceConfiguration) {
        declaredServices.put(serviceName, serviceClassName);
        declaredServicesConfiguration.put(serviceName, serviceConfiguration);
    }
}
