package org.nuiton.topia.persistence.support;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.SqlConversions;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.query.QueryParameter;

import javax.persistence.Parameter;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created on 20/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.62
 */
public interface QuerySupport {

    @SuppressWarnings("unchecked")
    static <I, T> Function<I, T> toSingleResult() {
        return r -> (T) r;
    }

    static String toId(Entity e) {
        return e == null ? null : e.getTopiaId();
    }

    static String toComment(Function<String, String> commentFormat, String comment) {
        return comment == null ? null : commentFormat.apply(comment);
//FIXME IT seems to work without the commentFormatter...
//                                                     comment
    }

    <T> NativeQuery<T> getSqlQuery(String queryName);

    <T> Query<T> getQuery(String queryName);

    default Query<Map<String, ?>> getMapQuery(String queryName) {
        return getQuery(queryName);
    }

    default <T> List<T> findMultipleResultBySqlQuery(NativeQuery<Object[]> sqlQuery) {
        return findMultipleResultBySqlQuery(sqlQuery, QuerySupport.toSingleResult());
    }

    default String fillSql(NativeQuery<?> sqlQuery) {
        String result = sqlQuery.getQueryString();
        List<Parameter<?>> parameters = new java.util.ArrayList<>(sqlQuery.getParameters());
        parameters.sort(Comparator.comparing(Parameter::getPosition));
        Collections.reverse(parameters);
        for (Parameter<?> parameter : parameters) {
            QueryParameter<?> p = (QueryParameter<?>) parameter;
            Object parameterValue = sqlQuery.getParameterValue(parameter);
            String value = parameterValue == null ? "NULL" : parameterValue.toString();
            if (parameterValue instanceof String && SqlHelper.needEscapeString(value)) {
                value = SqlHelper.escapeString(value);
            }
            if (parameterValue instanceof Timestamp) {
                value = SqlConversions.toTimeStamp((Timestamp) parameterValue);
            }
            for (int sourceLocation : p.getSourceLocations()) {
                result = result.substring(0, sourceLocation) + value + result.substring(sourceLocation + 1);
            }
        }
        return result;
    }

    default <T> List<T> findMultipleResultBySqlQuery(NativeQuery<Object[]> sqlQuery, Function<Object[], T> resultTransformer) {
        return sqlQuery.stream().map(resultTransformer).collect(Collectors.toList());
    }

    default <T> List<T> findMultipleResultBySqlQuery(NativeQuery<Object[]> sqlQuery, BiConsumer<Object[], List<T>> resultTransformer) {
        List<T> result = new LinkedList<>();
        sqlQuery.stream().forEach(r -> resultTransformer.accept(r, result));
        return result;
    }

    default <R> R findSingleResultBySqlQuery(NativeQuery<R> sqlQuery) {
        return findSingleResultBySqlQuery(sqlQuery, toSingleResult());
    }

    default <I, R> R findSingleResultBySqlQuery(NativeQuery<I> sqlQuery, Function<I, R> resultTransformer) {
        return sqlQuery.uniqueResultOptional().map(resultTransformer).orElse(null);
    }

    default int execute(NativeQuery<?> sqlQuery) {
        return sqlQuery.executeUpdate();
    }

    class MapBuilder<K extends ToolkitId, V> {

        private final Map<K, List<V>> result;

        private final Map<String, K> keysById;
        private final BiFunction<K, Object[], V> valueCreator;

        private K currentKey;
        private List<V> currentList;

        public MapBuilder(Map<K, List<V>> result, Collection<K> keys, BiFunction<K, Object[], V> valueCreator) {
            this.result = result;
            this.keysById = keys.stream().collect(Collectors.toMap(ToolkitId::getId, Function.identity()));
            this.valueCreator = valueCreator;
            keysById.values().forEach(r -> result.put(r, new LinkedList<>()));
        }

        public void addRow(Object[] row) {
            String keyId = (String) row[0];
            if (currentKey == null || !Objects.equals(currentKey.getId(), keyId)) {
                currentKey = keysById.get(keyId);
                currentList = result.get(currentKey);
            }
            currentList.add(valueCreator.apply(currentKey, row));
        }

        public Map<K, List<V>> build(Stream<Object[]> data) {
            data.forEach(this::addRow);
            return result;
        }
    }
}
