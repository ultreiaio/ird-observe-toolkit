package org.nuiton.topia.persistence;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.services.service.data.MissingReferentialRequest;
import fr.ird.observe.services.service.data.MissingReferentialResult;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.jdbc.JdbcHelperH2;
import org.nuiton.topia.persistence.jdbc.JdbcHelperPostgres;
import org.nuiton.topia.persistence.security.SecurityScriptHelper;
import org.nuiton.topia.persistence.support.TopiaServiceSupport;
import org.nuiton.topia.service.migration.TopiaMigrationService;
import org.nuiton.topia.service.sql.TopiaEntitySqlModelResource;
import org.nuiton.topia.service.sql.TopiaSqlService;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobIdsIterator;

import java.io.Closeable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>This is the main entry point for ToPIA. This interface represents the root context of an application.</p>
 *
 * <p>Most of the time, there is only one instance of this context during the entire lifecycle of a application.</p>
 *
 * <p>This contract provides facility to :</p>
 * <ul>
 * <li>create a new {@link TopiaPersistenceContext};</li>
 * <li>get meta information about the application (model name, version);</li>
 * <li>get meta information about the entities;</li>
 * <li>do schema related operations;</li>
 * <li>shutdown the application</li>
 * </ul>
 *
 * <p>This contract has an abstract implementation
 * ({@link AbstractTopiaApplicationContext}) which is itself extended by a
 * generated {@code AbstractXxxTopiaApplicationContext} (where {@code Xxx} is the project name), itself
 * extended by {@code XxxTopiaApplicationContext}.</p>
 *
 * <p>The full hierarchy is :</p> <pre>
 * TopiaApplicationContext (this contract)
 *  \--AbstractTopiaApplicationContext
 *      \--AbstractXxxTopiaApplicationContext (generated)
 *          \--XxxTopiaApplicationContext (generated)
 * </pre>
 * <p>(where {@code Xxx} is the project name)</p>
 *
 * <p>Created on 12/20/13.</p>
 *
 * @author Tony Chemit - tchemit@codelutin.com
 * @since 3.0
 */
public interface TopiaApplicationContext<K extends TopiaPersistenceContext> extends TopiaServiceSupport, Closeable, TopiaEntitySqlModelResource {

    /**
     * Creates a new instance of XxxTopiaPersistenceContext (where {@code Xxx} is the project name). This is the
     * method to use in order to start a new transaction.
     *
     * @return a newly created instance of your project's specific {@link TopiaPersistenceContext}
     * @see TopiaPersistenceContext
     */
    K newPersistenceContext();

    String getModelVersion();

    String getModelName();

    String getAuthenticationToken();

    TopiaConfiguration getConfiguration();

    TopiaSqlService getSqlService();

    TopiaMigrationService getMigrationService();

    default TopiaIdFactory getTopiaIdFactory() {
        return getConfiguration().getTopiaIdFactory();
    }

    TopiaIdFactoryForBulkSupport newIdFactoryForBulk(long timestamp);

    ReferentialIds getReferentialIds(Class<? extends TopiaEntity> dataType, String... ids);

    default SecurityScriptHelper newSecurityScriptHelper() {
        return new SecurityScriptHelper(this);
    }

    TopiaEntitySqlBlobIdsIterator newBlobIdsIterator(Path blobIdsPath);

    void executeSqlStatements(SqlScript content);

    default void migrate() {
        getMigrationService().runSchemaMigration();
    }

    /**
     * Get the list of entities (ToPIA or not) that Hibernate has to handle. By default it only contains ToPIA entities
     * but you may override it to add any other Hibernate entity.
     */
    default List<Class<?>> getPersistenceClasses() {
        // Par défaut la liste des entités qu'Hibernate gère est seulement la liste des entités ToPIA mais en
        // surchargeant cette méthode on peut ajouter des entités non ToPIA
        return new ArrayList<>(getImplementationClasses());
    }

    Set<Class<? extends TopiaEntity>> getImplementationClasses();

    /**
     * @return all schema names used by persistent entities.
     * @since 3.0.1
     */
    List<String> getSchemaNames();

    /**
     * @return true is the application context is opened (ie not yet {@link #isClosed()})
     */
    boolean isOpened();

    /**
     * @return true is the application context is already closed, false otherwise
     */
    boolean isClosed();

    /**
     * Method to call when the application is going for shutdown. It will trigger the shutdown of all the opened
     * children {@link TopiaPersistenceContext}
     */
    @Override
    void close();

    /**
     * @return a new jdbc helper.
     */
    JdbcHelper newJdbcHelper();

    JdbcHelperH2 newJdbcHelperH2();

    @SuppressWarnings("unused")
    JdbcHelperPostgres newJdbcHelperPostgres();

    <E extends RootOpenableEntity> MissingReferentialResult computeMissingReferential(ServiceContext context, Class<E> entityType, MissingReferentialRequest request);
}
