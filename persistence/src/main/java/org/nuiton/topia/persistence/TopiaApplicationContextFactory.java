package org.nuiton.topia.persistence;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created on 17/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.5
 */
public abstract class TopiaApplicationContextFactory<P extends TopiaPersistenceContext, A extends TopiaApplicationContext<P>> implements AutoCloseable {

    private static final Logger log = LogManager.getLogger(TopiaApplicationContextFactory.class);

    /**
     * Cache of open topia application contexts indexed by their authenticationToken.
     */
    private final Map<String, A> cache = new TreeMap<>();

    /**
     * Create a new topia application context from the given configuration.
     * <p>
     * <b>You must</b> call the method {@link #addToCache(TopiaApplicationContext)} to be able to get it back later.
     *
     * @param topiaConfiguration configuration
     * @return the new topia application context
     */
    public abstract A create(TopiaConfiguration topiaConfiguration);

    public final A get(String authenticationToken) {
        A result = cache.get(authenticationToken);
        return Objects.requireNonNull(result, "Did not find topia application context from: " + authenticationToken);
    }

    public final void close(String authenticationToken) {
        // we do not remove it from cache, the cache is always clean with method removeFromCache
        A applicationContext = cache.get(authenticationToken);
        if (applicationContext == null) {
            // no more in cache, nothing to do
            return;
        }
        close(applicationContext);
    }

    @Override
    public final void close() {
        for (A applicationContext : List.copyOf(cache.values())) {
            close(applicationContext);
        }
        // just to make sure everything was removed from cache.
        int cacheSize = cache.size();
        if (cacheSize > 0) {
            throw new IllegalStateException(String.format("There is still %d topia application context in cache!", cacheSize));
        }
    }

    protected synchronized final void addToCache(A applicationContext) {
        String authenticationToken = applicationContext.getAuthenticationToken();
        cache.put(authenticationToken, applicationContext);
        log.info(String.format("Topia application context: %s added   (total context in cache: %d)", authenticationToken, cache.size()));
    }

    protected synchronized final void removeFromCache(A applicationContext) {
        String authenticationToken = applicationContext.getAuthenticationToken();
        cache.remove(authenticationToken);
        log.info(String.format("Topia application context: %s removed (total context in cache: %d)", authenticationToken, cache.size()));
    }

    private void close(A applicationContext) {
        // any context coming from cache can not be closed! So this code is not required
//        if (!applicationContext.isClosed()) {
        try {
            applicationContext.close();
        } catch (Exception e) {
            log.error(String.format("Could not close topia application context: %s", applicationContext.getAuthenticationToken()), e);
        }
//        }
    }
}
