package org.nuiton.topia.service.migration;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Topia migration service default implementation.
 * <p>
 * This new version of the migration service requires no configuration by default.
 * <p>
 * The only thing you can configure is the migration user callback (see @link {@link TopiaMigrationServiceAskUserToMigrate}).
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class TopiaMigrationServiceImpl implements TopiaMigrationService {

    private static final TimeLog TIME_LOG = new TimeLog(TopiaMigrationServiceImpl.class, 100, 1000);
    private static final Logger log = LogManager.getLogger(TopiaMigrationServiceImpl.class);
    /**
     * Configuration of service.
     */
    protected TopiaMigrationServiceConfiguration configuration;

    @Override
    public void initTopiaService(TopiaApplicationContext<?> topiaApplicationContext, Map<String, String> serviceConfiguration) {
        this.configuration = TopiaMigrationServiceConfiguration.of(Objects.requireNonNull(topiaApplicationContext));
    }

    @Override
    public void runSchemaMigration() throws TopiaMigrationServiceException {

        log.info("Migration - Prepare internal states.");
        TopiaMigrationServiceContext context = TopiaMigrationServiceContext.create(configuration);
        log.info("Migration - Prepare internal states done.");

        try (TopiaPersistenceContext persistenceContext = context.newPersistenceContext()) {
            log.info("Migration - Adapting migration table.");
            boolean canContinue = adaptVersionTable(persistenceContext, context);
            log.info("Migration - Adapting migration table done.");
            if (!canContinue) {
                log.info(String.format("Migration - End - db version: %s (nothing more to do).", context.getDbVersion()));
                return;
            }
            log.info("Migration - Getting migration versions to apply.");
            List<Version> versionsToApply = context.getVersionsToApply();
            log.info("Migration - Getting migration versions to apply done.");
            if (versionsToApply == null) {
                log.info(String.format("Migration - End - db version: %s (nothing more to do).", context.getDbVersion()));
                return;
            }

            log.info(String.format("Migration - Will apply versions: %1$s.", versionsToApply));

            long statementCount = 0;
            for (Version version : versionsToApply) {
                long t0 = TimeLog.getTime();
                long versionCount = migrateVersion(persistenceContext, version, context);
                statementCount += versionCount;
                TIME_LOG.log(t0, "migrationVersion", version.toString() + " - " + versionCount + " sql statements");
            }
            log.info(String.format("Migration - End - db version: %s - consume %d sql statement(s)", context.getDbVersion(), statementCount));
        }

    }

    @Override
    public void close() {
        configuration = null;
    }

    protected boolean adaptVersionTable(TopiaPersistenceContext persistenceContext, TopiaMigrationServiceContext context) {
        try {
            boolean canContinue = context.adaptVersionTable(persistenceContext.getSqlSupport());
            persistenceContext.commit();
            return canContinue;
        } catch (Exception e) {
            persistenceContext.rollback();
            throw new TopiaMigrationServiceException("Exception during adapting migration table", e);
        }
    }

    protected long migrateVersion(TopiaPersistenceContext persistenceContext, Version version, TopiaMigrationServiceContext context) {

        MigrationVersionResource resource = context.getResource(version);

        long outStatementCount;

        TopiaSqlSupport sqlSupport = persistenceContext.getSqlSupport();
        try (MigrationVersionResourceExecutor executor = context.newExecutor(resource, sqlSupport)) {

            generateMainScript(executor, resource);

            outStatementCount = applyMainScript(executor, sqlSupport, resource);

            if (resource.requiresFinalize()) {

                log.info(executor.getLogPrefix() + "Flush session before finalize schema migration.");
                persistenceContext.flush();

                try (MigrationVersionResourceExecutor finalizeExecutor = executor.toFinalize()) {
                    generateFinalizeScript(finalizeExecutor, resource);
                    outStatementCount += applyFinalizeScript(finalizeExecutor, sqlSupport, resource);
                }
            }
            // here we use the sqlSupport to save version, to stay on the very same transaction
            context.saveVersion(sqlSupport, version, new Date());

            // do commit, this migration version is now completed
            persistenceContext.commit();
        } catch (Exception e) {
            // Exception, rollback transaction
            persistenceContext.rollback();
            throw new TopiaMigrationServiceException("Exception during schema migration on version: " + version, e);
        }
        return outStatementCount;
    }

    protected void generateMainScript(MigrationVersionResourceExecutor executor, MigrationVersionResource resource) throws IOException {
        String logPrefix = executor.getLogPrefix();

        long t0 = TimeLog.getTime();
        log.info(logPrefix + "Start schema migration.");
        resource.generateSqlScript(executor);
        long inStatementCount = executor.flush();
        TIME_LOG.log(t0, "generateSqlScript (main)", String.format("%s - %d sql statements", resource.getVersion(), inStatementCount));
        log.info(logPrefix + String.format("Discover %d sql statement(s) to apply.", inStatementCount));
    }

    protected long applyMainScript(MigrationVersionResourceExecutor executor, TopiaSqlSupport sqlSupport, MigrationVersionResource resource) throws IOException {
        String logPrefix = executor.getLogPrefix();

        long t0 = TimeLog.getTime();
        long outStatementCount;
        // See https://gitlab.com/ultreiaio/topia-extension/-/issues/126
        try (SqlScriptConsumer sqlWork = SqlScriptConsumer.builder(executor.getScriptForVersion())/*.batchSize(1000)*/.build()) {
            sqlSupport.doSqlWork(sqlWork);
            outStatementCount = sqlWork.getStatementCount();
            TIME_LOG.log(t0, "applySqlScript (main)", String.format("%s - %d sql statements", resource.getVersion(), outStatementCount));
            log.info(logPrefix + String.format("Consume %d sql statement(s).", outStatementCount));
        }
        return outStatementCount;
    }

    protected void generateFinalizeScript(MigrationVersionResourceExecutor finalizeExecutor, MigrationVersionResource resource) throws IOException {
        String logPrefix = finalizeExecutor.getLogPrefix();

        long t0 = TimeLog.getTime();
        resource.generateFinalizeSqlScript(finalizeExecutor);
        long inStatementCount = finalizeExecutor.flush();
        TIME_LOG.log(t0, "generateSqlScript (finalize)", String.format("%s - %d sql statements", resource.getVersion(), inStatementCount));
        log.info(logPrefix + String.format("Finalize - Discover %d sql statement(s) to apply.", inStatementCount));
    }

    protected long applyFinalizeScript(MigrationVersionResourceExecutor finalizeExecutor, TopiaSqlSupport sqlSupport, MigrationVersionResource resource) throws IOException {
        String logPrefix = finalizeExecutor.getLogPrefix();

        long t0 = TimeLog.getTime();
        long outStatementCount;
        // See https://gitlab.com/ultreiaio/topia-extension/-/issues/126
        try (SqlScriptConsumer sqlWork = SqlScriptConsumer.builder(finalizeExecutor.getScriptForVersion())/*.batchSize(1000)*/.build()) {
            sqlSupport.doSqlWork(sqlWork);
            outStatementCount = sqlWork.getStatementCount();
            TIME_LOG.log(t0, "applySqlScript (finalize)", String.format("%s - %d sql statements", resource.getVersion(), outStatementCount));
            log.info(logPrefix + String.format("Finalize - Consume %d sql statement(s).", outStatementCount));
        }
        return outStatementCount;
    }

}
