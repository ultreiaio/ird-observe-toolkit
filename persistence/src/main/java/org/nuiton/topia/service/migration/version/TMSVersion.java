package org.nuiton.topia.service.migration.version;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TMSVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Version version;
    private final Date date;

    public TMSVersion(String version, Date date) {
        if (version == null || version.isEmpty()) {
            throw new IllegalArgumentException("version parameter can not be null nor empty.");
        }
        this.version = Version.valueOf(version);
        this.date = date;
    }

    public Version getVersion() {
        return version;
    }

    public Date getDate() {
        return date;
    }

}
