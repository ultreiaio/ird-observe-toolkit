package org.nuiton.topia.service.migration;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ErrorFromServerException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.util.json.JsonAdapter;
import org.nuiton.topia.persistence.TopiaException;

import java.io.PrintWriter;
import java.io.StringWriter;

public class TopiaMigrationServiceException extends TopiaException {

    private static final long serialVersionUID = 18900570715595L;

    /**
     * Just the keep the full error as a text (we can't have a perfect exception via gson :().
     */
    @SuppressWarnings("unused")
    private String fullError;

    public TopiaMigrationServiceException(String message) {
        super(message);
    }

    public TopiaMigrationServiceException(String message, Throwable cause) {
        super(message, cause);
        this.fullError = getErrorStack(cause);
    }

    @SuppressWarnings("unused")
    public String getFullError() {
        return fullError;
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<TopiaMigrationServiceException> {

        @Override
        public TopiaMigrationServiceException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            JsonElement fullError = json.get("fullError");
            ErrorFromServerException cause = new ErrorFromServerException(fullError.getAsString());
            return new TopiaMigrationServiceException(message, cause);
        }

        @Override
        protected TopiaMigrationServiceException setStackTrace(JsonObject json, TopiaMigrationServiceException exception, JsonDeserializationContext context) {
            // do not add stackTrace, since it will then print the one coming from the deserializer
            return exception;
        }

        @Override
        public Class<?> type() {
            return TopiaMigrationServiceException.class;
        }
    }

    public static String getErrorStack(Throwable cause) {
        StringWriter w = new StringWriter();
        try (PrintWriter writer = new PrintWriter(w)) {
            cause.printStackTrace(writer);
        }
        return w.toString();
    }
}
