package org.nuiton.topia.service.migration.version;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Contract to define all the sql code used to manage the **version** table.
 * <p>
 * Created on 26/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public interface MigrationServiceSqlHelper {
    String CREATE_VERSION_SCHEMA_STATEMENT = "CREATE SCHEMA IF NOT EXISTS %s;";
    String DROP_VERSION_TABLE_STATEMENT = "DROP TABLE %s;";

    /**
     * Legacy layout.
     */
    MigrationServiceSqlHelper LEGACY = new MigrationServiceSqlHelper() {
        public static final String SCHEMA_NAME = "public";
        public static final String TABLE_NAME = "tms_version";
        public static final String CREATE_VERSION_TABLE_STATEMENT = "CREATE TABLE %s(version VARCHAR(255) NOT NULL, PRIMARY KEY(version));";
        public static final String FILL_VERSION_TABLE_STATEMENT = "INSERT INTO %s(version) VALUES('%s');";
        public static final String GET_VERSION_TABLE_STATEMENT = "SELECT version FROM %s;";
        public static final String DELETE_VERSION_TABLE_STATEMENT = "DELETE FROM %s;";

        @Override
        public String schemaName() {
            return SCHEMA_NAME;
        }

        @Override
        public String tableName() {
            return TABLE_NAME;
        }

        @Override
        public String createTableSql() {
            return String.format(CREATE_VERSION_TABLE_STATEMENT, gav());
        }

        @Override
        public String getVersionSql() {
            return String.format(GET_VERSION_TABLE_STATEMENT, gav());
        }

        @Override
        public Optional<TMSVersion> getVersion(JdbcHelper jdbcHelper) {
            try {
                try {
                    TMSVersion result = jdbcHelper.runSelect(getVersionSql(), r -> new TMSVersion(r.getString(1), null));
                    return Optional.ofNullable(result);
                } catch (Exception e) {
                    throw new TopiaException("Could not obtain version", e);
                }
            } catch (Exception e) {
                throw new TopiaException("Could not obtain version", e);
            }
        }

        @Override
        public String saveTableSql(String version, Date date) {
            return String.format(DELETE_VERSION_TABLE_STATEMENT, gav()) + String.format(FILL_VERSION_TABLE_STATEMENT, gav(), version);
        }
    };
    /**
     * Default layout.
     */
    MigrationServiceSqlHelper DEFAULT = new MigrationServiceSqlHelper() {
        public static final String SCHEMA_NAME = "common";
        //FIXME Find a better name
        public static final String TABLE_NAME = "database_version";
        public static final String CREATE_VERSION_TABLE_STATEMENT = "CREATE TABLE %s(version VARCHAR(255) NOT NULL, date TIMESTAMP, PRIMARY KEY(version));";
        public static final String FILL_VERSION_TABLE_STATEMENT = "INSERT INTO %s(version, date) VALUES('%s', %s);";
        public static final String GET_VERSION_TABLE_STATEMENT = "SELECT version, date FROM %s;";

        @Override
        public String schemaName() {
            return SCHEMA_NAME;
        }

        @Override
        public String tableName() {
            return TABLE_NAME;
        }

        @Override
        public String createTableSql() {
            return String.format(CREATE_VERSION_TABLE_STATEMENT, gav());
        }

        @Override
        public String getVersionSql() {
            return String.format(GET_VERSION_TABLE_STATEMENT, gav());
        }

        @Override
        public Optional<TMSVersion> getVersion(JdbcHelper jdbcHelper) {
            try {
                List<TMSVersion> result = jdbcHelper.runMultipleSelect(getVersionSql(), r -> new TMSVersion(r.getString(1), r.getTimestamp(2)));
                if (result.isEmpty()) {
                    return Optional.empty();
                }
                result.sort(Comparator.comparing(TMSVersion::getVersion).reversed());
                return Optional.of(result.get(0));
            } catch (Exception e) {
                throw new TopiaException("Could not obtain version", e);
            }
        }

        @Override
        public String saveTableSql(String version, Date date) {
            return String.format(FILL_VERSION_TABLE_STATEMENT, gav(), version, date == null ? "NULL" : ("'" + new Timestamp(date.getTime()) + "'::timestamp"));
        }
    };

    /**
     * Current layout used in application.
     * <p>
     * This is the only place we will need to change when we stop using the legacy migration table.
     * <p>
     * We will need even after that to keep the legacy definition, to be able to migration older database using it.
     */
    MigrationServiceSqlHelper CURRENT = DEFAULT;

    static boolean isLegacy() {
        return CURRENT.equals(LEGACY);
    }

    /**
     * @return the schema name of the **version** table.
     */
    String schemaName();

    /**
     * @return the table fo the **version** table.
     */
    String tableName();

    /**
     * @return the full schema + table name of the **version** table.
     */
    default String gav() {
        return schemaName() + "." + tableName();
    }

    Optional<TMSVersion> getVersion(JdbcHelper jdbcHelper);

    String getVersionSql();

    String createTableSql();

    String saveTableSql(String version, Date date);

    default String createSchemaSql() {
        return String.format(CREATE_VERSION_SCHEMA_STATEMENT, schemaName());
    }

    default String dropTableSql() {
        return String.format(DROP_VERSION_TABLE_STATEMENT, gav());
    }

    default boolean isTableExist(JdbcHelper jdbcHelper) {
        try {
            return jdbcHelper.isTableExist(schemaName(), tableName());
        } catch (SQLException e) {
            return false;
        }
    }

    default boolean isSchemaExist(JdbcHelper jdbcHelper) {
        try {
            return jdbcHelper.isSchemaExist(schemaName());
        } catch (SQLException e) {
            return false;
        }
    }

    default void createSchema(TopiaSqlSupport sqlSupport) {
        try {
            sqlSupport.executeSql(createSchemaSql());
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not create version schema %s", gav()), e);
        }
    }

    default void createTable(TopiaSqlSupport sqlSupport) {
        try {
            sqlSupport.executeSql(createTableSql());
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not create version table %s", gav()), e);
        }
    }

    default void dropTable(TopiaSqlSupport sqlSupport) {
        try {
            sqlSupport.executeSql(dropTableSql());
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not drop version table %s", gav()), e);
        }
    }

    default void save(TopiaSqlSupport sqlSupport, String version, Date date) {
        try {
            sqlSupport.executeSql(saveTableSql(version, date));
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not save version %s", version), e);
        }
    }
}
