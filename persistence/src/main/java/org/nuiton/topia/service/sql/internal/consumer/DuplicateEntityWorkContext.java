package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created on 15/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DuplicateEntityWorkContext {
    private final String id;

    private final String newId;
    private final SqlRequestSetConsumerContext context;
    private final SqlScriptWriter writer;

    DuplicateEntityWorkContext(String id, String newId, SqlRequestSetConsumerContext context) {
        this.id = id;
        this.newId = newId;
        this.context = context;
        this.writer = context.getWriter();
    }

    public String getNewId() {
        return newId;
    }

    public String getId() {
        return id;
    }

    public SqlRequestSetConsumerContext context() {
        return context;
    }

    public int getReadFetchSize() {
        return context.getReadFetchSize();
    }

    public Map<String, BlobsContainer.Builder> initBlobsBuilder(boolean useBlob, String schemaAndTableName) {
        return context.initBlobsBuilder(useBlob, schemaAndTableName);
    }

    public void copyBlob(PreparedStatement preparedStatement, String topiaId, Object columnValue, BlobsContainer.Builder blobsBuilder) throws SQLException {
        context.copyBlob(preparedStatement, topiaId, columnValue, blobsBuilder);
    }

    public void writeSql(String sql) {
        writer.writeSql(sql);
    }
}
