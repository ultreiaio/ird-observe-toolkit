package org.nuiton.topia.service.sql.blob;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.jdbc.JdbcHelper;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Objects;

/**
 * Created on 05/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.68
 */
public class TopiaEntitySqlBlobIdsIterator implements Iterator<String>, Closeable {

    private final TopiaEntitySqlBlobModel model;

    private final Path cachePath;
    private final JdbcHelper jdbcHelper;
    private TopiaEntitySqlBlobIdsFileIterator iterator;

    public TopiaEntitySqlBlobIdsIterator(TopiaEntitySqlBlobModel model, Path cachePath, JdbcHelper jdbcHelper) {
        this.model = Objects.requireNonNull(model);
        this.cachePath = Objects.requireNonNull(cachePath);
        this.jdbcHelper = Objects.requireNonNull(jdbcHelper);
    }

    @Override
    public boolean hasNext() {
        return iterator().hasNext();
    }

    @Override
    public String next() {
        return iterator().next();
    }

    public TopiaEntitySqlBlobIdsFileIterator iterator() {
        if (iterator == null) {
            iterator = new TopiaEntitySqlBlobIdsFileIterator(cachePath, model, jdbcHelper);
        }
        return iterator;
    }

    @Override
    public void close() throws IOException {
        if (iterator != null) {
            iterator.close();
            iterator = null;
        }
    }
}
