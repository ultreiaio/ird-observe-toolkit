package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Describes a slq table for a given entity.
 * <p>
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlTable extends AbstractTopiaEntitySqlTable {

    private static final Set<Class<?>> DECIMAL_TYPES = Set.of(
            float.class, Float.class,
            double.class, Double.class
    );

    private final Set<String> blobProperties;
    private final Map<String, Class<?>> simplePropertiesTypes;
    private final Map<String, Integer> decimalPropertiesScales;
    private final Set<String> enumerationPropertiesUsingName;
    private final String recursiveProperty;

    public TopiaEntitySqlTable(String entityName,
                               String schemaName,
                               String tableName,
                               Set<String> authorizedColumnNames,
                               List<TopiaEntitySqlSelector> selectors,
                               Set<String> blobProperties,
                               Map<String, Class<?>> simplePropertiesTypes,
                               Map<String, Integer> decimalPropertiesScales,
                               Set<String> enumerationPropertiesUsingName,
                               String recursiveProperty) {
        super(entityName, schemaName, tableName, authorizedColumnNames, selectors);
        this.blobProperties = Objects.requireNonNull(blobProperties);
        this.simplePropertiesTypes = Objects.requireNonNull(simplePropertiesTypes);
        this.decimalPropertiesScales = Objects.requireNonNull(decimalPropertiesScales);
        this.enumerationPropertiesUsingName = enumerationPropertiesUsingName;
        this.recursiveProperty = recursiveProperty;
    }

    public Map<String, Class<?>> getSimplePropertiesTypes() {
        return simplePropertiesTypes;
    }

    public Class<?> getSimplePropertiesType(String propertyName) {
        return getSimplePropertiesTypes().get(propertyName);
    }

    public Map<String, Integer> getDecimalPropertiesScales() {
        return decimalPropertiesScales;
    }

    public Integer getDecimalPropertiesScale(String propertyName) {
        return getDecimalPropertiesScales().get(propertyName);
    }

    public Set<String> getEnumerationPropertiesUsingName() {
        return enumerationPropertiesUsingName;
    }

    public boolean isEnumerationPropertyUsingName(String propertyName) {
        return getEnumerationPropertiesUsingName().contains(propertyName);
    }

    public Set<String> getBlobProperties() {
        return blobProperties;
    }

    public Optional<String> getOptionalRecursiveProperty() {
        return Optional.ofNullable(recursiveProperty);
    }

    @Override
    public String getJoinColumnName() {
        return TopiaEntity.PROPERTY_TOPIA_ID;
    }

    public String selectIdsPrototype(TopiaEntitySqlSelector selector) {
        return generatePrototype(selector, String.format("%s.%s", getTableName(), getJoinColumnName()));
    }

    public TopiaEntitySqlTable changeSelectors(List<TopiaEntitySqlSelector> newSelectors) {
        return new TopiaEntitySqlTable(
                getEntityName(),
                getSchemaName(),
                getTableName(),
                getAuthorizedColumnNames(),
                newSelectors,
                getBlobProperties(),
                getSimplePropertiesTypes(),
                getDecimalPropertiesScales(),
                getEnumerationPropertiesUsingName(),
                getOptionalRecursiveProperty().orElse(null));
    }

    public boolean isDecimalType(Class<?> type) {
        return DECIMAL_TYPES.contains(type);
    }
}
