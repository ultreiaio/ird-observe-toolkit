package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Set;

/**
 * Describes a sql association table for a given entity association.
 * <p>
 * Created on 22/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlAssociationTable extends AbstractTopiaEntitySqlAssociationTable {

    public TopiaEntitySqlAssociationTable(String entityName,
                                          String schemaName,
                                          String tableName,
                                          Set<String> authorizedColumnNames,
                                          List<TopiaEntitySqlSelector> selectors,
                                          String joinColumnName) {
        super(entityName,
              schemaName,
              tableName,
              authorizedColumnNames,
              selectors,
              joinColumnName);
    }
}
