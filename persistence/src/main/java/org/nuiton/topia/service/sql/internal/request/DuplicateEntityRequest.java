package org.nuiton.topia.service.sql.internal.request;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DuplicateRequest;
import org.nuiton.topia.service.sql.internal.SqlRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;

import java.util.Objects;

/**
 * Created on 15/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DuplicateEntityRequest implements SqlRequest {

    private final TopiaEntitySqlReplicatePlan plan;
    private final DuplicateRequest request;
    private final TopiaEntitySqlSelectArgument selectArgument;

    public DuplicateEntityRequest(TopiaEntitySqlReplicatePlan plan, DuplicateRequest request) {
        this.plan = Objects.requireNonNull(plan);
        this.request = Objects.requireNonNull(request);
        this.selectArgument = TopiaEntitySqlSelectArgument.of(request.getId());
    }

    public TopiaEntitySqlReplicatePlan getPlan() {
        return plan;
    }

    public DuplicateRequest getRequest() {
        return request;
    }

    public TopiaEntitySqlSelectArgument getSelectArgument() {
        return selectArgument;
    }

    public String getNewId() {
        return getRequest().getNewId();
    }

    public String getId() {
        return getRequest().getId();
    }
}

