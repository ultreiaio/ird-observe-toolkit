package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.DuplicateEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.util.Set;

/**
 * Created on 15/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DuplicateEntityConsumer implements SqlRequestConsumer<DuplicateEntityRequest> {

    @Override
    public void consume(DuplicateEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        // Never optimize queries here, since removing WHERE clause breaks some logic for multiple entity paths
        String ids = context.ids(selectArgument, false);

        TopiaEntitySqlReplicatePlan plan = request.getPlan();
        Set<String> shell = plan.getShell();
        DuplicateEntityWorkContext internalContext = new DuplicateEntityWorkContext(request.getId(), request.getNewId(), context);
        for (TopiaEntitySqlReplicatePlanTask task : plan) {
            boolean canExecute = task.accept(shell);
            if (canExecute) {
                DuplicateEntityWork readSqlWork = new DuplicateEntityWork(internalContext, task, ids);
                context.getSourcePersistenceContext().getSqlSupport().doSqlWork(readSqlWork);
            }
        }

    }
}
