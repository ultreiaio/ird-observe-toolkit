package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.ReplicateRequest;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.ReplicateEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created on 07/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.69
 */
public class ReplicateEntityConsumer implements SqlRequestConsumer<ReplicateEntityRequest> {

    @Override
    public void consume(ReplicateEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        // Never optimize queries here, since removing WHERE clause breaks some logic for multiple entity paths
        String ids = context.ids(selectArgument, false);

        TopiaEntitySqlReplicatePlan plan = request.getPlan();
        Set<String> shell = new LinkedHashSet<>(plan.getShell());
        ReplicateEntityWorkContext internalContext = new ReplicateEntityWorkContext(request.getOldParentId(), request.getNewParentId(), context, shell);
        for (TopiaEntitySqlReplicatePlanTask task : plan) {
            boolean canExecute = task.accept(shell);
            if (canExecute) {
                ReplicateEntityWork readSqlWork = new ReplicateEntityWork(internalContext, task, ids);
                context.getSourcePersistenceContext().getSqlSupport().doSqlWork(readSqlWork);
            } else {
                // this table was not used, so remove it from shell for children table to be skipped also
                shell.remove(task.getSchemaAndTableName());
            }
        }
        ReplicateRequest replicateRequest = request.getRequest();
        Map<String, String> replaceIds = internalContext.getReplaceIds();
        for (ReplicateRequestCallback callback : request.getCallbacks()) {
            callback.consume(replicateRequest, replaceIds, context);
        }
    }
}
