package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.28
 */
public class TopiaEntitySqlDescriptors implements Iterable<TopiaEntitySqlDescriptor> {

    private final List<TopiaEntitySqlDescriptor> descriptors;

    public TopiaEntitySqlDescriptors(TopiaEntitySqlDescriptor descriptor) {
        this(List.of(Objects.requireNonNull(descriptor)));
    }

    public TopiaEntitySqlDescriptors(List<TopiaEntitySqlDescriptor> descriptors) {
        this.descriptors = Objects.requireNonNull(descriptors);
    }

    public Stream<TopiaEntitySqlDescriptor> stream() {
        return descriptors.stream();
    }

    @Override
    public Iterator<TopiaEntitySqlDescriptor> iterator() {
        return descriptors.iterator();
    }

    public int size() {
        return descriptors.size();
    }

    public TopiaEntitySqlDescriptors reverse() {
        List<TopiaEntitySqlDescriptor> descriptors = new ArrayList<>(this.descriptors);
        Collections.reverse(descriptors);
        return new TopiaEntitySqlDescriptors(descriptors);
    }

    public List<String> getSchemaAndTableNames() {
        List<String> builder = new ArrayList<>(this.descriptors.size());
        descriptors.forEach(d -> builder.addAll(d.getSchemaAndTableNames()));
        return builder;
    }


    public List<Pair<String, String>> getTables(Iterable<Pair<String, String>> tables, Set<String> extraTables) {
        List<Pair<String, String>> result = new ArrayList<>();
        for (Pair<String, String> t : tables) {
            String table = t.getRight();
            String detectedType = null;
            for (TopiaEntitySqlDescriptor descriptor : this) {
                String name = descriptor.getTable().getTableName();
                if (table.equalsIgnoreCase(name) || table.startsWith(name + "_")) {
                    detectedType = name;
                    break;
                }
                for (TopiaEntitySqlAssociationTable association : descriptor.getAssociations()) {
                    name = association.getTableName();
                    if (table.equalsIgnoreCase(name) || table.startsWith(name + "_")) {
                        detectedType = name;
                        break;
                    }
                }
            }
            if (detectedType == null) {
                for (String extraType : extraTables) {
                    if (table.equalsIgnoreCase(extraType)) {
                        detectedType = extraType;
                        break;
                    }
                }
            }
            if (detectedType != null && !result.contains(t)) {
                result.add(t);
            }
        }
        Collections.sort(result);
        return result;
    }

}
