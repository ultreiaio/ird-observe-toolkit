package org.nuiton.topia.service.sql.internal;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.referential.OneSideSqlResultBuilder;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;
import org.nuiton.topia.persistence.TagValues;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.sql.TopiaEntitySqlModelResource;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlob;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobAdapter;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptorAdapter;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlReverseAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlReverseCompositionTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelector;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectorAdapter;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSimpleAssociationTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTableAdapter;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanAdapter;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTaskAdapter;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanAdapter;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanModel;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanTask;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanTaskAdapter;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlan;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanAdapter;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanModel;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTaskAdapter;
import org.nuiton.topia.service.sql.request.DeleteRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScript;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptAdapter;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptModel;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptModelAdapter;
import org.nuiton.topia.service.sql.usage.TopiaEntitySqlUsageModel;
import org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseMandatoryCompositionAdapter;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseAssociation;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseComposition;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseMandatoryComposition;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseManyToManyAssociation;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseOneToManyAssociation;
import org.nuiton.topia.service.sql.usage.TopiaUsageReverseOneToOneComposition;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;

import static org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlAssociationTableAdapter.TopiaEntitySqlAssociationTableAdapter;
import static org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlAssociationTableAdapter.TopiaEntitySqlReverseAssociationTableAdapter;
import static org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlAssociationTableAdapter.TopiaEntitySqlReverseCompositionTableAdapter;
import static org.nuiton.topia.service.sql.model.AbstractTopiaEntitySqlAssociationTableAdapter.TopiaEntitySqlSimpleAssociationTableAdapter;
import static org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseAssociationAdapter;
import static org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseCompositionAdapter;
import static org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseManyToManyAssociationAdapter;
import static org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseOneToManyAssociationAdapter;
import static org.nuiton.topia.service.sql.usage.TopiaUsageLinkAdapter.TopiaUsageReverseOneToOneCompositionAdapter;

/**
 * Created on 01/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.67
 */
public abstract class TopiaEntitySqlModelResourceImpl implements TopiaEntitySqlModelResource {

    private final SingletonSupplier<TopiaEntitySqlModel> model;
    private final SingletonSupplier<TopiaEntitySqlCopyPlanModel> copyPlanModel;
    private final SingletonSupplier<TopiaEntitySqlDeletePlanModel> deletePlanModel;
    private final SingletonSupplier<TopiaEntitySqlUsageModel> usageModel;
    private final SingletonSupplier<TopiaEntitySqlBlobModel> blobModel;
    private final SingletonSupplier<TopiaEntitySqlReplicatePlanModel> replicatePlanModel;
    private final SingletonSupplier<Set<ReplicatePartialRequestCallback>> replicatePartialRequestCallbacks;
    private final SingletonSupplier<Set<ReplicateRequestCallback>> replicateRequestCallbacks;
    private final SingletonSupplier<Set<DeleteRequestCallback>> deleteRequestCallbacks;
    /**
     * Persistence model tag-values.
     */
    private static TagValues persistenceTagValues;
    /**
     * Dto model tag-values.
     */
    private static TagValues dtoTagValues;
    /**
     * Cache of replication order for referential (based on dto types).
     */
    private static Set<Class<? extends ReferentialDto>> referentialForReplicationDto;
    /**
     * Project model name.
     */
    private final String modelName;
    /**
     * Supplier of entity sql script model.
     */
    private final SingletonSupplier<TopiaEntitySqlScriptModel> scriptModel;

    public static String toScriptModelLocation(String modelName) {
        return toLocation(TopiaEntitySqlScriptModel.class, modelName);
    }

    @Override
    public final TagValues getPersistenceTagValues() {
        if (persistenceTagValues == null) {
            persistenceTagValues = TagValues.persistence(modelName, ProjectPackagesDefinition::cleanType);
        }
        return persistenceTagValues;
    }

    @Override
    public final TagValues getDtoTagValues() {
        if (dtoTagValues == null) {
            dtoTagValues = TagValues.dto(modelName, ProjectPackagesDefinition::cleanType);
        }
        return dtoTagValues;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public final Set<Class<? extends ReferentialDto>> referentialForReplicationDto() {
        if (referentialForReplicationDto == null) {
            Set<Class<? extends ReferentialDto>> builder = new LinkedHashSet<>();
            for (TopiaEntitySqlDescriptor descriptor : getModel().getReplicationOrderWithStandaloneDescriptors()) {
                String entityName = descriptor.getTable().getEntityName();
                Class<? extends ReferentialEntity> entityType = Objects2.forName(entityName);
                ReferentialDtoEntityContext spi = PersistenceBusinessProject.fromReferentialEntity(entityType);
                if (spi != null) {
                    Class<? extends ReferentialDto> dtoType = spi.toDtoType();
                    builder.add(dtoType);
                }
            }
            referentialForReplicationDto = Collections.unmodifiableSet(builder);
        }
        return referentialForReplicationDto;
    }

    @Override
    public final OneSideSqlResultBuilder newOneSideSqlResultBuilder(ServiceContext context, Date defaultLastUpdateDate) {
        return new OneSideSqlResultBuilder(context, referentialForReplicationDto(), defaultLastUpdateDate);
    }

    @Override
    public final TopiaEntitySqlScriptModel getScriptModel() {
        return scriptModel.get();
    }

    public static GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
                .disableHtmlEscaping()
                .registerTypeAdapter(Class.class, new ClassAdapter())
                .registerTypeAdapter(TopiaEntitySqlBlob.class, new TopiaEntitySqlBlobAdapter())
                .registerTypeAdapter(TopiaUsageReverseOneToManyAssociation.class, new TopiaUsageReverseOneToManyAssociationAdapter())
                .registerTypeAdapter(TopiaUsageReverseOneToOneComposition.class, new TopiaUsageReverseOneToOneCompositionAdapter())
                .registerTypeAdapter(TopiaUsageReverseComposition.class, new TopiaUsageReverseCompositionAdapter())
                .registerTypeAdapter(TopiaUsageReverseMandatoryComposition.class, new TopiaUsageReverseMandatoryCompositionAdapter())
                .registerTypeAdapter(TopiaUsageReverseManyToManyAssociation.class, new TopiaUsageReverseManyToManyAssociationAdapter())
                .registerTypeAdapter(TopiaUsageReverseAssociation.class, new TopiaUsageReverseAssociationAdapter())
                .registerTypeAdapter(TopiaEntitySqlDescriptor.class, new TopiaEntitySqlDescriptorAdapter())
                .registerTypeAdapter(TopiaEntitySqlTable.class, new TopiaEntitySqlTableAdapter())
                .registerTypeAdapter(TopiaEntitySqlSelector.class, new TopiaEntitySqlSelectorAdapter())
                .registerTypeAdapter(TopiaEntitySqlAssociationTable.class, new TopiaEntitySqlAssociationTableAdapter())
                .registerTypeAdapter(TopiaEntitySqlSimpleAssociationTable.class, new TopiaEntitySqlSimpleAssociationTableAdapter())
                .registerTypeAdapter(TopiaEntitySqlReverseAssociationTable.class, new TopiaEntitySqlReverseAssociationTableAdapter())
                .registerTypeAdapter(TopiaEntitySqlReverseCompositionTable.class, new TopiaEntitySqlReverseCompositionTableAdapter())
                .registerTypeAdapter(TopiaEntitySqlDeletePlan.class, new TopiaEntitySqlDeletePlanAdapter())
                .registerTypeAdapter(TopiaEntitySqlDeletePlanTask.class, new TopiaEntitySqlDeletePlanTaskAdapter())
                .registerTypeAdapter(TopiaEntitySqlCopyPlan.class, new TopiaEntitySqlCopyPlanAdapter())
                .registerTypeAdapter(TopiaEntitySqlCopyPlanTask.class, new TopiaEntitySqlCopyPlanTaskAdapter())
                .registerTypeAdapter(TopiaEntitySqlReplicatePlan.class, new TopiaEntitySqlReplicatePlanAdapter())
                .registerTypeAdapter(TopiaEntitySqlReplicatePlanTask.class, new TopiaEntitySqlReplicatePlanTaskAdapter())
                .registerTypeAdapter(TopiaEntitySqlScript.class, new TopiaEntitySqlScriptAdapter())
                .registerTypeAdapter(TopiaEntitySqlScriptModel.class, new TopiaEntitySqlScriptModelAdapter())
                .setPrettyPrinting();
    }

    public static String toModelLocation(String modelName) {
        return toLocation(TopiaEntitySqlModel.class, modelName);
    }

    public static String toCopyPlanLocation(String modelName) {
        return toLocation(TopiaEntitySqlCopyPlanModel.class, modelName);
    }

    public static String toReplicatePlanLocation(String modelName) {
        return toLocation(TopiaEntitySqlReplicatePlanModel.class, modelName);
    }

    public static String toDeletePlanLocation(String modelName) {
        return toLocation(TopiaEntitySqlDeletePlanModel.class, modelName);
    }

    public static String toUsageModelLocation(String modelName) {
        return toLocation(TopiaEntitySqlUsageModel.class, modelName);
    }

    public static String toBlobModelLocation(String modelName) {
        return toLocation(TopiaEntitySqlBlobModel.class, modelName);
    }

    public static String toLocation(Class<?> type, String modelName) {
        return String.format("META-INF/persistence/%s/%s.json", Objects.requireNonNull(modelName), type.getSimpleName());
    }

    public static <O> O load(Class<O> type, String location, Gson gson) {
        URL resource = Objects.requireNonNull(type).getClassLoader().getResource(Objects.requireNonNull(location));
        try (Reader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(resource).openStream(), StandardCharsets.UTF_8))) {
            return Objects.requireNonNull(gson).fromJson(reader, type);
        } catch (IOException e) {
            throw new TopiaException(String.format("Could not load resource: %s (for type: %s)", resource, type.getName()), e);
        }
    }

    protected TopiaEntitySqlModelResourceImpl(String modelName, Gson gson) {
        this.modelName = modelName;
        this.model = SingletonSupplier.of(() -> load(TopiaEntitySqlModel.class, toModelLocation(modelName), gson));
        this.copyPlanModel = SingletonSupplier.of(() -> load(TopiaEntitySqlCopyPlanModel.class, toCopyPlanLocation(modelName), gson));
        this.replicatePlanModel = SingletonSupplier.of(() -> load(TopiaEntitySqlReplicatePlanModel.class, toReplicatePlanLocation(modelName), gson));
        this.deletePlanModel = SingletonSupplier.of(() -> load(TopiaEntitySqlDeletePlanModel.class, toDeletePlanLocation(modelName), gson));
        this.usageModel = SingletonSupplier.of(() -> load(TopiaEntitySqlUsageModel.class, toUsageModelLocation(modelName), gson));
        this.blobModel = SingletonSupplier.of(() -> load(TopiaEntitySqlBlobModel.class, toBlobModelLocation(modelName), gson));
        this.replicatePartialRequestCallbacks = SingletonSupplier.of(() -> {
            Set<ReplicatePartialRequestCallback> callbacks = new LinkedHashSet<>();
            for (ReplicatePartialRequestCallback callback : ServiceLoader.load(ReplicatePartialRequestCallback.class)) {
                callbacks.add(callback);
            }
            return Collections.unmodifiableSet(callbacks);
        });
        this.replicateRequestCallbacks = SingletonSupplier.of(() -> {
            Set<ReplicateRequestCallback> callbacks = new LinkedHashSet<>();
            for (ReplicateRequestCallback callback : ServiceLoader.load(ReplicateRequestCallback.class)) {
                callbacks.add(callback);
            }
            return Collections.unmodifiableSet(callbacks);
        });
        this.deleteRequestCallbacks = SingletonSupplier.of(() -> {
            Set<DeleteRequestCallback> callbacks = new LinkedHashSet<>();
            for (DeleteRequestCallback callback : ServiceLoader.load(DeleteRequestCallback.class)) {
                callbacks.add(callback);
            }
            return Collections.unmodifiableSet(callbacks);
        });
        this.scriptModel = SingletonSupplier.of(() -> load(TopiaEntitySqlScriptModel.class, toScriptModelLocation(modelName), gson));
    }

    @Override
    public Set<ReplicatePartialRequestCallback> getReplicatePartialRequestCallbacks() {
        return replicatePartialRequestCallbacks.get();
    }

    @Override
    public Set<ReplicateRequestCallback> getReplicateRequestCallbacks() {
        return replicateRequestCallbacks.get();
    }

    @Override
    public Set<DeleteRequestCallback> getDeleteRequestCallbacks() {
        return deleteRequestCallbacks.get();
    }

    @Override
    public TopiaEntitySqlModel getModel() {
        return model.get();
    }

    @Override
    public TopiaEntitySqlCopyPlanModel getCopyPlanModel() {
        return copyPlanModel.get();
    }

    @Override
    public TopiaEntitySqlReplicatePlanModel getReplicatePlanModel() {
        return replicatePlanModel.get();
    }

    @Override
    public TopiaEntitySqlDeletePlanModel getDeletePlanModel() {
        return deletePlanModel.get();
    }

    @Override
    public TopiaEntitySqlUsageModel getUsageModel() {
        return usageModel.get();
    }

    @Override
    public TopiaEntitySqlBlobModel getBlobModel() {
        return blobModel.get();
    }
}
