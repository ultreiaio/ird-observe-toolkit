package org.nuiton.topia.service.migration;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceProvider;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;
import org.nuiton.topia.service.migration.version.TMSVersion;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Contains all states of the migration service.
 * <p>
 * Created by tchemit on 05/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class TopiaMigrationServiceContext {

    private static final Logger log = LogManager.getLogger(TopiaMigrationServiceContext.class);

    /**
     * Service configuration.
     */
    protected final TopiaMigrationServiceConfiguration configuration;
    /**
     * Migration sql helper (could be legacy or default).
     */
    protected final MigrationServiceSqlHelper sqlHelper;
    /**
     * Available migration resources found in class-path.
     */
    protected final MigrationVersionResourceProvider resources;
    /**
     * Path where to store sql scripts.
     */
    protected final Path scriptPath;
    /**
     * Is database is not versioned?
     */
    protected final boolean dbNotVersioned;
    /**
     * Is legacy migration table exists? (only used with default mode)
     */
    protected final boolean legacyVersionTableExist;
    /**
     * Is default migration schema exists?
     */
    protected final boolean versionSchemaExist;
    /**
     * Is default migration table exists?
     */
    protected final boolean versionTableExist;
    /**
     * Can we apply migration versions?
     */
    protected final boolean canApplyMigrationVersions;
    /**
     * Migration versions to apply.
     */
    protected final List<Version> versionsToApply;
    /**
     * Current database version.
     */
    protected Version dbVersion;

    public static TopiaMigrationServiceContext create(TopiaMigrationServiceConfiguration configuration) {
        if (MigrationServiceSqlHelper.isLegacy()) {
            return createLegacy(configuration);
        } else {
            return createDefault(configuration);
        }
    }

    public static TopiaMigrationServiceContext createDefault(TopiaMigrationServiceConfiguration configuration) {
        TopiaApplicationContext<?> topiaApplicationContext = configuration.getApplicationContext();
        boolean versionSchemaExist;
        boolean versionTableExist;
        boolean legacyVersionTableExist = false;
        boolean dbNotVersioned = false;
        Version dbVersion;
        Version v = null;

        JdbcHelper jdbcHelper = topiaApplicationContext.newJdbcHelper();

        MigrationServiceSqlHelper sqlHelper = MigrationServiceSqlHelper.DEFAULT;
        MigrationServiceSqlHelper legacySqlHelper = MigrationServiceSqlHelper.LEGACY;
        try {
            versionTableExist = sqlHelper.isTableExist(jdbcHelper);
            if (versionTableExist) {
                log.info("Detected standard migration table {}.", sqlHelper.gav());
                versionSchemaExist = true;
                Optional<TMSVersion> tmsVersion = sqlHelper.getVersion(jdbcHelper);
                if (tmsVersion.isPresent()) {
                    v = tmsVersion.get().getVersion();
                }
                if (v == null) {
                    log.warn(String.format("Version not found on table %s", sqlHelper.gav()));
                }
            } else {
                versionSchemaExist = sqlHelper.isSchemaExist(jdbcHelper);
                legacyVersionTableExist = legacySqlHelper.isTableExist(jdbcHelper);
                if (legacyVersionTableExist) {
                    log.info("Detected legacy migration table {}.", legacySqlHelper.gav());
                    Optional<TMSVersion> tmsVersion = legacySqlHelper.getVersion(jdbcHelper);
                    if (tmsVersion.isPresent()) {
                        v = tmsVersion.get().getVersion();
                    }
                    if (v == null) {
                        log.warn(String.format("Version not found on legacy table %s", legacySqlHelper.gav()));
                    }
                }
            }
        } finally {
            if (v == null) {
                // If no version found, db not versioned
                dbNotVersioned = true;
                try {
                    // We can still try to detect database version from the structure of the database
                    // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2712
                    v = detectVersionFromStructure(jdbcHelper);
                    log.warn("Database version table not found, but database version was guessed : {}", v);
                } catch (SQLException ignored) {
                }
            } else {
                log.info(String.format("Detected database version: %s", v));
            }
            dbVersion = v;
        }
        return new TopiaMigrationServiceContext(configuration,
                                                sqlHelper,
                                                legacyVersionTableExist,
                                                versionSchemaExist,
                                                versionTableExist,
                                                dbNotVersioned,
                                                dbVersion,
                                                MigrationVersionResourceProvider.get()
        );
    }

    private static Version detectVersionFromStructure(JdbcHelper jdbcHelper) throws SQLException {
        if (!jdbcHelper.isSchemaExist("ps_logbook")) {
            // We are before v9.0
            return Version.valueOf("7.6");
        }
        // try to detect v9.0
        if (jdbcHelper.isTableExist("ps_logbook", "wellPlan")) {
            // We are on a version 9.0.x
            return Version.valueOf("9.0");
        }
        // try to detect v9.1
        if (jdbcHelper.isTableExist("ps_logbook", "well")) {
            // We are on a version 9.1.x
            return Version.valueOf("9.1");
        }
        // We could not find the database version
        throw new TopiaMigrationServiceException("Could not detected database version, no migration table found.");
    }

    public static TopiaMigrationServiceContext createLegacy(TopiaMigrationServiceConfiguration configuration) {
        TopiaApplicationContext<?> topiaApplicationContext = configuration.getApplicationContext();
        boolean versionTableExist;
        boolean dbNotVersioned = false;
        Version dbVersion;
        Version v = null;

        JdbcHelper jdbcHelper = topiaApplicationContext.newJdbcHelper();

        MigrationServiceSqlHelper sqlHelper = MigrationServiceSqlHelper.LEGACY;

        try {
            versionTableExist = sqlHelper.isTableExist(jdbcHelper);
            if (versionTableExist) {
                log.info("Detected legacy migration table {}.", sqlHelper.gav());
                Optional<TMSVersion> tmsVersion = sqlHelper.getVersion(jdbcHelper);
                if (tmsVersion.isPresent()) {
                    v = tmsVersion.get().getVersion();
                }
                if (v == null) {
                    log.warn(String.format("Version not found on table %s", sqlHelper.gav()));
                }
            }
        } finally {
            if (v == null) {
                // la base dans ce cas n'est pas versionee.
                // On dit que la version de la base est 0
                // et les schema de cette version 0 doivent
                // etre detenu en local
                v = Version.VZERO;
                dbNotVersioned = true;
                log.info("Database version not found, so database schema is considered as V0");
            } else {
                log.info(String.format("Detected database version: %s", v));
            }
            dbVersion = v;
        }
        return new TopiaMigrationServiceContext(configuration,
                                                sqlHelper,
                                                false,
                                                true,
                                                versionTableExist,
                                                dbNotVersioned,
                                                dbVersion,
                                                MigrationVersionResourceProvider.get()
        );
    }

    protected TopiaMigrationServiceContext(TopiaMigrationServiceConfiguration configuration,
                                           MigrationServiceSqlHelper sqlHelper,
                                           boolean legacyVersionTableExist,
                                           boolean versionSchemaExist,
                                           boolean versionTableExist,
                                           boolean dbNotVersioned,
                                           Version dbVersion,
                                           MigrationVersionResourceProvider resources) {
        this.configuration = configuration;
        this.sqlHelper = sqlHelper;
        this.versionSchemaExist = versionSchemaExist;
        this.versionTableExist = versionTableExist;
        this.dbNotVersioned = dbNotVersioned;
        this.dbVersion = dbVersion;
        this.resources = resources;
        this.legacyVersionTableExist = legacyVersionTableExist;
        try {
            this.scriptPath = Files.createTempDirectory(configuration.getTemporaryDirectory(), "topia-migration-service");
        } catch (IOException e) {
            throw new IllegalStateException("Can't create scripts path", e);
        }

        List<Version> allVersions = getResources().getAvailableVersions();
        log.info(String.format("Detected available versions: %1$s", allVersions));

        versionsToApply = getResources().getVersionsAfter(dbVersion);
        log.info(String.format("Detected versions to apply: %1$s", versionsToApply));

        Optional<TopiaMigrationServiceAskUserToMigrate> askUserToMigrate = Optional.ofNullable(configuration.getCallback());

        // ask to perform the migration
        canApplyMigrationVersions = askUserToMigrate.map(c -> c.canIMigrate(getDbVersion(), versionsToApply)).orElse(true);
        if (!canApplyMigrationVersions) {
            log.warn("Detected that we can not migrate versions (maybe version of database is too old?).");
        }
    }

    public Path getScriptPath() {
        return scriptPath;
    }

    public Version getModelVersion() {
        return configuration.getModelVersion();
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public MigrationVersionResourceProvider getResources() {
        return resources;
    }

    public boolean adaptVersionTable(TopiaSqlSupport sqlSupport) {

        Version modelVersion = getModelVersion();
        boolean dbVersionEqualsModelVersion = Objects.equals(dbVersion, modelVersion);

        if (!versionSchemaExist) {
            log.info("Create default migration schema.");
            sqlHelper.createSchema(sqlSupport);
        }
        if (!versionTableExist) {
            log.info("Create default migration table.");
            sqlHelper.createTable(sqlSupport);
        } else {
            if (dbVersionEqualsModelVersion) {
                log.info("Database is up to date, no migration needed.");
                return false;
            }
        }
        if (legacyVersionTableExist) {
            log.info("Drop legacy migration table.");
            MigrationServiceSqlHelper.LEGACY.dropTable(sqlSupport);
            if (dbVersionEqualsModelVersion) {
                log.info("Database is up to date (version: {}), but was coming from legacy migration table, fill new migration table.", modelVersion);
                saveVersion(sqlSupport, modelVersion, null);
                return false;
            }
            log.info("Fill version table {} from legacy table.", dbVersion);
            saveVersion(sqlSupport, dbVersion, null);
        } else if (dbNotVersioned) {
            if (Version.VZERO.equals(dbVersion)) {
                log.info("Database is empty, no migration needed, set current version {}", modelVersion);
                saveVersion(sqlSupport, modelVersion, new Date());
                return false;
            } else {
                log.info("Fill version table {} from guessed version (even if no legacy nor default version table detected).", dbVersion);
                saveVersion(sqlSupport, dbVersion, null);
            }
        }
        // In all other cases, we can try to perform migration
        return true;
    }

    public List<Version> getVersionsToApply() {

        if (versionsToApply.isEmpty()) {
            log.info("No version to apply, no migration needed.");
            // No migration, so nothing to save
//            context.saveModelVersion();
            return null;
        }

        // ask to perform the migration

        if (!canApplyMigrationVersions) {
            // user cancel migration
            log.warn("Migration was cancelled by migration callback (maybe version of database is too old?).");
            return null;
        }
        return versionsToApply;
    }

    protected void saveVersion(TopiaSqlSupport sqlSupport, Version version, Date date) {
        log.info(String.format("[ Version %s ] Saving new database version (date: %s).", version, date));
        sqlHelper.save(sqlSupport, version.getVersion(), date);
        dbVersion = version;
    }

    public TopiaPersistenceContext newPersistenceContext() {
        return configuration.getApplicationContext().newPersistenceContext();
    }

    public MigrationVersionResource getResource(Version version) {
        return resources.getResource(version);
    }

    public MigrationVersionResourceExecutor newExecutor(MigrationVersionResource migrationVersionResource, TopiaSqlSupport sqlSupport) {
        return new MigrationVersionResourceExecutor(migrationVersionResource, sqlSupport, configuration.getClassifier(), getScriptPath(), migrationVersionResource.getScriptVariables());
    }

}
