package org.nuiton.topia.service.sql.script;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Objects;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public class TopiaEntitySqlScriptModel {

    private final Map<String, TopiaEntitySqlScript> scripts;

    public TopiaEntitySqlScriptModel(Map<String, TopiaEntitySqlScript> scripts) {
        this.scripts = Objects.requireNonNull(scripts);
    }

    public TopiaEntitySqlScript getScript(String type) {
        return scripts.get(type);
    }

    public Map<String, TopiaEntitySqlScript> scripts() {
        return scripts;
    }
}
