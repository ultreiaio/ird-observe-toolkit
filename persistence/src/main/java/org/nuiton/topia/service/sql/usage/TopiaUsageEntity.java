package org.nuiton.topia.service.sql.usage;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Describes all usages of an entity type.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class TopiaUsageEntity {

    private final List<TopiaUsageReverseComposition> reverseCompositions;
    private final List<TopiaUsageReverseMandatoryComposition> reverseMandatoryCompositions;
    private final List<TopiaUsageReverseManyToManyAssociation> reverseManyToManyAssociations;
    private final List<TopiaUsageReverseOneToManyAssociation> reverseOneToManyAssociations;
    private final List<TopiaUsageReverseOneToOneComposition> reverseOneToOneCompositions;
    private final List<TopiaUsageReverseAssociation> reverseAssociations;

    protected TopiaUsageEntity(List<TopiaUsageReverseComposition> reverseCompositions,
                               List<TopiaUsageReverseMandatoryComposition> reverseMandatoryCompositions,
                               List<TopiaUsageReverseManyToManyAssociation> reverseManyToManyAssociations,
                               List<TopiaUsageReverseOneToManyAssociation> reverseOneToManyAssociations,
                               List<TopiaUsageReverseOneToOneComposition> reverseOneToOneCompositions,
                               List<TopiaUsageReverseAssociation> reverseAssociations) {
        this.reverseCompositions = reverseCompositions;
        this.reverseMandatoryCompositions = reverseMandatoryCompositions;
        this.reverseManyToManyAssociations = reverseManyToManyAssociations;
        this.reverseOneToManyAssociations = reverseOneToManyAssociations;
        this.reverseOneToOneCompositions = reverseOneToOneCompositions;
        this.reverseAssociations = reverseAssociations;
    }

    public void countReverseCompositions(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        compositions(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public void countReverseMandatoryCompositions(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        mandatoryCompositions(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public void countReverseAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        associations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public void countReverseManyToManyAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        manyToManyAssociations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public void countReverseOneToManyAssociations(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        oneToManyAssociations(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public void countReverseOneToOneCompositions(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Predicate<Class<? extends TopiaEntity>> entityTypeFilter, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        oneToOneCompositions(entityTypeFilter).ifPresent(links -> count(daoSupplier, links, entity, result, optionalPredicate));
    }

    public <E extends TopiaEntity> void findReverseComposition(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        compositions(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseMandatoryComposition(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        mandatoryCompositions(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseOneToManyAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        oneToManyAssociations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseOneToOneComposition(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        oneToOneCompositions(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseManyToManyAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        manyToManyAssociations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    public <E extends TopiaEntity> void findReverseAssociation(TopiaDaoSupplier daoSupplier, TopiaEntity entity, Class<E> entityType, Collection<E> result) {
        associations(entityType::equals).ifPresent(links -> find(daoSupplier, links, entity, result));
    }

    protected void count(TopiaDaoSupplier daoSupplier, Stream<? extends TopiaUsageLink> links, TopiaEntity entity, Map<Class<? extends TopiaEntity>, Set<String>> result, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        links.forEach(link -> {
            if (optionalPredicate != null && !optionalPredicate.test(link.getType())) {
                return;
            }
            List<String> linkResult = link.findIds(daoSupplier, entity);
            if (!linkResult.isEmpty()) {
                Set<String> typeResult = result.computeIfAbsent(link.getType(), k -> new TreeSet<>());
                typeResult.addAll(linkResult);
            }
        });
    }

    protected <E extends TopiaEntity> void find(TopiaDaoSupplier daoSupplier, Stream<? extends TopiaUsageLink> links, TopiaEntity entity, Collection<E> result) {
        links.forEach(link -> {
            List<E> count = link.find(daoSupplier, entity);
            result.addAll(count);
        });
    }

    protected Optional<Stream<TopiaUsageReverseComposition>> compositions(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseCompositions().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseMandatoryComposition>> mandatoryCompositions(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseMandatoryCompositions().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseAssociation>> associations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseManyToManyAssociation>> manyToManyAssociations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseManyToManyAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseOneToManyAssociation>> oneToManyAssociations(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseOneToManyAssociations().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<Stream<TopiaUsageReverseOneToOneComposition>> oneToOneCompositions(Predicate<Class<? extends TopiaEntity>> entityTypeFilter) {
        return reverseOneToOneCompositions().map(p -> p.stream().filter(l -> entityTypeFilter.test(l.getType())));
    }

    protected Optional<List<TopiaUsageReverseComposition>> reverseCompositions() {
        return Optional.ofNullable(reverseCompositions);
    }

    protected Optional<List<TopiaUsageReverseMandatoryComposition>> reverseMandatoryCompositions() {
        return Optional.ofNullable(reverseMandatoryCompositions);
    }

    protected Optional<List<TopiaUsageReverseManyToManyAssociation>> reverseManyToManyAssociations() {
        return Optional.ofNullable(reverseManyToManyAssociations);
    }

    protected Optional<List<TopiaUsageReverseOneToManyAssociation>> reverseOneToManyAssociations() {
        return Optional.ofNullable(reverseOneToManyAssociations);
    }

    protected Optional<List<TopiaUsageReverseOneToOneComposition>> reverseOneToOneCompositions() {
        return Optional.ofNullable(reverseOneToOneCompositions);
    }

    protected Optional<List<TopiaUsageReverseAssociation>> reverseAssociations() {
        return Optional.ofNullable(reverseAssociations);
    }
}
