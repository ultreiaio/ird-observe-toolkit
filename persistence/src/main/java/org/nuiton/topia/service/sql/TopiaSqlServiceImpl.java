package org.nuiton.topia.service.sql;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.SqlServiceRequest;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.referential.OneSideSqlResultBuilder;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TagValues;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.service.sql.blob.TopiaEntitySqlBlobModel;
import org.nuiton.topia.service.sql.internal.SqlRequestSet;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumer;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanModel;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanModel;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanModel;
import org.nuiton.topia.service.sql.request.DeleteRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicatePartialRequestCallback;
import org.nuiton.topia.service.sql.request.ReplicateRequestCallback;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScriptModel;
import org.nuiton.topia.service.sql.usage.TopiaEntitySqlUsageModel;

import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Service to produce high level sql scripts from sql model.
 * <p>
 * Created on 24/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class TopiaSqlServiceImpl implements TopiaSqlService {
    private static final TimeLog TIME_LOG = new TimeLog(TopiaSqlServiceImpl.class, 100, 1000);
    private static final Logger log = LogManager.getLogger(TopiaSqlServiceImpl.class);
    /**
     * Configuration of service.
     */
    protected TopiaSqlServiceConfiguration configuration;

    @Override
    public TopiaEntitySqlModel getModel() {
        return configuration.getModel();
    }

    @Override
    public TopiaEntitySqlCopyPlanModel getCopyPlanModel() {
        return configuration.getCopyPlanModel();
    }

    @Override
    public TopiaEntitySqlReplicatePlanModel getReplicatePlanModel() {
        return configuration.getReplicatePlanModel();
    }

    @Override
    public TopiaEntitySqlDeletePlanModel getDeletePlanModel() {
        return configuration.getDeletePlanModel();
    }

    @Override
    public TopiaEntitySqlUsageModel getUsageModel() {
        return configuration.getUsageModel();
    }

    @Override
    public TopiaEntitySqlBlobModel getBlobModel() {
        return configuration.getBlobModel();
    }

    @Override
    public Set<ReplicatePartialRequestCallback> getReplicatePartialRequestCallbacks() {
        return configuration.getReplicatePartialRequestCallbacks();
    }

    @Override
    public Set<ReplicateRequestCallback> getReplicateRequestCallbacks() {
        return configuration.getReplicateRequestCallbacks();
    }

    @Override
    public Set<DeleteRequestCallback> getDeleteRequestCallbacks() {
        return configuration.getDeleteRequestCallbacks();
    }

    @Override
    public TagValues getPersistenceTagValues() {
        return configuration.getPersistenceTagValues();
    }

    @Override
    public TagValues getDtoTagValues() {
        return configuration.getDtoTagValues();
    }

    @Override
    public Set<Class<? extends ReferentialDto>> referentialForReplicationDto() {
        return configuration.referentialForReplicationDto();
    }

    @Override
    public OneSideSqlResultBuilder newOneSideSqlResultBuilder(ServiceContext context, Date defaultLastUpdateDate) {
        return configuration.newOneSideSqlResultBuilder(context, defaultLastUpdateDate);
    }

    @Override
    public TopiaEntitySqlScriptModel getScriptModel() {
        return configuration.getScriptModel();
    }

    @Override
    public void initTopiaService(TopiaApplicationContext<?> topiaApplicationContext, Map<String, String> serviceConfiguration) {
        this.configuration = TopiaSqlServiceConfiguration.of(Objects.requireNonNull(topiaApplicationContext), topiaApplicationContext);
    }

    @Override
    public SqlScript consume(SqlRequestSet request) throws TopiaSqlServiceException {
        return SqlRequestSetConsumer.consume(configuration.getApplicationContext(), request);
    }

    @Override
    public SqlScript consume(SqlServiceRequest request) throws TopiaSqlServiceException {
        long t0 = TimeLog.getTime();
        try {
            Path targetPath = configuration.newTemporaryDirectory(request);
            log.info(String.format("Will consume request %s", request));
            log.info(String.format("Generate to %s", targetPath));
            SqlRequestSet sqlRequestSet = configuration.buildSqlRequestSet(request, targetPath);
            return consume(sqlRequestSet);
        } catch (Exception e) {
            throw new TopiaSqlServiceException(String.format("Could not produce sql script for: %s", request), e);
        } finally {
            TIME_LOG.log(t0, "consume request " + request.getClass().getName(), request.toString());
        }
    }

    @Override
    public void close() {
        configuration = null;
    }
}
