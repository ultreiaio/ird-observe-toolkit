package org.nuiton.topia.service.sql.script;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonHelper;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public class TopiaEntitySqlScriptAdapter implements JsonDeserializer<TopiaEntitySqlScript>, JsonSerializer<TopiaEntitySqlScript> {

    @Override
    public TopiaEntitySqlScript deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        UpdateLastUpdateDateFieldScript fieldScript = new UpdateLastUpdateDateFieldScript(JsonHelper.readStringList(context, UpdateLastUpdateDateFieldScript.class.getName(), object));
        UpdateLastUpdateDateTableScript tableScript = new UpdateLastUpdateDateTableScript(JsonHelper.readStringList(context, UpdateLastUpdateDateTableScript.class.getName(), object));
        List<String> replaceReferentialInDataRequests = JsonHelper.readStringList(context, ReplaceReferentialInDataScript.class.getName(), object);
        List<String> replaceReferentialInReferentialRequests = JsonHelper.readStringList(context, ReplaceReferentialInReferentialScript.class.getName(), object);
        List<String> deleteReferentialRequests = JsonHelper.readStringList(context, DeleteReferentialScript.class.getName(), object);
        List<String> disableReferentialRequests = JsonHelper.readStringList(context, DisableReferentialScript.class.getName(), object);
        ReplaceReferentialInDataScript replaceReferentialInDataScript = replaceReferentialInDataRequests == null ? null : new ReplaceReferentialInDataScript(replaceReferentialInDataRequests);
        ReplaceReferentialInReferentialScript replaceReferentialInReferentialScript = replaceReferentialInReferentialRequests == null ? null : new ReplaceReferentialInReferentialScript(replaceReferentialInReferentialRequests);
        DeleteReferentialScript deleteReferentialScript = deleteReferentialRequests == null ? null : new DeleteReferentialScript(deleteReferentialRequests);
        DisableReferentialScript disableReferentialScript = disableReferentialRequests == null ? null : new DisableReferentialScript(disableReferentialRequests);
        return new TopiaEntitySqlScript(fieldScript,
                tableScript,
                replaceReferentialInDataScript,
                replaceReferentialInReferentialScript,
                deleteReferentialScript,
                disableReferentialScript);
    }

    @Override
    public JsonElement serialize(TopiaEntitySqlScript src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        JsonHelper.addToResult(context, result, UpdateLastUpdateDateFieldScript.class.getName(), src.getLastUpdateDateFieldScript().requests());
        JsonHelper.addToResult(context, result, UpdateLastUpdateDateTableScript.class.getName(), src.getLastUpdateDateTableScript().requests());
        src.getReplaceReferentialInDataScript().ifPresent(script -> JsonHelper.addToResult(context, result, ReplaceReferentialInDataScript.class.getName(), script.requests()));
        src.getReplaceReferentialInReferentialScript().ifPresent(script -> JsonHelper.addToResult(context, result, ReplaceReferentialInReferentialScript.class.getName(), script.requests()));
        src.getDeleteReferentialScript().ifPresent(script -> JsonHelper.addToResult(context, result, DeleteReferentialScript.class.getName(), script.requests()));
        src.getDisableReferentialScript().ifPresent(script -> JsonHelper.addToResult(context, result, DisableReferentialScript.class.getName(), script.requests()));
        return result;
    }
}

