package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlWork;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 15/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DuplicateEntityWork implements SqlWork {

    private static final TimeLog TIME_LOG = new TimeLog(DuplicateEntityWork.class, 500L, 1000L);
    private final DuplicateEntityWorkContext context;
    private final String sql;
    private final Map<String, BlobsContainer.Builder> blobsBuilder;
    private final Set<String> blobColumns;
    private final List<String> columnNames;
    private final String insertStatementSql;
    private final String columnToReplace;

    public DuplicateEntityWork(DuplicateEntityWorkContext context, TopiaEntitySqlReplicatePlanTask task, String ids) {
        this.context = context;
        this.columnNames = task.getColumnNames();
        String selectClause = SqlRequestConsumer.getSelectClause(task.getTableName(), columnNames);
        this.sql = TopiaEntitySqlReplicatePlanTask.applyIds(task.getSelectSql(), selectClause, ids);

        this.blobsBuilder = context.initBlobsBuilder(task.useBlob(), task.getSchemaAndTableName());
        this.blobColumns = blobsBuilder.keySet();
        this.insertStatementSql = SqlRequestConsumer.newInsertStatementSql(task.getInsertSql(), columnNames, blobColumns);
        if (task.isEntryPoint()) {
            this.columnToReplace = TopiaEntity.PROPERTY_TOPIA_ID;
        } else {
            // must have exactly one column to replace
            if (task.getColumnsToReplace().size() != 1) {
                throw new IllegalStateException("Should have exactly one column to replace with task: " + task);
            }
            this.columnToReplace = task.getColumnsToReplace().iterator().next();
        }
    }

    public String getNewId() {
        return context.getNewId();
    }

    public void writeSql(String sql) {
        context.writeSql(sql);
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        long t0 = TimeLog.getTime();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setFetchSize(context.getReadFetchSize());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String sql = generateInsertSqlStatement(statement, resultSet);
                    context.writeSql(sql);
                }
            }
        } finally {
            TIME_LOG.log(t0, "Executed on table", sql);
        }
    }

    private String generateInsertSqlStatement(PreparedStatement statement, ResultSet resultSet) throws SQLException {
        StringBuilder argumentsBuilder = new StringBuilder();
        for (String columnName : columnNames) {
            if (blobColumns.contains(columnName)) {
                // processed later
                continue;
            }
            Object columnValue = resultSet.getObject(columnName);
            if (columnName.equals(columnToReplace)) {
                argumentsBuilder.append(", '").append(getNewId()).append("'");
                continue;
            }
            if (columnValue == null) {
                argumentsBuilder.append(", NULL");
                continue;
            }
            if (columnValue instanceof String) {
                String stringValue = (String) columnValue;
                argumentsBuilder.append(", '").append(stringValue.replaceAll("'", "''")).append("'");
                continue;
            }
            if (columnValue instanceof Date) {
                argumentsBuilder.append(", '").append(columnValue).append("'");
                continue;
            }
            argumentsBuilder.append(", ").append(columnValue);
        }
        for (String columnName : blobColumns) {
            Object columnValue = resultSet.getObject(columnName);
            if (blobColumns.contains(columnName)) {
                context.copyBlob(statement, context.getId(), columnValue, blobsBuilder.get(columnName));
            }
        }
        String arguments = argumentsBuilder.substring(2);
        return String.format(insertStatementSql, arguments);
    }
}
