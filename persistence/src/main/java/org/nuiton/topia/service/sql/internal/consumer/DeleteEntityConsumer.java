package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.DeleteEntityRequest;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlan;
import org.nuiton.topia.service.sql.plan.delete.TopiaEntitySqlDeletePlanTask;
import org.nuiton.topia.service.sql.request.DeleteRequestCallback;

/**
 * Created on 25/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class DeleteEntityConsumer implements SqlRequestConsumer<DeleteEntityRequest> {

    @Override
    public void consume(DeleteEntityRequest request, SqlRequestSetConsumerContext context) {
        TopiaEntitySqlDeletePlan deletePlan = request.getDeletePlan();
        TopiaEntitySqlSelectArgument selectArgument = request.getSelectArgument();
        // Never use optimize ids (this can delete all data!)
        String ids = context.ids(selectArgument, false);
        SqlScriptWriter writer = context.getWriter();
        for (TopiaEntitySqlDeletePlanTask task : deletePlan) {
            consume(writer, ids, task);
        }
        DeleteRequest deleteRequest = request.getRequest();
        for (DeleteRequestCallback callback : request.getCallbacks()) {
            callback.consume(deleteRequest, context);
        }
    }

    protected void consume(SqlScriptWriter writer, String ids, TopiaEntitySqlDeletePlanTask task) {
        String deleteSql = task.getDeleteSql();
        String realDeleteSql = task.applyIds(deleteSql, ids);
        writer.writeSql(realDeleteSql);
    }
}
