package org.nuiton.topia.service.sql.model;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Describes the sql metadata model.
 * <p>
 * Created on 24/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.27
 */
public class TopiaEntitySqlModel {

    /**
     * All schema names used.
     */
    private final List<String> schemaNames;
    /**
     * Replication order for all entities not managed by any entry point.
     */
    private final List<String> replicationOrderForStandalone;
    /**
     * Dictionary of sql descriptor indexed by their entity fully qualified name.
     */
    private final Map<String, TopiaEntitySqlDescriptor> descriptors;

    public TopiaEntitySqlModel(List<String> schemaNames, List<String> replicationOrderForStandalone, Map<String, TopiaEntitySqlDescriptor> descriptors) {
        this.schemaNames = Collections.unmodifiableList(Objects.requireNonNull(schemaNames));
        this.replicationOrderForStandalone = Objects.requireNonNull(replicationOrderForStandalone);
        this.descriptors = Objects.requireNonNull(descriptors);
    }

    public TopiaEntitySqlDescriptor getDescriptor(String type) {
        return descriptors.get(Objects.requireNonNull(type));
    }

    public TopiaEntitySqlDescriptors getReplicationOrderDescriptors(String entryPoint) {
        return getDescriptors(getReplicationOrderByType(Objects.requireNonNull(entryPoint)));
    }

    public TopiaEntitySqlDescriptors getReplicationOrderWithStandaloneDescriptors() {
        return getDescriptors(getReplicationOrderForStandalone());
    }

    public TopiaEntitySqlDescriptor getDescriptor(Class<? extends TopiaEntity> fqn) {
        return getDescriptor(Objects.requireNonNull(fqn).getName());
    }

    public List<String> getReplicationOrderByType(String entryPoint) {
        return getDescriptor(Objects.requireNonNull(entryPoint)).getReplicationOrder();
    }

    public List<String> getReplicationOrderForStandalone() {
        return replicationOrderForStandalone;
    }

    public List<String> getSchemaNames() {
        return schemaNames;
    }

    public Map<Class<? extends Entity>, TopiaEntitySqlDescriptor> getDescriptorsByType() {
        return descriptors.values().stream().collect(Collectors.toMap(TopiaEntitySqlDescriptor::getEntityType, Function.identity()));
    }

    protected Map<String, TopiaEntitySqlDescriptor> getDescriptors() {
        return descriptors;
    }

    protected TopiaEntitySqlDescriptors getDescriptors(Collection<String> fqnCollection) {
        List<TopiaEntitySqlDescriptor> builder = new LinkedList<>();
        for (String fqn : Objects.requireNonNull(fqnCollection)) {
            builder.add(getDescriptor(fqn));
        }
        return new TopiaEntitySqlDescriptors(Collections.unmodifiableList(builder));
    }
}
