package org.nuiton.topia.service.migration;

/*
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaService;

/**
 * The migration service must be invoke by ourself, nothing is done in ToPIA.
 * <p>
 * I don't want Hibernate to manage anything for use, we always use the generated schema script in class-path, this makes
 * sense and make us safe (compile time over runtime)
 *
 * @since 3.0
 */
public interface TopiaMigrationService extends TopiaService {

    /**
     * The schema already exists in an unknown version, service should update the schema if necessary.
     */
    void runSchemaMigration() throws TopiaMigrationServiceException;

}
