package org.nuiton.topia.service.sql.internal.consumer;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;
import org.nuiton.topia.service.sql.internal.SqlRequestConsumer;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.internal.request.AddVersionTableRequest;

import java.util.Date;

/**
 * Created on 21/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.43
 */
public class AddVersionTableConsumer implements SqlRequestConsumer<AddVersionTableRequest> {

    @Override
    public void consume(AddVersionTableRequest request, SqlRequestSetConsumerContext context) {
        try {
            SqlScriptWriter writer = context.getWriter();
            writer.writeSql(MigrationServiceSqlHelper.CURRENT.createSchemaSql());
            writer.writeSql(MigrationServiceSqlHelper.CURRENT.createTableSql());
            writer.writeSql(MigrationServiceSqlHelper.CURRENT.saveTableSql(request.getDbVersion().getVersion(), new Date()));
        } catch (Exception e) {
            throw new TopiaException(String.format("Could not add version table for reason: %s", e.getMessage()), e);
        }
    }
}
