package org.nuiton.topia.service.sql;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.SqlServiceRequest;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.TopiaService;
import org.nuiton.topia.service.sql.internal.SqlRequestSet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Service to produce high level sql scripts from sql model.
 * <p>
 * Created on 24/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public interface TopiaSqlService extends TopiaService, TopiaEntitySqlModelResource {

    static SqlScript generateH2Schema(Path targetPath, TopiaSqlService service) throws IOException {
        if (Files.notExists(targetPath.getParent())) {
            Files.createDirectories(targetPath.getParent());
        }
        SqlRequestSet request = SqlRequestSet.builder(targetPath).forH2().addCreateSchemaRequest().build();
        return service.consume(request);
    }

    static SqlScript generatePgSchema(Path targetPath, TopiaSqlService service) throws IOException {
        if (Files.notExists(targetPath.getParent())) {
            Files.createDirectories(targetPath.getParent());
        }
        SqlRequestSet request = SqlRequestSet.builder(targetPath).forPostgresql().addCreateSchemaRequest().build();
        return service.consume(request);
    }

    SqlScript consume(SqlRequestSet request) throws TopiaSqlServiceException;

    SqlScript consume(SqlServiceRequest request) throws TopiaSqlServiceException;

    @Override
    void close();
}
