package org.nuiton.topia.service.sql.script;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;

/**
 * Created on 06/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public class TopiaEntitySqlScript {

    /**
     * To update lastUpdateDate field (of all data in root path of this one).
     */
    private final UpdateLastUpdateDateFieldScript lastUpdateDateFieldScript;
    /**
     * To update lastUpdateDate table (for all data in root path of this one).
     */
    private final UpdateLastUpdateDateTableScript lastUpdateDateTableScript;
    /**
     * Optional script to replace a referential the id by another one in data only.
     */
    private final ReplaceReferentialInDataScript replaceReferentialInDataScript;
    /**
     * Optional script to replace a referential the id by another one in referential only.
     */
    private final ReplaceReferentialInReferentialScript replaceReferentialInReferentialScript;
    /**
     * Optional script to delete referential.
     */
    private final DeleteReferentialScript deleteReferentialScript;
    /**
     * Optional script to disable a referential.
     */

    private final DisableReferentialScript disableReferentialScript;


    public TopiaEntitySqlScript(UpdateLastUpdateDateFieldScript lastUpdateDateFieldScript,
                                UpdateLastUpdateDateTableScript lastUpdateDateTableScript,
                                ReplaceReferentialInDataScript replaceReferentialInDataScript,
                                ReplaceReferentialInReferentialScript replaceReferentialInReferentialScript,
                                DeleteReferentialScript deleteReferentialScript,
                                DisableReferentialScript disableReferentialScript) {
        this.lastUpdateDateFieldScript = Objects.requireNonNull(lastUpdateDateFieldScript);
        this.lastUpdateDateTableScript = Objects.requireNonNull(lastUpdateDateTableScript);
        this.replaceReferentialInDataScript = replaceReferentialInDataScript;
        this.replaceReferentialInReferentialScript = replaceReferentialInReferentialScript;
        this.deleteReferentialScript = deleteReferentialScript;
        this.disableReferentialScript = disableReferentialScript;
    }

    public UpdateLastUpdateDateFieldScript getLastUpdateDateFieldScript() {
        return lastUpdateDateFieldScript;
    }

    public UpdateLastUpdateDateTableScript getLastUpdateDateTableScript() {
        return lastUpdateDateTableScript;
    }

    public Optional<ReplaceReferentialInDataScript> getReplaceReferentialInDataScript() {
        return Optional.ofNullable(replaceReferentialInDataScript);
    }

    public Optional<ReplaceReferentialInReferentialScript> getReplaceReferentialInReferentialScript() {
        return Optional.ofNullable(replaceReferentialInReferentialScript);
    }

    public Optional<DeleteReferentialScript> getDeleteReferentialScript() {
        return Optional.ofNullable(deleteReferentialScript);
    }

    public Optional<DisableReferentialScript> getDisableReferentialScript() {
        return Optional.ofNullable(disableReferentialScript);
    }
}
