package fr.ird.observe.entities;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import org.nuiton.topia.persistence.TopiaIdFactory;

/**
 * Created on 19/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public interface ObserveIdFactoryContract extends TopiaIdFactory {

    String PACKAGE_PREFIX = "fr.ird.observe.entities.";
    String ID_PREFIX = "fr.ird.";

    @Override
    default String getIdClassNamePartFromClassName(String entityClassName) {
        return ID_PREFIX + Strings.removeStart(entityClassName, PACKAGE_PREFIX);
    }

    @Override
    default String getClassNameFromIdClassNamePart(String classNamePart) {
        return PACKAGE_PREFIX + Strings.removeStart(classNamePart, ID_PREFIX);
    }
}
