package fr.ird.observe.entities;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.internal.AbstractTopiaApplicationContext;
import org.nuiton.topia.service.migration.TopiaMigrationServiceImpl;
import org.nuiton.topia.service.sql.TopiaSqlServiceImpl;

import java.nio.file.Path;
import java.util.Map;

/**
 * Created on 23/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveTopiaConfigurationFactory {

    private static final JdbcConfigurationBuilder JDBC_CONFIGURATION_BUILDER = new JdbcConfigurationBuilder();
    private static final Logger log = LogManager.getLogger(ObserveTopiaConfigurationFactory.class);

    public static TopiaConfiguration create(ObserveDataSourceConfiguration configuration) {
        TopiaConfiguration topiaConfiguration;
        if (configuration.isH2Configuration()) {
            topiaConfiguration = forH2Database(configuration.getUrl(),
                                               configuration.getLogin(),
                                               String.valueOf(configuration.getPassword()),
                                               configuration.getTemporaryDirectory(),
                                               configuration.isTraceSql());
        } else if (configuration.isPGConfiguration()) {
            topiaConfiguration = forPostgresqlDatabase(configuration.getUrl(),
                                                       configuration.getLogin(),
                                                       String.valueOf(configuration.getPassword()),
                                                       configuration.getTemporaryDirectory(),
                                                       configuration.isTraceSql());
        } else {
            throw new IllegalArgumentException(String.format("Can't create topia configuration from url: %s", configuration.getUrl()));
        }

        log.debug(String.format("Create topia application configuration: %s", topiaConfiguration));
        return topiaConfiguration;
    }

    private static TopiaConfiguration forPostgresqlDatabase(String jdbcUrl,
                                                            String username,
                                                            String password,
                                                            Path temporaryDirectory,
                                                            boolean traceSql) {
        JdbcConfiguration jdbcConfiguration = JDBC_CONFIGURATION_BUILDER.forPostgresqlDatabase(jdbcUrl, username, password);
        return createTopiaConfiguration(jdbcConfiguration, temporaryDirectory, traceSql);
    }

    private static TopiaConfiguration forH2Database(String jdbcUrl,
                                                    String username,
                                                    String password,
                                                    Path temporaryDirectory,
                                                    boolean traceSql) {
        JdbcConfiguration jdbcConfiguration = JDBC_CONFIGURATION_BUILDER.forH2Database(jdbcUrl, username, password);
        return createTopiaConfiguration(jdbcConfiguration, temporaryDirectory, traceSql);
    }

    private static TopiaConfiguration createTopiaConfiguration(JdbcConfiguration jdbcConfiguration,
                                                               Path temporaryDirectory,
                                                               boolean traceSql) {
        BeanTopiaConfiguration topiaConfiguration = new BeanTopiaConfiguration(jdbcConfiguration, temporaryDirectory, traceSql);
        topiaConfiguration.setTopiaIdFactory(new ObserveIdFactory());
        topiaConfiguration.addDeclaredService(AbstractTopiaApplicationContext.MIGRATION_SERVICE_NAME, TopiaMigrationServiceImpl.class, Map.of());
        topiaConfiguration.addDeclaredService(AbstractTopiaApplicationContext.SQL_SERVICE_NAME, TopiaSqlServiceImpl.class, Map.of());
        return topiaConfiguration;
    }

}
