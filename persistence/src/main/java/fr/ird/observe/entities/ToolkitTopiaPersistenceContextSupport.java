package fr.ird.observe.entities;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.CopyRequest;
import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.security.DataFileNotFoundException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.DataNotFoundException;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdMap;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.data.DataFileDto;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.dto.reference.ReferenceSetsRequest;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.DataFileAware;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.services.service.data.DeleteDataRequest;
import fr.ird.observe.services.service.data.DeleteDataResult;
import fr.ird.observe.services.service.data.ExportDataRequest;
import fr.ird.observe.services.service.data.ExportDataResult;
import fr.ird.observe.services.service.data.ImportDataRequest;
import fr.ird.observe.services.service.data.ImportDataResult;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.parent.ToParentIdProvider;
import fr.ird.observe.spi.relation.DtoEntityRelation;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.matrix.DataMatrix;
import io.ultreia.java4all.util.sql.BlobsContainer;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContext;
import org.nuiton.topia.persistence.internal.AbstractTopiaPersistenceContextConstructorParameter;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * Topia persistence context support with nice methods (should be in ToPIA).
 * <p>
 * Created by tchemit on 2020/08/12.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ToolkitTopiaPersistenceContextSupport extends AbstractTopiaPersistenceContext {

    private static final Logger log = LogManager.getLogger(ToolkitTopiaPersistenceContextSupport.class);
    /**
     * Get get parent id for any type.
     */
    private transient ToParentIdProvider toParentIdProvider;

    public ToolkitTopiaPersistenceContextSupport(AbstractTopiaPersistenceContextConstructorParameter parameter) {
        super(parameter);
    }

    public ToParentIdProvider getToParentIdProvider() {
        if (toParentIdProvider == null) {
            toParentIdProvider = newToParentIdProvider();
        }
        return toParentIdProvider;
    }

    protected abstract ToParentIdProvider newToParentIdProvider();

    public abstract Date getLastUpdateDate(String type);

    public abstract void setLastUpdateDate(String type, Date date);

    public abstract ReportRequestExecutor newReportRequestExecutor(ReportDefinition reportDefinition, ReferentialLocale referentialLocale);

    public <E extends Entity> Date getLastUpdateDate(Class<E> entityType) {
        return getLastUpdateDate(entityType.getName());
    }

    public Stream<ToolkitId> executeQueryIdAll(Class<? extends Entity> entityType) {
        return getToolkitIdQuery(entityType.getName() + "::id::all").stream();
    }

    public Stream<ToolkitId> executeQueryIdIn(Class<? extends Entity> entityType, Collection<String> ids) {
        return getToolkitIdQuery(entityType.getName() + "::id::in").setParameterList(1, ids).list().stream();
    }

    public ToolkitId executeQueryIdEquals(Class<? extends Entity> entityType, String id) {
        return getToolkitIdQuery(entityType.getName() + "::id::equals").setParameter(1, id).uniqueResult();
    }

    public Stream<ToolkitId> executeQueryIdBefore(Class<? extends Entity> entityType, Date date) {
        return getToolkitIdQuery(entityType.getName() + "::id::before").setParameter(1, date).stream();
    }

    public Stream<ToolkitId> executeQueryIdAfter(Class<? extends Entity> entityType, Date date) {
        return getToolkitIdQuery(entityType.getName() + "::id::after").setParameter(1, date).stream();
    }

    public ToolkitIdMap executeQueryMapOne(Class<? extends Entity> entityType, String id) {
        return new ToolkitIdMap(getMapQuery(entityType.getName() + "::map::one").setParameter(1, id).uniqueResult());
    }

    public Stream<ToolkitIdMap> executeQueryMapAll(Class<? extends Entity> entityType) {
        return getMapQuery(entityType.getName() + "::map::all").stream().map(ToolkitIdMap::new);
    }

    public Stream<ToolkitIdMap> executeQueryMapSome(Class<? extends Entity> entityType, Collection<String> ids) {
        return getMapQuery(entityType.getName() + "::map::some").setParameterList(1, ids).list().stream().map(ToolkitIdMap::new);
    }

    public ToolkitParentIdDtoBean executeQueryGetParentId(DtoEntityRelation<?, ?, ?, ?> relation, String id) {
        Object[] getParentId = this.<Object[]>getSqlQuery(relation.requestName("getParentId")).setParameter(1, id).uniqueResult();
        if (getParentId == null) {
            return null;
        }
        return new ToolkitParentIdDtoBean(relation.parentDtoType(), (String) getParentId[0], (Date) getParentId[1], relation.relationName());
    }

    public Stream<String> executeQueryByParentId(DtoEntityRelation<?, ?, ?, ?> relation, Collection<String> id) {
        return this.<String>getSqlQuery(relation.requestName("byParentId")).setParameterList(1, id).getResultStream();
    }

    protected Query<ToolkitId> getToolkitIdQuery(String queryName) {
        return getQuery(queryName);
    }

    public DataMatrix select(String select) {
        List<Object[]> result = sqlSupport.findMultipleResult(select, r -> {
            int columnCount = r.getMetaData().getColumnCount();
            Object[] row = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {
                row[i] = r.getObject(i + 1);
            }
            return row;
        });
        DataMatrix dataMatrix = new DataMatrix();
        dataMatrix.setData(result.toArray(new Object[0][0]));
        return dataMatrix;
    }

    public ReferentialIds getReferentialIds(BusinessProject businessProject) {
        Map<Class<? extends ReferentialDto>, Set<String>> result = new LinkedHashMap<>();
        for (Class<? extends ReferentialDto> dtoType : businessProject.getReferentialTypes()) {
            ReferentialDtoEntityContext<?, ?, ?, ?> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
            List<String> allIds = spi.getDao(this).findAllIds();
            result.computeIfAbsent(dtoType, k -> new LinkedHashSet<>()).addAll(Set.copyOf(allIds));
        }
        return ReferentialIds.of(result);
    }

    public <D extends BusinessDto> Set<ReferentialDtoReferenceSet<?>> getReferentialReferenceSets(ServiceContext context, ReferenceSetsRequest<D> request, BusinessProject businessProject) {
        Class<D> dtoType = request.getDtoType();
        Set<ReferentialDtoReferenceSet<?>> resultBuilder = new LinkedHashSet<>();
        Optional<FormDefinition<D>> optionalRequestDefinition = businessProject.getOptionalFormDefinition(dtoType);
        if (optionalRequestDefinition.isPresent()) {
            FormDefinition<D> requestDefinition = optionalRequestDefinition.get();
            Map<Class<? extends ReferentialDto>, Date> lastUpdateDates = request.getLastUpdateDates();
            Objects.requireNonNull(lastUpdateDates);
            for (Class<? extends ReferentialDto> propertyType : requestDefinition.getPropertiesTypes()) {
                Date lastUpdateDate = lastUpdateDates.get(propertyType);
                ReferentialDtoEntityContext<?, ?, ?, ?> spi = PersistenceBusinessProject.fromReferentialDto(propertyType);
                ReferentialDtoReferenceSet<?> referenceSet = spi.getReferenceSet0(context, lastUpdateDate);
                if (referenceSet != null) {
                    resultBuilder.add(referenceSet);
                }
            }
        }
        return resultBuilder;
    }

    public final <E extends Entity> E loadEntity0(Locale applicationLocale, String id) {
        log.debug(String.format("Load entity with id: %s", id));
        try {
            return findByTopiaId(id);
        } catch (TopiaNoResultException e) {
            throw new DataNotFoundException(applicationLocale, id);
        }
    }

    public DataFileDto getDataFile(Locale applicationLocale, String id) throws DataFileNotFoundException {
        DataFileAware dataFile = loadEntity0(applicationLocale, id);
        if (dataFile.getData() == null) {
            throw new DataFileNotFoundException(applicationLocale, id);
        }
        return newDataFileDto(dataFile.getData(), dataFile.getDataFilename());
    }

    protected DataFileDto newDataFileDto(Blob data, String dataFilename) {
        DataFileDto dto = new DataFileDto();
        try {
            // copy blob
            int length = (int) data.length();
            byte[] bytes = data.getBytes(1, length);
            dto.setContent(bytes);
        } catch (SQLException e) {
            log.error(String.format("unable to read blob from %s", dataFilename), e);
        }
        dto.setName(dataFilename);
        return dto;
    }

    public DeleteDataResult deleteData(ServiceContext context, DeleteDataRequest request) {
        long t0 = System.nanoTime();
        String dataId = request.getDataId();
        SqlScript deleteScript = produceDeleteScript(context, dataId);
        if (deleteScript != null) {
            executeSqlScript(deleteScript);
            flush();
            long time = System.nanoTime() - t0;
            log.info(String.format("Delete of trip: %s done in %s", dataId, Strings.convertTime(time)));
            return new DeleteDataResult(request, time);
        }
        return null;
    }

    public ExportDataResult exportData(ServiceContext context, ExportDataRequest request) {
        long t0 = System.nanoTime();
        String dataId = request.getDataId();
        log.info(String.format("Start export of trip: %s", dataId));
        Class<TopiaEntity> entityClass = getTopiaIdFactory().getClassName(dataId);
        CopyRequest sqlRequest = new CopyRequest(request.isForPG(), entityClass.getName(), dataId);
        SqlScript tripContent = context.getTopiaApplicationContext().getSqlService().consume(sqlRequest);
        return new ExportDataResult(request, tripContent, System.nanoTime() - t0);

    }

    public ImportDataResult importData(ServiceContext context, ImportDataRequest request) {
        long t1 = TimeLog.getTime();
        String dataId = request.getDataId();
        SqlScript sqlContent = request.getSqlContent();

        SqlScript deleteScript = produceDeleteScript(context, dataId);
        boolean deleted = deleteScript != null;
        long deleteTime = 0;
        if (deleted) {
            executeSqlScript(deleteScript);
            flush();
            deleteTime = System.nanoTime() - t1;
            log.info(String.format("Delete of trip: %s done in %s", dataId, Strings.convertTime(deleteTime)));
        }
        t1 = System.nanoTime();
        log.info(String.format("Start import of trip: %s", request.getDataId()));
        Set<BlobsContainer> blobsContainers = sqlContent.getBlobsContainers();
        SqlScriptConsumer.Builder consumerBuilder = SqlScriptConsumer.builder(sqlContent.getLocation()).batchSize(50);
        if (!blobsContainers.isEmpty()) {
            consumerBuilder.blobs(blobsContainers);
        }
        try {
            executeSqlScript(consumerBuilder.build());
            flush();
        } catch (Exception e) {
            throw onImportError(e, dataId, sqlContent);
        }
        long t2 = System.nanoTime();
        log.info(String.format("Import of trip: %s done in %s", dataId, Strings.convertTime(t1, t2)));
        return new ImportDataResult(request, true, t2 - t1, deleted, deleteTime);
    }

    private TopiaException onImportError(Exception e, String dataId, SqlScript sqlContent) {
        // detect ids in import script
        Set<String> ids = new TreeSet<>();
        //FIXME Very weak
        for (String sqlStatement : sqlContent.getLocation()) {
            if (sqlStatement.toLowerCase().trim().startsWith("insert into")) {
                String id = sqlStatement.substring(sqlStatement.indexOf("VALUES") + 6);
                id = id.substring(id.indexOf("'") + 1);
                id = id.substring(0, id.indexOf("'"));
                ids.add(id);
            }
        }
        Set<String> existingIds = new TreeSet<>();
        if (!ids.isEmpty()) {
            for (String id : ids) {
                if (exists(id)) {
                    log.error(String.format("Can't import trip %s, - found existing id in target database (%s)", dataId, id));
                    existingIds.add(id);
                }
            }
        }
        if (!existingIds.isEmpty()) {
            return new TopiaException(String.format("Next %d id(s) are blocking the import of trip %s:\n%s", existingIds.size(), dataId, String.join("\n", existingIds)), e);
        }
        return new TopiaException(e);
    }

    private SqlScript produceDeleteScript(ServiceContext context, String dataId) {
        boolean exists = exists(dataId);
        if (exists) {
            Class<TopiaEntity> entityClass = getTopiaIdFactory().getClassName(dataId);
            log.info(String.format("Start delete of trip: %s", dataId));
            DeleteRequest sqlRequest = new DeleteRequest(context.isPostgresDatabase(), entityClass.getName(), null, dataId);
            return context.getTopiaApplicationContext().getSqlService().consume(sqlRequest);
        }
        return null;
    }
}
