package fr.ird.observe.entities.data;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByTemporalOption;
import fr.ird.observe.dto.data.DataGroupByType;
import fr.ird.observe.dto.data.DataGroupByValue;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Defines a data group by.
 * <p>
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DataGroupByEntityDefinition<
        E extends RootOpenableEntity,
        F extends DataGroupByEntity<E>,
        D extends RootOpenableDto,
        FF extends DataGroupByDto<D>> extends DataGroupByDefinition<E, F> {

    private final String groupBySecondTable;

    protected DataGroupByEntityDefinition(String filterName,
                                          DataGroupByType type,
                                          String propertyName,
                                          boolean propertyMandatory,
                                          Class<?> propertyType,
                                          Class<E> childrenType,
                                          Class<F> formType,
                                          Supplier<F> formSupplier,
                                          Supplier<BusinessModule> businessModuleSupplier,
                                          String groupBySecondTable) {
        super(filterName,
              type,
              propertyName,
              propertyMandatory,
              propertyType,
              childrenType,
              formType,
              formSupplier,
              businessModuleSupplier);
        this.groupBySecondTable = groupBySecondTable;
    }

    public abstract DataGroupByDtoDefinition<D, FF> dtoDefinition();

    @Override
    public final F newForm(DataGroupByValue value) {
        F dto = super.newForm();
        value.copyTo(dto);
        return dto;
    }

    public final String getGroupBySecondTable() {
        return groupBySecondTable;
    }

    public Object getGroupByEntityValue(ServiceContext context, String groupByFlavor, String groupByValue) {
        if (groupByValue == null) {
            return null;
        }
        if (isQualitative()) {
            @SuppressWarnings("unchecked") Class<? extends ReferentialEntity> propertyType = (Class<? extends ReferentialEntity>) getPropertyType();
            return context.fromReferentialEntity(propertyType).loadEntity(context, groupByValue);
        }
        Optional<DataGroupByTemporalOption> optional = DataGroupByTemporalOption.optionalValueOf(groupByFlavor);
        if (optional.isEmpty()) {
            // full date
            return DataGroupByTemporalOption.newFullDate(groupByValue);
        }
        return optional.get().newDate(groupByValue);
    }
}
