package fr.ird.observe.entities;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToToolkitIdLabel;
import fr.ird.observe.dto.ToToolkitIdTechnicalLabel;
import fr.ird.observe.dto.ToolkitIdImpl;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.dto.reference.DtoReferenceAware;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorated;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Date;

public interface Entity extends TopiaEntity, ToolkitIdImpl, DtoReferenceAware, JavaBean, Decorated, ToToolkitIdLabel, ToToolkitIdTechnicalLabel {

    String PROPERTY_LAST_UPDATE_DATE = "lastUpdateDate";

    String PROPERTY_HOME_ID = "homeId";

    void setLastUpdateDate(Date lastUpdateDate);

    String getHomeId();

    void setHomeId(String homeId);

    @Override
    default void setId(String id) {
        setTopiaId(id);
    }

    @Override
    default ToolkitIdLabel toLabel() {
        throw new IllegalStateException("Method toLabel not implemented for class: " + getClass());
    }

    @Override
    default ToolkitIdTechnicalLabel toTechnicalLabel() {
        throw new IllegalStateException("Method toTechnicalLabel not implemented for class: " + getClass());
    }
}
