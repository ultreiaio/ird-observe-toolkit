package fr.ird.observe.entities.data;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.AbstractTopiaEntity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DataGroupByEntityAbstract<E extends RootOpenableEntity> extends AbstractTopiaEntity implements DataGroupByEntity<E> {

    private Date lastUpdateDate;
    private List<E> children;
    private String filterValue;
    private String filterText;
    private long count;
    private boolean enabled = true;

    private transient JavaBeanDefinition JAVA_BEAN_DEFINITION;
    private transient Decorator decorator;

    @Override
    public final void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    @Override
    public final long getCount() {
        return children == null ? count : children.size();
    }

    @Override
    public final void setCount(long count) {
        this.count = count;
    }

    @Override
    public final List<E> getChildren() {
        if (children == null) {
            children = new LinkedList<>();
        }
        return children;
    }

    @Override
    public final void setChildren(List<E> children) {
        this.children = children;
        setCount(children == null ? 0 : children.size());
    }

    @Override
    public String getTopiaId() {
        return getFilterValue();
    }

    @Override
    public final boolean isPersisted() {
        return getTopiaId() != null;
    }

    @Override
    public final String getFilterValue() {
        return filterValue;
    }

    @Override
    public final String getFilterText() {
        return filterText;
    }

    @Override
    public final void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    @Override
    public final boolean isEnabled() {
        return enabled;
    }

    @Override
    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public final Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    @Override
    public final void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public final String getHomeId() {
        return null;
    }

    @Override
    public final void setHomeId(String homeId) {
    }

    @Override
    public final JavaBeanDefinition javaBeanDefinition() {
        return JAVA_BEAN_DEFINITION == null ? JAVA_BEAN_DEFINITION = JavaBeanDefinitionStore.getDefinition(getClass()).orElseThrow(IllegalStateException::new) : JAVA_BEAN_DEFINITION;
    }

    @Override
    public final void accept(TopiaEntityVisitor visitor) throws TopiaException {
        visitor.start(this);
        accept0(visitor);
        visitor.end(this);
    }

    protected void accept0(TopiaEntityVisitor visitor) throws TopiaException {
        visitor.visit(this, PROPERTY_LAST_UPDATE_DATE, Date.class, lastUpdateDate);
        visitor.visit(this, PROPERTY_FILTER_TEXT, String.class, getFilterText());
        visitor.visit(this, PROPERTY_FILTER_VALUE, String.class, getFilterValue());
        visitor.visit(this, PROPERTY_ENABLED, boolean.class, isEnabled());
        visitor.visit(this, PROPERTY_COUNT, int.class, getCount());
        visitor.visit(this, PROPERTY_CHILDREN, List.class, definition().getDataType(), children);
    }

    @Override
    public String getId() {
        return topiaId;
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e -> super.toString());
    }

    @Override
    public Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        this.decorator = Objects.requireNonNull(decorator);
    }
}
