package fr.ird.observe.entities.referential;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReferenceAware;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.entities.Entity;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ReferentialEntity extends Entity, ReferentialDtoReferenceAware {

    String PROPERTY_CODE = "code";

    String PROPERTY_URI = "uri";

    String PROPERTY_NEED_COMMENT = "needComment";

    String PROPERTY_STATUS = "status";

    static <E extends ReferentialEntity> List<E> reorder(List<E> incoming, String recursivePropertyName) {
        Set<E> roots = new LinkedHashSet<>();
        Map<String, List<E>> map = new LinkedHashMap<>();
        for (E entity : incoming) {
            E o = entity.get(recursivePropertyName);
            if (o == null) {
                roots.add(entity);
            } else {
                map.computeIfAbsent(o.getTopiaId(), k -> new LinkedList<>()).add(entity);
            }
        }
        List<E> result = new LinkedList<>();
        for (E root : roots) {
            fill(root, result, map);
        }
        for (E e : incoming) {
            if (!result.contains(e)) {
                result.add(e);
            }
        }
        return result;
    }

    private static <E extends ReferentialEntity> void fill(E root, List<E> result, Map<String, List<E>> map) {
        result.add(root);
        Collection<E> entitiesForRoot = map.get(root.getId());
        if (entitiesForRoot != null) {
            for (E entity : entitiesForRoot) {
                fill(entity, result, map);
            }
        }
    }

    String getCode();

    void setCode(String code);

    String getUri();

    void setUri(String uri);

    boolean isNeedComment();

    void setNeedComment(boolean needComment);

    ReferenceStatus getStatus();

    void setStatus(ReferenceStatus status);

    default boolean isEnabled() {
        return ReferenceStatus.enabled == getStatus();
    }

    default boolean isDisabled() {
        return ReferenceStatus.disabled == getStatus();
    }

}
