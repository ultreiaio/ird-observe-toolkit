package fr.ird.observe.entities;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;
import org.nuiton.topia.persistence.TopiaException;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

public abstract class EntityImpl implements Entity {

    private static final long serialVersionUID = 1L;

    protected String topiaId;

    protected long topiaVersion;

    protected Date topiaCreateDate = new Date();

    protected Date lastUpdateDate;

    protected transient boolean deleted;
    private transient Decorator decorator;
    /**
     * Lazy helper class that manages all the property change notification machinery.
     * PropertyChangeSupport cannot be extended directly because it requires
     * a bean in the constructor, and the "this" argument is not valid until
     * after super construction. Hence, delegation instead of extension.
     *
     * @see #pcs()
     */
    private transient PropertyChangeSupport pcs;

    @Override
    public String getTopiaId() {
        return topiaId;
    }

    @Override
    public void setTopiaId(String v) {
        topiaId = v;
    }

    @Override
    public long getTopiaVersion() {
        return topiaVersion;
    }

    @Override
    public void setTopiaVersion(long v) {
        topiaVersion = v;
    }

    @Override
    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    @Override
    public void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }

    @Override
    public boolean isPersisted() {
        // Is or was the entity persisted ?
        boolean result = topiaId != null;
        // Is the entity deleted ?
        result &= !deleted;
        return result;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void notifyDeleted() {
        deleted = true;
    }

    @Override
    public int hashCode() {
        Date date = getTopiaCreateDate();
        return date == null ? 0 : date.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TopiaEntity)) {
            return false;
        }
        TopiaEntity other = (TopiaEntity) obj;
        if (getTopiaId() == null || other.getTopiaId() == null) {
            return false;
        }
        return getTopiaId().equals(other.getTopiaId());
    }

    @Override
    public void accept(TopiaEntityVisitor visitor) throws TopiaException {
        visitor.start(this);
        accept0(visitor);
        visitor.end(this);
    }

    protected void accept0(TopiaEntityVisitor visitor) throws TopiaException {
        visitor.visit(this, PROPERTY_LAST_UPDATE_DATE, Date.class, lastUpdateDate);
    }

    @Override
    public void setLastUpdateDate(Date lastUpdateDate) {
        Date oldValue = this.lastUpdateDate;
        this.lastUpdateDate = lastUpdateDate;
        firePropertyChange(PROPERTY_LAST_UPDATE_DATE, oldValue, lastUpdateDate);
    }

    @Override
    public Date getLastUpdateDate() {
        return this.lastUpdateDate;
    }

    @Override
    public String getId() {
        return topiaId;
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e -> super.toString());
    }

    @Override
    public Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        this.decorator = Objects.requireNonNull(decorator);
    }

    @Override
    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(listener);
    }

    @Override
    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(listener);
    }

    @Override
    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Report a bound property update to any registered listeners.
     * No event is fired if old and new are equal and non-null.
     * <p>
     * This is merely a convenience wrapper around the more general
     * firePropertyChange method that takes {@code
     * PropertyChangeEvent} value.
     *
     * @param propertyName The programmatic name of the property
     *                     that was changed.
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     */
    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs().firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void fireOnPostWrite(String propertyName, Object oldValue, Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(String propertyName, Object newValue) {
        pcs().firePropertyChange(propertyName, null, newValue);
    }

    protected final void firePropertyChange(PropertyChangeEvent evt) {
        pcs().firePropertyChange(evt);
    }

    protected final PropertyChangeSupport pcs() {
        return pcs == null ? (pcs = new PropertyChangeSupport(this)) : pcs;
    }

}
