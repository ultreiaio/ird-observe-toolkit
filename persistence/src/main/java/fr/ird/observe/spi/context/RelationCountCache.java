package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 13/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class RelationCountCache<E extends Entity> {

    private final Map<String, Integer> count;
    private final TopiaEntitySqlTable mainTable;
    private final String property;

    public RelationCountCache(TopiaEntitySqlTable mainTable, DtoEntityContext<?, ?, E, ?> spi) {
        this.mainTable = mainTable;
        this.property = spi.getEntitySqlDescriptor().getTable().getTableName();
        this.count = new TreeMap<>();
    }

    protected abstract String generateSql(String property, String extraWhereClause);

    public final void reset(ToolkitTopiaPersistenceContextSupport persistenceContext, String extraSqlCode) {
        //FIXME as property we also use the main table main, but it could not work...

        count.clear();
        String sql = generateSql(property, extraSqlCode);
        SqlQuery<Void> query = SqlQuery.wrap(sql, resultSet -> {
            int rowCount = resultSet.getInt(2);
            if (rowCount > 0) {
                String id = resultSet.getString(1);
                count.put(id, rowCount);
            }
            return null;
        });
        persistenceContext.doMapSqlWork(query);
    }

    public final int count(String id) {
        return count.getOrDefault(id, 0);
    }

    public final TopiaEntitySqlTable mainTable() {
        return mainTable;
    }

}
