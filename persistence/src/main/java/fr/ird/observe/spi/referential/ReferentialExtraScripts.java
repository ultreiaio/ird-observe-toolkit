package fr.ird.observe.spi.referential;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * Created on 17/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public abstract class ReferentialExtraScripts<E extends ReferentialEntity> {
    /**
     * To insert a entity.
     */
    private static final String INSERT_STATEMENT = "INSERT INTO %s.%s(%s) VALUES (%s);";
    private final Set<String> simplePropertyNames;
    private final Set<String> compositionPropertyNames;
    private final Set<String> associationPropertyNames;
    private final ReferentialDtoEntityContext<?, ?, E, ?> spi;

    public ReferentialExtraScripts(ReferentialDtoEntityContext<?, ?, E, ?> spi, Set<String> simplePropertyNames, Set<String> compositionPropertyNames, Set<String> associationPropertyNames) {
        this.simplePropertyNames = simplePropertyNames;
        this.compositionPropertyNames = compositionPropertyNames;
        this.associationPropertyNames = associationPropertyNames;
        this.spi = spi;
    }

    protected abstract void computeUpdateParameters(E entity, Set<String> propertiesToProcess, StringBuilder parameters, Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell);

    public List<String> generateUpdateScript(TopiaPersistenceContext service, E source, Set<String> properties, Date lastUpdateDate, Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {

        StringBuilder parameters = new StringBuilder();

        addUpdatePrimitiveLongParameter(null, ReferentialEntity.PROPERTY_TOPIA_VERSION, ReferentialEntity.PROPERTY_TOPIA_VERSION, source.getTopiaVersion(), parameters);
        if (lastUpdateDate != null) {
            addUpdateTimestampParameter(null, ReferentialEntity.PROPERTY_LAST_UPDATE_DATE, ReferentialEntity.PROPERTY_LAST_UPDATE_DATE, lastUpdateDate, parameters);
        }

        Set<String> propertiesToProcess = computePropertiesToProcess(properties, lastUpdateDate);

        Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell = HashMultimap.create();
        computeUpdateParameters(source, propertiesToProcess, parameters, referentialShell);

        String updateSql = generateUpdateStatement(source.getId(), parameters.substring(2));

        List<String> associationsResult = new LinkedList<>();
        computeUpdateAssociations(source, propertiesToProcess, referentialShell, associationsResult);

        List<String> result = computeMissingReferential(idsOnlyExistingOnThisSide, service, referentialShell);
        result.add(updateSql);
        result.addAll(associationsResult);
        return result;
    }

    public Optional<String> getOptionalRecursiveProperty() {
        return Optional.empty();
    }

    public List<String> generateCopyScript(TopiaPersistenceContext service, E entity, Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {
        List<String> parameters = new LinkedList<>();
        List<String> columnNames = new LinkedList<>();

        Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell = HashMultimap.create();

        computeInsertColumnAndParameters(entity, columnNames, parameters, referentialShell);

        String insertSql = generateInsertStatement(columnNames, parameters);
        List<String> associationsResult = new LinkedList<>();
        computeInsertAssociations(entity, referentialShell, associationsResult);

        List<String> result = computeMissingReferential(idsOnlyExistingOnThisSide, service, referentialShell);
        result.add(insertSql);
        result.addAll(associationsResult);
        return result;
    }

    protected void computeUpdateAssociations(E entity, Set<String> propertiesToProcess, Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell, List<String> result) {
    }

    protected abstract void computeInsertColumnAndParameters(E entity, List<String> columnNames, List<String> parameters, Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell);
//        addEnumParameter("status", entity.getStatus(), columnNames, parameters);
//    }

    protected void computeInsertAssociations(E entity, Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell, List<String> result) {
    }

    private boolean skipUseProperty(String propertyName, Set<String> properties) {
        return properties != null && !properties.contains(propertyName);
    }

    private Set<String> computePropertiesToProcess(Set<String> properties, Date lastUpdateDate) {
        Set<String> propertiesToProcess = new LinkedHashSet<>();
        if (properties == null) {
            // use all properties
            propertiesToProcess.addAll(simplePropertyNames);
            propertiesToProcess.addAll(compositionPropertyNames);
            propertiesToProcess.addAll(associationPropertyNames);
        } else {
            propertiesToProcess.addAll(properties);
            if (properties.contains("enabled")) {
                propertiesToProcess.remove("enabled");
                propertiesToProcess.add("status");
            }
        }
        propertiesToProcess.remove(TopiaEntity.PROPERTY_TOPIA_VERSION);
        propertiesToProcess.remove(TopiaEntity.PROPERTY_TOPIA_CREATE_DATE);
        if (lastUpdateDate != null) {
            propertiesToProcess.remove(Entity.PROPERTY_LAST_UPDATE_DATE);
        }
        return propertiesToProcess;
    }

    protected void addUpdateStringParameter(Set<String> propertiesToProcess, String propertyName, String columnName, String parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "'" + parameter.replaceAll("'", "''") + "'", parameters);
    }

    protected void addUpdateDateParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Date parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateDateParameter(propertiesToProcess, propertyName, columnName, new java.sql.Date(parameter.getTime()), parameters);
    }

    protected void addUpdateTimestampParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Date parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateTimestampParameter(propertiesToProcess, propertyName, columnName, new java.sql.Timestamp(parameter.getTime()), parameters);
    }

    protected void addUpdateTimeParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Date parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateTimeParameter(propertiesToProcess, propertyName, columnName, new java.sql.Time(parameter.getTime()), parameters);
    }

    protected void addUpdateDateParameter(Set<String> propertiesToProcess, String propertyName, String columnName, java.sql.Date parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "'" + parameter + "'", parameters);
    }

    protected void addUpdateTimestampParameter(Set<String> propertiesToProcess, String propertyName, String columnName, java.sql.Timestamp parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "'" + parameter + "'", parameters);
    }

    protected void addUpdateTimeParameter(Set<String> propertiesToProcess, String propertyName, String columnName, java.sql.Time parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "'" + parameter + "'", parameters);
    }

    protected void addUpdateEnumParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Enum parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter.ordinal(), parameters);
    }

    protected void addUpdateOtherTypeParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Object parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdatePrimitiveBooleanParameter(Set<String> propertiesToProcess, String propertyName, String columnName, boolean parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdatePrimitiveIntegerParameter(Set<String> propertiesToProcess, String propertyName, String columnName, int parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdatePrimitiveLongParameter(Set<String> propertiesToProcess, String propertyName, String columnName, long parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdatePrimitiveFloatParameter(Set<String> propertiesToProcess, String propertyName, String columnName, float parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }


    protected void addUpdateBooleanParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Boolean parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdateIntegerParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Integer parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdateLongParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Long parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected void addUpdateFloatParameter(Set<String> propertiesToProcess, String propertyName, String columnName, Float parameter, StringBuilder parameters) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        addUpdateParameter(columnName, "" + parameter, parameters);
    }

    protected <EE extends ReferentialEntity> void addUpdateReferentialComposition(Set<String> propertiesToProcess,
                                                                                  ReferentialDtoEntityContext<?, ?, EE, ?> compositionSpi,
                                                                                  String propertyName,
                                                                                  String columnName,
                                                                                  EE parameter,
                                                                                  StringBuilder parameters,
                                                                                  Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        if (addUpdateNullParameter(columnName, parameter, parameters)) {
            return;
        }
        String compositionId = parameter.getId();
        if (referentialShell != null) {
            referentialShell.put(compositionSpi, compositionId);
        }
        addUpdateStringParameter(propertiesToProcess, propertyName, columnName, compositionId, parameters);
    }

    protected <EE extends ReferentialEntity> void addUpdateReferentialAssociation(Set<String> propertiesToProcess,
                                                                                  ReferentialDtoEntityContext<?, ?, EE, ?> compositionSpi,
                                                                                  String propertyName,
                                                                                  String tableName,
                                                                                  String sourceName,
                                                                                  String targetName,
                                                                                  String id,
                                                                                  Collection<EE> parameter,
                                                                                  Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell,
                                                                                  List<String> result) {
        if (skipUseProperty(propertyName, propertiesToProcess)) {
            return;
        }
        // On commence toujours par supprimer toutes les anciennes associations, elles seront ré-ajoutées juste après
        String deleteSql = generateManyToManyAssociationDeleteStatement(tableName, sourceName, id);
        result.add(deleteSql);
//        result.add(SqlStatements.boxAssociationStatement(deleteSql));

        if (parameter == null || parameter.isEmpty()) {
            return;
        }
        for (EE association : parameter) {
            String compositionId = association.getId();
            if (referentialShell != null) {
                referentialShell.put(compositionSpi, compositionId);
            }
            String insertSql = generateAssociationInsertStatement(compositionSpi.getEntitySqlDescriptor().getTable().getSchemaName(),
                                                                  tableName,
                                                                  sourceName,
                                                                  targetName,
                                                                  id,
                                                                  compositionId);
            result.add(insertSql);
//            result.add(SqlStatements.boxAssociationStatement(insertSql));
        }
    }

    public static List<String> computeMissingReferential(Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide, TopiaPersistenceContext service, Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell) {
        List<String> result = new LinkedList<>();
        if (idsOnlyExistingOnThisSide == null || referentialShell.isEmpty()) {
            return result;
        }
        for (Map.Entry<ReferentialDtoEntityContext<?, ?, ?, ?>, Collection<String>> entry : referentialShell.asMap().entrySet()) {
            addMissingReferentialIfNecessary(service, entry.getKey(), entry.getValue(), idsOnlyExistingOnThisSide, result);
        }

        return result;
    }

    protected void addInsertStringParameter(String columnName, String parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("'" + parameter.replaceAll("'", "''") + "'");
    }

    protected void addInsertDateParameter(String columnName, java.sql.Date parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("'" + parameter + "'");
    }

    protected void addInsertDateParameter(String columnName, Date parameter, List<String> columnNames, List<String> parameters) {
        addInsertDateParameter(columnName, parameter == null ? null : new java.sql.Date(parameter.getTime()), columnNames, parameters);
    }

    protected void addInsertTimestampParameter(String columnName, java.sql.Timestamp parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("'" + parameter + "'");
    }

    protected void addInsertTimestampParameter(String columnName, Date parameter, List<String> columnNames, List<String> parameters) {
        addInsertTimestampParameter(columnName, parameter == null ? null : new java.sql.Timestamp(parameter.getTime()), columnNames, parameters);
    }

    protected void addInsertTimeParameter(String columnName, java.sql.Time parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("'" + parameter + "'");
    }

    protected void addInsertTimeParameter(String columnName, Date parameter, List<String> columnNames, List<String> parameters) {
        addInsertTimeParameter(columnName, parameter == null ? null : new java.sql.Time(parameter.getTime()), columnNames, parameters);
    }

    protected void addInsertPrimitiveBooleanParameter(String columnName, boolean parameter, List<String> columnNames, List<String> parameters) {
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }

    protected void addInsertBooleanParameter(String columnName, Boolean parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        addInsertPrimitiveBooleanParameter(columnName, parameter, columnNames, parameters);
    }

    protected void addInsertPrimitiveIntegerParameter(String columnName, int parameter, List<String> columnNames, List<String> parameters) {
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }

    protected void addInsertIntegerParameter(String columnName, Integer parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        addInsertPrimitiveIntegerParameter(columnName, parameter, columnNames, parameters);
    }

    protected void addInsertPrimitiveLongParameter(String columnName, long parameter, List<String> columnNames, List<String> parameters) {
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }

    protected void addInsertLongParameter(String columnName, Long parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        addInsertPrimitiveLongParameter(columnName, parameter, columnNames, parameters);
    }

    protected void addInsertPrimitiveFloatParameter(String columnName, float parameter, List<String> columnNames, List<String> parameters) {
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }


    protected void addInsertFloatParameter(String columnName, Float parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        addInsertPrimitiveFloatParameter(columnName, parameter, columnNames, parameters);
    }

    protected void addInsertPrimitiveDoubleParameter(String columnName, double parameter, List<String> columnNames, List<String> parameters) {
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }

    protected void addInsertDoubleParameter(String columnName, Double parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        addInsertPrimitiveDoubleParameter(columnName, parameter, columnNames, parameters);
    }

    protected void addInsertEnumParameter(String columnName, Enum<?> parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("" + parameter.ordinal());
    }

    protected void addInsertOtherTypeParameter(String columnName, Object parameter, List<String> columnNames, List<String> parameters) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        columnNames.add(columnName);
        parameters.add("" + parameter);
    }

    protected <EE extends ReferentialEntity> void addInsertReferentialComposition(ReferentialDtoEntityContext<?, ?, EE, ?> compositionSpi,
                                                                                  String columnName,
                                                                                  EE parameter,
                                                                                  List<String> columnNames,
                                                                                  List<String> parameters,
                                                                                  Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell) {
        if (addInsertNullParameter(columnName, parameter, columnNames, parameters)) {
            return;
        }
        String compositionId = parameter.getId();
        if (referentialShell != null) {
            referentialShell.put(compositionSpi, compositionId);
        }
        addInsertStringParameter(columnName, compositionId, columnNames, parameters);
    }

    protected <EE extends ReferentialEntity> void addInsertReferentialAssociation(ReferentialDtoEntityContext<?, ?, EE, ?> compositionSpi,
                                                                                  String tableName,
                                                                                  String sourceName,
                                                                                  String targetName,
                                                                                  String id,
                                                                                  Collection<EE> parameter,
                                                                                  Multimap<ReferentialDtoEntityContext<?, ?, ?, ?>, String> referentialShell,
                                                                                  List<String> result) {
        if (parameter == null || parameter.isEmpty()) {
            return;
        }
        for (EE association : parameter) {
            String compositionId = association.getId();
            if (referentialShell != null) {
                referentialShell.put(compositionSpi, compositionId);
            }
            String insertSql = generateAssociationInsertStatement(compositionSpi.getEntitySqlDescriptor().getTable().getSchemaName(),
                                                                  tableName,
                                                                  sourceName,
                                                                  targetName,
                                                                  id,
                                                                  compositionId);
            result.add(insertSql);
//            result.add(SqlStatements.boxAssociationStatement(insertSql));
        }
    }

    public static <EE extends ReferentialEntity, D extends ReferentialDto> void addMissingReferentialIfNecessary(TopiaPersistenceContext service,
                                                                                                                 ReferentialDtoEntityContext<D, ?, EE, ?> compositionSpi,
                                                                                                                 Collection<String> ids,
                                                                                                                 Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide,
                                                                                                                 List<String> result) {
        ReferentialExtraScripts<EE> copyScript = compositionSpi.getExtraScripts();
        Set<String> existingOnlyOnThisSide = idsOnlyExistingOnThisSide.get(compositionSpi.toDtoType());
        if (existingOnlyOnThisSide == null || existingOnlyOnThisSide.isEmpty()) {
            return;
        }
        Set<String> toCopy = new TreeSet<>(ids);
        toCopy.retainAll(existingOnlyOnThisSide);
        if (toCopy.isEmpty()) {
            return;
        }
        existingOnlyOnThisSide.removeAll(toCopy);
        Stream<EE> entities = compositionSpi.loadEntities(service, toCopy);
        entities.forEach(entityToCopy -> {
            List<String> sqlList = copyScript.generateCopyScript(service, entityToCopy, idsOnlyExistingOnThisSide);
            result.addAll(sqlList);
        });

    }

    private String generateInsertStatement(List<String> columnNames, List<String> parameters) {
        return String.format(INSERT_STATEMENT,
                             spi.getEntitySqlDescriptor().getTable().getSchemaName(),
                             spi.getEntitySqlDescriptor().getTable().getTableName(),
                             String.join(",", columnNames),
                             String.join(",", parameters));
    }

    private boolean addInsertNullParameter(String columnName, Object parameter, List<String> columnNames, List<String> parameters) {
        if (parameter == null) {
            columnNames.add(columnName);
            parameters.add("NULL");
            return true;
        }
        return false;
    }

    public static String generateAssociationInsertStatement(String schemaName,
                                                            String tableName,
                                                            String sourceName,
                                                            String targetName,
                                                            String sourceId,
                                                            String targetId) {
        return String.format(SqlStatements.ASSOCIATION_INSERT_STATEMENT,
                             schemaName,
                             tableName,
                             sourceName,
                             targetName,
                             sourceId,
                             targetId);
    }

    private boolean addUpdateNullParameter(String columnName, Object parameter, StringBuilder parameters) {
        if (parameter == null) {
            addUpdateParameter(columnName, "NULL", parameters);
            return true;
        }
        return false;
    }

    private void addUpdateParameter(String columnName, String value, StringBuilder parameters) {
        parameters.append(", ").append(columnName).append(" = ").append(value);
    }

    private String generateUpdateStatement(String sourceId, String values) {
        return String.format(SqlStatements.UPDATE_STATEMENT,
                             spi.getEntitySqlDescriptor().getTable().getSchemaName(),
                             spi.getEntitySqlDescriptor().getTable().getTableName(),
                             values,
                             sourceId);
    }

    private String generateManyToManyAssociationDeleteStatement(String tableName,
                                                                String sourceName,
                                                                String sourceId) {
        return String.format(SqlStatements.ASSOCIATION_DELETE_STATEMENT,
                             spi.getEntitySqlDescriptor().getTable().getSchemaName(),
                             tableName,
                             sourceName,
                             sourceId);
    }
}
