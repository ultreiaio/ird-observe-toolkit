package fr.ird.observe.spi.validation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Set fake ids to an entity (for validation purpose, we need to have entities with ids).
 * <p>
 * Created on 28/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SetFakeIdsEntityVisitor implements TopiaEntityVisitor {
    private static final String CREATE = "__create_";
    private final AtomicLong fakeId = new AtomicLong();
    private final Set<TopiaEntity> explored = new LinkedHashSet<>();

    public void removeIds() {
        explored.forEach(e -> {
            if (e.getTopiaId().startsWith(CREATE)) {
                e.setTopiaId(null);
            }
        });
    }

    @Override
    public void start(TopiaEntity entity) {
        explored.add(entity);
        if (!entity.isPersisted()) {
            // add a fake id
            entity.setTopiaId(CREATE + fakeId.incrementAndGet());
        }
    }

    @Override
    public void end(TopiaEntity entity) {

    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
        if (value instanceof TopiaEntity) {
            addEntity((TopiaEntity) value);
        }
    }

    @Override
    public void visit(TopiaEntity e, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
        Collection<?> cValue = (Collection<?>) value;
        if (TopiaEntity.class.isAssignableFrom(type) && cValue != null && !cValue.isEmpty()) {
            for (Object currentValue : cValue) {
                TopiaEntity childEntity = (TopiaEntity) currentValue;
                addEntity(childEntity);
            }
        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
    }

    @Override
    public void clear() {
        explored.clear();
    }

    private void addEntity(TopiaEntity e) {
        if (e != null && !explored.contains(e)) {
            e.accept(this);
        }
    }
}
