package fr.ird.observe.spi;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataGroupByEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.context.ContainerDtoEntityContext;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.EditableDtoEntityContext;
import fr.ird.observe.spi.context.OpenableDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;
import fr.ird.observe.spi.context.SimpleDtoEntityContext;
import fr.ird.observe.spi.mapping.DtoToEntityContextMapping;
import fr.ird.observe.spi.mapping.EntityToDtoClassMapping;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.util.ServiceLoaders;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 06/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class PersistenceBusinessProject {

    private static PersistenceBusinessProject INSTANCE;
    private final EntityToDtoClassMapping entityToDtoClassMapping;
    private final DtoToEntityContextMapping dtoToEntityContextMapping;
    private final Map<String, GroupBySpiContext<?, ?, ?, ?, ?, ?, ?>> groupBySpiContextMapping;
    private final BusinessProject businessProject;

    public static PersistenceBusinessProject get() {
        return INSTANCE == null ? INSTANCE = ServiceLoaders.loadUniqueService(PersistenceBusinessProject.class) : INSTANCE;
    }

    public static boolean isData(Class<? extends TopiaEntity> type) {
        return DataEntity.class.isAssignableFrom(type);
    }

    public static <D extends BusinessDto, R extends DtoReference, E extends Entity> DtoEntityContext<D, R, E, ?> fromDto(D dto) {
        return get().getDtoToEntityContextMapping().fromDto(dto);
    }

    public static <D extends BusinessDto,
            R extends DtoReference,
            E extends Entity,
            T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromDto(Class<D> dtoType) {
        return get().getDtoToEntityContextMapping().fromDto(dtoType);
    }

    public static <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(Class<D> dtoType) {
        return get().getDtoToEntityContextMapping().fromReferentialDto(dtoType);
    }

    public static <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialReference(Class<R> referenceType) {
        Class<D> dtoType = get().getBusinessProject().getMapping().getDtoType(referenceType);
        return get().getDtoToEntityContextMapping().fromReferentialDto(dtoType);
    }

    public static <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(Class<D> dtoType) {
        return get().getDtoToEntityContextMapping().fromDataDto(dtoType);
    }

    public static <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataReference(Class<R> referenceType) {
        Class<D> dtoType = get().getBusinessProject().getMapping().getDtoType(referenceType);
        return get().getDtoToEntityContextMapping().fromDataDto(dtoType);
    }

    public static <PE extends Entity,
            D extends OpenableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> OpenableDtoEntityContext<PE, D, R, E, T> fromOpenableDto(Class<D> dtoType) {
        return (OpenableDtoEntityContext) fromDataDto(dtoType);
    }

    public static <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>
            > RootOpenableDtoEntityContext<D, R, E, T, H> fromRootOpenableDto(Class<D> dtoType) {
        return (RootOpenableDtoEntityContext) fromDataDto(dtoType);
    }

    public static <PE extends Entity,
            D extends EditableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> EditableDtoEntityContext<PE, D, R, E, T> fromEditableDto(Class<D> dtoType) {
        return (EditableDtoEntityContext) fromDataDto(dtoType);
    }

    public static <C extends ContainerChildDto,
            D extends ContainerDto<C>,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> ContainerDtoEntityContext<D, R, E, T> fromContainerDto(Class<D> dtoType) {
        return (ContainerDtoEntityContext) fromDataDto(dtoType);
    }

    public static <D extends SimpleDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> SimpleDtoEntityContext<D, R, E, T> fromSimpleDto(Class<D> dtoType) {
        return (SimpleDtoEntityContext) fromDataDto(dtoType);
    }

    public static <D extends BusinessDto, R extends DtoReference, E extends Entity> DtoEntityContext<D, R, E, ?> fromEntity(Class<E> entityType) {
        Class<D> dtoType = get().getEntityToDtoClassMapping().get(entityType);
        return get().getDtoToEntityContextMapping().fromDto(dtoType);
    }

    public static <D extends BusinessDto, R extends DtoReference, E extends Entity, T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromEntity(E entity) {
        Class<D> dtoType = get().getEntityToDtoClassMapping().get(entity);
        return get().getDtoToEntityContextMapping().fromDto(dtoType);
    }

    public static <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity, T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialEntity(Class<E> entityType) {
        Class<D> dtoType = get().getEntityToDtoClassMapping().get(entityType);
        return fromReferentialDto(dtoType);
    }

    public static <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialEntity(E entity) {
        Class<D> dtoType = get().getEntityToDtoClassMapping().get(entity);
        return get().getDtoToEntityContextMapping().fromReferentialDto(dtoType);
    }

    public static <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>,
            F extends DataGroupByEntity<E>,
            FF extends DataGroupByDto<D>
            > GroupBySpiContext<D, R, E, T, H, F, FF> fromGroupByName(String dtoType) {
        return (GroupBySpiContext<D, R, E, T, H, F, FF>) get().getGroupBySpiContextMapping().get(dtoType);
    }

    protected PersistenceBusinessProject(EntityToDtoClassMapping entityToDtoClassMapping, DtoToEntityContextMapping dtoToEntityContextMapping, RootOpenableDtoEntityContext<?, ?, ?, ?, ?>... groupByEntityToDefinitionMapping) {
        this.entityToDtoClassMapping = Objects.requireNonNull(entityToDtoClassMapping);
        this.dtoToEntityContextMapping = Objects.requireNonNull(dtoToEntityContextMapping);
        Map<String, GroupBySpiContext<?, ?, ?, ?, ?, ?, ?>> tmp = new LinkedHashMap<>();
        for (RootOpenableDtoEntityContext<?, ?, ?, ?, ?> rootOpenableDtoEntityContext : groupByEntityToDefinitionMapping) {
            rootOpenableDtoEntityContext.getGroupBy().forEach((key, value) -> tmp.put(value.getName(), new GroupBySpiContext(value, rootOpenableDtoEntityContext)));
        }
        this.groupBySpiContextMapping = Collections.unmodifiableMap(tmp);
        this.businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);
    }

    public DtoToEntityContextMapping getDtoToEntityContextMapping() {
        return dtoToEntityContextMapping;
    }

    public EntityToDtoClassMapping getEntityToDtoClassMapping() {
        return entityToDtoClassMapping;
    }

    public Map<String, GroupBySpiContext<?, ?, ?, ?, ?, ?, ?>> getGroupBySpiContextMapping() {
        return groupBySpiContextMapping;
    }

    public BusinessProject getBusinessProject() {
        return businessProject;
    }
}
