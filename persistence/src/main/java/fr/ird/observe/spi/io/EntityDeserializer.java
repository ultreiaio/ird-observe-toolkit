package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created on 02/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.21
 */
public class EntityDeserializer {

    private final EntityDeserializerContext context;

    public EntityDeserializer(ToolkitTopiaPersistenceContextSupport persistenceContext, Supplier<Gson> gson, TopiaIdFactoryForBulkSupport idFactory, Date now) {
        this.context = new EntityDeserializerContext(persistenceContext, gson,idFactory, now);
    }

    public <E extends Entity> void deserialize(Class<E> entityType, E entity, String json) {
        if (entity.isNotPersisted()) {
            context.generateId(entityType, entity);
        }
        Map<String, Object> jsonObject = context.toJson(json);
        context.fillEntity(entity, jsonObject);
    }

    public List<Entity> getCreated() {
        return context.getCreated();
    }
}
