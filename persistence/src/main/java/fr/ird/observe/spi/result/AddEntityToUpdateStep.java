package fr.ird.observe.spi.result;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.spi.context.DtoEntityContext;

import java.util.Collection;

/**
 * Created on 22/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.4
 */
public interface AddEntityToUpdateStep extends AddUpdateLastUpdateDateFieldStep {

    default <E extends Entity> AddEntityToUpdateStep updateCollection(DtoEntityContext<?, ?, E, ?> spi, Collection<E> entityCollection) {
        return updateCollection(spi, entityCollection, true);
    }

    default <E extends Entity> AddEntityToUpdateStep updateCollection(DtoEntityContext<?, ?, E, ?> spi, Collection<E> entityCollection, boolean updateLastUpdateTable) {
        spi.initId(persistenceContext(), entityCollection);
        for (E entity : entityCollection) {
            if (entity.isNotPersisted()) {
                create(spi, entity);
            }
            update(spi, entity, false);
        }
        if (updateLastUpdateTable) {
            updateLastUpdateDateTable(spi);
        }
        return this;
    }

    default <E extends Entity> AddEntityToUpdateStep update(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        return update(spi, entity, true);
    }

    ToolkitTopiaPersistenceContextSupport persistenceContext();

    <E extends Entity> AddEntityToUpdateStep create(DtoEntityContext<?, ?, E, ?> spi, E entity);

    <E extends Entity> AddEntityToUpdateStep createReal(DtoEntityContext<?, ?, E, ?> spi, E entity);

    <E extends Entity> AddEntityToUpdateStep update(DtoEntityContext<?, ?, E, ?> spi, E entity, boolean updateLastUpdateDateTable);

}
