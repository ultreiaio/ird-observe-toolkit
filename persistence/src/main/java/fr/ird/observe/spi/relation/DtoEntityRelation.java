package fr.ird.observe.spi.relation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.entities.Entity;

import java.util.Date;

/**
 * Represents a entity relation associated to a dto.
 * <p>
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DtoEntityRelation<
        PD extends BusinessDto,
        D extends BusinessDto,
        PE extends Entity,
        E extends Entity> {

    private final Class<PE> parentEntityType;
    private final Class<PD> parentDtoType;
    private final Class<E> targetEntityType;
    private final String relationName;
    private final boolean multiple;
    private final Class<D> targetDtoType;

    private final String requestPattern;

    protected DtoEntityRelation(Class<PD> parentDtoType, Class<D> targetDtoType, Class<PE> parentEntityType, Class<E> targetEntityType, String relationName, boolean multiple) {
        this.parentEntityType = parentEntityType;
        this.parentDtoType = parentDtoType;
        this.targetEntityType = targetEntityType;
        this.relationName = relationName;
        this.multiple = multiple;
        this.targetDtoType = targetDtoType;
        this.requestPattern = String.format("%s::%%s::%s-%s", targetEntityType.getName(), parentEntityType().getName(), relationName);
    }

    /**
     * @return parent entity type
     */
    public final Class<PE> parentEntityType() {
        return parentEntityType;
    }

    /**
     * @return parent dto type
     */
    public final Class<PD> parentDtoType() {
        return parentDtoType;
    }

    /**
     * @return the entity relation name
     */
    public final String relationName() {
        return relationName;
    }

    /**
     * @return target entity type
     */
    public final Class<E> targetEntityType() {
        return targetEntityType;
    }

    /**
     * @return target dto type
     */
    public final Class<D> targetDtoType() {
        return targetDtoType;
    }

    /**
     * @return {@code true} if relation is multiple
     */
    public final boolean multiple() {
        return multiple;
    }

    public final String requestName(String flavor) {
        return String.format(requestPattern, flavor);
    }

    public final ToolkitIdDtoBean toId(String id, Date timestamp) {
        return ToolkitIdDtoBean.of(targetDtoType(), id, timestamp);

    }
}
