package fr.ird.observe.spi.type;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.entities.Entity;
import io.ultreia.java4all.i18n.spi.type.TypeTranslator;
import io.ultreia.java4all.i18n.spi.type.SimpleTypeTranslatorSupport;

/**
 * Created on 25/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@AutoService(TypeTranslator.class)
public class EntityTypeTranslator extends SimpleTypeTranslatorSupport {

    public EntityTypeTranslator() {
        super("fr.ird.observe.entities", Entity.class, "Impl");
    }

    @Override
    public String cleanSimpleName(String simpleName) {
        String result = super.cleanSimpleName(simpleName);
        // for entity, hibernate proxy gives use some class name like Entity_xxx
        int index = result.indexOf("_");
        if (index > -1) {
            result = result.substring(0, index);
        } else {
            // for entity, hibernate
            index = result.indexOf("@");
            if (index > -1) {
                result = result.substring(0, index);
            }
        }
        return result;
    }
}
