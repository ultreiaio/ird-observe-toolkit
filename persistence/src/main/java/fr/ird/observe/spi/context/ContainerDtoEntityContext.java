package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.nuiton.topia.persistence.TopiaDao;

/**
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class ContainerDtoEntityContext<
        D extends ContainerDto<?>,
        R extends DataDtoReference,
        E extends DataEntity,
        T extends TopiaDao<E>>
        extends DataDtoEntityContext<D, R, E, T> {

    public abstract void saveCallback(AddEntityToUpdateStep saveHelper, E entity);

    public SaveResultDto save(ServiceContext context, D dto) throws ConcurrentModificationException {
        E entity = loadOrCreateEntityFromDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        fromDto(context.getReferentialLocale(), entity, dto);
        AddEntityToUpdateStep saveHelper = newSaveHelper(context)
                .update(this, entity, false);
        saveCallback(saveHelper, entity);
        return saveHelper
                .updateLastUpdateDateTable(this)
                .build(entity);
    }
}
