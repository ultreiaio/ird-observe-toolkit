package fr.ird.observe.spi.validation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.validation.bean.BeanValidatorListener;
import io.ultreia.java4all.validation.bean.BeanValidator;
import io.ultreia.java4all.validation.bean.BeanValidatorEvent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import static io.ultreia.java4all.i18n.I18n.l;

/**
 * Created on 31/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class MessagesCollector implements BeanValidatorListener {

    /**
     * Locale used to translate validation messages.
     */
    private final Locale locale;
    private final LinkedList<ValidationResultDtoMessage> messages;

    public MessagesCollector(Locale locale) {
        this.locale = locale;
        this.messages = new LinkedList<>();
    }

    public <D extends BusinessDto> LinkedList<ValidationResultDtoMessage> collect(BeanValidator<D> validator, D dto) {
        messages.clear();
        validator.addBeanValidatorListener(this);
        try {
            validator.setBean(dto);
        } finally {
            validator.removeBeanValidatorListener(this);
            validator.setBean(null);
        }
        return messages;
    }

    @Override
    public void onFieldChanged(BeanValidatorEvent<?> event) {
        if (event.getMessagesToAdd() == null) {
            return;
        }
        for (String message : event.getMessagesToAdd()) {
            String translateMessage = translateMessage(message);
            ValidationResultDtoMessage resultMessage = new ValidationResultDtoMessage(event.getScope(), event.getField(), translateMessage);
            messages.add(resultMessage);
        }
    }

    private String translateMessage(String message) {
        String text;
        if (!message.contains("##")) {
            text = l(locale, message);
        } else {
            //noinspection StringTokenizerDelimiter
            StringTokenizer stk = new StringTokenizer(message, "##");
            String errorName = stk.nextToken();
            List<String> args = new ArrayList<>();
            while (stk.hasMoreTokens()) {
                args.add(stk.nextToken());
            }
            text = l(locale, errorName, args.toArray());
        }
        return text;
    }
}
