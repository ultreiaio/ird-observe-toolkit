package fr.ird.observe.spi.validation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.navigation.id.IdProject;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.validation.ValidationContextSupport;
import fr.ird.observe.validation.api.result.ValidationResultBuilder;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.validation.bean.NuitonValidatorsMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * FIXME Move this to toolkit-validation
 * To run validation on a given entity and then walk through his shell.
 * <p>
 * while the visit, we keep any validation messages detected.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.3
 */
public class ValidationMessageDetector {

    private static final Logger log = LogManager.getLogger(ValidationMessageDetector.class);

    private final ValidationEntityVisitor entityVisitor;

    private ValidationMessageDetector(ServiceContext context,
                                      ReferentialLocale referentialLocale,
                                      ValidationContextSupport validationDataContext,
                                      ValidationResultBuilder resultBuilder,
                                      Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptors,
                                      boolean canWalkInSubclasses) {
        this.entityVisitor = new ValidationEntityVisitor(context,
                                                         validationDataContext,
                                                         interceptors,
                                                         resultBuilder,
                                                         new MessagesCollector(referentialLocale.getLocale()),
                                                         canWalkInSubclasses);
    }

    public static ValidationMessageDetector forData(ServiceContext context,
                                                    NuitonValidatorsMap validators,
                                                    ValidationContextSupport validationDataContext,
                                                    IdProject selectModel,
                                                    ReferentialLocale referentialLocale,
                                                    ValidationResultBuilder resultBuilder) {
        Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptors = EntityInterceptors.forData(validators,
                                                                                            validationDataContext,
                                                                                            selectModel,
                                                                                            referentialLocale);
        return new ValidationMessageDetector(context,
                                             referentialLocale,
                                             validationDataContext,
                                             resultBuilder,
                                             interceptors,
                                             true);
    }

    public static ValidationMessageDetector forReferential(ServiceContext context,
                                                           NuitonValidatorsMap validators,
                                                           ValidationContextSupport validationDataContext,
                                                           ReferentialLocale referentialLocale,
                                                           ValidationResultBuilder resultBuilder) {
        Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptors = EntityInterceptors.forReferential(validators,
                                                                                                   validationDataContext,
                                                                                                   referentialLocale);
        return new ValidationMessageDetector(context,
                                             referentialLocale,
                                             validationDataContext,
                                             resultBuilder,
                                             interceptors,
                                             false);
    }

    public <E extends Entity> void detectValidationMessages(E entity) {
        try {
            entity.accept(entityVisitor);
        } finally {
            entityVisitor.clear();
        }
    }

    public <E extends Entity> void detectValidationMessages(Iterable<E> entities) {
        for (E entity : entities) {
            entity.accept(entityVisitor);
        }
        entityVisitor.clear();
    }

    private static class ValidationEntityVisitor implements TopiaEntityVisitor {

        private final ServiceContext context;
        private final ValidationContextSupport validationDataContext;
        private final Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptors;
        private final Deque<Entity> path;
        private final Set<TopiaEntity> explored;
        private final ValidationResultBuilder resultBuilder;
        private final MessagesCollector messagesCollector;

        private final boolean canWalkInSubclasses;

        ValidationEntityVisitor(ServiceContext context,
                                ValidationContextSupport validationDataContext,
                                Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptors,
                                ValidationResultBuilder resultBuilder,
                                MessagesCollector messagesCollector, boolean canWalkInSubclasses) {
            this.context = context;
            this.validationDataContext = validationDataContext;
            this.interceptors = interceptors;
            this.resultBuilder = resultBuilder;
            this.messagesCollector = messagesCollector;
            this.path = new LinkedList<>();
            this.explored = new HashSet<>();
            this.canWalkInSubclasses = canWalkInSubclasses;
        }

        @Override
        public void start(TopiaEntity e) {
            log.debug(String.format("Validate %s", e.getTopiaId()));
            if (path.isEmpty()) {
                // start come in start method since last clear method invocation
                path.add((Entity) e);
            }
            explored.add(e);
            start0((Entity) e);
        }

        @Override
        public void end(TopiaEntity e) {
            if (path.size() == 1) {
                // global visit is done
                path.removeLast();
            }
            end0((Entity) e);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> type, Object value) {
            Entity e1 = getTopiaValue(value);
            addEntity(e1);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type, int index, Object value) {
            Entity e1 = getTopiaValue(value);
            addEntity(e1);
        }

        @Override
        public void visit(TopiaEntity e, String name, Class<?> collectionType, Class<?> type, Object value) {
            Collection<?> cValue = (Collection<?>) value;
            if (Entity.class.isAssignableFrom(type) && cValue != null && !cValue.isEmpty()) {
                int i = 0;
                for (Object currentValue : cValue) {
                    visit(e, name, collectionType, type, i++, currentValue);
                }
            }
        }

        @Override
        public void clear() {
            explored.clear();
            validationDataContext.reset();
            interceptors.values().forEach(EntityInterceptor::clear);
        }

        public Deque<Entity> getPath() {
            return path;
        }

        private <D extends BusinessDto, E extends Entity> void start0(E e) {
            EntityInterceptor<D, E, ?> interceptor = getInterceptor(e);
            if (interceptor == null) {
                return;
            }
            LinkedList<ValidationResultDtoMessage> messages = interceptor.intercept(context, messagesCollector, validationDataContext, path, e);
            resultBuilder.addMessages(getPath(), messages);
        }

        private <D extends BusinessDto, E extends Entity> void end0(E e) {
            EntityInterceptor<D, E, ?> interceptor = getInterceptor(e);
            if (interceptor == null) {
                return;
            }
            interceptor.release(validationDataContext, e.getTopiaId());
        }

        private Entity getTopiaValue(Object value) {
            Entity entity = (Entity) (value instanceof Entity ? value : null);
            if (entity != null && explored.contains(entity)) {
                // already processed
                entity = null;
            }
            return entity;
        }

        private void addEntity(Entity e1) {
            if (canWalkInSubclasses && e1 != null) {
                path.add(e1);
                try {
                    e1.accept(this);
                } finally {
                    path.removeLast();
                }
            }
        }

        public <D extends BusinessDto, E extends Entity> EntityInterceptor<D, E, ?> getInterceptor(E e) {
            @SuppressWarnings("unchecked") EntityInterceptor<D, E, ?> interceptor = (EntityInterceptor<D, E, ?>) interceptors.get(e.contractType());
            return interceptor;
        }
    }
}


