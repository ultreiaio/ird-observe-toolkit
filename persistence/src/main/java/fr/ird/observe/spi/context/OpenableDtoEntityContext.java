package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.data.MoveRequest;
import fr.ird.observe.spi.relation.WithParent;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public abstract class OpenableDtoEntityContext<
        PE extends Entity,
        D extends OpenableDto,
        R extends DataDtoReference,
        E extends DataEntity,
        T extends TopiaDao<E>> extends DataDtoEntityContext<D, R, E, T> implements WithParent {

    public abstract Collection<E> getChildrenFromParent(PE parent);

    public abstract void addChildrenToParent(PE parent, E entity);

    public abstract DtoEntityContext<?, ?, PE, ?> parentSpi();

    public Form<D> preCreate(ServiceContext context, PE parent, E preCreated) {
        Form<D> form = entityToForm(context.getReferentialLocale(), preCreated);
        loadDtoForValidation(context, parent, preCreated, form.getObject());
        return form;
    }

    @Override
    public final Form<D> loadForm(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        Form<D> form = entityToForm(context.getReferentialLocale(), entity);
        PE parent = getParent(context, id);
        loadDtoForValidation(context, parent, entity, form.getObject());
        return form;
    }

    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, PE parent, E entity, D dto, boolean needCopy) {
        if (needCopy) {
            fromDto(context.getReferentialLocale(), entity, dto);
        }
        if (dto.isNotPersisted()) {
            addChildrenToParent(parent, entity);
        }
        return null;
    }

    public Set<ToolkitIdDtoBean> getRealDependencies(ServiceContext context, Set<String> ids) {
        return ids.stream().map(id -> ToolkitIdDtoBean.of(toDtoType(), id)).collect(Collectors.toSet());
    }

    public final ReplicateRequest toSqlRequest(ServiceContext context, MoveRequest request) {
        return toSqlRequest(context.isPostgresDatabase(), request);
    }

    @Override
    protected DeleteRequest createDeleteRequest(ServiceContext context, String id) {
        PE parentId = getParent(context, id);
        return new DeleteRequest(context.isPostgresDatabase(), toEntityType().getName(), parentId.getTopiaId(), id);
    }

    @Override
    protected void onDeleteUpdateLastUpdateDate(ServiceContext context, String parentId, Date now) {
        super.onDeleteUpdateLastUpdateDate(context, parentId, now);
        // also update lastUpdateDateField on entities to root
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2786
        parentSpi().updateLastUpdateDateField(context.getTopiaPersistenceContext(), parentId, now);
    }

    public final ReplicateRequest toSqlRequest(boolean pg, MoveRequest request) {
        return new ReplicateRequest(pg,
                                    request.getOldParentId().getId(),
                                    request.getNewParentId().getId(),
                                    toEntityType().getName(),
                                    request.getIds().toArray(new String[0]));
    }

    public Set<String> move(ServiceContext context, MoveRequest request) throws ConcurrentModificationException {
        ToolkitIdDtoBean oldParentId = request.getOldParentId();
        DataDtoEntityContext<DataDto, ?, DataEntity, ?> oldParentSpi = context.fromDataDto(oldParentId);
        DataEntity oldParent = oldParentSpi.loadEntity0(context, oldParentId);

        ToolkitIdDtoBean newParentId = request.getNewParentId();
        DataDtoEntityContext<DataDto, ?, DataEntity, ?> newParentSpi = context.fromDataDto(newParentId);
        DataEntity newParent = newParentSpi.loadEntity0(context, newParentId);

        ReplicateRequest replicateRequest = toSqlRequest(context, request);
        DeleteRequest deleteRequest = replicateRequest.toDeleteRequest();
        SqlScript deleteScript = consume(context, deleteRequest);
        SqlScript replicateScript = consume(context, replicateRequest);
        context.getTopiaPersistenceContext().executeSqlScript(deleteScript);
        context.getTopiaPersistenceContext().executeSqlScript(replicateScript);
        String idPattern1 = "(topiaId, ";
        String idPattern = " VALUES ('" + context.getTopiaApplicationContext().getTopiaIdFactory().newTopiaId(toEntityType(), "", "").replace("##", "#");
        Set<String> newIds = new TreeSet<>();
        try (SqlScriptReader reader = replicateScript.getLocation()) {
            reader.forEach(statement -> {
                if (!statement.contains(idPattern1)) {
                    return;
                }
                int index = statement.indexOf(idPattern);
                if (index == -1) {
                    return;
                }
                int beginIndex = statement.indexOf("'", index) + 1;
                int endIndex = statement.indexOf("'", beginIndex);
                String id = statement.substring(beginIndex, endIndex);
                newIds.add(id);
            });
        } catch (IOException e) {
            throw new TopiaException("Can't read script", e);
        }
        newSaveHelper(context)
                .updateLastUpdateDateField(oldParentSpi, oldParent)
                .updateLastUpdateDateField(newParentSpi, newParent)
                .updateLastUpdateDateTable(this).build();
        return newIds;
    }

    public final PE getParent(ServiceContext context, String id) {
        ToolkitParentIdDtoBean parentId = getParentId(context, id);
        return parentSpi().loadEntity(context, parentId.getTopiaId());
    }

    public void loadDtoForValidation(ServiceContext context, PE parent, E entity, D dto) {
    }

    @Override
    public final void loadDtoForValidation(ServiceContext context, E entity, D dto) {
        PE parent = getParent(context, entity.getTopiaId());
        loadDtoForValidation(context, parent, entity, dto);
    }

    public final List<ToolkitIdLabel> getBrothersLabel(ServiceContext context, String id) {
        ToolkitParentIdDtoBean parentId = getParentId(context, id);
        return getBrothersFromParentLabel(context, parentId.getTopiaId(), id);
    }

    public final List<ToolkitIdLabel> getBrothersFromParentLabel(ServiceContext context, String parentId, String id) {
        PE parentEntity = parentSpi().loadEntity(context, parentId);
        Collection<E> brothers = getChildrenFromParent(parentEntity);
        Stream<E> stream = brothers.stream();
        if (id != null) {
            stream = stream.filter(e -> !id.equals(e.getTopiaId()));
        }
        return toLabelList(context.getApplicationLocale(), stream);
    }

    public final DataDtoReferenceSet<R> getChildren(ServiceContext context, String parentId) {
        PE parent = parentSpi().loadEntity(context, parentId);
        Collection<E> children = getChildrenFromParent(parent, null);
        return newDataDtoReferenceSetBuilder(getChildrenExtraConsumer(parent)).build(context.getReferentialLocale(), children, context.now());
    }

    public final UpdatedDataDtoReferenceSet<R> getChildrenUpdate(ServiceContext context, String parentId, Date lastUpdate) {
        PE parent = parentSpi().loadEntity(context, parentId);
        Set<String> allIds = getChildrenIds(parent);
        Collection<E> children0 = getChildrenFromParent(parent, lastUpdate);
        DataDtoReferenceSet<R> referenceSet = newDataDtoReferenceSetBuilder(getChildrenExtraConsumer(parent)).build(context.getReferentialLocale(), children0, context.now());
        return new UpdatedDataDtoReferenceSet<>(allIds, referenceSet);
    }

    public final Collection<E> getChildrenFromParent(PE parent, Date lastUpdate) {
        Collection<E> collection = getChildrenFromParent(parent);
        if (lastUpdate != null) {
            collection = collection.stream().filter(e -> e.getLastUpdateDate().after(lastUpdate)).collect(Collectors.toList());
        }
        return collection;
    }

    public final Set<String> getChildrenIds(PE parent) {
        Collection<E> collection = getChildrenFromParent(parent);
        return collection.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());
    }

    public final UsageCount getDependenciesCount(ServiceContext context, Function<ToolkitIdDtoBean, UsageCount> usageCountFunction, Set<String> ids) {
        Set<ToolkitIdDtoBean> newRequest = getRealDependencies(context, ids);
        return UsageCount.count(usageCountFunction, newRequest);
    }

    public final Set<ToolkitIdLabel> getDependencies(ServiceContext context, Function<ToolkitIdDtoBean, Set<ToolkitIdLabel>> usageCountFunction, Set<String> ids) {
        Set<ToolkitIdDtoBean> newRequest = getRealDependencies(context, ids);
        Set<ToolkitIdLabel> result = new LinkedHashSet<>();
        for (ToolkitIdDtoBean id : newRequest) {
            Set<ToolkitIdLabel> apply = usageCountFunction.apply(id);
            result.addAll(apply);
        }
        return result;
    }

    public final Form<D> preCreate(ServiceContext context, String parentId) {
        PE parent = parentSpi().loadEntity(context, parentId);
        E preCreated = newEntity(context.now());
        return preCreate(context, parent, preCreated);
    }

    public final SaveResultDto save(ServiceContext context, String parentId, D dto) throws ConcurrentModificationException {
        PE parent = parentSpi().loadEntity(context, parentId);
        E entity = loadOrCreateEntityFromDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        Consumer<AddEntityToUpdateStep> extraConsumer = onSave(context, parent, entity, dto, true);
        return saveEntity(context, entity, extraConsumer);
    }

    protected final SaveResultDto saveEntity(ServiceContext context, E entity, Consumer<AddEntityToUpdateStep> extraConsumer) {
        AddEntityToUpdateStep saveHelper = newSaveHelper(context).update(this, entity);
        if (extraConsumer != null) {
            extraConsumer.accept(saveHelper);
        }
        return saveHelper.build(entity);
    }

    protected BiConsumer<E, R> getChildrenExtraConsumer(PE parent) {
        return null;
    }
}
