package fr.ird.observe.spi.migration;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.migration.TopiaMigrationServiceException;
import org.nuiton.topia.service.migration.resources.MigrationVersionResource;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceExecutor;
import org.nuiton.topia.service.migration.resources.MigrationVersionResourceScriptLayout;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 08/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.x
 */
public abstract class ByMajorMigrationVersionResource extends MigrationVersionResource {

    public static final String associationGetSql = "SELECT DISTINCT a.%2$s, a.%3$s FROM %1$s a INNER JOIN %1$s b ON a.%2$s = b.%2$s AND a.%3$s = b.%3$s GROUP BY a.%2$s, a.%3$s HAVING COUNT( b.%3$s ) > 1";
    public static final String oneToOneGetIDsSql = "SELECT _target.topiaId FROM %1$s _target";
    public static final String oneToOneGetSql = "SELECT a.topiaId, a.%2$s FROM %1$s a WHERE a.%2$s IS NOT NULL;";
    public static final String associationDeleteSql = "DELETE FROM %1$s WHERE %2$s ='%4$s' and %3$s = '%5$s';";
    public static final String associationInsertSql = "INSERT INTO %1$s (%2$s, %3$s) VALUES('%4$s', '%5$s');";
    public static final String oneToOneUpdateSqlPrefix = "UPDATE %1$s SET %2$s =";
    public static final String oneToOneUpdateSql = "%1$s '%2$s' WHERE topiaId = '%3$s';";
    public static final String REFERENTIAL_PREFIX = "1464000000000#";


    /**
     * Template to create trigger.
     * <p>
     * Arguments are :
     * <ul>
     *     <li>$1 : name of trigger</li>
     *     <li>$2 : latitude column name</li>
     *     <li>$3 : longitude column name</li>
     *     <li>$4 : the_geom column name</li>
     * </ul>
     */
    public static final String TRIGGER_TEMPLATE = "" +
            "CREATE OR REPLACE FUNCTION %1$s() RETURNS TRIGGER AS \n" +
            "$$ \n" +
            "BEGIN \n" +
            "    IF (TG_OP = 'DELETE') THEN \n" +
            "        RETURN OLD; \n" +
            "    END IF; \n" +
            "    IF (NEW.%2$s IS NULL OR NEW.%3$s IS NULL) THEN \n" +
            "        RAISE NOTICE 'No %2$s or %3$s, can not compute postgis field for id %% ', NEW.topiaId; \n" +
            "        NEW.%4$s := NULL; \n" +
            "        return NEW; \n" +
            "    END IF; \n" +
            "    IF (TG_OP = 'UPDATE' AND NEW.%2$s = OLD.%2$s AND NEW.%3$s = OLD.%3$s) \n" +
            "    THEN \n" +
            "        return NEW; \n" +
            "    END IF; \n" +
            "    RAISE NOTICE 'Will compute %4$s for %%.%% %% - %2$s %% and %3$s %%', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.%2$s, NEW.%3$s; \n" +
            "    NEW.%4$s := ST_SetSRID(ST_MakePoint(NEW.%3$s, NEW.%2$s), 4326); \n" +
            "    RAISE NOTICE 'Computed for %%.%% %% %2$s %% and %3$s %%, %4$s %%', TG_TABLE_SCHEMA, TG_TABLE_NAME, NEW.topiaId, NEW.%2$s, NEW.%3$s, NEW.%4$s; \n" +
            "    RETURN NEW; \n" +
            "END; \n" +
            "$$ LANGUAGE 'plpgsql';";

    /**
     * To add a gis column.
     * Arguments are :
     * * <ul>
     * *     <li>$1 : name of trigger</li>
     * *     <li>$2 : latitude column name</li>
     * *     <li>$3 : longitude column name</li>
     * *     <li>$4 : the_geom column name</li>
     * *     <li>$5 : schema name</li>
     * *     <li>$6 : table name</li>
     * *     <li>$7 : table name (lowercase)</li>
     * * </ul>
     */
    public static final String GIS_TEMPLATE = "" +
            "SELECT AddGeometryColumn('%5$s', '%7$s', '%4$s', 4326, 'POINT', 2);\n" +
            "CREATE INDEX IF NOT EXISTS idx_%5$s_%7$s_gist ON %5$s.%6$s USING GIST (%4$s);\n" +
            "DROP TRIGGER IF EXISTS tr_sync_%5$s_%7$s_%4$s ON %5$s.%6$s;\n" +
            "CREATE TRIGGER tr_sync_%5$s_%7$s_%4$s BEFORE INSERT OR UPDATE OF %2$s, %3$s ON %5$s.%6$s FOR EACH ROW EXECUTE PROCEDURE %1$s();\n" +
            "UPDATE %5$s.%6$s SET %4$s = ST_SetSRID(ST_MakePoint(%3$s, %2$s), 4326) WHERE %2$s IS NOT NULL AND %3$s IS NOT NULL AND %4$s IS NULL;";

    private static final Logger log = LogManager.getLogger(ByMajorMigrationVersionResource.class);

    static class ByMajorMigrationVersionResourceScriptLayout implements MigrationVersionResourceScriptLayout {

        private final String major;

        ByMajorMigrationVersionResourceScriptLayout(Version version) {
            this.major = getMajor(version);
        }

        @Override
        public String getScriptPath(Version version, String rank, String prefix, String classifier) {
            String migrationScript = String.format("%s-%s.sql", prefix, classifier);
            return String.format("/db/migration/%s/%s/%s_%s", major, version, rank, migrationScript);
        }
    }

    public static String getMajor(Version version) {
        return "v" + version.getComponent(0).getValue();
    }

    public static TreeMap<String, String> createScriptVariables(String date, String timestamp) {
        return new TreeMap<>(Map.of(
                "CURRENT_DATE", String.format("'%s'::date", date),
                "CURRENT_TIMESTAMP", String.format("'%s'::timestamp", timestamp),
                "REFERENTIAL_PREFIX", REFERENTIAL_PREFIX,
                "LAST_UPDATE_PREFIX", "fr.ird.common.LastUpdateDate#0#"
        ));
    }

    public static void createResourceScriptVariables(MigrationVersionResource resource, String date, String timestamp) {
        resource.setScriptVariables(createScriptVariables(date, timestamp));
    }

    public static void addNewTable(MigrationVersionResourceExecutor executor, boolean withIds, String scriptPrefix, String... scriptNames) {
        List<String> scriptNamesList = Arrays.asList(scriptNames);
        scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "create-" + scriptName));
        if (withIds) {
            scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "fill-" + scriptName));
        }
        scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "finalize-" + scriptName));
    }

    public static void addNewSchema(MigrationVersionResourceExecutor executor, boolean withIds, String scriptPrefix, String... scriptNames) {
        List<String> scriptNamesList = Arrays.asList(scriptNames);
        scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "create-" + scriptName));
        if (withIds) {
            scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "fill-" + scriptName));
        }
        scriptNamesList.forEach(scriptName -> executor.addScript(scriptPrefix, "finalize-" + scriptName));
    }

    public static void removeDuplicatedAssociations(MigrationVersionResourceExecutor executor, boolean withIds, String schemaName, String mainTable, String dependencyTable) {
        removeDuplicatedAssociations(executor, withIds, schemaName, mainTable + "_" + dependencyTable, mainTable, dependencyTable);
    }

    public static void removeDuplicatedAssociations(MigrationVersionResourceExecutor executor, boolean withIds, String schemaName, String tableName, String mainTable, String dependencyTable) {
        if (!withIds) {
            return;
        }
        String fullyTableName = schemaName + "." + tableName;
        Set<Pair<String, String>> duplicatedPairs = executor.findMultipleResultAstSet(SqlQuery.wrap(String.format(associationGetSql, fullyTableName, mainTable, dependencyTable), resultSet -> Pair.of(resultSet.getString(1), resultSet.getString(2))));
        if (!duplicatedPairs.isEmpty()) {
            log.warn(String.format("Found %d duplicated rows on table: %s", duplicatedPairs.size(), fullyTableName));
            for (Pair<String, String> entry : duplicatedPairs) {
                String mainTableId = entry.getKey();
                String dependencyTableId = entry.getValue();
                log.warn(String.format("Found %s - %s  duplicated row on table: %s", mainTableId, dependencyTableId, fullyTableName));
                executor.writeSql(String.format(associationDeleteSql, fullyTableName, mainTable, dependencyTable, mainTableId, dependencyTableId));
                executor.writeSql(String.format(associationInsertSql, fullyTableName, mainTable, dependencyTable, mainTableId, dependencyTableId));
            }
        }
    }

    public static void migrateOneToOneComposition(MigrationVersionResourceExecutor executor, String schemaName, String sourceTable, String targetTable, String optionalSelectIdsWhereClause) {
        executor.doSqlWork(connection -> {
            String targetTableWithSchema = schemaName + "." + targetTable;
            String allIdSql = String.format(oneToOneGetIDsSql, targetTableWithSchema);
            if (optionalSelectIdsWhereClause != null) {
                allIdSql += " WHERE " + optionalSelectIdsWhereClause;
            }
            List<String> allIds = executor.findMultipleResult(SqlQuery.wrap(allIdSql, r -> r.getString(1)));
            String sourceTableWithSchema = schemaName + "." + sourceTable;
            String sql = String.format(oneToOneGetSql, sourceTableWithSchema, targetTable);
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                String updateSqlPrefix = String.format(oneToOneUpdateSqlPrefix, targetTableWithSchema, sourceTable);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String id = resultSet.getString(2);
                        String updateSql = String.format(oneToOneUpdateSql, updateSqlPrefix, resultSet.getString(1), id);
                        executor.writeSql(updateSql);
                        allIds.remove(id);
                    }
                }
            }
            if (!allIds.isEmpty()) {
                throw new TopiaMigrationServiceException(String.format("Found some one-to-one relation orphan (column: %s.%s is null): %s", targetTableWithSchema, sourceTable, allIds));
            }
        });
    }

    public ByMajorMigrationVersionResource(Version version) {
        this(version, false);
    }


    public ByMajorMigrationVersionResource(Version version, boolean requiresFinalize) {
        super(Objects.requireNonNull(version), new ByMajorMigrationVersionResourceScriptLayout(version), requiresFinalize);
    }

    public void cleanVariables(String... prefixes) {
        Map<String, String> scriptVariables = getScriptVariables();
        Set<String> keySet = new LinkedHashSet<>(scriptVariables.keySet());
        for (String prefix : prefixes) {
            List<String> toRemove = keySet.stream().filter(s -> s.startsWith(prefix)).collect(Collectors.toList());
            toRemove.forEach(scriptVariables::remove);
            toRemove.forEach(keySet::remove);
        }
    }


    protected void addTrigger(MigrationVersionResourceExecutor executor, String name, String latitude, String longitude, String gis, List<String> tables) {
        executor.doSqlWork(connection -> {
            try (PreparedStatement p = connection.prepareCall(String.format(TRIGGER_TEMPLATE, name, latitude, longitude, gis))) {
                p.execute();
            }
        });
        tables.forEach((schema_table) -> {
            String[] split = schema_table.split("\\.");
            addGis(executor, split[0], split[1], name, latitude, longitude, gis);
        });
    }

    private void addGis(MigrationVersionResourceExecutor executor, String schema, String table, String name, String latitude, String longitude, String gis) {
        executor.writeSql(String.format(GIS_TEMPLATE,
                                        name,
                                        latitude,
                                        longitude,
                                        gis,
                                        schema,
                                        table,
                                        table.toLowerCase()
        ));
    }
}

