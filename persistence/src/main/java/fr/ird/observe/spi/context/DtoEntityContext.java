package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.DataNotFoundException;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.dto.stats.WithStatisticsBuilder;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelContentRequest;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.services.service.api.OrderEnum;
import fr.ird.observe.spi.io.EntityDeserializer;
import fr.ird.observe.spi.io.EntitySerializer;
import fr.ird.observe.spi.io.EntitySerializerResult;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.navigation.tree.DtoToToolkitTreePathMapping;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.result.PersistenceResultBuilder;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.sql.SqlScriptReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.filter.EntityFilterConsumer;
import org.nuiton.topia.persistence.filter.ToolkitRequestFilter;
import org.nuiton.topia.persistence.internal.AbstractTopiaDao;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.script.UpdateLastUpdateDateFieldScript;
import org.nuiton.topia.service.sql.script.UpdateLastUpdateDateTableScript;
import org.nuiton.topia.service.sql.usage.TopiaUsageEntity;

import java.net.URL;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created on 08/02/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public interface DtoEntityContext<
        D extends BusinessDto,
        R extends DtoReference,
        E extends Entity,
        T extends TopiaDao<E>> {

    String DEFINITION_CLASSIFIER = "definition.json";
    String VALIDATION_CREATE_CLASSIFIER = "validation-create.json";
    String VALIDATION_UPDATE_CLASSIFIER = "validation-update.json";
    Logger log = LogManager.getLogger(DtoEntityContext.class);

    default <Z> Consumer<Z> andThen(Consumer<Z> consumer, Consumer<Z> nextConsumer) {
        Objects.requireNonNull(nextConsumer);
        return consumer == null ? nextConsumer : consumer.andThen(nextConsumer);
    }

    default URL getDefinitionContentURL() {
        return getClass().getResource(toEntityType().getSimpleName() + "/" + DEFINITION_CLASSIFIER);
    }

    default URL getValidationCreateContentURL() {
        return getClass().getResource(toEntityType().getSimpleName() + "/" + VALIDATION_CREATE_CLASSIFIER);
    }

    default URL getValidationUpdateContentURL() {
        return getClass().getResource(toEntityType().getSimpleName() + "/" + VALIDATION_UPDATE_CLASSIFIER);
    }

    String getDefinitionContent();

    String getValidationCreateContent();

    String getValidationUpdateContent();

    Class<D> toDtoType();

    Class<E> toEntityType();

    E newEntity();

    default Class<R> toReferenceType() {
        throw new NoSuchElementException("toReferenceType not defined for: " + getClass().getName());
    }

    default String getReferenceQuery() {
        throw new NoSuchElementException("getReferenceQuery not defined for: " + getClass().getName());
    }

    E newEntity(Date now);

    E newEntity(String id);

    default E newEntity(ToolkitId incoming) {
        E e = newEntity();
        e.copyTechnicalFields(incoming);
        return e;
    }

    E newEntity(String id, Date now);

    default D toDto(ServiceContext context, E entity) {
        return toDto(context.getReferentialLocale(), entity);
    }

    D toDto(ReferentialLocale referentialLocale, E entity);

    default E toEntity(ServiceContext context, D dto) {
        return toEntity(context.getReferentialLocale(), dto);
    }

    E toEntity(ReferentialLocale referentialLocale, D dto);

    default EntityFilterConsumer<E> newFilterConsumer(ServiceContext context) {
        return newFilterConsumer(context.getReferentialLocale());
    }

    EntityFilterConsumer<E> newFilterConsumer(ReferentialLocale referentialLocale);

    default List<String> getNaturalOrder() {
        return Collections.emptyList();
    }

    default TreeMap<String, OrderEnum> addNaturalOrder(TreeMap<String, OrderEnum> orders) {
        if (orders == null || orders.isEmpty()) {
            // use natural order
            orders = new TreeMap<>();
            for (String naturalOrder : getNaturalOrder()) {
                orders.put(naturalOrder, OrderEnum.ASC);
            }
        }
        return orders;
    }

    List<E> filter(ServiceContext context, ToolkitRequestFilter filter);

    UpdateLastUpdateDateFieldScript getUpdateLastUpdateDateFieldScript();

    UpdateLastUpdateDateTableScript getUpdateLastUpdateDateTableScript();

    TopiaEntitySqlDescriptor getEntitySqlDescriptor();

    T getDao(TopiaPersistenceContext persistenceContext);

    T getDao(ServiceContext context);

    R loadEntityToReferenceDto(ServiceContext context, String id);

    D loadEntityToDto(ServiceContext context, String id);

    default Form<D> entityToForm(ServiceContext context, E entity) {
        return entityToForm(context.getReferentialLocale(), entity);
    }

    Form<D> loadForm(ServiceContext context, String id);

    Form<D> entityToForm(ReferentialLocale referentialLocale, E entity);

    default void loadDtoForValidation(ServiceContext context, E entity, D dto) {
        // by default nothing to do
    }

    DtoReferenceCollection<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, Date now, String classifier);

    TopiaUsageEntity usageModel(ServiceContext context);

    DtoReferenceCollection<R> toReferenceSet(ServiceContext context, Date now);

    Map<String, R> uniqueIndex(ServiceContext context);

    Set<D> toDtoSet(ReferentialLocale referentialLocale, Stream<E> entities);

    List<D> toDtoList(ReferentialLocale referentialLocale, Stream<E> entities);

    List<ToolkitIdLabel> toLabelList(Locale referentialLocale, Stream<E> entities);

    default ToolkitId create(ServiceContext context, String content, Validation<E> consumer) throws InvalidDataException {
        T dao = getDao(context);
        E entity = dao.newInstance();
        return processContent(context, entity, content, consumer);
    }

    default ToolkitId update(ServiceContext context, String id, String content, Validation<E> consumer) throws InvalidDataException {
        T dao = getDao(context);
        E entity = dao.forTopiaIdEquals(id).findUnique();
        return processContent(context, entity, content, consumer);
    }

    default ToolkitId processContent(ServiceContext context, E entity, String content, Validation<E> consumer) throws InvalidDataException {
        Date now = context.now();
        EntityDeserializer deserializer = context.newEntityDeserializer(context.newGsonSupplier(false, false), now);
        boolean notPersisted = entity.isNotPersisted();
        deserializer.deserialize(toEntityType(), entity, content);
        consumer.doValidate(entity);
        return save(context, entity, notPersisted, now);
    }

    default AddEntityToUpdateStep newSaveHelper(ServiceContext context) {
        return PersistenceResultBuilder.create(context.getTopiaPersistenceContext(), context::now);
    }

    default AddEntityToUpdateStep newSaveHelper(ServiceContext context, Date now) {
        return PersistenceResultBuilder.create(context.getTopiaPersistenceContext(), () -> now);
    }

    default SaveResultDto save(ServiceContext context, E entity, boolean notPersisted, Date now) {
        AddEntityToUpdateStep saveHelper = newSaveHelper(context, now);
        if (notPersisted) {
            saveHelper.createReal(this, entity);
        } else {
            saveHelper.update(this, entity);
        }
        return saveHelper.build(entity);
    }

    default R toReference(ReferentialLocale referentialLocale, E entity) {
        return toReference(referentialLocale, entity, null);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    default R toReference(ReferentialLocale referentialLocale, E entity, String classifier) {
        if (entity == null) {
            return null;
        }
        D dto = toDto(referentialLocale, entity);
        R toReference = toReference(referentialLocale, dto);
        if (DecoratorService.WITH_STATS_CLASSIFIER.equals(classifier) && entity instanceof WithStatisticsBuilder<?>) {
            ((WithStatisticsBuilder) entity).buildStatistics((WithStatistics) toReference);
        }
        return toReference;
    }

    default List<String> updateLastUpdateDateField(ToolkitTopiaPersistenceContextSupport persistenceContext, String id, Date timeStamp) {
        List<String> requests = getUpdateLastUpdateDateFieldScript().generate(id, timeStamp);
        String sql = String.join("", requests);
        persistenceContext.executeSqlScript(SqlScriptReader.of(sql));
        return requests;
    }

    default List<String> updateLastUpdateDateTable(ToolkitTopiaPersistenceContextSupport persistenceContext, Date timeStamp) {
        List<String> requests = getUpdateLastUpdateDateTableScript().generate(timeStamp);
        String sql = String.join("", requests);
        persistenceContext.executeSqlScript(SqlScriptReader.of(sql));
        return requests;
    }

    default DtoReferenceCollection<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, String excludeId, Date now, String classifier) {
        return toReferenceSet(referentialLocale, entities.filter(e -> !Objects.equals(excludeId, e.getTopiaId())), now, classifier);
    }

    default DtoReferenceCollection<R> toReferenceSet(ReferentialLocale referentialLocale, Collection<E> entities, Date now, String classifier) {
        return toReferenceSet(referentialLocale, entities.stream(), now, classifier);
    }

    default Set<D> loadEntitiesToDto(ServiceContext context) {
        try (Stream<E> stream = getDao(context).streamAll()) {
            return toDtoSet(context.getReferentialLocale(), stream);
        }
    }

    default Stream<E> loadEntities(ServiceContext context, Predicate<E> predicate) {
        return getDao(context).streamAll().filter(predicate);
    }

    default Stream<E> loadEntities(ServiceContext context, Collection<String> ids) {
        return getDao(context).forTopiaIdIn(ids).stream();
    }

    default Stream<E> loadEntities(TopiaPersistenceContext persistenceContext, Predicate<E> predicate) {
        return getDao(persistenceContext).streamAll().filter(predicate);
    }

    default Stream<E> loadEntities(TopiaPersistenceContext persistenceContext, Collection<String> ids) {
        return getDao(persistenceContext).forTopiaIdIn(ids).stream();
    }

    default R toReference(ReferentialLocale referentialLocale, D dto) {
        throw new IllegalStateException("Cant use this method");
    }

    E loadEntity0(ServiceContext context, ToolkitIdDtoBean id) throws ConcurrentModificationException;

    default D newDto() {
        return Objects2.newInstance(toDtoType());
    }

    default E loadEntity(ServiceContext context, String id) {
        try {
            TopiaDao<E> dao = getDao(context);
            return dao.forTopiaIdEquals(id).findUnique();
        } catch (TopiaNoResultException e) {
            throw new DataNotFoundException(context.getApplicationLocale(), toDtoType(), id);
        }
    }

    default E loadEntity(Locale applicationLocale, TopiaPersistenceContext persistenceContext, String id) {
        try {
            TopiaDao<E> dao = getDao(persistenceContext);
            return dao.forTopiaIdEquals(id).findUnique();
        } catch (TopiaNoResultException e) {
            throw new DataNotFoundException(applicationLocale, toDtoType(), id);
        }
    }

    default E update(TopiaPersistenceContext persistenceContext, E entity) {
        if (entity.isPersisted()) {
            return getDao(persistenceContext).update(entity);
        } else {
            return getDao(persistenceContext).create(entity);
        }
    }

    default E create(TopiaPersistenceContext persistenceContext, E entity) {
        return getDao(persistenceContext).create(entity);
    }

    default void initId(TopiaPersistenceContext persistenceContext, Collection<E> list) {
        T dao = getDao(persistenceContext);
        list.forEach(dao::initId);
    }

    default void initId(ServiceContext context, Collection<E> list) {
        T dao = getDao(context);
        list.forEach(dao::initId);
    }

    default E loadOrCreateEntityFromDto(ServiceContext context, D dto) {
        E entity;
        if (dto.isPersisted()) {
            entity = loadEntity(context, dto.getId());
        } else {
            entity = newEntity(new Date());
        }
        return entity;
    }

    default boolean existsEntity(ServiceContext context, String id) {
        return getDao(context).forTopiaIdEquals(id).exists();
    }

    default void checkEntityExists(ServiceContext context, String id) {
        if (!existsEntity(context, id)) {
            throw new DataNotFoundException(context.getApplicationLocale(), toDtoType(), id);
        }
    }

    default E loadNullableEntity(ServiceContext context, String id) {
        if (id == null) {
            return null;
        }
        return loadEntity(context, id);
    }

    default E loadOrCreateEntity(ServiceContext context, String id) {
        if (id == null) {
            return newEntity(new Date());
        }
        return loadEntity(context, id);
    }

    default E toEntity(R reference) {
        if (reference == null) {
            return null;
        }
        E entity = newEntity(reference.getTopiaCreateDate());
        entity.setTopiaId(reference.getId());
        return entity;
    }

    default E toEntity(D reference) {
        if (reference == null) {
            return null;
        }
        E entity = newEntity(reference.getTopiaCreateDate());
        entity.setTopiaId(reference.getId());
        return entity;
    }

    default void initFilter(ToolkitRequestConfig config, ToolkitRequestFilter filter) {
        if (filter.onId()) {
            filter.setId(ToolkitId.decodeId(filter.getId()));
        } else {
            TreeMap<String, String> properties = filter.getProperties();
            if (properties != null) {
                for (String key : properties.keySet()) {
                    if (key.endsWith("_id")) {
                        String value = properties.get(key);
                        properties.replace(key, ToolkitId.decodeId(value));
                    }
                }
            }
            if (!filter.onOrders()) {
                TreeMap<String, OrderEnum> orders = filter.getOrders();
                TreeMap<String, OrderEnum> order = addNaturalOrder(orders);
                filter.setOrders(order);
            }
        }
        if (config.isCaseSensitive()) {
            filter.setCaseSensitive(true);
        }
    }

    default ToolkitTreeNodeStates loadContent(TopiaApplicationContext<?> applicationContext,
                                              DtoGsonSupplier gsonSupplier,
                                              ToolkitTreeFlatModelContentRequest request,
                                              List<? extends Entity> data) {
        Path nodePath = Path.of(request.getPath());
        EntitySerializerResult result = EntitySerializer.serializeList(request, applicationContext, gsonSupplier, data);
        ToolkitTreeNodeStates userObject = new ToolkitTreeNodeStates();
        userObject.addState(ToolkitTreeNodeBean.STATE_PATH.name(), nodePath.toString());
        userObject.addState(ToolkitTreeNodeBean.STATE_CONTENT.name(), result.getContent());
        if (result.getReferences() != null) {
            userObject.addState(ToolkitTreeNodeBean.STATE_REFERENCES.name(), result.getReferences());
        }
        return userObject;
    }

    default ToolkitTreeFlatModelContentRequest newToolkitTreeFlatModelContentRequest(Supplier<DtoToToolkitTreePathMapping> mapping, ToolkitRequestConfig config, ToolkitRequestFilter filter, Date now) {
        Path typeMapping = mapping.get().get(toDtoType(), filter == null ? null : filter.getGroupBy());
        ToolkitTreeNode node = ToolkitTreeNodeBuilder.build(mapping.get().rootSupplier().get(), typeMapping);
        String requestPath = node.getClass().getName();
        if (filter != null) {
            if (filter.onId()) {
                requestPath += ":" + filter.getId();
            } else if (filter.onProperties()) {
                requestPath += "?" + filter;
            }
        }
        return new ToolkitTreeFlatModelContentRequest(requestPath, now, config);
    }

    default void checkLastUpdateDate(ServiceContext context, Entity entity, BusinessDto dto) throws ConcurrentModificationException {
        checkLastUpdateDate(context.getApplicationLocale(), entity, dto);
    }

    default void checkLastUpdateDate(Locale applicationLocale, Entity entity, BusinessDto dto) throws ConcurrentModificationException {
        if (entity.isPersisted()) {
            Date lastUpdateDate = entity.getLastUpdateDate();
            Date currentUpdateDate = dto.getLastUpdateDate();
            if (lastUpdateDate.after(currentUpdateDate)) {
                throw new ConcurrentModificationException(applicationLocale, entity.getTopiaId(), lastUpdateDate, currentUpdateDate);
            }
        }
    }

    default void checkLastUpdateDate(ServiceContext context, Entity entity, ToolkitIdDtoBean id) throws ConcurrentModificationException {
        checkLastUpdateDate(context.getApplicationLocale(), entity, id);
    }

    default void checkLastUpdateDate(Locale applicationLocale, Entity entity, ToolkitIdDtoBean id) throws ConcurrentModificationException {
        if (entity.isPersisted()) {
            Date lastUpdateDate = entity.getLastUpdateDate();
            Optional<Date> currentUpdateDate = id.getOptionalLastUpdateDate();
            if (currentUpdateDate.isEmpty()) {
                return;
            }
            if (lastUpdateDate.after(currentUpdateDate.get())) {
                throw new ConcurrentModificationException(applicationLocale, entity.getTopiaId(), lastUpdateDate, currentUpdateDate.get());
            }
        }
    }

    default ToolkitTreeNodeStates toStates(ServiceContext context, ToolkitRequestConfig config, ToolkitRequestFilter filter) {
        initFilter(config, filter);
        List<E> data = filter(context, filter);
        ToolkitTreeFlatModelContentRequest request = newToolkitTreeFlatModelContentRequest(context.getNavigationTreePathMapping(), config, filter, context.now());
        log.debug(String.format("Build content [%s].", request.getPath()));
        return loadContent(context.getTopiaApplicationContext(), context.newGsonSupplier(config.isPrettyPrint(), config.isSerializeNulls()), request, data);
    }

    default Timestamp getLastUpdateDate(TopiaPersistenceContext persistenceContext) {
        T dao = getDao(persistenceContext);
        return dao.findSingleResultBySqlQuery(((AbstractTopiaDao<?>)dao).queryGetLastUpdateDate());
    }


    interface Validation<E> {

        void doValidate(E entity) throws InvalidDataException;
    }
}
