package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonArray;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;
import java.util.Date;

/**
 * Created on 30/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
class EntitySerializerVisitor implements TopiaEntityVisitor {
    private final EntitySerializerContext context;

    public EntitySerializerVisitor(EntitySerializerContext context) {
        this.context = context;
    }

    @Override
    public void start(TopiaEntity entity) {
        context.start(entity);
        visit(entity, Entity.PROPERTY_TOPIA_ID, String.class, entity.getTopiaId());
        visit(entity, Entity.PROPERTY_TOPIA_CREATE_DATE, Date.class, entity.getTopiaCreateDate());
        //FIXME Maybe we should add this
//        visit(entity, Entity.PROPERTY_TOPIA_VERSION, Long.class, entity.getTopiaVersion());
    }

    @Override
    public void end(TopiaEntity entity) {
        context.end();
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
        if (value == null) {
            if (context.isSerializeNulls()) {
                context.visitNull(propertyName);
            }
            return;
        }
        if (ReferentialEntity.class.isAssignableFrom(type)) {
//            if (context.isRecurse() || context.isLoadReferential()) {
            ReferentialEntity data = (ReferentialEntity) value;
            context.visitReferential(propertyName, data);
//            }
            return;
        }
        if (DataEntity.class.isAssignableFrom(type)) {
//            if (context.isRecurse()) {
            DataEntity data = (DataEntity) value;
            context.visitData(propertyName, data);
//            }
            return;
        }
        context.visit(propertyName, type, value);
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
        boolean isRef = ReferentialEntity.class.isAssignableFrom(type);
        boolean isData = DataEntity.class.isAssignableFrom(type);
//        if (!context.isRecurse() || isRef && !context.isLoadReferential()) {
//            return;
//        }
        Collection<?> collection = (Collection<?>) value;
        if (value == null) {
            if (context.isSerializeNulls()) {
                context.visitNullCollection(propertyName);
            }
            return;
        }
        JsonArray array = new JsonArray(collection.size());
        if (isRef) {
            context.visitReferentialCollection(propertyName, collection, array);
            return;
        }
        if (isData) {
            context.visitDataCollection(propertyName, collection, array);
            return;
        }
        context.visitCollection(propertyName, collection, array);
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
    }

    @Override
    public void clear() {
    }
}
