package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DuplicateRequest;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.I18nReferentialDto;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.referential.I18nReferentialEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.spi.referential.ReferentialExtraScripts;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.spi.usage.UsageHelper;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.service.sql.script.DeleteReferentialScript;
import org.nuiton.topia.service.sql.script.DisableReferentialScript;
import org.nuiton.topia.service.sql.script.ReplaceReferentialInReferentialScript;
import org.nuiton.topia.service.sql.script.ReplaceReferentialInDataScript;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScript;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 04/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ReferentialDtoEntityContext<
        D extends ReferentialDto,
        R extends ReferentialDtoReference,
        E extends ReferentialEntity,
        T extends TopiaDao<E>> extends DtoEntityContextSupport<D, R, E, T> {

    private final SingletonSupplier<ReferentialExtraScripts<E>> extraScripts;

    protected abstract TopiaEntitySqlScript loadSqlScript();


    protected abstract ReferentialExtraScripts<E> loadExtraScripts();

    public ReferentialDtoEntityContext() {
        this.extraScripts = SingletonSupplier.of(this::loadExtraScripts);
    }

    @Override
    public Form<D> loadForm(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        return referentialEntityToForm(context.getReferentialLocale(), entity);
    }

    public SaveResultDto saveEntity(ServiceContext context, E entity, D dto) {
        return saveEntity(context, entity);
    }

    public Optional<ReplaceReferentialInDataScript> getReplaceInDataScript() {
        return getSqlScript().getReplaceReferentialInDataScript();
    }

    public Optional<ReplaceReferentialInReferentialScript> getReplaceInReferentialScript() {
        return getSqlScript().getReplaceReferentialInReferentialScript();
    }

    public DeleteReferentialScript getDeleteScript() {
        return getSqlScript().getDeleteReferentialScript().orElseThrow();
    }

    public DisableReferentialScript getDisableScript() {
        return getSqlScript().getDisableReferentialScript().orElseThrow();
    }

    public ReferentialExtraScripts<E> getExtraScripts() {
        return extraScripts.get();
    }

    @Override
    public void fromDto(ReferentialLocale referentialLocale, E entity, D dto) {
        super.fromDto(referentialLocale, entity, dto);
        entity.setStatus(dto.getStatus());
        entity.setNeedComment(dto.isNeedComment());
        entity.setCode(dto.getCode());
        entity.setUri(dto.getUri());
        entity.setHomeId(dto.getHomeId());
        if (entity instanceof I18nReferentialEntity) {
            ((I18nReferentialEntity) entity).setLabel1(((I18nReferentialDto) dto).getLabel1());
            ((I18nReferentialEntity) entity).setLabel2(((I18nReferentialDto) dto).getLabel2());
            ((I18nReferentialEntity) entity).setLabel3(((I18nReferentialDto) dto).getLabel3());
            ((I18nReferentialEntity) entity).setLabel4(((I18nReferentialDto) dto).getLabel4());
            ((I18nReferentialEntity) entity).setLabel5(((I18nReferentialDto) dto).getLabel5());
            ((I18nReferentialEntity) entity).setLabel6(((I18nReferentialDto) dto).getLabel6());
            ((I18nReferentialEntity) entity).setLabel7(((I18nReferentialDto) dto).getLabel7());
            ((I18nReferentialEntity) entity).setLabel8(((I18nReferentialDto) dto).getLabel8());
        }
    }

    @Override
    public void toDto(ReferentialLocale referentialLocale, E entity, D dto) {
        super.toDto(referentialLocale, entity, dto);
        dto.setStatus(entity.getStatus());
        dto.setNeedComment(entity.isNeedComment());
        dto.setCode(entity.getCode());
        dto.setUri(entity.getUri());
        dto.setHomeId(entity.getHomeId());
        if (dto instanceof I18nReferentialDto) {
            ((I18nReferentialDto) dto).setLabel1(((I18nReferentialEntity) entity).getLabel1());
            ((I18nReferentialDto) dto).setLabel2(((I18nReferentialEntity) entity).getLabel2());
            ((I18nReferentialDto) dto).setLabel3(((I18nReferentialEntity) entity).getLabel3());
            ((I18nReferentialDto) dto).setLabel4(((I18nReferentialEntity) entity).getLabel4());
            ((I18nReferentialDto) dto).setLabel5(((I18nReferentialEntity) entity).getLabel5());
            ((I18nReferentialDto) dto).setLabel6(((I18nReferentialEntity) entity).getLabel6());
            ((I18nReferentialDto) dto).setLabel7(((I18nReferentialEntity) entity).getLabel7());
            ((I18nReferentialDto) dto).setLabel8(((I18nReferentialEntity) entity).getLabel8());
        }
    }

    public final ReferentialCache<R, E> newCache(ServiceContext context) {
        Map<String, R> data = toReferenceSet(context, null).toId();
        DecoratorService decoratorService = context.getDecoratorService();
        Decorator decorator = decoratorService.getDecoratorByType(toReferenceType());
        data.values().forEach(r -> r.registerDecorator(decorator));
        return new ReferentialCache<>(this, data, decorator, decoratorService.getReferentialLocale());
    }

    @Override
    public final ReferentialDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Collection<E> entities, Date now, String classifier) {
        return (ReferentialDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities, now, classifier);
    }

    @Override
    public final ReferentialDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, Date now, String classifier) {
        return (ReferentialDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities, now, classifier);
    }

    @Override
    public final ReferentialDtoReferenceSet<R> toReferenceSet(ServiceContext context, Date now) {
        return (ReferentialDtoReferenceSet<R>) super.toReferenceSet(context, now);
    }

    @Override
    public final ReferentialDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, String excludeId, Date now, String classifier) {
        return (ReferentialDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities, excludeId, now, classifier);
    }

    public final UsageHelper<? super Entity> newUsageHelper(ServiceContext context) {
        return new UsageHelper<>(() -> usageModel(context), context.getTopiaPersistenceContext(), true, true, false, true, false, false);
    }

    @Override
    protected final ReferentialDtoReferenceSet<R> createReferenceSet(ReferentialLocale referentialLocale, Collection<R> references, Date now) {
        return ReferentialDtoReferenceSet.of(toReferenceType(), references, now);
    }

    public final LinkedHashSet<E> toReferentialEntitySet(Collection<R> references) {
        LinkedHashSet<E> entityList = null;
        if (references != null && !references.isEmpty()) {
            entityList = new LinkedHashSet<>(references.size());
            for (R reference : references) {
                E entity = toEntity(reference);
                entityList.add(entity);
            }
        }
        return entityList;
    }

    public final List<R> toReferentialReferenceList(ReferentialLocale referentialLocale, Collection<E> entities) {
        List<R> references = null;
        if (entities != null && !entities.isEmpty()) {
            references = new ArrayList<>(entities.size());
            for (E entity : entities) {
                R reference = toReference(referentialLocale, entity);
                references.add(reference);
            }
        }
        return references;
    }

    public final Form<D> referentialEntityToForm(ReferentialLocale referentialLocale, E entity) {
        D dto = toDto(referentialLocale, entity);
        return Form.newFormDto(toDtoType(), dto);
    }

    public final Form<D> preCreate(ReferentialLocale referentialLocale) {
        E entity = newEntity((String) null);
        entity.setStatus(ReferenceStatus.enabled);
        return referentialEntityToForm(referentialLocale, entity);
    }

    public final void delete(ServiceContext context, String id) {
        checkEntityExists(context, id);
        log.info(String.format("will delete entity: %s", id));
        SqlScript deleteScript = generateDeleteScript(id);
        context.getTopiaPersistenceContext().executeSqlScript(deleteScript);
        updateLastUpdateDateTable(context.getTopiaPersistenceContext(), context.now());
    }

    public final Optional<Date> getLastUpdate(ToolkitTopiaPersistenceContextSupport persistenceContext) {
        try {
            Date lastUpdateDate = persistenceContext.getLastUpdateDate(toEntityType());
            return Optional.ofNullable(lastUpdateDate);
        } catch (Exception e) {
            throw new TopiaException(String.format("Can't find last update date for type: %s", toEntityType().getName()));
        }
    }

    public final void replaceReference(ServiceContext context, String idToReplace, String replaceId, boolean replaceInReferential, boolean replaceInData) {
        if (!replaceInReferential && !replaceInData) {
            throw new IllegalStateException("Need at least one of the parameters *replaceInReferential* or *replaceInData* to be on.");
        }
        checkEntityExists(context, idToReplace);
        checkEntityExists(context, replaceId);
        Optional<SqlScript> sqlScript = generateReplaceScript(idToReplace, replaceId, context.now(), replaceInReferential, replaceInData);
        sqlScript.ifPresent(s -> context.getTopiaPersistenceContext().executeSqlScript(s));
    }

    public final void changeId(ServiceContext context, String id, String newId) {
        checkEntityExists(context, id);
        log.info(String.format("will change id from %s to %s", id, newId));

        SqlScript duplicateScript = generateDuplicateScript(context, id, newId);
        Optional<SqlScript> replaceScript = generateReplaceScript(id, newId, context.now(), true, true);
        SqlScript deleteScript = generateDeleteScript(id);

        context.getTopiaPersistenceContext().executeSqlScript(duplicateScript);
        replaceScript.ifPresent(s -> context.getTopiaPersistenceContext().executeSqlScript(s));
        context.getTopiaPersistenceContext().executeSqlScript(deleteScript);
        updateLastUpdateDateTable(context.getTopiaPersistenceContext(), context.now());
//        D dto = loadEntityToDto(context, id);
//        // copy it to a new entity with newId
//        dto.setId(newId);
//        E entity = toEntity(context.getReferentialLocale(), dto);
//        saveEntity(context, entity);
//        // replace id
//        replaceReference(context, id, newId);
//        // delete old referential
//        delete(context, id);
    }

    public final SaveResultDto save(ServiceContext context, D dto) throws ConcurrentModificationException {
        E entity = loadOrCreateEntityFromReferentialDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        fromDto(context.getReferentialLocale(), entity, dto);
        return saveEntity(context, entity, dto);
    }

    private SqlScript generateDuplicateScript(ServiceContext context, String id, String newId) {
        return consume(context, new DuplicateRequest(context.isPostgresDatabase(), toEntityType().getName(), id, newId));
    }

    public Optional<SqlScript> generateReplaceScript(String id, String newId, Date now, boolean replaceInReferential, boolean replaceInData) {
        List<String> result = new LinkedList<>();
        if (replaceInReferential) {
            getReplaceInReferentialScript().ifPresent(script -> {
                List<String> sql = script.generate(id, newId, now);
                result.addAll(sql);

            });
        }
        if (replaceInData) {
            getReplaceInDataScript().ifPresent(script -> {
                List<String> sql = script.generate(id, newId, now);
                result.addAll(sql);

            });
        }

        if (result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(SqlScript.of(String.join("\n", result)));
    }

    private SqlScript generateDeleteScript(String id) {
        List<String> sql = getDeleteScript().generate(id);
        return SqlScript.of(String.join("\n", sql));
    }

    public final ReferentialDtoReferenceSet<R> getReferenceSet0(ServiceContext context, Date lastUpdateDate) {
        //FIXME A revoir car on devrait toujours avoir une date de dernière mise à jour
        ToolkitTopiaPersistenceContextSupport persistenceContext = context.getTopiaPersistenceContext();
        Optional<Date> lastUpdateOptional = getLastUpdate(persistenceContext);
        ReferentialDtoReferenceSet<R> result = null;
        if (lastUpdateOptional.isPresent() && (lastUpdateDate == null || lastUpdateOptional.get().after(lastUpdateDate))) {
            List<E> entities = persistenceContext.getDao(toEntityType()).findAll();
            result = toReferenceSet(context.getReferentialLocale(), entities, lastUpdateOptional.get(), null);
        }
        return result;
    }

    public final Set<D> loadDtoList(ServiceContext context) {
        List<D> result = new LinkedList<>(loadEntitiesToDto(context));
        Decorator decorator = DecoratorProvider.get().decorator(context.getReferentialLocale().getLocale(), toDtoType());
        result.forEach(r -> r.registerDecorator(decorator));
        result.sort(Comparator.comparing(ReferentialDto::toString));
        return new LinkedHashSet<>(result);
    }

    public final Set<ToolkitIdLabel> getEnabledReferentialLabelSet(ServiceContext context) {
        Stream<E> stream = loadEntities(context.getTopiaPersistenceContext(), ReferentialEntity::isEnabled);
        Decorator decorator = DecoratorProvider.get().decorator(context.getReferentialLocale().getLocale(), toEntityType());
        return stream.map(e -> {
            e.registerDecorator(decorator);
            return e.toLabel();
        }).sorted(Comparator.comparing(ToolkitIdLabel::toString)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public final R toReference(ServiceContext context, String id) {
        E entity = loadOrCreateEntity(context, id);
        return toReference(context.getReferentialLocale(), entity);
    }

    public final SaveResultDto saveEntity(ServiceContext context, E entity) {
        return newSaveHelper(context).update(this, entity).build(entity);
    }

    public final UsageCount count(ServiceContext context, ToolkitIdDtoBean request) {
        ReferentialEntity entity = loadEntity(context, request.getId());
        return newUsageHelper(context).count(entity);
    }

    public final UsageCount count(ServiceContext context, ToolkitIdDtoBean request, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        ReferentialEntity entity = loadEntity(context, request.getId());
        return newUsageHelper(context).count(entity, optionalPredicate);
    }

    public final <EE extends Entity> Set<ToolkitIdLabel> findUsages(ServiceContext context, ToolkitIdDtoBean request, DtoEntityContext<?, ?, EE, ?> spi2) {
        E entity = loadEntity(context, request.getId());
        Collection<EE> entities = newUsageHelper(context).find(entity, spi2.toEntityType());
        context.getDecoratorService().installDecorator(spi2.toEntityType(), entities.stream());
        return entities.stream().map(Entity::toLabel).collect(Collectors.toSet());
    }

    public final Set<ToolkitIdLabel> getReferentialToDelete(ServiceContext context, Set<String> ids) {
        List<E> entities = loadEntities(context.getTopiaPersistenceContext(), ids).collect(Collectors.toList());
        context.getDecoratorService().installDecorator(toEntityType(), entities.stream());
        return entities.stream().map(ReferentialEntity::toLabel).collect(Collectors.toSet());
    }

    private E loadOrCreateEntityFromReferentialDto(ServiceContext context, D dto) {
        E entity;
        if (dto.isPersisted()) {
            entity = loadEntity(context, dto.getId());
        } else {
            entity = newEntity();
        }
        return entity;
    }
}
