package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByValue;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.I18nReferentialHelper;
import fr.ird.observe.dto.referential.ReferenceStatus;
import fr.ird.observe.dto.referential.ReferentialDtoReferenceWithCodeAware;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.WithI18n;
import fr.ird.observe.entities.referential.I18nReferentialEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.tree.TreeConfig;
import io.ultreia.java4all.decoration.Decorator;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nuiton.topia.persistence.TopiaEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 13/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ReferentialCache<R extends ReferentialDtoReference, E extends ReferentialEntity> {

    private final ReferentialDtoEntityContext<?, R, E, ?> spi;
    private final Map<String, R> references;
    private final Decorator decorator;
    private final ReferentialLocale referentialLocale;

    public ReferentialCache(ReferentialDtoEntityContext<?, R, E, ?> spi, Map<String, R> references, Decorator decorator, ReferentialLocale referentialLocale) {
        this.spi = spi;
        this.references = references;
        this.decorator = decorator;
        this.referentialLocale = referentialLocale;
    }

    public R reference(String id) {
        return references.get(id);
    }

    public R reference(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return references.get(resultSet.getString(rowIndex.getAndIncrement()));
    }

    public E entity(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        R reference = reference(resultSet.getString(rowIndex.getAndIncrement()));
        E entity = null;
        if (reference != null) {
            entity = spi.toEntity(reference);
            if (reference instanceof WithI18n) {
                I18nReferentialHelper.setLabel(referentialLocale.ordinal(), (I18nReferentialEntity) entity, reference.get("label"));
            }
        }
        return entity;
    }

    public void decorate(List<DataGroupByValue> groupCountValues) {
        decorateFromReference(groupCountValues, Object::toString);
    }

    public void decorateFromReference(List<DataGroupByValue> groupCountValues, Function<R, String> textFunction) {
        for (DataGroupByValue groupCountValue : groupCountValues) {
            R reference = reference(groupCountValue.getValue());
            String text;
            boolean enabled;
            if (reference == null) {
                // just send the i18n key
                text = TreeConfig.NULL_GROUP_BY_VALUE_I18N_LABEL;
                enabled = true;
            } else {
                text = textFunction.apply(reference);
                enabled = reference.isEnabled();
            }
            groupCountValue.setText(text);
            groupCountValue.setEnabled(enabled);
        }
    }

    public Class<E> entityType() {
        return spi.toEntityType();
    }

    public <X extends TopiaEntity> void decorate(List<X> groupByValues, Function<X, E> get) {
        decorate(groupByValues, get, (x, r) -> r.toString());
    }

    public <X extends TopiaEntity> void decorate(List<X> groupByValues, Function<X, E> get, BiFunction<X, R, String> textFunction) {
        for (X groupByValue : groupByValues) {
            E referential = get.apply(groupByValue);
            if (referential != null) {
                R reference = reference(referential.getTopiaId());
                String text;
                boolean enabled;
                if (reference == null) {
                    // just send the i18n key
                    text = TreeConfig.NULL_GROUP_BY_VALUE_I18N_LABEL;
                    enabled = true;
                } else {
                    text = textFunction.apply(groupByValue, reference);
                    enabled = reference.isEnabled();
                }
                I18nReferentialHelper.setLabel(referentialLocale.ordinal(), ((WithI18n) referential), text);
                referential.setStatus(enabled ? ReferenceStatus.enabled : ReferenceStatus.disabled);
                //FIXME Maybe we need more stuff?
                if (reference instanceof ReferentialDtoReferenceWithCodeAware) {
                    String code = ((ReferentialDtoReferenceWithCodeAware) reference).getCode();
                    referential.setCode(code);
                }
            }
        }
    }

    public void sort(List<DataGroupByValue> list) {
        // get data indexed by id
        Map<String, DataGroupByValue> mapById = list.stream().collect(Collectors.toMap(DataGroupByValue::getValue, Function.identity()));
        // get used ids
        Set<String> usedIds = mapById.keySet();
        // get references used
        List<R> usedReferences = references.values().stream().filter(r -> usedIds.contains(r.getId())).collect(Collectors.toList());

        // sort references
        //FIXME Make sure position is always 0, should introduce in model a new tag value to define this default value
        //FIXME In this way could use it also in UI without any hard coded values
        decorator.sort(usedReferences, 0);
        // obtain used ids in correct order
        List<String> idsInOrder = usedReferences.stream().map(ReferentialDtoReference::getId).collect(Collectors.toList());
        // push back data using the id order
        list.clear();
        for (String id : idsInOrder) {
            list.add(mapById.get(id));
        }
        // manage optional data null value (value is id!)
        Optional<DataGroupByValue> nullDataGroupByValue = Optional.ofNullable(mapById.get(null));
        nullDataGroupByValue.ifPresent(list::add);
    }
}
