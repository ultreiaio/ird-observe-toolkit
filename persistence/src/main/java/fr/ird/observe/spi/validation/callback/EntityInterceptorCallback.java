package fr.ird.observe.spi.validation.callback;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Deque;
import java.util.List;
import java.util.Objects;

/**
 * To perform extra actions on interception.
 * <p>
 * Created at 21/05/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see fr.ird.observe.spi.validation.EntityInterceptor
 * @since 9.3.5
 */
public interface EntityInterceptorCallback<D extends BusinessDto, E extends Entity, S extends DtoEntityContext<D, ?, E, ?>> {

    static <E extends Entity, P extends Entity> P getParent(Deque<Entity> path, E e, Class<P> parentType) {
        List<Entity> copy = List.copyOf(path);
        int parentPosition = copy.indexOf(e) - 1;
        Entity parent = copy.get(parentPosition);
        while (!parentType.isAssignableFrom(parent.contractType())) {
            parent = copy.get(--parentPosition);
        }
        return parentType.cast(Objects.requireNonNull(parent, String.format("Could not find parent of type: %s for entity: %s on path: %s", parentType.getName(), e, path)));
    }

    boolean acceptType(Class<? extends BusinessDto> dtoType, Class<? extends Entity> entityType);

    void loadDtoForValidation(ServiceContext context, Deque<Entity> path, S spi, E e, D dto);
}
