package fr.ird.observe.spi.result;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.spi.context.DtoEntityContext;

import java.util.Date;
import java.util.Objects;

/**
 * FIXME Reuse the idea of TopiaContext on entity but with more more powerful stuff.
 * Created on 13/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.10
 */
public class EntityContext<E extends Entity> {

    private final DtoEntityContext<?, ?, E, ?> spi;
    private final E entity;

    public static <E extends Entity> EntityContext<E> of(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        return new EntityContext<>(spi, entity);
    }

    private EntityContext(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        this.spi = Objects.requireNonNull(spi);
        this.entity = Objects.requireNonNull(entity);
    }

    public DtoEntityContext<?, ?, E, ?> getSpi() {
        return spi;
    }

    public E getEntity() {
        return entity;
    }

    public void create(ToolkitTopiaPersistenceContextSupport persistenceContext) {
        spi.create(persistenceContext, entity);
    }

    public void update(ToolkitTopiaPersistenceContextSupport persistenceContext) {
        spi.update(persistenceContext, entity);
    }

    public void updateLastUpdateField(ToolkitTopiaPersistenceContextSupport persistenceContext, Date lastUpdateDate) {
        spi.updateLastUpdateDateField(persistenceContext, entity.getTopiaId(), lastUpdateDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EntityContext)) return false;
        EntityContext<?> that = (EntityContext<?>) o;
        return entity.equals(that.entity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entity);
    }
}
