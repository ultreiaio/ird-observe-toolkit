package fr.ird.observe.spi.result;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.context.DtoEntityContext;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Helps to create result after a persistence operation.
 * <p>
 * Created on 13/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.10
 */
public class PersistenceResultBuilder implements AddEntityToUpdateStep, AddUpdateLastUpdateDateFieldStep, AddUpdateLastUpdateDateTableStep, BuildStep {
    private final List<EntityContext<?>> updates;
    private final List<EntityContext<?>> creates;
    private final List<EntityContext<?>> createsReal;
    private final List<EntityContext<?>> lastUpdateDateFields;
    private final List<DtoEntityContext<?, ?, ?, ?>> lastUpdateDateTables;
    private final ToolkitTopiaPersistenceContextSupport persistenceContext;
    private final Supplier<Date> lastUpdateDateSupplier;

    public static AddEntityToUpdateStep create(ToolkitTopiaPersistenceContextSupport persistenceContext, Supplier<Date> lastUpdateDateSupplier) {
        return new PersistenceResultBuilder(persistenceContext, lastUpdateDateSupplier);
    }

    public PersistenceResultBuilder(ToolkitTopiaPersistenceContextSupport persistenceContext, Supplier<Date> lastUpdateDateSupplier) {
        this.persistenceContext = Objects.requireNonNull(persistenceContext);
        this.lastUpdateDateSupplier = Objects.requireNonNull(lastUpdateDateSupplier);
        this.updates = new LinkedList<>();
        this.creates = new LinkedList<>();
        this.createsReal = new LinkedList<>();
        this.lastUpdateDateFields = new LinkedList<>();
        this.lastUpdateDateTables = new LinkedList<>();
    }

    @Override
    public ToolkitTopiaPersistenceContextSupport persistenceContext() {
        return persistenceContext;
    }

    @Override
    public <E extends Entity> AddEntityToUpdateStep create(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        creates.add(EntityContext.of(spi, entity));
        return this;
    }

    @Override
    public <E extends Entity> AddEntityToUpdateStep createReal(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        createsReal.add(EntityContext.of(spi, entity));
        return this;
    }

    @Override
    public <E extends Entity> AddEntityToUpdateStep update(DtoEntityContext<?, ?, E, ?> spi, E entity, boolean updateLastUpdateDateTable) {
        EntityContext<E> context = EntityContext.of(spi, entity);
        updates.add(context);
        if (!lastUpdateDateFields.contains(context)) {
            lastUpdateDateFields.add(context);
        }
        if (updateLastUpdateDateTable) {
            updateLastUpdateDateTable(spi);
        }
        return this;
    }

    @Override
    public <E extends Entity> AddUpdateLastUpdateDateFieldStep updateLastUpdateDateField(DtoEntityContext<?, ?, E, ?> spi, E entity) {
        EntityContext<E> context = EntityContext.of(spi, entity);
        if (!lastUpdateDateFields.contains(context)) {
            lastUpdateDateFields.add(context);
        }
        return this;
    }

    @Override
    public AddUpdateLastUpdateDateTableStep updateLastUpdateDateTable(DtoEntityContext<?, ?, ?, ?> spi) {
        if (!lastUpdateDateTables.contains(spi)) {
            lastUpdateDateTables.add(spi);
        }
        return this;
    }

    @Override
    public Date build() {
        Date lastUpdateDate = lastUpdateDateSupplier.get();
        for (EntityContext<?> entityContext : creates) {
            Entity entity = entityContext.getEntity();
            entity.setLastUpdateDate(lastUpdateDate);
        }
        for (EntityContext<?> entityContext : createsReal) {
//            Entity entity = entityContext.getEntity();
//            entity.setLastUpdateDate(lastUpdateDate);
            entityContext.create(persistenceContext);
        }
        for (EntityContext<?> entityContext : updates) {
            Entity entity = entityContext.getEntity();
            if (entity.isNotPersisted()) {
                entity.setLastUpdateDate(lastUpdateDate);
            }
            entityContext.update(persistenceContext);
        }
        persistenceContext.flush();
        for (EntityContext<?> spi : lastUpdateDateFields) {
            spi.updateLastUpdateField(persistenceContext, lastUpdateDate);
        }
        for (DtoEntityContext<?, ?, ?, ?> spi : lastUpdateDateTables) {
            spi.updateLastUpdateDateTable(persistenceContext, lastUpdateDate);
        }
        return lastUpdateDate;
    }

    @Override
    public SaveResultDto build(Entity entity) {
        Date lastUpdateDate = build();
        SaveResultDto result = new SaveResultDto();
        result.copyTechnicalFields(entity, lastUpdateDate);
        return result;
    }

}
