package fr.ird.observe.spi.service;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataGroupByEntity;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.context.ContainerDtoEntityContext;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.EditableDtoEntityContext;
import fr.ird.observe.spi.context.OpenableDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;
import fr.ird.observe.spi.context.SimpleDtoEntityContext;
import fr.ird.observe.spi.io.EntityDeserializer;
import fr.ird.observe.spi.json.DtoGsonSupplier;
import fr.ird.observe.spi.navigation.tree.DtoToToolkitTreePathMapping;
import fr.ird.observe.spi.navigation.tree.TreeBuilderSupport;
import fr.ird.observe.validation.ValidationContextSupport;
import fr.ird.observe.validation.api.request.ValidationRequestConfigurationSupport;
import fr.ird.observe.validation.api.request.ValidationRequestSupport;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaDao;

import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.function.Supplier;

/**
 * Created on 16/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public interface ServiceContext {

    ReferentialLocale getReferentialLocale();

    Path getTemporaryDirectoryRoot();

    DecoratorService getDecoratorService();

    Locale getApplicationLocale();

    DataSourceValidationMode getValidationMode();

    ToolkitTopiaPersistenceContextSupport getTopiaPersistenceContext();

    TopiaApplicationContext<?> getTopiaApplicationContext();

    Date now();

     Timestamp timestampNow();

    boolean isPostgresDatabase();

    <T> Future<T> submit(Callable<T> task);

    DtoGsonSupplier newGsonSupplier(boolean prettyPrint, boolean serializeNulls);

    EntityDeserializer newEntityDeserializer(Supplier<Gson> gsonSupplier, Date now);

    TreeBuilderSupport newTreeBuilder();

    void checkCredentials(String methodName, Permission methodeCredentials);

    Supplier<DtoToToolkitTreePathMapping> getNavigationTreePathMapping();

    <D extends DataDto,
            R extends DtoReference,
            E extends Entity,
            T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromEntity(E dtoType);

    <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(Class<D> dtoType);

    <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataReference(Class<R> dtoType);

    <D extends DataDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(ToolkitIdDtoBean dtoType);

    <PE extends Entity,
            D extends OpenableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> OpenableDtoEntityContext<PE, D, R, E, T> fromOpenableDto(Class<D> dtoType);

    <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>> RootOpenableDtoEntityContext<D, R, E, T, H> fromRootOpenableDto(Class<D> dtoType);

    <PE extends Entity,
            D extends EditableDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> EditableDtoEntityContext<PE, D, R, E, T> fromEditableDto(Class<D> dtoType);

    <C extends ContainerChildDto,
            D extends ContainerDto<C>,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> ContainerDtoEntityContext<D, R, E, T> fromContainerDto(Class<D> dtoType);

    <D extends SimpleDto,
            R extends DataDtoReference,
            E extends DataEntity,
            T extends TopiaDao<E>> SimpleDtoEntityContext<D, R, E, T> fromSimpleDto(Class<D> dtoType);

    <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialEntity(Class<E> dtoType);

    <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(ToolkitIdDtoBean dtoType);

    <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(Class<D> dtoType);

    <D extends ReferentialDto,
            R extends ReferentialDtoReference,
            E extends ReferentialEntity,
            T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialReference(Class<R> dtoType);

    <D extends RootOpenableDto,
            R extends DataDtoReference,
            E extends RootOpenableEntity,
            T extends TopiaDao<E>,
            H extends DataGroupByReferentialHelper<D, R, E>,
            F extends DataGroupByEntity<E>,
            FF extends DataGroupByDto<D>> GroupBySpiContext<D, R, E, T, H, F, FF> fromGroupByName(String parentProperty);

    ObserveDataSourceInformation getDataSourceInformation();

    ValidationContextSupport createServiceValidationContext(ValidationRequestConfigurationSupport configuration, ValidationRequestSupport request);

}
