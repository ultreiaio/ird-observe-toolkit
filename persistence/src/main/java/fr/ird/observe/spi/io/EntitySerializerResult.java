package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 02/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.21
 */
public class EntitySerializerResult {

    private final String content;
    private final String references;

    public EntitySerializerResult(String content, String references) {
        this.content = content;
        this.references = references;
    }

    public String getContent() {
        return content;
    }

    public String getReferences() {
        return references;
    }
}
