package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.SqlServiceRequest;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.DataNotFoundException;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.decoration.DecoratorProvider;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.persistence.filter.ToolkitRequestFilter;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlDescriptor;
import org.nuiton.topia.service.sql.script.TopiaEntitySqlScript;
import org.nuiton.topia.service.sql.script.UpdateLastUpdateDateFieldScript;
import org.nuiton.topia.service.sql.script.UpdateLastUpdateDateTableScript;
import org.nuiton.topia.service.sql.usage.TopiaUsageEntity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 21/09/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public abstract class DtoEntityContextSupport<
        D extends BusinessDto,
        R extends DtoReference,
        E extends Entity,
        T extends TopiaDao<E>> implements DtoEntityContext<D, R, E, T> {

    private final SingletonSupplier<TopiaEntitySqlScript> sqlScript;
    private transient TopiaEntitySqlDescriptor entitySqlDescriptor;
    private transient String definitionContent;
    private transient String validationCreateContent;
    private transient String validationUpdateContent;
    private transient TopiaUsageEntity usageModel;

    protected DtoEntityContextSupport() {
        sqlScript = SingletonSupplier.of(this::loadSqlScript);
    }

    protected abstract TopiaEntitySqlScript loadSqlScript();

    protected abstract TopiaEntitySqlDescriptor loadEntitySqlDescriptor();

    protected abstract DtoReferenceCollection<R> createReferenceSet(ReferentialLocale referentialLocale, Collection<R> references, Date now);

    public void fromDto(ReferentialLocale referentialLocale, E entity, D dto) {
        entity.setTopiaId(dto.getId());
        entity.setTopiaVersion(dto.getTopiaVersion());
        entity.setTopiaCreateDate(dto.getTopiaCreateDate());
        Date lastUpdateDate = dto.getLastUpdateDate();
        if (lastUpdateDate == null) {
            lastUpdateDate = new Date();
        }
        entity.setLastUpdateDate(lastUpdateDate);
    }

    public void toDto(ReferentialLocale referentialLocale, E entity, D dto) {
        dto.setId(entity.getTopiaId());
        dto.setTopiaVersion(entity.getTopiaVersion());
        dto.setTopiaCreateDate(entity.getTopiaCreateDate());
        dto.setLastUpdateDate(entity.getLastUpdateDate());
    }

    @Override
    public DtoReferenceCollection<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, Date now, String classifier) {
        Set<R> references = new LinkedHashSet<>();
        entities.forEach(entity -> {
            R reference = toReference(referentialLocale, entity, classifier);
            references.add(reference);
        });
        return createReferenceSet(referentialLocale, references, now);
    }

    @Override
    public TopiaUsageEntity usageModel(ServiceContext context) {
        return usageModel == null ? usageModel = context.getTopiaApplicationContext().getUsageModel().getMapping(toEntityType().getName()) : usageModel;
    }

    @Override
    public DtoReferenceCollection<R> toReferenceSet(ServiceContext context, Date now) {
        ReferentialLocale referentialLocale = context.getReferentialLocale();
        Set<R> references = new LinkedHashSet<>();
        try (Stream<E> stream = getDao(context).streamAll()) {
            stream.forEach(entity -> {
                R reference = toReference(referentialLocale, entity);
                references.add(reference);
            });
        }
        return createReferenceSet(referentialLocale, references, now);
    }

    public final void fromDto(ServiceContext context, E entity, D dto) {
        fromDto(context.getReferentialLocale(), entity, dto);
    }

    public final void toDto(ServiceContext context, E entity, D dto) {
        toDto(context.getReferentialLocale(), entity, dto);
    }

    @Override
    public final UpdateLastUpdateDateFieldScript getUpdateLastUpdateDateFieldScript() {
        return getSqlScript().getLastUpdateDateFieldScript();
    }

    @Override
    public final UpdateLastUpdateDateTableScript getUpdateLastUpdateDateTableScript() {
        return getSqlScript().getLastUpdateDateTableScript();
    }

    @Override
    public final TopiaEntitySqlDescriptor getEntitySqlDescriptor() {
        return entitySqlDescriptor == null ? entitySqlDescriptor = loadEntitySqlDescriptor() : entitySqlDescriptor;
    }

    @Override
    public final T getDao(ServiceContext context) {
        return getDao(context.getTopiaPersistenceContext());
    }

    @Override
    public final List<E> filter(ServiceContext context, ToolkitRequestFilter filter) {
        T dao = getDao(context);
        if (filter == null) {
            return dao.findAll();
        }
        return dao.filter(newFilterConsumer(context.getReferentialLocale()), filter, dao.newQueryBuilder());
    }

    @Override
    public final E loadEntity0(ServiceContext context, ToolkitIdDtoBean id) throws ConcurrentModificationException {
        log.debug(String.format("Load entity with id: %s", id));
        try {
            E entity = loadEntity(context, id.getId());
            checkLastUpdateDate(context, entity, id);
            return entity;
        } catch (TopiaNoResultException e) {
            throw new DataNotFoundException(context.getApplicationLocale(), id.getId());
        }
    }

    @Override
    public final R loadEntityToReferenceDto(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        return toReference(context.getReferentialLocale(), entity);
    }

    @Override
    public final Form<D> entityToForm(ReferentialLocale referentialLocale, E entity) {
        D dto = toDto(referentialLocale, entity);
        return Form.newFormDto(toDtoType(), dto);
    }

    @Override
    public final D loadEntityToDto(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        return toDto(context.getReferentialLocale(), entity);
    }

    @Override
    public final E newEntity(Date now) {
        E result = newEntity();
        result.setTopiaCreateDate(now);
        return result;
    }

    @Override
    public final E newEntity(String id) {
        E result = newEntity();
        result.setTopiaId(id);
        return result;
    }

    @Override
    public final E newEntity(String id, Date now) {
        E entity = newEntity(now);
        entity.setTopiaId(id);
        return entity;
    }

    @Override
    public final Map<String, R> uniqueIndex(ServiceContext context) {
        Map<String, R> references = new LinkedHashMap<>();
        ReferentialLocale referentialLocale = context.getReferentialLocale();
        try (Stream<E> stream = getDao(context).streamAll()) {
            stream.forEach(entity -> {
                R reference = toReference(referentialLocale, entity);
                references.put(reference.getId(), reference);
            });
        }
        return references;
    }

    @Override
    public final D toDto(ReferentialLocale referentialLocale, E entity) {
        D dto = BusinessDto.newDto(toDtoType(), entity.getTopiaCreateDate());
        toDto(referentialLocale, entity, dto);
        return dto;
    }

    @Override
    public final E toEntity(ReferentialLocale referentialLocale, D dto) {
        E entity = newEntity(dto.getTopiaCreateDate());
        fromDto(referentialLocale, entity, dto);
        return entity;
    }

    @Override
    public final Set<D> toDtoSet(ReferentialLocale referentialLocale, Stream<E> entities) {
        Set<D> result = new LinkedHashSet<>();
        entities.forEach(entity -> {
            D dto = toDto(referentialLocale, entity);
            result.add(dto);
        });
        return result;
    }

    @Override
    public final List<D> toDtoList(ReferentialLocale referentialLocale, Stream<E> entities) {
        List<D> result = new LinkedList<>();
        entities.forEach(entity -> {
            D dto = toDto(referentialLocale, entity);
            result.add(dto);
        });
        return result;
    }

    @Override
    public final List<ToolkitIdLabel> toLabelList(Locale referentialLocale, Stream<E> entities) {
        List<ToolkitIdLabel> result = new LinkedList<>();
        DecoratorProvider.registerDecoratorThen(toEntityType(), referentialLocale, entities, e -> result.add(e.toLabel()));
        return result;
    }

    @Override
    public final String getDefinitionContent() {
        if (definitionContent == null) {
            definitionContent = loadContent(getDefinitionContentURL());
        }
        return definitionContent;
    }

    @Override
    public final String getValidationCreateContent() {
        if (validationCreateContent == null) {
            validationCreateContent = loadContent(getValidationCreateContentURL());
        }
        return validationCreateContent;
    }

    @Override
    public final String getValidationUpdateContent() {
        if (validationUpdateContent == null) {
            validationUpdateContent = loadContent(getValidationUpdateContentURL());
        }
        return validationUpdateContent;
    }

    public final List<ToolkitIdLabel> toReferenceLabelList(Locale referentialLocale, Stream<R> entities) {
        List<ToolkitIdLabel> result = new LinkedList<>();
        DecoratorProvider.registerDecoratorThen(toReferenceType(), referentialLocale, entities, e -> result.add(e.toLabel()));
        return result;
    }

    public final List<E> loadEntities(ServiceContext context) {
        return getDao(context).findAll();
    }

    public final List<E> loadEntities(TopiaPersistenceContext persistenceContext) {
        return getDao(persistenceContext).findAll();
    }

    public final void consumeAndExecute(ServiceContext context, SqlServiceRequest sqlRequest) {
        SqlScript script = consume(context, sqlRequest);
        context.getTopiaPersistenceContext().executeSqlScript(script);
    }

    public final SqlScript consume(ServiceContext context, SqlServiceRequest sqlRequest) {
        return context.getTopiaApplicationContext().getSqlService().consume(sqlRequest);
    }

    protected final String loadContent(URL resource) {
        try (Reader inputStream = new InputStreamReader(Objects.requireNonNull(resource).openStream(), StandardCharsets.UTF_8)) {
            try (StringWriter out = new StringWriter(1024)) {
                inputStream.transferTo(out);
                return out.toString();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    protected TopiaEntitySqlScript getSqlScript() {
        return sqlScript.get();
    }

}
