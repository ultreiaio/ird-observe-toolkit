package fr.ird.observe.spi.consolidation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdModifications;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.SqlConversions;
import fr.ird.observe.spi.context.DtoEntityContext;
import io.ultreia.java4all.bean.monitor.JavaBeanPropertyModification;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlModel;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * To produce sql update code from some consolidation modifications.
 * <p>
 * Created at 04/03/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ToolkitIdModificationsToSql implements Closeable {

    public static final String UPDATE_SQL = "UPDATE %s SET %s WHERE topiaId = '%s';";
    public static final String SET_SQL_CLAUSE = "%s = %s";

    /**
     * Where to write sql statements.
     */
    private final Path target;
    /**
     * Now timestamp to used in any sql statement that requires it.
     */
    private final Timestamp now;
    /**
     * Topia Entity sql model used to perform conversions while writing sql statements.
     */
    private final TopiaEntitySqlModel entitySqlModel;
    /**
     * Set of spi used to produce sql statements (we be used in method {@link #build()} to add extra sql statements
     * to update {@code LastUpdateDate} table).
     */
    private final Set<DtoEntityContext<?, ?, ?, ?>> spiUsed;
    /**
     * Internal writer used to store sql statements.
     */
    private SqlScriptWriter writer;

    public ToolkitIdModificationsToSql(Path target, Timestamp now, TopiaEntitySqlModel entitySqlModel) {
        this.target = Objects.requireNonNull(target);
        this.now = Objects.requireNonNull(now);
        this.entitySqlModel = Objects.requireNonNull(entitySqlModel);
        this.spiUsed = new LinkedHashSet<>();
    }


    public static String toSetClause(TopiaEntitySqlTable sqlModelDescriptorTable, String propertyName, Object value) {
        String stringValue = SqlConversions.convertValue(sqlModelDescriptorTable, propertyName, value);
        return String.format(SET_SQL_CLAUSE, propertyName, stringValue);
    }

    public SqlScript build() {
        SqlScriptWriter writer = writer();
        Set<String> lastUpdateDateStatements = new LinkedHashSet<>();
        spiUsed.forEach(spi -> spi.getUpdateLastUpdateDateTableScript().generate(now).forEach(l -> lastUpdateDateStatements.add(l.trim())));
        lastUpdateDateStatements.forEach(writer::writeSql);
        return SqlScript.of(target);
    }

    public void toSql(DtoEntityContext<?, ?, ?, ?> spi, ToolkitIdModifications modifications) {
        if (modifications == null) {
            return;
        }
        TopiaEntitySqlTable sqlModelDescriptorTable = entitySqlModel.getDescriptor(spi.toEntityType()).getTable();
        boolean used = toSql(sqlModelDescriptorTable, modifications);
        if (used) {
            spiUsed.add(spi);
        }
    }

    public void toSql(DtoEntityContext<?, ?, ?, ?> spi, Set<ToolkitIdModifications> modificationsSet) {
        if (modificationsSet == null) {
            return;
        }
        TopiaEntitySqlTable sqlModelDescriptorTable = entitySqlModel.getDescriptor(spi.toEntityType()).getTable();
        boolean used = false;
        for (ToolkitIdModifications modifications : modificationsSet) {
            used |= toSql(sqlModelDescriptorTable, modifications);
        }
        if (used) {
            spiUsed.add(spi);
        }
    }

    @Override
    public void close() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }

    protected SqlScriptWriter writer() {
        return writer == null ? writer = SqlScriptWriter.of(target) : writer;
    }

    protected boolean toSql(TopiaEntitySqlTable sqlModelDescriptorTable, ToolkitIdModifications modifications) {
        if (modifications == null || !modifications.withModifications()) {
            // only warning on this one
            return false;
        }
        List<String> setClauses = new ArrayList<>(modifications.modificationsCount() + 2);
        String schemaAndTableName = sqlModelDescriptorTable.getSchemaAndTableName();
        for (JavaBeanPropertyModification modification : modifications.getModifications()) {
            setClauses.add(toSetClause(sqlModelDescriptorTable, modification.getPropertyName(), modification.getNewValue()));
        }
        setClauses.add(String.format(SET_SQL_CLAUSE, Entity.PROPERTY_TOPIA_VERSION, Entity.PROPERTY_TOPIA_VERSION + " + 1"));
        setClauses.add(toSetClause(sqlModelDescriptorTable, Entity.PROPERTY_LAST_UPDATE_DATE, now));
        String sql = String.format(UPDATE_SQL, schemaAndTableName, String.join(", ", setClauses), modifications.getId());
        writer().writeSql(sql);
        return true;
    }
}
