package fr.ird.observe.spi;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import fr.ird.observe.entities.Entity;
import io.ultreia.java4all.lang.Numbers;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * To perform some conversions to sql.
 * <p>
 * Created at 05/03/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class SqlConversions {

    public static String escapeString(String comment) {
        return SqlHelper.escapeString(comment);
    }

    public static String timestamp(Date date) {
        return String.format("%1$tF %1$tT", date);
    }

    public static String toTimeStamp(Date time) {
        return time == null ? null : SqlHelper.escapeString(timestamp(time)) + "::timestamp";
    }

    public static String time(Date date) {
        return String.format("%1$tT", date);
    }

    public static String day(Date date) {
        return String.format("%1$tF", date);
    }

    public static String toTime(Date time) {
        return time == null ? null : SqlHelper.escapeString(time(time)) + "::time";
    }

    public static String toDate(Date time) {
        return time == null ? null : SqlHelper.escapeString(day(time)) + "::date";
    }

    public static String toId(Entity entity) {
        return entity == null ? "NULL" : SqlHelper.escapeString(entity.getTopiaId());
    }

    public static String convertValue(TopiaEntitySqlTable sqlModelDescriptorTable, String propertyName, Object value) {
        String stringValue;
        if (value == null) {
            stringValue = "NULL";
        } else if (value instanceof Entity) {
            stringValue = SqlHelper.escapeString(((Entity) value).getId());
        } else {
            Class<?> propertyType = sqlModelDescriptorTable.getSimplePropertiesType(propertyName);
            if (propertyType.equals(String.class)) {
                stringValue = SqlHelper.escapeString((String) value);
            } else if (propertyType.equals(Timestamp.class)) {
                stringValue = toTimeStamp((Date) value);
            } else if (propertyType.equals(Time.class)) {
                stringValue = toTime((Date) value);
            } else if (propertyType.equals(java.sql.Date.class)) {
                stringValue = toDate((Date) value);
            } else if (Enum.class.isAssignableFrom(propertyType)) {
                if (sqlModelDescriptorTable.isEnumerationPropertyUsingName(propertyName)) {
                    stringValue = SqlHelper.escapeString(((Enum<?>) value).name());
                } else {
                    stringValue = String.valueOf(((Enum<?>) value).ordinal());
                }
            } else if (sqlModelDescriptorTable.isDecimalType(propertyType)) {
                Integer scale = sqlModelDescriptorTable.getDecimalPropertiesScale(propertyName);
                stringValue = String.valueOf(Numbers.roundNDigits((Float) value, scale));
            } else {
                stringValue = value.toString();
            }
        }
        return stringValue;
    }
}
