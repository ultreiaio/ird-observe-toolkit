package fr.ird.observe.spi.navigation.tree;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.WithPermission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModelWriter;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelPathRequest;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeNode;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeNodeBean;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.tree.navigation.NavigationTreeNodeChildrenBuilderSupport;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 12/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class TreeBuilderSupport implements WithPermission {
    private static final Logger log = LogManager.getLogger(TreeBuilderSupport.class);
    protected final DecoratorService decoratorService;
    protected final ReferentialLocale referentialLocale;
    protected final ToolkitTopiaPersistenceContextSupport persistenceContext;
    protected final Date now;
    private final WithPermission permission;
    private final ServiceContext context;
    private final BusinessProject project;
    private final Function<String, GroupBySpiContext<RootOpenableDto, ?, RootOpenableEntity, ?, ?, ?, ?>> spiContextGetter;

    public TreeBuilderSupport(ServiceContext context, BusinessProject project, WithPermission permission, Function<String, GroupBySpiContext<RootOpenableDto, ?, RootOpenableEntity, ?, ?, ?, ?>> spiContextGetter) {
        this.context = context;
        this.project = project;
        this.decoratorService = context.getDecoratorService();
        this.permission = permission;
        this.referentialLocale = decoratorService.getReferentialLocale();
        this.persistenceContext = context.getTopiaPersistenceContext();
        this.now = context.now();
        this.spiContextGetter = spiContextGetter;
    }

    public DataGroupByDtoSet<?, ?> buildDataGroupByDtoSet(ToolkitTreeFlatModelRootRequest request, Date timestamp) {
        DataGroupByDtoSet<?, ?> data = null;
        if (request.isLoadData() && canReadData()) {
            GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spiContext = getSpiContext(request.getGroupByName());
            data = spiContext.loadNavigationResult(context, request, timestamp);
            log.info(String.format("Found %d groupBy entry(ies).", data.size()));
            log.info(String.format("Found %d trip(s).", data.stream().mapToLong(DataGroupByDto::getCount).sum()));
        }
        return data;
    }

    public NavigationResult buildNavigationResult(ToolkitTreeFlatModelRootRequest request, Date timestamp) {
        DataGroupByDtoSet<?, ?> data = null;
        long dataCount = -1;
        if (request.isLoadData() && canReadData()) {
            GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spiContext = getSpiContext(request.getGroupByName());
            data = spiContext.loadNavigationResult(context, request, timestamp);
            dataCount = spiContext.getDataCount(context);
            log.info(String.format("Found %d groupBy entry(ies).", data.size()));
            log.info(String.format("Found %d trip(s).", data.stream().mapToLong(DataGroupByDto::getCount).sum()));
        }
        Map<Class<? extends ReferentialDto>, Long> referentialCount = computeReferentialCount(request);
        if (referentialCount != null) {
            log.info(String.format("Found %d referential type(s).", referentialCount.size()));
        }
        return new NavigationResult(request, dataCount, data, timestamp, referentialCount);
    }

    public ToolkitTreeFlatModel buildNavigationFlatModel(ToolkitTreeFlatModelRootRequest request) {
        ToolkitTreeFlatModelWriter builder;
        if (request.isLoadData() && canReadData()) {
            GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spiContext = getSpiContext(request.getGroupByName());
            long dataCount = spiContext.getDataCount(context);
            builder = new ToolkitTreeFlatModelWriter("", now, dataCount);
            spiContext.loadFlatModel(context, request, false, builder, canWriteData());
        } else {
            builder = new ToolkitTreeFlatModelWriter("", now, -1);

        }
        return finalizeFlatModel(request, builder);
    }

    public ToolkitTreeFlatModel buildSelectionFlatModel(ToolkitTreeFlatModelRootRequest request) {
        ToolkitTreeFlatModelWriter builder;
        if (request.isLoadData() && canReadData()) {
            GroupBySpiContext<?, ?, ?, ?, ?, ?, ?> spiContext = getSpiContext(request.getGroupByName());
            long dataCount = spiContext.getDataCount(context);
            builder = new ToolkitTreeFlatModelWriter("", now, dataCount);
            spiContext.loadFlatModel(context, request, true, builder, canWriteData());
        } else {
            builder = new ToolkitTreeFlatModelWriter("", now, -1);
        }
        return finalizeFlatModel(request, builder);
    }

    public ToolkitTreeFlatModel loadNavigationPath(ToolkitTreeFlatModelPathRequest request, NavigationTreeNode rootNode, Function<Boolean, NavigationTreeNodeChildrenBuilderSupport> childrenBuilderFunction) {
        Path nodePath = Path.of(request.getPath());
        log.info(String.format("Load structure: %s", nodePath));
        NavigationTreeNode node = ToolkitTreeNodeBuilder.build(rootNode, nodePath);
        log.info(String.format("Fill node: %s", nodePath));
        Object rootData = loadPathData(node, request.isRecursive());
        log.info(String.format("Build [%s] children.", node.getNodePath()));
        childrenBuilderFunction.apply(request.isRecursive()).build(node, rootData);
        log.info(String.format("Generate flat model: %s", nodePath));
        return ToolkitTreeFlatModel.of(node.getRoot(), now, -1);
    }

    @Override
    public final Permission getCredentials() {
        return permission.getCredentials();
    }

    private ToolkitTreeFlatModel finalizeFlatModel(ToolkitTreeFlatModelRootRequest request, ToolkitTreeFlatModelWriter builder) {
        Map<Class<? extends ReferentialDto>, Long> referentialCount = computeReferentialCount(request);
        if (referentialCount != null) {
            builder.addReferential(project, request, referentialLocale.getLocale(), referentialCount, canWriteReferential());
        }
        return builder.build(true);
    }

    private Map<Class<? extends ReferentialDto>, Long> computeReferentialCount(ToolkitTreeFlatModelRootRequest request) {
        Map<Class<? extends ReferentialDto>, Long> referentialCount = null;
        if (request.isLoadReferential() && canReadReferential()) {
            Set<Class<? extends ReferentialDto>> referentialTypes = project.getReferentialTypes();
            referentialCount = new LinkedHashMap<>();
            Map<Class<? extends ReferentialDto>, Long> finalReferentialCount = referentialCount;
            referentialTypes.forEach(t -> finalReferentialCount.put(t, countReferential(t)));
            log.info(String.format("Found %d referential type(s).", referentialCount.size()));
        }
        return referentialCount;
    }

    protected Object loadPathData(NavigationTreeNode node, boolean loadData) {
        //FIXME Use a ToolkitId instead of a simple String, in that way we will always have the associated dto type
        Object rootData = null;
        String dataId = node.getState(NavigationTreeNodeBean.STATE_ID);
        if (node.getUserObject().isRootOpenFilter()) {
            String groupByName = node.getState(NavigationTreeNodeBean.STATE_FILTER_NAME);
            if (dataId == null) {
                throw new IllegalStateException(String.format("Can't load empty group by for: %s", groupByName));
            }
            String groupByFlavor = node.getState(NavigationTreeNodeBean.STATE_FILTER_FLAVOR);
            rootData = getSpiContext(groupByName).getGroupByValue(context, loadData, dataId, groupByFlavor);
        } else {
            if (dataId != null) {
                rootData = persistenceContext.findByTopiaId(dataId);
            }
            Entity entity = (Entity) Objects.requireNonNull(rootData);
            DtoEntityContext<?, ?, Entity, ?> spi = PersistenceBusinessProject.fromEntity(entity);
            decoratorService.installDecorator(spi.toEntityType(), entity);
        }
        log.info(String.format("Loaded [%s] entity.", rootData));
        return rootData;
    }

    protected GroupBySpiContext<RootOpenableDto, ?, RootOpenableEntity, ?, ?, ?, ?> getSpiContext(String groupName) {
        return spiContextGetter.apply(groupName);
    }

    protected Long countReferential(Class<? extends ReferentialDto> dtoType) {
        return PersistenceBusinessProject.fromReferentialDto(dtoType).getDao(persistenceContext).count();
    }

}
