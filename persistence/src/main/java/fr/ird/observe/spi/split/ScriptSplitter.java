package fr.ird.observe.spi.split;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.service.migration.version.MigrationServiceSqlHelper;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * To split a sql script into different parts.
 * <p>
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
public class ScriptSplitter {

    public static class SplitSimpleResult {
        private final List<String> createSchemaStatements;
        private final List<String> finalizeSchemaStatements;
        private final List<String> otherSchemaStatements;

        public SplitSimpleResult() {
            this.createSchemaStatements = new LinkedList<>();
            this.finalizeSchemaStatements = new LinkedList<>();
            this.otherSchemaStatements = new LinkedList<>();
        }

        public List<String> getCreateSchemaStatements() {
            return createSchemaStatements;
        }

        public List<String> getFinalizeSchemaStatements() {
            return finalizeSchemaStatements;
        }

        public List<String> getOtherSchemaStatements() {
            return otherSchemaStatements;
        }
    }

    public static class SplitResult {

        private final SplitSimpleResult parent;
        private final SplitNotSchemaResult notSchema;

        public SplitResult(SplitSimpleResult parent, SplitNotSchemaResult notSchemaResult) {
            this.parent = parent;
            this.notSchema = notSchemaResult;
        }

        public SplitSimpleResult getParent() {
            return parent;
        }

        public SplitNotSchemaResult getNotSchema() {
            return notSchema;
        }

    }

    public static class SplitNotSchemaResult {

        private final List<String> insertStatements;
        private final List<String> updateStatements;
        private final List<String> insertAssociationStatements;

        public SplitNotSchemaResult() {
            this.insertStatements = new LinkedList<>();
            this.updateStatements = new LinkedList<>();
            this.insertAssociationStatements = new LinkedList<>();
        }

        public List<String> getInsertStatements() {
            return insertStatements;
        }

        public List<String> getUpdateStatements() {
            return updateStatements;
        }

        public List<String> getInsertAssociationStatements() {
            return insertAssociationStatements;
        }

    }

    public static class SplitNotSchemaResultPair {
        private final SplitNotSchemaResult data;
        private final SplitNotSchemaResult referential;

        public SplitNotSchemaResultPair(SplitNotSchemaResult data, SplitNotSchemaResult referential) {
            this.data = data;
            this.referential = referential;
        }

        public SplitNotSchemaResult getData() {
            return data;
        }

        public SplitNotSchemaResult getReferential() {
            return referential;
        }
    }

    public static class Builder {

        private final boolean forH2;
        private final SqlScript script;

        Predicate<String> onSchema = trim -> trim.startsWith("create schema ") || (!trim.contains("system_lob_stream") && trim.startsWith("create table ")) || (!trim.contains("system_combine_blob") && !trim.contains("system_combine_clob") && trim.startsWith("create alias "));
        Predicate<String> onTable = trim -> trim.startsWith("alter table ") || trim.startsWith("alter table if exists ")
                || trim.startsWith("insert into " + MigrationServiceSqlHelper.LEGACY.gav().toLowerCase() + "(") //FIXME:Migration when going to the default layout we can remove this line
                || trim.startsWith("insert into " + MigrationServiceSqlHelper.DEFAULT.gav().toLowerCase() + "(");
        Predicate<String> onInsert = trim -> trim.startsWith("insert into ") ||
                (trim.contains("system_lob_stream") && !(trim.contains("drop "))) ||
                (trim.contains("system_combine_clob") && !(trim.contains("call ") || trim.contains("drop "))) ||
                trim.contains("system_combine_blob") && !(trim.contains("call ") || trim.contains("drop "));
        Pattern insertIntoAssociationPattern = Pattern.compile("\\s*insert\\s+into\\s+(.+)?\\s*\\(([^,]+)?,([^,]+)?\\)\\s+values\\s+\\(\\s*'([^']+)?'\\s*,\\s*'([^']+)?'\\s*\\).*;", Pattern.CASE_INSENSITIVE);
        Predicate<String> onInsertAssociation = trim -> insertIntoAssociationPattern.matcher(trim).matches();
        Predicate<String> onCreateIndex = trim -> trim.startsWith("create index idx_");
        Predicate<String> onUpdate = trim -> trim.startsWith("update ") ||
                (trim.contains("system_lob_stream") && (trim.contains("drop "))) ||
                (trim.contains("system_combine_blob") && (trim.contains("call ") || trim.contains("drop "))) ||
                (trim.contains("system_combine_clob") && (trim.contains("call ") || trim.contains("drop ")));

        public Builder(boolean forH2, Path scriptPath) {
            this.forH2 = forH2;
            this.script = SqlScript.of(scriptPath);
        }

        public Builder(boolean forH2, SqlScript script) {
            this.forH2 = forH2;
            this.script = script;
        }

        public SplitResult splitSchema() {

            SplitSimpleResult simpleResult = new SplitSimpleResult();
            SplitNotSchemaResult notSchemaResult = new SplitNotSchemaResult();

            script.getLocation().forEach(statement -> {
                String trim = statement.toLowerCase().trim();
                boolean hibernateGeneratedUniqueKey = statement.contains("UK_");
                if (hibernateGeneratedUniqueKey) {
                    // Skip hibernate bad unique key generation (use ours)
                    return;
                }
                if (onSchema.test(trim)) {
                    if (forH2) {
                        statement = statement.replace("data OID", "data blob");
                    } else {
                        statement = statement.replace("data blob", "data OID");
                    }
                    simpleResult.createSchemaStatements.add(statement);
                    return;
                }
                if (onTable.test(trim)) {
                    simpleResult.finalizeSchemaStatements.add(statement);
                    return;
                }
                if (onCreateIndex.test(trim)) {
                    simpleResult.finalizeSchemaStatements.add(statement);
                    return;
                }
                if (onUpdate.test(trim)) {
                    notSchemaResult.updateStatements.add(statement);
                    return;
                }
                if (onInsert.test(trim)) {
                    if (onInsertAssociation.test(trim)) {
                        notSchemaResult.insertAssociationStatements.add(statement);
                        return;
                    }
                    notSchemaResult.insertStatements.add(statement);
                    return;
                }
                simpleResult.otherSchemaStatements.add(statement);
            });
            simpleResult.finalizeSchemaStatements.sort(String::compareToIgnoreCase);
            notSchemaResult.insertAssociationStatements.sort(String::compareToIgnoreCase);
            return new SplitResult(simpleResult, notSchemaResult);
        }

        public SplitSimpleResult splitSimple() {

            SplitSimpleResult result = new SplitSimpleResult();
            List<String> insertAssociationStatements = new LinkedList<>();

            script.getLocation().forEach(statement -> {
                String trim = statement.toLowerCase().trim();
                boolean hibernateGeneratedUniqueKey = statement.contains("UK_");
                if (hibernateGeneratedUniqueKey) {
                    // Skip hibernate bad unique key generation (use ours)
                    return;
                }
                if (onSchema.test(trim)) {
                    if (forH2) {
                        statement = statement.replace("data OID", "data blob");
                    } else {
                        statement = statement.replace("data blob", "data OID");
                    }
                    result.createSchemaStatements.add(statement);
                    return;
                }
                if (onTable.test(trim)) {
                    result.finalizeSchemaStatements.add(statement);
                    return;
                }
                if (onCreateIndex.test(trim)) {
                    result.finalizeSchemaStatements.add(statement);
                    return;
                }
                if (onInsert.test(trim)) {
                    if (onInsertAssociation.test(trim)) {
                        insertAssociationStatements.add(statement);
                        return;
                    }
                }
                result.otherSchemaStatements.add(statement);
            });
            result.finalizeSchemaStatements.sort(String::compareToIgnoreCase);
            insertAssociationStatements.sort(String::compareToIgnoreCase);
            result.otherSchemaStatements.addAll(insertAssociationStatements);
            return result;
        }
    }

    public static SplitNotSchemaResultPair splitDataAndReferential(SplitNotSchemaResult context) {
        SplitNotSchemaResult data = new SplitNotSchemaResult();
        SplitNotSchemaResult referential = new SplitNotSchemaResult();

        Pattern insertIntoDataPattern = Pattern.compile("(.+)?values\\s*\\(\\s*'fr\\.ird\\.data\\.(.+)?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        Pattern updateDataPattern = Pattern.compile("(.+)?where\\s*topiaid\\s*=\\s*'fr\\.ird\\.data\\.(.+)?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);

        Pattern insertIntoReferentialPattern = Pattern.compile("(.+)?values\\s*\\(\\s*'fr\\.ird\\.referential\\.(.+)?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        Pattern updateReferentialPattern = Pattern.compile("(.+)?where\\s*topiaid\\s*=\\s*'fr\\.ird\\.referential\\.(.+)?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);

        Predicate<String> insertForData = trim ->
                trim.startsWith("insert into system_lob_stream") ||
                        (trim.contains("system_lob_stream") && !(trim.contains("drop "))) ||
                        (trim.contains("system_combine_clob") && !(trim.contains("call ") || trim.contains("drop "))) ||
                        trim.contains("system_combine_blob") && !(trim.contains("call ") || trim.contains("drop ")) ||
                        insertIntoDataPattern.matcher(trim).matches();
        Predicate<String> updateForData = trim ->
                (trim.contains("system_lob_stream") && (trim.contains("drop "))) ||
                        (trim.contains("system_combine_blob") && (trim.contains("call ") || trim.contains("drop "))) ||
                        (trim.contains("system_combine_clob") && (trim.contains("call ") || trim.contains("drop "))) ||
                        updateDataPattern.matcher(trim).matches();

        @SuppressWarnings("SpellCheckingInspection") Predicate<String> insertForReferential = trim -> trim.startsWith("insert into common.lastupdatedate(") || insertIntoReferentialPattern.matcher(trim).matches();
        Predicate<String> updateForReferential = trim -> updateReferentialPattern.matcher(trim).matches();

        boolean withBlob = false;
        for (String statement : context.insertStatements) {
            String trim = statement.trim().toLowerCase();
            if (insertForData.test(trim)) {
                data.insertStatements.add(statement);
                if (trim.contains("system_lob_stream") ||
                        trim.contains("system_combine_clob") ||
                        trim.contains("system_combine_blob")) {
                    withBlob = true;
                }
                continue;
            }
            if (insertForReferential.test(trim)) {
                referential.insertStatements.add(statement);
                continue;
            }
            throw new IllegalStateException("Can't manage insert statement: " + statement);
        }
        for (String statement : context.updateStatements) {
            String trim = statement.trim().toLowerCase();
            if (updateForData.test(trim)) {
                data.updateStatements.add(statement);
                continue;
            }
            if (updateForReferential.test(trim)) {
                referential.updateStatements.add(statement);
                continue;
            }
            throw new IllegalStateException("Can't manage update statement: " + statement);
        }
        if (withBlob) {
            data.updateStatements.add("DROP TABLE IF EXISTS SYSTEM_LOB_STREAM;");
            data.updateStatements.add("CALL SYSTEM_COMBINE_BLOB(-1);");
            data.updateStatements.add("DROP ALIAS IF EXISTS SYSTEM_COMBINE_CLOB;");
            data.updateStatements.add("DROP ALIAS IF EXISTS SYSTEM_COMBINE_BLOB;");
        }
        for (String statement : context.insertAssociationStatements) {
            String trim = statement.trim().toLowerCase();
            if (insertForData.test(trim)) {
                data.insertAssociationStatements.add(statement);
                continue;
            }
            if (insertForReferential.test(trim)) {
                referential.insertAssociationStatements.add(statement);
                continue;
            }
            throw new IllegalStateException("Can't manage insert association statement: " + statement);
        }
        return new SplitNotSchemaResultPair(data, referential);
    }

    public static Builder create(boolean forH2, Path scriptPath) {
        return new Builder(forH2, scriptPath);
    }

    public static Builder create(boolean forH2, SqlScript script) {
        return new Builder(forH2, script);
    }

    public static SplitSimpleResult filterSchema(SplitSimpleResult context, String schemaName) {
        return filter(context, schemaName, schemaName);
    }

    public static SplitSimpleResult filterTable(SplitSimpleResult context, String schemaName, String tableName) {
        return filter(context, schemaName + "." + tableName + " ", schemaName + "_" + tableName + "(");
    }

    public static SplitSimpleResult filter(SplitSimpleResult context, String suffix, String suffix2) {

        SplitSimpleResult result = new SplitSimpleResult();

        Predicate<String> onSchemaWithFilter = trim -> trim.startsWith("create schema " + suffix) || trim.startsWith("create schema if not exists " + suffix) || trim.startsWith("create table " + suffix);
        Predicate<String> onTableWithFilter = trim -> trim.startsWith("alter table " + suffix) || trim.startsWith("alter table if exists " + suffix);
        Predicate<String> onInsertWithFilter = trim -> trim.startsWith("insert into " + suffix);
        Predicate<String> onCreateIndexWithFilter = trim -> trim.startsWith("create index idx_" + suffix2);

        for (String statement : context.getCreateSchemaStatements()) {
            String trim = statement.toLowerCase().trim();
            if (onSchemaWithFilter.test(trim)) {
                result.createSchemaStatements.add(statement);
            }
        }
        for (String statement : context.getFinalizeSchemaStatements()) {
            String trim = statement.toLowerCase().trim();
            if (onTableWithFilter.test(trim)) {
                result.finalizeSchemaStatements.add(statement);
                continue;
            }
            if (onCreateIndexWithFilter.test(trim)) {
                result.finalizeSchemaStatements.add(statement);
            }
        }
        for (String statement : context.getOtherSchemaStatements()) {
            String trim = statement.toLowerCase().trim();
            if (onTableWithFilter.test(trim)) {
                result.finalizeSchemaStatements.add(statement);
                continue;
            }
            if (onInsertWithFilter.test(trim)) {
                result.otherSchemaStatements.add(statement);
            }
        }
        result.finalizeSchemaStatements.sort(String::compareToIgnoreCase);
        return result;
    }
}
