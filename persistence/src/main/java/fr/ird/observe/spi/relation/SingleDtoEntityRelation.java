package fr.ird.observe.spi.relation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.Entity;

/**
 * Represents a entity relation associated to a dto.
 * <p>
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class SingleDtoEntityRelation<
        PD extends BusinessDto,
        D extends BusinessDto,
        PE extends Entity,
        E extends Entity> extends DtoEntityRelation<PD, D, PE, E> {

    public static <
            PD extends BusinessDto,
            D extends BusinessDto,
            PE extends Entity,
            E extends Entity> SingleDtoEntityRelation<PD, D, PE, E> create(Class<PD> parentDtoType, Class<D> targetDtoType, Class<PE> parentEntityType, Class<E> targetEntityType, String relationName) {
        return new SingleDtoEntityRelation<>(parentDtoType, targetDtoType, parentEntityType, targetEntityType, relationName);
    }

    private SingleDtoEntityRelation(Class<PD> parentDtoType, Class<D> targetDtoType, Class<PE> parentEntityType, Class<E> targetEntityType, String relationName) {
        super(parentDtoType, targetDtoType, parentEntityType, targetEntityType, relationName, false);
    }
}
