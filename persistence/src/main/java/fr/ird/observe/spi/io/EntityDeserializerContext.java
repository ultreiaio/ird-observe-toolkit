package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import io.ultreia.java4all.util.Dates;
import io.ultreia.java4all.util.json.adapters.DateAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaIdFactoryForBulkSupport;
import org.nuiton.topia.persistence.TopiaNoResultException;

import javax.sql.rowset.serial.SerialBlob;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 02/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.21
 */
@SuppressWarnings("unchecked")
public class EntityDeserializerContext {

    public static final String ID_REFERENCE_PREFIX = "$$";
    private static final Logger log = LogManager.getLogger(EntityDeserializerContext.class);
    private final ToolkitTopiaPersistenceContextSupport persistenceContext;
    private final Gson gson;
    private final TopiaIdFactoryForBulkSupport idFactory;
    private final Date now;
    private final DateAdapter dataAdapter;
    private final List<Entity> created;
    private final Map<String, Entity> createdWithRefs;


    private static boolean isRef(String id) {
        return id != null && id.startsWith(ID_REFERENCE_PREFIX);
    }

    public EntityDeserializerContext(ToolkitTopiaPersistenceContextSupport persistenceContext, Supplier<Gson> gson, TopiaIdFactoryForBulkSupport idFactory, Date now) {
        this.persistenceContext = Objects.requireNonNull(persistenceContext);
        this.gson = Objects.requireNonNull(gson).get();
        this.idFactory = idFactory;
        this.now = now;
        this.dataAdapter = new DateAdapter();
        this.created = new LinkedList<>();
        this.createdWithRefs = new LinkedHashMap<>();
    }

    protected <E extends ReferentialEntity> void visitReferential(Entity entity, String propertyName, Class<E> data, Object jsonObject) {
        E e;
        if (jsonObject instanceof String) {
            // just a reference
            e = jsonToEntity(data, (String) jsonObject);
        } else {
            e = subJsonToEntity(data, (Map<String, Object>) jsonObject);
        }
        entity.set(propertyName, e);
    }

    protected <E extends DataEntity> void visitData(Entity entity, String propertyName, Class<E> data, Object jsonObject) {
        E e;
        if (jsonObject instanceof String) {
            // just a reference
            e = jsonToEntity(data, (String) jsonObject);
        } else {
            e = subJsonToEntity(data, (Map<String, Object>) jsonObject);
        }
        entity.set(propertyName, e);
    }

    protected <O> void visitSimple(Entity entity, String propertyName, Class<O> type, Object value) {
        O realValue = (O) jsonToObject(type, propertyName, value);
        set(entity, propertyName, realValue);
    }

    protected <E extends ReferentialEntity> void visitReferentialCollection(Entity entity, String propertyName, Class<?> collectionType, Class<E> type, Collection<?> jsonObject) {
        Collection<E> collection = entity.get(propertyName);
        if (!jsonObject.isEmpty() && collection == null) {
            collection = gson.fromJson("[]", (Type) collectionType);
            set(entity, propertyName, collection);
        }
        for (Object o : jsonObject) {
            E e;
            if (o instanceof String) {
                e = jsonToEntity(type, (String) o);
            } else {
                e = subJsonToEntity(type, (Map<String, Object>) o);
            }
            if (e != null) {
                collection.add(e);
            }
        }
    }

    protected <E extends DataEntity> void visitDataCollection(Entity entity, String propertyName, Class<?> collectionType, Class<E> type, Collection<?> jsonObject) {
        Collection<E> collection = entity.get(propertyName);
        if (!jsonObject.isEmpty() && collection == null) {
            collection = gson.fromJson("[]", (Type) collectionType);
            set(entity, propertyName, collection);
        }
        for (Object o : jsonObject) {
            E e;
            if (o instanceof String) {
                e = jsonToEntity(type, (String) o);
            } else {
                e = subJsonToEntity(type, (Map<String, Object>) o);
            }
            if (e != null) {
                collection.add(e);
            }
        }
    }

    protected void set(Entity entity, String propertyName, Object realValue) {
        try {
            entity.set(propertyName, realValue);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Could not set property named %s on entity '%s' with value: %s", propertyName, entity.getTopiaId(), realValue instanceof Collection ? ((Collection<?>) realValue).size() : realValue), e);
        }
    }

    protected List<Entity> getCreated() {
        return Collections.unmodifiableList(created);
    }

    protected Map<String, Object> toJson(String json) {
        return gson.fromJson(json, (Type) Object.class);
    }

    protected <E extends Entity> void fillEntity(E entity, Map<String, Object> jsonObject) {
        entity.accept(new EntityDeserializerVisitor(this, jsonObject));
    }

    protected <O> void visitCollection(Entity entity, String propertyName, Class<?> collectionType, Class<O> type, Collection<?> jsonObject) {
        Collection<O> collection = entity.get(propertyName);
        if (!jsonObject.isEmpty() && collection == null) {
            collection = gson.fromJson("[]", (Type) collectionType);
        }
        for (Object o : jsonObject) {
            O e = (O) jsonToObject(type, propertyName, o);
            collection.add(e);
        }
    }

    protected <E extends Entity> E subJsonToEntity(Class<E> entityType, Map<String, Object> jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        String id = (String) jsonObject.get("topiaId");
        E entity = jsonToEntity(entityType, id);
        fillEntity(entity, jsonObject);
        return entity;
    }

    protected <E extends Entity> E jsonToEntity(Class<E> entityType, String id) {
        E entity;
        if (id == null) {
            entity = newEntity(entityType);
        } else if (isRef(id)) {
            entity = newEntityRef(entityType, id);
        } else {
            id = URLDecoder.decode(id, StandardCharsets.UTF_8);
            entity = loadEntity(entityType, id);
        }
        return entity;
    }

    protected <E extends Entity> E newEntity(Class<E> entityType) {
        E e = persistenceContext.getDao(entityType).newInstance();
        generateId(entityType, e);
        created.add(e);
        return e;
    }

    protected <E extends Entity> E newEntityRef(Class<E> entityType, String id) {
        E e = (E) createdWithRefs.get(id);
        if (e == null) {
            e = persistenceContext.getDao(entityType).newInstance();
            created.add(e);
            createdWithRefs.put(id, e);
            // in fact we generate here the correct id, using the reference one for the mapping is enough
            generateId(entityType, e);
        }
        return e;
    }

    protected <E extends Entity> E loadEntity(Class<E> entityType, String id) {
        try {
            return persistenceContext.getDao(entityType).forTopiaIdEquals(id).findUnique();
        } catch (TopiaNoResultException e) {
            log.error("Could not get id: " + id);
            throw e;
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected Object jsonToObject(Class<?> type, String propertyName, Object jsonObject) {
        if (jsonObject != null) {
            if (jsonObject instanceof String) {
                if (String.class.isAssignableFrom(type)) {
                    // already treated
                    return jsonObject;
                }
                if (Date.class.isAssignableFrom(type)) {
                    String string = jsonObject.toString();
                    if (propertyName.equals("time") && string.length() == 5) {
                        LocalTime localTime = LocalTime.parse(string);
                        int hour = localTime.getHour();
                        int minute = localTime.getMinute();
                        return Dates.createDate(0, minute, hour, 1, 1, 1970);
                    }
                    return dataAdapter.deserializeToDate(string);
                }
                if (Enum.class.isAssignableFrom(type)) {
                    return Enum.valueOf((Class) type, (String) jsonObject);
                }
                if (Blob.class.isAssignableFrom(type)) {
                    try {
                        return new SerialBlob(((String) jsonObject).getBytes());
                    } catch (SQLException e) {
                        throw new IllegalStateException("Can't get blob", e);
                    }
                } else {
                    return gson.fromJson((String) jsonObject, type);
                }
            }
            if (Integer.class.isAssignableFrom(type) || int.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).intValue();
            }
            if (Float.class.isAssignableFrom(type) || float.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).floatValue();
            }
            if (Double.class.isAssignableFrom(type) || double.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).doubleValue();
            }
            if (Long.class.isAssignableFrom(type) || long.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).longValue();
            }
            if (Character.class.isAssignableFrom(type) || char.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).intValue();
            }
            if (Short.class.isAssignableFrom(type) || short.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).intValue();
            }
            if (Byte.class.isAssignableFrom(type) || byte.class.isAssignableFrom(type)) {
                return ((Number) jsonObject).byteValue();
            }
        }
        return jsonObject;
    }

    protected <E extends Entity> void generateId(Class<E> entityType, E e) {
        String id = idFactory.newTopiaId(entityType);
        e.setTopiaId(id);
        e.setLastUpdateDate(now);
    }
}
