package fr.ird.observe.spi.report;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.I18nReferentialEntity;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.ReportRequestExecutorI18nContext;
import fr.ird.observe.report.ReportVariableValueAware;
import fr.ird.observe.spi.PersistenceBusinessProject;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaDaoSupplier;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings("SpellCheckingInspection")
public class DefaultReportRequestExecutor implements ReportRequestExecutor {

    public static final TimeLog TIME_LOG = new TimeLog(DefaultReportRequestExecutor.class, 500, 1000);
    /**
     * Persistence context
     */
    private final TopiaPersistenceContext persistenceContext;
    /**
     * Dao supplier of main data
     */
    private final Supplier<? extends TopiaDao<?>> dataDaoSupplier;
    /**
     * To fmrat date.
     */
    private final SimpleDateFormat dateFormatter;
    /**
     * To format time.
     */
    private final SimpleDateFormat timeFormatter;
    /**
     * To format timestamp
     */
    private final SimpleDateFormat timestampFormatter;
    /**
     * I18n context.
     */
    private final ReportRequestExecutorI18nContext i18nContext;

    public DefaultReportRequestExecutor(ReferentialLocale referentialLocale, TopiaPersistenceContext persistenceContext, Class<? extends DataEntity> mainEntityType) {
        this.i18nContext = new ReportRequestExecutorI18nContext(Objects.requireNonNull(referentialLocale));
        this.persistenceContext = Objects.requireNonNull(persistenceContext);
        this.dataDaoSupplier = SingletonSupplier.of(() -> persistenceContext.getDao(mainEntityType));
        this.timeFormatter = I18nDecoratorHelper.newTimeFormat(referentialLocale.getLocale());
        this.dateFormatter = I18nDecoratorHelper.newDateFormat(referentialLocale.getLocale());
        this.timestampFormatter = I18nDecoratorHelper.newTimestampFormat(referentialLocale.getLocale());
    }

    @Override
    public <R> List<R> executeRequest(String request, Map<String, Object> params) {
        long t0 = TimeLog.getTime();
        // si il y a des references de DTO dans les valeurs des paramètres on les remplace par leur id
        // on copie pour ne pas modifier le paramètre de la méthode
        Map<String, Object> paramsFixes = new HashMap<>();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String name = entry.getKey();
            if (ReportRequestExecutor.isVariableUsed(request, name)) {
                Object value = entry.getValue();
                if (value instanceof DtoReference) {
                    DtoReference referenceDto = (DtoReference) value;
                    value = referenceDto.getId();
                } else if (value instanceof ReportVariableValueAware) {
                    ReportVariableValueAware<?> reportVariableValue = (ReportVariableValueAware<?>) value;
                    value = reportVariableValue.getValue();
                }
                paramsFixes.put(name, value);
            }
        }
        log.debug(String.format("Request: %s, params: %s", request, paramsFixes));
        try {
            return dataDaoSupplier.get().findAll(request, paramsFixes);
        } finally {
            TIME_LOG.log(t0, "execute request", request);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public <V> Set<V> transformVariableValues(List<?> universe, Class<V> type) {
        Set<V> values = new LinkedHashSet<>();
        if (ReferentialDtoReference.class.isAssignableFrom(type)) {
            DtoReferenceCollection<?> references = PersistenceBusinessProject.fromReferentialReference((Class) type).toReferenceSet(i18nContext().getReferentialLocale(), universe, null, null);
            values.addAll((Collection<? extends V>) references.toSet());
        } else if (DataDtoReference.class.isAssignableFrom(type)) {
            DtoReferenceCollection<?> references = PersistenceBusinessProject.fromDataReference((Class) type).toReferenceSet(i18nContext().getReferentialLocale(), universe, null, null);
            values.addAll((Collection<? extends V>) references.toSet());
        } else {
            values.addAll((List) universe);
        }
        return values;
    }

    @Override
    public ReportRequestExecutorI18nContext i18nContext() {
        return i18nContext;
    }

    @Override
    public Function<Object, String> cellValueFormatter() {
        return this::convertToString;
    }

    @Override
    public String getReferentialLabel(String id) {
        I18nReferentialEntity referential = Objects.requireNonNull(persistenceContext.findByTopiaId(id), "Could not find referential with id; " + id);
        return referential.getLabel(i18nContext().getReferentialLocale());
    }

    public TopiaDaoSupplier getDaoSupplier() {
        return persistenceContext;
    }

    private String convertToString(Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof Time) {
            return timeFormatter.format(value);
        } else if (value instanceof Date) {
            return dateFormatter.format(value);
        } else if (value instanceof Timestamp) {
            return timestampFormatter.format(value);
        }
        return value.toString();
    }
}
