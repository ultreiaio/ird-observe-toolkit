package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 24/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.18
 */
public class EntitySerializerContext {
    private static final Logger log = LogManager.getLogger(EntitySerializerContext.class);

    private final TopiaApplicationContext<?> applicationContext;
    private final Gson gson;
    private final ToolkitRequestConfig config;
    private final Multimap<String, String> skipModelNavigation;
    private final Map<String, JsonElement> referentialExplored;
    private final LinkedList<JsonObject> stack;
    private final Set<String> explored;
    private final Set<String> skipModelNavigationForContext;
    private JsonObject currentObject;

    protected EntitySerializerContext(ToolkitRequestConfig config, TopiaApplicationContext<?> applicationContext, Multimap<String, String> skipModelNavigation, Gson gson, Map<String, JsonElement> referentialExplored, Set<String> explored) {
        this.config = config;
        this.applicationContext = applicationContext;
        this.skipModelNavigation = skipModelNavigation;
        this.skipModelNavigationForContext = new LinkedHashSet<>();
        this.gson = gson;
        this.referentialExplored = referentialExplored;
        this.stack = new LinkedList<>();
        this.explored = explored;
    }

    public EntitySerializerContext(ToolkitRequestConfig config, TopiaApplicationContext<?> applicationContext, Supplier<Gson> gsonSupplier) {
        this.config = config;
        this.gson = gsonSupplier.get();
        this.applicationContext = applicationContext;
        this.stack = new LinkedList<>();
        this.explored = new LinkedHashSet<>();
        this.referentialExplored = new TreeMap<>();
        this.skipModelNavigation = applicationContext.getPersistenceTagValues().getAttributeStereotypes("skipModelNavigation");
        this.skipModelNavigationForContext = new LinkedHashSet<>();
    }

    public EntitySerializerContext sub() {
        return new EntitySerializerContext(config, applicationContext, skipModelNavigation, gson, referentialExplored, explored);
    }

    public Gson getGson() {
        return gson;
    }

    public boolean isLoadReferential() {
        return config.isLoadReferential();
    }

    public boolean isRecurse() {
        return config.isRecursive();
    }

    public boolean isSerializeNulls() {
        return config.isSerializeNulls();
    }

    public Map<String, JsonElement> getReferentialExplored() {
        return referentialExplored;
    }

    public LinkedList<JsonObject> getStack() {
        return stack;
    }

    public Set<String> getExplored() {
        return explored;
    }

    public JsonObject getCurrentObject() {
        return currentObject;
    }

    public String toJson(JsonElement result) {
        return gson.toJson(result);
    }

    public void start(TopiaEntity entity) {
        Class<? extends TopiaEntity> contractClass;
        skipModelNavigationForContext.clear();
        try {
            contractClass = entity.contractType();
            String contractClassName = contractClass.getName();
            skipModelNavigationForContext.addAll(skipModelNavigation.get(contractClassName));
        } catch (Exception e) {
            log.error(String.format("Can't find out type of entity: %s", entity));
        }
        getExplored().add(entity.getTopiaId());
        stack.add(currentObject = new JsonObject());
    }

    public void end() {
        currentObject = stack.removeLast();
    }

    public void visitReferential(String propertyName, ReferentialEntity data) {
        String id = data.getTopiaId();
        currentObject.addProperty(propertyName, id);
        if (isLoadReferential() && !getReferentialExplored().containsKey(id)) {
            referentialExplored.put(id, subDataToJson(data));
        }
    }

    public void visitData(String propertyName, DataEntity data) {
        String id = data.getTopiaId();
        if (skipModelNavigationForContext.contains(propertyName) || explored.contains(id)) {
            // just a reference to another data
            currentObject.addProperty(propertyName, id);
            return;
        }
        if (isRecurse()) {
            currentObject.add(propertyName, subDataToJson(data));
        }
    }

    public void visitReferentialCollection(String propertyName, Collection<?> collection, JsonArray array) {
        for (Object o : collection) {
            ReferentialEntity data = (ReferentialEntity) o;
            if (propertyName == null) {
                array.add(subDataToJson(data));
                continue;
            }
            String id = data.getTopiaId();
            array.add(id);
            if (isLoadReferential() && !referentialExplored.containsKey(id)) {
                referentialExplored.put(id, subDataToJson(data));
            }
        }
        visitCollectionEnd(propertyName, array);
    }

    public void visitDataCollection(String propertyName, Collection<?> collection, JsonArray array) {
        boolean skipThisData = skipModelNavigationForContext.contains(propertyName);
        for (Object o : collection) {
            DataEntity data = (DataEntity) o;
            if (data == null) {
                continue;
            }
            if (propertyName == null) {
                array.add(subDataToJson(data));
                continue;
            }
            String id = data.getTopiaId();
            if (skipThisData || explored.contains(id)) {
                // just a reference to another data
                array.add(id);
                continue;
            }
            array.add(subDataToJson(data));
        }
        visitCollectionEnd(propertyName, array);
    }

    public void visitCollection(String propertyName, Collection<?> collection, JsonArray array) {
        for (Object o : collection) {
            JsonElement id = gson.toJsonTree(o);
            array.add(id);
        }
        visitCollectionEnd(propertyName, array);
    }

    public void visitNullCollection(String propertyName) {
        visitCollectionEnd(propertyName, new JsonArray(0));
    }

    public void visit(String propertyName, Class<?> type, Object value) {
        JsonElement serialize = gson.toJsonTree(value, type);
        currentObject.add(propertyName, serialize);
    }

    public void visitNull(String propertyName) {
        currentObject.add(propertyName, JsonNull.INSTANCE);
    }

    protected void visitCollectionEnd(String propertyName, JsonArray array) {
        if (propertyName != null) {
            currentObject.add(propertyName, array);
        }
    }

    protected EntitySerializerResult jsonArrayToResult(int size, BiConsumer<EntitySerializerContext, JsonArray> consumer) {
        JsonArray list = new JsonArray(size);
        if (consumer != null) {
            consumer.accept(this, list);
        }
        JsonArray references = flushReferencesToJson();
        return new EntitySerializerResult(toJson(list), references == null ? null : toJson(references));
    }

    protected EntitySerializerResult jsonObjectToResult(Function<EntitySerializerContext, JsonObject> data) {
        JsonObject content = data.apply(this);
        JsonArray references = flushReferencesToJson();
        return new EntitySerializerResult(toJson(content), references == null ? null : toJson(references));
    }

    protected JsonObject subDataToJson(Entity data) {
        EntitySerializerContext sub = sub();
        EntitySerializerVisitor visitor = new EntitySerializerVisitor(sub);
        return sub.dataToJson(visitor, data);
    }

    protected JsonObject dataToJson(Entity data) {
        EntitySerializerVisitor visitor = new EntitySerializerVisitor(this);
        return dataToJson(visitor, data);
    }

    protected JsonObject dataToJson(TopiaEntityVisitor visitor, Entity data) {
        data.accept(visitor);
        return getCurrentObject();
    }

    protected JsonArray flushReferencesToJson() {
        if (isLoadReferential() && !referentialExplored.isEmpty()) {
            JsonArray referencesArray = new JsonArray(referentialExplored.size());
            referentialExplored.values().forEach(referencesArray::add);
            return referencesArray;
        }
        return null;
    }
}
