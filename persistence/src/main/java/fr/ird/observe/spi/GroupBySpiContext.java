package fr.ird.observe.spi;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.DataGroupByValue;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataGroupByEntity;
import fr.ird.observe.entities.data.DataGroupByEntityDefinition;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModelWriter;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.context.DataGroupByReferentialHelper;
import fr.ird.observe.spi.context.RootOpenableDtoEntityContext;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuildChildrenInterceptor;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.decoration.Decorator;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 13/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GroupBySpiContext<D extends RootOpenableDto,
        R extends DataDtoReference,
        E extends RootOpenableEntity,
        T extends TopiaDao<E>,
        H extends DataGroupByReferentialHelper<D, R, E>,
        F extends DataGroupByEntity<E>,
        FF extends DataGroupByDto<D>
        > {

    public final DataGroupByEntityDefinition<E, F, D, FF> definition;
    public final RootOpenableDtoEntityContext<D, R, E, T, H> spi;

    public GroupBySpiContext(DataGroupByEntityDefinition<E, F, D, FF> definition, RootOpenableDtoEntityContext<D, R, E, T, H> spi) {
        this.definition = definition;
        this.spi = spi;
    }

    public DataGroupByEntityDefinition<E, F, D, FF> definition() {
        return definition;
    }

    public final void loadFlatModel(ServiceContext context, ToolkitTreeFlatModelRootRequest request, boolean loadData, ToolkitTreeFlatModelWriter builder, boolean canWriteData) {

        String groupByFlavor = request.getGroupByFlavor();

        H referentialHelper = spi.createReferentialHelper(context);
        List<DataGroupByValue> groupCountValues = spi.findGroupCountValues(context, definition, request, referentialHelper);
        Decorator decorator = referentialHelper.decoratorService().getDecoratorByType(definition().getContainerType());
        String globalPrefix = definition.getName() + ":";
        for (DataGroupByValue groupCountValue : groupCountValues) {
            F form = definition.newForm(groupCountValue);
            String id = form.getFilterValue();
            String prefix = globalPrefix;
            addGroupEntity(builder, prefix, form, canWriteData, loadData, decorator, groupByFlavor);
            if (loadData) {
                prefix += id;
                List<E> children = spi.findGroupCountValueChildren(context, definition, form.getFilterValue(), groupByFlavor, referentialHelper);
                form.setChildren(children);
                for (RootOpenableEntity r : children) {
                    addEntity(builder, prefix, r, true);
                }
            }
        }
    }

    public final DataGroupByDtoSet<D, FF> loadNavigationResult(ServiceContext context, ToolkitTreeFlatModelRootRequest request, Date timestamp) {
        H referentialHelper = spi.createReferentialHelper(context);
        List<DataGroupByValue> groupCountValues = spi.findGroupCountValues(context, definition, request, referentialHelper);
        List<FF> result = new ArrayList<>(groupCountValues.size());
        DataGroupByDtoDefinition<D, FF> dtoDefinition = definition.dtoDefinition();
        for (DataGroupByValue groupCountValue : groupCountValues) {
            FF dto = dtoDefinition.newForm(groupCountValue);
            result.add(dto);
        }
        return DataGroupByDtoSet.of(dtoDefinition.getContainerType(), dtoDefinition.getPropertyType(), result, timestamp);
    }

    public F getGroupByValue(ServiceContext context, boolean loadData, String groupByValue, String groupByFlavor) {
        H referentialHelper = spi.createReferentialHelper(context);
        DataGroupByValue groupCountValue = spi.findGroupCountValue(context, definition, groupByValue, groupByFlavor, referentialHelper);
        F form = definition.newForm(groupCountValue);
        if (loadData) {
            List<E> children = spi.findGroupCountValueChildren(context, definition, form.getFilterValue(), groupByFlavor, referentialHelper);
            form.setChildren(children);
        }
        return form;
    }

    public String getGroupByValue(ServiceContext context, String groupByFlavor, String dataId) {
        //FIXME generate a new request (+ one more for each flavor)
        E entity = spi.loadEntity(context, dataId);
        return definition.toGroupByValue(entity, groupByFlavor);
    }

    public final Form<D> preCreate(ServiceContext context, String groupByFlavor, String groupByValue) {
        Object groupByEntityValue = definition.getGroupByEntityValue(context, groupByFlavor, groupByValue);
        E preCreated = spi.newEntity(context.now());
        if (groupByEntityValue != null) {
            // value is a referential id
            definition.setGroupByValue(preCreated, groupByEntityValue);
        }
        return spi.preCreate(context, preCreated);
    }

    public List<ToolkitIdLabel> getBrothersLabel(ServiceContext context, String parentId, String groupByFlavor, String id) {
        H referentialHelper = spi.createReferentialHelper(context);
        List<R> children = spi.getChildrenLabel(context, definition, parentId, groupByFlavor, id, referentialHelper);
        return spi.toReferenceLabelList(context.getApplicationLocale(), children.stream());
    }

    public final DataDtoReferenceSet<R> getChildren(ServiceContext context, String parentId, String groupByFlavor) {
        H referentialHelper = spi.createReferentialHelper(context);
        List<R> children = spi.getChildrenReference(context, definition, parentId, groupByFlavor, referentialHelper);
        return DataDtoReferenceSet.of(spi.toReferenceType(), children, context.now());
    }

    public final FF getGroupByDtoValue(ServiceContext context, String groupByValue, String groupByFlavor) {
        H referentialHelper = spi.createReferentialHelper(context);
        DataGroupByValue groupCountValue = spi.findGroupCountValue(context, definition(), groupByValue, groupByFlavor, referentialHelper);
        return definition().dtoDefinition().newForm(groupCountValue);
    }

    public UpdatedDataDtoReferenceSet<R> getChildrenUpdate(ServiceContext context, String parentId, String groupByFlavor, Date lastUpdate) {
        H referentialHelper = spi.createReferentialHelper(context);
        List<R> references = spi.getChildrenReference(context, definition, parentId, groupByFlavor, referentialHelper);
        Set<String> allIds = references.stream().map(DataDtoReference::getTopiaId).collect(Collectors.toSet());
        if (lastUpdate != null) {
            long time = lastUpdate.getTime();
            references = references.stream().filter(t -> t.getLastUpdateDate().getTime() > time).collect(Collectors.toList());
        }
        return new UpdatedDataDtoReferenceSet<>(allIds, DataDtoReferenceSet.of(spi.toReferenceType(), references, context.now()));
    }

    private void addGroupEntity(ToolkitTreeFlatModelWriter builder, String prefix, DataGroupByEntity<?> entity, boolean canWriteData, boolean loadData, Decorator decorator, String groupByFlavor) {
        ToolkitTreeNodeStates states = addEntity(builder, prefix, entity, false);
        ToolkitTreeNodeBean.STATE_COUNT.setValue(states, (int) entity.getCount());
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(states, entity.isEnabled());
        ToolkitTreeNodeBean.STATE_ID.setValue(states, entity.getFilterValue());
        ToolkitTreeNodeBean.STATE_FILTER_FLAVOR.setValue(states, groupByFlavor);
        String filterText = entity.getFilterText();
        if (loadData) {
            entity.registerDecorator(decorator);
            filterText = entity.toString();
        }
        ToolkitTreeNodeBean.STATE_TEXT.setValue(states, filterText);
        ToolkitTreeNodeBean.STATE_EDITABLE.setValue(states, canWriteData);
    }

    private ToolkitTreeNodeStates addEntity(ToolkitTreeFlatModelWriter builder, String prefix, Entity entity, boolean addText) {
        ToolkitTreeNodeStates states = builder.addId(prefix, entity);
        states.addState(ToolkitTreeNodeBuildChildrenInterceptor.$$_DATA, entity);
        if (addText) {
            //FIXME Should use the ToolkitIdLabel decorator see ToolkitIdLabel.of
            ToolkitTreeNodeBean.STATE_TEXT.setValue(states, entity.toString());
        }
        return states;
    }

    public long getDataCount(ServiceContext context) {
        return spi.getDao(context).count();
    }
}
