package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Mapping dto → dtoEntityContext.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5
 */
public abstract class DtoToEntityContextMapping extends ImmutableClassMapping<BusinessDto, Entity> {

    private final Map<Class<? extends BusinessDto>, DtoEntityContext<?, ?, ?, ?>> mapping;

    private static Map<Class<? extends BusinessDto>, Class<? extends Entity>> mapping(Map<Class<? extends BusinessDto>, DtoEntityContext<?, ?, ?, ?>> mapping) {
        Map<Class<? extends BusinessDto>, Class<? extends Entity>> builder = new LinkedHashMap<>();
        mapping.forEach((key, value) -> builder.put(key, value.toEntityType()));
        return Collections.unmodifiableMap(builder);
    }

    protected DtoToEntityContextMapping(Map<Class<? extends BusinessDto>, DtoEntityContext<?, ?, ?, ?>> mapping) {
        super(mapping(mapping));
        this.mapping = mapping;
    }

    @SuppressWarnings("unchecked")
    public <D extends BusinessDto, R extends DtoReference, E extends Entity, T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> getContext(Class<D> key) {
        return (DtoEntityContext<D, R, E, T>) mapping.get(key);
    }

    public <D extends BusinessDto, R extends DtoReference, E extends Entity, T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromDto(D dto) {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        return fromDto(dtoType);
    }

    public <D extends BusinessDto, R extends DtoReference, E extends Entity, T extends TopiaDao<E>> DtoEntityContext<D, R, E, T> fromDto(Class<D> dtoType) {
        return getContext(dtoType);
    }

    public <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity, T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(D dto) {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        return referential0(dtoType);
    }

    public <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity, T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> fromReferentialDto(Class<D> dtoType) {
        return referential0(dtoType);
    }

    public <D extends DataDto, R extends DataDtoReference, E extends DataEntity, T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(D dto) {
        @SuppressWarnings("unchecked") Class<D> dtoType = (Class<D>) dto.getClass();
        return data0(dtoType);
    }

    public <D extends DataDto, R extends DataDtoReference, E extends DataEntity, T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> fromDataDto(Class<D> dtoType) {
        return data0(dtoType);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity, T extends TopiaDao<E>> ReferentialDtoEntityContext<D, R, E, T> referential0(Class<D> dtoType) {
        return (ReferentialDtoEntityContext) this.<D, R, E, T>getContext(dtoType);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private <D extends DataDto, R extends DataDtoReference, E extends DataEntity, T extends TopiaDao<E>> DataDtoEntityContext<D, R, E, T> data0(Class<D> dtoType) {
        return (DataDtoEntityContext) this.<D, R, E, T>getContext(dtoType);
    }

}
