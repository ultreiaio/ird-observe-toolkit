package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataGroupByEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * To transform a entity to a flat map model, with only
 * Created on 23/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.18
 */
public class EntitySerializer {

    private final TopiaApplicationContext<?> applicationContext;
    private final Supplier<Gson> gsonSupplier;
    private final ToolkitRequestConfig config;

    public static synchronized EntitySerializerResult serialize(ToolkitRequestConfig config, TopiaApplicationContext<?> applicationContext, Supplier<Gson> gsonSupplier, Entity entity) {
        EntitySerializer serializer = new EntitySerializer(config, applicationContext, gsonSupplier);
        return serializer.serialize(entity);
    }

    public static synchronized EntitySerializerResult serializeList(ToolkitRequestConfig config, TopiaApplicationContext<?> applicationContext, Supplier<Gson> gsonSupplier, Collection<?> entity) {
        EntitySerializer serializer = new EntitySerializer(config, applicationContext, gsonSupplier);
        return serializer.serializeList(entity);
    }

    public EntitySerializer(ToolkitRequestConfig config, TopiaApplicationContext<?> applicationContext, Supplier<Gson> gsonSupplier) {
        this.config = Objects.requireNonNull(config);
        this.applicationContext = applicationContext;
        this.gsonSupplier = Objects.requireNonNull(gsonSupplier);
    }

    public EntitySerializerResult serialize(Object data) {
        Objects.requireNonNull(data);
        if (data instanceof ReferentialEntity) {
            return newContext().jsonObjectToResult(c -> c.dataToJson((Entity) data));
        }
        if (data instanceof DataEntity) {
            return newContext().jsonObjectToResult(c -> c.dataToJson((Entity) data));
        }
        if (data instanceof DataGroupByEntity<?>) {
            return newContext().jsonObjectToResult(c -> c.dataToJson((Entity) data));
        }
        throw new IllegalStateException(String.format("Can't manage: %s", data));
    }

    public EntitySerializerResult serializeList(Collection<?> data) {
        if (Objects.requireNonNull(data).isEmpty()) {
            return newContext().jsonArrayToResult(0, null);
        }
        Class<?> type = data.iterator().next().getClass();
        if (ReferentialEntity.class.isAssignableFrom(type)) {
            return newContext().jsonArrayToResult(data.size(), (c, l) -> c.visitReferentialCollection(null, data, l));
        }
        if (DataEntity.class.isAssignableFrom(type)) {
            return newContext().jsonArrayToResult(data.size(), (c, l) -> c.visitDataCollection(null, data, l));
        }
        if (DataGroupByEntity.class.isAssignableFrom(type)) {
            return newContext().jsonArrayToResult(data.size(), (c, l) -> c.visitDataCollection(null, data, l));
        }
        throw new IllegalStateException(String.format("Can't manage: %s", type));
    }

    private EntitySerializerContext newContext() {
        return new EntitySerializerContext(config, applicationContext, gsonSupplier);
    }
}
