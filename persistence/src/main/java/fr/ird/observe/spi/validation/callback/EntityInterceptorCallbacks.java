package fr.ird.observe.spi.validation.callback;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.context.DtoEntityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * Created at 21/05/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.5
 */
public final class EntityInterceptorCallbacks {
    private static final Logger log = LogManager.getLogger(EntityInterceptorCallbacks.class);

    private final List<EntityInterceptorCallback<?, ?, ?>> callbacks;

    public EntityInterceptorCallbacks() {
        log.info("Initializing... {}", this);
        callbacks = new LinkedList<>();
        for (EntityInterceptorCallback<?, ?, ?> entityInterceptorCallback : ServiceLoader.load(EntityInterceptorCallback.class)) {
            callbacks.add(entityInterceptorCallback);
        }
        callbacks.add(new OpenableEntityInterceptorCallback<>());
        callbacks.add(new EdiableEntityInterceptorCallback<>());
        callbacks.add(new DefaultEntityInterceptorCallback<>());
        log.info("Initialization done {}, register {} EntityInterceptorCallback(s).", this, callbacks.size());
    }

    @SuppressWarnings("unchecked")
    public <D extends BusinessDto, E extends Entity, S extends DtoEntityContext<D, ?, E, ?>> EntityInterceptorCallback<D, E, S> getCallback(Class<D> dtoType, Class<E> entityType) {
        return (EntityInterceptorCallback<D, E, S>) callbacks.stream().filter(entityInterceptorCallback -> entityInterceptorCallback.acceptType(dtoType, entityType)).findFirst().orElseThrow(() -> new IllegalStateException(String.format("Can't find callback for dto type: %s and entity type: %s", dtoType, entityType)));
    }
}
