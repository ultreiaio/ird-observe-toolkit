package fr.ird.observe.spi.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.navigation.bean.OpenNavigationTreeNodeBean;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuildChildrenInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * Created on 11/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class NavigationTreeNodeChildrenBuilderSupport extends ToolkitTreeNodeBuildChildrenInterceptor {
    private static final Logger log = LogManager.getLogger(NavigationTreeNodeChildrenBuilderSupport.class);

    private final boolean canWriteData;
    protected Path nodePath;

    public NavigationTreeNodeChildrenBuilderSupport(boolean recursive, boolean canWriteData) {
        super(recursive);
        this.canWriteData = canWriteData;
    }

    protected abstract String decorate(DataEntity data);

    public final void build(ToolkitTreeNode node, Object rootData) {
        this.nodePath = node.getNodePath();
        log.info(String.format("Build [%s] children.", node.getNodePath()));
        node.getUserObject().addTransientState($$_DATA, rootData);
        intercept(node);
    }

    protected final void intercept0(ToolkitTreeNode node, Consumer<ToolkitTreeNode> defaultIntercept) {
        if (node.getNodePath().equals(nodePath) || recursive) {
            log.debug(String.format("Intercept %s, will fill his node.", node.getUserObject().getPath()));
            defaultIntercept.accept(node);
        }
    }

    @Override
    protected <N extends ToolkitTreeNode> void addRootOpen(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        interceptEntity(childrenNode, (DataEntity) data);
        super.addRootOpen(node, data, enabled, childrenNode);
    }

    @Override
    protected <N extends ToolkitTreeNode> void addOpen(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        interceptEntity(childrenNode, (DataEntity) data);
        super.addOpen(node, data, enabled, childrenNode);
    }

    @Override
    protected <N extends ToolkitTreeNode> void addEdit(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        interceptEntity(childrenNode, (DataEntity) data);
        super.addEdit(node, data, enabled, childrenNode);
    }

    protected void interceptEntity(ToolkitTreeNode node, DataEntity data) {
        ToolkitTreeNodeBean userObject = node.getUserObject();
        userObject.copy(data);
        //FIXME Should use the ToolkitIdLabel decorator see ToolkitIdLabel.of
        String text = decorate(data);
        OpenNavigationTreeNodeBean.STATE_TEXT.setValue(userObject, text);
        ToolkitTreeNodeBean.STATE_EDITABLE.setValue(userObject, canWriteData);
    }
}
