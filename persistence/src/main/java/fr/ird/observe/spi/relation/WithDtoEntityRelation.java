package fr.ird.observe.spi.relation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Created on 19/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public interface WithDtoEntityRelation<
        PD extends BusinessDto,
        D extends BusinessDto,
        PE extends Entity,
        E extends Entity> extends WithParent {

    DtoEntityRelation<PD, D, PE, E> relation();

    @Override
    default ToolkitParentIdDtoBean getParentId(ToolkitTopiaPersistenceContextSupport persistenceContext, String id) {
        return persistenceContext.executeQueryGetParentId(relation(), id);
    }

    @Override
    default Stream<String> byParentId(ToolkitTopiaPersistenceContextSupport persistenceContext, Collection<String> id) {
        return persistenceContext.executeQueryByParentId(relation(), id);
    }

    default void loadDtoForValidation(ServiceContext context, PE parent, E entity, D dto) {
        // by default nothing to do
    }
}
