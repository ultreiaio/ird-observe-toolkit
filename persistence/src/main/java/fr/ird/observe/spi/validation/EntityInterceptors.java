package fr.ird.observe.spi.validation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.NotEntityDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.id.IdProject;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.validation.callback.EntityInterceptorCallback;
import fr.ird.observe.spi.validation.callback.EntityInterceptorCallbacks;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.validation.bean.BeanValidator;
import io.ultreia.java4all.validation.bean.NuitonValidatorsMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class EntityInterceptors {
    private static final Logger log = LogManager.getLogger(EntityInterceptors.class);

    private static final EntityInterceptorCallbacks CALLBACKS = new EntityInterceptorCallbacks();

    public static Map<Class<?>, EntityInterceptor<?, ?, ?>> forReferential(NuitonValidatorsMap validators,
                                                                        ValidationContextSupport validationDataContext,
                                                                        ReferentialLocale referentialLocale) {
        Map<Class<?>, EntityInterceptor<?, ?, ?>> result = new LinkedHashMap<>();
        Set<Class<?>> possibleTypes = new LinkedHashSet<>(validators.keySet());
        possibleTypes.removeIf(t -> NotEntityDto.class.isAssignableFrom(t) || DataDto.class.isAssignableFrom(t));
        DecoratorService decoratorService = validationDataContext.getDecoratorService();
        for (Class<?> possibleType : possibleTypes) {
            toReferential((Class) possibleType, result, validators, referentialLocale, decoratorService);
        }
        return result;
    }

    public static Map<Class<?>, EntityInterceptor<?, ?, ?>> forData(NuitonValidatorsMap validators,
                                                                 ValidationContextSupport validationDataContext,
                                                                 IdProject dataContext,
                                                                 ReferentialLocale referentialLocale) {

        Map<Class<?>, EntityInterceptor<?, ?, ?>> result = new LinkedHashMap<>();

        Set<Class<?>> possibleTypes = new LinkedHashSet<>(validators.keySet());
        possibleTypes.removeIf(t -> NotEntityDto.class.isAssignableFrom(t) || ReferentialDto.class.isAssignableFrom(t));
        DecoratorService decoratorService = validationDataContext.getDecoratorService();
        for (IdModel model : dataContext.getModels()) {
            for (IdNode<?> node : model.getNodes()) {
                Class<?> nodeType = node.getType();
                if (possibleTypes.contains(nodeType)) {
                    toData((IdNode) node, result, validators, referentialLocale, decoratorService);
                    log.debug(String.format("Add data type: %s", nodeType.getName()));
                    possibleTypes.remove(nodeType);
                }
            }
        }

//        for (IdNode<?> node : dataContext.getPs().getNodes()) {
//            Class<?> nodeType = node.getType();
//            if (possibleTypes.contains(nodeType)) {
//                toData((IdNode) node, result, validators, referentialLocale, decoratorService);
//                log.debug(String.format("Add data type: %s", nodeType.getName()));
//                possibleTypes.remove(nodeType);
//            }
//        }
//        for (IdNode<?> node : dataContext.getLl().getNodes()) {
//            Class<?> nodeType = node.getType();
//            if (possibleTypes.contains(nodeType)) {
//                toData((IdNode) node, result, validators, referentialLocale, decoratorService);
//                log.debug(String.format("Add data type: %s", nodeType.getName()));
//                possibleTypes.remove(nodeType);
//            }
//        }
        for (Class<?> possibleType : possibleTypes) {
            toData((Class) possibleType, null, result, validators, referentialLocale, decoratorService);
        }
        return result;
    }

    static <D extends ReferentialDto> void toReferential(Class<D> dtoType,
                                                         Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptorsBuilder,
                                                         NuitonValidatorsMap validators,
                                                         ReferentialLocale referentialLocale,
                                                         DecoratorService decoratorService) {
        BeanValidator<D> validator = validators.getValidator(dtoType);
        ReferentialDtoEntityContext<D, ?, ReferentialEntity, ?> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
        Class<ReferentialEntity> entityType = spi.toEntityType();
        Decorator decorator = decoratorService.getDecoratorByType(entityType);
        EntityInterceptorCallback<D, ReferentialEntity, ReferentialDtoEntityContext<D, ?, ReferentialEntity, ?>> callback = CALLBACKS.getCallback(dtoType, entityType);
        EntityInterceptor<D, ?, ?> interceptor = new EntityInterceptor<>(spi, null, validator, referentialLocale, decorator, callback);
        interceptorsBuilder.put(entityType, interceptor);
    }

    static <D extends DataDto, E extends DataEntity> void toData(Class<D> dtoType,
                                                                 IdNode<D> consumer,
                                                                 Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptorsBuilder,
                                                                 NuitonValidatorsMap validators,
                                                                 ReferentialLocale referentialLocale,
                                                                 DecoratorService decoratorService) {
        BeanValidator<D> validator = validators.getValidator(dtoType);
        if (validator == null) {
            log.warn(String.format("Not a main type (%s), skip it", dtoType));
            return;
        }

        DataDtoEntityContext<D, ?, E, ?> spi = PersistenceBusinessProject.fromDataDto(dtoType);
        Class<E> entityType = spi.toEntityType();
        Objects.requireNonNull(entityType);
        if (interceptorsBuilder.containsKey(entityType)) {
            return;
        }
        Decorator decorator = decoratorService.getDecoratorByType(entityType);
        EntityInterceptorCallback<D, E, DataDtoEntityContext<D, ?, E, ?>> callback = CALLBACKS.getCallback(dtoType, entityType);
        EntityInterceptor<D, E, DataDtoEntityContext<D, ?, E, ?>> interceptor = new EntityInterceptor<>(spi, consumer == null ? null : consumer::setId, validator, referentialLocale, decorator, callback);
        interceptorsBuilder.put(entityType, interceptor);
    }

    static <D extends DataDto> void toData(IdNode<D> consumer,
                                           Map<Class<?>, EntityInterceptor<?, ?, ?>> interceptorsBuilder,
                                           NuitonValidatorsMap validators,
                                           ReferentialLocale referentialLocale,
                                           DecoratorService decoratorService) {
        toData(consumer == null ? null : consumer.getType(),
               consumer,
               interceptorsBuilder,
               validators,
               referentialLocale,
               decoratorService);
    }
}
