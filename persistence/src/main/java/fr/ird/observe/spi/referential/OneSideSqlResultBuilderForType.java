package fr.ird.observe.spi.referential;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTask;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.service.sql.script.EntitySqlScript;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 16/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class OneSideSqlResultBuilderForType<D extends ReferentialDto, E extends ReferentialEntity> {
    private static final Logger log = LogManager.getLogger(OneSideSqlResultBuilderForType.class);

    private final ServiceContext context;
    private final ReferentialDtoEntityContext<D, ?, E, ?> spi;
    private final Class<D> dtoType;
    private final OneSideSqlResult.Builder resultBuilder;
    private final Date defaultLastUpdateDate;

    public OneSideSqlResultBuilderForType(ServiceContext context,
                                          ReferentialDtoEntityContext<D, ?, E, ?> spi,
                                          OneSideSqlResult.Builder resultBuilder,
                                          Date defaultLastUpdateDate) {
        this.context = Objects.requireNonNull(context);
        this.spi = Objects.requireNonNull(spi);
        this.dtoType = Objects.requireNonNull(spi.toDtoType());
        this.resultBuilder = Objects.requireNonNull(resultBuilder);
        this.defaultLastUpdateDate = Objects.requireNonNull(defaultLastUpdateDate);
    }

    public void build(OneSideSqlRequest request) {
        List<SynchronizeTask> addTasks = request.getAddTasks(dtoType);
        if (!addTasks.isEmpty()) {
            log.info(String.format("Process %d Add task(s).", addTasks.size()));
            onAdd(addTasks);
        }
        List<SynchronizeTask> updateTasks = request.getUpdateTasks(dtoType);
        if (!updateTasks.isEmpty()) {
            log.info(String.format("Process %d Update task(s).", updateTasks.size()));
            onUpdate(updateTasks);
        }
        List<SynchronizeTask> revertTasks = request.getRevertTasks(dtoType);
        if (!revertTasks.isEmpty()) {
            log.info(String.format("Process %d Revert task(s).", revertTasks.size()));
            onRevert(revertTasks);
        }
        List<SynchronizeTask> deleteTasks = request.getDeleteTasks(dtoType);
        if (!deleteTasks.isEmpty()) {
            log.info(String.format("Process %d Delete task(s).", deleteTasks.size()));
            onDelete(deleteTasks);
        }
        List<SynchronizeTask> deactivateTasks = request.getDeactivateTasks(dtoType);
        if (!deactivateTasks.isEmpty()) {
            log.info(String.format("Process %d Deactivate task(s).", deactivateTasks.size()));
            onDeactivate(deactivateTasks);
        }
        List<SynchronizeTask> deactivateWithReplacementTasks = request.getDeactivateWithReplacementTasks(dtoType);
        if (!deactivateWithReplacementTasks.isEmpty()) {
            log.info(String.format("Process %d DeactivateWithReplacement task(s).", deactivateWithReplacementTasks.size()));
            onDeactivateWithReplacement(deactivateWithReplacementTasks);
        }
    }

    public void onAdd(List<SynchronizeTask> tasks) {
        List<E> insertEntities = loadEntities(tasks);
        // remove all ids we will add here
        Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide = resultBuilder.idsOnlyExistingOnThisSide();
        Set<String> idsOnlyOnThisSide = idsOnlyExistingOnThisSide.get(spi.toDtoType());
        //FIXME If one of entities was already added by missingReferential on other entity, this may produce duplicate sql code ?
        if (idsOnlyOnThisSide != null && !idsOnlyExistingOnThisSide.isEmpty()) {
            insertEntities.forEach(e -> idsOnlyOnThisSide.remove(e.getTopiaId()));
        }
        ReferentialExtraScripts<E> copyScript = spi.getExtraScripts();
        Optional<String> optionalRecursiveProperty = copyScript.getOptionalRecursiveProperty();
        if (optionalRecursiveProperty.isPresent()) {
            // must sort entities with recursive property in correct order
            insertEntities = ReferentialEntity.reorder(insertEntities, optionalRecursiveProperty.get());
        }
        for (E entity : insertEntities) {
            List<String> sqlList = copyScript.generateCopyScript(context.getTopiaPersistenceContext(), entity, idsOnlyExistingOnThisSide);
            EntitySqlScript.appendEol(sqlList.stream()).forEach(resultBuilder::addInsertStatement);
        }
    }

    public void onUpdate(List<SynchronizeTask> tasks) {
        Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide = resultBuilder.idsOnlyExistingOnThisSide();
        ReferentialExtraScripts<E> updateScript = spi.getExtraScripts();
        for (SynchronizeTask task : tasks) {
            E entity = loadEntity(task);
            Set<String> optionalIncludedPropertyNames = task.getOptionalIncludedPropertyNames().orElse(null);
            Date lastUpdateDate = date(task.getLastUpdateDate());
            List<String> sqlList = updateScript.generateUpdateScript(context.getTopiaPersistenceContext(), entity, optionalIncludedPropertyNames, lastUpdateDate, idsOnlyExistingOnThisSide);
            EntitySqlScript.appendEol(sqlList.stream()).forEach(resultBuilder::addUpdateStatement);
        }
    }

    public void onRevert(List<SynchronizeTask> tasks) {
        Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide = resultBuilder.idsOnlyExistingOnThisSide();
        ReferentialExtraScripts<E> updateScript = spi.getExtraScripts();
        for (SynchronizeTask task : tasks) {
            E entity = loadEntity(task);
            Set<String> optionalIncludedPropertyNames = task.getOptionalIncludedPropertyNames().orElse(null);
            Date lastUpdateDate = date(task.getLastUpdateDate());
            List<String> sqlList = updateScript.generateUpdateScript(context.getTopiaPersistenceContext(), entity, optionalIncludedPropertyNames, lastUpdateDate, idsOnlyExistingOnThisSide);
            EntitySqlScript.appendEol(sqlList.stream()).forEach(resultBuilder::addUpdateStatement);
        }
    }

    public void onDelete(List<SynchronizeTask> tasks) {
        for (SynchronizeTask task : tasks) {
            String id = task.getReferentialId();
            Date lastUpdateDate = date(task.getLastUpdateDate());
            Optional<String> replaceReferentialId = task.getOptionalReplaceReferentialId();
            replaceReferentialId.flatMap(s -> spi.generateReplaceScript(id, s, lastUpdateDate, true, true)).ifPresent(script -> script.getLocation().forEach(resultBuilder::addDeactivateStatement));
            List<String> sqlList = spi.getDeleteScript().generate(id);
            sqlList.forEach(resultBuilder::addDeleteStatement);
        }
    }

    public void onDeactivate(List<SynchronizeTask> tasks) {
        for (SynchronizeTask task : tasks) {
            String id = task.getReferentialId();
            Optional<String> replaceReferentialId = task.getOptionalReplaceReferentialId();
            if (replaceReferentialId.isPresent()) {
                throw new IllegalStateException("Can't have a simple deactivate task with a replace to do... Use deactivateWithReplacement task instead.");
            }
            Date lastUpdateDate = date(task.getLastUpdateDate());
            List<String> sqlList = spi.getDisableScript().generate(id, lastUpdateDate);
            sqlList.forEach(resultBuilder::addDeactivateStatement);
        }
    }

    public void onDeactivateWithReplacement(List<SynchronizeTask> tasks) {
        for (SynchronizeTask task : tasks) {
            String id = task.getReferentialId();
            Date lastUpdateDate = date(task.getLastUpdateDate());
            Optional<String> replaceReferentialId = task.getOptionalReplaceReferentialId();
            if (replaceReferentialId.isEmpty()) {
                throw new IllegalStateException("Can't have a deactivate task with replacement without replacement id... Use deactivate task instead.");
            }
            replaceReferentialId.flatMap(s -> spi.generateReplaceScript(id, s, lastUpdateDate, true, true)).ifPresent(script -> script.getLocation().forEach(resultBuilder::addDeactivateStatement));
            List<String> sqlList = spi.getDisableScript().generate(id, lastUpdateDate);
            sqlList.forEach(resultBuilder::addDeactivateStatement);
        }
    }

    private List<E> loadEntities(List<SynchronizeTask> tasks) {
        Set<String> ids = tasks.stream().map(SynchronizeTask::getReferentialId).collect(Collectors.toSet());
        return spi.loadEntities(context, ids).collect(Collectors.toList());
    }

    private E loadEntity(SynchronizeTask tasks) {
        return spi.loadEntity(context, tasks.getReferentialId());
    }

    private Date date(Date incomingDate) {
        return incomingDate == null ? defaultLastUpdateDate : incomingDate;
    }
}
