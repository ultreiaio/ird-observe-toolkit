package fr.ird.observe.spi.referential;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.service.ServiceContext;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class OneSideSqlResultBuilder {

    private final ServiceContext context;
    private final Set<Class<? extends ReferentialDto>> replicationOrder;
    private final Date defaultLastUpdateDate;

    public OneSideSqlResultBuilder(ServiceContext context, Set<Class<? extends ReferentialDto>> replicationOrder, Date defaultLastUpdateDate) {
        this.context = context;
        this.replicationOrder = replicationOrder;
        this.defaultLastUpdateDate = defaultLastUpdateDate;
    }

    public OneSideSqlResult build(OneSideSqlRequest request) {
        Set<Class<? extends ReferentialDto>> types = request.getTypes();
        Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide = new LinkedHashMap<>(request.getIdsOnlyExistingOnThisSide());
        idsOnlyExistingOnThisSide.putAll(request.getNewIds());
        OneSideSqlResult.Builder resultBuilder = new OneSideSqlResult.Builder(idsOnlyExistingOnThisSide);
        replicationOrder.stream().filter(types::contains).forEach(dtoType -> build0(request, dtoType, resultBuilder));
        return resultBuilder.build();
    }

    protected <D extends ReferentialDto, R extends ReferentialDtoReference, E extends ReferentialEntity> void build0(OneSideSqlRequest request, Class<D> dtoType, OneSideSqlResult.Builder resultBuilder) {
        ReferentialDtoEntityContext<D, R, E, ?> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
        new OneSideSqlResultBuilderForType<>(context, spi, resultBuilder, defaultLastUpdateDate).build(request);
    }

}

