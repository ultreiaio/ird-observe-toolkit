package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.util.Objects;

/**
 * Created on 28/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.1.4
 */
public class SecondLevelRelationCountCache<E extends Entity> extends RelationCountCache<E> {

    private static final String GET_COUNT_BY_PARENT_ID_2 =
            "SELECT main.%3$s, COUNT(second.topiaId)\n"
                    + " FROM %1$s.%2$s main\n"
                    + " INNER JOIN %4$s.%5$s second ON second.%2$s = main.topiaId\n"
                    + "%6$s"
                    + " GROUP BY main.%3$s";
    private final TopiaEntitySqlTable secondTable;

    public SecondLevelRelationCountCache(TopiaEntitySqlTable mainTable, TopiaEntitySqlTable secondTable, DtoEntityContext<?, ?, E, ?> spi) {
        super(mainTable, spi);
        this.secondTable = Objects.requireNonNull(secondTable);
    }

    public final TopiaEntitySqlTable secondTable() {
        return secondTable;
    }

    @Override
    protected String generateSql(String property, String extraWhereClause) {
        return String.format(GET_COUNT_BY_PARENT_ID_2,
                             mainTable().getSchemaName(),
                             mainTable().getTableName(),
                             property,
                             secondTable.getSchemaName(),
                             secondTable.getTableName(),
                             extraWhereClause == null ? "" : " WHERE " + extraWhereClause + "\n");
    }
}
