package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.relation.WithParent;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import org.nuiton.topia.persistence.TopiaDao;

import java.util.Date;
import java.util.function.Consumer;

/**
 * Created on 10/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public abstract class EditableDtoEntityContext<
        PE extends Entity,
        D extends EditableDto,
        R extends DataDtoReference,
        E extends DataEntity,
        T extends TopiaDao<E>> extends DataDtoEntityContext<D, R, E, T> implements WithParent {

    protected abstract void setNotPersistedEntity(PE parent, E entity);

    public abstract DtoEntityContext<?, ?, PE, ?> parentSpi();

    public Form<D> preCreate(ServiceContext context, PE parent, E preCreated) {
        Form<D> form = entityToForm(context.getReferentialLocale(), preCreated);
        loadDtoForValidation(context, parent, preCreated, form.getObject());
        return form;
    }

    public Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, PE parent, E entity, D dto) {
        fromDto(context.getReferentialLocale(), entity, dto);
        if (dto.isNotPersisted()) {
            setNotPersistedEntity(parent, entity);
        }
        return null;
    }

    @Override
    public final Form<D> loadForm(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        PE parent = getParent(context, id);
        Form<D> form = entityToForm(context.getReferentialLocale(), entity);
        loadDtoForValidation(context, parent, entity, form.getObject());
        return form;
    }

    @Override
    protected DeleteRequest createDeleteRequest(ServiceContext context, String id) {
        PE parentId = getParent(context, id);
        return new DeleteRequest(context.isPostgresDatabase(), toEntityType().getName(), parentId.getTopiaId(), id);
    }

    @Override
    protected void onDeleteUpdateLastUpdateDate(ServiceContext context, String parentId, Date now) {
        super.onDeleteUpdateLastUpdateDate(context, parentId, now);
        // also update lastUpdateDateField on entities to root
        // See https://gitlab.com/ultreiaio/ird-observe/-/issues/2786
        parentSpi().updateLastUpdateDateField(context.getTopiaPersistenceContext(), parentId, now);
    }

    public final PE getParent(ServiceContext context, String id) {
        ToolkitParentIdDtoBean parentId = getParentId(context, id);
        return parentSpi().loadEntity(context, parentId.getTopiaId());
    }

    public void loadDtoForValidation(ServiceContext context, PE parent, E entity, D dto) {
    }

    @Override
    public final void loadDtoForValidation(ServiceContext context, E entity, D dto) {
        PE parent = getParent(context, entity.getTopiaId());
        loadDtoForValidation(context, parent, entity, dto);
    }

    public final Form<D> preCreate(ServiceContext context, String parentId) {
        PE parent = parentSpi().loadEntity(context, parentId);
        E preCreated = newEntity(context.now());
        return preCreate(context, parent, preCreated);
    }

    public final SaveResultDto save(ServiceContext context, String parentId, D dto) throws ConcurrentModificationException {
        PE parent = parentSpi().loadEntity(context, parentId);
        E entity = loadOrCreateEntityFromDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        Consumer<AddEntityToUpdateStep> extraConsumer = onSave(context, parent, entity, dto);
        return saveEntity(context, entity, extraConsumer);
    }

    protected final SaveResultDto saveEntity(ServiceContext context, E entity, Consumer<AddEntityToUpdateStep> extraConsumer) {
        AddEntityToUpdateStep result = newSaveHelper(context).update(this, entity);
        if (extraConsumer != null) {
            extraConsumer.accept(result);
        }
        return result.build(entity);
    }

}
