package fr.ird.observe.spi.referential;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.ReferentialIds;
import fr.ird.observe.spi.PersistenceBusinessProject;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.TimeLog;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.service.sql.TopiaEntitySqlModelResource;
import org.nuiton.topia.service.sql.internal.SqlRequestSetConsumerContext;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlSelectArgument;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlan;
import org.nuiton.topia.service.sql.plan.copy.TopiaEntitySqlCopyPlanTask;
import org.nuiton.topia.service.sql.plan.replicate.TopiaEntitySqlReplicatePlanTask;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * To obtain all referential ids for some data.
 * <p>
 * Created on 12/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.8
 */
public class ReferentialIdsBuilder {

    protected static final TimeLog TIME_LOG = new TimeLog(ReferentialIdsBuilder.class, 500, 1000);
    private final PersistenceBusinessProject persistenceBusinessProject;
    private final Class<? extends TopiaEntity> dataType;
    private final Set<String> ids;
    private final TopiaEntitySqlCopyPlan copyPlan;

    public static ReferentialIdsBuilder builder(PersistenceBusinessProject persistenceBusinessProject, TopiaEntitySqlModelResource resource, Class<? extends TopiaEntity> dataType, String... ids) {
        return new ReferentialIdsBuilder(persistenceBusinessProject, resource, dataType, ids);
    }

    public ReferentialIdsBuilder(PersistenceBusinessProject persistenceBusinessProject, TopiaEntitySqlModelResource resource, Class<? extends TopiaEntity> dataType, String... ids) {
        this.persistenceBusinessProject = persistenceBusinessProject;
        this.dataType = dataType;
        this.ids = Set.of(ids);
        this.copyPlan = resource.getCopyPlanModel().getEntryPointPlan(dataType.getName());
    }

    public ReferentialIds build(TopiaApplicationContext<?> applicationContext) {
        Map<Class<? extends ReferentialDto>, Set<String>> result = new LinkedHashMap<>();
        try (TopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext()) {

            TopiaEntitySqlSelectArgument selectArgument = persistenceContext.getDao(dataType).optimizeSelectArgument(TopiaEntitySqlSelectArgument.of(ids));
            String ids = SqlRequestSetConsumerContext.ids(selectArgument);

            for (TopiaEntitySqlCopyPlanTask task : copyPlan) {
                Map<String, Map<String, String>> extra = task.getExtra();
                if (extra == null) {
                    continue;
                }
                Map<String, String> referentialShell = extra.get(TopiaEntitySqlModelResource.REFERENTIAL_SHELL);
                if (referentialShell == null) {
                    continue;
                }
                String selectClause = String.format("DISTINCT(%s.%%s)", task.getTableName());
                String sql = TopiaEntitySqlReplicatePlanTask.applyIds(task.getSelectSql(), selectClause, ids);

                for (Map.Entry<String, String> entry : referentialShell.entrySet()) {
                    String referentialType = entry.getKey();
                    Class<? extends TopiaEntity> entityType = Objects2.forName(referentialType);
                    Class<? extends ReferentialDto> dtoType = persistenceBusinessProject.getEntityToDtoClassMapping().get(entityType);
                    for (String columnName : entry.getValue().split("\\s*,\\s*")) {

                        String realSql = String.format(sql, columnName);

                        long t0 = TimeLog.getTime();
                        List<String> count = persistenceContext.getSqlSupport().findMultipleResult(realSql, r -> r.getString(1));
                        TIME_LOG.log(t0, "Get referential ids", String.format(TopiaEntitySqlReplicatePlanTask.applyIds(task.getSelectSql(), selectClause, "in (" + this.ids.size() + ")"), columnName));
                        if (!count.isEmpty()) {
                            result.computeIfAbsent(dtoType, e -> new TreeSet<>()).addAll(count);
                        }
                    }
                }
            }
        }
        return ReferentialIds.of(result);
    }
}
