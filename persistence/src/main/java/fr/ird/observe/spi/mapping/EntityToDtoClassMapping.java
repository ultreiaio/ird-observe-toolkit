package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.Entity;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5
 */
public class EntityToDtoClassMapping extends ImmutableClassMapping<Entity, BusinessDto> {

    @SuppressWarnings("unchecked")
    private static <O> Class<O> getClass(Object entity) {
        if (entity.getClass().getName().contains("net.bytebuddy")) {
            return (Class<O>) entity.getClass().getInterfaces()[0];
        }
        if (entity.getClass().getName().contains("HibernateProxy")) {
            return (Class<O>) entity.getClass().getInterfaces()[0];
        }
        return (Class<O>) entity.getClass();
    }

    protected EntityToDtoClassMapping(Map<Class<? extends Entity>, Class<? extends BusinessDto>> mapping) {
        super(mapping);
    }

    public <VV extends BusinessDto> Class<VV> get(TopiaEntity key) {
        return get(getClass(key));
    }

    public LinkedHashSet<Class<? extends TopiaEntity>> getImplementationClasses() {
        return keySet().stream().filter(c -> !c.isInterface()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public LinkedHashSet<Class<? extends TopiaEntity>> getContractClasses() {
        return keySet().stream().filter(Class::isInterface).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
