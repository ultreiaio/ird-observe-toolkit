package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.dto.DtoParentAware;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.WithDataFile;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataFileAware;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.spi.usage.UsageHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 04/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class DataDtoEntityContext<
        D extends DataDto,
        R extends DataDtoReference,
        E extends DataEntity,
        T extends TopiaDao<E>> extends DtoEntityContextSupport<D, R, E, T> {

    private static final Logger log = LogManager.getLogger(DataDtoEntityContext.class);

    public static Blob byteArrayToBlob(byte[] bytes) {
        try {
            return new SerialBlob(bytes);
        } catch (SQLException e) {
            throw new RuntimeException("unable to create blob ", e);
        }
    }

    @Override
    public void toDto(ReferentialLocale referentialLocale, E entity, D dto) {
        super.toDto(referentialLocale, entity, dto);
        dto.setHomeId(entity.getHomeId());
        if (dto instanceof DtoParentAware && entity instanceof DtoParentAware) {
            DtoParentAware dto1 = (DtoParentAware) dto;
            dto1.setParentId(((DtoParentAware) entity).getParentId());
        }
        if (dto instanceof WithDataFile && entity instanceof DataFileAware) {
            WithDataFile dto1 = (WithDataFile) dto;
            dto1.setHasData(((DataFileAware) entity).getData() != null);
        }
    }

    @Override
    public void fromDto(ReferentialLocale referentialLocale, E entity, D dto) {
        super.fromDto(referentialLocale, entity, dto);
        entity.setHomeId(dto.getHomeId());
        if (dto instanceof WithDataFile && entity instanceof DataFileAware) {
            WithDataFile dto1 = (WithDataFile) dto;
            if (dto1.isHasData()) {
                if (dto1.getData() != null) {
                    ((DataFileAware) entity).setData(byteArrayToBlob(dto1.getData().getContent()));
                    ((DataFileAware) entity).setDataFilename(dto1.getData().getName());
                }
            } else {
                ((DataFileAware) entity).setData(null);
                ((DataFileAware) entity).setDataFilename(null);
            }
        }
    }

    public final R toReference(ServiceContext context, String id, String classifier) {
        E entity = loadOrCreateEntity(context, id);
        return toReference(context.getReferentialLocale(), entity, classifier);
    }

    @Override
    public final DataDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Collection<E> entities, Date now, String classifier) {
        return (DataDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities.stream(), now, classifier);
    }

    @Override
    public final DataDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, Date now, String classifier) {
        return (DataDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities, now, classifier);
    }

    @Override
    public final DataDtoReferenceSet<R> toReferenceSet(ServiceContext context, Date now) {
        return (DataDtoReferenceSet<R>) super.toReferenceSet(context, now);
    }

    @Override
    public final DataDtoReferenceSet<R> toReferenceSet(ReferentialLocale referentialLocale, Stream<E> entities, String excludeId, Date now, String classifier) {
        return (DataDtoReferenceSet<R>) super.toReferenceSet(referentialLocale, entities, excludeId, now, classifier);
    }

    @Override
    protected final DataDtoReferenceSet<R> createReferenceSet(ReferentialLocale referentialLocale, Collection<R> references, Date now) {
        return DataDtoReferenceSet.of(toReferenceType(), references, now);
    }

    public final UsageHelper<? super Entity> newOptionalUsageHelper(ServiceContext context) {
        return new UsageHelper<>(() -> usageModel(context), context.getTopiaPersistenceContext(), true, false, true, true, false, false);
    }

    public final UsageHelper<? super Entity> newMandatoryUsageHelper(ServiceContext context) {
        return new UsageHelper<>(() -> usageModel(context), context.getTopiaPersistenceContext(), false, true, false, false, false, false);
    }

    public Form<D> loadForm(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        Form<D> form = entityToForm(context, entity);
        loadDtoForValidation(context, entity, form.getObject());
        return form;
    }

    public RelationCountCache<E> newRelationCountCache(DtoEntityContext<?, ?, ?, ?> mainTable) {
        return new SimpleRelationCountCache<>(mainTable.getEntitySqlDescriptor().getTable(), this);
    }

    public RelationCountCache<E> newRelationCountCache(DtoEntityContext<?, ?, ?, ?> mainTable, DtoEntityContext<?, ?, ?, ?> secondTable) {
        return new SecondLevelRelationCountCache<>(mainTable.getEntitySqlDescriptor().getTable(), secondTable.getEntitySqlDescriptor().getTable(), this);
    }

    public <C extends Set<E>> C toDataEntityTypedSet(ReferentialLocale referentialLocale, Collection<D> dtoList, C entityList) {
        if (entityList == null) {
            //noinspection unchecked
            entityList = (C) new LinkedHashSet<E>();
        }
        fillEntityCollection(referentialLocale, dtoList, entityList);
        return entityList;
    }

    public List<E> toDataEntityList(ReferentialLocale referentialLocale, Collection<D> dtoList, List<E> entityList) {
        if (entityList == null) {
            entityList = new LinkedList<>();
        }
        fillEntityCollection(referentialLocale, dtoList, entityList);
        return entityList;
    }

    public List<D> toDataDtoList(ReferentialLocale referentialLocale, Collection<E> entities) {
        List<D> dtoList = null;
        if (entities != null) {
            dtoList = new ArrayList<>(entities.size());
            for (E entity : entities) {
                if (entity == null) {
                    continue;
                }
                D dto = toDto(referentialLocale, entity);
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    public LinkedHashSet<R> toDataReferenceLinkedHashSet(ReferentialLocale referentialLocale, Collection<E> entities) {
        LinkedHashSet<R> references = null;
        if (entities != null && !entities.isEmpty()) {
            references = new LinkedHashSet<>(entities.size());
            for (E entity : entities) {
                R reference = toReference(referentialLocale, entity);
                references.add(reference);
            }
        }
        return references;
    }

    public LinkedHashSet<D> toDtoLinkedHashSet(ReferentialLocale referentialLocale, Collection<E> entities) {
        LinkedHashSet<D> dtoList = null;
        if (entities != null) {
            dtoList = new LinkedHashSet<>(entities.size());
            for (E entity : entities) {
                D dto = toDto(referentialLocale, entity);
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    public final UsageCount countOptional(ServiceContext context, ToolkitIdDtoBean request) {
        DataEntity entity = loadEntity(context, request.getId());
        return newOptionalUsageHelper(context).count(entity);
    }

    public final UsageCount countMandatory(ServiceContext context, ToolkitIdDtoBean request) {
        DataEntity entity = loadEntity(context, request.getId());
        return newMandatoryUsageHelper(context).count(entity);
    }

    public <EE extends Entity> Set<ToolkitIdLabel> findOptionalUsages(ServiceContext context, ToolkitIdDtoBean request, DtoEntityContext<?, ?, EE, ?> spi2) {
        E entity = loadEntity(context, request.getId());
        Collection<EE> entities = newOptionalUsageHelper(context).find(entity, spi2.toEntityType());
        return findUsages(context, entities, spi2);
    }

    public <EE extends Entity> Set<ToolkitIdLabel> findMandatoryUsages(ServiceContext context, ToolkitIdDtoBean request, DtoEntityContext<?, ?, EE, ?> spi2) {
        E entity = loadEntity(context, request.getId());
        Collection<EE> entities = newMandatoryUsageHelper(context).find(entity, spi2.toEntityType());
        return findUsages(context, entities, spi2);
    }

    public final void delete(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        log.info(String.format("will delete entity: %s", entity.getTopiaId()));
        DeleteRequest sqlRequest = createDeleteRequest(context, id);
        consumeAndExecute(context, sqlRequest);
        Date now = context.now();
        onDeleteUpdateLastUpdateDate(context, sqlRequest.getParentId(), now);
    }

    protected void onDeleteUpdateLastUpdateDate(ServiceContext context, String parentId, Date now) {
        // by default, just update the lastUpdateTable for the given entity type
        updateLastUpdateDateTable(context.getTopiaPersistenceContext(), now);
    }

    protected DeleteRequest createDeleteRequest(ServiceContext context, String id) {
        return new DeleteRequest(context.isPostgresDatabase(), toEntityType().getName(), null, id);
    }

    protected void clearBeforeUpdate(ServiceContext context, String id) {
        delete(context, id);
        context.getTopiaPersistenceContext().flush();
        context.getTopiaPersistenceContext().getHibernateSupport().getHibernateSession().clear();
    }

    protected <C extends Collection<E>> void fillEntityCollection(ReferentialLocale referentialLocale, Collection<D> dtoList, C entityList) {
        new ArrayList<>(entityList).stream().filter(Objects::isNull).forEach(entityList::remove);
        Map<String, E> entitiesById = entityList.stream().collect(Collectors.toMap(TopiaEntity::getTopiaId, Function.identity()));
        entityList.clear();
        if (dtoList != null) {
            for (D dto : dtoList) {
                E entity = entitiesById.get(dto.getId());
                if (entity == null) {
                    // Create new entity
                    entity = newEntity(dto.getTopiaCreateDate());
                }
                fromDto(referentialLocale, entity, dto);
                entityList.add(entity);
            }
        }
    }

    protected final DataDtoReferenceSetBuilder<E, R> newDataDtoReferenceSetBuilder(BiConsumer<E, R> optionalConsumer) {
        return DataDtoReferenceSetBuilder.create(this, optionalConsumer);
    }

    private <EE extends Entity> Set<ToolkitIdLabel> findUsages(ServiceContext context, Collection<EE> entities, DtoEntityContext<?, ?, EE, ?> spi2) {
        context.getDecoratorService().installDecorator(spi2.toEntityType(), entities.stream());
        return entities.stream().map(Entity::toLabel).collect(Collectors.toSet());
    }
}
