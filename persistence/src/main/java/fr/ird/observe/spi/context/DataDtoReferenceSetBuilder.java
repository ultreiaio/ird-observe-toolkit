package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.entities.data.DataEntity;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Created on 30/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.1.4
 */
public class DataDtoReferenceSetBuilder<E extends DataEntity, R extends DataDtoReference> {

    private final DataDtoEntityContext<?, R, E, ?> spi;
    private final BiConsumer<E, R> extraConsumer;
    private final String decoratorClassifier;

    public static <E extends DataEntity, R extends DataDtoReference> DataDtoReferenceSetBuilder<E, R> create(DataDtoEntityContext<?, R, E, ?> spi, BiConsumer<E, R> optionalConsumer) {
        return new DataDtoReferenceSetBuilder<>(spi, optionalConsumer);
    }

    protected DataDtoReferenceSetBuilder(DataDtoEntityContext<?, R, E, ?> spi, BiConsumer<E, R> optionalConsumer) {
        this.spi = Objects.requireNonNull(spi);
        this.extraConsumer = optionalConsumer;
        this.decoratorClassifier = WithStatistics.class.isAssignableFrom(spi.toReferenceType()) ? DecoratorService.WITH_STATS_CLASSIFIER : null;
    }

    public DataDtoReferenceSet<R> build(ReferentialLocale referentialLocale, Collection<E> entities, Date lastUpdateDate) {
        Set<R> references = new LinkedHashSet<>(entities.size());
        Function<E, R> transformer = e -> spi.toReference(referentialLocale, e, decoratorClassifier);
        if (extraConsumer == null) {
            entities.forEach(entity -> {
                R reference = transformer.apply(entity);
                references.add(reference);
            });
        } else {
            entities.forEach(entity -> {
                R reference = transformer.apply(entity);
                extraConsumer.accept(entity, reference);
                references.add(reference);
            });
        }
        return DataDtoReferenceSet.of(spi.toReferenceType(), references, lastUpdateDate);
    }
}
