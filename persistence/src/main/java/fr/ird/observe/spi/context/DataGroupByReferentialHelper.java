package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByValue;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.ToolkitTopiaPersistenceContextSupport;
import fr.ird.observe.entities.data.DataGroupByEntityDefinition;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.service.ServiceContext;
import org.apache.commons.lang3.mutable.MutableInt;
import org.nuiton.topia.service.sql.model.TopiaEntitySqlTable;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DataGroupByReferentialHelper<D extends DataDto, R extends DataDtoReference, E extends RootOpenableEntity> {

    public abstract R newReference(ResultSet resultSet, MutableInt mutableInt) throws SQLException;

    protected final ToolkitTopiaPersistenceContextSupport persistenceContext;
    protected final DecoratorService decoratorService;
    protected final ReferentialLocale referentialLocale;
    protected final Map<Class<? extends ReferentialEntity>, ReferentialCache<?, ?>> referential;
    protected final DataDtoEntityContext<D, R, E, ?> spi;
    private final List<RelationCountCache<E>> relations;
    private final TopiaEntitySqlTable topiaEntityEnum;
    private final ServiceContext context;

    public DataGroupByReferentialHelper(DataDtoEntityContext<D, R, E, ?> spi,
                                        ServiceContext context,
                                        List<? extends DtoEntityContext<?, ?, ?, ?>> relationCountCaches,
                                        List<ReferentialDtoEntityContext<?, ?, ?, ?>> referentialCaches) {
        this.spi = spi;
        this.context = Objects.requireNonNull(context);
        this.persistenceContext = context.getTopiaPersistenceContext();
        this.topiaEntityEnum = spi.getEntitySqlDescriptor().getTable();
        this.decoratorService = Objects.requireNonNull(context.getDecoratorService());
        this.referentialLocale = decoratorService.getReferentialLocale();
        this.referential = referentialCaches.stream().map(s -> s.newCache(context)).collect(Collectors.toMap(ReferentialCache::entityType, Function.identity()));
        this.relations = relationCountCaches.stream().map(spi::newRelationCountCache).collect(Collectors.toCollection(LinkedList::new));
        addExtraCaches(relations);
    }

    protected void addExtraCaches(List<RelationCountCache<E>> relations) {
    }

    public ServiceContext getContext() {
        return context;
    }

    @SuppressWarnings("unchecked")
    public <RR extends ReferentialDtoReference, EE extends ReferentialEntity> ReferentialCache<RR, EE> referentialGet(Class<EE> index) {
        return (ReferentialCache<RR, EE>) referential.get(index);
    }

    public RelationCountCache<E> relationGet(DtoEntityContext<?, ?, ?, ?> mainSpi) {
        TopiaEntitySqlTable mainTable = mainSpi.getEntitySqlDescriptor().getTable();
        return relations.stream().filter(r -> r instanceof SimpleRelationCountCache<?> && r.mainTable().equals(mainTable)).findFirst().orElseThrow();
    }

    public RelationCountCache<E> relationGet(DtoEntityContext<?, ?, ?, ?> mainSpi, DtoEntityContext<?, ?, ?, ?> secondSpi) {
        TopiaEntitySqlTable mainTable = mainSpi.getEntitySqlDescriptor().getTable();
        TopiaEntitySqlTable secondTable = secondSpi.getEntitySqlDescriptor().getTable();
        return relations.stream().filter(r -> r instanceof SecondLevelRelationCountCache<?> && Objects.equals(r.mainTable(), mainTable) && Objects.equals(((SecondLevelRelationCountCache<?>) r).secondTable(), secondTable)).findFirst().orElseThrow();
    }

    public R newReference(ResultSet resultSet) throws SQLException {
        return newReference(resultSet, new MutableInt(1));
    }

    public D newDto(ResultSet resultSet) throws SQLException {
        return newDto(resultSet, new MutableInt(1));
    }

    public E newChildren(ResultSet resultSet) throws SQLException {
        return newChildren(resultSet, new MutableInt(1));
    }

    public void decorate(DataGroupByEntityDefinition<?, ?, ?, ?> definition, DataGroupByValue groupCountValue) {
        decorate(definition, List.of(groupCountValue));
    }


    public E newChildren(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        E entity = spi.newEntity(resultSet.getString(rowIndex.getAndIncrement()));
        entity.setTopiaCreateDate(resultSet.getTimestamp(rowIndex.getAndIncrement()));
        entity.setTopiaVersion(resultSet.getLong(rowIndex.getAndIncrement()));
        entity.setLastUpdateDate(resultSet.getTimestamp(rowIndex.getAndIncrement()));
        return entity;
    }

    public D newDto(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        D dto = spi.newDto();
        dto.setId(resultSet.getString(rowIndex.getAndIncrement()));
        dto.setTopiaCreateDate(resultSet.getTimestamp(rowIndex.getAndIncrement()));
        dto.setTopiaVersion(resultSet.getLong(rowIndex.getAndIncrement()));
        dto.setLastUpdateDate(resultSet.getTimestamp(rowIndex.getAndIncrement()));
        return dto;
    }

    public final R toReference(D dto) {
        return spi.toReference(referentialLocale, dto);
    }

    public final DecoratorService decoratorService() {
        return decoratorService;
    }

    public void resetCount(DataGroupByEntityDefinition<?, ?, ?, ?> definition, String filterCode) {
        String extraSqlCode;
        String finalFilterCode = filterCode.replaceAll("@alias@", "t");
        if (definition.isQualitativeWithSecondLevel()) {
            String secondLevelTable = definition.getGroupBySecondTable();
            String join = " LEFT JOIN " + secondLevelTable + " _second ON t." + definition.getPropertyName() + " = _second.topiaId ";
            extraSqlCode = String.format("main.%2$s in (SELECT t.topiaId FROM %1$s.%2$s t %3$s  WHERE %4$s)", topiaEntityEnum.getSchemaName(), topiaEntityEnum.getTableName(), join, finalFilterCode.replaceAll("@secondAlias@", "_second"));
        } else {
            extraSqlCode = String.format("main.%2$s in (SELECT t.topiaId FROM %1$s.%2$s t WHERE %3$s)", topiaEntityEnum.getSchemaName(), topiaEntityEnum.getTableName(), finalFilterCode);
        }
        resetRelations(extraSqlCode);
    }

    protected void resetRelations(String extraSqlCode) {
        relations.forEach(r -> r.reset(persistenceContext, extraSqlCode));
    }

    public void decorate(DataGroupByEntityDefinition<?, ?, ?, ?> definition, List<DataGroupByValue> groupCountValues) {
        if (definition.isQualitative()) {
            Class<?> propertyType = definition.getPropertyType();
            for (ReferentialCache<?, ?> cache : referential.values()) {
                if (propertyType.equals(cache.entityType())) {
                    decorate(definition, cache, groupCountValues);
                    break;
                }
            }
        } else {
            for (DataGroupByValue dataGroupByValue : groupCountValues) {
                dataGroupByValue.setEnabled(true);
                dataGroupByValue.setText(dataGroupByValue.getValue());
            }
        }
    }

    public void sort(DataGroupByEntityDefinition<?, ?, ?, ?> definition, List<DataGroupByValue> list) {
        if (definition.isQualitative()) {
            @SuppressWarnings("unchecked") Class<? extends ReferentialEntity> propertyType = (Class<? extends ReferentialEntity>) definition.getPropertyType();
            ReferentialCache<?, ?> referentialCache = referentialGet(propertyType);
            referentialCache.sort(list);
        }
    }

    public void decorate(@SuppressWarnings("unused") DataGroupByEntityDefinition<?, ?, ?, ?> definition, ReferentialCache<?, ?> cache, List<DataGroupByValue> groupCountValues) {
        cache.decorate(groupCountValues);
    }

    public String filterCode(DataGroupByEntityDefinition<?, ?, ?, ?> definition, String groupByValue, String groupByFlavor, String excludeId) {
        String result;
        boolean qualitativeComplex = definition.isQualitativeWithSecondLevel();
        if (groupByValue == null) {
            if (qualitativeComplex) {
                result = String.format("@secondAlias@.%s IS NULL", definition.getPropertySecondLevelName());
            } else {
                result = String.format("@alias@.%s IS NULL", definition.getPropertyName());
            }
            return addExcludeInFilterCode(result, excludeId);
        }
        if (definition.isQualitative()) {
            if (qualitativeComplex) {
                result = String.format("@secondAlias@.%s = '%s'", definition.getPropertySecondLevelName(), groupByValue);
            } else {
                result = String.format("@alias@.%s = '%s'", definition.getPropertyName(), groupByValue);
            }
            return addExcludeInFilterCode(result, excludeId);
        }
        if (groupByFlavor == null) {
            if (definition.isTemporal()) {
                result = String.format("TO_CHAR(@alias@.%s, 'YYYY-MM-DD') = '%s'", definition.getPropertyName(), groupByValue);
            } else {
                result = String.format("@alias@.%s = '%s'", definition.getPropertyName(), groupByValue);
            }
            return addExcludeInFilterCode(result, excludeId);
        }
        switch (groupByFlavor) {
            case "year":
                result = String.format("TO_CHAR(@alias@.%s, 'YYYY') = '%s'", definition.getPropertyName(), groupByValue);
                break;
            case "month":
                result = String.format("TO_CHAR(@alias@.%s, 'YYYY-MM') = '%s'", definition.getPropertyName(), groupByValue);
                break;
            default:
                throw new IllegalStateException(String.format("Can't manage groupByFlavor: %s on %s", groupByFlavor, this));
        }
        return addExcludeInFilterCode(result, excludeId);
    }

    public final String getReferencesSql(DataGroupByEntityDefinition<?, ?, ?, ?> definition, String filterCode) {
        String finalFilterCode = filterCode == null ? null : filterCode.replaceAll("@alias@", "main");
        if (definition.isQualitativeWithSecondLevel()) {
            String secondLevelTable = definition.getGroupBySecondTable();
            String join = " LEFT JOIN " + secondLevelTable + " _second ON main." + definition.getPropertyName() + " = _second.topiaId ";
            return spi.getReferenceQuery() + join + (finalFilterCode == null ? "" : " WHERE " + finalFilterCode.replaceAll("@secondAlias@", "_second"));
        }
        return spi.getReferenceQuery() + (finalFilterCode == null ? "" : " WHERE " + finalFilterCode);
    }

    public void decorateGroupValueChildren(List<E> groupByValues) {
        decoratorService.installDecorator(spi.toEntityType(), groupByValues.stream());
    }

    protected boolean getBoolean(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return resultSet.getBoolean(rowIndex.getAndIncrement());
    }

    protected String getString(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return resultSet.getString(rowIndex.getAndIncrement());
    }

    @SuppressWarnings("unused")
    protected int getInteger(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return resultSet.getInt(rowIndex.getAndIncrement());
    }

    @SuppressWarnings("unused")
    protected long getLong(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return resultSet.getLong(rowIndex.getAndIncrement());
    }

    protected Date getDate(ResultSet resultSet, MutableInt rowIndex) throws SQLException {
        return resultSet.getDate(rowIndex.getAndIncrement());
    }

    protected String addExcludeInFilterCode(String result, String excludeId) {
        if (excludeId != null) {
            result += String.format(" AND @alias@.topiaId != '%s'", excludeId);
        }
        return result;
    }
}
