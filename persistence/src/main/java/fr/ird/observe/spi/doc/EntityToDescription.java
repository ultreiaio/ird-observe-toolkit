package fr.ird.observe.spi.doc;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * To get for an entity the dictionary of his properties, keys are property name, values are property type.
 * <p>
 * This objects helps us to generate documentation.
 * <p>
 * Created on 05/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.22
 */
public class EntityToDescription {

    public static final String JAVA_LANG = "java.lang.";
    private final Map<String, String> result;
    private final Map<String, String> dateAttributes;
    private final TopiaEntityVisitor visitor = new TopiaEntityVisitor() {

        @Override
        public void start(TopiaEntity entity) {
        }

        @Override
        public void end(TopiaEntity entity) {
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
            String typeName = type.getName();
            if (Enum.class.isAssignableFrom(type)) {
                List<String> collect = new LinkedList<>();
                for (Object o : EnumSet.allOf((Class) type)) {
                    collect.add(((Enum) o).name());
                }
                typeName = "enum:" + typeName + ":" + collect;
            } else {
                typeName = cleanJavaLang(typeName);
            }
            addProperty(propertyName, typeName);
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
            addProperty(propertyName, collectionType.getName() + ":" + cleanJavaLang(type.getName()));
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
        }

        @Override
        public void clear() {
        }

        private void addProperty(String propertyName, String type) {
            if (dateAttributes.containsKey(propertyName)) {
                type = dateAttributes.get(propertyName);
            }
            result.put(propertyName, type);
        }

        private String cleanJavaLang(String typeName) {
            if (typeName.startsWith(JAVA_LANG)) {
                return typeName.substring(JAVA_LANG.length());
            }
            return typeName;
        }
    };

    public static Map<String, String> generate(TopiaEntity entity, Map<String, String> dateAttributes) {
        EntityToDescription entityToDescription = new EntityToDescription(dateAttributes);
        entityToDescription.visit(entity);
        return entityToDescription.getResult();
    }

    public EntityToDescription(Map<String, String> dateAttributes) {
        this.dateAttributes = dateAttributes;
        result = new LinkedHashMap<>();
    }

    private void visit(TopiaEntity entity) {
        entity.accept(visitor);
    }

    public Map<String, String> getResult() {
        return result;
    }
}
