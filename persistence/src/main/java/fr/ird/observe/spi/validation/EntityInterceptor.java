package fr.ird.observe.spi.validation;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.service.ServiceContext;
import fr.ird.observe.spi.validation.callback.EntityInterceptorCallback;
import fr.ird.observe.validation.ValidationContextSupport;
import fr.ird.observe.validation.api.result.ValidationResultDtoMessage;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.validation.bean.BeanValidator;
import io.ultreia.java4all.validation.bean.BeanValidatorListener;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Consumer;

/**
 * Created by tchemit on 06/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class EntityInterceptor<D extends BusinessDto, E extends Entity, S extends DtoEntityContext<D, ?, E, ?>> {
    private final S spi;
    private final Consumer<String> dataContextConsumer;
    private final BeanValidator<D> validator;
    private final TreeMap<String, D> cache;
    private final ReferentialLocale referentialLocale;
    private final Decorator decorator;
    private final EntityInterceptorCallback<D, E, S> callback;

    EntityInterceptor(S spi,
                      Consumer<String> dataContextConsumer,
                      BeanValidator<D> validator,
                      ReferentialLocale referentialLocale,
                      Decorator decorator,
                      EntityInterceptorCallback<D, E, S> callback) {
        this.spi = spi;
        this.dataContextConsumer = dataContextConsumer;
        this.validator = validator;
        this.referentialLocale = referentialLocale;
        this.decorator = decorator;
        this.callback = Objects.requireNonNull(callback);
        this.cache = new TreeMap<>();
    }

    @SuppressWarnings("unchecked")
    LinkedList<ValidationResultDtoMessage> intercept(ServiceContext context, MessagesCollector messagesCollector, ValidationContextSupport validationDataContext, Deque<Entity> path, E e) {
        e.registerDecorator(decorator);
        if (validator != null) {
            D dto = cache.computeIfAbsent(e.getTopiaId(), id -> {
                D result = spi.toDto(referentialLocale, e);
                callback.loadDtoForValidation(context, path, spi, e, result);
                return result;
            });
            if (dataContextConsumer != null) {
                dataContextConsumer.accept(dto.getId());
                validationDataContext.add(dto);
            }
            return messagesCollector.collect(validator, dto);
        }
        return null;
    }

    void release(ValidationContextSupport validationContext, String id) {
        if (dataContextConsumer != null) {
            dataContextConsumer.accept(null);
            validationContext.remove(id);
        }
    }

    void clear() {
        validator.setBean(null);
        for (BeanValidatorListener listener : validator.getBeanValidatorListeners()) {
            validator.removeBeanValidatorListener(listener);
        }
        cache.clear();
    }

}
