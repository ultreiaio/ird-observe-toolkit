package fr.ird.observe.spi.context;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.DeletePartialRequest;
import fr.ird.observe.datasource.request.ReplicatePartialRequest;
import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.DataGroupByValue;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.data.DataGroupByEntityDefinition;
import fr.ird.observe.entities.data.RootOpenableEntity;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.services.service.api.InvalidDataException;
import fr.ird.observe.services.service.data.DeleteLayoutRequest;
import fr.ird.observe.services.service.data.MoveLayoutRequest;
import fr.ird.observe.spi.GroupBySpiContext;
import fr.ird.observe.spi.result.AddEntityToUpdateStep;
import fr.ird.observe.spi.service.ServiceContext;
import io.ultreia.java4all.util.sql.SqlFunction;
import io.ultreia.java4all.util.sql.SqlQuery;
import org.hibernate.query.NativeQuery;
import org.nuiton.topia.persistence.TopiaDao;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class RootOpenableDtoEntityContext<
        D extends RootOpenableDto,
        R extends DataDtoReference,
        E extends RootOpenableEntity,
        T extends TopiaDao<E>,
        H extends DataGroupByReferentialHelper<D, R, E>
        > extends DataDtoEntityContext<D, R, E, T> {

    public abstract H createReferentialHelper(ServiceContext context);

    public abstract Map<String, DataGroupByEntityDefinition<E, ?, D, ?>> getGroupBy();

    public abstract Consumer<AddEntityToUpdateStep> onSave(ServiceContext context, E entity, D dto);

    public Set<Class<? extends DataEntity>> layoutTypeToDataTypes(Class<? extends DataDto> dtoType) {
        throw new IllegalArgumentException("Can't find entity types for dto type: " + dtoType.getName());
    }

    public final ReplicatePartialRequest toSqlRequest(ServiceContext context, MoveLayoutRequest request) {
        return toSqlRequest(context.isPostgresDatabase(), request);
    }

    public final DeletePartialRequest toSqlRequest(ServiceContext context, DeleteLayoutRequest request) {
        return toSqlRequest(context.isPostgresDatabase(), request);
    }

    public final Set<Class<? extends DataEntity>> layoutTypesToEntityTypes(Set<Class<? extends DataDto>> scopes) {
        Set<Class<? extends DataEntity>> result = new LinkedHashSet<>();
        for (Class<? extends DataDto> dtoType : scopes) {
            Set<Class<? extends DataEntity>> entityTypes = layoutTypeToDataTypes(dtoType);
            result.addAll(entityTypes);
        }
        return result;
    }

    public final ReplicatePartialRequest toSqlRequest(boolean pg, MoveLayoutRequest request) {
        Set<Class<? extends DataEntity>> entityTypes = layoutTypesToEntityTypes(request.getScopes());
        return new ReplicatePartialRequest(pg,
                                           toEntityType().getName(),
                                           entityTypes.stream().map(Class::getName).collect(Collectors.toSet()),
                                           request.getOldParentId(),
                                           request.getNewParentId());
    }

    public final DeletePartialRequest toSqlRequest(boolean pg, DeleteLayoutRequest request) {
        Set<Class<? extends DataEntity>> entityTypes = layoutTypesToEntityTypes(request.getScopes());
        Set<String> layoutTypes = entityTypes.stream().map(Class::getName).collect(Collectors.toSet());
        String dataType = toEntityType().getName();
        return new DeletePartialRequest(pg, dataType, layoutTypes, request.getId());
    }

    public void deleteLayout(ServiceContext context, DeleteLayoutRequest request) {
        DeletePartialRequest deleteRequest = toSqlRequest(context, request);
        consumeAndExecute(context, deleteRequest);
    }

    public void moveLayout(ServiceContext context, MoveLayoutRequest request) {
        ReplicatePartialRequest replicateRequest = toSqlRequest(context, request);
        DeletePartialRequest deleteRequest = replicateRequest.toDeleteRequest();
        consumeAndExecute(context, deleteRequest);
        consumeAndExecute(context, replicateRequest);
    }

    public Form<D> preCreate(ServiceContext context, E preCreated) {
        // by default nothing to do
        return entityToForm(context.getReferentialLocale(), preCreated);
    }

    @Override
    public final ToolkitId update(ServiceContext context, String id, String content, Validation<E> consumer) throws InvalidDataException {
        E existingEntity = loadEntity(context, id);
        E entity = newEntity(existingEntity);
        clearBeforeUpdate(context, id);
        return processContent(context, entity, content, consumer);
    }

    @Override
    public final Form<D> loadForm(ServiceContext context, String id) {
        E entity = loadEntity(context, id);
        Form<D> form = entityToForm(context.getReferentialLocale(), entity);
        loadDtoForValidation(context, entity, form.getObject());
        return form;
    }

    public final UsageCount getDependenciesCount(Function<ToolkitIdDtoBean, UsageCount> usageCountFunction, Set<String> ids) {
        return UsageCount.count(usageCountFunction, ids.stream().map(id -> ToolkitIdDtoBean.of(toDtoType(), id)).collect(Collectors.toSet()));
    }

    public final Set<ToolkitIdLabel> getDependencies(Function<ToolkitIdDtoBean, Set<ToolkitIdLabel>> usageCountFunction, Set<String> ids) {
        Set<ToolkitIdLabel> result = new LinkedHashSet<>();
        for (String id : ids) {
            Set<ToolkitIdLabel> apply = usageCountFunction.apply(ToolkitIdDtoBean.of(toDtoType(), id));
            result.addAll(apply);
        }
        return result;
    }

    public final Form<D> preCreate(ServiceContext context, DataGroupByEntityDefinition<E, ?, D, ?> definition, Object groupByEntityValue) {
        E preCreated = newEntity(context.now());
        if (definition.isQualitative() && groupByEntityValue != null) {
            // value is a referential id
            definition.setGroupByValue(preCreated, groupByEntityValue);
        }
        return preCreate(context, preCreated);
    }

    public final SaveResultDto save(ServiceContext context, D dto) throws ConcurrentModificationException {
        E entity = loadOrCreateEntityFromDto(context, dto);
        checkLastUpdateDate(context, entity, dto);
        Consumer<AddEntityToUpdateStep> extraConsumer = onSave(context, entity, dto);
        return saveEntity(context, entity, extraConsumer);
    }

    public final List<DataGroupByValue> findGroupCountValues(ServiceContext context, DataGroupByEntityDefinition<?, ?, ?, ?> definition, ToolkitTreeFlatModelRootRequest request, DataGroupByReferentialHelper<?, R, E> referentialHelper) {
        T dao = getDao(context);
        NativeQuery<Object[]> query = dao.getSqlQuery(definition.getCountAllQueryName(request));
        Stream<Object[]> stream = query.stream();
        List<DataGroupByValue> result = stream.map(DataGroupByValue::of).collect(Collectors.toList());
        if (request.isLoadNullGroupBy() && result.stream().noneMatch(d -> d.getValue() == null)) {
            // add null count (if not already found)
            NativeQuery<Number> nullQuery = dao.getSqlQuery(definition.getCountOneQueryName(request.getGroupByFlavor(), false));
            long nullCount = nullQuery.uniqueResultOptional().map(Number::longValue).orElse(0L);
            if (nullCount > 0) {
                DataGroupByValue nullResult = new DataGroupByValue(null, nullCount);
                result.add(nullResult);
            }
        }
        referentialHelper.decorate(definition, result);
        if (definition.isQualitative()) {
            referentialHelper.sort(definition, result);
        }
        return result;
    }

    public final DataGroupByValue findGroupCountValue(ServiceContext context, DataGroupByEntityDefinition<?, ?, ?, ?> definition, String groupByValue, String groupByFlavor, DataGroupByReferentialHelper<?, R, E> referentialHelper) {
        T dao = getDao(context);
        boolean withValue = groupByValue != null;
        NativeQuery<Number> query = dao.getSqlQuery(definition.getCountOneQueryName(groupByFlavor, withValue));
        if (withValue) {
            query.setParameter(1, groupByValue);
        }
        long count = query.uniqueResultOptional().map(Number::longValue).orElse(0L);
        DataGroupByValue result = new DataGroupByValue(groupByValue, count);
        referentialHelper.decorate(definition, result);
        return result;
    }

    public final List<E> findGroupCountValueChildren(ServiceContext context,
                                                     DataGroupByEntityDefinition<E, ?, D, ?> definition,
                                                     String parentId,
                                                     String groupByFlavor,
                                                     DataGroupByReferentialHelper<?, R, E> referentialHelper) {
        List<E> result = getChildrenSql(context, definition, parentId, groupByFlavor, null, referentialHelper, referentialHelper::newChildren);
        referentialHelper.decorateGroupValueChildren(result);
        return result;
    }

    public final List<R> getChildrenReference(ServiceContext context, DataGroupByEntityDefinition<E, ?, D, ?> definition, String parentId, String groupByFlavor, DataGroupByReferentialHelper<?, R, E> referentialHelper) {
        return getChildrenSql(context, definition, parentId, groupByFlavor, null, referentialHelper, referentialHelper::newReference);
    }

    public final List<R> getChildrenLabel(ServiceContext context, DataGroupByEntityDefinition<E, ?, D, ?> definition, String parentId, String groupByFlavor, String excludeId, DataGroupByReferentialHelper<D, R, E> referentialHelper) {
        return getChildrenSql(context, definition, parentId, groupByFlavor, excludeId, referentialHelper, referentialHelper::newReference);
    }

    public void updateQualitativeProperty(ServiceContext context,DataGroupByParameter groupBy, Set<String> ids) {
        T dao = getDao(context);
        GroupBySpiContext<D, ?, E, ?, ?, ?, ?> groupBySpi = context.fromGroupByName(groupBy.getGroupByName());
        DataGroupByEntityDefinition<E, ?, D, ?> definition = groupBySpi.definition();
        NativeQuery<Object[]> query = dao.getSqlQuery(definition.getUpdateQueryName());
        query.setParameter(1, groupBy.getValue());
        Timestamp now = context.timestampNow();
        query.setParameter(2, now);
        for (String id : ids) {
            query.setParameter(3, id);
            query.executeUpdate();
        }
        newSaveHelper(context, now).updateLastUpdateDateTable(this).build();
    }

    private SaveResultDto saveEntity(ServiceContext context, E entity, Consumer<AddEntityToUpdateStep> extraConsumer) {
        AddEntityToUpdateStep update = newSaveHelper(context).update(this, entity);
        if (extraConsumer != null) {
            extraConsumer.accept(update);
        }
        return update.build(entity);
    }

    private <X> List<X> getChildrenSql(ServiceContext context, DataGroupByEntityDefinition<E, ?, D, ?> definition, String parentId, String groupByFlavor, String excludeId, DataGroupByReferentialHelper<?, R, E> referentialHelper, SqlFunction<ResultSet, X> transformer) {
        String filterCode = referentialHelper.filterCode(definition, parentId, groupByFlavor, excludeId);
        referentialHelper.resetCount(definition, filterCode);
        String sql = referentialHelper.getReferencesSql(definition, filterCode);
        SqlQuery<X> query = SqlQuery.wrap(sql, transformer);
        return getDao(context).findMultipleResult(query);
    }
}
