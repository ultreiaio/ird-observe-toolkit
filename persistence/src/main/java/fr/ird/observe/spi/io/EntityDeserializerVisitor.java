package fr.ird.observe.spi.io;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 02/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.21
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class EntityDeserializerVisitor implements TopiaEntityVisitor {
    private final EntityDeserializerContext context;
    private final Map<String, Object> jsonObject;

    public EntityDeserializerVisitor(EntityDeserializerContext context, Map<String, Object> jsonObject) {
        this.context = Objects.requireNonNull(context);
        this.jsonObject = Objects.requireNonNull(jsonObject);
    }

    @Override
    public void start(TopiaEntity entity) {
    }

    @Override
    public void end(TopiaEntity entity) {
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
        Object jsonValue = getProperty(propertyName);
        if (jsonValue == null) {
            return;
        }
        if (isReferential(type)) {
            context.visitReferential((Entity) entity, propertyName, (Class) type, jsonValue);
            return;
        }
        if (isData(type)) {
            context.visitData((Entity) entity, propertyName, (Class) type, jsonValue);
            return;
        }
        context.visitSimple((Entity) entity, propertyName, type, jsonValue);
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
        Collection<?> jsonValue = (Collection<?>) getProperty(propertyName);
        if (jsonValue == null) {
            return;
        }
        if (isReferential(type)) {
            context.visitReferentialCollection((Entity) entity, propertyName, collectionType, (Class) type, jsonValue);
            return;
        }
        if (isData(type)) {
            context.visitDataCollection((Entity) entity, propertyName, collectionType, (Class) type, jsonValue);
            return;
        }
        context.visitCollection((Entity) entity, propertyName, collectionType, type, jsonValue);
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
    }

    @Override
    public void clear() {
    }

    protected boolean isReferential(Class<?> type) {
        return ReferentialEntity.class.isAssignableFrom(type);
    }

    protected boolean isData(Class<?> type) {
        return DataEntity.class.isAssignableFrom(type);
    }

    protected Object getProperty(String propertyName) {
        return jsonObject.get(propertyName);
    }
}
