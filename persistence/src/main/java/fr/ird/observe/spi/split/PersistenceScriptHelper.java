package fr.ird.observe.spi.split;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.util.sql.SqlScript;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 26/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.57
 */
public class PersistenceScriptHelper {
    private static final Logger log = LogManager.getLogger(PersistenceScriptHelper.class);

    public static void generateTckScripts(boolean h2, Path sourceFile, Path targetPath) throws IOException {

        Files.createDirectories(targetPath);

        ScriptSplitter.SplitResult context = ScriptSplitter.create(h2, SqlScript.of(sourceFile)).splitSchema();
        write(targetPath, context.getParent(), "%s.sql", "", true);

        ScriptSplitter.SplitNotSchemaResultPair splitNotSchemaResultPair = ScriptSplitter.splitDataAndReferential(context.getNotSchema());
        writeScript(targetPath, "%s.sql", "referential", splitNotSchemaResultPair.getReferential());
        writeScript(targetPath, "%s.sql", "data", splitNotSchemaResultPair.getData());
    }

    public static void write(Path scriptPath, ScriptSplitter.SplitSimpleResult result, String baseName, String classifier, boolean generateSchema) {
        try {
            if (generateSchema) {
                List<String> fullStatements = new LinkedList<>(result.getCreateSchemaStatements());
                fullStatements.addAll(result.getFinalizeSchemaStatements());
                writeScript(scriptPath, baseName, "schema" + classifier, fullStatements);
            } else {
                writeScript(scriptPath, baseName, "create" + classifier, result.getCreateSchemaStatements());
                writeScript(scriptPath, baseName, "finalize" + classifier, result.getFinalizeSchemaStatements());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void writeScript(Path targetPath, String basename, String classifier, ScriptSplitter.SplitNotSchemaResult data) throws IOException {
        Path targetFile = targetPath.resolve(String.format(basename, classifier));
        List<String> statements = new LinkedList<>(data.getInsertStatements());
        statements.addAll(data.getUpdateStatements());
        statements.addAll(data.getInsertAssociationStatements());
        writeScript(targetFile, statements);
    }

    public static void writeScript(Path targetPath, String baseName, String classifier, List<String> statements) throws IOException {
        String fileName = String.format(baseName, classifier);
        Path path = targetPath.resolve(fileName);
        writeScript(path, statements);
    }

    public static void writeScript(Path targetFile, List<String> statements) throws IOException {
        int insertsCount = statements.size();
        log.info(String.format("Generate %s (%d statements).", targetFile.toFile().getName(), insertsCount));
        Files.write(targetFile, statements);
    }
}
