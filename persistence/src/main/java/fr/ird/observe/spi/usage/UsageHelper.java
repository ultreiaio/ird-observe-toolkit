package fr.ird.observe.spi.usage;

/*-
 * #%L
 * Toolkit :: Persistence
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.spi.PersistenceBusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;
import org.nuiton.topia.service.sql.usage.TopiaUsageEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created on 04/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.53
 */
public class UsageHelper<D extends TopiaEntity> {

    private static final Logger log = LogManager.getLogger(UsageHelper.class);
    protected final boolean useReverseComposition;
    protected final boolean useReverseMandatoryComposition;
    protected final boolean useReverseAssociation;
    protected final boolean useReverseManyToManyAssociation;
    protected final boolean useReverseOneToManyAssociation;
    protected final boolean useReverseOneToOneCompositions;
    private final Supplier<TopiaUsageEntity> usageModel;
    private final TopiaPersistenceContext topiaPersistenceContext;

    public UsageHelper(Supplier<TopiaUsageEntity> usageModel,
                       TopiaPersistenceContext topiaPersistenceContext,
                       boolean useReverseComposition,
                       boolean useReverseMandatoryComposition,
                       boolean useReverseAssociation,
                       boolean useReverseManyToManyAssociation,
                       boolean useReverseOneToManyAssociation,
                       boolean useReverseOneToOneCompositions) {
        this.usageModel = usageModel;
        this.topiaPersistenceContext = topiaPersistenceContext;
        this.useReverseComposition = useReverseComposition;
        this.useReverseMandatoryComposition = useReverseMandatoryComposition;
        this.useReverseAssociation = useReverseAssociation;
        this.useReverseManyToManyAssociation = useReverseManyToManyAssociation;
        this.useReverseOneToManyAssociation = useReverseOneToManyAssociation;
        this.useReverseOneToOneCompositions = useReverseOneToOneCompositions;
    }

    public final UsageCount count(D entity) {
        return count(entity, (Predicate<Class<? extends TopiaEntity>>) null);
    }

    public final UsageCount count(D entity, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        Map<Class<? extends TopiaEntity>, Set<String>> entitiesCountByIds = new LinkedHashMap<>();
        count(entity, entitiesCountByIds, optionalPredicate);
        Map<Class<? extends TopiaEntity>, Long> entitiesCount = new LinkedHashMap<>();
        entitiesCountByIds.forEach((k, v) -> entitiesCount.put(k, (long) v.size()));
        long compositionsSum = entitiesCount.values().stream().reduce(Long::sum).orElse(0L);
        UsageCount result = new UsageCount();
        if (compositionsSum > 0) {
            log.info(String.format("Found %d entity usage(s) from entity: %s", compositionsSum, entity.getTopiaId()));
            buildCountUsageResult(entitiesCount, result);
        }
        return result;
    }

    public final <E extends Entity> Collection<E> find(D entity, Class<E> entityType) {
        Set<E> result = new TreeSet<>(Comparator.comparing(Entity::getTopiaId));
        find(entity, entityType, result);
        return result;
    }

    protected void count(D entity, Map<Class<? extends TopiaEntity>, Set<String>> entitiesCount, Predicate<Class<? extends TopiaEntity>> optionalPredicate) {
        TopiaUsageEntity usage = usageModel.get();
        if (useReverseComposition) {
            usage.countReverseCompositions(topiaPersistenceContext, entity, e -> true, entitiesCount, optionalPredicate);
        }
        if (useReverseMandatoryComposition) {
            usage.countReverseMandatoryCompositions(topiaPersistenceContext, entity, e -> true, entitiesCount, optionalPredicate);
        }
        if (useReverseAssociation) {
            usage.countReverseAssociations(topiaPersistenceContext, entity, e -> true, entitiesCount, optionalPredicate);
        }
        if (useReverseManyToManyAssociation) {
            usage.countReverseManyToManyAssociations(topiaPersistenceContext, entity, PersistenceBusinessProject::isData, entitiesCount, optionalPredicate);
        }
        if (useReverseOneToManyAssociation) {
            usage.countReverseOneToManyAssociations(topiaPersistenceContext, entity, PersistenceBusinessProject::isData, entitiesCount, optionalPredicate);
        }
        if (useReverseOneToOneCompositions) {
            usage.countReverseOneToOneCompositions(topiaPersistenceContext, entity, PersistenceBusinessProject::isData, entitiesCount, optionalPredicate);
        }
    }

    protected <E extends Entity> void find(D entity, Class<E> entityType, Set<E> result) {
        TopiaUsageEntity usage = usageModel.get();
        if (useReverseComposition) {
            usage.findReverseComposition(topiaPersistenceContext, entity, entityType, result);
        }
        if (useReverseMandatoryComposition) {
            usage.findReverseMandatoryComposition(topiaPersistenceContext, entity, entityType, result);
        }
        if (useReverseAssociation) {
            usage.findReverseAssociation(topiaPersistenceContext, entity, entityType, result);
        }
        if (useReverseManyToManyAssociation) {
            usage.findReverseManyToManyAssociation(topiaPersistenceContext, entity, entityType, result);
        }
        if (useReverseOneToManyAssociation) {
            usage.findReverseOneToManyAssociation(topiaPersistenceContext, entity, entityType, result);
        }
        if (useReverseOneToOneCompositions) {
            usage.findReverseOneToOneComposition(topiaPersistenceContext, entity, entityType, result);
        }
    }

    protected void buildCountUsageResult(Map<Class<? extends TopiaEntity>, Long> compositions, UsageCount result) {
        for (Map.Entry<Class<? extends TopiaEntity>, Long> entry : compositions.entrySet()) {
            @SuppressWarnings("unchecked") Class<? extends Entity> type = (Class<? extends Entity>) entry.getKey();
            Class<? extends BusinessDto> dtoType = PersistenceBusinessProject.fromEntity(type).toDtoType();
            Long entities = entry.getValue();
            result.merge(dtoType, entities, Long::sum);
        }
    }
}
