# ObServe Toolkit changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-10-09 10:29.

## Version [10.0.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/156)

**Closed at 2024-10-09.**


### Issues
  * [[bug 116]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/116) **Fix deploy issue (nexus-stating must be declare in plugins as extension)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 115]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/115) **Remove some ObServe business objects (to move to ObServe project)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [10.0.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/155)
Back from ird-observe project :smile: 

**Closed at 2024-10-02.**


### Issues
  * [[enhancement 112]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/112) **Project is back from hell** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 113]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/113) **Introduce ReferentialSecondLevelDeepBehaviourModel and referentialSecondLevelDeepBehaviour tagValue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 114]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/114) **Remove usage of paren pom** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [6.1.5](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/154)

**Closed at 2022-06-10.**


### Issues
No issue.

## Version [6.1.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/153)

**Closed at 2022-05-26.**


### Issues
No issue.

## Version [6.1.3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/152)

**Closed at 2022-05-18.**


### Issues
No issue.

## Version [6.1.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/151)

**Closed at 2022-05-08.**


### Issues
No issue.

## Version [6.1.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/150)

**Closed at 2022-05-04.**


### Issues
No issue.

## Version [6.1.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/149)
Add webmotion + many fixes

**Closed at 2022-05-04.**


### Issues
No issue.

## Version [6.0.10](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/148)

**Closed at 2022-04-26.**


### Issues
No issue.

## Version [6.0.9](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/147)

**Closed at 2022-04-20.**


### Issues
No issue.

## Version [6.0.8](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/146)

**Closed at 2022-04-14.**


### Issues
No issue.

## Version [6.0.7](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/145)

**Closed at 2022-03-20.**


### Issues
No issue.

## Version [6.0.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/144)

**Closed at *In progress*.**


### Issues
No issue.

## Version [6.0.5](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/143)

**Closed at 2022-03-01.**


### Issues
No issue.

## Version [6.0.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/142)

**Closed at 2022-02-21.**


### Issues
No issue.

## Version [6.0.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/141)

**Closed at 2022-02-16.**


### Issues
No issue.

## Version [6.0.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/140)

**Closed at *In progress*.**


### Issues
No issue.

## Version [6.0.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/139)

**Closed at 2022-02-14.**


### Issues
No issue.

## Version [5.0.65](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/138)

**Closed at *In progress*.**


### Issues
No issue.

## Version [5.0.64](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/137)

**Closed at 2022-02-03.**


### Issues
  * [[enhancement 110]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/110) **Improve server config file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 111]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/111) **Introduce ng navigation metamodel** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.63](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/136)

**Closed at 2021-12-14.**


### Issues
No issue.

## Version [5.0.62](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/135)

**Closed at 2021-12-14.**


### Issues
No issue.

## Version [5.0.61](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/134)

**Closed at *In progress*.**


### Issues
No issue.

## Version [5.0.60](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/133)

**Closed at *In progress*.**


### Issues
No issue.

## Version [5.0.59](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/132)

**Closed at 2021-12-02.**


### Issues
No issue.

## Version [5.0.58](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/131)

**Closed at 2021-11-29.**


### Issues
No issue.

## Version [5.0.57](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/130)

**Closed at 2021-11-28.**


### Issues
  * [[enhancement 109]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/109) **Review tck API and tests** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.56](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/129)

**Closed at 2021-11-24.**


### Issues
No issue.

## Version [5.0.55](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/128)

**Closed at 2021-11-23.**


### Issues
  * [[enhancement 107]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/107) **replace graalvm by rhino (20Mo less in release^^)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 108]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/108) **Add more definitions on entity spi (entity definition, create / update validation definition)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.53](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/126)

**Closed at 2021-11-08.**


### Issues
  * [[enhancement 104]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/104) **Add tag value to decompose timeStamp to date and time dto property and generate the code** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 105]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/105) **Clean validators** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 106]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/106) **Improve UsageService to be usable also for data** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.52](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/125)

**Closed at 2021-10-27.**


### Issues
  * [[bug 102]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/102) **Gestion des dates de mises à jour lors de suppression de référentiels via les synchronisations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 103]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/103) **Toujours utiliser le même ordre lors du calcul de différentiel de référentiel.** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.51](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/124)

**Closed at 2021-10-26.**


### Issues
No issue.

## Version [5.0.50](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/123)

**Closed at 2021-10-25.**


### Issues
No issue.

## Version [5.0.49](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/122)

**Closed at 2021-10-08.**


### Issues
No issue.

## Version [5.0.48](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/121)

**Closed at 2021-10-02.**


### Issues
No issue.

## Version [5.0.47](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/120)

**Closed at *In progress*.**


### Issues
No issue.

## Version [5.0.46](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/119)

**Closed at 2021-09-29.**


### Issues
No issue.

## Version [5.0.45](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/118)

**Closed at 2021-09-27.**


### Issues
No issue.

## Version [5.0.44](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/117)

**Closed at 2021-09-23.**


### Issues
No issue.

## Version [5.0.43](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/116)

**Closed at 2021-09-20.**


### Issues
No issue.

## Version [5.0.42](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/115)

**Closed at *In progress*.**


### Issues
No issue.

## Version [5.0.41](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/114)

**Closed at 2021-09-14.**


### Issues
No issue.

## Version [5.0.40](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/113)

**Closed at 2021-09-11.**


### Issues
No issue.

## Version [5.0.39](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/112)

**Closed at 2021-08-19.**


### Issues
  * [[bug 91]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/91) **Fix generate services local for new services** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 87]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/87) **Remove usages of ImmutableSet, ImmutableList and ImmutableMap** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 88]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/88) **Improve PingService** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 89]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/89) **Remove generic on Entity, and hard link to dto and reference** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 90]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/90) **Generate differential properties in SPI** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 92]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/92) **Introduce Synchronize service** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 93]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/93) **Review spi package** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 94]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/94) **Review UsageService and related** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 95]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/95) **Introduce InlineDataDto** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 96]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/96) **Introduce new tag values to characterize tableColumns (InlineDataDto and ContainerChildDto)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 97]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/97) **Add a new dto template **ConsolidateDtoModel** to generate missing tagValues by convention** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 98]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/98) **Add new I18n labels API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.38](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/111)

**Closed at 2021-07-07.**


### Issues
  * [[bug 86]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/86) **Revert previous ImmutableSetAdapter modification** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.37](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/110)

**Closed at 2021-07-06.**


### Issues
  * [[enhancement 84]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/84) **Improve ImmutableSet from gson** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.36](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/109)

**Closed at 2021-07-06.**


### Issues
No issue.

## Version [5.0.35](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/108)

**Closed at 2021-07-05.**


### Issues
  * [[enhancement 82]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/82) **Improve EntityTransformer (do not generate toDto and fromDto method inside entity)** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 83]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/83) **Improve server generators (filters part)** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.34](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/107)

**Closed at 2021-07-02.**


### Issues
  * [[bug 81]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/81) **Clarify what EntitySerializer should do about loadReferential and improve the result (always a list)** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 78]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/78) **Improve ReferentialEntityService** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 79]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/79) **Improve server generators** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 80]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/80) **Improve BusinessProjectVisitor** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.33](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/106)

**Closed at 2021-07-01.**


### Issues
  * [[bug 77]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/77) **Fix documentation generators** (Thanks to ) (Reported by Tony CHEMIT)

## Version [5.0.32](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/105)

**Closed at 2021-06-30.**


### Issues
  * [[bug 73]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/73) **Make ToolkitRequestFilter simple bean (setters must be public)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 74]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/74) **Normalize ReferentialEntityService with DataEntityService** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 75]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/75) **Update documentation generators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 76]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/76) **Generate EntityFilterConsumer in Spi** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.31](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/104)

**Closed at 2021-06-30.**


### Issues
  * [[enhancement 72]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/72) **Add caseSensitive in ToolkitRequestFilter** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.30](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/103)

**Closed at 2021-06-29.**


### Issues
  * [[enhancement 71]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/71) **Introduce ToolkitRequestFilter and filter method in DtoEntityContext** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.29](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/102)

**Closed at 2021-06-25.**


### Issues
  * [[enhancement 70]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/70) **Always sort tag values and store them back** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.28](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/101)

**Closed at 2021-06-23.**


### Issues
No issue.

## Version [5.0.27](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/100)

**Closed at 2021-06-18.**


### Issues
  * [[bug 68]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/68) **Fix some issues with decorators (with species/scientificLabel) and others...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 67]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/67) **Review CollectionUniqueKey validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 69]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/69) **Add a new SqlService** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.26](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/99)

**Closed at 2021-06-05.**


### Issues
  * [[enhancement 65]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/65) **Add new method to create entity with a fixed id prefix** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 66]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/66) **Add a new method to get all referential entities of a type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.25](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/98)

**Closed at 2021-05-19.**


### Issues
  * [[enhancement 64]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/64) **Improve web services documentation skin** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.24](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/97)

**Closed at 2021-05-18.**


### Issues
  * [[enhancement 58]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/58) **Improve server API again!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 59]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/59) **Review DtoEntityContext, make it more alive** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 60]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/60) **Improve how to deal with validation files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 61]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/61) **Introduce Temporal validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 62]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/62) **Improve validators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 63]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/63) **Remove obsolete maven goals** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.23](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/96)

**Closed at 2021-05-07.**


### Issues
  * [[enhancement 55]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/55) **Remove AbstractObserveDto** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 56]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/56) **Review User dto and augments with DataSourceApiAccess and DataSourceValidationMode** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 57]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/57) **Rethink gson usage and simplify it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.22](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/95)

**Closed at 2021-05-05.**


### Issues
  * [[enhancement 45]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/45) **Improve EntityDeserializer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 46]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/46) **Improve generated public API documentation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 47]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/47) **Improve GsonBuilder** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 48]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/48) **Introduce runner-server module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 49]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/49) **Introduce runner-persistence module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 50]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/50) **Introduce runner-navigation module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 51]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/51) **Introduce ToolkitRequestConfig to store any configuration around json building in public API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 52]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/52) **Generate fixtures before generating documentation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 53]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/53) **Generate simple entity description in json** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 54]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/54) **Make create and update works for the available public API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.21](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/94)

**Closed at 2021-05-02.**


### Issues
  * [[bug 41]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/41) **Improve WellIdAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 42]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/42) **Improve some decorators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 43]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/43) **Improve EntitySerializer to get content and references separated** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 44]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/44) **Review API services to implements create and update** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.20](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/93)

**Closed at 2021-05-01.**


### Issues
  * [[bug 40]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/40) **Fix getParentId lasUpdateDate for multiple association** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.19](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/92)

**Closed at 2021-04-30.**


### Issues
  * [[enhancement 34]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/34) **Generate name queries in entity mapping to use dto projection on hibernate** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 35]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/35) **Replace ShortIdDto by ToolkitDtoIdBean** (Thanks to ) (Reported by Tony CHEMIT)
  * [[enhancement 36]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/36) **Improve ToParentIdProvider and use generated getParentId sql query (moreover have it for any persisted data, not only for navigation data)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 37]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/37) **Improve how to load data to root in a navigation node (now get all the data then fill the parent userObject)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 38]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/38) **Sanitize Navigation API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 39]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/39) **Introduce BusinessProjectVisitor and use it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.18](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/91)

**Closed at 2021-04-26.**


### Issues
  * [[enhancement 33]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/33) **Generate API documentation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.17](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/90)

**Closed at 2021-04-20.**


### Issues
No issue.

## Version [5.0.16](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/89)

**Closed at 2021-03-27.**


### Issues
  * [[enhancement 32]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/32) **Improve BusinessProject I18n layouts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.15](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/88)

**Closed at 2021-03-23.**


### Issues
  * [[enhancement 31]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/31) **Improve WellIdAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.14](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/87)

**Closed at 2021-03-20.**


### Issues
  * [[enhancement 30]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/30) **Be able to bind dto to entity data reference set** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.13](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/86)

**Closed at 2021-03-20.**


### Issues
  * [[bug 28]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/28) **Some exceptions are not translated (and moreover should be in another module...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 29]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/29) **Remove DataSourceService.destroy** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.12](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/85)

**Closed at 2021-03-18.**


### Issues
  * [[enhancement 26]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/26) **Introduce WellIdAware** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 27]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/27) **Be able to add mainDto and form tag value on other packages** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.11](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/84)

**Closed at 2021-03-15.**


### Issues
  * [[bug 25]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/25) **ExtractSchema does not filter on schema and require tableName property :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.10](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/83)

**Closed at 2021-03-14.**


### Issues
  * [[enhancement 22]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/22) **Improve OpenableService.move API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 23]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/23) **Introduce a nice and simple ReplicateDataEngine** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 24]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/24) **Introduce PersistenceResult to perform better save in services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.9](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/82)

**Closed at 2021-03-12.**


### Issues
No issue.

## Version [5.0.8](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/81)

**Closed at 2021-03-11.**


### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/20) **Better exception management for service client** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 21]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/21) **Improve some storage templates** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.7](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/80)

**Closed at 2021-03-08.**


### Issues
  * [[enhancement 19]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/19) **Review and move to dto DataSourceConfiguration API and his refactor the templates linked to it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/79)

**Closed at 2021-02-22.**


### Issues
  * [[enhancement 17]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/17) **Fix ExtractTable and improve others generators** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 18]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/18) **Use last topia-extension API and improve it usage.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.5](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/78)

**Closed at 2021-02-19.**


### Issues
  * [[enhancement 15]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/15) **Improve some data services** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 16]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/16) **Introduce ShortIdDto** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/77)

**Closed at 2021-02-18.**


### Issues
  * [[bug 13]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/13) **Fix decorator comparator on date-time tokens** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 14]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/14) **Introduce fr.ird.observe.maven.plugins.toolbox.persistence.GenerateTckArchive** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/76)

**Closed at 2021-02-16.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/12) **Improve fr.ird.observe.entities.ObserveTopiaPersistenceContextSupport#isTopiaId method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/75)

**Closed at 2021-02-11.**


### Issues
  * [[bug 8]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/8) **DtoEntityContext.toReference does not use entity user code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 9]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/9) **fr.ird.observe.spi.map.ImmutableSetStringMap bad design** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 11]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/11) **Fix LazyDataDtoReferenceSet.reload (in fact is never reload...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/10) **Add nice method to filter reference types** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/74)

**Closed at 2021-02-06.**


### Issues
  * [[bug 5]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/5) **Bad unique tag value loading** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 2]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/2) **Replace nuiton-validator by java4all validation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/3) **Use http-api Write annotation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 4]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/4) **Improve GenerateEmptyDatabases finalize script (sorting it and remove bad hibernate uk)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 6]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/6) **Improve schema extraction code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 7]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/7) **Introduce tagValue skipCopyToEntity to avoid copy back from dto on transient properties** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [5.0.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/73)

**Closed at 2021-01-31.**


### Issues
No issue.

## Version [4.35](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/70)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.34](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/69)

**Closed at 2020-06-27.**


### Issues
No issue.

## Version [4.33](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/68)

**Closed at 2020-06-27.**


### Issues
No issue.

## Version [4.32](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/67)

**Closed at 2020-05-17.**


### Issues
No issue.

## Version [4.31](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/66)

**Closed at 2020-05-15.**


### Issues
No issue.

## Version [4.30](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/65)

**Closed at 2020-05-12.**


### Issues
No issue.

## Version [4.29](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/63)

**Closed at 2020-04-15.**


### Issues
No issue.

## Version [4.28](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/62)

**Closed at 2020-04-13.**


### Issues
No issue.

## Version [4.27](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/60)

**Closed at 2020-03-30.**


### Issues
No issue.

## Version [4.26](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/57)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.25](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/56)

**Closed at 2020-03-24.**


### Issues
No issue.

## Version [4.24](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/50)

**Closed at 2020-03-22.**


### Issues
No issue.

## Version [4.23](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/49)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.22](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/48)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.21](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/47)

**Closed at 2020-01-03.**


### Issues
No issue.

## Version [4.20](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/46)

**Closed at 2019-12-11.**


### Issues
No issue.

## Version [4.19](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/45)

**Closed at 2019-09-28.**


### Issues
No issue.

## Version [4.18](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/44)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.17](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/43)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.16](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/42)

**Closed at 2019-08-05.**


### Issues
No issue.

## Version [4.15](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/40)

**Closed at 2019-07-14.**


### Issues
No issue.

## Version [4.14](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/39)

**Closed at 2019-07-02.**


### Issues
No issue.

## Version [4.13](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/38)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.12](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/37)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.11](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/36)

**Closed at 2019-06-03.**


### Issues
No issue.

## Version [4.10](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/33)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.8](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/32)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.7](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/31)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/30)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/25)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/24)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/23)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/21)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/20)

**Closed at 2018-05-22.**


### Issues
No issue.

## Version [3.8](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/19)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.31](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/72)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.30](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/71)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.29](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/64)

**Closed at 2020-07-01.**


### Issues
No issue.

## Version [3.7.28](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/61)

**Closed at 2020-04-14.**


### Issues
No issue.

## Version [3.7.27](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/59)

**Closed at 2020-03-29.**


### Issues
No issue.

## Version [3.7.26](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/58)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.25](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/55)

**Closed at 2020-03-24.**


### Issues
No issue.

## Version [3.7.24](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/54)

**Closed at 2020-03-19.**


### Issues
No issue.

## Version [3.7.23](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/53)

**Closed at 2020-03-07.**


### Issues
No issue.

## Version [3.7.22](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/52)

**Closed at 2020-03-04.**


### Issues
No issue.

## Version [3.7.21](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/51)

**Closed at 2020-02-17.**


### Issues
No issue.

## Version [3.7.20](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/41)

**Closed at 2019-09-16.**


### Issues
No issue.

## Version [3.7.16](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/35)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.9](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/29)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.8](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/28)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.7](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/27)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/26)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.7.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/22)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/issues/1) **Improve topia migration execution time using a batch** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.7](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/18)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/17)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.5](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/16)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/15)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/14)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/13)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/12)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/11)

**Closed at *In progress*.**


### Issues
No issue.

## Version [2.0-alpha-3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [2.0-alpha-2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/9)

**Closed at *In progress*.**


### Issues
No issue.

## Version [2.0-alpha-1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.6](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.5](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/6)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.4](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/5)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.3](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.2](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.0](https://gitlab.com/ultreiaio/ird-observe-toolkit/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

