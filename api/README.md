# Abstract

The toolkit-api module offers the very common API :

  * **DTO** api
  * **Navigation** api
  * **SPI** tools
  * **Test** fixtures api