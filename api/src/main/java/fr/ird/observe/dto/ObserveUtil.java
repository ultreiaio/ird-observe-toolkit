package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.config.ApplicationConfig;
import io.ultreia.java4all.config.ConfigResource;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.hashids.Hashids;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Pour mettre du code util commun.
 * <p>
 * Created on 01/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@SuppressWarnings("rawtypes")
public class ObserveUtil {

    public static final String SQL_GZ_EXTENSION_PATTERN = "^.+\\.sql\\.gz|.+\\.SQL\\.GZ$";
    public static final String SQL_GZ_EXTENSION = ".sql.gz";
    public static final String CSV_EXTENSION = ".csv";
    public static final String PROPERTIES_EXTENSION_PATTERN = "^.+\\.properties|.+\\.PROPERTIES$";
    public static final String PROPERTIES_EXTENSION = ".properties";
    public static final String PNG_EXTENSION_PATTERN = "^.+\\.png|.+\\.PNG$";
    public static final String PNG_EXTENSION = ".png";
    public static final String JS_ENGINE_NAME = "rhino";
    private static final Hashids ID_GENERATOR = new Hashids("ObServeHasSomeSalt", 8, "0123456789#abcdefghijklmnopqrestuvwxyz");
    private static final Logger log = LogManager.getLogger(ObserveUtil.class);

    public static String newUUID(Date now) {
        return ID_GENERATOR.encode(now.getTime());
    }

    public static <C extends Class<?>> List<C> sortTypes(Collection<C> types, Function<Class, String> function, Locale locale) {

        List<C> list = new ArrayList<>(types);
        new ClassComparator<C>(function, locale).sort(list);
        return list;

    }

    public static void loadLogConfiguration(ConfigResource resource, Path sharedFile, Path logFile, Supplier<ApplicationConfig> configSupplier) {
        URL logInput = resource.getUrl();
        if (sharedFile != null && Files.exists(sharedFile)) {
            try {
                logInput = sharedFile.toUri().toURL();
            } catch (MalformedURLException e) {
                // can't happen
            }
        }
        ConfigResource.loadFile(logInput, logFile, configSupplier);
        Configurator.initialize(null, logFile.toFile().getAbsolutePath());
    }

    public static void cleanMemory() {
        System.runFinalization();
        System.gc();
    }

    public static String addSqlGzExtension(String filePath) {
        if (!filePath.endsWith(SQL_GZ_EXTENSION)) {
            filePath += SQL_GZ_EXTENSION;
        }
        return filePath;
    }

    public static String removeSqlGzExtension(String filePath) {
        if (filePath.endsWith(SQL_GZ_EXTENSION)) {
            filePath = Strings.removeEnd(filePath, SQL_GZ_EXTENSION);
        }
        return filePath;
    }

    public static boolean withSqlGzExtension(String filePath) {
        return filePath.endsWith(SQL_GZ_EXTENSION);
    }

    public static String addCsvExtension(String filePath) {
        if (!filePath.endsWith(CSV_EXTENSION)) {
            filePath += CSV_EXTENSION;
        }
        return filePath;
    }

    public static String removeCsvExtension(String filePath) {
        if (filePath.endsWith(CSV_EXTENSION)) {
            filePath = Strings.removeEnd(filePath, CSV_EXTENSION);
        }
        return filePath;
    }

    public static boolean withCsvExtension(String filePath) {
        return filePath.endsWith(CSV_EXTENSION);
    }

    public static ScriptEngine getScriptEngine() {
        ScriptEngineManager factory = new ScriptEngineManager();
        return factory.getEngineByName(JS_ENGINE_NAME);
    }

    public static String getIconSuffix(BusinessModule module, BusinessSubModule subModule) {
        return module.getName().endsWith(subModule.getName()) ? "" : ("_" + module.getName().toLowerCase());
    }

    public static <T> Class<T> getFirstType(Object o) {
        return getType(o, 0);
    }

    public static <T> Class<T> getSecondType(Object o) {
        return getType(o, 1);
    }

    public static <T> Class<T> getThirdType(Object o) {
        return getType(o, 2);
    }

    public static <T> Class<T> getForthType(Object o) {
        return getType(o, 2);
    }

    public static <T> Class<T> getType(Object o, int index) {
        return getClassType(o.getClass(), index);
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClassType(Class<?> o, int index) {
        Type genericSuperclass = o.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            Type actualTypeArgument = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[index];
            if (actualTypeArgument instanceof ParameterizedType) {
                actualTypeArgument = ((ParameterizedType) actualTypeArgument).getRawType();
            }
            return (Class<T>) actualTypeArgument;
        }
        throw new IllegalStateException("Object does not extends an abstract class: " + o.getName());
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClassType(Class<?> o, String genericName) {
        Type genericSuperclass = o.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            Type[] rawTypeArguments = ((ParameterizedType) ((Class) ((ParameterizedType) genericSuperclass).getRawType()).getGenericSuperclass()).getActualTypeArguments();
            int index = -1;
            for (int i = 0; i < rawTypeArguments.length; i++) {
                Type rawTypeArgument = rawTypeArguments[i];
                if (genericName.equals(rawTypeArgument.getTypeName())) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                throw new IllegalStateException(String.format("Can't find generic %s class: %s", genericName, o.getName()));
            }
            Type actualTypeArgument = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[index];
            if (actualTypeArgument instanceof ParameterizedType) {
                actualTypeArgument = ((ParameterizedType) actualTypeArgument).getRawType();
            }
            return (Class<T>) actualTypeArgument;
        }
        if (o.getSuperclass() != null) {
            return getClassType(o.getSuperclass(), genericName);
        }
        throw new IllegalStateException("Object does not extends an abstract class: " + o.getName());
    }

    public static boolean isEmpty(Collection<?> o) {
        return o == null || o.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> o) {
        return !isEmpty(o);
    }

    /**
     * @param jsonString the json string in compact mode
     * @return the gson in a pretty mode
     */
    public static String toPrettyFormat(String jsonString) {
        JsonObject json = JsonParser.parseString(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }

    public static SingletonSupplier<String> loadResourceContentSupplier(URL url, Function<String, String> contentTransformer) {
        return SingletonSupplier.of(() -> {
            String content = loadResourceContent(url);
            return contentTransformer == null ? content : contentTransformer.apply(content);
        });
    }

    public static String loadResourceContent(URL url) {
        log.info("Loading resource content: {}", url);
        try (InputStream in = url.openStream()) {
            return new String(in.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Function<String, String> removeJavaLicense() {
        return content -> content.substring(content.indexOf("*/") + 2).trim();
    }

    public static void createDirectories(Path path) {
        try {
            if (Files.notExists(path)) {
                Files.createDirectories(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("rawtypes")
    private static class ClassComparator<C extends Class<?>> implements Comparator<C> {

        private final Map<Class, String> cache;
        private final Function<Class, String> function;

        private final Collator collator;

        private ClassComparator(Function<Class, String> function, Locale locale) {
            this.cache = new HashMap<>();
            this.function = function;
            this.collator = Collator.getInstance(locale);
            this.collator.setStrength(Collator.PRIMARY);
        }

        @Override
        public int compare(Class o1, Class o2) {
            String s1 = getValue(o1);
            String s2 = getValue(o2);
            return this.collator.compare(s1, s2);
        }

        String getValue(Class klass) {
            return cache.computeIfAbsent(klass, k -> function.apply(klass));
        }

        public void sort(List<C> list) {
            list.sort(this);
            cache.clear();
        }
    }
}
