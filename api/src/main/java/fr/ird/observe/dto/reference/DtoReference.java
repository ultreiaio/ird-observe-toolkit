package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitId;

import java.util.Objects;
import java.util.Set;

/**
 * Created on 21/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class DtoReference extends BusinessDto implements DtoReferenceAware {

    public static final String PROPERTY_TYPE = "type";

    public void fromToolkitId(ToolkitId dto) {
        Objects.requireNonNull(dto);
        this.id = dto.getTopiaId();
        this.lastUpdateDate = dto.getLastUpdateDate();
    }

    public void fromDtoReferenceAware(DtoReferenceAware dto) {
        fromToolkitId(dto);
        this.topiaVersion = dto.getTopiaVersion();
        this.topiaCreateDate = dto.getTopiaCreateDate();
        if (dto instanceof BusinessDto) {
            setHomeId(((BusinessDto) dto).getHomeId());
        }
    }

    public abstract DtoReferenceDefinition<?, ?> getDefinition();

    public abstract Class<? extends BusinessDto> getDtoType();

    public abstract Class<? extends DtoReference> getReferenceType();

    @SuppressWarnings("unchecked")
    public <R extends DtoReference> Class<R> getReferenceType0() {
        return (Class<R>) getReferenceType();
    }

    public <O> O getPropertyValue(String propertyName) {
        return get(propertyName);
    }

    public Set<String> getPropertyNames() {
        return getDefinition().getPropertyNames();
    }

}
