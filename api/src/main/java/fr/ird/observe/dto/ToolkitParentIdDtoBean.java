package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.StringJoiner;

/**
 * Created on 29/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
public class ToolkitParentIdDtoBean extends ToolkitIdDtoBean {

    /**
     * The data link from where parent comes.
     */
    private final String linkName;

    public ToolkitParentIdDtoBean(Class<? extends BusinessDto> type, String id, Date lastUpdateDate, String linkName) {
        super(type, id, lastUpdateDate);
        this.linkName = linkName;
    }

    public String getLinkName() {
        return linkName;
    }

    @Override
    public ToolkitParentIdDtoBean of(String id) {
        return new ToolkitParentIdDtoBean(getType(), id, getLastUpdateDate(), linkName);
    }

    @Override
    public ToolkitParentIdDtoBean of(Date timestamp) {
        return new ToolkitParentIdDtoBean(getType(), getId(), timestamp, linkName);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ToolkitParentIdDtoBean.class.getSimpleName() + "[", "]")
                .add("id='" + getId() + "'")
                .add("type=" + getType().getName())
                .add("linkName=" + getLinkName())
                .add(getLastUpdateDate() == null ? "" : "lastUpdateDate=" + getLastUpdateDate())
                .toString();
    }
}
