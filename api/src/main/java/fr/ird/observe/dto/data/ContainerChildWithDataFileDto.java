package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 02/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class ContainerChildWithDataFileDto extends ContainerChildDto implements WithDataFile {

    public static final String PROPERTY_HAS_DATA = "hasData";
    public static final String PROPERTY_DATA_LOCATION = "dataLocation";
    public static final String PROPERTY_DATA = "data";

    protected boolean hasData;
    protected String dataLocation;
    protected DataFileDto data;

    @Override
    public boolean isHasData() {
        return hasData;
    }

    @Override
    public void setHasData(boolean hasData) {
        boolean oldValue = isHasData();
        this.hasData = hasData;
        firePropertyChange(PROPERTY_HAS_DATA, oldValue, hasData);
    }

    @Override
    public String getDataLocation() {
        return dataLocation;
    }

    @Override
    public void setDataLocation(String dataLocation) {
        String oldValue = getDataLocation();
        this.dataLocation = dataLocation;
        firePropertyChange(PROPERTY_DATA_LOCATION, oldValue, dataLocation);
    }

    @Override
    public DataFileDto getData() {
        return data;
    }

    @Override
    public void setData(DataFileDto data) {
        DataFileDto oldValue = getData();
        this.data = data;
        firePropertyChange(PROPERTY_DATA, oldValue, data);
    }
}
