package fr.ird.observe.dto.referential;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

public class I18nReferentialHelper {

    public static <E extends WithI18n> String getLabel(int referenceLocaleOrdinal, E i18nDto) {

        String result = null;

        switch (referenceLocaleOrdinal + 1) {
            case 1:
                result = i18nDto.getLabel1();
                break;
            case 2:
                result = i18nDto.getLabel2();
                break;
            case 3:
                result = i18nDto.getLabel3();
                break;
            case 4:
                result = i18nDto.getLabel4();
                break;
            case 5:
                result = i18nDto.getLabel5();
                break;
            case 6:
                result = i18nDto.getLabel6();
                break;
            case 7:
                result = i18nDto.getLabel7();
                break;
            case 8:
                result = i18nDto.getLabel8();
                break;
        }

        return result;

    }

    public static <E extends WithI18n> String getLabel(Locale locale, E i18nDto) {
        String result = null;
        switch (locale.getLanguage()) {
            case "en":
                result = i18nDto.getLabel1();
                break;
            case "es":
                result = i18nDto.getLabel3();
                break;
            // default is french since this is the best translation quality
            default:
                result = i18nDto.getLabel2();
                break;
        }
        return result;

    }

    public static <E extends WithI18n> void setLabel(int referenceLocaleOrdinal, E i18nEntity, String label) {

        switch (referenceLocaleOrdinal + 1) {
            case 1:
                i18nEntity.setLabel1(label);
                break;
            case 2:
                i18nEntity.setLabel2(label);
                break;
            case 3:
                i18nEntity.setLabel3(label);
                break;
            case 4:
                i18nEntity.setLabel4(label);
                break;
            case 5:
                i18nEntity.setLabel5(label);
                break;
            case 6:
                i18nEntity.setLabel6(label);
                break;
            case 7:
                i18nEntity.setLabel7(label);
                break;
            case 8:
                i18nEntity.setLabel8(label);
                break;
        }

    }

    public static String getPropertyName(int referenceLocaleOrdinal) {
        return "label" + (referenceLocaleOrdinal + 1);
    }

}
