package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Represents a main root data.
 * <p>
 * A such data has a node in edit model (and of course in select model).
 * <p>
 * It can be create, update, delete but not move.
 * <p>
 * A such data has no parent, but one or many discriminant.
 * <p>
 * In client, you will have always many forms for a such data :
 *
 *  <ul>
 *      <li>Many for the list of all data for his discriminant</li>
 *      <li>One for the viewing/editing a such data</li>
 *  </ul>
 * <p>
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class RootOpenableDto extends DataDto {
}
