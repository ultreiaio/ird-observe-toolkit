package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.module.BusinessModule;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created on 05/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DataGroupByDefinition<D, F extends DataGroupBy<D>> {
    /**
     * Add this flavor on request for all to get no empty group by.
     *
     * <b>Note:</b> Only apply for qualitative definition.
     */
    public static final String FLAVOR_NO_EMPTY = "-noEmpty";
    /**
     * All this flavor on request for all to get no disabled group by.
     *
     * <b>Note:</b> Only apply for qualitative definition.
     */
    public static final String FLAVOR_NO_DISABLED = "-noDisabled";
    /**
     * Group by name (unique).
     */
    private final String name;
    /**
     * Type of groupBy.
     */
    private final DataGroupByType type;
    /**
     * Data type (means on which we groupBy, a RootOpenable object).
     */
    private final Class<D> dataType;
    /**
     * Optional second level data type (type of intermediate type).
     */
    private final Class<?> dataTypeSecondLevel;
    /**
     * Group by property name.
     */
    private final String propertyName;
    /**
     * Optional group by second level property name, can only be used with a qualitative type.
     */
    private final String propertySecondLevelName;
    /**
     * Group by property type (a data object attribute type, may be referential, date, string, ...).
     */
    private final Class<?> propertyType;
    /**
     * Is groupBy mandatory ? (this will modify possible options)
     */
    private final boolean propertyMandatory;
    /**
     * Container type (a GroupBy object).
     */
    private final Class<F> containerType;
    /**
     * To create a new container.
     */
    private final Supplier<F> containerSupplier;
    /**
     * Query name to count children for all group by values.
     */
    private final String countAllQueryName;
    /**
     * Query name to count children for one group by value.
     */
    private final String countOneQueryName;
    /**
     * Query name to count children for null group.
     */
    private final String countNullQueryName;
    /**
     * Query name to update entity with a specific value.
     */
    private final String updateQueryName;
    /**
     * Business module provider.
     */
    private final Supplier<BusinessModule> businessModuleSupplier;
    /**
     * Available options for this groupBy.
     */
    private final EnumSet<DataGroupByOption> options;

    protected DataGroupByDefinition(String name,
                                    DataGroupByType type,
                                    String propertyName,
                                    boolean propertyMandatory,
                                    Class<?> propertyType,
                                    Class<D> dataType,
                                    Class<F> containerType,
                                    Supplier<F> containerSupplier,
                                    Supplier<BusinessModule> businessModuleSupplier) {
        this.name = Objects.requireNonNull(name);
        this.type = Objects.requireNonNull(type);
        String propertySecondLevelName = null;
        Class<?> dataTypeSecondLevel = null;
        if (isQualitative()) {
            int index = propertyName.indexOf(".");
            if (index == -1) {
                this.propertyName = Objects.requireNonNull(propertyName);
            } else {
                this.propertyName = Objects.requireNonNull(propertyName.substring(0, index));
                propertySecondLevelName = propertyName.substring(index + 1);
                dataTypeSecondLevel = JavaBeanDefinitionStore.getDefinition(dataType).orElseThrow().property(this.propertyName).type();
            }
        } else {
            this.propertyName = Objects.requireNonNull(propertyName);
        }
        this.dataTypeSecondLevel = dataTypeSecondLevel;
        this.propertySecondLevelName = propertySecondLevelName;
        this.propertyMandatory = propertyMandatory;
        this.propertyType = Objects.requireNonNull(propertyType);
        this.dataType = Objects.requireNonNull(dataType);
        this.containerType = Objects.requireNonNull(containerType);
        this.containerSupplier = Objects.requireNonNull(containerSupplier);
        this.countOneQueryName = "groupBy::" + propertyName + "::count::one";
        this.countAllQueryName = "groupBy::" + propertyName + "::count::all";
        this.countNullQueryName = "groupBy::" + propertyName + "::count::null";
        this.updateQueryName = "groupBy::" + propertyName + "::update";
        this.businessModuleSupplier = businessModuleSupplier;
        EnumSet<DataGroupByOption> options = type.options();
        if (isPropertyMandatory()) {
            // can't have a null groupBy value
            options.remove(DataGroupByOption.loadNullGroupBy);
        }
        this.options = options;
    }

    public final boolean isPropertyMandatory() {
        return propertyMandatory;
    }

    public final BusinessModule getBusinessModule() {
        return businessModuleSupplier.get();
    }

    public final String getDefinitionLabel() {
        return getDefinitionLabel(true);
    }

    public final String getDefinitionLabel(Locale locale) {
        return getDefinitionLabel(locale, true);
    }

    public final String getMandatoryLabel() {
        String type = getType().getLabel();
        if (isPropertyMandatory()) {
            return I18n.t("observe.Common.navigation.config.DataGroupBy.mandatory", type);
        } else {
            return I18n.t("observe.Common.navigation.config.DataGroupBy.optional", type);
        }
    }

    public final String getDefinitionLabel(boolean addPrefix) {
        return I18nDecoratorHelper.getGroupByI18nLabel(this, addPrefix);
    }

    public final String getDefinitionLabel(Locale locale, boolean addPrefix) {
        return I18nDecoratorHelper.getGroupByI18nLabel(locale, this, addPrefix);
    }

    public final String getCountAllQueryName(ToolkitTreeFlatModelRootRequest request) {
        String result = countAllQueryName;
        if (isQualitative()) {
            result = addLoadEmptyGroupBy(result, request.isLoadEmptyGroupBy());
            return addLoadDisabledGroupBy(result, request.isLoadDisabledGroupBy());
        }
        return addGroupByFlavor(result, request.getGroupByFlavor());
    }

    public final String getCountOneQueryName(String groupByFlavor, boolean withValue) {
        String result = withValue ? countOneQueryName : countNullQueryName;
        if (isQualitative()) {
            return result;
        }
        return addGroupByFlavor(result, groupByFlavor);
    }

    public final String getUpdateQueryName() {
        return updateQueryName;
    }

    public final DataGroupByType getType() {
        return type;
    }

    public Class<?> getDataTypeSecondLevel() {
        return dataTypeSecondLevel;
    }

    public EnumSet<DataGroupByOption> options() {
        return options;
    }

    public final Class<F> getContainerType() {
        return containerType;
    }

    public final String getName() {
        return name;
    }

    public final String getPropertyName() {
        return propertyName;
    }

    public final String getPropertySecondLevelName() {
        return propertySecondLevelName;
    }

    public final Class<?> getPropertyType() {
        return propertyType;
    }

    public final boolean isTemporal() {
        return Date.class.isAssignableFrom(getPropertyType());
    }

    public final Class<D> getDataType() {
        return dataType;
    }

    public final F newForm() {
        return containerSupplier.get();
    }

    public F newForm(DataGroupByValue groupCountValue) {
        F form = newForm();
        groupCountValue.copyTo(form);
        return form;
    }

    public abstract Object toGroupByObjectValue(D value);

    public abstract void setGroupByValue(D data, Object groupByValue);

    public final DataGroupByParameter toParameter(String value, String groupByFlavor) {
        return new DataGroupByParameter(name, groupByFlavor, value);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataGroupByDefinition)) return false;
        DataGroupByDefinition<?, ?> that = (DataGroupByDefinition<?, ?>) o;
        return name.equals(that.name);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(name);
    }

    //FIXME Remove this, use instead getType
    public final boolean isQualitative() {
        return type == DataGroupByType.QUALITATIVE;
    }

    public final boolean isQualitativeWithSecondLevel() {
        return isQualitative() && getPropertySecondLevelName() != null;
    }

    private String addLoadDisabledGroupBy(String result, boolean value) {
        if (!value) {
            result += FLAVOR_NO_DISABLED;
        }
        return result;
    }

    private String addLoadEmptyGroupBy(String result, boolean value) {
        if (!value) {
            result += FLAVOR_NO_EMPTY;
        }
        return result;
    }

    private String addGroupByFlavor(String result, String groupByFlavor) {
        if (groupByFlavor != null) {
            result += "-" + groupByFlavor;
        }
        return result;
    }

    public List<String> getOptionNames() {
        return options().stream().map(DataGroupByOption::name).collect(Collectors.toList());
    }

    public final String toGroupByValue(D data, String groupByFlavor) {
        Object value = toGroupByObjectValue(data);
        if (value == null) {
            return null;
        }
        if (isQualitative()) {
            return ((ToolkitId) value).getTopiaId();
        }
        // qualitative group by value
        if (value instanceof Date) {
            Date date = (Date) value;
            Optional<DataGroupByTemporalOption> optional = DataGroupByTemporalOption.optionalValueOf(groupByFlavor);
            if (optional.isEmpty()) {
                // full date
                return DataGroupByTemporalOption.formatFullDate(date);
            }
            return optional.get().formatDate(date);
        }
        return value.toString();
    }
}
