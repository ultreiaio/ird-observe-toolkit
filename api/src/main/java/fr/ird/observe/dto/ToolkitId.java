package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Unified id for dto and persistence.
 * <p>
 * It is based of course on persistence id :(
 * <p>
 * Created on 13/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public interface ToolkitId extends ObserveDto {

    String PROPERTY_TOOLKIT_TOPIA_ID = "topiaId";
    String PROPERTY_TOOLKIT_ID = "id";
    String PROPERTY_TOOLKIT_VERSION = "topiaVersion";
    String PROPERTY_TOOLKIT_CREATE_DATE = "topiaCreateDate";
    String PROPERTY_TOOLKIT_LAST_UPDATE_DATE = "lastUpdateDate";

    static Set<String> ids(Stream<? extends ToolkitId> stream) {
        return stream.map(ToolkitId::getTopiaId).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    static <R extends ToolkitId> Stream<R> contains(Stream<R> stream, Collection<String> ids) {
        return stream.filter(id -> ids.contains(id.getTopiaId()));
    }

    static <R extends ToolkitId> Stream<R> notContains(Stream<R> stream, Collection<String> ids) {
        return stream.filter(id -> !ids.contains(id.getTopiaId()));
    }

    static <R extends ToolkitId> Map<String, R> uniqueIndex(Collection<R> data) {
        return Maps.uniqueIndex(data, ToolkitId::getTopiaId);
    }

    static <R extends ToolkitId> R find(Collection<R> source, String id) {
        return source.stream().filter(r -> Objects.equals(id, r.getTopiaId())).findFirst().orElse(null);
    }

    static <R extends ToolkitId> List<R> filterContains(Collection<R> references, String... containsIds) {
        return filterContains(references, Arrays.asList(containsIds));
    }

    static <R extends ToolkitId> List<R> filterContains(Collection<R> references, Collection<String> containsIds) {
        return contains(references.stream(), containsIds).collect(Collectors.toList());
    }

    static <R extends ToolkitId> List<R> filterNotContains(Collection<R> references, Collection<String> containsIds) {
        return notContains(references.stream(), containsIds).collect(Collectors.toList());
    }

    static String decodeId(String id) {
        return id.replaceAll("-", "#");
    }

    static String encodeId(String id) {
        return id.replaceAll("#", "-");
    }

    String getId();

    String getTopiaId();

    long getTopiaVersion();

    Date getTopiaCreateDate();

    Date getLastUpdateDate();

    boolean isPersisted();

    default boolean isNotPersisted() {
        return !isPersisted();
    }

    default Optional<String> getOptionalId() {
        return Optional.ofNullable(getId());
    }

    default Optional<Date> getOptionalLastUpdateDate() {
        return Optional.ofNullable(getLastUpdateDate());
    }

}
