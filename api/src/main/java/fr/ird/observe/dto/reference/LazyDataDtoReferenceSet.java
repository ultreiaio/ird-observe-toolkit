package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;

import java.util.Comparator;
import java.util.function.Supplier;

/**
 * Created on 04/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class LazyDataDtoReferenceSet<D extends DataDto, R extends DataDtoReference> implements Supplier<DataDtoReferenceSet<R>> {
    private final Class<D> dtoType;
    private final Comparator<R> comparator;
    private final Class<R> referenceType;
    private final int initialCount;
    private DataDtoReferenceSet<R> delegate;

    public LazyDataDtoReferenceSet(Class<D> dtoType, Class<R> referenceType, Comparator<R> comparator, int initialCount) {
        this.dtoType = dtoType;
        this.referenceType = referenceType;
        this.comparator = comparator;
        this.initialCount = initialCount;
//        this.decoratorService = decoratorService;
    }

    protected abstract DataDtoReferenceSet<R> createDelegate();

    protected abstract void installDecorator(DataDtoReferenceSet<R> result);

    public final int getInitialCount() {
        return initialCount;
    }

    @Override
    public final DataDtoReferenceSet<R> get() {
        return delegate == null ? delegate = createDelegate() : delegate;
    }

    public final DataDtoReferenceSet<R> reload() {
        delegate = createDelegate();
        return get();
    }

    public final Class<D> getDtoType() {
        return dtoType;
    }

    public final Class<R> getReferenceType() {
        return referenceType;
    }

    public final Comparator<R> getComparator() {
        return comparator;
    }

    protected final DataDtoReferenceSet<R> getDelegate() {
        return delegate;
    }
}
