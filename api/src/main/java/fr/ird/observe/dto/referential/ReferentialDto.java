package fr.ird.observe.dto.referential;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceAware;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

@GenerateJavaBeanDefinition
public abstract class ReferentialDto extends BusinessDto implements ReferentialDtoReferenceAware {


    public static final String PROPERTY_ENABLED = "enabled";
    public static final String PROPERTY_CODE = "code";
    public static final String PROPERTY_STATUS = "status";
    public static final String PROPERTY_URI = "uri";
    public static final String PROPERTY_NEED_COMMENT = "needComment";
    protected String code;
    protected ReferenceStatus status;
    protected String uri;
    protected boolean needComment;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        String oldValue = getCode();
        this.code = code;
        firePropertyChange(PROPERTY_CODE, oldValue, code);
    }

    public ReferenceStatus getStatus() {
        return status;
    }

    public void setStatus(ReferenceStatus status) {
        ReferenceStatus oldValue = getStatus();
        this.status = status;
        firePropertyChange(PROPERTY_STATUS, oldValue, status);
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        String oldValue = getUri();
        this.uri = uri;
        firePropertyChange(PROPERTY_URI, oldValue, uri);
    }

    @Override
    public boolean isNeedComment() {
        return needComment;
    }

    public void setNeedComment(boolean needComment) {
        boolean oldValue = isNeedComment();
        this.needComment = needComment;
        firePropertyChange(PROPERTY_NEED_COMMENT, oldValue, needComment);
    }

    @Override
    public boolean isEnabled() {
        return ReferenceStatus.enabled == status;
    }

    public void setEnabled(boolean enabled) {
        setStatus(enabled ? ReferenceStatus.enabled : ReferenceStatus.disabled);
    }

    @Override
    public <D extends IdDto> void toDto(D dto) {
        super.toDto(dto);
        if (dto instanceof ReferentialDto) {
            ((ReferentialDto) dto).setStatus(getStatus());
        }
    }

    @Override
    public void autoTrim() {
        super.autoTrim();
        autoTrim(this::getCode, this::setCode);
        autoTrim(this::getUri, this::setUri);

    }
}
