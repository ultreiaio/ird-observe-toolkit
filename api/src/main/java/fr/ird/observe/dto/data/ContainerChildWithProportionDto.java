package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 02/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
@GenerateJavaBeanDefinition
public abstract class ContainerChildWithProportionDto extends ContainerChildDto implements WithProportion {

    public static final String PROPERTY_PROPORTION = "proportion";

    protected Integer proportion;

    public Integer getProportion() {
        return proportion;
    }

    public void setProportion(Integer proportion) {
        Integer oldValue = getProportion();
        this.proportion = proportion;
        firePropertyChange(PROPERTY_PROPORTION, oldValue, proportion);
    }

}
