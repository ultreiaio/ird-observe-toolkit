package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Represents a data that you can edit. Most of the time a such object is unique for his container.
 * <p>
 * A such data has a node in select model, but not in edit model.
 * <p>
 * This means, can create, edit and delete, but not move.
 * <p>
 * A form will be available in client and a dedicated service.
 * <p>
 * Created on 02/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class EditableDto extends DataDto implements WithSimpleComment {

    public static final String PROPERTY_COMMENT = "comment";

    protected String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public void autoTrim() {
        super.autoTrim();
        WithSimpleComment.super.autoTrim();
    }
}
