package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4
 */
@GenerateJavaBeanDefinition
public class ProgressionModel extends AbstractJavaBean implements BoundedRangeModel, PropertyChangeListener {

    public static final String PROPERTY_MESSAGE = "message";

    private final DefaultBoundedRangeModel delegate;

    private String message;
    private JProgressBar progressBar;
    private boolean showMessages;

    public ProgressionModel() {
        delegate = new DefaultBoundedRangeModel();
    }

    public void installUI(JProgressBar progressBar) {
        installUI(progressBar, true);
    }

    public void installUI(JProgressBar progressBar, boolean showMessages) {
        this.progressBar = progressBar;
        setShowMessages(showMessages);
        BoundedRangeModel init = (BoundedRangeModel) progressBar.getClientProperty("oldModel");
        if (init != null) {
            throw new IllegalStateException("progressBar " + this + " was already init");
        }
        progressBar.putClientProperty("oldModel", progressBar.getModel());
        progressBar.putClientProperty("oldIndeterminate", progressBar.isIndeterminate());
        progressBar.putClientProperty("oldVisible", progressBar.isVisible());

        progressBar.setVisible(true);
        progressBar.setModel(this);
        progressBar.setIndeterminate(false);
        if (showMessages) {
            removePropertyChangeListener(PROPERTY_MESSAGE, this);
            addPropertyChangeListener(PROPERTY_MESSAGE, this);
        }
    }

    public void uninstallUI(JProgressBar progressBar) {
        progressBar.setModel((BoundedRangeModel) progressBar.getClientProperty("oldModel"));
        progressBar.setVisible((Boolean) progressBar.getClientProperty("oldVisible"));
        progressBar.setIndeterminate((Boolean) progressBar.getClientProperty("oldIndeterminate"));
        progressBar.setString(null);
        removePropertyChangeListener(PROPERTY_MESSAGE, this);
        progressBar.putClientProperty("oldModel", null);
        progressBar.putClientProperty("oldIndeterminate", null);
        progressBar.putClientProperty("oldVisible", null);
    }

    @Override
    public int getValue() {
        return delegate.getValue();
    }

    @Override
    public int getExtent() {
        return delegate.getExtent();
    }

    @Override
    public int getMinimum() {
        return delegate.getMinimum();
    }

    @Override
    public int getMaximum() {
        return delegate.getMaximum();
    }

    @Override
    public void setValue(int n) {
        delegate.setValue(n);
    }

    @Override
    public void setExtent(int n) {
        delegate.setExtent(n);
    }

    @Override
    public void setMinimum(int n) {
        delegate.setMinimum(n);
    }

    @Override
    public void setMaximum(int n) {
        delegate.setMaximum(n);
    }

    @Override
    public void setValueIsAdjusting(boolean b) {
        delegate.setValueIsAdjusting(b);
    }

    @Override
    public boolean getValueIsAdjusting() {
        return delegate.getValueIsAdjusting();
    }

    @Override
    public void setRangeProperties(int newValue, int newExtent, int newMin, int newMax, boolean adjusting) {
        delegate.setRangeProperties(newValue, newExtent, newMin, newMax, adjusting);
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        delegate.addChangeListener(l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        delegate.removeChangeListener(l);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        increments();
        String oldValue = getMessage();
        this.message = message;
        firePropertyChange(PROPERTY_MESSAGE, oldValue, message);
    }

    public void increments() {
        setValue(getValue() + 1);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (isShowMessages() && progressBar != null) {
            progressBar.setString(null);
            String message = String.format("%s %s", evt.getNewValue(), progressBar.getString());
            progressBar.setString(message);
        }
    }

    public boolean isShowMessages() {
        return showMessages;
    }

    public void setShowMessages(boolean showMessages) {
        this.showMessages = showMessages;
    }
}
