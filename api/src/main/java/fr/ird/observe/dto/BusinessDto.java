package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.MoreObjects;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.lang.Objects2;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created on 21/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
@GenerateJavaBeanDefinition
public abstract class BusinessDto extends IdDto implements ObserveDto, JavaBean, Decorated, ToolkitId, ToToolkitIdLabel, ToToolkitIdTechnicalLabel {

    public static final String PROPERTY_HOME_ID = "homeId";
    private String homeId;
    private transient Decorator decorator;

    public static <O extends JavaBean> boolean valueChangedToFalse(O o1, O o2, Predicate<O> predicate) {
        return predicate.test(o1) && !predicate.test(o2);
    }

    public static <BeanType extends IdDto> BeanType newDto(Class<BeanType> dtoType, Date now) {
        BeanType result = Objects2.newInstance(dtoType);
        result.setTopiaCreateDate(now);
        return result;
    }

    public static <BeanType extends BusinessDto> boolean exists(Collection<BeanType> source, String id) {
        return source.stream().anyMatch(newIdPredicate(id));
    }

    public static <BeanType extends BusinessDto> Predicate<BeanType> newIdPredicate(String id) {
        return o -> Objects.equals(id, o.getId());
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e ->
                MoreObjects.toStringHelper(e)
                        .add(PROPERTY_TOOLKIT_ID, getId())
                        .add(PROPERTY_TOOLKIT_LAST_UPDATE_DATE, lastUpdateDate)
                        .toString());
    }

    public <D extends IdDto> void toDto(D dto) {
        super.toDto(dto);
        if (dto instanceof BusinessDto) {
            ((BusinessDto) dto).setHomeId(getHomeId());
        }
    }

    @Override
    public Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        this.decorator = Objects.requireNonNull(decorator);
    }

    public String getHomeId() {
        return homeId;
    }

    public void setHomeId(String homeId) {
        String oldValue = getHomeId();
        this.homeId = homeId;
        firePropertyChange(PROPERTY_HOME_ID, oldValue, homeId);
    }

    @Override
    public void autoTrim() {
        autoTrim(this::getHomeId, this::setHomeId);
    }
}
