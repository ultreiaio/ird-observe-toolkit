package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;

import java.util.Comparator;
import java.util.Map;

/**
 * Describe the definition of a reference on a dto.
 * <p>
 * Created on 11/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataDtoReferenceDefinition<D extends DataDto, R extends DataDtoReference> extends DtoReferenceDefinition<D, R> {

    private static final long serialVersionUID = 1L;

    public static <D extends DataDto, R extends DataDtoReference> Builder<D, R, DataDtoReferenceDefinition<D, R>> builder(Class<D> dtoType, Class<R> type, Comparator<R> comparator) {
        return new Builder<>(dtoType, type, comparator) {
            @Override
            public DataDtoReferenceDefinition<D, R> build() {
                return new DataDtoReferenceDefinition<>(dtoType, referenceType, propertiesBuilder, comparator);
            }
        };
    }

    protected DataDtoReferenceDefinition(Class<D> dtoType, Class<R> referenceType, Map<String, Class<?>> properties, Comparator<R> comparator) {
        super(dtoType, referenceType, properties, comparator);
    }

}
