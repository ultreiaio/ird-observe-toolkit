/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.dto.referential;


import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

/**
 * Pour définir les différentes langues du référentiel.
 * <p>
 * La position de chaque constante définit l'index du champs libelleXXX à
 * utiliser.
 * <p>
 * En base on a actuellement 8 langues possibles, pour gérer une nouvelle
 * langue, il faut définir une nouvelle constante ici.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum ReferentialLocale {
    /**
     * correspond a la propriete {@code label1} d'un {@link I18nReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel1()
     */
    UK(Locale.UK, true),
    /**
     * correspond a la propriete {@code label2} d'un {@link ReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel2()
     */
    FR(Locale.FRANCE, false),
    /**
     * correspond a la propriete {@code label3} d'un {@link ReferentialDto}.
     *
     * @see I18nReferentialDto#getLabel3()
     */
    ES(new Locale("es", "ES"), false);

    private final Locale locale;
    private final boolean naturalDateSortOrder;

    public static ReferentialLocale valueOf(Locale locale) {
        for (ReferentialLocale anEnum : values()) {
            if (locale.equals(anEnum.getLocale()) || Objects.equals(locale.getLanguage(), anEnum.getLocale().getLanguage())) {
                return anEnum;
            }
        }
        throw new IllegalArgumentException(
                "could not find referentiel locale from locale " + locale);
    }

    public static Set<Locale> initNotDateNaturalSortOrder() {
        Set<Locale> result = new LinkedHashSet<>();
        for (ReferentialLocale referentialLocale : values()) {
            if (referentialLocale.isNaturalDateSortOrder()) {
                continue;
            }
            result.add(referentialLocale.getLocale());
        }
        return result;
    }

    ReferentialLocale(Locale locale, boolean naturalDateSortOrder) {
        this.locale = locale;
        this.naturalDateSortOrder = naturalDateSortOrder;
    }

    public Locale getLocale() {
        return locale;
    }

    public boolean isNaturalDateSortOrder() {
        return naturalDateSortOrder;
    }

    public String getLibelle() {
        return "label" + (ordinal() + 1);
    }

    public String getColumnName() {
        return "label" + (ordinal() + 1);
    }

    @Override
    public String toString() {
        return "<ReferentialLocale: " + locale + ", index: " + name() + ">";
    }

    public <E extends I18nReferentialDto> void setLabel(String label, E i18nDto) {

        switch (ordinal() + 1) {
            case 1:
                i18nDto.setLabel1(label);
                break;
            case 2:
                i18nDto.setLabel2(label);
                break;
            case 3:
                i18nDto.setLabel3(label);
                break;
            case 4:
                i18nDto.setLabel4(label);
                break;
            case 5:
                i18nDto.setLabel5(label);
                break;
            case 6:
                i18nDto.setLabel6(label);
                break;
            case 7:
                i18nDto.setLabel7(label);
                break;
            case 8:
                i18nDto.setLabel8(label);
                break;
        }

    }

    public <E extends I18nReferentialDto> String getLabel(E i18nEntity) {

        return I18nReferentialHelper.getLabel(ordinal(), i18nEntity);

    }
}
