package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;

/**
 * Created on 15/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class DataGroupByParameter implements JsonAware {

    private final String groupByName;
    private final String groupByFlavor;
    private final String value;

    public DataGroupByParameter(String groupByName, String groupByFlavor, String value) {
        this.groupByName = groupByName;
        this.groupByFlavor = groupByFlavor;
        this.value = value;
    }

    public String getGroupByName() {
        return groupByName;
    }

    public String getGroupByFlavor() {
        return groupByFlavor;
    }

    public String getValue() {
        return value;
    }
}
