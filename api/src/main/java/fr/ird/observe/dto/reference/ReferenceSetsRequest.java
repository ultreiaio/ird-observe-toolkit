package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Date;
import java.util.Map;

/**
 * Pour demander plusieurs ensembles de références dans un même appel.
 * <p>
 * Created on 10/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferenceSetsRequest<D extends IdDto> implements ObserveDto {

    private Class<D> dtoType;

    private Map<Class<? extends ReferentialDto>, Date> lastUpdateDates;

    public Class<D> getDtoType() {
        return dtoType;
    }

    public void setDtoType(Class<D> dtoType) {
        this.dtoType = dtoType;
    }

    public Map<Class<? extends ReferentialDto>, Date> getLastUpdateDates() {
        return lastUpdateDates;
    }

    public void setLastUpdateDates(Map<Class<? extends ReferentialDto>, Date> lastUpdateDates) {
        this.lastUpdateDates = lastUpdateDates;
    }

}
