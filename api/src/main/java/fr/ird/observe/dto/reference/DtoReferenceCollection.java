package fr.ird.observe.dto.reference;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ToolkitId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class DtoReferenceCollection<R extends DtoReference> implements ObserveDto, Serializable {

    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_REFERENCES = "references";
    public static final String PROPERTY_LAST_UPDATE = "lastUpdate";

    private static final long serialVersionUID = 1L;

    final Class<R> type;
    private final List<R> references;
    private final Date lastUpdate;

    public static <R extends ReferentialDtoReference> List<R> filterEnabled(Collection<R> references) {
        return references.stream().filter(ReferentialDtoReference::isEnabled).collect(Collectors.toList());
    }

    DtoReferenceCollection(Class<R> type, List<R> references, Date lastUpdate) {
        this.type = type;
        this.references = references;
        this.lastUpdate = lastUpdate;
    }

    public void sort(Comparator<R> referenceComparator) {
        references.sort(referenceComparator);
    }

    public Class<R> getType() {
        return type;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public Optional<R> tryGetReferenceById(String id) {
        return references.stream().filter(DtoReference.newIdPredicate(id)).findFirst();
    }

    public R getReferenceByPosition(int index) {
        return references.get(index);
    }

    public int size() {
        return references.size();
    }

    public Stream<R> stream() {
        return references.stream();
    }

    public Stream<R> subSet(String... ids) {
        return subSet(Arrays.asList(ids));
    }

    public Stream<R> subSet(Collection<String> ids) {
        return ToolkitId.contains(stream(), ids);
    }

    public Set<R> toSet() {
        return new LinkedHashSet<>(references);
    }

    public List<R> toList() {
        return references;
    }

    public Set<String> toIds() {
        return ToolkitId.ids(stream());
    }

    public ArrayList<R> toArrayList() {
        return new ArrayList<>(references);
    }

    public <O> Map<O, R> toMap(Function<R, O> function) {
        return Maps.uniqueIndex(references, function::apply);
    }

    public Map<String, R> toId() {
        return Maps.uniqueIndex(references, IdDto::getId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("type=" + type)
                .add("size=" + size())
                .toString();
    }
}
