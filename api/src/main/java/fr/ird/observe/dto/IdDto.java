package fr.ird.observe.dto;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DtoReferenceAware;
import io.ultreia.java4all.bean.AbstractJavaBean;

import java.util.Date;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class IdDto extends AbstractJavaBean implements ObserveDto, DtoReferenceAware, ToolkitIdImpl {
    public static final String PROPERTY_ID = PROPERTY_TOOLKIT_ID;
    public static final String PROPERTY_LAST_UPDATE_DATE = PROPERTY_TOOLKIT_LAST_UPDATE_DATE;
    public static final String PROPERTY_CREATE_DATE = PROPERTY_TOOLKIT_CREATE_DATE;
    public static final String PROPERTY_VERSION = PROPERTY_TOOLKIT_VERSION;
    protected String id;
    protected Date lastUpdateDate;
    protected long topiaVersion;
    protected Date topiaCreateDate;

    public ToolkitIdDtoBean toShortDto() {
        throw new IllegalStateException("Not implemented");
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
    }

    public final Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public final void setLastUpdateDate(Date lastUpdateDate) {
        Date oldValue = getLastUpdateDate();
        this.lastUpdateDate = lastUpdateDate;
        firePropertyChange(PROPERTY_LAST_UPDATE_DATE, oldValue, lastUpdateDate);
    }

    public boolean isPersisted() {
        return getId() != null && !getId().isBlank();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IdDto)) return false;
        IdDto that = (IdDto) o;
        return !(getId() == null || that.getId() == null) && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getTopiaId() {
        return id;
    }

    @Override
    public final long getTopiaVersion() {
        return topiaVersion;
    }

    @Override
    public final Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    public final void setTopiaVersion(long topiaVersion) {
        this.topiaVersion = topiaVersion;
    }

    public final void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }

    public <D extends IdDto> void toDto(D dto) {
        dto.setId(getId());
        dto.setTopiaCreateDate(getTopiaCreateDate());
        dto.setTopiaVersion(getTopiaVersion());
        dto.setLastUpdateDate(getLastUpdateDate());
    }

    /**
     * Use this method when dto was initialized and want to do some adjustments: typically after init from entity.
     * <p>
     * for example, initialize coordinate quadrant.
     *
     * @since 8.0.2
     */
    public void postInit() {
    }

    /**
     * Method to trim text fields which have the tag-value {@code notBlank}.
     */
    public void autoTrim() {
    }

    public static void autoTrim(Supplier<String> getter, Consumer<String> setter) {
        String stringValue = getter.get();
        if (stringValue != null) {
            String stringTrimmedValue = stringValue.trim();
            if (!stringValue.equals(stringTrimmedValue)) {
                if ("".equals(stringTrimmedValue)) {
                    // empty string is null to us
                    stringTrimmedValue = null;
                }
                setter.accept(stringTrimmedValue);
            }
        }
    }
}
