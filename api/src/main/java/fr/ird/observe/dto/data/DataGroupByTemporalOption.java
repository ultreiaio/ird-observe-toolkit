package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;
import io.ultreia.java4all.util.Dates;

import java.util.Date;
import java.util.Optional;

/**
 * Different options for a temporal groupBy type.
 * <p>
 * Created on 10/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.65
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_NAVIGATION_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_NAVIGATION_DESCRIPTION_PATTERN)
})
public enum DataGroupByTemporalOption {
    year("%1$tY"),
    month("%1$tY-%1$tm");

    public static final String FULL_FORMAT = "%1$tY-%1$tm-%1$td";
    private final String format;

    public static Optional<DataGroupByTemporalOption> optionalValueOf(String name) {
        if (name == null) {
            return Optional.empty();
        }
        for (DataGroupByTemporalOption value : values()) {
            if (name.equals(value.name())) {
                return Optional.of(value);
            }
        }
        return Optional.empty();
    }

    public static String formatFullDate(Date date) {
        return String.format(FULL_FORMAT, date);
    }

    public static Date newFullDate(String value) {
        String[] split = value.split("-");
        return Dates.createDate(Integer.parseInt(split[2]), Integer.parseInt(split[1]), Integer.parseInt(split[0]));
    }

    DataGroupByTemporalOption(String format) {
        this.format = format;
    }

    public String formatDate(Date date) {
        return String.format(format, date);
    }

    public Date newDate(String date) {
        String[] split = date.split("-");
        int year = Integer.parseInt(split[0]);
        int month = split.length > 1 ? Integer.parseInt(split[1]) : 1;
        int day = split.length > 2 ? Integer.parseInt(split[2]) : 1;
        return Dates.createDate(day, month, year);
    }

    public String getLabel() {
        return DataGroupByTemporalOptionI18n.getLabel(this);
    }

    public String getDescription() {
        return DataGroupByTemporalOptionI18n.getDescription(this);
    }
}
