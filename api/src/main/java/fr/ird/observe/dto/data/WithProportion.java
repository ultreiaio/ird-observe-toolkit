package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.reference.DtoReferenceAware;

import java.util.Collection;
import java.util.Objects;

/**
 * Created on 05/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface WithProportion extends ObserveDto, DtoReferenceAware {

    String PROPERTY_PROPORTION = "proportion";

    static <D extends WithProportion> int sumProportions(Collection<D> proportions) {
        return proportions == null ? 0 : proportions.stream().map(WithProportion::getProportion).filter(Objects::nonNull).reduce(Integer::sum).orElse(0);
    }

    Integer getProportion();

    default void setProportion(Integer proportion) {
        // not used for some implementation (like references)
    }
}
