package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 14/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class UpdatedDataDtoReferenceSet<R extends DataDtoReference> implements ObserveDto {

    private final Set<String> allIds;
    private final DataDtoReferenceSet<R> updatedReferences;

    public UpdatedDataDtoReferenceSet(Set<String> allIds, DataDtoReferenceSet<R> updatedReferences) {
        this.allIds = Objects.requireNonNull(allIds);
        this.updatedReferences = Objects.requireNonNull(updatedReferences);
    }

    public Set<String> getAllIds() {
        return allIds;
    }

    public DataDtoReferenceSet<R> getUpdatedReferences() {
        return updatedReferences;
    }

    public DataDtoReferenceSet<R> mergeTo(DataDtoReferenceSet<R> incoming) {
        Set<String> previousIdsToKeep = new LinkedHashSet<>(getAllIds());
        updatedReferences.toIds().forEach(previousIdsToKeep::remove);
        List<R> collect = incoming.subSet(previousIdsToKeep).collect(Collectors.toList());
        collect.addAll(getUpdatedReferences().toArrayList());
        return DataDtoReferenceSet.of(incoming.getType(), collect, getUpdatedReferences().getLastUpdate());
    }
}
