package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.bean.BeanPropertyI18nKeyProducerProvider;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.lang.Strings;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import java.util.ServiceLoader;

import static io.ultreia.java4all.i18n.I18n.getDefaultLocale;
import static io.ultreia.java4all.i18n.I18n.l;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 03/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class I18nDecoratorHelper extends BeanPropertyI18nKeyProducerProvider {

    public static final String I18N_CONSTANT_TIP = "tip";
    public static final String I18N_CONSTANT_LABEL = "label";
    public static final String I18N_CONSTANT_LABEL_PATTERN = "observe.constant.@CLASS_SIMPLE_NAME@.@NAME@";
    public static final String I18N_CONSTANT_DESCRIPTION_PATTERN = "observe.constant.@CLASS_SIMPLE_NAME@.@NAME@.description";
    public static final String I18N_CONSTANT_ACTION = "action";
    public static final String I18N_CONSTANT_ACTION_PATTERN = "observe.constant.@CLASS_SIMPLE_NAME@.@NAME@.action";
    public static final String I18N_CONSTANT_STORAGE_LABEL_PATTERN = "observe.constant.storage.@CLASS_SIMPLE_NAME@.@NAME@";
    public static final String I18N_CONSTANT_NAVIGATION_LABEL_PATTERN = "observe.Common.navigation.config.@CLASS_SIMPLE_NAME@.@NAME@";
    public static final String I18N_CONSTANT_NAVIGATION_DESCRIPTION_PATTERN = "observe.Common.navigation.config.@CLASS_SIMPLE_NAME@.@NAME@.description";
    public static final String I18N_CONSTANT_DESCRIPTION = "description";
    public static final String I18N_CONSTANT_STORAGE_DESCRIPTION_PATTERN = "observe.constant.storage.@CLASS_SIMPLE_NAME@.@NAME@.description";
    public static final String I18N_CONSTANT_STORAGE_CONFIG = "config";
    public static final String I18N_CONSTANT_STORAGE_CONFIG_PATTERN = "observe.constant.storage.@CLASS_SIMPLE_NAME@.@NAME@.config";
    public static final String I18N_CONSTANT_STORAGE_USE = "use";
    public static final String I18N_CONSTANT_STORAGE_USE_PATTERN = "observe.constant.storage.@CLASS_SIMPLE_NAME@.@NAME@.use";

    private static I18nDecoratorHelper INSTANCE;

    public static I18nDecoratorHelper get() {
        if (INSTANCE == null) {
            Iterator<BeanPropertyI18nKeyProducerProvider> iterator = ServiceLoader.load(BeanPropertyI18nKeyProducerProvider.class, Thread.currentThread().getContextClassLoader()).iterator();
            if (!iterator.hasNext()) {
                throw new IllegalStateException("No BeanPropertyI18nKeyProducerProvider instance found.");
            }
            INSTANCE = (I18nDecoratorHelper) iterator.next();
            if (iterator.hasNext()) {
                throw new IllegalStateException("More than one BeanPropertyI18nKeyProducerProvider instance found.");
            }
        }
        return INSTANCE;
    }

    public static SimpleDateFormat newDateFormat(Locale locale) {
        return Objects.equals(locale.getLanguage(), "fr") ? new SimpleDateFormat("dd/MM/yyyy") : new SimpleDateFormat("yyyy-MM-dd");
    }

    public static SimpleDateFormat newTimestampFormat(Locale locale) {
        return Objects.equals(locale.getLanguage(), "fr") ? new SimpleDateFormat("dd/MM/yyyy HH:mm") : new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    public static SimpleDateFormat newTimeFormat(Locale locale) {
        return new SimpleDateFormat("HH:mm");
    }

    public static String getTypeKey(Class<?> key) {
        return getPropertyI18nKey(key, "type");
    }

    public static String getType(Class<?> key) {
        return t(getTypeKey(key));
    }

    public static String getType(Locale locale, Class<?> key) {
        return l(locale, getTypeKey(key));
    }

    public static String getGroupByI18nLabel(DataGroupByDefinition<?, ?> definition, boolean addPrefix) {
        return getGroupByI18nLabel(getDefaultLocale(), definition, addPrefix);
    }

    public static String getGroupByI18nLabel(Locale locale, DataGroupByDefinition<?, ?> definition, boolean addPrefix) {
        String propertyKey = getPropertyI18nKey(definition.getDataType(), definition.getPropertyName());
        String result = l(locale, propertyKey);
        if (definition.isQualitativeWithSecondLevel()) {
            propertyKey = getPropertyI18nKey(definition.getDataTypeSecondLevel(), definition.getPropertySecondLevelName());
            result += " - " + l(locale, propertyKey);
        }
        if (addPrefix) {
            return String.format("[%s] %s", definition.getBusinessModule().getShortLabel(), result);
        }
        return result;
    }

    public static String getPropertyI18nKey(Class<?> type, String propertyName) {
        I18nDecoratorHelper i18nDecoratorHelper = get();
        return i18nDecoratorHelper.getDefaultLabelsBuilder().getI18nPropertyKey(type, propertyName);
    }

    public static void getDateLabel(Locale locale, StringBuilder result, Date date, String nullLabel) {
        if (date == null) {
            result.append(nullLabel);
        } else {
            getDateLabel(locale, result, date);
        }
    }

    public static void getDateLabel(Locale locale, StringBuilder result, Date date) {
        result.append(getDateLabel(locale, date));
    }

    public static String getDateLabel(Locale locale, Date date) {
        return I18n.l(locale, "observe.Common.date.format", date);
    }

    public static String getTimestampLabel(Locale locale, Date date) {
        return I18n.l(locale, "observe.Common.timestamp.format", date);
    }

    public static String getTimestampWithSecondsLabel(Locale locale, Date date) {
        return I18n.l(locale, "observe.Common.timestamp.seconds.format", date);
    }

    public static String getTimeLabel(Locale locale, Date date) {
        return I18n.l(locale, "observe.Common.time.format", date);
    }

    public static String decorateCoordinate(Float latitude, Float longitude) {
        if (latitude == null || longitude == null) {
            return t("observe.Common.no.coordinate");
        }
        return String.format("(%s°;%s°)", fillCoordinateComponent(2, latitude), fillCoordinateComponent(3, longitude));
    }

    public static String fillCoordinateComponent(int integerPartDigit, Float component) {
        boolean withSign = component < 0;
        String round = String.valueOf(Numbers.roundFourDigits(component));
        int index = round.indexOf(".");
        return (withSign ? "-" : " ")
                + Strings.leftPad(round.substring(0, index).replace("-", ""), integerPartDigit, "0")
                + Strings.rightPad(round.substring(index), 5, "0");
    }

}
