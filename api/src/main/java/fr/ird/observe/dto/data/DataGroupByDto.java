package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 05/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DataGroupByDto<D extends RootOpenableDto> extends DataDto implements DataGroupBy<D> {

    private List<D> children;
    private String filterValue;
    private String filterText;
    private long count;
    private boolean enabled = true;

    @Override
    public abstract DataGroupByDtoDefinition<D, ?> definition();

    @Override
    public final ToolkitIdLabel toLabel() {
        return new ToolkitIdLabel(definition().getContainerType(), getFilterValue(), getLastUpdateDate(), getFilterText());
    }

    @Override
    public final ToolkitIdTechnicalLabel toTechnicalLabel() {
        return new ToolkitIdTechnicalLabel(definition().getContainerType(), getFilterValue(), getLastUpdateDate(), getLastUpdateDate(), 0, getFilterText());
    }

    @Override
    public final String getId() {
        return getFilterValue();
    }

    @Override
    public final String getTopiaId() {
        return getFilterValue();
    }

    @Override
    public final String getFilterValue() {
        return filterValue;
    }

    @Override
    public final void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    @Override
    public final String getFilterText() {
        return filterText;
    }

    @Override
    public final void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    @Override
    public final void setId(String id) {
        setFilterValue(id);
    }

    @Override
    public final long getCount() {
        return children == null ? count : children.size();
    }

    @Override
    public final void setCount(long count) {
        this.count = count;
    }

    @Override
    public final boolean isEnabled() {
        return enabled;
    }

    @Override
    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public final List<D> getChildren() {
        if (children == null) {
            children = new LinkedList<>();
        }
        return children;
    }
}
