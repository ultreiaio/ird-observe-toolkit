package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorated;

import java.util.Date;

/**
 * A ToolkitIdLabel with extra technical fields.
 * <p>
 * This object can be used for example in some generic code to get minimum data to transport (synchronisation,...) and
 * avoid to keep dto or even references.
 *
 * <p>
 * Created on 16/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
@GenerateJavaBeanDefinition
public class ToolkitIdTechnicalLabel extends ToolkitIdLabel {

    private final Date topiaCreateDate;
    private final long topiaVersion;

    public static <O extends ToolkitId & Decorated> ToolkitIdTechnicalLabel newTechnicalLabel(Class<? extends BusinessDto> dtoType, O source) {
        return new ToolkitIdTechnicalLabel(dtoType, source.getId(), source.getTopiaCreateDate(), source.getLastUpdateDate(), source.getTopiaVersion(), source.decorator().map(d -> d.decorateWithContextSeparator(source)).orElseThrow());
    }

    public ToolkitIdTechnicalLabel(Class<? extends BusinessDto> type, String id, Date topiaCreateDate, Date lastUpdateDate, long topiaVersion, String text) {
        super(type, id, lastUpdateDate, text);
        this.topiaVersion = topiaVersion;
        this.topiaCreateDate = topiaCreateDate;
    }

    @Override
    public ToolkitIdTechnicalLabel of(String id) {
        return new ToolkitIdTechnicalLabel(getType(), id, getTopiaCreateDate(), getLastUpdateDate(), getTopiaVersion(), getText());
    }

    @Override
    public ToolkitIdTechnicalLabel of(Date timestamp) {
        return new ToolkitIdTechnicalLabel(getType(), getId(), getTopiaCreateDate(), timestamp, getTopiaVersion(), getText());
    }

    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    public long getTopiaVersion() {
        return topiaVersion;
    }
}
