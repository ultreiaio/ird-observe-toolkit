package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 06/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class DataGroupByDtoSet<D extends RootOpenableDto, G extends DataGroupByDto<D>> implements JsonAware {

    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_PROPERTY_TYPE = "propertyType";
    public static final String PROPERTY_REFERENCES = "references";
    public static final String PROPERTY_LAST_UPDATE = "lastUpdate";
    private final Class<G> type;
    private final Class<?> propertyType;
    private final List<G> references;
    private final Date lastUpdate;

    public static <D extends RootOpenableDto, G extends DataGroupByDto<D>> DataGroupByDtoSet<D, G> of(Class<G> type,
                                                                                                      Class<?> propertyType,
                                                                                                      Stream<G> references,
                                                                                                      Date lastUpdate) {
        return new DataGroupByDtoSet<>(type, propertyType, references.collect(Collectors.toList()), lastUpdate);
    }

    public static <D extends RootOpenableDto, G extends DataGroupByDto<D>> DataGroupByDtoSet<D, G> of(Class<G> type,
                                                                                                      Class<?> propertyType,
                                                                                                      Collection<G> references,
                                                                                                      Date lastUpdate) {
        return of(type, propertyType, references.stream(), lastUpdate);
    }

    public static <R extends ReferentialDtoReference> List<R> filterEnabled(Collection<R> references) {
        return references.stream().filter(ReferentialDtoReference::isEnabled).collect(Collectors.toList());
    }

    public DataGroupByDtoSet(Class<G> type, Class<?> propertyType, List<G> references, Date lastUpdate) {
        this.type = type;
        this.propertyType = propertyType;
        this.references = references;
        this.lastUpdate = lastUpdate;
    }

    public DataGroupByDtoSet<D, G> subset(String... ids) {
        return of(type, propertyType, subSet(ids), getLastUpdate());
    }

    public void sort(Comparator<G> referenceComparator) {
        references.sort(referenceComparator);
    }

    public Class<G> getType() {
        return type;
    }

    public Class<?> getPropertyType() {
        return propertyType;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public Optional<G> tryGetReferenceById(String id) {
        return references.stream().filter(DtoReference.newIdPredicate(id)).findFirst();
    }

    public int size() {
        return references.size();
    }

    public Stream<G> stream() {
        return references.stream();
    }

    public Stream<G> subSet(String... ids) {
        return subSet(Arrays.asList(ids));
    }

    public Stream<G> subSet(Collection<String> ids) {
        return ToolkitId.contains(stream(), ids);
    }

    public Set<G> toSet() {
        return new LinkedHashSet<>(references);
    }

    public List<G> toList() {
        return references;
    }

    public Set<String> toIds() {
        return ToolkitId.ids(stream());
    }

    public ArrayList<G> toArrayList() {
        return new ArrayList<>(references);
    }

    public Map<String, G> toId() {
        return Maps.uniqueIndex(references, IdDto::getId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("type=" + type)
                .add("size=" + size())
                .toString();
    }

    public long dataCount() {
        return references.stream().mapToLong(DataGroupByDto::getCount).sum();
    }

    public int indexOf(DataGroupByDto<?> groupByDto) {
        return references.indexOf(groupByDto);
    }
}


