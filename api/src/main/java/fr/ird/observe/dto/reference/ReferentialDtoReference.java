package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;

/**
 * Created on 21/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ReferentialDtoReference extends DtoReference implements ReferentialDtoReferenceAware {

    public static final String PROPERTY_ENABLED = "enabled";
    public static final String PROPERTY_CODE = "code";
    public static final String PROPERTY_URI = "uri";
    private boolean needComment;
    private boolean enabled;
    private String uri;

    public void fromReferentialDtoReferenceAware(ReferentialDtoReferenceAware dto) {
        fromDtoReferenceAware(dto);
        this.enabled = dto.isEnabled();
        this.needComment = dto.isNeedComment();
        setUri(dto.getUri());
        setHomeId(dto.getHomeId());
    }

    public String getLabel() {
        return null;
    }

    @Override
    public abstract Class<? extends ReferentialDto> getDtoType();

    @Override
    public abstract Class<? extends ReferentialDtoReference> getReferenceType();

    @Override
    public abstract ReferentialDtoReferenceDefinition<?, ?> getDefinition();

    @Override
    public boolean isNeedComment() {
        return needComment;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public boolean isDisabled() {
        return !enabled;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
