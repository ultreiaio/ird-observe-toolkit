package fr.ird.observe.dto.form;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceDefinition;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * For a given of type {@link #type}, define all dependencies of referential set it need.
 * <p>
 * Means, for each property which is a reference to a dto, let's grab mark it, this make use possible then to download
 * <p>
 * in one request, all referential references required by this object.
 * Created on 11/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FormDefinition<D extends BusinessDto> {

    /**
     * Property defines on each referential, it defines the list of references of this type.
     */
    public static final String REFERENTIAL_LIST_HEADER = "referentialListHeader";

    private final Class<D> type;
    private final Map<String, ReferentialDtoReferenceDefinition<?, ?>> properties;

    public static class Builder<D extends BusinessDto> {

        private final Class<D> type;

        private final Map<String, ReferentialDtoReferenceDefinition<?, ?>> propertiesBuilder;

        public Builder(Class<D> type) {
            this.type = type;
            this.propertiesBuilder = new LinkedHashMap<>();
        }

        public <DD extends ReferentialDto, RR extends ReferentialDtoReference> Builder<D> addProperty(String name, ReferentialDtoReferenceDefinition<DD, RR> definition) {
            propertiesBuilder.put(name, definition);
            return this;
        }

        public FormDefinition<D> build() {
            return new FormDefinition<>(type, propertiesBuilder);
        }
    }

    public static <D extends BusinessDto> Builder<D> builder(Class<D> type) {
        return new Builder<>(type);
    }

    FormDefinition(Class<D> type, Map<String, ReferentialDtoReferenceDefinition<?, ?>> properties) {
        this.type = type;
        this.properties = properties;
    }

    public Map<String, ReferentialDtoReferenceDefinition<?, ?>> getProperties() {
        return properties;
    }

    public Class<D> getType() {
        return type;
    }

    public Set<Class<? extends ReferentialDto>> getPropertiesTypes() {
        return properties.values().stream().map(ReferentialDtoReferenceDefinition::getDtoType).collect(Collectors.toSet());
    }

    public Set<Class<? extends ReferentialDtoReference>> getPropertiesReferenceTypes() {
        return properties.values().stream().map(ReferentialDtoReferenceDefinition::getType).collect(Collectors.toSet());
    }
}
