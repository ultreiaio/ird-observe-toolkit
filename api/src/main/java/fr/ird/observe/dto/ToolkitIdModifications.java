package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.monitor.JavaBeanModifications;
import io.ultreia.java4all.bean.monitor.JavaBeanPropertyModification;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * To keep all modifications on a {@link ToolkitId} or (/and) just some warnings.
 * <p>
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public class ToolkitIdModifications extends JavaBeanModifications implements JsonAware {

    private final String id;
    private final String label;
    private final Set<String> warnings;

    public ToolkitIdModifications(ToolkitId toolkitId, Set<JavaBeanPropertyModification> modifications, Set<String> warnings) {
        super(modifications.stream().sorted(Comparator.comparing(JavaBeanPropertyModification::getPropertyName)).collect(Collectors.toCollection(LinkedHashSet::new)));
        this.id = toolkitId.getId();
        this.label = toolkitId.toString();
        this.warnings = warnings;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public Set<String> getWarnings() {
        return warnings;
    }

    public boolean withModifications() {
        return !getModifications().isEmpty();
    }

    public boolean withWarnings() {
        return getWarnings() != null && !getWarnings().isEmpty();
    }

    public int modificationsCount() {
        return getModifications().size();
    }

    public int warningsCount() {
        return getWarnings() == null ? 0 : getWarnings().size();
    }

    public void replaceToolkitIdReferencesById() {
        for (JavaBeanPropertyModification modification : getModifications()) {
            if (modification.getOldValue() instanceof ToolkitId) {
                modification.setOldValue(((ToolkitId) modification.getOldValue()).getTopiaId());
            }
            if (modification.getNewValue() instanceof ToolkitId) {
                modification.setNewValue(((ToolkitId) modification.getNewValue()).getTopiaId());
            }
        }
    }
}
