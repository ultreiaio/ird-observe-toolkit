package fr.ird.observe.dto.reference;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReferentialDtoReferenceSet<R extends ReferentialDtoReference> extends DtoReferenceCollection<R> {

    private static final long serialVersionUID = 1L;

    public static <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> of(Class<R> type,
                                                                                       Stream<R> references,
                                                                                       Date lastUpdate) {
        return new ReferentialDtoReferenceSet<>(type, references.collect(Collectors.toList()), lastUpdate);
    }

    public static <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> of(Class<R> type,
                                                                                       Collection<R> references,
                                                                                       Date lastUpdate) {
        return of(type, references.stream(), lastUpdate);
    }

    protected ReferentialDtoReferenceSet(Class<R> type, List<R> references, Date lastUpdate) {
        super(type, references, lastUpdate);
    }

    public ReferentialDtoReferenceSet<R> subset(String... ids) {
        return of(type, subSet(ids), getLastUpdate());
    }
}
