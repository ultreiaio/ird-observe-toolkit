package fr.ird.observe.dto.stats;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 04/07/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.15
 */
public abstract class WithStatisticsHolder<X extends WithStatistics<X>> implements WithStatistics<X>, WithStatisticsBuilder<X> {

    @Override
    public final void removeStatistics() {
        // in holder, we can't remove statistics
    }

    @Override
    public final void addStatistics() {
        // in holder, we can't add statistics
    }

    @Override
    public final boolean withStatistics() {
        // in holder, we always have statistics
        return true;
    }

    @Override
    public final void copyTo(X target) {
        // in holder we use a special builder which just deliver internal values
        // in that way we have a unique generated code to build or copy
        buildStatistics(target);
    }
}
