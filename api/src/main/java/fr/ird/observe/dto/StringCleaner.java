package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

/**
 * Object to clean a string related to a database field.
 * <p>
 * Created on 12/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.017
 */
public class StringCleaner implements Function<String, String> {

    public static final StringCleaner ALL = new StringCleaner(true, true, true);
    public static final StringCleaner REMOVE_SINGLE_QUOTE_AND_TRIM = new StringCleaner(true, true, false);
    public static final StringCleaner TRIM_AND_REPLACE_EMPTY_BY_NULL = new StringCleaner(false, true, true);
    public static final StringCleaner TRIM = new StringCleaner(false, true, false);

    /**
     * To remove any single quote in given string.
     */
    private final boolean removeSingleQuote;
    /**
     * To perform a trim on given string.
     */
    private final boolean trim;
    /**
     * To replace the given string value by null if it is empty.
     */
    private final boolean replaceEmptyByNull;

    public StringCleaner(boolean removeSingleQuote, boolean trim, boolean replaceEmptyByNull) {
        this.removeSingleQuote = removeSingleQuote;
        this.trim = trim;
        this.replaceEmptyByNull = replaceEmptyByNull;
    }

    public static String removeSingleQuote(String string) {
        return string.replaceAll("'", "");
    }

    @Override
    public String apply(String string) {
        if (string == null) {
            return null;
        }
        String result = string;
        if (removeSingleQuote) {
            result = removeSingleQuote(result);
        }
        if (trim) {
            result = result.trim();
        }
        if (replaceEmptyByNull && result.isEmpty()) {
            result = null;
        }
        return result;
    }
}
