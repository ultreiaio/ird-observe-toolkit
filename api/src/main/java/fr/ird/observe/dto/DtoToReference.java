package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.referential.ReferentialLocale;

/**
 * Place this contract on any IdDto which should be able to transform to a dto reference.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4
 */
public interface DtoToReference<R extends DtoReference> {

    R toReference(ReferentialLocale referentialLocale);

    default Class<R> referenceType() {
        return ObserveUtil.getClassType(getClass(), "R");
    }
}
