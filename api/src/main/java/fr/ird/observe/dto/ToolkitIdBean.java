package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;

import java.util.Date;
import java.util.StringJoiner;

/**
 * Immutable implementation of a {@link ToolkitId}.
 * <p>
 * Created on 28/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
public class ToolkitIdBean extends AbstractJavaBean implements ToolkitId {
    private String id;
    private long topiaVersion;
    private Date topiaCreateDate;
    private Date lastUpdateDate;

    public ToolkitIdBean() {
    }

    public ToolkitIdBean(String id, Date lastUpdateDate) {
        this.id = id;
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public final String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getTopiaId() {
        return id;
    }

    @Override
    public final Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean isPersisted() {
        return id != null;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ToolkitId.class.getSimpleName() + "[", "]")
                .add("topiaId='" + getTopiaId() + "'")
                .add(getLastUpdateDate() == null ? "" : "lastUpdateDate=" + getLastUpdateDate())
                .toString();
    }

    @Override
    public long getTopiaVersion() {
        return topiaVersion;
    }

    public void setTopiaVersion(long topiaVersion) {
        this.topiaVersion = topiaVersion;
    }

    @Override
    public Date getTopiaCreateDate() {
        return topiaCreateDate;
    }

    public void setTopiaCreateDate(Date topiaCreateDate) {
        this.topiaCreateDate = topiaCreateDate;
    }
}
