package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.StringJoiner;

/**
 * A {@link ToolkitIdBean} extended with the matching dto type.
 * <p>
 * Note: for a same entity type, we can have multiple dto types, so this object from dto model is useful.
 * <p>
 * Created on 29/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
public class ToolkitIdDtoBean extends ToolkitIdBean {
    /**
     * Immutable dto type.
     */
    private Class<? extends BusinessDto> type;

    /**
     * Create a short id with no timestamp.
     *
     * @param type type of id
     * @param id   id
     * @return new short id
     */
    public static ToolkitIdDtoBean of(Class<? extends BusinessDto> type, String id) {
        return of(type, id, null);
    }

    /**
     * Create a short id.
     *
     * @param type      type of id
     * @param id        id
     * @param timestamp timestamp associated to id or request
     * @return new short id
     */
    public static ToolkitIdDtoBean of(Class<? extends BusinessDto> type, String id, Date timestamp) {
        return new ToolkitIdDtoBean(type, id, timestamp);
    }

    /**
     * Create a short id.
     *
     * @param type   type of id
     * @param source source
     * @return new short id
     */
    public static ToolkitIdDtoBean of(Class<? extends BusinessDto> type, ToolkitId source) {
        return new ToolkitIdDtoBean(type, source.getId(), source.getLastUpdateDate());
    }

    protected ToolkitIdDtoBean() {
    }

    protected ToolkitIdDtoBean(Class<? extends BusinessDto> type, String id, Date lastUpdateDate) {
        super(id, lastUpdateDate);
        this.type = type;
    }

    public ToolkitIdDtoBean of(String id) {
        return new ToolkitIdDtoBean(getType(), id, getLastUpdateDate());
    }

    public ToolkitIdDtoBean of(Date timestamp) {
        return new ToolkitIdDtoBean(getType(), getId(), timestamp);
    }

    public Class<? extends BusinessDto> getType() {
        return type;
    }

    public void setType(Class<? extends BusinessDto> type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("id='" + getId() + "'")
                .add("type=" + type.getName())
                .add(getLastUpdateDate() == null ? "" : "lastUpdateDate=" + getLastUpdateDate())
                .toString();
    }
}
