package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

/**
 * Created on 03/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public interface WithAtLeastOneSelected extends CommonDto, JavaBean {

    String PROPERTY_AT_LEAST_ONE_SELECTED = "atLeastOneSelected";

    boolean isAtLeastOneSelected();

    default void setAtLeastOneSelected(boolean atLeastOneSelected) {
        // by default do nothing, just here to have a complete java bean property (required by validation)
    }
}
