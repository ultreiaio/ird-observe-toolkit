package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorDefinition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * A ToolkitIdBean with a text.
 * <p>
 * Created on 14/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
@GenerateJavaBeanDefinition
public class ToolkitIdLabel extends ToolkitIdDtoBean implements Decorated {

    private final String text;
    private transient String[] textParts;
    private transient Decorator decorator;

    public static <O extends ToolkitId & Decorated> ToolkitIdLabel newLabel(Class<? extends BusinessDto> type, O source) {
        return new ToolkitIdLabel(type, source.getId(), source.getLastUpdateDate(), source.decorator().map(d -> d.decorateWithContextSeparator(source)).orElseThrow());
    }

    public ToolkitIdLabel(Class<? extends BusinessDto> type, String id, Date lastUpdateDate, String text) {
        super(type, id, lastUpdateDate);
        this.text = text;
    }

    @Override
    public ToolkitIdDtoBean of(String id) {
        return new ToolkitIdLabel(getType(), id, getLastUpdateDate(), text);
    }

    @Override
    public ToolkitIdDtoBean of(Date timestamp) {
        return new ToolkitIdLabel(getType(), getId(), timestamp, text);
    }

    public String getText() {
        return text;
    }

    public String getText(int index) {
        try {
            return textParts()[index];
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't get part at index %d from %s: %s", index, getId(), Arrays.toString(textParts())));
        }
    }

    public String[] textParts() {
        if (textParts == null) {
            String separator = DecoratorDefinition.DEFAULT_CONTEXT_SEPARATOR;
            if (!text.contains(separator)) {
                separator = DecoratorDefinition.DEFAULT_RENDERER_SEPARATOR;
            }
            int length = separator.length();
            List<String> tokens = new ArrayList<>();
            int currentIndex = 0;
            int nextSeparator;
            while ((nextSeparator = text.indexOf(separator, currentIndex)) > -1) {
                // found a new part
                tokens.add(text.substring(currentIndex, nextSeparator));
                currentIndex = nextSeparator + length;
            }
            if (currentIndex < text.length()) {
                tokens.add(text.substring(currentIndex));
            }
            if (text.endsWith(separator)) {
                tokens.add("");
            }
            textParts = tokens.toArray(new String[0]);
        }
//        return textParts == null ? textParts = text.split("\\s*" + DecoratorDefinition.DEFAULT_CONTEXT_SEPARATOR + "\\s*") : textParts;
        return textParts;
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e -> ((ToolkitIdLabel) e).getText());
    }

    @Override
    public Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        this.decorator = Objects.requireNonNull(decorator);
    }
}
