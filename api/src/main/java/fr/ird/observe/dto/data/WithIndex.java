package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created at 27/08/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public interface WithIndex extends ObserveDto {
    String PROPERTY_INDEX = "index";

    static <C extends WithIndex> List<C> reorder(Collection<C> collection) {
        return collection.stream().sorted(Comparator.comparing(WithIndex::getIndex)).collect(Collectors.toList());
    }

    static void installOrder(Collection<? extends WithIndex> collection) {
        int index = 0;
        for (WithIndex withIndex : collection) {
            withIndex.setIndex(++index);
        }
    }

    int getIndex();

    void setIndex(int index);
}
