package fr.ird.observe.dto.referential;

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
@GenerateJavaBeanDefinition
public abstract class I18nReferentialDto extends ReferentialDto implements WithI18n {

    private String label1;
    private String label2;
    private String label3;
    private String label4;
    private String label5;
    private String label6;
    private String label7;
    private String label8;

    @Override
    public String getLabel1() {
        return label1;
    }

    @Override
    public void setLabel1(String label1) {
        String oldValue = getLabel1();
        this.label1 = label1;
        firePropertyChange(PROPERTY_LABEL1, oldValue, label1);
    }

    @Override
    public String getLabel2() {
        return label2;
    }

    @Override
    public void setLabel2(String label2) {
        String oldValue = getLabel2();
        this.label2 = label2;
        firePropertyChange(PROPERTY_LABEL2, oldValue, label2);
    }

    @Override
    public String getLabel3() {
        return label3;
    }

    @Override
    public void setLabel3(String label3) {
        String oldValue = getLabel3();
        this.label3 = label3;
        firePropertyChange(PROPERTY_LABEL3, oldValue, label3);
    }

    @Override
    public String getLabel4() {
        return label4;
    }

    @Override
    public void setLabel4(String label4) {
        String oldValue = getLabel4();
        this.label4 = label4;
        firePropertyChange(PROPERTY_LABEL4, oldValue, label4);
    }

    @Override
    public String getLabel5() {
        return label5;
    }

    @Override
    public void setLabel5(String label5) {
        String oldValue = getLabel5();
        this.label5 = label5;
        firePropertyChange(PROPERTY_LABEL5, oldValue, label5);
    }

    @Override
    public String getLabel6() {
        return label6;
    }

    @Override
    public void setLabel6(String label6) {
        String oldValue = getLabel6();
        this.label6 = label6;
        firePropertyChange(PROPERTY_LABEL6, oldValue, label6);
    }

    @Override
    public String getLabel7() {
        return label7;
    }

    @Override
    public void setLabel7(String label7) {
        String oldValue = getLabel7();
        this.label7 = label7;
        firePropertyChange(PROPERTY_LABEL7, oldValue, label7);
    }

    @Override
    public String getLabel8() {
        return label8;
    }

    @Override
    public void setLabel8(String label8) {
        String oldValue = getLabel8();
        this.label8 = label8;
        firePropertyChange(PROPERTY_LABEL8, oldValue, label8);
    }

    @Override
    public void autoTrim() {
        super.autoTrim();
        autoTrim(this::getLabel1, this::setLabel1);
        autoTrim(this::getLabel2, this::setLabel2);
        autoTrim(this::getLabel3, this::setLabel3);
        autoTrim(this::getLabel4, this::setLabel4);
        autoTrim(this::getLabel5, this::setLabel5);
        autoTrim(this::getLabel6, this::setLabel6);
        autoTrim(this::getLabel7, this::setLabel7);
        autoTrim(this::getLabel8, this::setLabel8);
    }
}
