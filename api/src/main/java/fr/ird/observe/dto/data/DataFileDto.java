package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created on ?.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
@GenerateJavaBeanDefinition
public class DataFileDto extends AbstractJavaBean implements ObserveDto {

    public static final String PROPERTY_CONTENT = "content";
    public static final String PROPERTY_NAME = "name";

    protected byte[] content;
    protected String name;

    public static DataFileDto from(File file) {
        DataFileDto result = new DataFileDto();
        try {
            byte[] fileContent = Files.readAllBytes(file.toPath());
            result.setContent(fileContent);
            result.setName(file.getName());
        } catch (IOException e) {
            throw new IllegalStateException("Could not read file: " + file, e);
        }
        return result;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        byte[] oldValue = getContent();
        this.content = content;
        firePropertyChange(PROPERTY_CONTENT, oldValue, content);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }


}
