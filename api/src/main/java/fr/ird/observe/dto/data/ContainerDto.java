package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

/**
 * Represents a data which contains a collection of {@link ContainerChildDto}.
 * <p>
 * Created on 02/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public abstract class ContainerDto<C extends ContainerChildDto> extends DataDto implements WithSimpleComment {

    public static final String PROPERTY_CHILDREN = "children";
    public static final String PROPERTY_COMMENT = "comment";

    private final String propertyName;
    protected String comment;

    protected ContainerDto(String propertyName) {
        this.propertyName = Objects.requireNonNull(propertyName);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    public String getPropertyName() {
        return propertyName;
    }

    public boolean isEmpty() {
        return getChildren() == null || getChildren().isEmpty();
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public int getChildrenSize() {
        return getChildren() == null ? 0 : getChildren().size();
    }

    public List<C> getChildren() {
        return get(propertyName);
    }

    public void setChildren(List<C> children) {
        List<? extends ContainerChildDto> oldValue = getChildren();
        set(propertyName, children);
        firePropertyChange(PROPERTY_CHILDREN, oldValue, children);
    }

    @Override
    public void autoTrim() {
        super.autoTrim();
        WithSimpleComment.super.autoTrim();
    }
}
