package fr.ird.observe.dto.form;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ObserveDto;

import java.io.Serializable;

public class Form<R extends BusinessDto> implements ObserveDto, Serializable {

    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_OBJECT = "object";

    private static final long serialVersionUID = 1L;

    private final Class<R> type;
    private final R object;

    public static <R extends BusinessDto> Form<R> newFormDto(Class<R> type, R object) {
        return new Form<>(type, object);
    }

    protected Form(Class<R> type, R object) {
        this.type = type;
        this.object = object;
    }

    public R getObject() {
        return object;
    }

    public Class<R> getType() {
        return type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add(PROPERTY_TYPE, type.getName())
                .toString();
    }

}
