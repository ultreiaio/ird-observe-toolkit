package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

/**
 * Created on 23/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.4
 */
public interface ToolkitIdImpl extends ToolkitId {

    default void copyTechnicalFields(ToolkitId incoming) {
        copyTechnicalFields(incoming, null);
    }

    default void copyTechnicalFields(ToolkitId incoming, Date lastUpdateDate) {
        setId(incoming.getTopiaId());
        setTopiaVersion(incoming.getTopiaVersion());
        setTopiaCreateDate(incoming.getTopiaCreateDate());
        setLastUpdateDate(lastUpdateDate == null ? incoming.getLastUpdateDate() : lastUpdateDate);
    }

    void setId(String id);

    void setTopiaCreateDate(Date topiaCreateDate);

    void setLastUpdateDate(Date lastUpdateDate);

    void setTopiaVersion(long topiaVersion);
}
