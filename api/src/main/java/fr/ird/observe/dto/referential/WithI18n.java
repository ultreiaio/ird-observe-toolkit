package fr.ird.observe.dto.referential;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

/**
 * Created on 10/06/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public interface WithI18n extends ObserveDto {

    String PROPERTY_LABEL1 = "label1";
    String PROPERTY_LABEL2 = "label2";
    String PROPERTY_LABEL3 = "label3";
    String PROPERTY_LABEL4 = "label4";
    String PROPERTY_LABEL5 = "label5";
    String PROPERTY_LABEL6 = "label6";
    String PROPERTY_LABEL7 = "label7";
    String PROPERTY_LABEL8 = "label8";

    default String getLabel(ReferentialLocale referentialLocale) {
        return I18nReferentialHelper.getLabel(referentialLocale.getLocale(), this);
    }

    String getLabel1();

    void setLabel1(String label1);

    String getLabel2();

    void setLabel2(String label2);

    String getLabel3();

    void setLabel3(String label3);

    String getLabel4();

    void setLabel4(String label4);

    String getLabel5();

    void setLabel5(String label5);

    String getLabel6();

    void setLabel6(String label6);

    String getLabel7();

    void setLabel7(String label7);

    String getLabel8();

    void setLabel8(String label8);

    default boolean isLabelTranslated(String x, String y, String z) {
        if (x == null || x.isEmpty()) {
            return true;
        }
        if (x.contains("#TODO") || x.contains(" TODO")) {
            return false;
        }
        if (y == null && z == null) {
            return true;
        }
        return !x.equals(y) && !x.equals(z);
    }

}
