package fr.ird.observe.dto.reference;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;

import java.io.Serializable;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Describe the definition of a reference on a dto.
 * <p>
 * Created on 11/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class DtoReferenceDefinition<D extends BusinessDto, R extends DtoReference> implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Class<D> dtoType;
    private final Class<R> referenceType;
    private final Map<String, Class<?>> properties;
    private final Comparator<R> comparator;

    public abstract static class Builder<D extends BusinessDto, R extends DtoReference, X extends DtoReferenceDefinition<D, R>> {

        final Class<D> dtoType;
        final Class<R> referenceType;
        final Map<String, Class<?>> propertiesBuilder;
        final Comparator<R> comparator;

        public Builder(Class<D> dtoType, Class<R> referenceType, Comparator<R> comparator) {
            this.dtoType = dtoType;
            this.referenceType = referenceType;
            this.comparator = comparator;
            this.propertiesBuilder = new LinkedHashMap<>();
        }

        public <O> Builder<D, R, X> addProperty(Class<O> type, String name) {
            propertiesBuilder.put(name, type);
            return this;
        }

        public Class<R> getType() {
            return referenceType;
        }

        public abstract X build();
    }

    protected DtoReferenceDefinition(Class<D> dtoType, Class<R> referenceType, Map<String, Class<?>> properties, Comparator<R> comparator) {
        this.dtoType = dtoType;
        this.referenceType = referenceType;
        this.properties = properties;
        this.comparator = comparator;
    }

    public Class<R> getType() {
        return referenceType;
    }

    public Class<D> getDtoType() {
        return dtoType;
    }

    public Set<String> getPropertyNames() {
        return properties.keySet();
    }

    public Comparator<R> getComparator() {
        return comparator;
    }
}
