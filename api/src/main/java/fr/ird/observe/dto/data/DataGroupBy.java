package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ToolkitId;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.decoration.Decorated;

import java.util.List;
import java.util.Locale;

/**
 * Created on 08/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public interface DataGroupBy<D> extends ObserveDto, JavaBean, Decorated, ToolkitId {
    String PROPERTY_CHILDREN = "children";
    String PROPERTY_COUNT = "count";
    String PROPERTY_FILTER_TEXT = "filterText";
    String PROPERTY_FILTER_VALUE = "filterValue";
    String PROPERTY_ENABLED = "enabled";

    DataGroupByDefinition<D, ?> definition();

    String getFilterValue();

    void setFilterValue(String filterValue);

    String getFilterText();

    void setFilterText(String filterText);

    long getCount();

    void setCount(long count);

    List<D> getChildren();

    boolean isEnabled();

    void setEnabled(boolean enabled);

    default String getDefinitionLabel(Locale locale) {
        return definition().getDefinitionLabel(locale);
    }

    default DataGroupByParameter toParameter(String groupByFlavor) {
        return definition().toParameter(getFilterValue(), groupByFlavor);
    }

    default boolean isDisabled() {
        return !isEnabled();
    }

    default boolean isNull() {
        return getFilterValue() == null;
    }

    default boolean isEmpty() {
        return getCount() == 0;
    }
}

