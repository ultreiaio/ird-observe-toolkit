package fr.ird.observe.dto.data;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

/**
 * Created on 13/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class DataGroupByValue implements ObserveDto {

    private final String value;
    private final long count;
    private boolean enabled;
    private String text;

    public static DataGroupByValue of(Object[] row) {
        String value = (String) row[0];
        long count = ((Number) row[1]).longValue();
        return new DataGroupByValue(value, count);
    }

    public DataGroupByValue(String value, long count) {
        this.value = value;
        this.count = count;
    }

    public String getValue() {
        return value;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getCount() {
        return count;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void copyTo(DataGroupBy<?> dto) {
        dto.setFilterValue(getValue());
        dto.setFilterText(getText());
        dto.setCount(getCount());
        dto.setEnabled(isEnabled());
    }
}
