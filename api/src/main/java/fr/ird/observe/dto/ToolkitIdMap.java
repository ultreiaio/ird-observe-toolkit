package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Map based implementation of a {@link ToolkitId}.
 * <p>
 * Created on 28/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
public class ToolkitIdMap implements ToolkitId {

    private final Map<String, Object> delegate;

    public ToolkitIdMap(Map<String, ?> delegate) {
        this.delegate = new LinkedHashMap<>(Objects.requireNonNull(delegate).size());
        // the incoming map may have key prefixed by _
        delegate.forEach((key, value) -> this.delegate.put(key.startsWith("_") ? key.substring(1) : key, value));
    }

    @Override
    public long getTopiaVersion() {
        return (long) delegate.get(ToolkitId.PROPERTY_TOOLKIT_VERSION);
    }

    @Override
    public Date getTopiaCreateDate() {
        return (Date) delegate.get(ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE);
    }

    @Override
    public String getTopiaId() {
        return (String) delegate.get(ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID);
    }

    @Override
    public String getId() {
        return getTopiaId();
    }

    @Override
    public Date getLastUpdateDate() {
        return (Date) delegate.get(IdDto.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
    }

    @Override
    public boolean isPersisted() {
        return getId() != null;
    }
}
