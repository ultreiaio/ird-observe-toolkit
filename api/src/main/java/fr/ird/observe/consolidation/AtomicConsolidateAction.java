package fr.ird.observe.consolidation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;

/**
 * Created on 23/02/2023.
 *
 * @param <C> the type of data consolidate context
 * @param <E> the type of data to consolidate
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.26
 */
public interface AtomicConsolidateAction<C extends ConsolidateContext<E>, E extends ToolkitId> extends BiPredicate<C, E>, BiConsumer<C, E> {

    Logger log = LogManager.getLogger(AtomicConsolidateAction.class);

    /**
     * @return field names on which action may have change the value.
     */
    List<String> fieldNames();

    /**
     * Test if this action can be executed, and if so, then execute it.
     *
     * @param context data consolidation context
     * @param data    data to consolidate
     * @return {@code true} if this action was executed (means some data has been computed), {@code false} otherwise.
     */
    default boolean execute(C context, E data) {
        if (test(context, data)) {
            accept(context, data);
            logModification(context, data);
            return true;
        }
        return false;
    }

    private void logModification(C context, E data) {
        fieldNames().forEach(fieldName -> logModification(context, data, fieldName));
    }

    private void logModification(C context, E data, String fieldName) {
        context.getModification(fieldName).ifPresent(modification -> log.info(String.format("[%s on %s] property '%s.%s' has changed [%s → %s]",
                                                                                            this,
                                                                                            data.getTopiaId(),
                                                                                            context.dataType().getSimpleName(),
                                                                                            fieldName,
                                                                                            decorateValue(modification.getOldValue()),
                                                                                            decorateValue(modification.getNewValue()))));
    }

    static String decorateValue(Object value) {
        if (value == null) {
            return null;
        }
        if (value instanceof ToolkitId) {
            ToolkitId toolkitId = (ToolkitId) value;
            return toolkitId.getId();
        }
        if (value instanceof Enum<?>) {
            Enum<?> anEnum = (Enum<?>) value;
            return anEnum.name() + " (" + anEnum + ")";
        }
        return value.toString();
    }
}
