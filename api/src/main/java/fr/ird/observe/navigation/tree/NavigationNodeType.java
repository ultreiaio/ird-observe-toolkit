package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 03/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public enum NavigationNodeType {
    Root("   "),
    ReferentialPackage("R:P"),
    ReferentialType("R:T"),
    OpenList("D:L"),
    Open("D:O"),
    RootOpen("D:R"),
    RootOpenFilter("D:F"),
    Edit("D:E"),
    Table("D:T"),
    Simple("D:S");
    private final String label;

    NavigationNodeType(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

    public interface WithNavigationNodeType {
        NavigationNodeType getType();

        default boolean isData() {
            return !isRoot() && !isReferential();
        }

        default boolean isReferential() {
            return isReferentialPackage() || isReferentialType();
        }

        default boolean isEdit() {
            return NavigationNodeType.Edit.equals(getType());
        }

        default boolean isTable() {
            return NavigationNodeType.Table.equals(getType());
        }

        default boolean isSimple() {
            return NavigationNodeType.Simple.equals(getType());
        }

        default boolean isReferentialPackage() {
            return NavigationNodeType.ReferentialPackage.equals(getType());
        }

        default boolean isReferentialType() {
            return NavigationNodeType.ReferentialType.equals(getType());
        }

        default boolean isOpen() {
            return NavigationNodeType.Open.equals(getType());
        }

        default boolean isReference() {
            return isEdit() || isOpen();
        }

        default boolean isRoot() {
            return NavigationNodeType.Root.equals(getType());
        }

        default boolean isRootOpen() {
            return RootOpen.equals(getType());
        }

        default boolean isRootOpenFilter() {
            return RootOpenFilter.equals(getType());
        }

        default boolean isOpenList() {
            return NavigationNodeType.OpenList.equals(getType());
        }

        default String typeLabel() {
            return getType().label();
        }

    }
}
