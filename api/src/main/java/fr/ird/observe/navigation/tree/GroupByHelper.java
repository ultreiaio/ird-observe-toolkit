package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.spi.module.BusinessProject;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created on 10/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.65
 */
public class GroupByHelper {

    /**
     * List of group by definition by their module names.
     */
    public final Map<String, List<DataGroupByDtoDefinition<?, ?>>> groupByDefinitionByModuleNames;
    /**
     * List of group option names by their group by name.
     */
    public final Map<String, List<String>> groupByOptionByGroupByNames;

    public GroupByHelper(BusinessProject businessProject) {
        this.groupByDefinitionByModuleNames = businessProject.getGroupByDefinitionsByModuleNames();
        this.groupByOptionByGroupByNames = new LinkedHashMap<>();
        for (List<DataGroupByDtoDefinition<?, ?>> k : groupByDefinitionByModuleNames.values()) {
            for (DataGroupByDtoDefinition<?, ?> definition : k) {
                groupByOptionByGroupByNames.computeIfAbsent(definition.getName(), k2 -> new LinkedList<>()).addAll(definition.getOptionNames());
            }
        }
    }

    public DataGroupByDtoDefinition<?, ?> getDefinition(String groupByName) {
        for (List<DataGroupByDtoDefinition<?, ?>> d : groupByDefinitionByModuleNames.values()) {
            for (DataGroupByDtoDefinition<?, ?> definition : d) {
                if (groupByName.equals(definition.getName())) {
                    return definition;
                }
            }
        }
        throw new IllegalStateException(String.format("Can't find definition for groupByName:%s", groupByName));
    }

    public List<String> getGroupByOptionNames(String groupByName) {
        return groupByOptionByGroupByNames.get(groupByName);
    }
}
