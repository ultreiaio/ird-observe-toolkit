package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelPathRequest;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Date;
import java.util.function.Function;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class NavigationTreeNodeUpdaterImpl implements NavigationTreeNodeUpdater {
    private static final Logger log = LogManager.getLogger(NavigationTreeNodeUpdaterImpl.class);
    private final Function<ToolkitTreeFlatModelPathRequest, ToolkitTreeFlatModel> loadChildren;
    private final ToolkitTreeNodeBuilder treeNodeBuilder;

    public NavigationTreeNodeUpdaterImpl(Function<ToolkitTreeFlatModelPathRequest, ToolkitTreeFlatModel> loadChildren) {
        this.loadChildren = loadChildren;
        this.treeNodeBuilder = new ToolkitTreeNodeBuilder();
    }

    @Override
    public void loadChildren(NavigationTreeNode node) {
        Path nodePath = node.getNodePath();
        ToolkitTreeFlatModelPathRequest request = new ToolkitTreeFlatModelPathRequest(nodePath.toString(), new Date(), false);
        ToolkitTreeFlatModel flatModel = loadChildren.apply(request);
        log.info(String.format("Load structure: %s", nodePath));
        flatModel.addState(NavigationTreeNodeBean.STATE_UPDATER, this, p -> true);

        treeNodeBuilder.load(node, flatModel.getMapping());
        log.info(String.format("Fill node: %s", nodePath));
    }
}
