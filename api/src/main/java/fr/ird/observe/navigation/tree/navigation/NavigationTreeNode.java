package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;
import fr.ird.observe.navigation.id.IdNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.VerticalNavigationHelper;
import io.ultreia.java4all.util.SingletonSupplier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.TreeNode;
import java.util.Iterator;
import java.util.Objects;

/**
 * Represents a node in the navigation tree model.
 * <p>
 * Created on 02/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
@SuppressWarnings("unused")
public abstract class NavigationTreeNode extends ToolkitTreeNode {
    private static final Logger log = LogManager.getLogger(NavigationTreeNode.class);

    private final SingletonSupplier<NavigationTreeNode> previous;
    private final SingletonSupplier<NavigationTreeNode> next;
    private final SingletonSupplier<Boolean> withPrevious;
    private final SingletonSupplier<Boolean> withNext;

    protected NavigationTreeNode(NavigationTreeNodeBean userObject) {
        super(userObject);
        this.previous = SingletonSupplier.of(() -> VerticalNavigationHelper.findPrevious(this), false);
        this.next = SingletonSupplier.of(() -> VerticalNavigationHelper.findNext(this), false);
        this.withPrevious = SingletonSupplier.of(() -> getPrevious() != null);
        this.withNext = SingletonSupplier.of(() -> getNext() != null);
    }


    public void loadIds(Iterator<ToolkitParentIdDtoBean> dataToRoot, ToolkitIdDtoBean id) {
        if (getUserObject().isReference()) {
            if (!dataToRoot.hasNext()) {
                throw new IllegalStateException("No more data in path, was required for id: " + id);
            }
            id = dataToRoot.next();
            log.info(String.format("Loaded id: %s", id.getId()));
            ToolkitTreeNodeBean userObject = getUserObject();
            userObject.copy(id);
        }
        NavigationTreeNode parent = getParent();
        if (parent != null) {
            parent.loadIds(dataToRoot, id);
        }
    }

    public final <N extends NavigationTreeNode> N findPath(String path) {
        return VerticalNavigationHelper.findPath0(this, path);
    }

    public final <N extends NavigationTreeNode> N find(Class<N> nodeType) {
        return VerticalNavigationHelper.find0(this, nodeType, n -> true);
    }

    public final <N extends NavigationTreeNode> N find(Class<N> nodeType, ToolkitIdDtoBean id) {
        return VerticalNavigationHelper.find0(this, nodeType, n -> Objects.equals(n.getUserObject().getId(), id.getId()));
    }

    public NavigationTreeNode getPrevious() {
        return previous.get();
    }

    public NavigationTreeNode getNext() {
        return next.get();
    }

    public SingletonSupplier<Boolean> getWithPrevious() {
        return withPrevious;
    }

    public SingletonSupplier<Boolean> getWithNext() {
        return withNext;
    }

    //--------------------------------------------------------------------------------------------
    // Find methods
    //--------------------------------------------------------------------------------------------

    public NavigationTreeNode findChildById(String id) {
        return VerticalNavigationHelper.findChildById(this, id);
    }

    public <N extends NavigationTreeNode> N findChildByType(Class<N> childType) {
        return VerticalNavigationHelper.findChildByType(this, childType);
    }

    public <N extends NavigationTreeNode> N findChildByType(Class<N> childType, String id) {
        return VerticalNavigationHelper.findChildByType(this, childType, id);
    }

    public NavigationTreeNode findChildByModelNode(IdNode<?> node) {
        return VerticalNavigationHelper.findChildByModelNode(this, node);
    }

    /**
     * @param siblingId id to seek
     * @return sibling node with the given id
     */
    public NavigationTreeNode findSibling(String siblingId) {
        NavigationTreeNode parent = getParent();
        return VerticalNavigationHelper.findChildByType(parent, getClass(), siblingId);
    }

    /**
     * @param siblingId id to seek
     * @return sibling node with the given id (using parent sibling on given id)
     */
    public NavigationTreeNode findParentSibling(String siblingId) {
        NavigationTreeNode sibling = getParent().findSibling(siblingId);
        return VerticalNavigationHelper.findChildByType(sibling, getClass());
    }

    @Override
    protected void dirtyStructure() {
        withPrevious.clear();
        withNext.clear();
        previous.clear();
        next.clear();
    }

    @Override
    protected final void populateChildrenIfNotLoaded() {
        if (isLoaded()) {
            // already loaded
            return;
        }
        getUserObject().buildChildren(this);
        loaded();
    }

    //--------------------------------------------------------------------------------------------
    // TreeNode override methods
    //--------------------------------------------------------------------------------------------

    @Override
    public NavigationTreeNode getParent() {
        return (NavigationTreeNode) super.getParent();
    }

    @Override
    public NavigationTreeNode getRoot() {
        return (NavigationTreeNode) super.getRoot();
    }

    @Override
    public NavigationTreeNodeBean getUserObject() {
        return (NavigationTreeNodeBean) super.getUserObject();
    }

    @Override
    public NavigationTreeNode getChildAt(int index) {
        return (NavigationTreeNode) super.getChildAt(index);
    }

    @Override
    public NavigationTreeNode getFirstChild() {
        return (NavigationTreeNode) super.getFirstChild();
    }

    @Override
    public NavigationTreeNode getLastChild() {
        return (NavigationTreeNode) super.getLastChild();
    }

    @Override
    public NavigationTreeNode getChildAfter(TreeNode aChild) {
        return (NavigationTreeNode) super.getChildAfter(aChild);
    }

    @Override
    public NavigationTreeNode getChildBefore(TreeNode aChild) {
        return (NavigationTreeNode) super.getChildBefore(aChild);
    }

    @Override
    public NavigationTreeNode getNextSibling() {
        return (NavigationTreeNode) super.getNextSibling();
    }

    @Override
    public NavigationTreeNode getPreviousSibling() {
        return (NavigationTreeNode) super.getPreviousSibling();
    }

    @Override
    public NavigationTreeNode getFirstLeaf() {
        return (NavigationTreeNode) super.getFirstLeaf();
    }

    @Override
    public NavigationTreeNode getLastLeaf() {
        return (NavigationTreeNode) super.getLastLeaf();
    }

    @Override
    public NavigationTreeNode getNextLeaf() {
        return (NavigationTreeNode) super.getNextLeaf();
    }

    @Override
    public NavigationTreeNode getPreviousLeaf() {
        return (NavigationTreeNode) super.getPreviousLeaf();
    }
}
