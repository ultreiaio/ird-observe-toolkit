package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Represents id plus his lastUpdateDate of a data.
 * <p>
 * Used to compute differential between two selection models.
 * <p>
 * The {@link #equals(Object)} and {@link #hashCode()} methods are based only on the {@link #id} field, in that way
 * we can compare a such object by id.
 * <p>
 * Created on 26/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class IdAndLastUpdateDate {

    private final String id;
    private final Date lastUpdateDate;

    public IdAndLastUpdateDate(String id, Date lastUpdateDate) {
        this.id = id;
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getId() {
        return id;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IdAndLastUpdateDate)) return false;
        IdAndLastUpdateDate that = (IdAndLastUpdateDate) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", IdAndLastUpdateDate.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("lastUpdateDate=" + lastUpdateDate)
                .toString();
    }

}
