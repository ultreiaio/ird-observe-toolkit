package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 04/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class NavigationResult implements ObserveDto {

    private final ToolkitTreeFlatModelRootRequest request;
    private final DataGroupByDtoSet<?, ?> data;
    private final long dataCount;
    private final Date timestamp;
    private final Map<Class<? extends ReferentialDto>, Long> referentialCount;

    public NavigationResult(ToolkitTreeFlatModelRootRequest request, long dataCount, DataGroupByDtoSet<?, ?> data, Date timestamp, Map<Class<? extends ReferentialDto>, Long> referentialCount) {
        this.request = request;
        this.dataCount = dataCount;
        this.data = data;
        this.timestamp = timestamp;
        this.referentialCount = referentialCount;
    }

    public ToolkitTreeFlatModelRootRequest getRequest() {
        return request;
    }

    public int getGroupByCount() {
        return data == null ? 0 : data.size();
    }

    public long getGroupByDataCount() {
        return data == null ? 0 : data.dataCount();
    }

    public long getDataCount() {
        return dataCount;
    }

    public Map<Class<? extends ReferentialDto>, Long> getReferentialCountMap() {
        return referentialCount;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public DataGroupByDto<?> getGroupBy(String groupByValue) {
//        DataGroupByDto<?> result = getData().stream().filter(p -> Objects.equals(groupByValue, p.getFilterValue())).findFirst().orElse(null);
//        if (result == null) {
//            throw new IllegalStateException(String.format("Can't find groupBy with id: %s", groupByValue));
//        }
//        return result;
        return getData().stream().filter(p -> Objects.equals(groupByValue, p.getFilterValue())).findFirst().orElse(null);
    }

    public DataGroupByDtoSet<?, ?> getData() {
        return data;
    }
}
