package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import java.awt.Color;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class NavigationTreeNodeRendererContext {
    public final boolean open;
    public final boolean enabled;
    public final Icon icon;
    public final String text;
    public final Color color;
    public final Integer count;

    public NavigationTreeNodeRendererContext(NavigationTreeNode node) {
        this.open = Boolean.TRUE.equals(node.getState(NavigationTreeNodeBean.STATE_EDIT_ID));
        this.enabled = node.getUserObject().isReferential() || Boolean.TRUE.equals(node.getState(NavigationTreeNodeBean.STATE_ENABLED));
        if (node.isLeaf()) {
            this.count = node.getState(NavigationTreeNodeBean.STATE_COUNT);
        } else {
            this.count = node.getChildCount();
        }
        this.icon = node.getIcon();
        this.text = node.getUserObject().getText();
        this.color = enabled && (!node.getUserObject().isReferentialType() || count > 0) ? Color.BLACK : Color.GRAY;
    }
}
