package fr.ird.observe.navigation.tree.selection.bean;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.selection.IdAndLastUpdateDate;
import fr.ird.observe.navigation.tree.selection.IdState;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;

import java.util.Date;

/**
 * Created on 04/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class RootOpenSelectionTreeNodeBean extends SelectionTreeNodeBean implements IdState.WithIdState {

    private transient IdAndLastUpdateDate idAndLastUpdateDate;

    public final Date getLastUpdateDate() {
        return STATE_LAST_UPDATE_DATE.getValue(this);
    }

    public final Class<? extends DataDto> getDataType() {
        return STATE_DATA_TYPE.getValue(this);
    }

    @Override
    public ToolkitIdLabel toLabel() {
        return new ToolkitIdLabel(getDataType(), getId(), getLastUpdateDate(), getText());
    }

    @Override
    public IdState getIdState() {
        return ID_STATE.getValue(this);
    }

    public void setIdState(IdState idState) {
        ID_STATE.setValue(this, idState);
    }

    public IdAndLastUpdateDate toIdAndLastUpdateDate() {
        if (idAndLastUpdateDate == null) {
            idAndLastUpdateDate = new IdAndLastUpdateDate(getId(), getLastUpdateDate());
        }
        return idAndLastUpdateDate;
    }

    @Override
    public <N extends ToolkitTreeNode> Class<N> getNodeMapping(String name) {
        return super.getNodeMapping("children");
    }
}
