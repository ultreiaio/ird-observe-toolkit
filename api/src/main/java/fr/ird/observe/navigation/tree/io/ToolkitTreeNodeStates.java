package fr.ird.observe.navigation.tree.io;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Represents all states of a bean.
 * <p>
 * We need this special object to make gson usage simple
 * <p>
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeNodeStates implements JsonAware {

    private final Map<String, Object> states = new LinkedHashMap<>();

    public static ToolkitTreeNodeStates of() {
        return new ToolkitTreeNodeStates();
    }

    public static ToolkitTreeNodeStates of(String k1, Object v1) {
        ToolkitTreeNodeStates states = of();
        states.addState(k1, v1);
        return states;
    }

    public static ToolkitTreeNodeStates of(Stream<Map.Entry<String, Object>> states) {
        ToolkitTreeNodeStates state = of();
        states.forEach(e -> state.getStates().put(e.getKey(), e.getValue()));
        return state;
    }

    public Map<String, Object> getStates() {
        return states;
    }

    public final void addState(String stateName, Object value) {
        getStates().put(Objects.requireNonNull(stateName), value);
    }

    @SuppressWarnings("unchecked")
    public final <O> O getState(String stateName) {
        return (O) getStates().get(Objects.requireNonNull(stateName));
    }

}
