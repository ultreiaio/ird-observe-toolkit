package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.id.IdNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Vector;
import java.util.function.Predicate;

/**
 * Created on 16/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
@SuppressWarnings("unchecked")
public class VerticalNavigationHelper {
    private static final Logger log = LogManager.getLogger(VerticalNavigationHelper.class);

    public static <N extends ToolkitTreeNode> N find0(ToolkitTreeNode node, Class<N> nodeType, Predicate<ToolkitTreeNode> predicate) {
        Enumeration<TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode treeNode = (ToolkitTreeNode) children.nextElement();
            if (nodeType.isAssignableFrom(treeNode.getClass()) && predicate.test(treeNode)) {
                return (N) treeNode;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <N extends ToolkitTreeNode> N findPath0(ToolkitTreeNode node, String path) {
        String[] paths = path.split("/");
        ToolkitTreeNode result = node;
        for (String directPath : paths) {
            ToolkitTreeNode childNode = findDirectPath0(result, directPath);
            if (childNode == null) {
                return null;
            }
            result = childNode;
        }
        return (N) result;
    }

    @SuppressWarnings("unchecked")
    public static <N extends ToolkitTreeNode> N findDirectPath0(ToolkitTreeNode node, String path) {
        boolean root = node.isRoot();
        Enumeration<TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode treeNode = (ToolkitTreeNode) children.nextElement();
            if (Objects.equals(path, treeNode.getUserObject().getPath(root))) {
                return (N) treeNode;
            }
        }
        return null;
    }


    /**
     * @param node          node to use
     * @param <N>           type of node
     * @param childNodeType type of node
     * @return first child with exact node type matching
     */
    public static <N extends ToolkitTreeNode> N findFirstChild(ToolkitTreeNode node, Class<N> childNodeType) {
        // il faut charger les fils du nœud pour effectuer la recherche
        node.populateChildrenIfNotLoaded();

        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode o = (ToolkitTreeNode) children.nextElement();
            if (Objects.equals(o.getClass(), childNodeType)) {
                return childNodeType.cast(o);
            }
        }
        return null;
    }

    public static <N extends ToolkitTreeNode> N findPrevious(N node) {
        N result = findPreviousSibling(node);
        if (result != null) {
            log.info(String.format("%s - Found direct previous sibling: %s", node.getUserObject().getPath(), result.getNodePath()));
            return result;
        }
        // go to parent to find this previous sibling
        ToolkitTreeNode parent = node.getParent();
        do {
            ToolkitTreeNode newParent = null;
            if (parent != null) {
                log.info(String.format("%s - Will try to get previous parent sibling from parent: %s", node.getUserObject().getPath(), parent.getNodePath()));
                newParent = findPrevious(parent);
            }
            // get new parent candidate
            if (newParent == null) {
                return null;
            }
            log.info(String.format("%s - Found previous parent sibling candidate: %s", node.getUserObject().getPath(), newParent.getNodePath()));

            // go to the last child
            result = (N) findLastChild(newParent, node.getClass());
            if (result == null) {
                // will try from this parent
                parent = newParent;
                log.info(String.format("%s - No matching previous sibling from parent candidate: %s", node.getUserObject().getPath(), newParent.getNodePath()));
            } else {
                log.info(String.format("%s - Found matching previous sibling from parent candidate: %s", node.getUserObject().getPath(), result.getNodePath()));
            }
        } while (result == null);
        return result;
    }

    public static <N extends ToolkitTreeNode> N findNext(N node) {
        N result = findNextSibling(node);
        if (result != null) {
            log.info(String.format("%s - Found direct next sibling: %s", node.getUserObject().getPath(), result.getNodePath()));
            return result;
        }
        // go to parent to find this next sibling
        ToolkitTreeNode parent = node.getParent();
        do {
            // get new parent candidate
            ToolkitTreeNode newParent = parent == null ? null : findNext(parent);
            if (newParent == null) {
                return null;
            }
            log.info(String.format("%s - Found next parent sibling candidate: %s", node.getUserObject().getPath(), newParent.getNodePath()));
            // go to first child
            result = (N) findFirstChild(newParent, node.getClass());
            if (result == null) {
                // will try from this parent
                parent = newParent;
                log.info(String.format("%s - No matching next sibling from parent candidate: %s", node.getUserObject().getPath(), newParent.getNodePath()));
            } else {
                log.info(String.format("%s - Found matching next sibling from parent candidate: %s", node.getUserObject().getPath(), result.getNodePath()));
            }
        } while (result == null);
        return result;
    }


    public static <N extends ToolkitTreeNode> N findChildByType(ToolkitTreeNode node, Class<N> childType) {
        // il faut charger les fils du nœud pour effectuer la recherche
        node.populateChildrenIfNotLoaded();

        if (node.isLeaf()) {
            // au final le nœud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode childrenNode = (ToolkitTreeNode) children.nextElement();
            if (childType.isAssignableFrom(childrenNode.getClass())) {
                return childType.cast(childrenNode);
            }
        }
        return null;
    }

    public static <N extends ToolkitTreeNode> N findChildByType(ToolkitTreeNode node, Class<N> childType, String id) {
        // il faut charger les fils du nœud pour effectuer la recherche
        node.populateChildrenIfNotLoaded();

        if (node.isLeaf()) {
            // au final le nœud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode childrenNode = (ToolkitTreeNode) children.nextElement();
            if (childType.isAssignableFrom(childrenNode.getClass()) && childrenNode.isEqualsId(id)) {
                return childType.cast(childrenNode);
            }
        }
        return null;
    }

    /**
     * @param node node to use
     * @param <N>  type of node
     * @return previous sibling with exact node type matching
     */
    public static <N extends ToolkitTreeNode> N findPreviousSibling(N node) {
        ToolkitTreeNode parent = node.getParent();
        if (parent == null) {
            return null;
        }
        int index = parent.getIndex(node);
        Vector<?> children = parent.childrenVector();
        for (int i = index - 1; i > -1; i--) {
            ToolkitTreeNode o = (ToolkitTreeNode) children.elementAt(i);
            if (Objects.equals(node.getClass(), o.getClass())) {
                return (N) o;
            }
        }
        return null;
    }

    /**
     * @param node      node to use
     * @param siblingId id to seek
     * @param <N>       type of node
     * @return sibling node with the given id
     */
    public static <N extends ToolkitTreeNode> N findSibling(N node, String siblingId) {
        ToolkitTreeNode parent = node.getParent();
        return (N) VerticalNavigationHelper.findChildByType(parent, node.getClass(), siblingId);
    }

    /**
     * @param node      node to use
     * @param siblingId id to seek
     * @param <N>       type of node
     * @return sibling node with the given id (using parent sibling on given id)
     */
    public static <N extends ToolkitTreeNode> N findParentSibling(N node, String siblingId) {
        ToolkitTreeNode sibling = findSibling(node.getParent(), siblingId);
        return (N) VerticalNavigationHelper.findChildByType(sibling, node.getClass());
    }

    /**
     * @param node node to use
     * @param <N>  type of node
     * @return next sibling with exact node type matching
     */
    public static <N extends ToolkitTreeNode> N findNextSibling(N node) {
        ToolkitTreeNode parent = node.getParent();
        if (parent == null) {
            return null;
        }
        int index = parent.getIndex(node);
        int childCount = parent.getChildCount();
        Vector<?> children = parent.childrenVector();
        for (int i = index + 1; i < childCount; i++) {
            ToolkitTreeNode o = (ToolkitTreeNode) children.elementAt(i);
            if (Objects.equals(node.getClass(), o.getClass())) {
                return (N) o;
            }
        }
        return null;
    }

    /**
     * @param node          node to use
     * @param <N>           type of node
     * @param childNodeType type of node
     * @return last child with exact node type matching
     */
    public static <N extends ToolkitTreeNode> N findLastChild(ToolkitTreeNode node, Class<N> childNodeType) {
        node.populateChildrenIfNotLoaded();

        int childCount = node.getChildCount();
        Vector<?> childrenVector = node.childrenVector();
        for (int i = childCount - 1; i > -1; i--) {
            ToolkitTreeNode o = (ToolkitTreeNode) childrenVector.elementAt(i);
            if (Objects.equals(o.getClass(), childNodeType)) {
                return childNodeType.cast(o);
            }
        }
        return null;
    }

    public static <N extends ToolkitTreeNode> N findChildById(ToolkitTreeNode node, String id) {
        if (id == null) {
            // id null ? donc rien a faire
            return null;
        }
        if (node.isEqualsId(id)) {
            // on a trouve le bon nœud
            return (N) node;
        }

        // il faut charger les fils du nœud pour effectuer la recherche
        node.populateChildrenIfNotLoaded();

        if (node.getChildCount() == 0) {
            // au final le nœud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            ToolkitTreeNode childrenNode = (ToolkitTreeNode) children.nextElement();
            if (childrenNode.isEqualsId(id)) {
                return (N) childrenNode;
            }
        }
        return null;
    }

    public static <N extends ToolkitTreeNode> N findChildByModelNode(ToolkitTreeNode node, IdNode<?> modelNode) {

        //FIXME generate in navigation model node the path to data... so after just a search of the exact path
        // il faut charger les fils du nœud pour effectuer la recherche
        node.populateChildrenIfNotLoaded();

        if (node.isLeaf()) {
            return null;
        }
        String modelNodePath = modelNode.getModel();
        return findPath0(node, modelNodePath);

    }
}
