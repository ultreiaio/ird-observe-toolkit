package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import java.awt.Color;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 14/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class SelectionTreeNodeRendererContext {
    public final boolean open;
    public final boolean enabled;
    public final boolean exist;
    public final Icon icon;
    public final String text;
    public final String tip;
    public final Color color;
    public final Integer count;
    public final SelectionState selectionState;

    public SelectionTreeNodeRendererContext(SelectionTreeNode node) {
        this.open = Boolean.TRUE.equals(node.getState(SelectionTreeNodeBean.STATE_EDIT_ID));
        this.enabled = node.getUserObject().isReferential() || Boolean.TRUE.equals(node.getState(SelectionTreeNodeBean.STATE_ENABLED));
        this.exist = Boolean.TRUE.equals(node.getState(SelectionTreeNodeBean.STATE_EXIST));
        if (node.isLeaf()) {
            this.count = node.getState(SelectionTreeNodeBean.STATE_COUNT);
        } else {
            this.count = node.getChildCount();
        }
        this.icon = buildIcon(node);
        this.text = buildText(node);
        this.tip = buildTip(node, text);
        this.color = enabled && (!node.getUserObject().isReferentialType() || count > 0) ? Color.BLACK : Color.GRAY;
        this.selectionState = node.getSelectionState();
    }

    protected Icon buildIcon(SelectionTreeNode node) {
        return node.getIcon();
    }

    protected String buildText(SelectionTreeNode node) {
        String text = node.getUserObject().getText();
        if (exist) {
            text = t("observe.common.exist.on.remote", text);
        }
        return text;
    }

    protected String buildTip(SelectionTreeNode node, String text) {
        String tip = text;
        if (exist) {
            tip += t("observe.ui.message.warning.will.be.delete", text);
        }
        return tip;
    }
}
