package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * A node with an id.
 * <p>
 * Created on 12/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.0
 */
public abstract class IdNode<D extends BusinessDto> extends AbstractJavaBean {

    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_ENABLED = "enabled";

    protected transient final SingletonSupplier<? extends BusinessProject> project;
    protected transient final SingletonSupplier<? extends BusinessModule> module;
    protected transient final SingletonSupplier<? extends BusinessSubModule> subModule;
    protected transient final SingletonSupplier<Class<? extends DtoReference>> referenceType;

    /**
     * Type of dto associated to this node.
     */
    private final Class<D> type;
    /**
     * Optional children of this node.
     */
    private final List<IdNode<?>> children;
    /**
     * Is node selectable? (means is there a concrete node in tree model)
     */
    private final boolean selectable;
    /**
     * Is node editable? (means is selectable plus open/close concept)
     */
    private final boolean editable;
    /**
     * Optional parent of this node.
     */
    private IdNode<?> parent;
    /**
     * Optional id associated with this node.
     */
    private String id;

    public IdNode(IdNode<?>... children) {
        this.type = ObserveUtil.getFirstType(this);
        this.editable = OpenableDto.class.isAssignableFrom(type) || RootOpenableDto.class.isAssignableFrom(type);
        this.selectable = editable || EditableDto.class.isAssignableFrom(type);
        this.children = List.of(children);
        this.children.forEach(c -> c.parent = this);
        this.project = SingletonSupplier.of(getProjectSupplier());
        this.module = SingletonSupplier.of(() -> getProject().getBusinessModule(getClass().getPackage().getName()));
        this.subModule = SingletonSupplier.of(() -> getProject().getBusinessSubModule(getModule(), getClass().getPackage().getName()));
        this.referenceType = SingletonSupplier.of(() -> getProject().getMapping().getReferenceType(type));
    }

    public abstract int getLevel();

    protected abstract Supplier<? extends BusinessProject> getProjectSupplier();

    public Class<D> getType() {
        return type;
    }

    public Class<? extends DtoReference> getReferenceType() {
        return referenceType.get();
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public String getSimpleName() {
        return getSubModule().getName().toLowerCase() + getType().getSimpleName().replace("Dto", "");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        String oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
        firePropertyChange(PROPERTY_ENABLED, oldValue != null, id != null);
        if (id == null) {
            getChildren().forEach(n -> n.setId(null));
        }
    }

    public List<IdNode<?>> getChildren() {
        return children;
    }

    public List<IdNode<?>> getEditableChildren() {
        return children.stream().filter(IdNode::isEditable).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <N extends IdNode<?>> N getChildren(Class<N> nodeType) {
        return (N) children.stream().filter(t -> t.getClass().equals(nodeType)).findFirst().orElse(null);
    }

    @SuppressWarnings("unchecked")
    public <N extends IdNode<?>> N getChildren(int index) {
        return (N) children.get(index);
    }

    public <N extends IdNode<?>> N getNode(int index) {
        return getChildren(index);
    }

    public String getModel() {
        return getModule().getName();
    }

    public BusinessProject getProject() {
        return project.get();
    }

    public BusinessModule getModule() {
        return module.get();
    }

    public BusinessSubModule getSubModule() {
        return subModule.get();
    }

    public IdNode<?> getRoot() {
        return isRoot() ? this : getParent().getRoot();
    }

    public IdNode<?> getParent() {
        return parent;
    }

    public boolean accept(BusinessDto dto) {
        boolean accept = getType().isAssignableFrom(dto.getClass());
        if (accept && getLevel() == 0) {
            accept = getProject().accept(getModule(), dto);
        }
        return accept;
    }

    @SuppressWarnings("unchecked")
    public <N extends IdNode<?>> List<N> getShellForClose() {
        return IdNodeHelper.getShellForClose((N) this);
    }

    public IdNode<?> upToSharedAncestor(IdNode<?> otherNode) {
        return IdNodeHelper.upToSharedAncestor(this, otherNode);
    }

    public List<IdNode<?>> getNodesFromAncestor(IdNode<?> other) {
        return IdNodeHelper.getNodesFromAncestor(this, other);
    }

    public List<IdNode<?>> getNodesToAncestor(IdNode<?> other) {
        return IdNodeHelper.getNodesFromAncestor(this, other);
    }

    public List<IdNode<?>> getNodesFromRoot() {
        return IdNodeHelper.getNodesFromRoot(this);
    }

    public List<IdNode<?>> getNodesToRoot() {
        return IdNodeHelper.getNodesToRoot(this);
    }

    public boolean isRoot() {
        return getParent() == null;
    }

    public boolean isLeaf() {
        return getChildren().isEmpty();
    }

    public boolean isEnabled() {
        return getId() != null;
    }

    public boolean isDisabled() {
        return !isEnabled();
    }

    public boolean acceptModel(String model) {
        return Objects.equals(getModel(), model);
    }

    public void copyTo(IdNode<?> targetNode) {
        targetNode.setId(getId());
        IdNode<?> parent = getParent();
        IdNode<?> targetNodeParent = targetNode.getParent();
        if (parent != null) {
            parent.copyTo(targetNodeParent);
        }
    }

    public void getIds(List<IdNode<?>> builder) {
        if (this.isEnabled()) {
            builder.add(this);
            for (IdNode<?> child : getChildren()) {
                child.getIds(builder);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdNode<?> that = (IdNode<?>) o;
        return Objects.equals(type, that.type)
                && Objects.equals(id, that.id)
                && Objects.equals(getModel(), that.getModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getModel(), type, id);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "parent=" + (parent == null ? "" : parent.getClass().getSimpleName()) +
                ", level=" + getLevel() +
                (isEnabled() ? ", id='" + id + '\'' : ", disabled") + '}';
    }

}
