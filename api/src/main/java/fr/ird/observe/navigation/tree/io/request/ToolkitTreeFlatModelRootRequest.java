package fr.ird.observe.navigation.tree.io.request;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.ToolkitTreeLoadingConfig;
import io.ultreia.java4all.util.json.JsonAware;

/**
 * To load the root node of a tree model.
 * <p>
 * Created on 07/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeFlatModelRootRequest implements JsonAware, ToolkitTreeLoadingConfig {
    /**
     * Business module name.
     */
    private String moduleName;
    /**
     * Group by name.
     */
    private String groupByName;
    /**
     * Group by flavor.
     */
    private String groupByFlavor;
    /**
     * Load data?
     */
    private boolean loadData = true;
    /**
     * Load referential?
     */
    private boolean loadReferential;
    /**
     * Load empty group by?
     */
    private boolean loadEmptyGroupBy;
    /**
     * Load disabled group by?
     */
    private boolean loadDisabledGroupBy;
    /**
     * Load null group by?
     */
    private boolean loadNullGroupBy;
    /**
     * Load temporal group by?
     */
    private boolean loadTemporalGroupBy;

    @Override
    public boolean isLoadData() {
        return loadData;
    }

    @Override
    public void setLoadData(boolean loadData) {
        this.loadData = loadData;
    }

    @Override
    public boolean isLoadReferential() {
        return loadReferential;
    }

    @Override
    public void setLoadReferential(boolean loadReferential) {
        this.loadReferential = loadReferential;
    }

    @Override
    public String getModuleName() {
        return moduleName;
    }

    @Override
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public String getGroupByName() {
        return groupByName;
    }

    @Override
    public void setGroupByName(String groupByName) {
        this.groupByName = groupByName;
    }

    @Override
    public String getGroupByFlavor() {
        return groupByFlavor;
    }

    @Override
    public void setGroupByFlavor(String groupByFlavor) {
        this.groupByFlavor = groupByFlavor;
    }

    @Override
    public boolean isLoadTemporalGroupBy() {
        return loadTemporalGroupBy;
    }

    @Override
    public void setLoadTemporalGroupBy(boolean loadTemporalGroupBy) {
        this.loadTemporalGroupBy = loadTemporalGroupBy;
    }

    @Override
    public boolean isLoadEmptyGroupBy() {
        return loadEmptyGroupBy;
    }

    @Override
    public void setLoadEmptyGroupBy(boolean loadEmptyProgram) {
        this.loadEmptyGroupBy = loadEmptyProgram;
    }

    @Override
    public boolean isLoadDisabledGroupBy() {
        return loadDisabledGroupBy;
    }

    @Override
    public void setLoadDisabledGroupBy(boolean loadDisabledProgram) {
        this.loadDisabledGroupBy = loadDisabledProgram;
    }

    @Override
    public boolean isLoadNullGroupBy() {
        return loadNullGroupBy;
    }

    @Override
    public void setLoadNullGroupBy(boolean loadNullGroupBy) {
        this.loadNullGroupBy = loadNullGroupBy;
    }

    @Override
    public String toString() {
        return print();
    }
}
