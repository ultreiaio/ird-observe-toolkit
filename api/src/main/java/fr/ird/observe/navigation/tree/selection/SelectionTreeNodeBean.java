package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToToolkitIdLabel;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBeanState;
import fr.ird.observe.navigation.tree.states.BooleanState;

/**
 * Data bean associated within a node in the selection tree model.
 * <p>
 * Created on 03/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class SelectionTreeNodeBean extends ToolkitTreeNodeBean implements SelectionState.WithSelectionState, ToToolkitIdLabel {
    public static final ToolkitTreeNodeBeanState<SelectionState> STATE_SELECTION_STATE = ToolkitTreeNodeBeanState.state(false, SelectionState.class, "selectionState");
    public static final BooleanState STATE_EXIST = BooleanState.transientState(false, "exist");

    public SelectionTreeNodeBean() {
        setSelectionState(SelectionState.EMPTY);
    }

    @Override
    public final SelectionState getSelectionState() {
        return STATE_SELECTION_STATE.getValue(this);
    }

    public final void setSelectionState(SelectionState selectionState) {
        STATE_SELECTION_STATE.setValue(this, selectionState);
    }

    public boolean existsOnOtherSide() {
        return SelectionTreeNodeBean.STATE_EXIST.getValue(this);
    }
}
