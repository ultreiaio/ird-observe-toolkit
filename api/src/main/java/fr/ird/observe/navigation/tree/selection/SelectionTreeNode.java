package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.VerticalNavigationHelper;

import javax.swing.tree.TreeNode;
import java.util.Objects;

/**
 * Represents a node in the selection tree model.
 * Created on 03/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class SelectionTreeNode extends ToolkitTreeNode implements SelectionState.WithSelectionState {

    protected SelectionTreeNode(SelectionTreeNodeBean userObject) {
        super(userObject);
        // selection node is always loaded, no auto-children loading mechanism here
        loaded();
    }

    /**
     * @return the selection state of the node
     */
    @Override
    public final SelectionState getSelectionState() {
        return getUserObject().getSelectionState();
    }

    /**
     * Change selection state on this node.
     * <p>
     * This will modify selection state on all children and all maybe on all parents to root.
     *
     * @param selected new selected state value
     */
    public final void setSelected(boolean selected) {
        SelectionHelper.setSelected(this, selected);
    }

    public final <N extends SelectionTreeNode> N find(Class<N> nodeType) {
        return VerticalNavigationHelper.find0(this, nodeType, n -> true);
    }

    public final <N extends SelectionTreeNode> N findPath(String path) {
        return VerticalNavigationHelper.findPath0(this, path);
    }

    public final <N extends SelectionTreeNode> N find(Class<N> nodeType, ToolkitIdDtoBean id) {
        return VerticalNavigationHelper.find0(this, nodeType, n -> Objects.equals(n.getUserObject().getId(), id.getId()));
    }

    @Override
    public final void dirty() {
    }

    @Override
    protected final void dirtyStructure() {
    }

    @Override
    protected final void populateChildrenIfNotLoaded() {
        // in selection node, all node are always loaded
    }


    //--------------------------------------------------------------------------------------------
    // TreeNode override methods
    //--------------------------------------------------------------------------------------------

    @Override
    public SelectionTreeNode getParent() {
        return (SelectionTreeNode) super.getParent();
    }

    @Override
    public SelectionTreeNode getRoot() {
        return (SelectionTreeNode) super.getRoot();
    }

    @Override
    public SelectionTreeNodeBean getUserObject() {
        return (SelectionTreeNodeBean) super.getUserObject();
    }

    @Override
    public SelectionTreeNode getChildAt(int index) {
        return (SelectionTreeNode) super.getChildAt(index);
    }

    @Override
    public SelectionTreeNode getFirstChild() {
        return (SelectionTreeNode) super.getFirstChild();
    }

    @Override
    public SelectionTreeNode getLastChild() {
        return (SelectionTreeNode) super.getLastChild();
    }

    @Override
    public SelectionTreeNode getChildAfter(TreeNode aChild) {
        return (SelectionTreeNode) super.getChildAfter(aChild);
    }

    @Override
    public SelectionTreeNode getChildBefore(TreeNode aChild) {
        return (SelectionTreeNode) super.getChildBefore(aChild);
    }

    @Override
    public SelectionTreeNode getNextSibling() {
        return (SelectionTreeNode) super.getNextSibling();
    }

    @Override
    public SelectionTreeNode getPreviousSibling() {
        return (SelectionTreeNode) super.getPreviousSibling();
    }

    @Override
    public SelectionTreeNode getFirstLeaf() {
        return (SelectionTreeNode) super.getFirstLeaf();
    }

    @Override
    public SelectionTreeNode getLastLeaf() {
        return (SelectionTreeNode) super.getLastLeaf();
    }

    @Override
    public SelectionTreeNode getNextLeaf() {
        return (SelectionTreeNode) super.getNextLeaf();
    }

    @Override
    public SelectionTreeNode getPreviousLeaf() {
        return (SelectionTreeNode) super.getPreviousLeaf();
    }
}
