package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.Arrays;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class NavigationTreeSupport<R extends NavigationTreeNode, M extends NavigationTreeModelSupport<R>> extends JXTreeWithNoSearch {
    private static final Logger log = LogManager.getLogger(NavigationTreeSupport.class);

    public NavigationTreeSupport(M model) {
        super(model);
        setLargeModel(true);
        setCellRenderer(new NavigationTreeCellRenderer());
        setRootVisible(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        setToggleClickCount(2);
    }

    @Override
    public void updateUI() {
        setSelectionModel(new NavigationTreeSelectionModel(null));
        super.updateUI();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final M getModel() {
        return (M) super.getModel();
    }

    @Override
    public NavigationTreeSelectionModel getSelectionModel() {
        return (NavigationTreeSelectionModel) super.getSelectionModel();
    }

    public NavigationTreeNode getSelectedNode() {
        return isSelectionEmpty() || getSelectionPath() == null ? null : (NavigationTreeNode) getSelectionPath().getLastPathComponent();
    }

    /**
     * Selects the given {@code node} in the tree without doing any check on previous content loaded.
     * <p>
     * This method should be used only programmatically.
     *
     * @param node the node to select
     */
    public void reloadAndSelectSafeNode(NavigationTreeNode node) {
        log.info(String.format("reload and to select safe node [%s]", node));
        //FIXME:TreeNg node.reloadNodeData();
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().setSkipCheckPreviousContent(true);
        try {
            setSelectionPath(path);
        } finally {
            getSelectionModel().setSkipCheckPreviousContent(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    /**
     * Selects the given {@code node} in the tree without doing any check on previous content loaded.
     * <p>
     * This method should be used only programmatically.
     *
     * @param node the node to select
     */
    public void selectSafeNode(TreeNode node) {
        log.info(String.format("try to select safe node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().setSkipCheckPreviousContent(true);
        try {
            setSelectionPath(path);
        } finally {
            getSelectionModel().setSkipCheckPreviousContent(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    public void reSelectSafeNode(TreeNode node) {
        log.info(String.format("try to reselect safe node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        getSelectionModel().clearSelection();
        getSelectionModel().setSkipCheckPreviousContent(true);
        try {
            setSelectionPath(path);
        } finally {
            getSelectionModel().setSkipCheckPreviousContent(false);
        }
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    @Override
    public boolean isRowSelected(int requiredRow) {
        int[] selectedRows = getSelectionRows();
        if (selectedRows != null) {
            return Arrays.stream(selectedRows).anyMatch(selectedRow -> requiredRow == selectedRow);
        }
        return false;
    }

    /**
     * Selects the given {@code node} in the registered tree.
     *
     * @param node the node to select
     */
    public void selectNode(NavigationTreeNode node) {
        if (node == null) {
            log.error("Can't load null node.", new NullPointerException());
            return;
        }
        log.info(String.format("try to select node [%s]", node));
        TreePath path = new TreePath(getModel().getPathToRoot(node));
        setSelectionPath(path);
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    public void addNavigationTreeSelectionListener(NavigationTreeSelectionListener x) {
        getSelectionModel().addNavigationTreeSelectionListener(x);
    }

    @SuppressWarnings("unused")
    public void removeNavigationTreeSelectionListener(NavigationTreeSelectionListener x) {
        getSelectionModel().removeNavigationTreeSelectionListener(x);
    }

    public NavigationTreeNode getNodeForRow(int row) {
        return (NavigationTreeNode) getPathForRow(row).getLastPathComponent();
    }

    public void selectFirstNode() {
        setSelectionRow(0);
    }
}
