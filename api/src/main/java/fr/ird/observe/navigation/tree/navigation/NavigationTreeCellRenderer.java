package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.JTree;
import java.awt.Component;
import java.awt.Font;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class NavigationTreeCellRenderer extends DefaultXTreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        NavigationTreeNode node = (NavigationTreeNode) value;
        NavigationTreeNodeRendererContext context = newContext(node);
        String text = context.text;
        Integer count = context.count;
        if (count != null) {
            text = String.format("%s (%d)", text, count);
        }
        super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
        apply(context);
        return this;
    }

    protected NavigationTreeNodeRendererContext newContext(NavigationTreeNode node) {
        return new NavigationTreeNodeRendererContext(node);
    }

    protected void apply(NavigationTreeNodeRendererContext context) {
        setForeground(context.color);
        setIcon(context.icon);
        if (context.open) {
            setFont(getFont().deriveFont(Font.BOLD));
        } else {
            setFont(getFont().deriveFont(Font.PLAIN));
        }
    }

}

