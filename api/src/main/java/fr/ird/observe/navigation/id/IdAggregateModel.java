package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import io.ultreia.java4all.bean.AbstractJavaBean;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 26/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class IdAggregateModel extends AbstractJavaBean {

    public static final String PROPERTY_ENABLED = "enabled";
    private final List<IdModel> models;
    private transient boolean enabled;

    public IdAggregateModel(IdModel... models) {
        this.models = List.of(models);
        for (IdModel model : this.models) {
            model.addPropertyChangeListener(IdModel.PROPERTY_ENABLED, e -> updateEnabled());
        }
        updateEnabled();
    }

    public boolean isEnabled() {
        return enabled;
    }

    @SuppressWarnings("unchecked")
    public <M extends IdModel> Optional<M> forModelType(Class<M> type) {
        Objects.requireNonNull(type);
        return (Optional<M>) models.stream().filter(e -> Objects.equals(type, e.getClass())).findFirst();
    }

    private void updateEnabled() {
        boolean oldValue = isEnabled();
        enabled = models.stream().anyMatch(IdModel::isEnabled);
        firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    public List<IdModel> getModels() {
        return models;
    }

    public List<String> getIds() {
        List<String> builder = new LinkedList<>();
        getModels().forEach(m -> m.getNodesWithIds().forEach(b -> builder.add(b.getId())));
        return builder;
    }

    //FIXME Unsafe method We should remove this method since result is not deterministic and can be wrong :(
    public <D extends BusinessDto> Optional<? extends IdNode<D>> forDtoType(Class<D> dtoType) {
        Objects.requireNonNull(dtoType);
        for (IdModel model : models) {
            IdNode<D> node = model.getDtoNode(dtoType);
            if (node != null) {
                return Optional.of(node);
            }
        }
        return Optional.empty();
    }

    public <D extends BusinessDto> Optional<IdNode<D>> forNavigationNode(IdNode<?> source) {
        Objects.requireNonNull(source);
        for (IdModel model : models) {
            if (model.rejectModel(source.getModel())) {
                continue;
            }
            IdNode<D> node = model.getNavigationNode(source);
            if (node != null) {
                return Optional.of(node);
            }
        }
        return Optional.empty();
    }

    public <N extends IdNode<?>> Optional<N> forNodeType(Class<N> nodeType) {
        Objects.requireNonNull(nodeType);
        for (IdModel model : models) {
            N node = model.getNode(nodeType);
            if (node != null) {
                return Optional.of(node);
            }
        }
        return Optional.empty();
    }

    public void load(IdAggregateModel model) {
        for (IdModel IdModel : model.getModels()) {
            for (IdNode<?> node : IdModel.getNodesWithIds()) {
                forNodeType(node.getClass()).orElseThrow(IllegalStateException::new).setId(node.getId());
            }
        }
    }

    public List<IdModel> getEnabledModels() {
        return getModels().stream().filter(IdModel::isEnabled).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdAggregateModel that = (IdAggregateModel) o;
        return Objects.equals(models, that.models);
    }

    @Override
    public int hashCode() {
        return Objects.hash(models);
    }

    public int count() {
        return models.stream().mapToInt(IdModel::count).sum();
    }

    public void clearModel() {
        models.forEach(IdModel::clearModel);
    }
}
