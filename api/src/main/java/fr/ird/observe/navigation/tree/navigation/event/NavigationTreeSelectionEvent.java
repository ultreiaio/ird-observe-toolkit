package fr.ird.observe.navigation.tree.navigation.event;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;

public class NavigationTreeSelectionEvent extends TreeSelectionEvent {

    private final boolean skipCheckPreviousContent;

    public NavigationTreeSelectionEvent(Object source, TreePath[] paths, boolean[] areNew, TreePath oldLeadSelectionPath, TreePath newLeadSelectionPath, boolean skipCheckPreviousContent) {
        super(source, paths, areNew, oldLeadSelectionPath, newLeadSelectionPath);
        this.skipCheckPreviousContent = skipCheckPreviousContent;
    }

    public NavigationTreeSelectionEvent(Object source, TreePath path, boolean isNew, TreePath oldLeadSelectionPath, TreePath newLeadSelectionPath, boolean skipCheckPreviousContent) {
        super(source, path, isNew, oldLeadSelectionPath, newLeadSelectionPath);
        this.skipCheckPreviousContent = skipCheckPreviousContent;
    }

    public boolean isSkipCheckPreviousContent() {
        return skipCheckPreviousContent;
    }
}
