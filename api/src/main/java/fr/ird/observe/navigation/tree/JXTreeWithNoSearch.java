package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.JXTree;

import javax.swing.text.Position;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Overrides a {@link org.jdesktop.swingx.JXTree} and disable the text search (it does not work very well and throws
 * some exception we don't want, moreover we do not use it).
 * <p>
 * Created at 13/09/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 * FIXME We should move this to JAXX project.
 */
public class JXTreeWithNoSearch extends JXTree {
    public JXTreeWithNoSearch() {
    }

    public JXTreeWithNoSearch(TreeModel newModel) {
        super(newModel);
    }

    @Override
    public TreePath getNextMatch(String prefix, int startingRow, Position.Bias bias) {
        return null;
    }
}
