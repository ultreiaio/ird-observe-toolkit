package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.ToolkitTreeModelSupport;
import fr.ird.observe.navigation.tree.selection.bean.ReferentialTypeSelectionTreeNodeBean;
import fr.ird.observe.spi.module.BusinessProject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.TreeNode;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreeModelSupport<R extends SelectionTreeNode> extends ToolkitTreeModelSupport<R> {

    public static final String SELECTED_COUNT = "selectedCount";
    public static final String SELECTION_EMPTY = "selectionEmpty";
    private static final Logger log = LogManager.getLogger(SelectionTreeModelSupport.class);
    private final List<SelectionTreeNode> selectedData;
    private final List<SelectionTreeNode> selectedReferential;
    /**
     * Count of selected nodes.
     */
    private int selectedCount = -1;

    public static List<String> allDataIds(TreeNode root) {
        Enumeration<?> children = root.children();
        List<String> result = new LinkedList<>();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            Enumeration<TreeNode> dataEnum = topNode.children();
            while (dataEnum.hasMoreElements()) {
                SelectionTreeNode treeNode = (SelectionTreeNode) dataEnum.nextElement();
                result.add(treeNode.getUserObject().getId());
            }
        }
        return result;
    }

    public SelectionTreeModelSupport(BusinessProject businessProject, SelectionTreeConfig config, Supplier<R> rootSupplier) {
        super(businessProject, config, rootSupplier);
        selectedData = new LinkedList<>();
        selectedReferential = new LinkedList<>();
    }

    @Override
    public SelectionTreeConfig getConfig() {
        return (SelectionTreeConfig) super.getConfig();
    }

    @Override
    public final void setRoot(TreeNode root) {
        super.setRoot(root);
        recomputeSelectedCount();
    }

    public final void setValueAt(SelectionTreeNode node, boolean value) {
        node.setSelected(value);
        if (node.isLeaf()) {
            nodeChanged(node.getParent());
            nodeChanged(node);
        } else {
            nodeChanged(node);
            Enumeration<?> children = node.children();
            while (children.hasMoreElements()) {
                nodeChanged((TreeNode) children.nextElement());
            }
        }
        recomputeSelectedCount();
    }

    public int getSelectedCount() {
        return selectedCount;
    }

    /**
     * @return {@code true} if there is nothing selected, {@code false} otherwise
     */
    public boolean isSelectionEmpty() {
        return selectedCount == 0;
    }

    public void setSelectedCount(int selectedCount) {
        int oldValue = getSelectedCount();
        boolean oldSelectionEmpty = isSelectionEmpty();
        this.selectedCount = selectedCount;
        pcs.firePropertyChange(SELECTED_COUNT, oldValue, selectedCount);
        pcs.firePropertyChange(SELECTION_EMPTY, oldSelectionEmpty, isSelectionEmpty());
    }

    public Set<Class<? extends ReferentialDto>> getSelectedReferential() {
        Set<Class<? extends ReferentialDto>> builder = new LinkedHashSet<>();
        for (SelectionTreeNode node : selectedReferential) {
            ReferentialTypeSelectionTreeNodeBean userObject = (ReferentialTypeSelectionTreeNodeBean) node.getUserObject();
            builder.add(userObject.getDataType());
        }
        return builder;
    }

    public Optional<SelectionTreeNode> getDataNode(ToolkitIdLabel data) {
        Enumeration<?> children = getRoot().children();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            SelectionTreeNode tripNode = topNode.findPath(data.getId());
            if (tripNode != null) {
                return Optional.of(tripNode);
            }
        }
        return Optional.empty();
    }

    public void removeData(List<SelectionTreeNode> data) {
        for (SelectionTreeNode node : data) {
            SelectionTreeNode parent = node.getParent();
            if (parent.getParent() == null) {
                // This means that the parent was already removed
                continue;
            }
            if (parent.isSelected() || data.contains(parent)) {
                parent.removeFromParent();
            } else {
                node.removeFromParent();
            }
            if (parent.isLeaf()) {
                parent.removeFromParent();
            }
        }
        recomputeSelectedCount();
    }

    public long allDataCount() {
        Enumeration<?> children = getRoot().children();
        long result = 0;
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            if (topNode.getUserObject().isData()) {
                result += topNode.getChildCount();
            }
        }
        return result;
    }

    /**
     * Select all top nodes, after this the model is full
     *
     * @see #isDataFull()
     */
    public void selectAll() {
        Enumeration<?> children = getRoot().children();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            topNode.setSelected(true);
        }
        recomputeSelectedCount();
    }

    /**
     * Unselect all top nodes, after this the model is empty
     *
     * @see #isSelectionEmpty()
     */
    public void unselectAll() {
        Enumeration<?> children = getRoot().children();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            topNode.setSelected(false);
        }
        recomputeSelectedCount();
    }

    /**
     * @return {@code true} if model is not empty (means contains at least one top node), {@code false} otherwise.
     */
    public boolean isNotEmpty() {
        return !isEmpty();
    }

    /**
     * @return {@code true} if model is empty (means contains no top node), {@code false} otherwise.
     */
    public boolean isEmpty() {
        return getRoot().isLeaf();
    }

    /**
     * @return {@code true} if all top node are selected, {@code false} otherwise.
     */
    public boolean isDataFull() {
        Enumeration<?> children = getRoot().children();
        while (children.hasMoreElements()) {
            SelectionTreeNode topNode = (SelectionTreeNode) children.nextElement();
            if (!topNode.isSelected()) {
                return false;
            }
        }
        return !getRoot().isLeaf();
    }

    public Set<ToolkitIdLabel> getSelectedData() {
        return selectedDataNodes().stream().map(n -> n.getUserObject().toLabel()).collect(Collectors.toSet());
    }

    public Set<String> getSelectedDataIds() {
        return getSelectedDataIds(selectedDataNodes().stream());
    }

    public Set<String> getSelectedDataIds(Stream<SelectionTreeNode> stream) {
        return stream.map(n -> n.getUserObject().getId()).collect(Collectors.toSet());
    }

    public Map<String, List<String>> getSelectedLabels() {
        return selectedDataByParent(k -> k.toLabel().toString(), (k, v) -> v.toLabel().toString());
    }

    public Map<String, List<ToolkitIdLabel>> getSelectedDataByParent() {
        return selectedDataByParent(p -> p.toLabel().toString(), (k, v) -> v.toLabel());
    }

    public <K, V> Map<K, List<V>> selectedDataByParent(Function<ToToolkitIdLabel, K> key, BiFunction<K, ToToolkitIdLabel, V> value) {
        Map<K, List<V>> byParent = new LinkedHashMap<>();
        for (SelectionTreeNode dataNode : selectedDataNodes()) {
            K prefix = key.apply(dataNode.getParent().getUserObject());
            byParent.computeIfAbsent(prefix, k -> new LinkedList<>()).add(value.apply(prefix, dataNode.getUserObject()));
        }
        return byParent;
    }

    public <K, V> Stream<V> streamOnSelectedDataByParent(Function<ToToolkitIdLabel, K> key, BiFunction<K, SelectionTreeNodeBean, V> value) {
        return selectedDataNodes().stream().map(dataNode -> {
            K prefix = key.apply(dataNode.getParent().getUserObject());
            return value.apply(prefix, dataNode.getUserObject());
        });
    }

    public List<SelectionTreeNode> selectedDataNodes() {
        return selectedData;
    }

    public void augmentsExistIds(List<String> otherSideIds) {
        if (otherSideIds != null) {
            List<String> thisSideIds = allDataIds(getRoot());
            // optimization: only deal with this ids that are also on this side
            otherSideIds.removeIf(id -> !thisSideIds.contains(id));
            // augments exist state
            Enumeration<?> enumeration = getRoot().postorderEnumeration();
            while (enumeration.hasMoreElements()) {
                SelectionTreeNode node = (SelectionTreeNode) enumeration.nextElement();
                SelectionTreeNodeBean userObject = node.getUserObject();
                if (otherSideIds.contains(userObject.getId())) {
                    log.debug(String.format("Set exist on other side for node: %s", userObject));
                    SelectionTreeNodeBean.STATE_EXIST.setValue(userObject, true);
                }
            }
        }
    }

    protected void recomputeSelectedCount() {
        selectedData.clear();
        selectedReferential.clear();
        Enumeration<?> enumeration = getRoot().postorderEnumeration();
        while (enumeration.hasMoreElements()) {
            SelectionTreeNode node = (SelectionTreeNode) enumeration.nextElement();
            if (node.isLeaf() && node.isSelected()) {
                if (node.getUserObject().isData()) {
                    selectedData.add(node);
                } else {
                    selectedReferential.add(node);
                }
            }
        }
        int result = selectedData.size() + selectedReferential.size();
        setSelectedCount(result);
    }
}
