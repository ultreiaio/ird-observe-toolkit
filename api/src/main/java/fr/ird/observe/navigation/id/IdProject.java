package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Objects2;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 12/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.0
 */
public abstract class IdProject extends IdAggregateModel {

    public IdProject(IdModel... models) {
        super(models);
    }

    @SuppressWarnings("unchecked")
    <N extends IdProject> N newInstance() {
        return (N) Objects2.newInstance(getClass());
    }

    //
    public <N extends IdNode<?>> N copyNode(IdNode<?> incomingNode, Class<N> IdNodeType, String id) {
        N copy = forNodeType(IdNodeType).orElseThrow(IllegalStateException::new);
        incomingNode.copyTo(copy);
        copy.setId(id);
        return copy;
    }


    public void replaceBy(List<IdNode<?>> selectNodes) {
        Map<IdNode<?>, String> orderedNodes = new LinkedHashMap<>();
        selectNodes.stream().sorted(Comparator.comparing(IdNode::getLevel)).forEach(s -> orderedNodes.put(s, s.getId()));
        clearModel();
        orderedNodes.forEach(IdNode::setId);
    }
//
//    //FIXME Unsafe method We should remove this method since result is not deterministic and can be wrong :(
//    @SuppressWarnings("unchecked")
//    @Override
//    public <D extends BusinessDto> Optional<IdNode<D>> forDtoType(Class<D> dtoType) {
//        return (Optional<IdNode<D>>) super.forDtoType(dtoType);
//    }

}
