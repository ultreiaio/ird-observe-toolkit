package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreeCellRenderer extends DefaultXTreeCellRenderer {

    private final JPanel panel;
    private final JLabel selected;

    public SelectionTreeCellRenderer() {
        selected = new JLabel();
        panel = new JPanel(new BorderLayout(0, 0));
        panel.setOpaque(false);
        panel.add(selected, BorderLayout.WEST);
        panel.add(this, BorderLayout.CENTER);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        SelectionTreeNode node = (SelectionTreeNode) value;
        SelectionTreeNodeRendererContext context = newContext(node);
        String text = context.text;
        Integer count = context.count;
        if (count != null) {
            text = String.format("%s (%d)", text, count);
        }
        super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
        apply(context);
        panel.setPreferredSize(new Dimension(getPreferredSize().width + 20, getPreferredSize().height + 2));
        return panel;
    }

    protected SelectionTreeNodeRendererContext newContext(SelectionTreeNode node) {
        return new SelectionTreeNodeRendererContext(node);
    }

    protected void apply(SelectionTreeNodeRendererContext context) {
        this.selected.setIcon(context.selectionState.getIcon());
        setForeground(context.color);
        setIcon(context.icon);
        panel.setToolTipText(context.tip);
        if (context.open) {
            setFont(getFont().deriveFont(Font.BOLD));
        } else {
            setFont(getFont().deriveFont(Font.PLAIN));
        }
    }

}
