package fr.ird.observe.navigation.tree.navigation.bean;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.navigation.NavigationTreeNodeBean;

/**
 * Created on 04/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class RootOpenFilterNavigationTreeNodeBean extends NavigationTreeNodeBean {
    public int getCount() {
        return STATE_COUNT.getValue(this);
    }

    public final Class<? extends DataGroupByDto<?>> getDataType() {
        return STATE_FILTER_TYPE.getValue(this);
    }

    public final String getGroupByName() {
        return STATE_FILTER_NAME.getValue(this);
    }

    @Override
    public <N extends ToolkitTreeNode> Class<N> getNodeMapping(String name) {
        return super.getNodeMapping("children");
    }
}
