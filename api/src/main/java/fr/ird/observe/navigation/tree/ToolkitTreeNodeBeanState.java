package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;

/**
 * Defines a state in a node bean.
 * Created on 13/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeNodeBeanState<O> {

    private final boolean recursive;
    private final Class<O> type;
    private final String name;
    private boolean _transient;

    public static <O> ToolkitTreeNodeBeanState<O> transientState(boolean recursive, Class<O> type, String name) {
        return new ToolkitTreeNodeBeanState<>(true, recursive, type, name);
    }

    public static <O> ToolkitTreeNodeBeanState<O> state(boolean recursive, Class<O> type, String name) {
        return new ToolkitTreeNodeBeanState<>(false, recursive, type, name);
    }

    public ToolkitTreeNodeBeanState(boolean aTransient, boolean recursive, Class<O> type, String name) {
        this._transient = aTransient;
        this.recursive = recursive;
        this.type = type;
        this.name = name;
    }

    public final boolean isTransient() {
        return _transient;
    }

    public final boolean isRecursive() {
        return recursive;
    }

    public final String name() {
        return name;
    }

    public final Class<O> type() {
        return type;
    }

    public final void makeTransient() {
        _transient = true;
    }

    public final void makeUnTransient() {
        _transient = false;
    }

    public O getValue(ToolkitTreeNodeBean bean) {
        return bean.getState(name());
    }

    public final void setValue(ToolkitTreeNodeBean bean, O value) {
        if (isTransient()) {
            bean.addTransientState(name(), value);
        } else {
            bean.addState(name(), value);
        }
    }

    public final void setValue(ToolkitTreeNodeStates bean, O value) {
        bean.addState(name(), value);
    }
}
