package fr.ird.observe.navigation.tree.states;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBeanState;

/**
 * Created on 14/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class BooleanState extends ToolkitTreeNodeBeanState<Boolean> {

    public static BooleanState transientState(boolean recursive, String name) {
        return new BooleanState(true, recursive, name);
    }

    public static BooleanState state(boolean recursive, String name) {
        return new BooleanState(false, recursive, name);
    }

    public BooleanState(boolean aTransient, boolean recursive, String name) {
        super(aTransient, recursive, Boolean.class, name);
    }

    @Override
    public final Boolean getValue(ToolkitTreeNodeBean bean) {
        return Boolean.TRUE.equals(bean.getState(name()));
    }
}
