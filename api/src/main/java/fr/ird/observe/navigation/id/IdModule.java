package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.function.Supplier;

/**
 * Created on 12/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.0
 */
public abstract class IdModule extends IdModel {

    protected transient final SingletonSupplier<? extends BusinessProject> project;
    protected transient final SingletonSupplier<? extends BusinessModule> module;

    protected IdModule(IdNode<?> root) {
        super(root);
        this.project = SingletonSupplier.of(getProjectSupplier());
        this.module = SingletonSupplier.of(() -> getProject().getBusinessModule(getClass().getPackage().getName()));
    }

    protected abstract Supplier<? extends BusinessProject> getProjectSupplier();

    @Override
    public final String getModel() {
        return getModule().getName();
    }

    public BusinessProject getProject() {
        return project.get();
    }

    public BusinessModule getModule() {
        return module.get();
    }

}
