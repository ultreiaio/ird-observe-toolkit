package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBeanState;

/**
 * Data bean associated within a node in the navigation tree model.
 * <p>
 * Created on 02/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class NavigationTreeNodeBean extends ToolkitTreeNodeBean {
    public static final ToolkitTreeNodeBeanState<NavigationTreeNodeUpdater> STATE_UPDATER = ToolkitTreeNodeBeanState.transientState(false, NavigationTreeNodeUpdater.class, "$$updater");

    public void buildChildren(NavigationTreeNode node) {
        NavigationTreeNodeUpdater updater = STATE_UPDATER.getValue(this);
        if (updater != null) {
            updater.loadChildren(node);
        }
    }
}
