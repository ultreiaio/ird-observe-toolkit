package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.JXTreeWithNoSearch;

import javax.swing.text.Position;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class SelectionTreeSupport<R extends SelectionTreeNode, M extends SelectionTreeModelSupport<R>> extends JXTreeWithNoSearch {

    public SelectionTreeSupport(M model) {
        super(model);
        setLargeModel(true);
        setCellRenderer(new SelectionTreeCellRenderer());
        setRootVisible(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        setToggleClickCount(100);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final M getModel() {
        return (M) super.getModel();
    }

    @Override
    public TreePath getNextMatch(String prefix, int startingRow, Position.Bias bias) {
//        return super.getNextMatch(prefix, startingRow, bias);
        return null;
    }
}
