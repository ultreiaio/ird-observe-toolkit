package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;
import fr.ird.observe.navigation.tree.selection.bean.RootOpenFilterSelectionTreeNodeBean;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class ToolkitTreeModelSupport<R extends ToolkitTreeNode> extends DefaultTreeModel {
    private static final Logger log = LogManager.getLogger(ToolkitTreeModelSupport.class);

    protected final TreeConfig config;
    protected final Supplier<R> rootSupplier;
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final GroupByHelper groupByHelper;
    protected List<String> editIds;
    private ToolkitTreeFlatModelRootRequest request;
    private long dataCount;

    public ToolkitTreeModelSupport(BusinessProject businessProject, TreeConfig config, Supplier<R> rootSupplier) {
        super(rootSupplier.get());
        this.groupByHelper = new GroupByHelper(businessProject);
        this.config = config;
        this.rootSupplier = rootSupplier;
    }

    public TreeConfig getConfig() {
        return config;
    }

    public GroupByHelper getGroupByHelper() {
        return groupByHelper;
    }

    protected ToolkitTreeFlatModelRootRequest toRequest() {
        return getConfig().toRootRequest(groupByHelper);
    }

    @SuppressWarnings("unchecked")
    @Override
    public final R getRoot() {
        return (R) super.getRoot();
    }

    public final void setEditIds(List<String> editIds) {
        this.editIds = editIds;
    }

    public final ToolkitTreeFlatModel buildFlatModel(Function<ToolkitTreeFlatModelRootRequest, ToolkitTreeFlatModel> service) {
        request = toRequest();
        ToolkitTreeFlatModel flatModel = service.apply(request);
        dataCount = flatModel.getDataCount();
        augmentsModel(flatModel);
        return flatModel;
    }

    public final void populate(ToolkitTreeFlatModel flatModel, Consumer<R> rootConsumer) {
        R rootNode = new ToolkitTreeNodeBuilder().load(rootSupplier.get(), flatModel.getMapping());
        augmentsRoot(rootNode);
        if (rootConsumer != null) {
            rootConsumer.accept(rootNode);
        }
        setRoot(rootNode);
    }

    public final void populate(Function<ToolkitTreeFlatModelRootRequest, ToolkitTreeFlatModel> service) {
        populate(service, null);
    }

    public final void populate(Function<ToolkitTreeFlatModelRootRequest, ToolkitTreeFlatModel> service, Consumer<R> rootConsumer) {
        ToolkitTreeFlatModel flatModel = buildFlatModel(service);
        populate(flatModel, rootConsumer);
    }

    public final void updateDataCount(ToolkitTreeFlatModel flatModel) {
        dataCount = flatModel.getDataCount();
    }

    public final ToolkitTreeFlatModelRootRequest getRequest() {
        return request;
    }

    public long getDataCount() {
        return dataCount;
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public final void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(name, listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(name, listener);
    }

    @Override
    public void setRoot(TreeNode root) {
        log.info(String.format("setRoot init: %s", root));
        R oldRoot = getRoot();
        if (Objects.equals(root, oldRoot)) {
            throw new IllegalStateException("Can't set the same root node twice");
        }
        if (oldRoot != null && oldRoot.isLoaded()) {
            // clean old root
            log.info(String.format("Clean old root node: %s", oldRoot));
            //FIXME???
//            oldRoot.clear();
        }
        log.info(String.format("Opening new root node: %s", root));
        @SuppressWarnings("unchecked") R node = (R) root;
        //FIXME???
//        node.open();

        log.info(String.format("Root node is open: %s", root));
        super.setRoot(root);
        // now can use tree model inside node
        ToolkitTreeNodeBean.STATE_MODEL.setValue(node.getUserObject(), this);
        log.info(String.format("setRoot done: %s", root));
    }

    protected void augmentsModel(ToolkitTreeFlatModel flatModel) {
        if (editIds != null) {
            // augments open state
            flatModel.addState(ToolkitTreeNodeBean.STATE_EDIT_ID, path -> editIds.stream().anyMatch(path::endsWith));
        }
    }

    protected void augmentsRoot(R rootNode) {
        // by default do nothing
        if (editIds != null) {
            Enumeration<TreeNode> children = rootNode.children();
            while (children.hasMoreElements()) {
                SelectionTreeNode treeNode = (SelectionTreeNode) children.nextElement();
                if (treeNode.getUserObject() instanceof RootOpenFilterSelectionTreeNodeBean) {
                    Enumeration<TreeNode> children1 = treeNode.children();
                    while (children1.hasMoreElements()) {
                        SelectionTreeNode node = (SelectionTreeNode) children1.nextElement();
                        if (node.getUserObject().isEditId()) {
                            ((RootOpenFilterSelectionTreeNodeBean) treeNode.getUserObject()).setEditId();
                            break;
                        }
                    }
                }
            }
        }
    }
}
