package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.spi.module.BusinessModule;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * GroupBy helper for only one given module.
 * <p>
 * Created on 04/09/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class ModuleGroupByHelper {

    /**
     * Business module used.
     */
    private final BusinessModule module;
    /**
     * List of group by definitions for the given module.
     */
    public final List<DataGroupByDtoDefinition<?, ?>> groupByDefinitions;

    public ModuleGroupByHelper(BusinessModule module) {
        this.module = module;
        this.groupByDefinitions = new LinkedList<>(module.getDataGroupByDtoDefinitions());
    }

    public BusinessModule getModule() {
        return module;
    }

    public Class<? extends RootOpenableDto> getDtoType() {
        Set<Class<? extends RootOpenableDto>> rootOpenableDataTypes = getModule().getRootOpenableDataTypes();
        return rootOpenableDataTypes.iterator().next();
    }

    public List<DataGroupByDtoDefinition<?, ?>> getGroupByDefinitions() {
        return groupByDefinitions;
    }

    public DataGroupByDtoDefinition<?, ?> getDefinition(String groupByName) {
        for (DataGroupByDtoDefinition<?, ?> definition : getGroupByDefinitions()) {
            if (groupByName.equals(definition.getName())) {
                return definition;
            }
        }
        throw new IllegalStateException(String.format("Can't find definition for groupByName:%s", groupByName));
    }

    public void sort(List<DataGroupByDtoDefinition<?, ?>> incomingDefinitions) {
        List<DataGroupByDtoDefinition<?, ?>> result = new LinkedList<>(getGroupByDefinitions());
        result.retainAll(incomingDefinitions);
        incomingDefinitions.clear();
        incomingDefinitions.addAll(result);
    }
}
