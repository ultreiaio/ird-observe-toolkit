package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 27/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class IdProjectManager {

    private static final Logger log = LogManager.getLogger(IdProjectManager.class);
    private final Map<Class<?>, List<OpenNodeCallback>> openCallbacks;
    private final Map<Class<?>, List<CloseNodeCallback>> closeCallbacks;
    private final IdProject editModel;

    public IdProjectManager(IdProject editModel) {
        this.editModel = editModel;
        this.openCallbacks = new LinkedHashMap<>();
        this.closeCallbacks = new LinkedHashMap<>();
        for (OpenNodeCallback callback : ServiceLoader.load(OpenNodeCallback.class)) {
            Class<? extends IdNode<?>> nodeType = callback.getNodeType();
            log.info(String.format("Detect open edit node (%s) callback: %s", nodeType.getName(), callback));
            editModel.forNodeType(nodeType).orElseThrow(() -> new IllegalStateException(String.format("Could not find edit node of type: %s", nodeType.getName())));
            List<OpenNodeCallback> openNodeCallbacks = openCallbacks.computeIfAbsent(nodeType, k -> new LinkedList<>());
            Set<Integer> priorities = openNodeCallbacks.stream().map(OpenNodeCallback::getPriority).collect(Collectors.toSet());
            if (priorities.contains(callback.getPriority())) {
                throw new IllegalStateException(String.format("Open edit node callback %s use a already reserved priority: %d", callback, callback.getPriority()));
            }
            openNodeCallbacks.add(callback);
        }
        openCallbacks.values().forEach(t -> t.sort(Comparator.comparing(OpenNodeCallback::getPriority)));
        for (CloseNodeCallback callback : ServiceLoader.load(CloseNodeCallback.class)) {
            Class<? extends IdNode<?>> nodeType = callback.getNodeType();
            log.info(String.format("Detect close edit node (%s) callback: %s", nodeType.getName(), callback));
            editModel.forNodeType(nodeType).orElseThrow(() -> new IllegalStateException(String.format("Could not find edit node of type: %s", nodeType.getName())));
            List<CloseNodeCallback> closeNodeCallbacks = closeCallbacks.computeIfAbsent(nodeType, k -> new LinkedList<>());
            Set<Integer> priorities = closeNodeCallbacks.stream().map(CloseNodeCallback::getPriority).collect(Collectors.toSet());
            if (priorities.contains(callback.getPriority())) {
                throw new IllegalStateException(String.format("Close edit node callback %s use a already reserved priority: %d", callback, callback.getPriority()));
            }
            closeNodeCallbacks.add(callback);
        }
        closeCallbacks.values().forEach(t -> t.sort(Comparator.comparing(CloseNodeCallback::getPriority)));
    }

    public OpenNodeRequest createOpenIdNodeRequest(IdProject model, IdNode<?> nodeToClose, IdNode<?> selectedNode, String id) {
        IdNode<?> nodeToOpen = model.newInstance().copyNode(selectedNode, nodeToClose.getClass(), id);
        OpenNodeRequestConfiguration configuration = new OpenNodeRequestConfiguration(nodeToClose, nodeToOpen);
        return createOpenNodeRequest(configuration);
    }

    public CloseNodeRequest createCloseIdNodeRequest(IdNode<?> nodeToClose) {
        CloseNodeRequestConfiguration configuration = new CloseNodeRequestConfiguration(nodeToClose);
        return createCloseNodeRequest(configuration);
    }

    protected OpenNodeRequest createOpenNodeRequest(OpenNodeRequestConfiguration request) {
        IdNode<?> nodeToOpen = request.getNodeToOpen();
        IdNode<?> nodeToClose = request.getNodeToClose();
        // Get all ids to open
        List<String> idsToOpen = nodeToOpen.getNodesToRoot().stream().map(IdNode::getId).collect(Collectors.toList());
        // Get all ids already open
        List<String> idsAlreadyOpen = nodeToClose.getNodesToRoot().stream().map(IdNode::getId).collect(Collectors.toList());
        IdNode<?> realNodeToClose = null;
        // Get first node to close
        while (nodeToClose != null) {
            if (nodeToClose.isDisabled()) {
                // node not open
                // try on parent
                nodeToClose = nodeToClose.getParent();
                continue;
            }
            if (!idsToOpen.contains(nodeToClose.getId())) {
                // this is a node to close
                // won't be available
                idsAlreadyOpen.remove(nodeToClose.getId());
                realNodeToClose = nodeToClose;
                // try on parent
                nodeToClose = nodeToClose.getParent();
            } else {
                // this is not a node to close
                break;
            }
        }

        List<IdNode<?>> nodesToClose = realNodeToClose == null ? Collections.emptyList() : realNodeToClose.getShellForClose().stream().filter(n -> !idsAlreadyOpen.contains(n.getId())).collect(Collectors.toList());
        IdNode<?> sharedAncestorNode = null;
        if (realNodeToClose != null) {
            // need to find shared ancestor to keep open
            // get shared ancestor
            sharedAncestorNode = nodeToOpen.upToSharedAncestor(realNodeToClose);
        }
        if (sharedAncestorNode == null) {
            sharedAncestorNode = nodeToOpen.getRoot();
        }
        List<IdNode<?>> nodesToOpen = nodeToOpen.getNodesFromAncestor(sharedAncestorNode).stream().filter(n -> !idsAlreadyOpen.contains(n.getId())).collect(Collectors.toList());
        return new OpenNodeRequest(request, List.copyOf(nodesToClose), nodesToOpen);
    }

    public void applyOpenIdNodeRequest(OpenNodeRequest request) throws CloseNodeVetoException {
        List<IdNode<?>> nodesToClose = request.getNodesToClose();
        for (IdNode<?> node : nodesToClose) {
            closeNode(node, true);
        }
        List<IdNode<?>> nodesToOpen = request.getNodesToOpen();
        for (IdNode<?> node : nodesToOpen) {
            openNode(node, true);
        }
    }

    public void applyCloseIdNodeRequest(CloseNodeRequest request) throws CloseNodeVetoException {
        List<IdNode<?>> nodesToClose = request.getNodesToClose();
        for (IdNode<?> node : nodesToClose) {
            closeNode(node, true);
        }
    }

    protected CloseNodeRequest createCloseNodeRequest(CloseNodeRequestConfiguration request) {
        IdNode<?> nodeToClose = request.getNodeToClose();
        List<IdNode<?>> nodesToClose = nodeToClose.getShellForClose();
        return new CloseNodeRequest(request, nodesToClose);
    }

    protected void closeNode(IdNode<?> node) throws CloseNodeVetoException {
        closeNode(node, false);
    }

    protected void openNode(IdNode<?> node) {
        openNode(node, false);
    }

    private void closeNode(IdNode<?> node, boolean adjusting) throws CloseNodeVetoException {
        log.debug("Will close edit node from incoming: " + node);
        IdNode<?> realIdNode = editModel.forNodeType(node.getClass()).orElseThrow(IllegalStateException::new);
        log.info("Close edit node: " + node);
        List<CloseNodeCallback> callbacks = closeCallbacks.get(node.getClass());
        if (callbacks != null) {
            for (CloseNodeCallback c : callbacks) {
                log.info("Apply callback on closed edit node: " + realIdNode + " - " + c);
                c.onIdNodeClosed(node, adjusting);
            }
        }
        realIdNode.setId(null);
    }

    private void openNode(IdNode<?> node, boolean adjusting) {
        log.debug("Will open edit node from incoming: " + node);
        IdNode<?> realIdNode = editModel.forNodeType(node.getClass()).orElseThrow(IllegalStateException::new);
        realIdNode.setId(node.getId());
        log.info(String.format("Open edit node: %s", realIdNode));
        List<OpenNodeCallback> callbacks = openCallbacks.get(node.getClass());
        if (callbacks != null) {
            for (OpenNodeCallback c : callbacks) {
                log.info("Apply callback on opened edit node: " + realIdNode + " - " + c);
                c.onIdNodeOpened(node, adjusting);
            }
        }
    }
}
