package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * Represents the state of a data on both side : based on the id and last update date of a data.
 * <p>
 * Created on 27/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_ACTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_ACTION_PATTERN)
})
public enum IdState {
    /**
     * Id exists on both sides and lastUpdateDate is also equals.
     */
    EQUALS("data-state.equals"),
    /**
     * Id exists on both sides LastUpdate is older on this side.
     */
    OBSOLETE("data-state.obsolete"),
    /**
     * Id exists on both sides and LastUpdate is newer on this side.
     */
    UPDATED("data-state.updated"),
    /**
     * Id exists only on this side.
     */
    ADDED("data-state.added");

    private final String iconPath;

    IdState(String iconPath) {
        this.iconPath = iconPath;
    }

    public Icon getIcon() {
        return UIManager.getIcon(iconPath);
    }

    public Icon getSmallIcon() {
        return UIManager.getIcon(iconPath + "-small");
    }

    public String getLabel() {
        return IdStateI18n.getLabel(this);
    }

    public String getDescription() {
        return IdStateI18n.getDescription(this);
    }

    public String getAction() {
        return IdStateI18n.getAction(this);
    }

    public interface WithIdState {

        IdState getIdState();

        default boolean isAdded() {
            return getIdState() == ADDED;
        }

        default boolean isUpdated() {
            return getIdState() == UPDATED;
        }

        default boolean isObsolete() {
            return getIdState() == OBSOLETE;
        }
        default boolean isEquals() {
            return getIdState() == EQUALS;
        }

    }
}
