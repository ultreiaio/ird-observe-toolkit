package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.selection.IdState;
import fr.ird.observe.navigation.tree.states.BooleanState;
import fr.ird.observe.navigation.tree.states.ClassState;
import fr.ird.observe.navigation.tree.states.DateState;
import fr.ird.observe.navigation.tree.states.IntegerState;
import fr.ird.observe.navigation.tree.states.StringState;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class ToolkitTreeNodeBean implements ObserveDto, NavigationNodeType.WithNavigationNodeType {

    public static final StringState STATE_ICON_PATH = StringState.transientState(false, "iconPath");
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static final ToolkitTreeNodeBeanState<Map<String, Class<?>>> STATE_NODE_MAPPING = ToolkitTreeNodeBeanState.transientState(false, (Class) Map.class, "nodeMapping");
    public static final ToolkitTreeNodeBeanState<NavigationNodeType> STATE_TYPE = ToolkitTreeNodeBeanState.transientState(false, NavigationNodeType.class, "type");
    public static final ToolkitTreeNodeBeanState<String> STATE_CONTENT = StringState.state(false, "content");
    public static final ToolkitTreeNodeBeanState<String> STATE_REFERENCES = StringState.state(false, "references");
    public static final BooleanState STATE_MULTIPLICITY = BooleanState.transientState(false, "multiplicity");
    public static final ClassState<? extends ReferentialDto> STATE_REFERENTIAL_TYPE = ClassState.transientState(false, "referentialType");
    public static final ClassState<? extends DataDto> STATE_DATA_TYPE = ClassState.transientState(false, "dataType");
    public static final ClassState<? extends DataGroupByDto<?>> STATE_FILTER_TYPE = ClassState.transientState(false, "filterType");
    public static final StringState STATE_FILTER_NAME = StringState.transientState(false, "filterName");
    public static final StringState STATE_FILTER_FLAVOR = StringState.state(true, "filterFlavor");

    public static final BooleanState STATE_EDIT_ID = BooleanState.transientState(false, "editId");
    /**
     * Is the node a leaf?
     */
    public static final BooleanState STATE_CAPABILITY_LEAF = BooleanState.transientState(false, "capability.leaf");
    /**
     * Is the node a reference?
     */
    public static final BooleanState STATE_CAPABILITY_SELECT_REFERENCE = BooleanState.transientState(false, "capability.select.reference");
    /**
     * Is the node a edit reference?
     */
    public static final BooleanState STATE_CAPABILITY_EDIT_REFERENCE = BooleanState.transientState(false, "capability.edit.reference");
    /**
     * Is the node contains other nodes?
     */
    public static final BooleanState STATE_CAPABILITY_CONTAINER = BooleanState.transientState(false, "capability.container");
    /**
     * Is the node a filter?
     */
    public static final BooleanState STATE_CAPABILITY_FILTER = BooleanState.transientState(false, "capability.filter");
    /**
     * Is the node contains other edit reference nodes?
     */
    public static final BooleanState STATE_CAPABILITY_EDIT_REFERENCE_CONTAINER = BooleanState.transientState(false, "capability.edit.reference.container");
    /**
     * Is the node contains other select reference nodes?
     */
    public static final BooleanState STATE_CAPABILITY_SELECT_REFERENCE_CONTAINER = BooleanState.transientState(false, "capability.select.reference.container");

    public static final StringState STATE_ID = StringState.state(true, "id");
    public static final StringState STATE_PATH = StringState.state(false, "path");
    public static final StringState STATE_STANDALONE_PATH = StringState.transientState(false, "standalonePath");
    public static final StringState STATE_TEXT = StringState.state(false, "text");
    public static final BooleanState STATE_ENABLED = BooleanState.state(true, "enable");
    public static final BooleanState STATE_EDITABLE = BooleanState.state(true, "editable");
    public static final DateState STATE_LAST_UPDATE_DATE = DateState.state(false, "lastUpdateDate");
    public static final IntegerState STATE_COUNT = IntegerState.state(false, "count");
    @SuppressWarnings("rawtypes")
    public static final ToolkitTreeNodeBeanState<ToolkitTreeModelSupport> STATE_MODEL = ToolkitTreeNodeBeanState.transientState(false, ToolkitTreeModelSupport.class, "$$updater");
    public static final ToolkitTreeNodeBeanState<IdState> ID_STATE = ToolkitTreeNodeBeanState.transientState(false, IdState.class, "idState");

    protected transient final SingletonSupplier<? extends BusinessProject> project;
    protected transient final SingletonSupplier<? extends BusinessModule> module;
    protected transient final SingletonSupplier<? extends BusinessSubModule> subModule;
    protected transient final Set<String> transientStates;
    private final Map<String, Object> states;

    public ToolkitTreeNodeBean() {
        this.project = SingletonSupplier.of(getProjectSupplier());
        this.module = SingletonSupplier.of(() -> project().getBusinessModule(getClass().getPackage().getName()));
        this.subModule = SingletonSupplier.of(() -> project().getBusinessSubModule(module(), getClass().getPackage().getName()));
        this.states = new LinkedHashMap<>();
        Map<ToolkitTreeNodeBeanState<?>, Object> defaultStates = defaultStates();
        this.transientStates = new TreeSet<>();
        for (Map.Entry<ToolkitTreeNodeBeanState<?>, Object> entry : defaultStates.entrySet()) {
            @SuppressWarnings("unchecked") ToolkitTreeNodeBeanState<Object> key = (ToolkitTreeNodeBeanState<Object>) entry.getKey();
            key.setValue(this, entry.getValue());
        }
    }

    protected abstract Map<ToolkitTreeNodeBeanState<?>, Object> defaultStates();

    protected abstract Supplier<? extends BusinessProject> getProjectSupplier();

    /**
     * @return text associated with the node.
     */
    public String getText() {
        return STATE_TEXT.getValue(this);
    }

    /**
     * @return id associated with the node.
     */
    public final String getId() {
        return STATE_ID.getValue(this);
    }

    /**
     * @return {@code true} if the node is an edit id.
     */
    public final boolean isEditId() {
        return STATE_EDIT_ID.getValue(this);
    }

    /**
     * @return {@code true} if the node is a leaf.
     */
    public final boolean isLeaf() {
        return STATE_CAPABILITY_LEAF.getValue(this);
    }

    /**
     * @return {@code true} if the node is a container.
     */
    public final boolean isContainer() {
        return STATE_CAPABILITY_CONTAINER.getValue(this);
    }

    /**
     * @return {@code true} if the node is a filter.
     */
    public final boolean isFilter() {
        return STATE_CAPABILITY_FILTER.getValue(this);
    }

    /**
     * @return {@code true} if the node is a reference.
     */
    public final boolean isReference() {
        return isEditReference() || isSelectReference();
    }

    /**
     * @return {@code true} if the node is a edit reference.
     */
    public final boolean isEditReference() {
        return STATE_CAPABILITY_EDIT_REFERENCE.getValue(this);
    }

    /**
     * @return {@code true} if the node is a select reference.
     */
    public final boolean isSelectReference() {
        return STATE_CAPABILITY_SELECT_REFERENCE.getValue(this);
    }

    /**
     * @return {@code true} if the node is a reference container.
     */
    public final boolean isReferenceContainer() {
        return isEditReferenceContainer() || isSelectReferenceContainer();
    }

    /**
     * @return {@code true} if the node is a edit reference container.
     */
    public final boolean isEditReferenceContainer() {
        return STATE_CAPABILITY_EDIT_REFERENCE_CONTAINER.getValue(this);
    }

    /**
     * @return {@code true} if the node is a select reference container.
     */
    public final boolean isSelectReferenceContainer() {
        return STATE_CAPABILITY_SELECT_REFERENCE_CONTAINER.getValue(this);
    }

    /**
     * @return path to icon associated with the node.
     */
    public final String getIconPath() {
        return STATE_ICON_PATH.getValue(this);
    }

    /**
     * @return multiplicity of this node (means can we have more than one for the same parent?)
     */
    @SuppressWarnings("unused")
    public final boolean getMultiplicity() {
        return STATE_MULTIPLICITY.getValue(this);
    }

    @Override
    public final NavigationNodeType getType() {
        return STATE_TYPE.getValue(this);
    }

    /**
     * @return path associated with the node.
     */
    public final String getPath() {
        return getPath(false);
    }

    public String getStatePath() {
        return STATE_PATH.getValue(this);
    }

    public String getPath(boolean fromRootNode) {
        String path = getStatePath();
        String id = getId();
        if (id == null) {
            return path;
        }
        if (path == null) {
            return id;
        }
        return path + ":" + id;
    }

    public final boolean isEnabled() {
        return Boolean.TRUE.equals(STATE_ENABLED.getValue(this));
    }

    public final boolean isEditable() {
        return Boolean.TRUE.equals(STATE_EDITABLE.getValue(this));
    }

    public final BusinessProject project() {
        return project.get();
    }

    public final BusinessModule module() {
        return module.get();
    }

    public final BusinessSubModule subModule() {
        return subModule.get();
    }

    @SuppressWarnings("unchecked")
    public final <O> O getState(String stateName) {
        return (O) getStates().get(Objects.requireNonNull(stateName));
    }

    public final void setId(String id) {
        STATE_ID.setValue(this, id);
    }

    public final void setLastUpdateDate(Date lastUpdateDate) {
        STATE_LAST_UPDATE_DATE.setValue(this, lastUpdateDate);
    }

    public final void copy(ToolkitId id) {
        setId(id.getTopiaId());
        setLastUpdateDate(id.getLastUpdateDate());
    }

    public final ToolkitTreeNodeStates getUserStates() {
        return ToolkitTreeNodeStates.of(getStates().entrySet().stream()
                                                .filter(e -> !transientStates.contains(e.getKey()) && !e.getKey().startsWith("$$")));
    }

    public final void addState(String stateName, Object value) {
        getStates().put(Objects.requireNonNull(stateName), value);
    }

    public final void addTransientState(String stateName, Object value) {
        getStates().put(Objects.requireNonNull(stateName), value);
        transientStates.add(stateName);
    }

    public final Map<String, Class<?>> getNodeMapping() {
        return STATE_NODE_MAPPING.getValue(this);
    }

    public void removeState(String stateName) {
        getStates().remove(stateName);
    }

    public Map<String, Object> getStates() {
        return states;
    }

    @SuppressWarnings("unchecked")
    public <N extends ToolkitTreeNode> Class<N> getNodeMapping(String name) {
        return (Class<N>) getNodeMapping().get(name);
    }

    protected final void makeTextTransient() {
        STATE_TEXT.makeUnTransient();
        transientStates.add(STATE_TEXT.name());
    }

    public void reload() {
        //FIXME
    }

    public void updateSelectNodeId() {
        if (isReference()) {
            //FIXME push this id to associated select node
        }
    }
}

