package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.TreeConfig;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
@GenerateJavaBeanDefinition
public class SelectionTreeConfig extends TreeConfig {
    public static final String USE_OPEN_DATA = "useOpenData";

    private boolean useOpenData;

    public boolean isUseOpenData() {
        return useOpenData;
    }

    public void setUseOpenData(boolean useOpenData) {
        boolean oldValue = this.useOpenData;
        this.useOpenData = useOpenData;
        firePropertyChange(USE_OPEN_DATA, oldValue, useOpenData);
    }

//    @Override
//    public ToolkitTreeFlatModelRootRequest toRootRequest() {
//        ToolkitTreeFlatModelRootRequest request = super.toRootRequest();
//        //FIXME Not sure of it, could use this in data synchro (admin actions)
//        // selection model never accept empty group by
//        request.setLoadEmptyGroupBy(false);
//        return request;
//    }

}
