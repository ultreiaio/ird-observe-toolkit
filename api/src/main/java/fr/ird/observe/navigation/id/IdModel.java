package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import io.ultreia.java4all.bean.AbstractJavaBean;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 26/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class IdModel extends AbstractJavaBean {

    public static final String PROPERTY_ENABLED = "enabled";

    private final IdNode<?> root;
    private final List<IdNode<?>> nodes;

    public IdModel(IdNode<?> root) {
        this.root = Objects.requireNonNull(root);
        List<IdNode<?>> builder = new LinkedList<>();
        collectNodes(root, builder);
        this.nodes = builder;
        root.addPropertyChangeListener(IdNode.PROPERTY_ENABLED, e -> {
            boolean oldValue = (boolean) e.getOldValue();
            boolean newValue = (boolean) e.getNewValue();
            firePropertyChange(PROPERTY_ENABLED, oldValue, newValue);
        });
    }

    public abstract String getModel();

    public boolean accept(BusinessDto dto) {
        return nodes.stream().anyMatch(n -> n.accept(dto));
    }

    @SuppressWarnings("unchecked")
    public <D extends BusinessDto> IdNode<D> getDtoNode(Class<D> type) {
        return (IdNode<D>) nodes.stream().filter(n -> type.equals(n.getType())).findFirst().orElse(null);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <D extends BusinessDto> Stream<IdNode<D>> getDtoNodes(Class<D> type) {
        return (Stream) nodes.stream().filter(n -> type.equals(n.getType()));
    }

    @SuppressWarnings("unchecked")
    public <D extends BusinessDto> IdNode<D> getNavigationNode(IdNode<?> selectNode) {
        IdNode<?> editNode = null;
        while (selectNode != null && editNode == null) {
            Class<? extends BusinessDto> type = selectNode.getType();
            Stream<? extends IdNode<? extends BusinessDto>> candidates = getDtoNodes(type);
            Optional<IdNode<?>> sourceParent = Optional.ofNullable(selectNode.getParent());
            IdNode<? extends BusinessDto> node;
            if (sourceParent.isEmpty()) {
                // root node
                IdNode<?> finalSelectNode = selectNode;
                node = candidates.filter(n -> n.getParent() == null && Objects.equals(n.getType(), finalSelectNode.getType())).findFirst().orElse(null);
            } else {
                node = candidates.filter(n -> sourceParent.map(parent -> n.getParent() != null && Objects.equals(parent.getType(), n.getParent().getType())).orElseGet(() -> n.getType() == null)).findFirst().orElse(null);
            }
            if (node == null) {
                selectNode = selectNode.getParent();
            } else {
                editNode = node;
            }
        }
        return (IdNode<D>) editNode;
    }

    @SuppressWarnings("unchecked")
    public <D extends IdNode<?>> D getNode(Class<D> type) {
        return (D) nodes.stream().filter(n -> type.equals(n.getClass())).findFirst().orElse(null);
    }

    @SuppressWarnings("unchecked")
    public <D extends IdNode<?>> D getNode(int index) {
        return (D) nodes.get(index);
    }

    public IdNode<?> getRoot() {
        return root;
    }

    public List<IdNode<?>> getNodes() {
        return nodes;
    }

    public List<IdNode<?>> getEditableNodes() {
        return nodes.stream().filter(IdNode::isEditable).collect(Collectors.toList());
    }

    public List<IdNode<?>> getNodesWithIds() {
        List<IdNode<?>> builder = new LinkedList<>();
        root.getIds(builder);
        return builder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdModel that = (IdModel) o;
        return Objects.equals(nodes, that.nodes) &&
                Objects.equals(root, that.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodes, root);
    }

    private void collectNodes(IdNode<?> root, List<IdNode<?>> builder) {
        builder.add(root);
        for (IdNode<?> child : root.getChildren()) {
            collectNodes(child, builder);
        }
    }

    public boolean isEnabled() {
        return getRoot().isEnabled();
    }

    public boolean isDisabled() {
        return !isEnabled();
    }

    public boolean rejectModel(String model) {
        return !acceptModel(model);
    }

    public boolean acceptModel(String model) {
        return Objects.equals(getModel(), model);
    }

    public void load(IdModel model) {
        for (IdNode<?> n : model.getNodesWithIds()) {
            getNode(n.getClass()).setId(n.getId());
        }
    }

    public int count() {
        return (int) getNodes().stream().filter(IdNode::isEnabled).count();
    }

    public void clearModel() {
        getNodes().forEach(n -> n.setId(null));
    }
}
