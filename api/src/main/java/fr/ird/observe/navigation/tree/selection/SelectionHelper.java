package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Enumeration;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class SelectionHelper {

    public static void setSelected(SelectionTreeNode node, boolean selected) {
        SelectionState selectionState = selected ? SelectionState.FULL : SelectionState.EMPTY;
        setSelectionState(node, selectionState);
        promoteParents(node);
    }

    private static void setSelectionState(SelectionTreeNode node, SelectionState selectionState) {
        node.getUserObject().setSelectionState(selectionState);
        // always propagate to all children
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            SelectionTreeNode treeNode = (SelectionTreeNode) children.nextElement();
            setSelectionState(treeNode, selectionState);
        }
    }

    private static void promoteParents(SelectionTreeNode node) {
        // update parents to root
        SelectionTreeNode parent = node.getParent();
        while (parent != null) {
            int selectionCount = getSelectionCount(parent);
            int childCount = parent.getChildCount();
            if (selectionCount == 0) {
                parent.getUserObject().setSelectionState(SelectionState.EMPTY);
            } else if (selectionCount == childCount) {
                parent.getUserObject().setSelectionState(SelectionState.FULL);
            } else {
                parent.getUserObject().setSelectionState(SelectionState.PARTIAL);
            }
            parent = parent.getParent();
        }
    }

    private static int getSelectionCount(SelectionTreeNode node) {
        int result = 0;
        Enumeration<?> children = node.children();
        while (children.hasMoreElements()) {
            SelectionTreeNode treeNode = (SelectionTreeNode) children.nextElement();
            if (treeNode.isSelected()) {
                result++;
            }
        }
        return result;
    }
}
