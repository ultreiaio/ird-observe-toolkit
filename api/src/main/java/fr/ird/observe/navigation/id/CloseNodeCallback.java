package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * Created by tchemit on 27/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface CloseNodeCallback {

    /**
     * @return type of node
     */
    Class<? extends IdNode<?>> getNodeType();

    /**
     * @return priority of this callback (first are 0, then 1, ...)
     */
    int getPriority();

    void onIdNodeClosed(IdNode<?> node, boolean adjusting) throws CloseNodeVetoException;
}
