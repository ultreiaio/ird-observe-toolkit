package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Helper method around {@link IdNode}.
 * Created on 04/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class IdNodeHelper {

    public static List<IdNode<?>> getNodesFromAncestor(IdNode<?> first, IdNode<?> other) {
        List<IdNode<?>> result = new ArrayList<>(first.getLevel());
        getAncestorNodes(first, other, result);
        Collections.reverse(result);
        return result;
    }

    public static List<IdNode<?>> getNodesToAncestor(IdNode<?> first, IdNode<?> other) {
        List<IdNode<?>> result = new ArrayList<>(first.getLevel());
        getAncestorNodes(first, other, result);
        return result;
    }

    public static List<IdNode<?>> getNodesFromRoot(IdNode<?> first) {
        return getNodesFromAncestor(first, first.getRoot());
    }

    public static List<IdNode<?>> getNodesToRoot(IdNode<?> first) {
        return getNodesToAncestor(first, first.getRoot());
    }

    public static IdNode<?> upToSharedAncestor(IdNode<?> first, IdNode<?> otherNode) {
        if (!first.acceptModel(otherNode.getModel())) {
            return null;
        }
        if (first.isRoot()) {
            return null;
        }
        Map<Class<?>, IdNode<?>> otherNodesToRootMapping = Maps.uniqueIndex(otherNode.getNodesToRoot(), IdNode::getType);
        for (IdNode<?> thisNode : first.getParent().getNodesToRoot()) {
            IdNode<?> otherOptionalNode = otherNodesToRootMapping.get(thisNode.getType());
            if (otherOptionalNode != null) {
                return thisNode;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <N extends IdNode<?>> void getAncestorNodes(IdNode<?> first, IdNode<?> other, List<N> builder) {
        builder.add((N) first);
        if (!first.equals(other)) {
            getAncestorNodes(first.getParent(), other, builder);
        }
    }

    public static <N extends IdNode<?>> List<N> getShellForClose(N first) {
        List<N> builder = new LinkedList<>();
        getShellForClose0(first, builder);
        return builder;
    }

    @SuppressWarnings("unchecked")
    public static <N extends IdNode<?>> void getShellForClose0(N first, List<N> builder) {
        if (first.isDisabled()) {
            // do not add closed nodes
            return;
        }
        for (IdNode<?> child : first.getChildren()) {
            getShellForClose0((N) child, builder);
        }
        builder.add(first);
    }

    private IdNodeHelper() {
        // no instance
    }
}
