package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.TreeConfig;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.awt.Color;

/**
 * Created by tchemit on 01/04/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition
public class NavigationTreeConfig extends TreeConfig {

    public static final String NODE_UNLOADED_COLOR = "nodeUnloadedColor";
    public static final String NODE_UNSAVED_COLOR = "nodeUnsavedColor";
    public static final String NODE_EMPTY_COLOR = "nodeEmptyColor";

    private Color nodeUnloadedColor;
    private Color nodeUnsavedColor;
    private Color nodeEmptyColor;

    public Color getNodeUnloadedColor() {
        return nodeUnloadedColor;
    }

    public void setNodeUnloadedColor(Color nodeUnloadedColor) {
        Color oldValue = this.nodeUnloadedColor;
        this.nodeUnloadedColor = nodeUnloadedColor;
        firePropertyChange(NODE_UNLOADED_COLOR, oldValue, nodeUnloadedColor);
    }

    public Color getNodeUnsavedColor() {
        return nodeUnsavedColor;
    }

    public void setNodeUnsavedColor(Color nodeUnsavedColor) {
        Color oldValue = this.nodeUnsavedColor;
        this.nodeUnsavedColor = nodeUnsavedColor;
        firePropertyChange(NODE_UNSAVED_COLOR, oldValue, nodeUnsavedColor);
    }

    public Color getNodeEmptyColor() {
        return nodeEmptyColor;
    }

    public void setNodeEmptyColor(Color nodeEmptyColor) {
        Color oldValue = this.nodeEmptyColor;
        this.nodeEmptyColor = nodeEmptyColor;
        firePropertyChange(NODE_EMPTY_COLOR, oldValue, nodeEmptyColor);
    }
}
