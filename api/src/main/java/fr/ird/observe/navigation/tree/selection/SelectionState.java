package fr.ird.observe.navigation.tree.selection;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * Selection states used by a node.
 * <p>
 * Created on 04/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public enum SelectionState {
    /**
     * When selection is empty.
     */
    EMPTY("checkbox.empty"),
    /**
     * When selection is partial.
     */
    PARTIAL("checkbox.partial"),
    /**
     * When selection is full.
     */
    FULL("checkbox.full");

    private final String iconPath;

    SelectionState(String iconPath) {
        this.iconPath = iconPath;
    }

    public Icon getIcon() {
        return UIManager.getIcon(iconPath);
    }

    public interface WithSelectionState {

        SelectionState getSelectionState();

        default boolean isNotSelected() {
            return getSelectionState() == EMPTY;
        }

        default boolean isPartialSelected() {
            return getSelectionState() == PARTIAL;
        }

        default boolean isSelected() {
            return getSelectionState() == FULL;
        }

    }
}
