package fr.ird.observe.navigation.tree.io;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBeanState;
import fr.ird.observe.navigation.tree.states.BooleanState;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Flat representation of a tree model.
 * <p>
 * Created on 07/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeFlatModel implements ObserveDto {

    private final String path;
    private final Date lastUpdateDate;
    private final Map<String, ToolkitTreeNodeStates> mapping;
    private long dataCount;

    public static ToolkitTreeFlatModel of(ToolkitTreeNode node, Date now, long dataCount) {
        return ToolkitTreeFlatModelWriter.of(node.getNodePath().toString(), now, node, dataCount);
    }

    public ToolkitTreeFlatModel(Map<String, ToolkitTreeNodeStates> mapping, long dataCount) {
        this(null, new Date(), mapping, dataCount);
    }

    public ToolkitTreeFlatModel(String path, Date lastUpdateDate, Map<String, ToolkitTreeNodeStates> mapping, long dataCount) {
        this.path = path;
        this.lastUpdateDate = lastUpdateDate;
        this.mapping = Objects.requireNonNull(mapping);
        this.dataCount = dataCount;
    }

    public final String getPath() {
        return path;
    }

    public final Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public final Map<String, ToolkitTreeNodeStates> getMapping() {
        return mapping;
    }

    public final void addState(BooleanState state, Predicate<String> pathFilter) {
        mapping.entrySet().stream().filter(e -> pathFilter.test(e.getKey())).forEach(e -> state.setValue(e.getValue(), true));
    }

    public long getDataCount() {
        return dataCount;
    }

    public final <O> void addState(ToolkitTreeNodeBeanState<O> state, O value, Predicate<String> pathFilter) {
        mapping.entrySet().stream().filter(e -> pathFilter.test(e.getKey())).forEach(e -> state.setValue(e.getValue(), value));
    }

    public void removeMapping(String path) {
        ToolkitTreeNodeStates removed = mapping.remove(path);
        Integer count = removed.getState(ToolkitTreeNodeBean.STATE_COUNT.name());
        if (count == null) {
            dataCount--;
        }
    }
}
