package fr.ird.observe.navigation.tree.io.request;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

/**
 * Describes all configuration possible for a toolkit request (means will produce json data).
 * <p>
 * Created on 04/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.22
 */
public class ToolkitRequestConfig implements ObserveDto {
    private boolean loadReferential;
    private boolean recursive;
    private boolean serializeNulls;
    private boolean prettyPrint;
    /**
     * Is textual search case sensitive?  (only works with properties).
     *
     * <b>Note:</b> default behaviour is <b>not case sensitive</b>.
     */
    private boolean caseSensitive;

    public ToolkitRequestConfig() {
    }

    public ToolkitRequestConfig(ToolkitRequestConfig config) {
        setLoadReferential(config.isLoadReferential());
        setRecursive(config.isRecursive());
        setPrettyPrint(config.isPrettyPrint());
        setSerializeNulls(config.isSerializeNulls());
        setCaseSensitive(config.isCaseSensitive());
    }

    public ToolkitRequestConfig(boolean loadReferential, boolean recursive, boolean prettyPrint, boolean serializeNulls, boolean caseSensitive) {
        this.loadReferential = loadReferential;
        this.recursive = recursive;
        this.serializeNulls = serializeNulls;
        this.prettyPrint = prettyPrint;
        this.caseSensitive = caseSensitive;
    }

    public boolean isLoadReferential() {
        return loadReferential;
    }

    public boolean isRecursive() {
        return recursive;
    }

    public boolean isSerializeNulls() {
        return serializeNulls;
    }

    public boolean isPrettyPrint() {
        return prettyPrint;
    }

    public void setLoadReferential(boolean loadReferential) {
        this.loadReferential = loadReferential;
    }

    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    public void setSerializeNulls(boolean serializeNulls) {
        this.serializeNulls = serializeNulls;
    }

    public void setPrettyPrint(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }
}
