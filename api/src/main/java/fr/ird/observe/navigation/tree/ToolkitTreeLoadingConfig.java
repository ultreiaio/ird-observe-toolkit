package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.module.BusinessModule;

import java.util.StringJoiner;

/**
 * Describe how to load a tree.
 * <p>
 * Created on 31/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public interface ToolkitTreeLoadingConfig {

    String GROUP_BY_NAME = "groupByName";
    String GROUP_BY_FLAVOR = "groupByFlavor";
    String MODULE_NAME = "moduleName";
    String LOAD_DATA = "loadData";
    String LOAD_REFERENTIAL = "loadReferential";
    String LOAD_EMPTY_GROUP_BY = "loadEmptyGroupBy";
    String LOAD_DISABLED_GROUP_BY = "loadDisabledGroupBy";
    String LOAD_NULL_GROUP_BY = "loadNullGroupBy";
    String LOAD_TEMPORAL_GROUP_BY = "loadTemporalGroupBy";

    boolean isLoadData();

    void setLoadData(boolean loadData);

    boolean isLoadReferential();

    void setLoadReferential(boolean loadReferential);

    String getGroupByFlavor();

    String getGroupByName();

    void setGroupByName(String groupByName);

    void setGroupByFlavor(String groupByFlavor);

    String getModuleName();

    void setModuleName(String moduleName);

    boolean isLoadTemporalGroupBy();

    void setLoadTemporalGroupBy(boolean loadTemporalGroupBy);

    boolean isLoadDisabledGroupBy();

    void setLoadDisabledGroupBy(boolean loadDisabledGroupBy);

    boolean isLoadEmptyGroupBy();

    void setLoadEmptyGroupBy(boolean loadEmptyGroupBy);

    boolean isLoadNullGroupBy();

    void setLoadNullGroupBy(boolean loadNullGroupBy);

    //FIXME Review this, very weak
    default boolean acceptedModule(BusinessModule businessModule) {
        if (getModuleName() == null || getModuleName().equals(businessModule.getName())) {
            return true;
        }
        if (isLoadReferential() &&  !isLoadData()) {
            return getModuleName() != null && businessModule.getName().equals("common");

        }
        return false;
    }

    default void init(ToolkitTreeLoadingConfig incoming) {
        setLoadData(incoming.isLoadData());
        setLoadReferential(incoming.isLoadReferential());
        setModuleName(incoming.getModuleName());
        setGroupByName(incoming.getGroupByName());
        setGroupByFlavor(incoming.getGroupByFlavor());
        setLoadDisabledGroupBy(incoming.isLoadDisabledGroupBy());
        setLoadEmptyGroupBy(incoming.isLoadEmptyGroupBy());
        setLoadNullGroupBy(incoming.isLoadNullGroupBy());
        setLoadTemporalGroupBy(incoming.isLoadTemporalGroupBy());
    }

    default ToolkitTreeFlatModelRootRequest toRootRequest(GroupByHelper groupByHelper) {
        ToolkitTreeFlatModelRootRequest request = new ToolkitTreeFlatModelRootRequest();
        request.init(this);
        if (!isLoadData()) {
            request.setLoadDisabledGroupBy(false);
            request.setLoadEmptyGroupBy(false);
            request.setLoadNullGroupBy(false);
            request.setLoadTemporalGroupBy(false);
            request.setGroupByName(null);
            request.setGroupByFlavor(null);
            return request;
        }
        String groupByName = getGroupByName();
        DataGroupByDtoDefinition<?, ?> definition = groupByHelper.getDefinition(groupByName);
        if (definition.isQualitative()) {
            request.setLoadTemporalGroupBy(false);
            request.setGroupByFlavor(null);
            return request;
        }
        request.setLoadDisabledGroupBy(false);
        request.setLoadEmptyGroupBy(false);
        if (!isLoadTemporalGroupBy()) {
            request.setGroupByFlavor(null);
        }
        return request;
    }

    default String print() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("loadData=" + isLoadData())
                .add("loadReferential=" + isLoadReferential())
                .add("groupByName='" + getGroupByName() + "'")
                .add("groupByFlavor='" + getGroupByFlavor() + "'")
                .add("moduleName='" + getModuleName() + "'")
                .add("loadDisabledGroupBy=" + isLoadDisabledGroupBy())
                .add("loadEmptyGroupBy=" + isLoadEmptyGroupBy())
                .add("loadNullGroupBy=" + isLoadNullGroupBy())
                .add("loadTemporalGroupBy=" + isLoadTemporalGroupBy())
                .toString();
    }
}
