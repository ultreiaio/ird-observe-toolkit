package fr.ird.observe.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * Created by tchemit on 27/09/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class OpenNodeRequest {

    private final OpenNodeRequestConfiguration configuration;
    private final List<IdNode<?>> nodesToClose;
    private final List<IdNode<?>> nodesToOpen;

    public OpenNodeRequest(OpenNodeRequestConfiguration configuration, List<IdNode<?>> nodesToClose, List<IdNode<?>> nodesToOpen) {
        this.configuration = configuration;
        this.nodesToClose = nodesToClose;
        this.nodesToOpen = nodesToOpen;
    }

    public OpenNodeRequestConfiguration getRequest() {
        return configuration;
    }

    public List<IdNode<?>> getNodesToClose() {
        return nodesToClose;
    }

    public List<IdNode<?>> getNodesToOpen() {
        return nodesToOpen;
    }
}

