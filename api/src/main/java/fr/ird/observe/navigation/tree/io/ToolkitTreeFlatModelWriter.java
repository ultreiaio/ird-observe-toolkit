package fr.ird.observe.navigation.tree.io;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.lang.Strings;

import javax.swing.tree.TreeNode;
import java.beans.Introspector;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created on 08/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeFlatModelWriter {
    private final ToolkitTreeFlatModel result;
    private final LinkedHashMap<String, ToolkitTreeNodeStates> mapping;

    public static ToolkitTreeFlatModel of(String path, Date lastUpdateDate, ToolkitTreeNode node, long dataCount) {
        ToolkitTreeFlatModelWriter writer = new ToolkitTreeFlatModelWriter(path, lastUpdateDate, dataCount);
        Enumeration<TreeNode> enumeration = node.preorderEnumeration();

        if (node.isRoot()) {
            // skip the root node, we don't need it
            enumeration.nextElement();
        }
        while (enumeration.hasMoreElements()) {
            ToolkitTreeNode treeNode = (ToolkitTreeNode) enumeration.nextElement();
            writer.addPath(treeNode.getNodePath().toString(), treeNode.getUserObject().getUserStates());
        }
        return writer.build(false);
    }

    public ToolkitTreeFlatModelWriter(String path, Date lastUpdateDate, long dataCount) {
        this.result = new ToolkitTreeFlatModel(path, lastUpdateDate, mapping = new LinkedHashMap<>(), dataCount);
    }

    public ToolkitTreeNodeStates getStates(String path) {
        return mapping.get(path);
    }

    public ToolkitTreeFlatModel build(boolean removeTransient) {
        if (removeTransient) {
            for (String path : mapping.keySet()) {
                Iterator<Map.Entry<String, Object>> states = mapping.get(path).getStates().entrySet().iterator();
                while (states.hasNext()) {
                    Map.Entry<String, Object> entry = states.next();
                    String stateName = entry.getKey();
                    if (stateName.startsWith("$$") || stateName.equals(ToolkitTreeNodeBean.STATE_ID.name())) {
                        states.remove();
                    }
                }
            }
        }
        return result;
    }

    public void addPath(String path, ToolkitTreeNodeStates states) {
        mapping.put(path, states);
    }

    public void addReferential(BusinessProject project, ToolkitTreeFlatModelRootRequest request, Locale referentialLocale, Map<Class<? extends ReferentialDto>, Long> referentialCount, boolean canWrite) {
        List<BusinessModule> modules = project.getModules();
        for (BusinessModule module : modules) {
            if (request.acceptedModule(module)) {
                for (BusinessSubModule subModule : module.getSubModules()) {
                    subModule.getReferentialPackage().ifPresent(referentialPackage -> addReferentialPackage(project, module, subModule, referentialPackage, referentialLocale, referentialCount, canWrite));
                }
            }
        }
    }

    public void addReferentialPackage(BusinessProject project, BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Locale referentialLocale, Map<Class<? extends ReferentialDto>, Long> referentialCount, boolean canWrite) {
        String prefix = "referential" + (module.getName().equals(subModule.getName()) ? "" : Strings.capitalize(module.getName())) + Strings.capitalize(subModule.getName());
        addPath(prefix, ToolkitTreeNodeStates.of(ToolkitTreeNodeBean.STATE_EDITABLE.name(), canWrite));
        List<Class<? extends ReferentialDto>> types = project.sortWithLongTitle(new LinkedList<>(referentialPackage.getTypes()), referentialLocale);
        for (Class<? extends ReferentialDto> type : types) {
            int count = referentialCount.get(type).intValue();
            addPath(prefix + "/" + Introspector.decapitalize(type.getSimpleName().replace("Dto", "")), ToolkitTreeNodeStates.of(ToolkitTreeNodeBean.STATE_COUNT.name(), count));
        }
    }

    public ToolkitTreeNodeStates addId(String prefix, ToolkitId id) {
        ToolkitTreeNodeStates states = ToolkitTreeNodeStates.of(ToolkitTreeNodeBean.STATE_LAST_UPDATE_DATE.name(), id.getLastUpdateDate());
        addPath(prefix + (prefix.endsWith(":") ? "" : "/") + id.getTopiaId(), states);
        return states;
    }
}
