package fr.ird.observe.navigation.tree.io.request;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;

import java.util.Date;
import java.util.Objects;

/**
 * To load a tree flat model from a given path.
 * <p>
 * Created on 08/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeFlatModelPathRequest implements ObserveDto {

    private final String path;
    private final Date lastUpdateDate;
    private final boolean recursive;

    public ToolkitTreeFlatModelPathRequest(String path, Date lastUpdateDate, boolean recursive) {
        this.path = Objects.requireNonNull(path);
        this.lastUpdateDate = lastUpdateDate;
        this.recursive = recursive;
    }

    public String getPath() {
        return path;
    }

    public boolean isRecursive() {
        return recursive;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }
}
