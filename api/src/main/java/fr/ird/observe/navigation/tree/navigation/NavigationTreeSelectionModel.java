package fr.ird.observe.navigation.tree.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionEvent;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionListener;
import fr.ird.observe.navigation.tree.navigation.event.NavigationTreeSelectionVetoException;
import fr.ird.observe.spi.ui.BusyModel;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 07/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class NavigationTreeSelectionModel extends DefaultTreeSelectionModel {

    private static final Logger log = LogManager.getLogger(NavigationTreeSelectionModel.class);

    private final BusyModel busyModel;
    private boolean skipCheckPreviousContent;
    private boolean skipBusyModel;

    public NavigationTreeSelectionModel(BusyModel busyModel) {
        this.busyModel = busyModel;
        setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    public void addNavigationTreeSelectionListener(NavigationTreeSelectionListener x) {
        listenerList.add(NavigationTreeSelectionListener.class, x);
    }

    public void removeNavigationTreeSelectionListener(NavigationTreeSelectionListener x) {
        listenerList.remove(NavigationTreeSelectionListener.class, x);
    }

    public void fireVetoValueChanged(NavigationTreeSelectionEvent event) throws NavigationTreeSelectionVetoException {
        for (NavigationTreeSelectionListener listener : listenerList.getListeners(NavigationTreeSelectionListener.class)) {
            listener.valueChanged(event);
        }
    }

    public void fireTreeWillCollapseEvent(TreeExpansionEvent event) throws ExpandVetoException {
        for (NavigationTreeSelectionListener listener : listenerList.getListeners(NavigationTreeSelectionListener.class)) {
            listener.treeWillCollapse(event);
        }
    }

    public void fireTreeWillExpandEvent(TreeExpansionEvent event) throws ExpandVetoException {
        for (NavigationTreeSelectionListener listener : listenerList.getListeners(NavigationTreeSelectionListener.class)) {
            listener.treeWillExpand(event);
        }
    }

    @Override
    public void setSelectionPaths(TreePath[] pPaths) {
        if (pPaths.length > 1) {
            return;
        }
        TreePath path = pPaths[0];
        boolean canChange = !Objects.equals(path, getSelectionPath());
        if (canChange) {
            boolean[] newness = new boolean[pPaths.length];
            Arrays.fill(newness, true);
            NavigationTreeSelectionEvent event = new NavigationTreeSelectionEvent(this, pPaths, newness, getLeadSelectionPath(), path, skipCheckPreviousContent);
            try {
                fireVetoValueChanged(event);
            } catch (NavigationTreeSelectionVetoException e) {
                canChange = false;
            }
        }
        if (!canChange) {
            // cancel the change of node
            log.warn(String.format("Do not change selection path: %s against: %s", path, getSelectionPath()));
            return;
        }
        // can now safely select new path
        if (busyModel == null || skipBusyModel) {
            super.setSelectionPaths(pPaths);
        } else {
            busyModel.addTask(I18n.t("observe.ui.tree.loading.node"));
            SwingUtilities.invokeLater(() -> {
                try {
                    super.setSelectionPaths(pPaths);
                } finally {
                    busyModel.popTask();
                }
            });
        }
    }

    @Override
    public void clearSelection() {
        int selSize = selection == null ? 0 : selection.length;
        boolean[] newness = new boolean[selSize];

        for (int counter = 0; counter < selSize; counter++)
            newness[counter] = false;

        NavigationTreeSelectionEvent event = new NavigationTreeSelectionEvent
                (this, selection, newness, leadPath, null, skipCheckPreviousContent);
        try {
            fireVetoValueChanged(event);
        } catch (NavigationTreeSelectionVetoException e) {
            // cancel reset change
            return;
        }
        super.clearSelection();
    }

    public boolean isSkipCheckPreviousContent() {
        return skipCheckPreviousContent;
    }

    public void setSkipCheckPreviousContent(boolean skipCheckPreviousContent) {
        this.skipCheckPreviousContent = skipCheckPreviousContent;
    }

    public boolean isSkipBusyModel() {
        return skipBusyModel;
    }

    public void setSkipBusyModel(boolean skipBusyModel) {
        this.skipBusyModel = skipBusyModel;
    }
}
