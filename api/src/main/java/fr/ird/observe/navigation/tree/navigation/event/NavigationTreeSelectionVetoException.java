package fr.ird.observe.navigation.tree.navigation.event;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.event.TreeSelectionEvent;

public class NavigationTreeSelectionVetoException extends Exception {
    /**
     * The event that the exception was created for.
     */
    protected TreeSelectionEvent event;

    /**
     * Constructs an ExpandVetoException object with no message.
     *
     * @param event a TreeSelectionEvent object
     */

    public NavigationTreeSelectionVetoException(TreeSelectionEvent event) {
        this(event, null);
    }

    /**
     * Constructs an ExpandVetoException object with the specified message.
     *
     * @param event   a TreeSelectionEvent object
     * @param message a String containing the message
     */
    public NavigationTreeSelectionVetoException(TreeSelectionEvent event, String message) {
        super(message);
        this.event = event;
    }
}

