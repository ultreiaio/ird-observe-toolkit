package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.Icon;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class ToolkitTreeNode extends DefaultMutableTreeNode {
    private static final Logger log = LogManager.getLogger(ToolkitTreeNode.class);

    /**
     * Is the node loaded?
     */
    private boolean loaded;
    /**
     * Is model adjusting?
     */
    private boolean adjusting;

    protected ToolkitTreeNode(ToolkitTreeNodeBean userObject) {
        super(Objects.requireNonNull(userObject));
    }

    /**
     * To load children nodes if this node is not loaded.
     */
    protected abstract void populateChildrenIfNotLoaded();

    public <O> O getState(String stateName) {
        return getUserObject().getState(stateName);
    }

    public <O> O getState(String stateName, boolean up) {
        O state = getState(stateName);
        if (state != null) {
            return state;
        }
        if (up && getParent() != null) {
            return getParent().getState(stateName, true);
        }
        return null;
    }

    public <O> O getState(ToolkitTreeNodeBeanState<O> stateName) {
        return getState(stateName.name(), stateName.isRecursive());
    }

    /**
     * @return the path of this node to root.
     */
    public final Path getNodePath() {
        Path result = Path.of("");
        Object[] paths = getUserObjectPath();
        for (int i = 1, length = paths.length; i < length; i++) {
            ToolkitTreeNodeBean bean = (ToolkitTreeNodeBean) paths[i];
            String path = bean.getPath();
            result = result.resolve(path);
        }
        return result;
    }

    /**
     * @return icon associated with the node
     */
    public final Icon getIcon() {
        String iconPath = getUserObject().getIconPath();
        return iconPath == null ? null : UIManager.getIcon(iconPath);
    }

    public final boolean isEqualsId(String id) {
        ToolkitTreeNodeBean userObject = getUserObject();
        if (userObject == null) {
            return false;
        }
        if (Objects.equals(id, userObject.getId())) {
            return true;
        }
        String path = userObject.getPath();
        return Objects.equals(id, path);
    }

    public final boolean isLoaded() {
        return loaded;
    }

    public final boolean isNotLoaded() {
        return !loaded;
    }

    public void dirty() {
        if (isNotLoaded()) {
            // Avoid re-entrant code
            return;
        }
        loaded = false;
        log.info(String.format("%s Make dirty node %s", getUserObject().getPath(), this));
        dirtyStructure();
    }

    public final void loaded() {
        if (isLoaded()) {
            // Avoid re-entrant code
            return;
        }
        loaded = true;
        // when node is loaded, refresh ui
        //FIXME getTreeModel().ifPresent(t -> nodeChanged());
    }

    public final boolean isNotLeaf() {
        return !isLeaf();
    }

    protected void dirtyStructure() {

    }

    final Vector<?> childrenVector() {
        return this.children;
    }

    protected Optional<ToolkitTreeModelSupport<?>> getTreeModel() {
        ToolkitTreeNode root = getRoot();
        ToolkitTreeNodeBean userObject = root.getUserObject();
        if (userObject.isRoot()) {
            ToolkitTreeModelSupport<?> model = ToolkitTreeNodeBean.STATE_MODEL.getValue(userObject);
            return Optional.ofNullable(model);
        }
        return Optional.empty();
    }

    //--------------------------------------------------------------------------------------------
    // Update methods
    //--------------------------------------------------------------------------------------------

    public final void reloadNodeData() {
        reloadNodeData(false);
    }

    public final void reloadNodeData(boolean reloadChildren) {
        dirty();
        getUserObject().reload();
        loaded();
        if (reloadChildren) {
            Enumeration<?> children = children();
            while (children.hasMoreElements()) {
                ToolkitTreeNode childNode = (ToolkitTreeNode) children.nextElement();
                childNode.reloadNodeData();
            }
        }
    }

    public void updateSelectNodeId() {
        getUserObject().updateSelectNodeId();
    }

    //--------------------------------------------------------------------------------------------
    // Node changed methods
    //--------------------------------------------------------------------------------------------

    public final void updateNode() {
        reloadNodeData();
        removeAllChildren();
        dirty();
        populateChildrenIfNotLoaded();
        loaded();
    }

    public final void nodeChanged(boolean refreshFromParent, boolean refreshChildren) {
        if (refreshFromParent) {
            getParent().nodeChanged(false, false);
        }
        if (refreshChildren) {
            nodeChangedDeep();
        } else {
            nodeChanged();
        }
    }

    public final void nodeChanged() {
        ToolkitTreeModelSupport<?> model = getTreeModel().orElseThrow(IllegalStateException::new);
        log.debug(String.format("Will refresh node %s", this));
        model.nodeChanged(this);
    }

    public final void nodeChangedDeep() {
        ToolkitTreeModelSupport<?> model = getTreeModel().orElseThrow(IllegalStateException::new);
        log.debug(String.format("Will refresh deep node %s", this));
        model.nodeChanged(this);
        // repaint children nodes
        Enumeration<?> e = this.children();
        while (e.hasMoreElements()) {
            ToolkitTreeNode child = (ToolkitTreeNode) e.nextElement();
            model.nodeChanged(child);
            if (child.isNotLeaf()) {
                child.nodeChangedDeep();
            }
        }
    }

    public final void refreshToRoot() {
        if (!isRoot()) {
            log.info(String.format("Refresh to Root node: %s", this));
            nodeChangedDeep();
            getParent().refreshToRoot();
        }
    }

    public final void reloadNodeDataToRoot() {
        if (!isRoot()) {
            log.info(String.format("Reload to Root node: %s", this));
            reloadNodeData();
            getParent().reloadNodeDataToRoot();
        }
    }

    //--------------------------------------------------------------------------------------------
    // Find methods
    //--------------------------------------------------------------------------------------------

    public ToolkitTreeNode findChildById(String id) {
        return VerticalNavigationHelper.findChildById(this, id);
    }

    //--------------------------------------------------------------------------------------------
    // Modify structure methods
    //--------------------------------------------------------------------------------------------

    public boolean isAdjusting() {
        return adjusting;
    }

    public void adjusting() {
        this.adjusting = true;
    }

    public void unAdjusting() {
        this.adjusting = false;
    }

    @Override
    public void insert(MutableTreeNode newChild, int childIndex) {
        Optional<ToolkitTreeModelSupport<?>> treeModel = getTreeModel();
        if (treeModel.isPresent()) {
            adjusting();
            log.info(String.format("Insert node from model: %s", newChild));
            try {
                treeModel.get().insertNodeInto(newChild, this, childIndex);
            } finally {
                unAdjusting();
            }
            return;
        }
        super.insert(newChild, childIndex);
    }

    @Override
    public void removeFromParent() {
        Optional<ToolkitTreeModelSupport<?>> treeModel = getTreeModel();
        if (treeModel.isPresent()) {
            adjusting();
            log.info(String.format("%s Remove node from model: %s", getUserObject().getPath(), this));
            try {
                treeModel.get().removeNodeFromParent(this);
            } finally {
                unAdjusting();
            }
            return;
        }
        super.removeFromParent();
    }

//    @Override
//    public void remove(int childIndex) {
//        Optional<ToolkitTreeModelSupport<?>> treeModel = getTreeModel();
//        if (treeModel.isPresent()) {
//            adjusting();
//            ToolkitTreeNode childNode = getChildAt(childIndex);
//            log.info(String.format("%s Remove node from model: %s", getUserObject().getPath(), childNode));
//            try {
//                treeModel.get().removeNodeFromParent(childNode);
//            } finally {
//                unAdjusting();
//            }
//            return;
//        }
//        super.remove(childIndex);
//    }

    public void removeChildren(Set<String> ids) {
        for (String id : ids) {
            ToolkitTreeNode childNode = findChildById(id);
            Objects.requireNonNull(childNode, String.format("Could not find child node from %s with id: '%s'.", id, this)).removeFromParent();
        }
    }

    public void moveNode(ToolkitTreeNode child, int position) {
        ToolkitTreeModelSupport<?> model = getTreeModel().orElseThrow(IllegalStateException::new);
        remove(child);
        insert(child, position);
        model.nodeStructureChanged(this);
    }

    //--------------------------------------------------------------------------------------------
    // TreeNode override methods
    //--------------------------------------------------------------------------------------------

    @Override
    public ToolkitTreeNode getParent() {
        return (ToolkitTreeNode) super.getParent();
    }

    @Override
    public ToolkitTreeNode getRoot() {
        return (ToolkitTreeNode) super.getRoot();
    }

    @Override
    public ToolkitTreeNodeBean getUserObject() {
        return (ToolkitTreeNodeBean) super.getUserObject();
    }

    @Override
    public ToolkitTreeNode getChildAt(int index) {
        return (ToolkitTreeNode) super.getChildAt(index);
    }

    @Override
    public ToolkitTreeNode getFirstChild() {
        return (ToolkitTreeNode) super.getFirstChild();
    }

    @Override
    public ToolkitTreeNode getLastChild() {
        return (ToolkitTreeNode) super.getLastChild();
    }

    @Override
    public ToolkitTreeNode getChildAfter(TreeNode aChild) {
        return (ToolkitTreeNode) super.getChildAfter(aChild);
    }

    @Override
    public ToolkitTreeNode getChildBefore(TreeNode aChild) {
        return (ToolkitTreeNode) super.getChildBefore(aChild);
    }

    @Override
    public ToolkitTreeNode getNextSibling() {
        return (ToolkitTreeNode) super.getNextSibling();
    }

    @Override
    public ToolkitTreeNode getPreviousSibling() {
        return (ToolkitTreeNode) super.getPreviousSibling();
    }

    @Override
    public ToolkitTreeNode getFirstLeaf() {
        return (ToolkitTreeNode) super.getFirstLeaf();
    }

    @Override
    public ToolkitTreeNode getLastLeaf() {
        return (ToolkitTreeNode) super.getLastLeaf();
    }

    @Override
    public ToolkitTreeNode getNextLeaf() {
        return (ToolkitTreeNode) super.getNextLeaf();
    }

    @Override
    public ToolkitTreeNode getPreviousLeaf() {
        return (ToolkitTreeNode) super.getPreviousLeaf();
    }

}
