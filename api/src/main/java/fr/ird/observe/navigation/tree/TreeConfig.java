package fr.ird.observe.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;

/**
 * Created on 06/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
@GenerateJavaBeanDefinition
public class TreeConfig extends AbstractJavaBean implements ToolkitTreeLoadingConfig {

    public static final String NODE_DISABLED_COLOR = "nodeDisabledColor";
    public static final String CAN_LOAD_DATA = "canLoadData";
    public static final String CAN_LOAD_REFERENTIAL = "canLoadReferential";
    public static final String GROUP_BY_NAME_LABEL = I18n.n("observe.Common.navigation.config.groupByName");
    public static final String GROUP_BY_NAME_DESCRIPTION = I18n.n("observe.Common.navigation.config.groupByName.description");
    public static final String GROUP_BY_FLAVOR_LABEL = I18n.n("observe.Common.navigation.config.groupByFlavor");
    public static final String GROUP_BY_FLAVOR_DESCRIPTION = I18n.n("observe.Common.navigation.config.groupByFlavor.description");
    public static final String MODULE_NAME_LABEL = I18n.n("observe.Common.navigation.config.moduleName");
    public static final String MODULE_NAME_DESCRIPTION = I18n.n("observe.Common.navigation.config.moduleName.description");
    public static final String LOAD_DATA_I18N_LABEL = I18n.n("observe.Common.navigation.config.loadData");
    public static final String LOAD_DATA_I18N_DESCRIPTION = I18n.n("observe.Common.navigation.config.loadData.description");
    public static final String LOAD_REFERENTIAL_I18N_LABEL = I18n.n("observe.Common.navigation.config.loadReferential");
    public static final String LOAD_REFERENTIAL_I18N_DESCRIPTION = I18n.n("observe.Common.navigation.config.loadReferential.description");
    public static final String NULL_GROUP_BY_VALUE_I18N_LABEL = I18n.n("observe.Common.no.value");
    private static final Logger log = LogManager.getLogger(TreeConfig.class);

    /**
     * Load data?
     */
    private boolean loadData;
    /**
     * Load referential?
     */
    private boolean loadReferential;
    /**
     * Allow to load data?
     */
    private boolean canLoadData;
    /**
     * Allow to load referential?
     */
    private boolean canLoadReferential;
    /**
     * Group by name (optional).
     */
    private String groupByName;
    /**
     * Group by flavor (optional).
     */
    private String groupByFlavor;
    /**
     * Module name (optional).
     */
    private String moduleName;
    /**
     * Load disabled group by?
     */
    private boolean loadDisabledGroupBy;
    /**
     * Load empty group by?
     */
    private boolean loadEmptyGroupBy;
    /**
     * Load null group by?
     */
    private boolean loadNullGroupBy;
    /**
     * Load temporal group by?
     */
    private boolean loadTemporalGroupBy;
    /**
     * Color of disabled node.
     */
    private Color nodeDisabledColor;

    public void init(TreeConfig incoming) {
        setCanLoadData(incoming.isCanLoadData());
        setCanLoadReferential(incoming.isCanLoadReferential());
        ToolkitTreeLoadingConfig.super.init(incoming);
        log.info(String.format("Tree config initialized with: %s", this));
    }

    public boolean isCanLoadData() {
        return canLoadData;
    }

    public boolean isCanLoadReferential() {
        return canLoadReferential;
    }

    @Override
    public String getGroupByFlavor() {
        return groupByFlavor;
    }

    @Override
    public void setGroupByFlavor(String groupByFlavor) {
        String oldValue = getGroupByFlavor();
        this.groupByFlavor = groupByFlavor;
        firePropertyChange(GROUP_BY_FLAVOR, oldValue, groupByFlavor);
    }

    public void setCanLoadData(boolean canLoadData) {
        boolean oldValue = isCanLoadData();
        this.canLoadData = canLoadData;
        firePropertyChange(CAN_LOAD_DATA, oldValue, canLoadData);
    }

    public void setCanLoadReferential(boolean canLoadReferential) {
        boolean oldValue = isCanLoadReferential();
        this.canLoadReferential = canLoadReferential;
        firePropertyChange(CAN_LOAD_REFERENTIAL, oldValue, canLoadReferential);
    }

    @Override
    public boolean isLoadTemporalGroupBy() {
        return loadTemporalGroupBy;
    }

    @Override
    public void setLoadTemporalGroupBy(boolean loadTemporalGroupBy) {
        boolean oldValue = isLoadTemporalGroupBy();
        this.loadTemporalGroupBy = loadTemporalGroupBy;
        firePropertyChange(LOAD_TEMPORAL_GROUP_BY, oldValue, loadTemporalGroupBy);
    }

    @Override
    public boolean isLoadData() {
        return loadData;
    }

    @Override
    public void setLoadData(boolean loadData) {
        boolean oldValue = isLoadData();
        this.loadData = loadData;
        firePropertyChange(LOAD_DATA, oldValue, loadData);
    }

    @Override
    public boolean isLoadReferential() {
        return loadReferential;
    }

    @Override
    public void setLoadReferential(boolean loadReferential) {
        boolean oldValue = isLoadReferential();
        this.loadReferential = loadReferential;
        firePropertyChange(LOAD_REFERENTIAL, oldValue, loadReferential);
    }

    @Override
    public String getModuleName() {
        return moduleName;
    }

    @Override
    public void setModuleName(String moduleName) {
        String oldValue = getModuleName();
        this.moduleName = moduleName;
        firePropertyChange(MODULE_NAME, oldValue, moduleName);
    }

    @Override
    public String getGroupByName() {
        return groupByName;
    }

    @Override
    public void setGroupByName(String groupByName) {
        String oldValue = getGroupByName();
        this.groupByName = groupByName;
        firePropertyChange(GROUP_BY_NAME, oldValue, groupByName);
    }

    @Override
    public boolean isLoadEmptyGroupBy() {
        return loadEmptyGroupBy;
    }

    @Override
    public void setLoadEmptyGroupBy(boolean loadEmptyGroupBy) {
        boolean oldValue = isLoadEmptyGroupBy();
        this.loadEmptyGroupBy = loadEmptyGroupBy;
        firePropertyChange(LOAD_EMPTY_GROUP_BY, oldValue, loadEmptyGroupBy);
    }

    @Override
    public boolean isLoadDisabledGroupBy() {
        return loadDisabledGroupBy;
    }

    @Override
    public void setLoadDisabledGroupBy(boolean loadDisabledGroupBy) {
        boolean oldValue = isLoadDisabledGroupBy();
        this.loadDisabledGroupBy = loadDisabledGroupBy;
        firePropertyChange(LOAD_DISABLED_GROUP_BY, oldValue, loadDisabledGroupBy);
    }

    @Override
    public boolean isLoadNullGroupBy() {
        return loadNullGroupBy;
    }

    @Override
    public void setLoadNullGroupBy(boolean loadNullGroupBy) {
        boolean oldValue = this.loadNullGroupBy;
        this.loadNullGroupBy = loadNullGroupBy;
        firePropertyChange(LOAD_NULL_GROUP_BY, oldValue, loadNullGroupBy);
    }

    public Color getNodeDisabledColor() {
        return nodeDisabledColor;
    }

    public void setNodeDisabledColor(Color nodeDisabledColor) {
        Color oldValue = this.nodeDisabledColor;
        this.nodeDisabledColor = nodeDisabledColor;
        firePropertyChange(NODE_DISABLED_COLOR, oldValue, nodeDisabledColor);
    }

    @Override
    public String toString() {
        return print();
    }
}
