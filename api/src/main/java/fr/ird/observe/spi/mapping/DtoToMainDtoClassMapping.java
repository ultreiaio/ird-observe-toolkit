package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;

import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5
 */
public abstract class DtoToMainDtoClassMapping extends ImmutableClassMapping<BusinessDto, BusinessDto> {

    protected DtoToMainDtoClassMapping(Map<Class<? extends BusinessDto>, Class<? extends BusinessDto>> mapping) {
        super(mapping);
    }
}
