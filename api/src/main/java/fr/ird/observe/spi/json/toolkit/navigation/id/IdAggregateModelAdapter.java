package fr.ird.observe.spi.json.toolkit.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.navigation.id.IdAggregateModel;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import io.ultreia.java4all.util.json.JsonHierarchicAdapter;
import io.ultreia.java4all.lang.Objects2;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by tchemit on 28/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(JsonHierarchicAdapter.class)
public class IdAggregateModelAdapter implements JsonSerializer<IdAggregateModel>, JsonDeserializer<IdAggregateModel>, JsonHierarchicAdapter {

    @Override
    public Class<?> type() {
        return IdAggregateModel.class;
    }

    @Override
    public JsonElement serialize(IdAggregateModel src, Type typeOfSrc, JsonSerializationContext context) {
        Map<Class<?>, Map<Class<?>, String>> builder = new LinkedHashMap<>();
        for (IdModel model : src.getEnabledModels()) {
            Map<Class<?>, String> nodeBuilder = new LinkedHashMap<>();
            for (IdNode<?> node : model.getNodesWithIds()) {
                nodeBuilder.put(node.getClass(), node.getId());
            }
            builder.put(model.getClass(), nodeBuilder);
        }
        return context.serialize(builder);
    }

    @Override
    public IdAggregateModel deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        Map<Class<? extends IdModel>, Map<Class<? extends IdNode<?>>, String>> models = context.deserialize(json, new TypeToken<Map<Class<? extends IdModel>, Map<Class<? extends IdNode<?>>, String>>>() {
        }.getType());
        IdAggregateModel result = (IdAggregateModel) Objects2.newInstance((Class<?>) type);
        for (Map.Entry<Class<? extends IdModel>, Map<Class<? extends IdNode<?>>, String>> entry : models.entrySet()) {
            IdModel model = result.forModelType(entry.getKey()).orElseThrow(IllegalStateException::new);
            for (Map.Entry<Class<? extends IdNode<?>>, String> nodeEntry : entry.getValue().entrySet()) {
                model.getNode(nodeEntry.getKey()).setId(nodeEntry.getValue());
            }
        }
        return result;
    }
}
