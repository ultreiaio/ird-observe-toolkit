package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.I18n;

/**
 * Created on 09/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class BusinessProjectI18nHelper {

    static String i18nKeyPrefix;

    public static String getI18nKeyPrefix() {
        if (i18nKeyPrefix == null) {
            i18nKeyPrefix = I18nDecoratorHelper.get().getCommonPrefix();
        }
        return i18nKeyPrefix;
    }

    public static void setI18nKeyPrefix(String i18nKeyPrefix) {
        BusinessProjectI18nHelper.i18nKeyPrefix = i18nKeyPrefix;
    }

    public static String getModule(String moduleName) {
        return I18n.t(getModuleKey(moduleName));
    }

    public static String getModuleKey(String moduleName) {
        return String.format(getI18nKey("business.module.%1$s"), moduleName);
    }

    public static String getModuleShortKey(String moduleName) {
        return String.format(getI18nKey("business.module.%1$s.short"), moduleName);
    }

    public static String getSubModule(String moduleName, String subModuleName) {
        return I18n.t(getSubModuleKey(moduleName, subModuleName));
    }

    public static String getSubModuleKey(String moduleName, String subModuleName) {
        return String.format(getI18nKey("business.module.%1$s.package.%2$s"), moduleName, subModuleName);
    }

    private static String getI18nKey(String suffix) {
        return i18nKeyPrefix + suffix;
    }
}
