package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.LayoutAware;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.i18n.I18n;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class BusinessSubModule {

    private final String name;
    private final BusinessReferentialPackage referentialPackage;
    private final BusinessDataPackage dataPackage;

    public BusinessSubModule(String name, BusinessReferentialPackage referentialPackage, BusinessDataPackage dataPackage) {
        this.name = Objects.requireNonNull(name);
        this.referentialPackage = referentialPackage;
        this.dataPackage = dataPackage;
    }

    public String getName() {
        return name;
    }

    public abstract String getLabelKey();

    public final String getLabel(Locale l) {
        return I18n.l(l, getLabelKey());
    }

    public final String getLabel() {
        return I18n.t(getLabelKey());
    }

    public Optional<BusinessReferentialPackage> getReferentialPackage() {
        return Optional.ofNullable(referentialPackage);
    }

    public Optional<BusinessDataPackage> getDataPackage() {
        return Optional.ofNullable(dataPackage);
    }

    public Set<Class<? extends ReferentialDto>> getReferentialTypes() {
        return getReferentialPackage().map(BusinessPackage::getTypes).orElse(Set.of());
    }

    public Set<Class<? extends BusinessDto>> getTypes() {
        Set<Class<? extends BusinessDto>> result = new LinkedHashSet<>(getReferentialTypes());
        result.addAll(getDataTypes());
        return result;
    }

    public Set<Class<? extends DataDto>> getDataTypes() {
        return getDataPackage().map(BusinessPackage::getTypes).orElse(Set.of());
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends SimpleDto>> getSimpleDataTypes() {
        return (Set) getDataTypes().stream().filter(SimpleDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends SimpleDto>> getLayoutDataTypes() {
        return (Set) getDataTypes().stream().filter(LayoutAware.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends ContainerDto<?>>> getContainerDataTypes() {
        return (Set) getDataTypes().stream().filter(ContainerDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends ContainerChildDto>> getContainerChildDataTypes() {
        return (Set) getDataTypes().stream().filter(ContainerChildDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends RootOpenableDto>> getRootOpenableDataTypes() {
        return (Set) getDataTypes().stream().filter(RootOpenableDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends OpenableDto>> getOpenableDataTypes() {
        return (Set) getDataTypes().stream().filter(OpenableDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public Set<Class<? extends EditableDto>> getEditableDataTypes() {
        return (Set) getDataTypes().stream().filter(EditableDto.class::isAssignableFrom).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public String getDataPackageName() {
        return getDataPackage().map(BusinessPackage::getPackageName).orElse(null);
    }

    public String getReferentialPackageName() {
        return getReferentialPackage().map(BusinessPackage::getPackageName).orElse(null);
    }
}
