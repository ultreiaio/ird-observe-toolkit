package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Describes a business module.
 * <p>
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class BusinessModule {

    protected transient final SingletonSupplier<? extends BusinessProject> project;
    private final String name;
    private final List<BusinessSubModule> subModules;

    public BusinessModule(String name, List<BusinessSubModule> subModules) {
        this.name = Objects.requireNonNull(name);
        this.subModules = Objects.requireNonNull(subModules);
        this.project = SingletonSupplier.of(getProjectSupplier());
    }

    public int getSubModulePriority(BusinessSubModule subModule) {
        return subModules.indexOf(subModule);
    }

    public String getName() {
        return name;
    }

    public List<BusinessSubModule> getSubModules() {
        return subModules;
    }

    public List<DataGroupByDtoDefinition<?, ?>> getDataGroupByDtoDefinitions() {
        return project().getMapping().getDataGroupByDtoToDefinitionMapping().getDefinitions(this);
    }

    public BusinessProject project() {
        return project.get();
    }

    public String getModuleShortLabel() {
        String shortLabel;
        if (getName().equals("common")) {
            shortLabel = "";
        } else {
            shortLabel = String.format("[%s] ", getShortLabel());
        }
        return shortLabel;
    }

    public String getModuleShortLabel(Locale locale) {
        String shortLabel;
        if (getName().equals("common")) {
            shortLabel = "";
        } else {
            shortLabel = String.format("[%s] ", getShortLabel(locale));
        }
        return shortLabel;
    }

    protected abstract Supplier<? extends BusinessProject> getProjectSupplier();

    public abstract String getLabelKey();

    public abstract String getShortLabelKey();

    public final String getLabel(Locale l) {
        return I18n.l(l, getLabelKey());
    }

    public final String getLabel() {
        return I18n.t(getLabelKey());
    }

    public final String getShortLabel(Locale l) {
        return I18n.l(l, getShortLabelKey());
    }

    public final String getShortLabel() {
        return I18n.t(getShortLabelKey());
    }

    public Set<Class<? extends BusinessDto>> getTypes() {
        return subModules.stream().flatMap(m -> m.getTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends ReferentialDto>> getReferentialTypes() {
        return subModules.stream().flatMap(m -> m.getReferentialTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends DataDto>> getDataTypes() {
        return subModules.stream().flatMap(m -> m.getDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends SimpleDto>> getLayoutDataTypes() {
        return subModules.stream().flatMap(m -> m.getLayoutDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends SimpleDto>> getSimpleDataTypes() {
        return subModules.stream().flatMap(m -> m.getSimpleDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends ContainerDto<?>>> getContainerDataTypes() {
        return subModules.stream().flatMap(m -> m.getContainerDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends ContainerChildDto>> getContainerChildDataTypes() {
        return subModules.stream().flatMap(m -> m.getContainerChildDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends RootOpenableDto>> getRootOpenableDataTypes() {
        return subModules.stream().flatMap(m -> m.getRootOpenableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends OpenableDto>> getOpenableDataTypes() {
        return subModules.stream().flatMap(m -> m.getOpenableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends EditableDto>> getEditableDataTypes() {
        return subModules.stream().flatMap(m -> m.getEditableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public boolean accept(String id) {
        String gearType = getName().toLowerCase();
        return id != null && id.contains("." + gearType + ".");
    }

    public boolean acceptModuleName(String id) {
        return name.equalsIgnoreCase(id);
    }

    public int computeNodeCount() {
        int expectedNodeCount = 0;
        for (BusinessSubModule subModule : this.getSubModules()) {
            Set<Class<? extends ReferentialDto>> referentialTypes = subModule.getReferentialTypes();
            int tmp = expectedNodeCount;
            int referentialSize = referentialTypes.size();
            if (referentialSize > 0) {
                expectedNodeCount += 1 + referentialSize; // home referential + one for each referential type
            }
            int total = expectedNodeCount - tmp;
            System.out.printf("Found %s-%s REFERENTIAL.... %d%n", this.getName(), subModule.getName(), total);
            tmp = expectedNodeCount;

            int openableSize = subModule.getOpenableDataTypes().size();
            int rootOpenableSize = subModule.getRootOpenableDataTypes().size();
            int editableSize = subModule.getEditableDataTypes().size();
            int layoutSize = subModule.getLayoutDataTypes().size();
            int simpleSize = subModule.getSimpleDataTypes().size() - layoutSize; // layout are simple... FIXME
            int containerChildSize = subModule.getContainerChildDataTypes().size();
            if (rootOpenableSize > 0) {
                expectedNodeCount += rootOpenableSize; // root open form
                System.out.printf("Found %s-%s ROOT_OPEN...... %d%n", this.getName(), subModule.getName(), rootOpenableSize);
                expectedNodeCount += rootOpenableSize; // root open list form
                System.out.printf("Found %s-%s ROOT_OPEN_LIST. %d%n", this.getName(), subModule.getName(), rootOpenableSize);
            }
            if (openableSize > 0) {
                expectedNodeCount += openableSize; // open form
                System.out.printf("Found %s-%s OPEN........... %d%n", this.getName(), subModule.getName(), openableSize);
                expectedNodeCount += openableSize; // open list form
                System.out.printf("Found %s-%s OPEN_LIST...... %d%n", this.getName(), subModule.getName(), openableSize);
            }
            if (editableSize > 0) {
                expectedNodeCount += editableSize; // edit form
                System.out.printf("Found %s-%s EDIT........... %d%n", this.getName(), subModule.getName(), editableSize);
            }
            if (simpleSize > 0) {
                expectedNodeCount += simpleSize; // simple form
                System.out.printf("Found %s-%s SIMPLE......... %d%n", this.getName(), subModule.getName(), simpleSize);
            }
            if (layoutSize > 0) {
                expectedNodeCount += layoutSize; // layout form
                System.out.printf("Found %s-%s LAYOUT......... %d%n", this.getName(), subModule.getName(), layoutSize);
            }
            if (containerChildSize > 0) {
                expectedNodeCount += containerChildSize; // container form
                System.out.printf("Found %s-%s CONTAINER_CHILD %d%n", this.getName(), subModule.getName(), containerChildSize);
            }
            System.out.printf("Found %s-%s DATA........... %d%n", this.getName(), subModule.getName(), expectedNodeCount - tmp);
            total += expectedNodeCount - tmp;
            System.out.printf("Found %s-%s ............... %d%n", this.getName(), subModule.getName(), total);
        }
        System.out.printf("Found %s ...................... %d%n", this.getName(), expectedNodeCount);
        return expectedNodeCount;
    }
}
