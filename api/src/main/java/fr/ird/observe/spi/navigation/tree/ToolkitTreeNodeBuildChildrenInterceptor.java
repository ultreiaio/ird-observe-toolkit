package fr.ird.observe.spi.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;

import java.util.Collection;

/**
 * To generate children of a node via interceptor mechanism.
 * <p>
 * Created on 10/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class ToolkitTreeNodeBuildChildrenInterceptor implements ToolkitTreeNodeInterceptor {

    public static final String $$_DATA = "$$data";
    protected final boolean recursive;

    protected ToolkitTreeNodeBuildChildrenInterceptor(boolean recursive) {
        this.recursive = recursive;
    }

    protected <N extends ToolkitTreeNode> void addTableChildren(ToolkitTreeNode node, Collection<?> data, boolean enabled, N childrenNode) {
        if (data == null) {
            return;
        }
        ToolkitTreeNodeBean.STATE_COUNT.setValue(childrenNode.getUserObject(), data.size());
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
    }

    protected <N extends ToolkitTreeNode> void addSimpleChildren(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        childrenNode.getUserObject().addTransientState($$_DATA, data);
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
    }

    protected <N extends ToolkitTreeNode> void addOpenList(ToolkitTreeNode node, Collection<?> data, boolean enabled, N childrenNode) {
        if (data == null) {
            return;
        }
        ToolkitTreeNodeBean.STATE_COUNT.setValue(childrenNode.getUserObject(), data.size());
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
        if (recursive) {
            intercept(childrenNode);
        }
    }

    protected <N extends ToolkitTreeNode> void addOpen(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        childrenNode.getUserObject().addTransientState($$_DATA, data);
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
        if (recursive) {
            intercept(childrenNode);
        }
    }

    protected <N extends ToolkitTreeNode> void addRootOpen(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        childrenNode.getUserObject().addTransientState($$_DATA, data);
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
        if (recursive) {
            intercept(childrenNode);
        }
    }

    protected <N extends ToolkitTreeNode> void addEdit(ToolkitTreeNode node, Object data, boolean enabled, N childrenNode) {
        childrenNode.getUserObject().addTransientState($$_DATA, data);
        ToolkitTreeNodeBean.STATE_ENABLED.setValue(childrenNode.getUserObject(), enabled);
        node.add(childrenNode);
        if (recursive) {
            intercept(childrenNode);
        }
    }
}
