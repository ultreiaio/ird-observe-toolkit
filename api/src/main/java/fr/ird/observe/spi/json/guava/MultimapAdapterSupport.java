package fr.ird.observe.spi.json.guava;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class MultimapAdapterSupport<K, V, M extends Multimap<K, V>> implements JsonDeserializer<M>, JsonSerializer<M> {

    static <K, V> TypeToken<Map<K, Collection<V>>> multimapOf(TypeToken<K> keyType, TypeToken<V> valueType) {
        return new TypeToken<Map<K, Collection<V>>>() {
        }
                .where(new TypeParameter<>() {
                }, keyType)
                .where(new TypeParameter<>() {
                }, valueType);
    }

    protected abstract M createMultimap();

    @Override
    public M deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type keyType = actualTypeArguments[0];
        Type valueType = actualTypeArguments[1];

        Type type2 = multimapOf(TypeToken.of(keyType), TypeToken.of(valueType)).getType();

        M result = createMultimap();
        if (!json.isJsonArray()) {

            Map<K, List<V>> map = context.deserialize(json, type2);

            for (K key : map.keySet()) {
                List<V> multimapValues = map.get(key);
                result.putAll(key, multimapValues);
            }
        } else {
            JsonArray asJsonArray = json.getAsJsonArray();
            for (JsonElement jsonElement : asJsonArray) {
                JsonObject entryObject = (JsonObject) jsonElement;
                K key = context.deserialize(entryObject.get("key"), keyType);
                JsonArray valuesArray = (JsonArray) entryObject.get("value");
                Iterator<JsonElement> valuesIterator = valuesArray.iterator();
                List<V> entryValues = new LinkedList<>();
                while (valuesIterator.hasNext()) {
                    JsonElement value = valuesIterator.next();
                    entryValues.add(context.deserialize(value, valueType));
                }
                result.putAll(key, entryValues);
            }
        }
        return result;
    }

    @Override
    public JsonElement serialize(M src, Type type, JsonSerializationContext context) {
        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
//        Type keyType = actualTypeArguments[0];
        Type valueType = actualTypeArguments[1];
        JsonArray result = new JsonArray();
        for (Map.Entry<K, Collection<V>> entry : src.asMap().entrySet()) {
            K key = entry.getKey();
            Collection<V> value = entry.getValue();
            JsonObject entryObject = new JsonObject();
            entryObject.add("key", context.serialize(key));
            JsonArray valuesObject = new JsonArray();
            for (V o1 : value) {
                valuesObject.add(context.serialize(o1, valueType));
            }
            entryObject.add("value", valuesObject);
            result.add(entryObject);
        }

        return result;
    }
}
