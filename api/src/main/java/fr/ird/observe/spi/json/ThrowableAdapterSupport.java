package fr.ird.observe.spi.json;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;

/**
 * Created on 20/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.1.6
 */
public abstract class ThrowableAdapterSupport<T extends Throwable> implements JsonDeserializer<T>, JsonAdapter {
    public static final String MESSAGE = "message";
    public static final String STACK_TRACE = "stackTrace";

    protected String getMessage(JsonObject json) {
        JsonElement message = json.get(MESSAGE);
        if (message == null) {
            // For java > 11 (TODO Find exactly when, but on Java 17 it is now detailMessage)
            message = json.get("detailMessage");
        }
        return message == null ? null : message.getAsString();
    }

    protected abstract T create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException;

    @Override
    public final T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        T result = create(jsonObject, getMessage(jsonObject), context);
        return setStackTrace(jsonObject, result, context);
    }

    protected T setStackTrace(JsonObject json, T exception, JsonDeserializationContext context) {
        JsonElement stackTrace = json.get(STACK_TRACE);
        if (stackTrace != null) {
            StackTraceElement[] stackTraceElements = context.deserialize(stackTrace, StackTraceElement[].class);
            exception.setStackTrace(stackTraceElements);
        }
        return exception;
    }
}
