package fr.ird.observe.spi.navigation.parent;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;

import java.util.Objects;

/**
 * Created on 06/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ParentLink {

    private final String propertyName;
    private final String className;
    private final String parentClassName;
    private final MetaModelNodeLinkMultiplicity multiplicity;

    public ParentLink(String propertyName, String className, String parentClassName, MetaModelNodeLinkMultiplicity multiplicity) {
        this.propertyName = propertyName;
        this.className = className;
        this.parentClassName = parentClassName;
        this.multiplicity = multiplicity;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getClassName() {
        return className;
    }

    public String getParentClassName() {
        return parentClassName;
    }

    public MetaModelNodeLinkMultiplicity getMultiplicity() {
        return multiplicity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParentLink)) return false;
        ParentLink that = (ParentLink) o;
        return propertyName.equals(that.propertyName) && className.equals(that.className) && parentClassName.equals(that.parentClassName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(propertyName, className, parentClassName);
    }
}
