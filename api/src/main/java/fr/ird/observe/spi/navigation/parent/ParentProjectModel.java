package fr.ird.observe.spi.navigation.parent;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ParentProjectModel {

    private final List<ParentLink> links;

    public ParentProjectModel(List<ParentLink> links) {
        this.links = links;
    }

    public List<ParentLink> getLinks() {
        return links;
    }

    public String getterName(String node) {
        String packageName = ProjectPackagesDefinition.getPackageName(node);
        String simpleName = ProjectPackagesDefinition.getSimpleName(node);
        Pair<String, String> gav = ProjectPackagesDefinition.gav(packageName);
        return Strings.capitalize(gav.getLeft()) +
                Strings.capitalize(gav.getRight()) +
                simpleName;
    }
}
