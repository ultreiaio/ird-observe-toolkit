package fr.ird.observe.spi.navigation.model.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 31/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class IdNodeModel {

    private final String parentClassName;
    private final String className;
    private final String type;
    private final int level;
    private Map<String, String> nodes;

    public IdNodeModel(String parentClassName, String className, String type, int level) {
        this.parentClassName = parentClassName;
        this.className = className;
        this.type = type;
        this.level = level;
    }

    public String getParentClassName() {
        return parentClassName;
    }

    public String getClassName() {
        return className;
    }

    public String getType() {
        return type;
    }

    public int getLevel() {
        return level;
    }

    public Map<String, String> getNodes() {
        if (nodes == null) {
            nodes = new LinkedHashMap<>();
        }
        return nodes;
    }
}
