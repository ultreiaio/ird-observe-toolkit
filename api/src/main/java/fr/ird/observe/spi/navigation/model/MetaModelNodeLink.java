package fr.ird.observe.spi.navigation.model;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.StringJoiner;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class MetaModelNodeLink {

    private final String propertyName;
    private final MetaModelNodeLinkMultiplicity multiplicity;
    private final String target;

    public MetaModelNodeLink(String propertyName, MetaModelNodeLinkMultiplicity multiplicity, String target) {
        this.propertyName = propertyName;
        this.multiplicity = multiplicity;
        this.target = target;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public MetaModelNodeLinkMultiplicity getMultiplicity() {
        return multiplicity;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "{", "}")
                .add("'propertyName':'" + propertyName + "'")
                .add("'target':'" + target + "'")
                .add("'multiplicity':" + multiplicity)
                .toString();
    }
}
