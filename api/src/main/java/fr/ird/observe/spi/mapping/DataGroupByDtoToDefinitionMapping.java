package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.spi.module.BusinessModule;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;
import io.ultreia.java4all.lang.Strings;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created on 05/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class DataGroupByDtoToDefinitionMapping extends ImmutableClassMapping<DataGroupByDto<?>, DataGroupByDtoDefinition<?, ?>> {

    private final Map<Class<? extends DataGroupByDto<?>>, DataGroupByDtoDefinition<?, ?>> mapping;
    private final Map<String, DataGroupByDtoDefinition<?, ?>> mappingByName;

    private static Map<Class<? extends DataGroupByDto<?>>, Class<? extends DataGroupByDtoDefinition<?, ?>>> mapping(Map<Class<? extends DataGroupByDto<?>>, DataGroupByDtoDefinition<?, ?>> mapping) {
        Map<Class<? extends DataGroupByDto<?>>, Class<? extends DataGroupByDtoDefinition<?, ?>>> builder = new LinkedHashMap<>(mapping.size());
        for (Map.Entry<Class<? extends DataGroupByDto<?>>, DataGroupByDtoDefinition<?, ?>> entry : mapping.entrySet()) {
            Class<? extends DataGroupByDto<?>> key = entry.getKey();
            DataGroupByDtoDefinition<?, ?> value = entry.getValue();
            @SuppressWarnings("unchecked") Class<? extends DataGroupByDtoDefinition<?, ?>> definitionType = (Class<? extends DataGroupByDtoDefinition<?, ?>>) value.getClass();
            builder.put(key, definitionType);
        }
        return Collections.unmodifiableMap(builder);
    }

    protected DataGroupByDtoToDefinitionMapping(Map<Class<? extends DataGroupByDto<?>>, DataGroupByDtoDefinition<?, ?>> mapping) {
        super(mapping(mapping));
        this.mapping = mapping;
        this.mappingByName = Maps.uniqueIndex(mapping.values(), DataGroupByDefinition::getName);
    }

    @SuppressWarnings("unchecked")
    public <D extends RootOpenableDto, F extends DataGroupByDto<D>> DataGroupByDtoDefinition<D, F> getDefinition(String key) {
        return (DataGroupByDtoDefinition<D, F>) mappingByName.get(key);
    }

    @SuppressWarnings("unchecked")
    public <D extends RootOpenableDto> DataGroupByDtoDefinition<D, ?> getDefinition(Class<? extends DataGroupByDto<D>> key) {
        return (DataGroupByDtoDefinition<D, ?>) mapping.get(key);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <D extends RootOpenableDto> List<DataGroupByDtoDefinition<D, ?>> getDefinitions(Class<D> key) {
        return (List) mapping.values().stream().filter(d -> key.equals(d.getDataType())).collect(Collectors.toList());
    }

    public List<DataGroupByDtoDefinition<?, ?>> getDefinitions(BusinessModule module) {
        String prefix = "data" + Strings.capitalize(module.getName());
        return mapping.values().stream().filter(d -> d.getName().startsWith(prefix)).collect(Collectors.toList());
    }
}
