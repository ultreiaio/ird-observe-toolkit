package fr.ird.observe.spi.json.guava;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ArrayListMultimap;
import io.ultreia.java4all.util.json.JsonAdapter;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@AutoService(JsonAdapter.class)
public class ArrayListMultimapAdapter<K, V> extends MultimapAdapterSupport<K, V, ArrayListMultimap<K, V>> implements JsonAdapter {
    @Override
    public Class<?> type() {
        return ArrayListMultimap.class;
    }

    @Override
    protected ArrayListMultimap<K, V> createMultimap() {
        return ArrayListMultimap.create();
    }

}
