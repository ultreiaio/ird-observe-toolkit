package fr.ird.observe.spi.json;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.dto.reference.DtoReference;
import io.ultreia.java4all.http.HResponseError;
import io.ultreia.java4all.http.HResponseErrorAdapter;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import io.ultreia.java4all.util.json.adapters.DateAdapter;
import io.ultreia.java4all.util.json.adapters.TypedAdapter;

import java.util.function.Supplier;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DtoGsonSupplier implements Supplier<Gson> {
    private final boolean prettyPrint;
    private final boolean serializeNulls;
    private boolean disableHtmlEscaping;
    protected GsonBuilder gsonBuilder;
    private Gson gson;

    public DtoGsonSupplier() {
        this(false, false);
    }

    public DtoGsonSupplier(boolean prettyPrint) {
        this(prettyPrint, false);
    }

    public DtoGsonSupplier(boolean prettyPrint, boolean serializeNulls) {
        this.prettyPrint = prettyPrint;
        this.serializeNulls = serializeNulls;
    }

    public DtoGsonSupplier disableHtmlEscaping() {
        disableHtmlEscaping = true;
        return this;
    }

    @Override
    public Gson get() {
        if (gson == null) {
            gson = getGsonBuilder().create();
        }
        return gson;
    }

    protected GsonBuilder getGsonBuilder() {

        if (gsonBuilder == null) {

            // Assign only when initialization is finished to be thread-safe
            gsonBuilder = new GsonBuilder();

            if (prettyPrint) {
                gsonBuilder.setPrettyPrinting();
            }
            if (serializeNulls) {
                gsonBuilder.serializeNulls();
            }
            if (disableHtmlEscaping) {
                gsonBuilder.disableHtmlEscaping();
            }

            // Type adapters : base types
            gsonBuilder.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(java.sql.Time.class, new DateAdapter());
            gsonBuilder.registerTypeAdapter(java.sql.Date.class, new DateAdapter());

            gsonBuilder.registerTypeAdapter(DtoReference.class, new TypedAdapter());

            gsonBuilder.registerTypeAdapter(HResponseError.class, new HResponseErrorAdapter());

            JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);

            gsonBuilder.enableComplexMapKeySerialization();
        }
        return gsonBuilder;

    }
}
