package fr.ird.observe.spi.json.java4all;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonAdapter;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created at 01/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@AutoService(JsonAdapter.class)
public class DataMatrixAdapter implements JsonDeserializer<DataMatrix>, JsonSerializer<DataMatrix>, JsonAdapter {

    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static final String X = "x";
    private static final String Y = "y";
    private static final String ROWS = "rows";

    @Override
    public Class<?> type() {
        return DataMatrix.class;
    }

    @Override
    public DataMatrix deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject dataMatrixAsJsonObject = json.getAsJsonObject();

        DataMatrix result = new DataMatrix();
        result.setWidth(context.deserialize(dataMatrixAsJsonObject.get(WIDTH), int.class));
        result.setHeight(context.deserialize(dataMatrixAsJsonObject.get(HEIGHT), int.class));
        result.setX(context.deserialize(dataMatrixAsJsonObject.get(X), int.class));
        result.setY(context.deserialize(dataMatrixAsJsonObject.get(Y), int.class));
        JsonArray rows = dataMatrixAsJsonObject.getAsJsonArray(ROWS);
        Object[][] data = new Object[result.getHeight()][result.getWidth()];
        int index = 0;
        for (JsonElement rowElement : rows) {
            String[] deserialize = rowElement.getAsString().split("\\|\\|");
            Object[] row = new Object[deserialize.length];
            for (int i = 0; i < deserialize.length; i++) {
                String s = deserialize[i];
                row[i] = s.equals("$") ? null : s;
            }
            data[index++] = row;
        }
        result.setData(data);
        return result;
    }

    @Override
    public JsonElement serialize(DataMatrix src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.addProperty(WIDTH, src.getWidth());
        result.addProperty(HEIGHT, src.getHeight());
        result.addProperty(X, src.getX());
        result.addProperty(Y, src.getY());
        JsonArray rows = new JsonArray(src.getHeight());
        result.add(ROWS, rows);
        for (Object[] row : src.getData()) {
            rows.add(new JsonPrimitive(Arrays.stream(row).map(d -> d == null ? "$" : d.toString()).collect(Collectors.joining("||"))));
        }

        return result;
    }
}

