package fr.ird.observe.spi.navigation.model.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.navigation.model.MetaModel;
import fr.ird.observe.spi.navigation.model.MetaModelNode;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLink;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 31/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class IdProjectModelBuilder {

    public static IdProjectModel createIdModel(MetaModel metaModel) {
        return createIdNodeModels(metaModel, true);
    }

    public static IdProjectModel createEditModel(MetaModel metaModel) {
        return createIdNodeModels(metaModel, false);
    }

    public static IdProjectModel createSelectModel(MetaModel metaModel) {
        return createIdNodeModels(metaModel, true);
    }

    public static IdProjectModel createIdNodeModels(MetaModel metaModel, boolean includeEdit) {

        List<IdNodeModel> nodes = new LinkedList<>();
        List<IdModuleModel> modules = new LinkedList<>();

        IdNodeModel rootNode = new IdNodeModel(null, "Root", "Void", -1);
        nodes.add(rootNode);
        for (MetaModelNode metaModelNode : metaModel.getRoot()) {
            if (metaModelNode.isRootOpen()) {
                List<IdNodeModel> moduleResult = new LinkedList<>();
                IdNodeModel node = createNodeModel(metaModel, metaModelNode, moduleResult, includeEdit, rootNode);
                String propertyName = node.getClassName().replace(".common.", "");
                rootNode.getNodes().put(propertyName, node.getClassName());
                nodes.addAll(moduleResult);
                IdModuleModel moduleModel = new IdModuleModel(ProjectPackagesDefinition.extractFirstPackage(node.getClassName()), node.getClassName());
                for (IdNodeModel nodeModel : moduleResult) {
                    moduleModel.getNodes().put(ProjectPackagesDefinition.removeFirstPackage(nodeModel.getClassName()).replace(".", ""), nodeModel.getClassName());
                }
                modules.add(moduleModel);
            }
        }
        return new IdProjectModel(modules, nodes);
    }

    public static IdNodeModel createNodeModel(MetaModel metaModel, MetaModelNode metaModelNode, List<IdNodeModel> nodeModels, boolean includeEdit, IdNodeModel parentNode) {
        IdNodeModel result = new IdNodeModel(parentNode.getClassName(), ProjectPackagesDefinition.removeFirstPackage(metaModelNode.getDataType()), metaModelNode.getDataType(), metaModelNode.getLevel());
        nodeModels.add(result);
        for (MetaModelNodeLink child : metaModelNode.getChildren()) {
            MetaModelNode childNode = metaModel.getNode(child);
            if (childNode.isOpen() || (includeEdit && childNode.isEdit())) {
                IdNodeModel childResult = createNodeModel(metaModel, childNode, nodeModels, includeEdit, result);
                result.getNodes().put(child.getPropertyName(), childResult.getClassName());
            }
        }
        return result;
    }

}
