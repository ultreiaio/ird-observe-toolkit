package fr.ird.observe.spi.navigation.parent;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.ToolkitParentIdDtoBean;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * To get from any id his parent.
 * <p>
 * Created on 09/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public interface ToParentIdProvider {

    static List<ToolkitParentIdDtoBean> getPathFromRoot(ToParentIdProvider provider, ToolkitIdDtoBean id) {
        List<ToolkitParentIdDtoBean> pathToRoot = getPathToRoot(provider, id);
        Collections.reverse(pathToRoot);
        return pathToRoot;
    }

    static List<ToolkitParentIdDtoBean> getPathToRoot(ToParentIdProvider provider, ToolkitIdDtoBean id) {
        List<ToolkitParentIdDtoBean> paths = new LinkedList<>();
        ToolkitParentIdDtoBean parent;
        while ((parent = provider.getParent(id)) != null) {
            paths.add(parent);
            id = parent;
        }
        return paths;
    }

    ToolkitParentIdDtoBean getParent(ToolkitIdDtoBean id);

    default List<ToolkitParentIdDtoBean> getPathToRoot(ToolkitIdDtoBean id) {
        return ToParentIdProvider.getPathToRoot(this, id);
    }

    default List<ToolkitParentIdDtoBean> getPathFromRoot(ToolkitIdDtoBean id) {
        return ToParentIdProvider.getPathFromRoot(this, id);
    }

}
