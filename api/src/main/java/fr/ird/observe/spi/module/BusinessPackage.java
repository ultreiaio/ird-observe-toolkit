package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;

import java.util.Objects;
import java.util.Set;

/**
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class BusinessPackage<D extends BusinessDto> {

    private final int priority;
    private final String packageName;
    private final Set<Class<? extends D>> types;

    public BusinessPackage(int priority, String packageName, Set<Class<? extends D>> types) {
        this.priority = priority;
        this.packageName = Objects.requireNonNull(packageName);
        this.types = Objects.requireNonNull(types);
    }

    public int getPriority() {
        return priority;
    }

    public String getPackageName() {
        return packageName;
    }

    public Set<Class<? extends D>> getTypes() {
        return types;
    }
}
