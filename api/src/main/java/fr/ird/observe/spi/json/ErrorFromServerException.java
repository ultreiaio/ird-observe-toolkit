package fr.ird.observe.spi.json;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * A dummy error to box any error exception, we can use when deserialize exception in client...
 * <p>
 * Not perfect but do the job
 * <p>
 * Created on 27/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since
 */
public class ErrorFromServerException extends Exception {
    public ErrorFromServerException(String message) {
        super(message);
    }
}
