package fr.ird.observe.spi;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;

/**
 * Represents some packages used in a project.
 *
 * <p>Mainly the dto package, entity package Created on 25/07/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class ProjectPackagesDefinition {

    private static final String LOCATION = "project-packages.properties";

    static {
        I18n.n("application.config.option.build.date");
        I18n.n("application.config.option.build.number");
        I18n.n("application.config.option.build.version");
        I18n.n("application.config.option.build.version.major");
        I18n.n("application.config.option.build.version.persistence");
    }

    private final String dtoRootPackage;
    private final String dtoReferentialPackage;
    private final String dtoDataPackage;
    private final String entityRootPackage;
    private final String entityReferentialPackage;
    private final String entityDataPackage;
    private final String serviceRootPackage;
    private final String serviceReferentialPackage;
    private final String serviceDataPackage;
    private final String idPrefix = "fr.ird.";

    private final String[] dtoPackageCandidates;
    private final String name;

    public static ProjectPackagesDefinition of(ClassLoader classLoader) {
        return new ProjectPackagesDefinition(getResource(classLoader));
    }

    public static URL getResource(ClassLoader classLoader) {
        return classLoader.getResource(LOCATION);
    }

    public static String cleanType(String fqn) {
        if (fqn.endsWith("Dto")) {
            return fqn.substring(0, fqn.length() - 3);
        }
        if (fqn.endsWith("Reference")) {
            return fqn.substring(0, fqn.length() - 9);
        }
        // for entity, hibernate proxy gives use some class name like Entity_xxx
        int index = fqn.indexOf("_");
        if (index > -1) {
            fqn = fqn.substring(0, index);
        } else {
            // for entity, hibernate
            index = fqn.indexOf("@");
            if (index > -1) {
                fqn = fqn.substring(0, index);
            }
        }
        if (fqn.endsWith("Impl")) {
            return fqn.substring(0, fqn.length() - 4);
        }
        return fqn;
    }

    public String dtoToEntity(String dtoFqn) {
        String result = cleanType(dtoFqn);
        result = result.replace(getDtoRootPackage(), getEntityRootPackage());
        return result;
    }

    public static Pair<String, String> gav(String packageName) {
        String[] gav = removeFirstPackage(packageName).split("\\.");
        String moduleName = gav[0];
        String subModuleName = gav.length == 1 ? moduleName : gav[1];
        return Pair.of(moduleName, subModuleName);
    }

    public static String extractFirstPackage(String packageName) {
        return packageName.substring(0, packageName.indexOf("."));
    }

    public static String removeFirstPackage(String packageName) {
        return packageName.substring(packageName.indexOf(".") + 1);
    }

    public static String getSimpleName(String className) {
        int endIndex = className.lastIndexOf(".");
        return endIndex == -1 ? className : className.substring(endIndex + 1);
    }

    public static String getPackageName(String className) {
        int endIndex = className.lastIndexOf(".");
        return endIndex == -1 ? "" : className.substring(0, endIndex);
    }

    public ProjectPackagesDefinition(URL resource) {
        try (InputStream inputStream = Objects.requireNonNull(resource).openStream()) {
            Properties p = new Properties();
            p.load(inputStream);
            this.name = p.getProperty("name");
            this.dtoRootPackage = p.getProperty("dtoRootPackage");
            this.dtoReferentialPackage = p.getProperty("dtoReferentialPackage", this.dtoRootPackage + ".referential");
            this.dtoDataPackage = p.getProperty("dtoDataPackage", this.dtoRootPackage + ".data");
            this.entityRootPackage = p.getProperty("entityRootPackage");
            this.entityReferentialPackage = p.getProperty("entityReferentialPackage", this.entityRootPackage + ".referential");
            this.entityDataPackage = p.getProperty("entityDataPackage", this.entityRootPackage + ".data");
            this.serviceRootPackage = p.getProperty("serviceRootPackage");
            this.serviceReferentialPackage = p.getProperty("serviceReferentialPackage", this.serviceRootPackage + ".referential");
            this.serviceDataPackage = p.getProperty("serviceDataPackage", this.serviceRootPackage + ".data");
            this.dtoPackageCandidates = p.getProperty("dtoPackageCandidates").split("\\s*,\\s*");

        } catch (IOException e) {
            throw new IllegalStateException("Can't load " + resource);
        }
    }

    public String getName() {
        return name;
    }

    public String getDtoRootPackage() {
        return dtoRootPackage;
    }

    public String getDtoReferentialPackage() {
        return dtoReferentialPackage;
    }

    public String getDtoDataPackage() {
        return dtoDataPackage;
    }

    public String getEntityRootPackage() {
        return entityRootPackage;
    }

    public String getEntityReferentialPackage() {
        return entityReferentialPackage;
    }

    public String getEntityDataPackage() {
        return entityDataPackage;
    }

    public String getServiceRootPackage() {
        return serviceRootPackage;
    }

    public String getServiceReferentialPackage() {
        return serviceReferentialPackage;
    }

    public String getServiceDataPackage() {
        return serviceDataPackage;
    }

    public String[] getDtoPackageCandidates() {
        return dtoPackageCandidates;
    }

    public boolean isInModule(String moduleName, String fqn) {
        return fqn.startsWith(getDtoReferentialPackage() + "." + moduleName) || fqn.startsWith(getDtoDataPackage() + "." + moduleName);
    }

    public boolean isReferential(Class<?> type) {
        return isReferentialFromPackageName(type.getPackage().getName());
    }

    public boolean isReferentialFromPackageName(String packageName) {
        return packageName.startsWith(getDtoReferentialPackage());
    }

    public boolean isReferentialFromEntityPackageName(String packageName) {
        return packageName.startsWith(getEntityReferentialPackage());
    }

    public boolean isData(Class<?> type) {
        return isDataFromPackageName(type.getPackage().getName());
    }

    public boolean isDataFromPackageName(String packageName) {
        return packageName.startsWith(getDtoDataPackage());
    }

    public String getRelativeDtoPackage(String dtoClassName) {
        return Strings.removeStart(dtoClassName, getDtoRootPackage());
    }

    public String getRelativeEntityPackage(String entityClassName) {
        return Strings.removeStart(entityClassName, getEntityRootPackage());
    }

    public String getRelativeEntityPackageFromId(String id) {
        String entityClassName = getClassFromId(id);
        return Strings.removeStart(entityClassName, getEntityRootPackage());
    }

    public String getRelativeServicePackage(String serviceClassName) {
        return Strings.removeStart(serviceClassName, getServiceRootPackage());
    }

    public String getClassFromId(String id) {
        return getEntityRootPackage() + Strings.removeStart(id.split("#")[0], idPrefix);
    }
}
