package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.FormDefinition;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Defines the mapping between any dto type and his associated form definition.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5
 */
public abstract class DtoToFormDtoMapping extends ImmutableClassMapping<BusinessDto, BusinessDto> {

    private final Map<Class<? extends BusinessDto>, FormDefinition<? extends BusinessDto>> mapping;

    private static Map<Class<? extends BusinessDto>, Class<? extends BusinessDto>> mapping(Map<Class<? extends BusinessDto>, FormDefinition<? extends BusinessDto>> mapping) {
        Map<Class<? extends BusinessDto>, Class<? extends BusinessDto>> builder = new LinkedHashMap<>(mapping.size());
        mapping.forEach((key, value) -> builder.put(key, value.getType()));
        return Collections.unmodifiableMap(builder);
    }

    protected DtoToFormDtoMapping(Map<Class<? extends BusinessDto>, FormDefinition<? extends BusinessDto>> mapping) {
        super(mapping(mapping));
        this.mapping = mapping;
    }

    @SuppressWarnings("unchecked")
    public <DD extends BusinessDto> FormDefinition<DD> getForm(Class<? extends BusinessDto> key) {
        return (FormDefinition<DD>) mapping.get(key);
    }
}
