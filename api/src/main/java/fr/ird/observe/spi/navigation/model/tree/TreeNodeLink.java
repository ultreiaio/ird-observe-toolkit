package fr.ird.observe.spi.navigation.model.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import io.ultreia.java4all.lang.Strings;

/**
 * Created on 02/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class TreeNodeLink {

    private final String propertyName;
    private final String targetClassName;
    private final MetaModelNodeLinkMultiplicity multiplicity;

    public TreeNodeLink(String propertyName, String targetClassName, MetaModelNodeLinkMultiplicity multiplicity) {
        this.propertyName = propertyName;
        this.targetClassName = targetClassName;
        this.multiplicity = multiplicity;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public MetaModelNodeLinkMultiplicity getMultiplicity() {
        return multiplicity;
    }

    public String getTargetClassName() {
        return targetClassName;
    }


    public String getCapitalizePropertyName() {
        return Strings.capitalize(getPropertyName());
    }
}
