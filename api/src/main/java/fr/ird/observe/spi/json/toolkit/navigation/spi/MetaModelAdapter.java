package fr.ird.observe.spi.json.toolkit.navigation.spi;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import fr.ird.observe.spi.navigation.model.MetaModel;
import fr.ird.observe.spi.navigation.model.MetaModelNode;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLink;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import fr.ird.observe.spi.navigation.model.MetaModelNodeType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class MetaModelAdapter extends TypeAdapter<MetaModel> {

    public static final String GROUP_BY = "groupBy";
    public static final String CHILDREN = "children";

    @Override
    public MetaModel read(JsonReader in) throws IOException {

        List<MetaModelNode> nodes = new ArrayList<>();
        in.beginObject();
        while (in.hasNext()) {
            String[] parts = explode(in.nextName());
            MetaModelNode node = new MetaModelNode(parts[0], MetaModelNodeType.valueOf(parts[1]), parts[2]);
            nodes.add(node);

            in.beginObject();
            while (in.peek() != JsonToken.END_OBJECT) {
                String nextName = in.nextName();
                switch (nextName) {
                    case GROUP_BY:
                        in.beginArray();
                        while (in.peek() != JsonToken.END_ARRAY) {
                            parts = explode(in.nextString());
                            node.getGroupBy().put(parts[0], parts[1]);
                        }
                        in.endArray();
                        break;
                    case CHILDREN:
                        in.beginArray();
                        while (in.peek() != JsonToken.END_ARRAY) {
                            parts = explode(in.nextString());
                            MetaModelNodeLink link = new MetaModelNodeLink(parts[0], MetaModelNodeLinkMultiplicity.valueOf(parts[1]), parts[2]);
                            node.getChildren().add(link);
                            if (parts.length == 5) {
                                nodes.add(new MetaModelNode(link.getTarget(), MetaModelNodeType.valueOf(parts[3]), parts[4]));
                            }
                        }
                        in.endArray();
                        break;
                    default:
                        throw new IllegalStateException("Can't read node property: " + nextName);
                }
            }
            in.endObject();
        }
        in.endObject();
        return new MetaModel(Maps.uniqueIndex(nodes, MetaModelNode::getKey));
    }

    @Override
    public void write(JsonWriter out, MetaModel src) throws IOException {
        out.beginObject();
        Set<String> skipNodes = new TreeSet<>();
        for (Map.Entry<String, MetaModelNode> entry : src.getNodes().entrySet()) {
            String nodeKey = entry.getKey();
            if (!skipNodes.add(nodeKey)) {
                continue;
            }
            MetaModelNode stateValue = entry.getValue();
            out.name(compact(nodeKey, stateValue.getNodeType(), stateValue.getDataType()));
            out.beginObject();
            if (stateValue.withGroupBy()) {
                out.name(GROUP_BY);
                out.beginArray();
                for (Map.Entry<String, String> s : stateValue.getGroupBy().entrySet()) {
                    out.value(compact(s.getKey(), s.getValue()));
                }
                out.endArray();
            }
            if (stateValue.withChildren()) {
                out.name(CHILDREN);
                out.beginArray();
                for (MetaModelNodeLink s : stateValue.getChildren()) {
                    MetaModelNode targetNode = src.getNode(s.getTarget());
                    if (targetNode.leaf()) {
                        out.value(compactWithNode(s, targetNode));
                        skipNodes.add(targetNode.getKey());
                    } else {
                        out.value(compact(s));
                    }
                }
                out.endArray();
            }
            out.endObject();
        }
        out.endObject();
    }

    protected String[] explode(String value) {
        return value.split("\\s*→\\s*");
    }

    protected String compact(Object... values) {
        return Arrays.stream(values).map(Object::toString).collect(Collectors.joining(" → "));
    }

    protected String compact(MetaModelNodeLink s) {
        return compact(s.getPropertyName(), s.getMultiplicity(), s.getTarget());
    }

    protected String compactWithNode(MetaModelNodeLink s, MetaModelNode targetNode) {
        return compact(s.getPropertyName(), s.getMultiplicity(), s.getTarget(), targetNode.getNodeType(), targetNode.getDataType());
    }
}
