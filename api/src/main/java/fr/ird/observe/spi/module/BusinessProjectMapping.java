package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.mapping.DataGroupByDtoToDefinitionMapping;
import fr.ird.observe.spi.mapping.DtoToFormDtoMapping;
import fr.ird.observe.spi.mapping.DtoToMainDtoClassMapping;
import fr.ird.observe.spi.mapping.DtoToReferenceDtoMapping;
import fr.ird.observe.spi.mapping.ReferenceDtoToDtoClassMapping;

import java.lang.reflect.Modifier;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 05/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class BusinessProjectMapping {

    private final DtoToReferenceDtoMapping dtoToReferenceDtoClassMapping;
    private final ReferenceDtoToDtoClassMapping referenceDtoToDtoClassMapping;
    private final DtoToFormDtoMapping dtoToFormDtoMapping;
    private final DataGroupByDtoToDefinitionMapping dataGroupByDtoToDefinitionMapping;
    private final DtoToMainDtoClassMapping dtoToMainDtoClassMapping;
    private final Set<Class<? extends DataDto>> mainDataClasses;

    public BusinessProjectMapping(DtoToReferenceDtoMapping dtoToReferenceDtoClassMapping,
                                  ReferenceDtoToDtoClassMapping referenceDtoToDtoClassMapping,
                                  DtoToFormDtoMapping dtoToFormDtoMapping,
                                  DataGroupByDtoToDefinitionMapping dataGroupByDtoToDefinitionMapping,
                                  DtoToMainDtoClassMapping dtoToMainDtoClassMapping) {
        this.dtoToReferenceDtoClassMapping = Objects.requireNonNull(dtoToReferenceDtoClassMapping);
        this.referenceDtoToDtoClassMapping = Objects.requireNonNull(referenceDtoToDtoClassMapping);
        this.dtoToFormDtoMapping = Objects.requireNonNull(dtoToFormDtoMapping);
        this.dataGroupByDtoToDefinitionMapping = Objects.requireNonNull(dataGroupByDtoToDefinitionMapping);
        this.dtoToMainDtoClassMapping = Objects.requireNonNull(dtoToMainDtoClassMapping);
        this.mainDataClasses = new LinkedHashSet<>();
        for (Class<? extends BusinessDto> dtoType : this.dtoToMainDtoClassMapping.values()) {
            if (DataDto.class.isAssignableFrom(dtoType)) {
                @SuppressWarnings("unchecked") Class<? extends DataDto> type = (Class<? extends DataDto>) dtoType;
                mainDataClasses.add(type);
            }
        }
    }

    public DtoToReferenceDtoMapping getDtoToReferenceDtoClassMapping() {
        return dtoToReferenceDtoClassMapping;
    }

    public ReferenceDtoToDtoClassMapping getReferenceDtoToDtoClassMapping() {
        return referenceDtoToDtoClassMapping;
    }

    public DtoToFormDtoMapping getDtoToFormDtoMapping() {
        return dtoToFormDtoMapping;
    }

    public DataGroupByDtoToDefinitionMapping getDataGroupByDtoToDefinitionMapping() {
        return dataGroupByDtoToDefinitionMapping;
    }

    public DtoToMainDtoClassMapping getDtoToMainDtoClassMapping() {
        return dtoToMainDtoClassMapping;
    }

    public Set<Class<? extends DataDto>> getMainDataClasses() {
        return mainDataClasses;
    }

    public <D extends BusinessDto, S extends BusinessDto> Class<S> getMainDtoType(Class<D> dtoType) {
        return getDtoToMainDtoClassMapping().get(dtoType);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <D extends BusinessDto, R extends DtoReference> Class<R> getReferenceType(Class<D> dtoType) {
        if (Modifier.isAbstract(dtoType.getModifiers())) {
            if (ReferentialDto.class.isAssignableFrom(dtoType)) {
                return (Class) ReferentialDtoReference.class;
            }
            if (DataDto.class.equals(dtoType)) {
                return (Class) DataDtoReference.class;
            }
        }
        return getDtoToReferenceDtoClassMapping().get(dtoType);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <D extends BusinessDto, R extends DtoReference> Class<D> getDtoType(Class<R> dtoType) {
        if (Modifier.isAbstract(dtoType.getModifiers())) {
            if (ReferentialDtoReference.class.isAssignableFrom(dtoType)) {
                return (Class) ReferentialDto.class;
            }
            if (DataDtoReference.class.equals(dtoType)) {
                return (Class) DataDto.class;
            }
        }
        return getReferenceDtoToDtoClassMapping().get(dtoType);
    }

    public Set<Class<? extends BusinessDto>> getSubDtoTypes(Class<? extends BusinessDto> mainDtoType) {
        Set<Class<? extends BusinessDto>> builder = new LinkedHashSet<>();
        for (Class<? extends BusinessDto> dtoType : getDtoToMainDtoClassMapping().keySet()) {
            if (Objects.equals(mainDtoType, getMainDtoType(dtoType))) {
                builder.add(dtoType);
            }
        }
        return builder;
    }
}
