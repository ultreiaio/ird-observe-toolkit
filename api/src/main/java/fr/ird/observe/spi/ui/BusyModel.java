package fr.ird.observe.spi.ui;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.0
 */
@GenerateJavaBeanDefinition
public class BusyModel extends AbstractJavaBean {
    private static final TimeLog timeLog = new TimeLog(BusyModel.class, 500, 1000);
    private static final Logger log = LogManager.getLogger(BusyModel.class);

    static class BusyTask {
        private final String taskName;
        private final int rank;
        private final long startTime;

        BusyTask(String taskName, int rank) {
            this.taskName = taskName;
            this.rank = rank;
            this.startTime = TimeLog.getTime();
        }

        public int getRank() {
            return rank;
        }

        public String getTaskName() {
            return taskName;
        }

        public long getStartTime() {
            return startTime;
        }

    }

    public static final String BUSY_PROPERTY_NAME = "busy";
    private final Deque<BusyTask> tasks;

    public BusyModel() {
        tasks = new ArrayDeque<>();
    }

    public synchronized void addTask(String taskName) {
        boolean oldValue = isBusy();
        BusyTask task = new BusyTask(taskName, tasks.size() + 1);
        log.info(I18n.t("observe.ui.busy.start.task", task.getRank(), taskName));
        tasks.add(task);
        fireBusyStateChanged(oldValue, task, null);
    }

    public synchronized void popTask() {
        boolean oldValue = isBusy();
        BusyTask task = tasks.removeLast();
        String taskName = task.getTaskName();
        long startTime = task.getStartTime();
        String delay = Strings.convertTime(TimeLog.getTime() - startTime);
        log.info(I18n.t("observe.ui.busy.end.task", task.getRank(), delay, taskName));
        timeLog.log(startTime, "task done", taskName);
        fireBusyStateChanged(oldValue, task, delay);
    }

    public boolean isBusy() {
        return !tasks.isEmpty();
    }

    void fireBusyStateChanged(boolean oldValue, BusyTask task, String delay) {
        boolean busy = isBusy();
        if (oldValue != busy) {
            String taskName = task.getTaskName();
            if (busy) {
                log.info(I18n.t("observe.ui.busy.start.block", taskName));
            } else {
                long startTime = task.getStartTime();
                log.info(I18n.t("observe.ui.busy.end.block", delay, taskName));
                timeLog.log(startTime, "unblock", taskName);
            }
            firePropertyChange(BUSY_PROPERTY_NAME, oldValue, busy);
        }
    }
}
