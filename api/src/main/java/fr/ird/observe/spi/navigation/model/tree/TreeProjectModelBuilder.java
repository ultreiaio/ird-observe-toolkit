package fr.ird.observe.spi.navigation.model.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModel;
import fr.ird.observe.spi.navigation.model.MetaModelNode;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLink;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import io.ultreia.java4all.lang.Strings;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 01/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class TreeProjectModelBuilder {

    public static TreeProjectModel createNavigationModel(MetaModel metaModel) {
        return createTreeNodeModels(metaModel, true);
    }

    public static TreeProjectModel createSelectionModel(MetaModel metaModel) {
        return createTreeNodeModels(metaModel, false);
    }

    public static TreeProjectModel createTreeNodeModels(MetaModel metaModel, boolean forNavigation) {
        List<TreeNodeModel> nodes = new LinkedList<>();
        TreeNodeModel rootNode = TreeNodeModel.newRoot();
        nodes.add(rootNode);
        for (MetaModelNode metaModelNode : metaModel.getRoot()) {
            if (metaModelNode.isRootOpen()) {
                TreeNodeModel groupByNodeSupport = TreeNodeModel.newDataGroupBySupport(rootNode, metaModelNode);
                List<TreeNodeModel> groupByNodes = createDataGroupByNodes(metaModelNode, rootNode, groupByNodeSupport);
                nodes.addAll(groupByNodes);
                List<TreeNodeModel> parentNodes = new LinkedList<>();
                parentNodes.add(rootNode);
                parentNodes.addAll(groupByNodes);
                TreeNodeModel node = createRootDataNode(metaModel, metaModelNode, forNavigation, nodes, parentNodes);
                // add it to root node (for none group by requests)
                rootNode.getChildren().add(new TreeNodeLink(toCamelCase(node.getType()), node.getClassName(), MetaModelNodeLinkMultiplicity.MANY));
                // add it to all his group by parent (for group by requests)
                groupByNodes.forEach(model -> model.getChildren().add(new TreeNodeLink("children", node.getClassName(), MetaModelNodeLinkMultiplicity.MANY)));
            } else if (metaModelNode.isReferentialPackage()) {
                List<TreeNodeModel> referentialNodes = createReferentialNodes(metaModel, metaModelNode, rootNode);
                nodes.addAll(referentialNodes);
            }
        }
        return new TreeProjectModel(nodes);
    }

    static List<TreeNodeModel> createDataGroupByNodes(MetaModelNode metaModelNode, TreeNodeModel parentNode, TreeNodeModel abstractNode) {
        List<TreeNodeModel> nodes = new LinkedList<>();
        for (Map.Entry<String, String> entry : metaModelNode.getGroupBy().entrySet()) {
            String propertyName = entry.getKey();
            String groupByType = entry.getValue();
            TreeNodeModel child = TreeNodeModel.newDataGroupBy(parentNode, abstractNode, propertyName);
            nodes.add(child);
        }
        return nodes;
    }

    static TreeNodeModel createRootDataNode(MetaModel metaModel, MetaModelNode metaModelNode, boolean forNavigation, List<TreeNodeModel> nodes, List<TreeNodeModel> groupByNodes) {
        TreeNodeModel node = TreeNodeModel.newRootData(groupByNodes, metaModelNode);
        nodes.add(node);
        if (forNavigation) {
            for (MetaModelNodeLink link : metaModelNode.getChildren()) {
                MetaModelNode childNode = metaModel.getNode(link);
                if (childNode.isOpen()) {
                    // add intermediate openList node
                    TreeNodeModel listNode = TreeNodeModel.newOpenDataList(node, childNode, new MetaModelNodeLink(link.getPropertyName(), MetaModelNodeLinkMultiplicity.ONE, link.getTarget()));
                    nodes.add(listNode);
                    addDataNode(metaModel, childNode, listNode, nodes, new MetaModelNodeLink("children", MetaModelNodeLinkMultiplicity.MANY, childNode.getDataType()));
                    continue;
                }
                addDataNode(metaModel, childNode, node, nodes, link);
            }
        }
        return node;
    }

    static void addDataNode(MetaModel metaModel, MetaModelNode metaModelNode, TreeNodeModel parentNode, List<TreeNodeModel> nodes, MetaModelNodeLink parentLink) {
        TreeNodeModel node = TreeNodeModel.newData(parentNode, metaModelNode, parentLink);
        nodes.add(node);
        for (MetaModelNodeLink link : metaModelNode.getChildren()) {
            MetaModelNode childNode = metaModel.getNode(link);
            if (childNode.isOpen()) {
                // add intermediate openList node
                TreeNodeModel listNode = TreeNodeModel.newOpenDataList(node, childNode, new MetaModelNodeLink(link.getPropertyName(), MetaModelNodeLinkMultiplicity.ONE, link.getTarget()));
                nodes.add(listNode);
                addDataNode(metaModel, childNode, listNode, nodes, new MetaModelNodeLink("children", MetaModelNodeLinkMultiplicity.MANY, childNode.getDataType()));
                continue;
            }
            addDataNode(metaModel, childNode, node, nodes, link);
        }
    }

    static List<TreeNodeModel> createReferentialNodes(MetaModel metaModel, MetaModelNode metaModelNode, TreeNodeModel parentNode) {
        TreeNodeModel packageNode = TreeNodeModel.newReferentialPackageNode(parentNode, metaModelNode);
        List<TreeNodeModel> nodes = new LinkedList<>();
        nodes.add(packageNode);
        for (MetaModelNodeLink link : metaModelNode.getChildren()) {
            MetaModelNode node = metaModel.getNode(link);
            TreeNodeModel childNode = TreeNodeModel.newReferentialPackageType(packageNode, node, link);
            nodes.add(childNode);
        }
        return nodes;
    }

    public static String toCamelCase(String nodeClassName) {
        StringBuilder result = null;
        for (String s : nodeClassName.split("\\.")) {
            if (result == null) {
                result = new StringBuilder(s);
                continue;
            }
            result.append(Strings.capitalize(s));
        }
        return Objects.requireNonNull(result).toString();
    }
}
