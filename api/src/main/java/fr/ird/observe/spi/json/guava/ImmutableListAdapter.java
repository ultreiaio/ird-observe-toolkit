package fr.ird.observe.spi.json.guava;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@AutoService(JsonAdapter.class)
public class ImmutableListAdapter<V> implements JsonDeserializer<ImmutableList<V>>, JsonAdapter {
    public static Type listOf(Type subType) {
        return TypeToken.getParameterized(List.class, subType).getType();
    }

    @Override
    public Class<?> type() {
        return ImmutableList.class;
    }

    @Override
    public ImmutableList<V> deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type actualTypeArgument = actualTypeArguments[0];
        Type type2 = listOf(actualTypeArgument);
        List<V> list = context.deserialize(json, type2);
        return ImmutableList.copyOf(list);
    }
}
