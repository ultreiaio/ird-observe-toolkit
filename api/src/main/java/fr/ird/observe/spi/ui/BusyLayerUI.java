package fr.ird.observe.spi.ui;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.RootPaneContainer;
import javax.swing.UIManager;
import javax.swing.plaf.LayerUI;
import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Closeable;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.0
 */
public class BusyLayerUI<U extends Component> extends LayerUI<U> implements Closeable, PropertyChangeListener {

    private final RootPaneContainer rootPanecontainer;
    private final Container container;
    private final BusyModel model;
    private final Color busyColor;
    private transient Color color;

    public static void create(RootPaneContainer container, BusyModel busyModel) {
        Container contentPane = container.getRootPane().getContentPane();
        container.setContentPane(new JLayer<>(contentPane, new BusyLayerUI<>(container, busyModel)));
    }

    public static void close(RootPaneContainer container) {
        Component glassPane = container.getContentPane();
        if (glassPane instanceof JLayer) {
            LayerUI<?> component = ((JLayer<?>) glassPane).getUI();
            if (component instanceof BusyLayerUI) {
                ((BusyLayerUI<?>) component).close();
            }
        }
    }

    public BusyLayerUI(RootPaneContainer rootPanecontainer, BusyModel model) {
        this.rootPanecontainer = Objects.requireNonNull(rootPanecontainer);
        this.container = Objects.requireNonNull(rootPanecontainer.getRootPane());
        this.model = Objects.requireNonNull(model);
        this.busyColor = UIManager.getColor("BlockingLayerUI.busyColor");
        this.model.addPropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, this);
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        // enable all input events (and block them if necessary)
        ((JLayer<?>) c).setLayerEventMask(
                AWTEvent.KEY_EVENT_MASK |
                        AWTEvent.MOUSE_WHEEL_EVENT_MASK |
                        AWTEvent.MOUSE_MOTION_EVENT_MASK |
                        AWTEvent.MOUSE_EVENT_MASK |
                        AWTEvent.INPUT_METHOD_EVENT_MASK);
    }

    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
        // JLayer must be returned to its initial state
        ((JLayer<?>) c).setLayerEventMask(0);
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);

        if (isBusy()) {
            g.setColor(color);
            Graphics2D g2 = (Graphics2D) g;
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .2f));
            g.fillRect(0, 0, c.getWidth(), c.getHeight());
        }
    }

    @Override
    public void eventDispatched(AWTEvent e, JLayer<? extends U> l) {
        if (color != null && e instanceof InputEvent) {
            // busy mode
            ((InputEvent) e).consume();
        }
    }

    @Override
    public void close() {
        model.removePropertyChangeListener(BusyModel.BUSY_PROPERTY_NAME, this);
    }

    public Container getContainer() {
        return container;
    }

    public boolean isBusy() {
        return color != null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (BusyModel.BUSY_PROPERTY_NAME.equals(evt.getPropertyName())) {
            boolean wasBusy = (boolean) evt.getOldValue();
            boolean isBusy = (boolean) evt.getNewValue();
            if (wasBusy == isBusy) {
                return;
            }
            color = isBusy ? busyColor : null;
            if (isBusy) {
                container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            } else {
                container.setCursor(Cursor.getDefaultCursor());
            }
            rootPanecontainer.getRootPane().repaint();
        }
    }
}
