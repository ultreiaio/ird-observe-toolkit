package fr.ird.observe.spi.json.toolkit.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.selection.SelectionTreeNodeBean;

/**
 * Created on 17/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public final class DefaultStates {
    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class LastUpdateDateState extends ToolkitTreeNodeStateAdapter {
        public LastUpdateDateState() {
            super(ToolkitTreeNodeBean.STATE_LAST_UPDATE_DATE);
        }
    }

    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class DataTypeState extends ToolkitTreeNodeStateAdapter {
        public DataTypeState() {
            super(ToolkitTreeNodeBean.STATE_DATA_TYPE);
        }
    }

    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class ReferentialTypeDateState extends ToolkitTreeNodeStateAdapter {
        public ReferentialTypeDateState() {
            super(ToolkitTreeNodeBean.STATE_REFERENTIAL_TYPE);
        }
    }

    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class ContentState extends ToolkitTreeNodeStateAdapter {
        public ContentState() {
            super(ToolkitTreeNodeBean.STATE_CONTENT);
        }
    }

    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class ReferencesState extends ToolkitTreeNodeStateAdapter {
        public ReferencesState() {
            super(ToolkitTreeNodeBean.STATE_REFERENCES);
        }
    }

    @AutoService(ToolkitTreeNodeStateAdapter.class)
    public static class SelectionStateState extends ToolkitTreeNodeStateAdapter {
        public SelectionStateState() {
            super(SelectionTreeNodeBean.STATE_SELECTION_STATE);
        }
    }

    private DefaultStates() {
    }
}
