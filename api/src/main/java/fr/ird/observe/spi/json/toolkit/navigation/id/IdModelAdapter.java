package fr.ird.observe.spi.json.toolkit.navigation.id;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.navigation.id.IdModel;
import fr.ird.observe.navigation.id.IdNode;
import io.ultreia.java4all.util.json.JsonHierarchicAdapter;
import io.ultreia.java4all.lang.Objects2;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by tchemit on 28/05/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(JsonHierarchicAdapter.class)
public class IdModelAdapter implements JsonSerializer<IdModel>, JsonDeserializer<IdModel>, JsonHierarchicAdapter {

    static class DtoModelNavigationModelStr {

        private final Class<? extends IdModel> type;
        private final Map<Class<? extends IdNode<?>>, String> ids;

        DtoModelNavigationModelStr(Class<? extends IdModel> type, Map<Class<? extends IdNode<?>>, String> ids) {
            this.type = type;
            this.ids = ids;
        }

        public Class<? extends IdModel> getType() {
            return type;
        }

        public Map<Class<? extends IdNode<?>>, String> getIds() {
            return ids;
        }
    }

    @Override
    public Class<?> type() {
        return IdModel.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public JsonElement serialize(IdModel src, Type typeOfSrc, JsonSerializationContext context) {
        Map<Class<? extends IdNode<?>>, String> builder = new LinkedHashMap<>();
        for (IdNode<?> node : src.getNodesWithIds()) {
            builder.put((Class<? extends IdNode<?>>) node.getClass(), node.getId());
        }
        return context.serialize(new DtoModelNavigationModelStr(src.getClass(), builder));
    }

    @Override
    public IdModel deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        DtoModelNavigationModelStr deserialize = context.deserialize(json, DtoModelNavigationModelStr.class);
        Class<? extends IdModel> modelType = deserialize.getType();
        IdModel result = Objects2.newInstance(modelType);
        for (Map.Entry<Class<? extends IdNode<?>>, String> entry : deserialize.getIds().entrySet()) {
            IdNode<?> model = result.getNode(entry.getKey());
            model.setId(entry.getValue());
        }
        return result;
    }
}
