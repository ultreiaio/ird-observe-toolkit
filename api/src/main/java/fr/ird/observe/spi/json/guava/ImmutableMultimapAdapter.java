package fr.ird.observe.spi.json.guava;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
@AutoService(JsonAdapter.class)
public class ImmutableMultimapAdapter<K, V> implements JsonSerializer<ImmutableMultimap<K, V>>, JsonDeserializer<ImmutableMultimap<K, V>>, JsonAdapter {
    @Override
    public Class<?> type() {
        return ImmutableMultimap.class;
    }

    @Override
    public JsonElement serialize(ImmutableMultimap<K, V> src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.asMap());
    }

    @Override
    public ImmutableMultimap<K, V> deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        ImmutableMultimap.Builder<K, V> builder = ImmutableSetMultimap.builder();

        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type keyType = actualTypeArguments[0];
        Type valueType = actualTypeArguments[1];

        Type type2 = MultimapAdapterSupport.multimapOf(TypeToken.of(keyType), TypeToken.of(valueType)).getType();
        Map<K, List<V>> map = context.deserialize(json, type2);

        for (K key : map.keySet()) {
            List<V> multimapValues = map.get(key);
            for (V multimapValue : multimapValues) {
                builder.put(key, multimapValue);
            }
        }

        return builder.build();

    }

}
