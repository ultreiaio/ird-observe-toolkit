package fr.ird.observe.spi.navigation.model;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public enum MetaModelNodeType {
    /**
     * Referential package node.
     */
    ReferentialPackage("R:P"),
    /**
     * Referential type node.
     */
    ReferentialType("R:T"),
    /**
     * Root open node.
     */
    RootOpen("D:R"),
    /**
     * Root open node parent filter.
     */
    RootOpenFilter("D:F"),
    /**
     * Open node.
     */
    Open("D:O"),
    /**
     * Open list.
     */
    OpenList("D:L"),
    /**
     * Edit node.
     */
    Edit("D:E"),
    /**
     * Table node.
     */
    Table("D:T"),
    /**
     * Simple node.
     */
    Simple("D:S");
    /**
     * Label.
     */
    private final String label;

    MetaModelNodeType(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

    public interface WithMetaModelNodeType {
        MetaModelNodeType getNodeType();

        default boolean isData() {
            return !isReferential();
        }

        default boolean isReferential() {
            return isReferentialPackage() || isReferentialType();
        }

        default boolean isEdit() {
            return Edit.equals(getNodeType());
        }

        default boolean isTable() {
            return Table.equals(getNodeType());
        }

        default boolean isSimple() {
            return Simple.equals(getNodeType());
        }

        default boolean isReferentialPackage() {
            return ReferentialPackage.equals(getNodeType());
        }

        default boolean isReferentialType() {
            return ReferentialType.equals(getNodeType());
        }

        default boolean isOpen() {
            return Open.equals(getNodeType());
        }

        default boolean isOpenList() {
            return OpenList.equals(getNodeType());
        }

        default boolean isReference() {
            return isEdit() || isOpen() || isRootOpen();
        }

        default boolean isRootOpen() {
            return RootOpen.equals(getNodeType());
        }

        default boolean isRoot() {
            return getNodeType() == null;
        }

        default boolean isRootOpenFilter() {
            return RootOpenFilter.equals(getNodeType());
        }

        default String typeLabel() {
            return getNodeType().label();
        }

    }
}

