package fr.ird.observe.spi.json.toolkit;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Date;
import java.util.List;

/**
 * Created on 23/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(JsonAdapter.class)
public class ReferentialReferenceSetAdapter<R extends ReferentialDtoReference> extends ReferenceCollectionSupportAdapter<R, ReferentialDtoReferenceSet<R>> implements JsonAdapter {
    @Override
    public Class<?> type() {
        return ReferentialDtoReferenceSet.class;
    }


    @Override
    protected ReferentialDtoReferenceSet<R> newReferenceSet(Class<R> referenceType, List<R> references, Date lastUpdate, JsonObject jsonObject, JsonDeserializationContext context) {
        return ReferentialDtoReferenceSet.of(referenceType, references, lastUpdate);
    }


}
