package fr.ird.observe.spi.navigation.model;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class MetaModelNode implements MetaModelNodeType.WithMetaModelNodeType {

    /**
     * Unique key to identify a node.
     */
    private final String key;
    /**
     * Type of node.
     */
    private final MetaModelNodeType nodeType;
    /**
     * Data type.
     */
    private final String dataType;
    /**
     * Optional groupBy for this node.
     *
     * <ul>
     *     <li>Keys are property names</li>
     *     <li>Values are type (or type + flavor)</li>
     * </ul>
     */
    private final Map<String, String> groupBy;
    /**
     * Optional children for this node.
     */
    private final List<MetaModelNodeLink> children;

    public MetaModelNode(String key, MetaModelNodeType nodeType, String dataType) {
        this.key = key;
        this.nodeType = nodeType;
        this.dataType = dataType;
        this.groupBy = new LinkedHashMap<>();
        this.children = new LinkedList<>();
    }

    public String getKey() {
        return key;
    }

    public int getLevel() {
        return key.split("\\.").length - 1;
    }

    @Override
    public MetaModelNodeType getNodeType() {
        return nodeType;
    }

    public String getDataType() {
        return dataType;
    }

    public List<MetaModelNodeLink> getChildren() {
        return children;
    }

    public Map<String, String> getGroupBy() {
        return groupBy;
    }

    public boolean withGroupBy() {
        return groupBy.size() > 0;
    }

    public boolean withChildren() {
        return children.size() > 0;
    }

    public boolean leaf() {
        return !withChildren() && !withGroupBy();
    }
}
