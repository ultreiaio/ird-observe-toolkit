package fr.ird.observe.spi.navigation.model;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;

import java.beans.Introspector;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
@SuppressWarnings("UnusedReturnValue")
public class MetaModelBuilder {

    /**
     * Internal stack of nodes
     */
    private final LinkedList<MetaModelNode> stack;

    private final Map<String, MetaModelNode> rootNodes;
    private final Map<String, MetaModelNode> nodes;

    private MetaModelNode currentNode;

    public MetaModelBuilder() {
        stack = new LinkedList<>();
        nodes = new TreeMap<>();
        rootNodes = new TreeMap<>();
    }

    public MetaModelBuilder addEntryPoint(String dataType, MetaModelNodeType nodeType, Runnable runnable) {
        entryEntryPoint(dataType, nodeType);
        if (runnable != null) {
            runnable.run();
        }
        return exitEntryPoint();
    }

    public MetaModelBuilder entryEntryPoint(String dataType, MetaModelNodeType nodeType) {
        String key = Strings.leftPad("" + (rootNodes.size() + 1), 2, "0");
        registerNode(new MetaModelNode(key, nodeType, dataType));
        rootNodes.put(key, currentNode);
        return this;
    }

    public MetaModelBuilder exitEntryPoint() {
        if (currentNode == null) {
            throw new IllegalStateException("No node registered");
        }
        stack.removeLast();
        return this;
    }

    public MetaModelBuilder addGroupBy(String propertyName, String propertyType) {
        if (currentNode == null) {
            throw new IllegalStateException("No node registered");
        }
        currentNode.getGroupBy().put(propertyName, propertyType);
        return this;
    }

    public MetaModelBuilder addChildren(String propertyName, MetaModelNodeType nodeType, String propertyType, MetaModelNodeLinkMultiplicity multiplicity, Runnable runnable) {
        enterChildren(propertyName, nodeType, propertyType, multiplicity);
        if (runnable != null) {
            runnable.run();
        }
        return exitChildren();
    }

    public MetaModelBuilder enterChildren(String propertyName, MetaModelNodeType nodeType, String propertyType, MetaModelNodeLinkMultiplicity multiplicity) {
        if (currentNode == null) {
            throw new IllegalStateException("No node registered");
        }
        String key = currentNode.getKey() + "." + Strings.leftPad("" + (currentNode.getChildren().size() + 1), 2, "0");

        MetaModelNodeLink link = new MetaModelNodeLink(Introspector.decapitalize(propertyName), multiplicity, key);
        currentNode.getChildren().add(link);
        registerNode(new MetaModelNode(key, nodeType, propertyType));
        return this;
    }

    public MetaModel build() {
        return new MetaModel(nodes);
    }

    public MetaModelBuilder exitChildren() {
        if (currentNode == null) {
            throw new IllegalStateException("No node registered");
        }
        if (stack.isEmpty()) {
            throw new IllegalStateException("No more node registered, can't exit children");
        }
        stack.removeLast();
        currentNode = stack.getLast();
        return this;
    }

    protected void registerNode(MetaModelNode node) {
        stack.add(currentNode = node);
        nodes.put(node.getKey(), currentNode);
    }

}
