package fr.ird.observe.spi.navigation.model;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.spi.json.toolkit.navigation.spi.MetaModelAdapter;
import fr.ird.observe.spi.json.toolkit.navigation.spi.ParentLinkAdapter;
import fr.ird.observe.spi.json.toolkit.navigation.spi.TreeNodeLinkAdapter;
import fr.ird.observe.spi.navigation.model.id.IdProjectModel;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import fr.ird.observe.spi.navigation.parent.ParentLink;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;
import io.ultreia.java4all.util.SingletonSupplier;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Created on 30/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class MetaModelSupport {

    public static final String LOCATION_TEMPLATE = "META-INF/navigation/%s";
    private final String name;
    private final SingletonSupplier<MetaModel> metaModel;
    private final SingletonSupplier<IdProjectModel> idProjectModel;
    private final SingletonSupplier<TreeProjectModel> treeSelectionProjectModel;
    private final SingletonSupplier<TreeProjectModel> treeNavigationProjectModel;
    private final SingletonSupplier<ParentProjectModel> parentProjectModel;

    public static Gson getGson() {
        return new GsonBuilder().setPrettyPrinting()
                .registerTypeAdapter(MetaModel.class, new MetaModelAdapter())
                .registerTypeAdapter(TreeNodeLink.class, new TreeNodeLinkAdapter())
                .registerTypeAdapter(ParentLink.class, new ParentLinkAdapter())
                .create();
    }

    public MetaModelSupport(String name) {
        this(MetaModelSupport.class.getClassLoader(), name);
    }

    public MetaModelSupport(ClassLoader classLoader, String name) {
        this.name = name;
        String location = String.format(LOCATION_TEMPLATE, name);
        metaModel = SingletonSupplier.of(() -> {
            String resource = location + "-metamodel.json";
            try (InputStreamReader inputStream = new InputStreamReader(Objects.requireNonNull(classLoader.getResource(resource), String.format("Could not find resource: %s", resource)).openStream(), StandardCharsets.UTF_8)) {
                return getGson().fromJson(inputStream, MetaModel.class);
            } catch (Exception e) {
                throw new IllegalStateException("Can't load meta-model from " + resource, e);
            }
        });
        idProjectModel = SingletonSupplier.of(() -> {
            String resource = location + "-id-model.json";
            try (InputStreamReader inputStream = new InputStreamReader(Objects.requireNonNull(classLoader.getResource(resource), String.format("Could not find resource: %s", resource)).openStream(), StandardCharsets.UTF_8)) {
                return getGson().fromJson(inputStream, IdProjectModel.class);
            } catch (Exception e) {
                throw new IllegalStateException("Can't load idSelectProjectModel from " + resource, e);
            }
        });
        treeSelectionProjectModel = SingletonSupplier.of(() -> {
            String resource = location + "-tree-selection-model.json";
            try (InputStreamReader inputStream = new InputStreamReader(Objects.requireNonNull(classLoader.getResource(resource), String.format("Could not find resource: %s", resource)).openStream(), StandardCharsets.UTF_8)) {
                return getGson().fromJson(inputStream, TreeProjectModel.class);
            } catch (Exception e) {
                throw new IllegalStateException("Can't load treeSelectionProjectModel from " + resource, e);
            }
        });
        treeNavigationProjectModel = SingletonSupplier.of(() -> {
            String resource = location + "-tree-navigation-model.json";
            try (InputStreamReader inputStream = new InputStreamReader(Objects.requireNonNull(classLoader.getResource(resource), String.format("Could not find resource: %s", resource)).openStream(), StandardCharsets.UTF_8)) {
                return getGson().fromJson(inputStream, TreeProjectModel.class);
            } catch (Exception e) {
                throw new IllegalStateException("Can't load treeNavigationProjectModel from " + resource, e);
            }
        });
        parentProjectModel = SingletonSupplier.of(() -> {
            String resource = location + "-parent-model.json";
            try (InputStreamReader inputStream = new InputStreamReader(Objects.requireNonNull(classLoader.getResource(resource), String.format("Could not find resource: %s", resource)).openStream(), StandardCharsets.UTF_8)) {
                return getGson().fromJson(inputStream, ParentProjectModel.class);
            } catch (Exception e) {
                throw new IllegalStateException("Can't load parentProjectModel from " + resource, e);
            }
        });
    }

    public String getName() {
        return name;
    }

    public MetaModel getModel() {
        return metaModel.get();
    }

    public IdProjectModel getIdProjectModel() {
        return idProjectModel.get();
    }

    public TreeProjectModel getTreeNavigationProjectModel() {
        return treeNavigationProjectModel.get();
    }

    public TreeProjectModel getTreeSelectionProjectModel() {
        return treeSelectionProjectModel.get();
    }

    public ParentProjectModel getParentProjectModel() {
        return parentProjectModel.get();
    }
}
