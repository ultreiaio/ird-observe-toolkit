package fr.ird.observe.spi.navigation.model.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.navigation.model.MetaModelNode;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLink;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import fr.ird.observe.spi.navigation.model.MetaModelNodeType;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created on 01/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class TreeNodeModel implements MetaModelNodeType.WithMetaModelNodeType {

    public static final String GROUP_BY = "GroupBy";
    public static final String LIST = "List";
    private final List<String> parentClassNames;
    private final String className;
    private final String type;
    private final MetaModelNodeType nodeType;
    private transient Pair<String, String> gav;
    private List<TreeNodeLink> children;

    public static TreeNodeModel newRoot() {
        return new TreeNodeModel(null, null, "Root", "Void", null);
    }

    public static TreeNodeModel newReferentialPackageNode(TreeNodeModel parentNode, MetaModelNode node) {
        String dataType = node.getDataType();
        String className = node.getDataType() + ".ReferentialPackage";
        return new TreeNodeModel(List.of(parentNode), node.getNodeType(), className, dataType, new MetaModelNodeLink(TreeProjectModelBuilder.toCamelCase(dataType), MetaModelNodeLinkMultiplicity.ONE, className));
    }

    public static TreeNodeModel newReferentialPackageType(TreeNodeModel parentNode, MetaModelNode node, MetaModelNodeLink link) {
        return new TreeNodeModel(List.of(parentNode), node.getNodeType(), node.getDataType(), node.getDataType(), link);
    }

    public static TreeNodeModel newRootData(List<TreeNodeModel> parentNodes, MetaModelNode metaModelNode) {
        return new TreeNodeModel(parentNodes, metaModelNode.getNodeType(), metaModelNode.getDataType(), metaModelNode.getDataType());
    }

    public static TreeNodeModel newData(TreeNodeModel parentNode, MetaModelNode node, MetaModelNodeLink link) {
        return new TreeNodeModel(List.of(parentNode), node.getNodeType(), node.getDataType(), node.getDataType(), link);
    }

    public static TreeNodeModel newOpenDataList(TreeNodeModel parentNode, MetaModelNode node, MetaModelNodeLink link) {
        return new TreeNodeModel(List.of(parentNode), MetaModelNodeType.OpenList, node.getDataType() + LIST, node.getDataType(), link);
    }

    public static TreeNodeModel newDataGroupBySupport(TreeNodeModel parentNode, MetaModelNode metaModelNode) {
        return new TreeNodeModel(List.of(parentNode), MetaModelNodeType.RootOpenFilter, metaModelNode.getDataType() + GROUP_BY, null);
    }

    public static TreeNodeModel newDataGroupBy(TreeNodeModel parentNode, TreeNodeModel abstractNode, String propertyName) {
        String linkName = TreeProjectModelBuilder.toCamelCase(abstractNode.getClassName() + "." + propertyName);
        String className = abstractNode.getClassName() + Strings.capitalize(propertyName);
        return new TreeNodeModel(List.of(parentNode), MetaModelNodeType.RootOpenFilter, className, className, new MetaModelNodeLink(linkName, MetaModelNodeLinkMultiplicity.MANY, className));
    }

    TreeNodeModel(List<TreeNodeModel> parentNode, MetaModelNodeType nodeType, String className, String type, MetaModelNodeLink link) {
        this(parentNode, nodeType, className, type);
        if (parentNode != null) {
            TreeNodeLink mapping = new TreeNodeLink(link.getPropertyName(), getClassName(), link.getMultiplicity());
            parentNode.forEach(p -> p.getChildren().add(mapping));
        }
    }

    TreeNodeModel(List<TreeNodeModel> parentNodes, MetaModelNodeType nodeType, String className, String type) {
        this.parentClassNames = parentNodes == null ? null : parentNodes.stream().map(TreeNodeModel::getClassName).collect(Collectors.toList());
        this.nodeType = nodeType;
        this.className = className;
        this.type = type;
    }

    @Override
    public MetaModelNodeType getNodeType() {
        return nodeType;
    }

    public String getParentClassName() {
        return parentClassNames == null || withMultipleParent() ? null : parentClassNames.get(0);
    }

    public List<String> getParentClassNames() {
        return parentClassNames;
    }

    public String getClassName() {
        return className;
    }

    public String getType() {
        return type;
    }

    public boolean isLeaf() {
        return children == null || children.isEmpty();
    }

    public List<TreeNodeLink> getChildren() {
        if (children == null) {
            children = new LinkedList<>();
        }
        return children;
    }

    public Optional<TreeNodeLink> getChildren(String className) {
        return getChildren().stream().filter(l -> className.equals(l.getTargetClassName())).findFirst();
    }

    public String getSimpleName() {
        return ProjectPackagesDefinition.getSimpleName(className);
    }

    public String getPackageName() {
        return ProjectPackagesDefinition.getPackageName(className);
    }

    public Pair<String, String> gav() {
        if (isRoot()) {
            return null;
        }
        if (gav == null) {
            this.gav = ProjectPackagesDefinition.gav(getPackageName());
        }
        return gav;
    }

    public String getIconPath() {
        if (nodeType == null) {
            return null;
        }
        Pair<String, String> gav = gav();
        switch (nodeType) {
            case ReferentialPackage:
            case ReferentialType:
                return String.format("%1$s.referential.%2$s.Home", gav.getLeft(), gav.getRight());
            case RootOpenFilter:
                String name = getSimpleName();
                return String.format("%1$s.data.%2$s.%3$s", gav.getLeft(), gav.getRight(), name.substring(0, name.indexOf("GroupBy")) + "List");
            case OpenList:
            case RootOpen:
            case Edit:
            case Open:
            case Table:
            case Simple:
                String simpleName = getSimpleName();
                return String.format("%1$s.data.%2$s.%3$s", gav.getLeft(), gav.getRight(), simpleName);
        }
        return null;
    }

    public String getStatePath(TreeProjectModel project) {
        if (isRoot() || isOpen()) {
            // root has no path
            // open is pure id path (path is data id)
            return null;
        }
        if (isRootOpen()) {
            // use rood node (always the first one)
            TreeNodeModel parentNode = project.getNode(getParentClassNames().get(0)).orElseThrow();
            TreeNodeLink link = parentNode.getChildren(getClassName()).orElseThrow();
            return link.getPropertyName();
        }
        TreeNodeLink link = project.getNodeLink(this).orElseThrow();
        return link.getPropertyName();
    }

    public boolean withMultipleParent() {
        return parentClassNames != null && parentClassNames.size() > 1;
    }
}
