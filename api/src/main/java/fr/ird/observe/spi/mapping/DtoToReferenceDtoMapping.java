package fr.ird.observe.spi.mapping;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.classmapping.ImmutableClassMapping;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5
 */
public abstract class DtoToReferenceDtoMapping extends ImmutableClassMapping<BusinessDto, DtoReference> {

    private final Map<Class<? extends BusinessDto>, DtoReferenceDefinition<?, ?>> mapping;

    private static Map<Class<? extends BusinessDto>, Class<? extends DtoReference>> mapping(Map<Class<? extends BusinessDto>, DtoReferenceDefinition<?, ?>> mapping) {
        Map<Class<? extends BusinessDto>, Class<? extends DtoReference>> builder = new LinkedHashMap<>(mapping.size());
        mapping.forEach((key, value) -> builder.put(key, value.getType()));
        return Collections.unmodifiableMap(builder);
    }


    protected DtoToReferenceDtoMapping(Map<Class<? extends BusinessDto>, DtoReferenceDefinition<?, ?>> mapping) {
        super(mapping(mapping));
        this.mapping = mapping;
    }

    public Class<? extends ReferentialDtoReference> get(ReferentialDto dto) {
        return get(dto.getClass());
    }

    @SuppressWarnings("unchecked")
    public <DD extends BusinessDto, RR extends DtoReference> DtoReferenceDefinition<DD, RR> getDefinition(Class<? extends BusinessDto> key) {
        return (DtoReferenceDefinition<DD, RR>) mapping.get(key);
    }
}
