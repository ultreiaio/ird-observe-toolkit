package fr.ird.observe.spi.json.toolkit.stats;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.dto.stats.StatisticValue;
import fr.ird.observe.dto.stats.WithStatistics;
import fr.ird.observe.dto.stats.WithStatisticsHolder;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;

/**
 * Created on 04/07/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public abstract class StatisticsHolderAdapter<X extends WithStatistics<X>, H extends WithStatisticsHolder<X>> implements JsonSerializer<H>, JsonDeserializer<H>, JsonAdapter {

    protected abstract H deserialize(JsonObject source);

    protected abstract JsonObject serialize(H source);

    @Override
    public final H deserialize(JsonElement source, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return deserialize(source.getAsJsonObject());
    }

    @Override
    public final JsonElement serialize(H source, Type typeOfSrc, JsonSerializationContext context) {
        return serialize(source);
    }

    protected void setStaticsValue(JsonObject target, StatisticValue value) {
        target.addProperty(value.name() + "Stat", value.getValue());
    }

    protected long getStaticsValue(JsonObject source, String name) {
        return source.getAsJsonPrimitive(name).getAsLong();
    }
}
