package fr.ird.observe.spi.json.toolkit;

/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Note: Pour une référence, on ne sérialize pas les méta-données (noms et types des propriétés), on les récupère à la
 * désérialisation via {@link DtoReferenceDefinition}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ReferenceCollectionSupportAdapter<R extends DtoReference, S extends DtoReferenceCollection<R>> implements JsonDeserializer<S> {

    @Override
    public final S deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Class<R> referenceType = context.deserialize(jsonObject.get(DtoReferenceCollection.PROPERTY_TYPE), Class.class);
        JsonElement lastUpdateElement = jsonObject.get(ReferentialDtoReferenceSet.PROPERTY_LAST_UPDATE);
        Date lastUpdate = lastUpdateElement != null ? context.deserialize(lastUpdateElement, Date.class) : null;
        JsonArray jsonPropertyValues = jsonObject.get(DtoReferenceCollection.PROPERTY_REFERENCES).getAsJsonArray();
        int referencesSize = jsonPropertyValues.size();
        List<R> references = new LinkedList<>();
        for (int i = 0; i < referencesSize; i++) {
            R reference = context.deserialize(jsonPropertyValues.get(i), referenceType);
            references.add(reference);
        }
        return newReferenceSet(referenceType, references, lastUpdate, jsonObject, context);
    }

    protected abstract S newReferenceSet(Class<R> referenceType, List<R> references, Date lastUpdate, JsonObject jsonObject, JsonDeserializationContext context);

}
