package fr.ird.observe.spi.json;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.commons.lang3.tuple.Pair;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 05/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.22
 */
public class JsonHelper {

    public static String removeProperties(Gson gson, String content, String... propertyNames) {
        Map<String, Object> o = gson.fromJson(content, (Type) Object.class);
        removeProperties(o, propertyNames);
        return gson.toJson(o);
    }

    public static void removeProperties(Map<String, Object> o, String... propertyNames) {
        for (String propertyName : propertyNames) {
            o.remove(propertyName);
        }
    }

    public static Pair<String, List<Object>> removeAndCollectProperties(Gson gson, String content, String... propertyNames) {
        Map<String, Object> o = gson.fromJson(content, (Type) Object.class);
        List<Object> removed = new ArrayList<>(propertyNames.length);
        for (String propertyName : propertyNames) {
            Object remove = o.remove(propertyName);
            removed.add(remove);
        }
        return Pair.of(gson.toJson(o), removed);
    }
}
