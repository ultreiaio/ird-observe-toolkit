package fr.ird.observe.spi.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableListMultimap;
import com.google.gson.GsonBuilder;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import io.ultreia.java4all.lang.Objects2;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * For a given dto type, associate a navigation node path.
 * <p>
 * Useful to get back the full path to the given node.
 * <p>
 * Created on 08/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class DtoToToolkitTreePathMapping {

    private final ImmutableListMultimap<Class<? extends BusinessDto>, Path> data;

    public static final String LOCATION_TEMPLATE = "META-INF/mapping/%s";

    protected DtoToToolkitTreePathMapping(String location) {
        String resourcePath = String.format(LOCATION_TEMPLATE, location);
        URL resource = getClass().getClassLoader().getResource(resourcePath);
        try (Reader reader = new InputStreamReader(Objects.requireNonNull(resource, String.format("Can't find resource: %s", resourcePath)).openStream())) {
            Map<String, List<String>> data = new GsonBuilder().create().fromJson(reader, (Type) Object.class);
            ImmutableListMultimap.Builder<Class<? extends BusinessDto>, Path> builder = ImmutableListMultimap.builder();
            data.forEach((k, v) -> builder.putAll(Objects2.forName(k), v.stream().map(Path::of).collect(Collectors.toList())));
            this.data = builder.build();
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't read resource: %s", resourcePath));
        }
    }

    public abstract Supplier<? extends ToolkitTreeNode> rootSupplier();

    public Path get(Class<? extends BusinessDto> type) {
        Collection<Path> paths = data.get(type);
        return paths.isEmpty() ? null : paths.iterator().next();
    }

    public Path get(Class<? extends BusinessDto> type, String classifier) {
        Collection<Path> paths = data.get(type);
        return paths.isEmpty() ? null : paths.stream().filter(p -> classifier == null || p.startsWith(classifier)).findFirst().orElseThrow();
    }

    public Set<Class<? extends BusinessDto>> keySet() {
        return data.keySet();
    }

    public int size() {
        return data.size();
    }

    public String print() {
        int length = 0;
        StringBuilder result = new StringBuilder();
        for (Class<?> entry : keySet()) {
            length = Math.max(length, entry.getName().length());
        }
        String format = "%3d %-" + (length + 1) + "s → %s%n";
        int index = 1;
        for (Class<? extends BusinessDto> entry : keySet()) {
            result.append(String.format(format, index++, entry.getName(), get(entry)));
        }
        return result.toString();
    }
}
