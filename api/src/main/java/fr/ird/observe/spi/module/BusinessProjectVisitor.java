package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.referential.ReferentialDto;

/**
 * To visit a project.
 * <p>
 * Created on 30/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.19
 */
public interface BusinessProjectVisitor {

    static void simpleVisit(BusinessProject project, BusinessProjectVisitor visitor) {
        visitor.enterProject(project);
        for (BusinessModule module : project.getModules()) {
            visitor.enterModule(module);
            for (BusinessSubModule subModule : module.getSubModules()) {
                visitor.enterSubModule(module, subModule);
                subModule.getDataPackage().ifPresent(p -> {
                    visitor.enterSubModuleDataPackage(module, subModule, p);
                    for (Class<? extends DataDto> type : p.getTypes()) {
                        visitor.enterSubModuleDataType(module, subModule, p, type);
                    }
                    visitor.exitSubModuleDataPackage(module, subModule, p);
                });
                subModule.getReferentialPackage().ifPresent(p -> {
                    visitor.enterSubModuleReferentialPackage(module, subModule, p);
                    for (Class<? extends ReferentialDto> type : p.getTypes()) {
                        visitor.enterSubModuleReferentialType(module, subModule, p, type);
                    }
                    visitor.exitSubModuleReferentialPackage(module, subModule, p);
                });
                visitor.exitSubModule(module, subModule);
            }
            visitor.exitModule(module);
        }
        visitor.exitProject(project);
    }

    default void enterProject(BusinessProject project) {
    }

    default void exitProject(BusinessProject project) {
    }

    default void enterModule(BusinessModule module) {
    }

    default void exitModule(BusinessModule module) {
    }

    default void enterSubModule(BusinessModule module, BusinessSubModule subModule) {
    }

    default void exitSubModule(BusinessModule module, BusinessSubModule subModule) {
    }

    default void enterSubModuleDataPackage(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage) {
    }

    default void exitSubModuleDataPackage(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage) {
    }

    default void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage, Class<? extends DataDto> dtoType) {
    }

    default void enterSubModuleReferentialPackage(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage) {
    }

    default void exitSubModuleReferentialPackage(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage) {
    }

    default void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
    }

}
