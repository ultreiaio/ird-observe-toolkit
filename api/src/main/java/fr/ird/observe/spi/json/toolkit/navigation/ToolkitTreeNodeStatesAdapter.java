package fr.ird.observe.spi.json.toolkit.navigation;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import io.ultreia.java4all.util.json.adapters.DateAdapter;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Created on 15/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
@AutoService(JsonAdapter.class)
public class ToolkitTreeNodeStatesAdapter extends TypeAdapter<ToolkitTreeNodeStates> implements JsonAdapter {

    private static final DateAdapter DATE_ADAPTER = new DateAdapter();
    private static Map<String, Class<?>> deserializeStates;

    public static Map<String, Class<?>> getDeserializeStates() {
        if (deserializeStates == null) {
            deserializeStates = loadDeserializeStates();
        }
        return deserializeStates;
    }


    private static Map<String, Class<?>> loadDeserializeStates() {
        Map<String, Class<?>> result = new LinkedHashMap<>();
        for (ToolkitTreeNodeStateAdapter adapter : ServiceLoader.load(ToolkitTreeNodeStateAdapter.class)) {
            String name = adapter.getState().name();
            result.put(name, adapter.getState().type());
        }
        return result;
    }

    @Override
    public ToolkitTreeNodeStates read(JsonReader in) throws IOException {

        ToolkitTreeNodeStates result = new ToolkitTreeNodeStates();
        in.beginObject();
        while (in.hasNext()) {
            String stateName = in.nextName();
            Object value = null;
            JsonToken peek = in.peek();
            Class<?> special = getDeserializeStates().get(stateName);
            if (special != null) {
                value = read(in, special);
            } else {
                switch (peek) {
                    case STRING:
                        value = in.nextString();
                        break;
                    case NUMBER:
                        value = in.nextInt();
                        break;
                    case BOOLEAN:
                        value = in.nextBoolean();
                        break;
                    case NULL:
                        value = null;
                        break;
                }
            }
            if (value != null) {
                result.addState(stateName, value);
            }
        }
        in.endObject();
        return result;
    }

    private Object read(JsonReader in, Class<?> special) throws IOException {
        if (special == String.class) {
            return in.nextString();
        }
        if (special == Integer.class) {
            return in.nextInt();
        }
        if (special == Boolean.class) {
            return in.nextBoolean();
        }
        if (special == Date.class) {
            String dateStr = in.nextString();
            return DATE_ADAPTER.deserializeToDate(dateStr);
        }
        return null;
    }

    @Override
    public void write(JsonWriter out, ToolkitTreeNodeStates src) throws IOException {
        out.beginObject();
        for (Map.Entry<String, Object> entry : src.getStates().entrySet()) {
            String stateName = entry.getKey();
            Object stateValue = entry.getValue();
            if (stateValue == null) {
                continue;
            }
            out.name(stateName);
            if (stateName.equals(ToolkitTreeNodeBean.STATE_CONTENT.name())) {
                out.jsonValue((String) stateValue);
            } else if (stateName.equals(ToolkitTreeNodeBean.STATE_REFERENCES.name())) {
                out.jsonValue((String) stateValue);
            } else if (stateValue instanceof String) {
                out.value((String) stateValue);
            } else if (stateValue instanceof Number) {
                out.value((Number) stateValue);
            } else if (stateValue instanceof Boolean) {
                out.value((Boolean) stateValue);
            } else if (stateValue instanceof Date) {
                out.value(DATE_ADAPTER.serialize((Date) stateValue));
            } else {
                out.value(stateValue.toString());
            }
        }
        out.endObject();
    }

    @Override
    public Class<?> type() {
        return ToolkitTreeNodeStates.class;
    }
}
