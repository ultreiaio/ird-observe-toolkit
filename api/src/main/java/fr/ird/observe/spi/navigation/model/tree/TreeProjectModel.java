package fr.ird.observe.spi.navigation.model.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Optional;

/**
 * Created on 01/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class TreeProjectModel {

    private final List<TreeNodeModel> nodes;

    public TreeProjectModel(List<TreeNodeModel> nodes) {
        this.nodes = nodes;
    }

    public List<TreeNodeModel> getNodes() {
        return nodes;
    }

    public Optional<TreeNodeModel> getNode(String className) {
        return nodes.stream().filter(n -> className.equals(n.getClassName())).findFirst();
    }

    public Optional<TreeNodeLink> getNodeLink(TreeNodeModel node) {
        String parentClassName = node.getParentClassName();
        return Optional.ofNullable(parentClassName).flatMap(this::getNode).flatMap(p -> p.getChildren(node.getClassName()));
    }

    public Optional<TreeNodeModel> getParent(TreeNodeModel node) {
        String parentClassName = node.getParentClassName();
        return Optional.ofNullable(parentClassName).flatMap(this::getNode);
    }
}
