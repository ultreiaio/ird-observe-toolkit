package fr.ird.observe.spi.type;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.CommonDto;
import io.ultreia.java4all.i18n.spi.type.SimpleTypeTranslatorSupport;
import io.ultreia.java4all.i18n.spi.type.TypeTranslator;

/**
 * Unique type translator for the dto domain, just add all possible suffixes to clean
 * Created on 07/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
@AutoService(TypeTranslator.class)
public class ObserveDtoTypeTranslator extends SimpleTypeTranslatorSupport {

    public ObserveDtoTypeTranslator() {
        super("fr.ird.observe.dto", CommonDto.class, "Dto", "Reference", "Aware");
    }

}
