package fr.ird.observe.spi.json;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;

/**
 * Created on 20/06/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.6
 */
@AutoService(JsonAdapter.class)
public class StackTraceElementAdapter implements JsonDeserializer<StackTraceElement>, JsonAdapter {
    @Override
    public StackTraceElement deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String declaringClass = json.getAsJsonObject().get("declaringClass").getAsString();
        String methodName = json.getAsJsonObject().get("methodName").getAsString();
        String fileName = json.getAsJsonObject().get("fileName").getAsString();
        int lineNumber = json.getAsJsonObject().get("lineNumber").getAsInt();
        return new StackTraceElement(declaringClass, methodName, fileName, lineNumber);
    }

    @Override
    public Class<?> type() {
        return StackTraceElement.class;
    }
}
