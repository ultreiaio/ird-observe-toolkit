package fr.ird.observe.spi.module;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.ContainerChildDto;
import fr.ird.observe.dto.data.ContainerDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByDtoDefinition;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.FormDefinition;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceDefinition;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.type.TypeTranslators;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 03/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class BusinessProject {

    private static final Logger log = LogManager.getLogger(BusinessProject.class);
    private final String name;
    private final List<BusinessModule> modules;
    private final BusinessProjectMapping mapping;
    private ProjectPackagesDefinition packagesDefinition;
    private Map<String, List<DataGroupByDtoDefinition<?, ?>>> groupByDefinitionsByModuleNames;

    public static boolean isReferential(Class<?> type) {
        return ReferentialDto.class.isAssignableFrom(type) || ReferentialDtoReference.class.isAssignableFrom(type);
    }

    public static boolean isData(Class<?> type) {
        return DataDto.class.isAssignableFrom(type) || DataDtoReference.class.isAssignableFrom(type);
    }

    public static String getLayout(BusinessModule businessModule, BusinessSubModule subBusinessModule, boolean data, String simpleName) {
        return businessModule.getName() + "." + (data ? "data" : "referential") + "." + subBusinessModule.getName() + "." + simpleName;
    }

    public static Path getDtoVariablePath(Path rootPath, BusinessModule module, BusinessSubModule subModule, Class<? extends BusinessDto> dtoType) {
        String simpleName = dtoType.getSimpleName().replace("Dto", "");
        return rootPath.resolve(isData(dtoType) ? "data" : "referential").resolve(module.getName()).resolve(subModule.getName()).resolve(simpleName);
    }

    public static Path getDtoPackagePath(Path rootPath, BusinessModule module, BusinessSubModule subModule, boolean data) {
        return rootPath.resolve(data ? "data" : "referential").resolve(module.getName()).resolve(subModule.getName());
    }

    public static Path getDtoModulePath(Path rootPath, BusinessModule module, boolean data) {
        return rootPath.resolve(data ? "data" : "referential").resolve(module.getName());
    }

    public static Path getDtoVariablePath(Path rootPath, BusinessModule module, Class<? extends BusinessDto> dtoType) {
        String simpleName = dtoType.getSimpleName().replace("Dto", "");
        return rootPath.resolve(isData(dtoType) ? "data" : "referential").resolve(module.getName()).resolve(simpleName);
    }

    public BusinessProject(String name, List<BusinessModule> modules, BusinessProjectMapping mapping) {
        this.name = Objects.requireNonNull(name);
        this.modules = Objects.requireNonNull(modules);
        this.mapping = mapping;
    }

    public Map<String, List<DataGroupByDtoDefinition<?, ?>>> getGroupByDefinitionsByModuleNames() {
        if (groupByDefinitionsByModuleNames == null) {
            groupByDefinitionsByModuleNames = new LinkedHashMap<>();
            for (BusinessModule module : modules) {
                List<String> groupByNames = module.getDataGroupByDtoDefinitions().stream().map(DataGroupByDefinition::getName).collect(Collectors.toList());
                String moduleName = module.getName();
                log.info(String.format("Module %s - Detected %s groupBy definition(s): %s.", moduleName, groupByNames.size(), groupByNames));
                groupByDefinitionsByModuleNames.computeIfAbsent(moduleName, k -> new LinkedList<>()).addAll(module.getDataGroupByDtoDefinitions());
            }
        }
        return groupByDefinitionsByModuleNames;
    }

    @SuppressWarnings("unchecked")
    public <C extends BusinessDto> List<Class<? extends C>> sortWithLongTitle(Collection<Class<? extends C>> types, Locale locale) {
        return ObserveUtil.sortTypes(types, t -> getLongTitle(t, locale), locale);
    }

    public ToolkitIdDtoBean toShortDto(String id) {
        Class<? extends BusinessDto> type = toDtoType(id);
        return ToolkitIdDtoBean.of(type, id);
    }

    public Class<? extends BusinessDto> toDtoType(String id) {
        if (id == null) {
            return null;
        }
        ProjectPackagesDefinition packagesDefinition = getPackagesDefinition();
        String relativeEntityPackage = packagesDefinition.getRelativeEntityPackageFromId(id);
        String dtoFqn = packagesDefinition.getDtoRootPackage() + "." + relativeEntityPackage + "Dto";
        return getTypes().stream().filter(t -> dtoFqn.equals(t.getName())).findFirst().orElse(null);
    }

    public ProjectPackagesDefinition getPackagesDefinition() {
        if (packagesDefinition == null) {
            packagesDefinition = ProjectPackagesDefinition.of(getClass().getClassLoader());
        }
        return packagesDefinition;
    }

    public boolean accept(BusinessModule module, BusinessDto dto) {
        return module.getDataTypes().contains(dto.getClass())
                || module.getReferentialTypes().contains(dto.getClass())
                // special case for cross-module dto (FIXME Remove this ASAP)
                || module.accept(dto.getClass().getName());
    }

    public String getDtoSimplifiedName(Class<?> type) {
        String packageName = type.getPackage().getName();
        BusinessModule businessModule = getBusinessModule(packageName);
        BusinessSubModule subBusinessModule = getBusinessSubModule(businessModule, packageName);
        String simpleName = TypeTranslators.getTranslator(type).cleanSimpleName(type.getSimpleName());
        return getLayout(businessModule, subBusinessModule, isData(type), simpleName);
    }

    public Pair<BusinessModule, BusinessSubModule> load(Class<? extends BusinessDto> type) {
        String packageName = type.getPackage().getName();
        BusinessModule businessModule = getBusinessModule(packageName);
        BusinessSubModule subBusinessModule = getBusinessSubModule(businessModule, packageName);
        return Pair.of(businessModule, subBusinessModule);
    }

    public int getModulePriority(BusinessModule module) {
        return modules.indexOf(module);
    }

    public <D extends BusinessDto, R extends DtoReference> Optional<DtoReferenceDefinition<D, R>> getOptionalReferenceDefinition(Class<? extends BusinessDto> dtoType) {
        return Optional.ofNullable(getMapping().getDtoToReferenceDtoClassMapping().getDefinition(dtoType));
    }

    public <D extends BusinessDto, R extends DtoReference> Comparator<R> getReferenceComparator(Class<D> dtoType) {
        return getMapping().getDtoToReferenceDtoClassMapping().<D, R>getDefinition(dtoType).getComparator();
    }

    public <D extends BusinessDto> Optional<FormDefinition<D>> getOptionalFormDefinition(Class<? extends BusinessDto> dtoType) {
        return Optional.ofNullable(getMapping().getDtoToFormDtoMapping().getForm(dtoType));
    }

    public <D extends RootOpenableDto, F extends DataGroupByDto<D>> DataGroupByDtoDefinition<D, F> getDataGroupByDtoDefinition(String filterName) {
        return getMapping().getDataGroupByDtoToDefinitionMapping().getDefinition(filterName);
    }

    public <D extends RootOpenableDto> List<DataGroupByDtoDefinition<D, ?>> getDataGroupByDtoDefinitions(Class<D> dtoType) {
        return getMapping().getDataGroupByDtoToDefinitionMapping().getDefinitions(dtoType);
    }

    public String getName() {
        return name;
    }

    public List<BusinessModule> getModules() {
        return modules;
    }

    public Set<Class<? extends BusinessDto>> getTypes() {
        return modules.stream().flatMap(m -> m.getTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends ReferentialDto>> getReferentialTypes() {
        return modules.stream().flatMap(m -> m.getReferentialTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends DataDto>> getDataTypes() {
        return modules.stream().flatMap(m -> m.getDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends SimpleDto>> getSimpleDataTypes() {
        return modules.stream().flatMap(m -> m.getSimpleDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends SimpleDto>> getLayoutDataTypes() {
        return modules.stream().flatMap(m -> m.getLayoutDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends RootOpenableDto>> getRootOpenableDataTypes() {
        return modules.stream().flatMap(m -> m.getRootOpenableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends OpenableDto>> getOpenableDataTypes() {
        return modules.stream().flatMap(m -> m.getOpenableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends EditableDto>> getEditableDataTypes() {
        return modules.stream().flatMap(m -> m.getEditableDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Set<Class<? extends ContainerDto<?>>> getContainerDataTypes() {
        return modules.stream().flatMap(m -> m.getContainerDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @SuppressWarnings("unused")
    public Set<Class<? extends ContainerChildDto>> getContainerChildDataTypes() {
        return modules.stream().flatMap(m -> m.getContainerChildDataTypes().stream()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public BusinessProjectMapping getMapping() {
        return mapping;
    }

    public BusinessModule getBusinessModule(String packageName) {
        String cleanPackageName = packageName.replace(".data", "")
                .replace(".referential", "");
        int lastIndexOf = cleanPackageName.lastIndexOf(".");
        String tmp = cleanPackageName.substring(0, lastIndexOf);
        String subModule = cleanPackageName.substring(lastIndexOf + 1);
        String module = tmp.substring(tmp.lastIndexOf(".") + 1);
        return modules.stream()
                .filter(m -> m.getName().equalsIgnoreCase(module))
                .findFirst()
                .orElse(modules.stream().filter(m -> m.getName().equalsIgnoreCase(subModule)).findFirst().orElse(null));
    }

    public BusinessSubModule getBusinessSubModule(BusinessModule module, String packageName) {
        String suffix = packageName.substring(packageName.lastIndexOf(".") + 1);
        return module.getSubModules().stream().filter(m -> m.getName().equalsIgnoreCase(suffix)).findFirst().orElse(null);
    }

    public BusinessSubModule getBusinessSubModule(BusinessModule module, Class<? extends BusinessSubModule> subModuleType) {
        return module.getSubModules().stream().filter(m -> subModuleType.equals(m.getClass())).findFirst().orElse(null);
    }

    public BusinessModule getBusinessModule(Class<? extends BusinessModule> moduleType) {
        return modules.stream().filter(m -> moduleType.equals(m.getClass())).findFirst().orElse(null);
    }

    public String getReferentialPackageTitle(BusinessModule module, BusinessSubModule subModule) {
        String shortLabel = module.getModuleShortLabel();
        return I18n.t("observe.referential.package.title", shortLabel, subModule.getLabel());
    }

    public String getReferentialPackageTitle(BusinessModule module, BusinessSubModule subModule, Locale locale) {
        String shortLabel = module.getModuleShortLabel(locale);
        return I18n.t("observe.referential.package.title", shortLabel, subModule.getLabel(locale));
    }

    public String gotoReferentialHomeToolTipText(BusinessModule module, BusinessSubModule subModule) {
        String text = getReferentialPackageTitle(module, subModule);
        return I18n.t("observe.Common.action.goto.tip", text);
    }

    public String gotoSelectedDataToolTipText(Class<? extends BusinessDto> type) {
        return I18n.t("observe.Common.action.goto.selected.tip", I18nDecoratorHelper.getType(type));
    }

    public String createDataToolTipText(Class<? extends BusinessDto> type) {
        String text = getDataTitleWithSubModulePrefix(type);
        return I18n.t("observe.Common.action.create.tip", text);
    }

    public String saveDataToolTipText(Class<? extends BusinessDto> type) {
        String text = getDataTitleWithSubModulePrefix(type);
        return I18n.t("observe.Common.action.save.tip", text);
    }

    public String gotoOpenDataToolTipText(Class<? extends BusinessDto> type) {
        String text = getDataTitleWithSubModulePrefix(type);
        return I18n.t("observe.Common.action.goto.open.tip", text);
    }

    public String getDataTitleWithSubModulePrefix(Class<? extends BusinessDto> type) {
        Pair<BusinessModule, BusinessSubModule> pair = load(type);
        BusinessSubModule subModule = pair.getRight();
        String prefix;
        if (subModule.getName().equals("common")) {
            prefix = "";
        } else {
            prefix = subModule.getLabel() + " - ";

        }
        return prefix + I18nDecoratorHelper.getType(type);
    }

    public String getFullTitle(Locale locale, Class<? extends BusinessDto> type) {
        String prefix = isReferential(type) ? I18n.l(locale, "observe.Common.type.referential") : I18n.l(locale, "observe.Common.type.data");
        return prefix + " ` " + getLongTitle(type) + " `";
    }

    public String getLongTitle(Class<? extends BusinessDto> type) {
        Pair<BusinessModule, BusinessSubModule> pair = load(type);
        BusinessModule module = pair.getLeft();
        BusinessSubModule subModule = pair.getRight();
        String prefix = module.getLabel() + " - ";
        if (!module.getName().equals("common") || !subModule.getName().equals("common")) {
            prefix += subModule.getLabel() + " - ";
        }
        return prefix + I18nDecoratorHelper.getType(type);
    }

    public String getLongTitle(Class<? extends BusinessDto> type, Locale locale) {
        Pair<BusinessModule, BusinessSubModule> pair = load(type);
        BusinessModule module = pair.getLeft();
        BusinessSubModule subModule = pair.getRight();
        String prefix = module.getLabel(locale) + " - ";
        if (!module.getName().equals("common") || !subModule.getName().equals("common")) {
            prefix += subModule.getLabel(locale) + " - ";
        }
        return prefix + I18nDecoratorHelper.getType(locale, type);
    }

    @SuppressWarnings("unchecked")
    public List<Class<? extends ReferentialDto>> sortReferentialTypesWithPackage(Locale locale, Set<Class<? extends ReferentialDto>> classes) {
        return ObserveUtil.sortTypes(classes, t -> getReferentialWithPackage(locale, t), locale);
    }

    public void accept(BusinessProjectVisitor visitor) {
        BusinessProjectVisitor.simpleVisit(this, visitor);
    }

    private String getReferentialWithPackage(Locale locale, Class<? extends ReferentialDto> type) {
        String packageName = type.getPackageName();
        BusinessModule module = getBusinessModule(packageName);
        BusinessSubModule subModule = getBusinessSubModule(module, packageName);
        String prefix = getReferentialPackageTitle(module, subModule, locale);
        return prefix + " - " + I18nDecoratorHelper.getType(locale, type);
    }

    public <D extends BusinessDto> BusinessModule getBusinessModuleByDtoType(Class<D> dtoType) {
        return modules.stream().filter(m -> m.getTypes().contains(dtoType)).findFirst().orElse(null);
    }

    public BusinessModule getBusinessModuleByName(String moduleName) {
        return getModules().stream().filter(m -> moduleName.equalsIgnoreCase(m.getName())).findFirst().orElseThrow();
    }
}
