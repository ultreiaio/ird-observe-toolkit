package fr.ird.observe.spi.json.java4all;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.ultreia.java4all.bean.monitor.JavaBeanPropertyModification;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;

/**
 * Created on 30/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
@AutoService(JsonAdapter.class)
public class JavaBeanPropertyModificationAdapter implements JsonDeserializer<JavaBeanPropertyModification>, JsonSerializer<JavaBeanPropertyModification>, JsonAdapter {

    public static final String PROPERTY_NAME = "propertyName";
    public static final String TYPE = "type";
    public static final String OLD_VALUE = "oldValue";
    public static final String NEW_VALUE = "newValue";

    @Override
    public Class<?> type() {
        return JavaBeanPropertyModification.class;
    }

    @Override
    public JavaBeanPropertyModification deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String propertyName = context.deserialize(jsonObject.get(PROPERTY_NAME), String.class);
        Class<?> type = context.deserialize(jsonObject.get(TYPE), Class.class);
        Object oldValue = context.deserialize(jsonObject.get(OLD_VALUE), type);
        Object newValue = context.deserialize(jsonObject.get(NEW_VALUE), type);
        return new JavaBeanPropertyModification(propertyName, oldValue, newValue);
    }

    @Override
    public JsonElement serialize(JavaBeanPropertyModification src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add(PROPERTY_NAME, context.serialize(src.getPropertyName()));
        Object newValue = src.getNewValue();
        Object oldValue = src.getOldValue();
        Class<?> type = newValue == null ? (oldValue == null ? String.class : oldValue.getClass()) : newValue.getClass();
        result.add(TYPE, context.serialize(type));
        result.add(OLD_VALUE, context.serialize(oldValue));
        result.add(NEW_VALUE, context.serialize(newValue));
        return result;
    }
}
