package fr.ird.observe.spi;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.DataHolder;
import com.vladsch.flexmark.util.data.MutableDataSet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * Created on 30/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class RenderMarkdown {

    private static DataHolder OPTIONS;
    private static HtmlRenderer RENDERER;
    private static Parser PARSER;

    public static String renderResource(String resourceName) {
        try (Reader resource = new InputStreamReader(Objects.requireNonNull(RenderMarkdown.class.getClassLoader().getResourceAsStream(resourceName)), StandardCharsets.UTF_8)) {
            Node node = parser().parseReader(resource);
            return renderer().render(node);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't render resource %s", resourceName), e);
        }
    }

    public static String renderContent(String resourceContent) {
        try (Reader resource = new StringReader(resourceContent)) {
            Node node = parser().parseReader(resource);
            String content = renderer().render(node);
            return String.format("<!DOCTYPE html>\n" +
                                         "<html lang=\"en\">\n" +
                                         "<head>\n" +
                                         "    <meta charset=\"UTF-8\">\n" +
                                         "</head>\n" +
                                         "<body>\n" +
                                         "%s" +
                                         "</body>\n" +
                                         "</html>", content);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't render content %s", resourceContent), e);
        }
    }

    protected static DataHolder options() {
        return OPTIONS == null ? OPTIONS = new MutableDataSet()
                .set(Parser.EXTENSIONS, List.of(TablesExtension.create()))
                .toImmutable() : OPTIONS;
    }

    protected static HtmlRenderer renderer() {
        return RENDERER == null ? RENDERER = HtmlRenderer.builder(options()).build() : RENDERER;

    }

    protected static Parser parser() {
        return PARSER == null ? PARSER = Parser.builder(options()).build() : PARSER;
    }
}
