package fr.ird.observe.spi.json.toolkit;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 08/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.66
 */
@AutoService(JsonAdapter.class)
public class DataGroupByDtoSetAdapter<D extends RootOpenableDto, G extends DataGroupByDto<D>> implements JsonDeserializer<DataGroupByDtoSet<D, G>>, JsonAdapter {

    @Override
    public final DataGroupByDtoSet<D, G> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        Class<G> type = context.deserialize(jsonObject.get(DataGroupByDtoSet.PROPERTY_TYPE), Class.class);
        Class<G> propertyType = context.deserialize(jsonObject.get(DataGroupByDtoSet.PROPERTY_PROPERTY_TYPE), Class.class);
        JsonElement lastUpdateElement = jsonObject.get(DataGroupByDtoSet.PROPERTY_LAST_UPDATE);
        Date lastUpdate = lastUpdateElement == null ? null : context.deserialize(lastUpdateElement, Date.class);
        JsonArray jsonPropertyValues = jsonObject.get(DataGroupByDtoSet.PROPERTY_REFERENCES).getAsJsonArray();
        List<G> references = new ArrayList<>(jsonPropertyValues.size());
        for (JsonElement jsonPropertyValue : jsonPropertyValues) {
            G reference = context.deserialize(jsonPropertyValue, type);
            references.add(reference);
        }
        return DataGroupByDtoSet.of(type, propertyType, references, lastUpdate);
    }

    @Override
    public Class<?> type() {
        return DataGroupByDtoSet.class;
    }
}
