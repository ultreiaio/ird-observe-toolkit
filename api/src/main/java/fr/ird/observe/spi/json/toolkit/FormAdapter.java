package fr.ird.observe.spi.json.toolkit;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.Form;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(JsonAdapter.class)
public class FormAdapter<R extends BusinessDto> implements JsonDeserializer<Form<R>>, JsonAdapter {
    @Override
    public Class<?> type() {
        return Form.class;
    }


    @Override
    public Form<R> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonForm = json.getAsJsonObject();

        JsonElement jsonType = jsonForm.get(Form.PROPERTY_TYPE);
        Class<R> type = context.deserialize(jsonType, Class.class);

        JsonElement jsonObject = jsonForm.get(Form.PROPERTY_OBJECT);
        R object = context.deserialize(jsonObject, type);

        return Form.newFormDto(type, object);

    }
}
