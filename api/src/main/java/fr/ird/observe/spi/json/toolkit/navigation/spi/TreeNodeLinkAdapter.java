package fr.ird.observe.spi.json.toolkit.navigation.spi;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class TreeNodeLinkAdapter extends TypeAdapter<TreeNodeLink> {

    @Override
    public TreeNodeLink read(JsonReader in) throws IOException {
        String[] parts = explode(in.nextString());
        return new TreeNodeLink(parts[0], parts[2], MetaModelNodeLinkMultiplicity.valueOf(parts[1]));
    }

    @Override
    public void write(JsonWriter out, TreeNodeLink src) throws IOException {
        out.value(compact(src.getPropertyName(), src.getMultiplicity(), src.getTargetClassName()));
    }

    protected String[] explode(String value) {
        return value.split("\\s*→\\s*");
    }

    protected String compact(Object... values) {
        return Arrays.stream(values).map(Object::toString).collect(Collectors.joining(" → "));
    }
}
