package fr.ird.observe.spi.navigation.tree;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.navigation.tree.ToolkitTreeNode;
import fr.ird.observe.navigation.tree.ToolkitTreeNodeBean;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import io.ultreia.java4all.lang.Objects2;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class ToolkitTreeNodeBuilder {

    private final ToolkitTreeNodeInterceptor interceptor;

    public static ToolkitTreeNodeBuilder of(ToolkitTreeNodeInterceptor interceptor) {
        return new ToolkitTreeNodeBuilder(interceptor);
    }

    public static <N extends ToolkitTreeNode> N build(N node, Path nodePath) {
        return new ToolkitTreeNodeBuilder().load(node, nodePath);
    }

    public ToolkitTreeNodeBuilder() {
        this(null);
    }

    public ToolkitTreeNodeBuilder(ToolkitTreeNodeInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    public <N extends ToolkitTreeNode> N load(N root, Map<String, ToolkitTreeNodeStates> mapping) {
        LinkedList<Pair<String, N>> stack = new LinkedList<>();
        stack.add(Pair.of(null, root));

        Iterator<Map.Entry<String, ToolkitTreeNodeStates>> iterator = mapping.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, ToolkitTreeNodeStates> entry = iterator.next();
            String path = entry.getKey();
            Map<String, Object> states = entry.getValue().getStates();
            Pair<String, N> parent = getParent(stack, path);
            String lastPath = parent.getKey();
            String relativePath;
            if (lastPath == null) {
                relativePath = path;
            } else {
                relativePath = path.substring(lastPath.length() + 1);
            }
            N parentNode = parent.getValue();
            N newNode = createNode(parentNode, relativePath, states);
            if (iterator.hasNext()) {
                stack.add(Pair.of(path, newNode));
            }
        }
        return root;
    }

    public <N extends ToolkitTreeNode> N load(N root, Path nodePath) {
        LinkedList<Pair<String, N>> stack = new LinkedList<>();
        stack.add(Pair.of(null, root));
        if (nodePath != null) {
            nodePath.spliterator().forEachRemaining(p -> {
                String path = p.toFile().getName();
                Pair<String, N> parent = stack.getLast();
                N parentNode = parent.getValue();
                N newNode = createNode(parentNode, path, new LinkedHashMap<>());
                stack.add(Pair.of(path, newNode));
            });
        }
        return stack.getLast().getRight();
    }

    public <N extends ToolkitTreeNode> N createNode(N parentNode, String relativePath, Map<String, Object> states) {
        ToolkitTreeNodeBean bean = parentNode.getUserObject();
        String[] pathAndId = relativePath.split(":");
        String path;
        String id;
        switch (pathAndId.length) {
            case 1:
                path = pathAndId[0];
                id = path;
                break;
            case 2:
                path = pathAndId[0];
                id = pathAndId[1];
                break;
            default:
                throw new IllegalStateException("Can't manage relative path: " + relativePath);
        }
        Class<N> nodeType = bean.getNodeMapping(path);
        if (nodeType == null) {
            return null;
        }
        N newNode = Objects2.newInstance(Objects.requireNonNull(nodeType));
        ToolkitTreeNodeBean newNodeBean = newNode.getUserObject();
        if (states != null) {
            newNodeBean.getStates().putAll(states);
        }
        if (!id.startsWith("$") && !id.equals(newNodeBean.getPath())) {
            ToolkitTreeNodeBean.STATE_ID.setValue(newNodeBean, id);
        }
        parentNode.add(newNode);
        if (interceptor != null) {
            interceptor.intercept(newNode);
        }
        return newNode;
    }

    protected <N extends ToolkitTreeNode> Pair<String, N> getParent(LinkedList<Pair<String, N>> stack, String path) {
        Pair<String, N> last = stack.getLast();
        String lastPath = last.getKey();
        if (lastPath == null) {
            // on root
            return last;
        }
        if (path.startsWith(lastPath + "/")) {
            // found it
            return last;
        }
        stack.removeLast();
        return getParent(stack, path);
    }
}
