package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * Created on 20/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class ToolkitFixtures {

    private static final Logger log = LogManager.getLogger(ToolkitFixtures.class);
    protected static final Map<String, String> GLOBAL = loadFixturesMap("global");
    public static final int REFERENCE_DATA_COUNT = Integer.parseInt(GLOBAL.get("REFERENCE_DATA_COUNT"));
    public static final int ENTITIES_LIMIT_SIZE = Integer.parseInt(GLOBAL.get("ENTITIES_LIMIT_SIZE"));
    public static final int REFERENTIAL_COUNT = Integer.parseInt(GLOBAL.get("REFERENTIAL_COUNT"));
    protected static final Map<String, String> IDS = loadFixturesMap("ids");
    protected static final Map<String, String> VARIABLES = loadFixturesMap("variables");
    private static final String TEST_PROPERTIES_FILE_NAME = "observetest.properties";
    public static final String TEST_CREDENTIALS_KEY = "observetest.tck.credentials.path";
    private static ProjectPackagesDefinition PACKAGES_DEFINITION = ProjectPackagesDefinition.of(ToolkitFixtures.class.getClassLoader());
    private static final Set<String> asserts = new LinkedHashSet<>();
    private static Properties testProperties;
    private static File testsBasedir;
    private static File basedir;

    public static String getId(String propertyName) {
        return IDS.get(propertyName);
    }

    public static String getVariable(String propertyName) {
        return VARIABLES.get(propertyName);
    }

    public static String logTest(String action, int size, String typeName) {
        if (size < ENTITIES_LIMIT_SIZE) {
            return String.format("Will %s %d object(s) for %s", action, size, typeName);
        } else {
            return String.format("Will %s %d out of %d object(s) for %s", action, ENTITIES_LIMIT_SIZE, size, typeName);
        }
    }

    public static Map<String, String> loadFixturesMap(String... names) {
        Map<String, String> builder = new LinkedHashMap<>();
        for (String name : names) {
            URL url = Objects.requireNonNull(ToolkitFixtures.class.getClassLoader().getResource("fixtures/" + name + ".properties"), "Fixture not found: " + name);
            try (InputStreamReader inputStream = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)) {
                log.debug(String.format("Fixture [%s] - Loading...", url));
                Properties p = new Properties();
                p.load(inputStream);
                log.debug(String.format("Fixture [%s] - Loaded %d properties.", url, p.size()));
                for (Map.Entry<Object, Object> entry : p.entrySet()) {
                    builder.put(entry.getKey().toString(), entry.getValue().toString());
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Can't load fixtures " + name, e);
            }
        }
        return builder;
    }

    public static List<String> loadFixturesList(String name) {
        URL url = Objects.requireNonNull(ToolkitFixtures.class.getClassLoader().getResource("fixtures/" + name + ".list"));
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            log.debug(String.format("Fixture [%s] - Loading...", url));
            List<String> builder = new LinkedList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.add(line);
            }
            log.debug(String.format("Fixture [%s] - Loaded %d elements.", url, builder.size()));
            return builder;
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't load fixtures " + name, e);
        }
    }

    public static Map<String, Long> loadFixturesMapCount(String name) {
        Map<String, Long> builder = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : loadFixturesMap(name).entrySet()) {
            builder.put(entry.getKey(), Long.valueOf(entry.getValue()));
        }
        return builder;
    }

    public static List<Class<?>> toClass(Collection<String> names, String... packageCandidates) {
        List<Class<?>> builder = new LinkedList<>();
        for (String name : names) {
            builder.add(Objects.requireNonNull(toClass(name, packageCandidates), "Can't find class: " + name));
        }
        return builder;
    }

    public static Class<?> toClass(String name, String... packageCandidates) {
        for (String packageCandidate : packageCandidates) {
            String fqn = packageCandidate + name;
            try {
                return Class.forName(fqn);
            } catch (ClassNotFoundException ignored) {
            }
        }

        return null;
    }

    public static List<Class<?>> toDtoClass(Collection<String> names) {
        return toClass(names, PACKAGES_DEFINITION.getDtoPackageCandidates());
    }

    public static List<Class<?>> loadFixtureDtoList(String name) {
        return toDtoClass(loadFixturesList(name));
    }

    public static String loadFixture(BusinessModule module, BusinessSubModule subModule, Class<? extends BusinessDto> dtoType, String name) throws IOException {
        Path path = BusinessProject.getDtoVariablePath(Path.of("/fixtures"), module, subModule, dtoType).resolve(name);
        return IOUtils.resourceToString(path.toString(), StandardCharsets.UTF_8);
    }

    public static String constantToType(String key) {
        if (key.startsWith("REFERENTIAL")) {
            return null;
        }
        if (key.endsWith("_MOVE")) {
            return null;
        }
        String[] split = key.split("_");
        StringBuilder dtoNameBuilder = new StringBuilder(PACKAGES_DEFINITION.getDtoDataPackage() + ".");
        dtoNameBuilder.append(split[0].toLowerCase()).append('.');
        dtoNameBuilder.append(split[1].toLowerCase()).append('.');
        for (int i = 2; i < split.length; i++) {
            dtoNameBuilder.append(Strings.capitalize(split[i].toLowerCase()));
        }
        return dtoNameBuilder.append("Dto").toString();
    }

    public static ProjectPackagesDefinition packagesDefinition() {
        return PACKAGES_DEFINITION == null ? PACKAGES_DEFINITION = ProjectPackagesDefinition.of(ToolkitFixtures.class.getClassLoader()) : PACKAGES_DEFINITION;
    }

    public static SortedProperties loadVariables(Path variablesPath) {
        SortedProperties variables = new SortedProperties();
        URL resource;
        if (variablesPath != null) {
            try {
                resource = variablesPath.toUri().toURL();
            } catch (MalformedURLException e) {
                throw new IllegalStateException(e);
            }
        } else {
            resource = ToolkitFixtures.class.getResource("/fixtures/variables.properties");
            if (resource == null) {
                throw new IllegalStateException("Can't find /fixtures/variables.properties");
            }
        }
        try (Reader writer = new InputStreamReader(resource.openStream())) {
            variables.load(writer);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return variables;
    }

    protected static Properties getTestProperties() {
        if (testProperties == null) {
            testProperties = new Properties();
            try (InputStream inputStream = ToolkitFixtures.class.getClassLoader().getResourceAsStream(TEST_PROPERTIES_FILE_NAME)) {
                testProperties.load(inputStream);
            } catch (IOException e) {
                throw new IllegalStateException("unable to load test properties", e);
            }
        }
        return testProperties;
    }

    public static void injectCredentials(Path credentialsFile, String... keys) {
        if (Files.exists(credentialsFile)) {
            Properties credentialsStore = new Properties();
            try (BufferedReader reader = Files.newBufferedReader(credentialsFile)) {
                credentialsStore.load(reader);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            Properties properties = getTestProperties();
            addProperty(credentialsStore, properties, keys);
        }
    }


    private static void addProperty(Properties credentialsStore, Properties properties, String... keys) {
        for (String key : keys) {

            String property = credentialsStore.getProperty(key);
            properties.setProperty(key, property);
        }
    }

    public static File getCommonsDir() {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        return new File(baseDir, "commons");
    }

    public static File getTestBasedir(Class<?> testClass) {
        return new File(getTestsBasedir(), testClass.getSimpleName());
    }

    public static File getBasedir() {
        if (basedir == null) {
            String tmp = System.getProperty("basedir");
            if (tmp == null) {
                tmp = new File("").getAbsolutePath();
            }
            basedir = new File(tmp);
        }
        return basedir;
    }

    public static File getTestsBasedir() {
        if (testsBasedir == null) {
            testsBasedir = getBasedir().toPath().resolve("target").resolve("surefire-workdir").resolve("tests").resolve(System.nanoTime() + "").toFile();
        }
        return testsBasedir;
    }

    public static String getEntityVariableName(Class<?> dtoType) {
        return getEntityVariableName(dtoType, ".id");
    }

    public static String getEntityVariableName(Class<?> dtoType, String suffix) {
        return packagesDefinition().getRelativeEntityPackage(dtoType.getName()).substring(1) + suffix;
    }

    public static void addAssert(String property, Object asserts) {
        ToolkitFixtures.asserts.add(property + "=" + asserts);
    }

    public static Set<String> getAsserts() {
        return asserts;
    }

    public static void writeArrayAssert(String property, Object... data) {
        StringBuilder text1 = new StringBuilder();
        for (Object o : data) {
            text1.append("^").append(o);
        }
        String text = text1.length() == 0 ? "" : text1.substring(1);
        addAssert(property, text);
    }

    public String getVariableName(Class<?> dtoType) {
        return getVariableName(dtoType, ".id");
    }

    public String getVariableName(Class<?> dtoType, String suffix) {
        return Strings.removeEnd(packagesDefinition().getRelativeDtoPackage(dtoType.getName()).substring(1), "Dto") + suffix;
    }
}
