/*
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.dto;

import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class ScriptTest {

    protected static final String EXPRESSION = "parseFloat(a*(Math.pow(x, b)))";
    protected static final String EXPRESSION_RESULT = "[%1$-10s] '%2$s' nb eval %3$-5s in %4$-10s (~ %5$s)";
    /**
     * Logger
     */
    private static final Logger log = LogManager.getLogger(ScriptTest.class);
    final double a = 0.2f;
    final double b = 1.5f;
    final double x = 10f;
    protected ScriptEngineManager factory;

    @Before
    public void init() {
        factory = new ScriptEngineManager();
        List<ScriptEngineFactory> scriptEngineFactoryList = factory.getEngineFactories();
        for (ScriptEngineFactory factory : scriptEngineFactoryList) {
            if (log.isInfoEnabled()) {
                log.info("available factory [engine: " +
                                 factory.getEngineName() + " v." +
                                 factory.getEngineVersion() + "] [language: " +
                                 factory.getLanguageName() + ", version:" +
                                 factory.getLanguageVersion() + "], mime-types : " +
                                 "?" + factory.getMimeTypes() +
                                 "?" + factory.getExtensions());
            }
        }
    }

    @Test
    public void testScripts() throws Exception {

        doEvaluate(1);
        doEvaluate(10);
        doEvaluate(100);
        doEvaluate(1000);

        doEvaluate(1000);
        doEvaluate(100);
        doEvaluate(10);
        doEvaluate(1);
    }

    protected void doEvaluate(int nb) throws ScriptException {
        ScriptEngine engine = ObserveUtil.getScriptEngine();

        long t0 = System.nanoTime();
        for (int i = 0; i < nb; i++) {
            ScriptContext c = engine.getContext();

            c.setAttribute("a", a, ScriptContext.GLOBAL_SCOPE);
            c.setAttribute("x", x, ScriptContext.GLOBAL_SCOPE);
            c.setAttribute("b", b, ScriptContext.GLOBAL_SCOPE);
            Double o = (Double) engine.eval(EXPRESSION);
            Assert.assertEquals(a * Math.pow(x, b), o, 5);
        }
        long t1 = System.nanoTime();
        String result = String.format(EXPRESSION_RESULT,
                                      ObserveUtil.JS_ENGINE_NAME,
                                      EXPRESSION,
                                      nb,
                                      Strings.convertTime(t1 - t0),
                                      Strings.convertTime((t1 - t0) / nb));
        if (log.isInfoEnabled()) {
            log.info(result);
        }
    }
}
