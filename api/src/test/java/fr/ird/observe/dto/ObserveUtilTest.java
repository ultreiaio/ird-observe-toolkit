package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 12/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class ObserveUtilTest {

    static abstract class AbstractTaiste<A extends Number, B> {
        public void helloNumber(A number) {
            System.out.println("Hello number: " + number);
        }

        public void helloObject(B object) {
            System.out.println("Hello object: " + object);
        }

    }

    static class ConcreteTaiste<A extends Number, B> {
        public void helloNumber(A number) {
            System.out.println("Hello number: " + number);
        }

        public void helloObject(B object) {
            System.out.println("Hello object: " + object);
        }

    }

    @Test
    public void getType() {

        AbstractTaiste<Integer, String> abstractTaiste = new AbstractTaiste<Integer, String>() {
        };
        abstractTaiste.helloNumber(5);
        abstractTaiste.helloObject("Yo");
        Assert.assertEquals(Integer.class, ObserveUtil.getFirstType(abstractTaiste));
        Assert.assertEquals(String.class, ObserveUtil.getSecondType(abstractTaiste));
        ConcreteTaiste<Integer, String> concreteTaiste = new ConcreteTaiste<Integer, String>() {
        };
        concreteTaiste.helloNumber(4);
        concreteTaiste.helloObject("Ya");
        Assert.assertEquals(Integer.class, ObserveUtil.getFirstType(concreteTaiste));
        Assert.assertEquals(String.class, ObserveUtil.getSecondType(concreteTaiste));

    }

    @Test(expected = IllegalStateException.class)
    public void getTypeNotWorking() {

        ConcreteTaiste<Integer, String> concreteTaiste2 = new ConcreteTaiste<>();
        Assert.assertEquals(Integer.class, ObserveUtil.getFirstType(concreteTaiste2));


    }
}
