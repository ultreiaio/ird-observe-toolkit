package fr.ird.observe.dto;

/*-
 * #%L
 * Toolkit :: API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 17/02/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.6
 */
public class I18nDecoratorHelperTest {

    @Test
    public void fillCoordinateComponent() {
        assertFillCoordinateComponent(1.02f, " 001.0200");
        assertFillCoordinateComponent(-1.02f, "-001.0200");
        assertFillCoordinateComponent(-10.02f, "-010.0200");
        assertFillCoordinateComponent(-10.032f, "-010.0320");
        assertFillCoordinateComponent(10.032f, " 010.0320");
        assertFillCoordinateComponent(-100.0321f, "-100.0321");
    }

    private void assertFillCoordinateComponent(Float component, String excepted) {
        String actual = I18nDecoratorHelper.fillCoordinateComponent(3, component);
        Assert.assertEquals(excepted, actual);
    }
}
