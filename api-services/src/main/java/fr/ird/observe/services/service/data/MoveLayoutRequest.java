package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Objects;
import java.util.Set;

/**
 * To move some layout of a data.
 * <p>
 * Created on 02/04/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class MoveLayoutRequest implements JsonAware {
    /**
     * All data types to move.
     */
    private final Set<Class<? extends DataDto>> scopes;
    /**
     * Current parent id of the data entry point to process.
     */
    private final String oldParentId;
    /**
     * Target parent id of the data entry point to receive data.
     */
    private final String newParentId;
    /**
     * to select target node after move operation.
     */
    private final boolean selectTarget;

    public MoveLayoutRequest(Class<? extends DataDto> scope, String oldParentId, String newParentId, boolean selectTarget) {
        this(Set.of(scope), oldParentId, newParentId, selectTarget);
    }

    public MoveLayoutRequest(Set<Class<? extends DataDto>> scopes, String oldParentId, String newParentId, boolean selectTarget) {
        this.scopes = Objects.requireNonNull(scopes);
        this.oldParentId = Objects.requireNonNull(oldParentId);
        this.newParentId = Objects.requireNonNull(newParentId);
        this.selectTarget = selectTarget;
    }

    public Set<Class<? extends DataDto>> getScopes() {
        return scopes;
    }

    public String getOldParentId() {
        return oldParentId;
    }

    public String getNewParentId() {
        return newParentId;
    }

    public boolean isSelectTarget() {
        return selectTarget;
    }
}
