package fr.ird.observe.services;

/*
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import io.ultreia.java4all.http.service.ServiceInitializerImpl;

import java.util.Date;

/**
 * Created on 31/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveServiceInitializer extends ServiceInitializerImpl<ObserveServiceInitializerConfig, ObserveDataSourceConfiguration, ObserveDataSourceConnection> {

    public ObserveServiceInitializer(ObserveServiceInitializer initializer, ObserveDataSourceConfiguration dataSourceConfiguration, ObserveDataSourceConnection dataSourceConnection) {
        super(initializer, dataSourceConfiguration, dataSourceConnection);
    }

    public ObserveServiceInitializer(ObserveServiceInitializerConfig initializer, ObserveDataSourceConfiguration dataSourceConfiguration, ObserveDataSourceConnection dataSourceConnection) {
        super(initializer, dataSourceConfiguration, dataSourceConnection);
    }

    public Date now() {
        return new Date();
    }
}
