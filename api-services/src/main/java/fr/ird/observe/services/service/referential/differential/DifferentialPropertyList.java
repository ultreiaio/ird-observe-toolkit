package fr.ird.observe.services.service.referential.differential;
/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.IdDto;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a list of property modification.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class DifferentialPropertyList {

    private final List<DifferentialProperty> properties;

    public static DifferentialPropertyList of(Map<String, Object[]> modifiedProperties) {
        if (modifiedProperties != null && !modifiedProperties.isEmpty()) {
            List<DifferentialProperty> builder = new LinkedList<>();
            for (Map.Entry<String, Object[]> entry : modifiedProperties.entrySet()) {
                String propertyName = entry.getKey();
                Object[] value = entry.getValue();
                builder.add(new DifferentialProperty(propertyName, value[0], value[1]));
            }
            return new DifferentialPropertyList(builder);
        }
        return null;
    }

    public DifferentialPropertyList(List<DifferentialProperty> properties) {
        this.properties = properties;
    }

    public DifferentialPropertyList of(Collection<String> propertyNames, boolean flip) {
        if (propertyNames != null && !propertyNames.isEmpty() && properties != null) {
            List<DifferentialProperty> builder = new LinkedList<>();
            for (DifferentialProperty property : properties) {
                if (propertyNames.contains(property.getPropertyName())) {
                    builder.add(flip ? property.flip() : property);
                }
            }
            return new DifferentialPropertyList(builder);
        }
        return null;
    }

    public List<DifferentialProperty> getProperties() {
        return properties;
    }

    public List<String> getPropertyNameLabels(Class<? extends IdDto> dtoType) {
        List<String> builder = new LinkedList<>();
        if (properties != null) {
            for (DifferentialProperty property : properties) {
                builder.add(property.getPropertyNameLabel(dtoType));
            }
        }
        return builder;
    }

    public boolean isEmpty() {
        return properties == null || properties.isEmpty();
    }

    public Set<String> getPropertyNames() {
        return isEmpty() ? null : properties.stream().map(DifferentialProperty::getPropertyName).collect(Collectors.toSet());
    }
}
