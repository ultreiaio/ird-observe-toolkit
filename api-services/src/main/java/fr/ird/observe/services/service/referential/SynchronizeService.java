package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Set;

/**
 * Unify referential synchronize service. Replace both {@code legacy} and {@code ng} version.
 * <p>
 * This new version is based on the {@code ng} version ({@code legacy} version was just another way to only produce
 * for one side).
 * <p>
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
@Service
public interface SynchronizeService extends ObserveService {
    String SIMPLE_REFERENTIAL_SYNCHRONISATION = "SIMPLE_REFERENTIAL_SYNCHRONISATION";
    String ADVANCED_REFERENTIAL_SYNCHRONISATION = "ADVANCED_REFERENTIAL_SYNCHRONISATION";

    /**
     * From the given {@code request} which defines all sql requests that should be produced from this side, generate
     * the sql code and return them.
     *
     * @param request one side request producer
     * @return all sql requests to be generated from this side
     */
    // Note : sur cette méthode aucun droit métier à poser, les actions sont légitimes
    @Post(useMultiPartForm = true, timeOut = 120)
    @MethodCredential
    OneSideSqlResult produceSqlResult(OneSideSqlRequest request);

    /**
     * Apply the given {@code script} on this side and update lastUpdateDates.
     *
     * @param script            the sql request to execute (optional)
     * @param lastUpdateDateKey the lastUpdateDate key
     */
    // Note : sur cette méthode aucun droit métier à poser, les actions sont légitimes
    @Write
    @Post(useMultiPartForm = true)
    @MethodCredential
    void applySql(@Nullable SqlScript script, String lastUpdateDateKey);

    /**
     * Pour un référentiel d'un type donné (son nom est donné), détecte les référentiels dont
     * les identifiants sont passés en paramètres qui sont réellement utilisés dans la source locale.
     *
     * @param referentialName le nom du référentiel
     * @param ids             les identifiants du référentiel dont on recherche le nombre d'utilisation
     * @return les identifiants des référentiels passés en paramètres qui sont réellement utilisés dans la source locale.
     */
    @MethodCredential(Permission.READ_REFERENTIAL)
    @Post(useMultiPartForm = true)
    Set<String> filterIdsUsed(Class<? extends ReferentialDto> referentialName, Set<String> ids);

    /**
     * Pour récupérer les références sur les référentiels d'un certain type à supprimer,
     * cet ensemble servira à les afficher dans l'interface graphique pour effectuer le remplacement.
     *
     * @param referentialName le nom du référentiel à récupérer
     * @param ids             les identifiants des référentiels à supprimer
     * @return l'ensemble des références de référentiel à supprimer.
     */
    @MethodCredential(Permission.READ_REFERENTIAL)
    @Post(useMultiPartForm = true)
    Set<ToolkitIdLabel> getReferentialToDelete(Class<? extends ReferentialDto> referentialName, Set<String> ids);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> Set<ToolkitIdLabel> getEnabledReferentialLabelSet(Class<D> type);
}
