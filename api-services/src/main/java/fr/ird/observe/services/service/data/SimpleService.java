package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.data.SimpleDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.SaveResultDto;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

/**
 * Created on 26/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
//@ServiceInternal(value = "D", type = MetaModelNodeType.Simple)
//@Internal
@Service
public interface SimpleService extends ObserveService {

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends SimpleDto> Form<D> loadForm(Class<D> dtoType, String id);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    <D extends SimpleDto> SaveResultDto save(D dto) throws ConcurrentModificationException;
}
