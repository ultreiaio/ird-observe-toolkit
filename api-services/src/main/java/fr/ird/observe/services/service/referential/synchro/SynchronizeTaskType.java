package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.LeftOrRight;
import io.ultreia.java4all.util.LeftOrRightContext;

import java.util.function.Function;

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public enum SynchronizeTaskType {

    ADD(false, true, false),
    UPDATE(true, true, false),
    REVERT(true, false, true),
    DELETE(false, false, false),
    DEACTIVATE(false, false, false),
    DEACTIVATE_WITH_REPLACEMENT(false, false, false);

    private final boolean withProperties;
    private final boolean toOtherSide;
    private final boolean useOtherSideSqlBuilder;

    SynchronizeTaskType(boolean withProperties, boolean toOtherSide, boolean useOtherSideSqlBuilder) {
        this.withProperties = withProperties;
        this.toOtherSide = toOtherSide;
        this.useOtherSideSqlBuilder = useOtherSideSqlBuilder;
    }

    public boolean withProperties() {
        return withProperties;
    }

    public boolean toOtherSide() {
        return toOtherSide;
    }

    public <O> Function<LeftOrRight, O> chooseSqlBuilder(LeftOrRightContext<O> sides) {
        return useOtherSideSqlBuilder ? sides::onOppositeSide : sides::onSameSide;
    }
}
