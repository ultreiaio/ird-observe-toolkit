package fr.ird.observe.services.service;

/*
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.request.CopyRequest;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.datasource.request.DeleteRequest;
import fr.ird.observe.datasource.request.ReplicateRequest;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.BusinessDto;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Date;
import java.util.Set;

/**
 * Created on 21/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Service
public interface DataSourceService extends ObserveService {

    @Get
    void close();

    @Get
    @MethodCredential
    <D extends BusinessDto> boolean isIdValid(Class<D> type, String id);

    @Get
    @MethodCredential
    Date getLastUpdateDate(String type);

    @Post
    @MethodCredential(Permission.READ_DATA)
    Set<String> retainExistingIds(Set<String> ids);

    /**
     * Produce a sql script for the given {@code create request}.
     *
     * <b>Node:</b> permissions are check in the implementation method, since we may not need the same credentials for
     * different requests, some just need READ_REFERENTIAL, other READ_ALL.
     *
     * @param request request to consume
     * @return the generated sql script.
     */
    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.NONE)
    SqlScript produceCreateSqlScript(CreateDatabaseRequest request);

    /**
     * Produce a sql script for the given {@code add request}.
     *
     * @param request request to consume
     * @return the generated sql script.
     */
    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL)
    SqlScript produceAddSqlScript(CopyRequest request);

    /**
     * Produce a sql script for the given {@code move request}.
     *
     * @param request request to consume
     * @return the generated sql script.
     */
    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL)
    SqlScript produceMoveSqlScript(ReplicateRequest request);

    /**
     * Produce a sql script for the given {@code delete request}.
     *
     * @param request request to consume
     * @return the generated sql script.
     */
    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL)
    SqlScript produceDeleteSqlScript(DeleteRequest request);

    /**
     * Execute a sql script.
     *
     * @param sqlScript sql script to execute.
     */
    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    @Write
    void executeSqlScript(SqlScript sqlScript);
}
