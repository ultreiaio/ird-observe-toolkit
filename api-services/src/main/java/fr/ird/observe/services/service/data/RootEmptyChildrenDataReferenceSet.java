package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class RootEmptyChildrenDataReferenceSet<D extends RootOpenableDto, R extends DataDtoReference> extends LazyServiceDataDtoReferenceSet<D, R, RootOpenableService> {

    private final DataGroupByParameter parentProperty;

    public RootEmptyChildrenDataReferenceSet(Class<D> dtoType, Class<R> referenceType, Comparator<R> comparator, Supplier<RootOpenableService> serviceSupplier, DecoratorService decoratorService, DataGroupByDto<?> parentProperty, String groupByFlavor) {
        super(dtoType, referenceType, (int) Objects.requireNonNull(parentProperty).getCount(), comparator, serviceSupplier, decoratorService);
        this.parentProperty = parentProperty.toParameter(groupByFlavor);
    }

    @Override
    protected DataDtoReferenceSet<R> createInitialResult(RootOpenableService service) {
        return service.getChildren(parentProperty);
    }

    @Override
    protected UpdatedDataDtoReferenceSet<R> createUpdatedResult(RootOpenableService service, DataDtoReferenceSet<R> previousResult) {
        return service.getChildrenUpdate(parentProperty, previousResult.getLastUpdate());
    }

    public DataGroupByParameter getParentProperty() {
        return parentProperty;
    }
}

