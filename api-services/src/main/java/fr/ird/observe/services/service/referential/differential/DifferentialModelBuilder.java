package fr.ird.observe.services.service.referential.differential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.TimeLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 13/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class DifferentialModelBuilder {

    private static final Logger log = LogManager.getLogger(DifferentialModelBuilder.class);
    private static final TimeLog timeLog = new TimeLog(DifferentialModelBuilder.class, 0, 100);

    /**
     * To compare dto on both sides.
     */
    private final Comparator<ReferentialDto> dtoComparator = (o1, o2) -> {
        long result = (o1.getLastUpdateDate().getTime() - o2.getLastUpdateDate().getTime());
        if (result == 0) {
            // equals from lastUpdateDate, fall back on topiaVersion
            result = (o1.getTopiaVersion() - o2.getTopiaVersion());
        }
        return result == 0 ? 0 : result > 0 ? 1 : -1;
    };
    private final Locale locale;
    private final Supplier<DifferentialMetaModel> metaModelSupplier;
    private final LeftOrRightContext<DifferentialModelBuilderSide> sides;
    private ProgressionModel progressionModel;

    public DifferentialModelBuilder(Locale locale,
                                    Supplier<DifferentialMetaModel> metaModelSupplier,
                                    LeftOrRightContext<Function<Class<? extends ReferentialDto>, Set<? extends ReferentialDto>>> dtoSuppliers) {
        this.locale = Objects.requireNonNull(locale);
        this.metaModelSupplier = SingletonSupplier.of(Objects.requireNonNull(metaModelSupplier));
        this.sides = dtoSuppliers.apply(DifferentialModelBuilderSide::new);
    }

    LeftOrRightContext<DifferentialModelBuilderSide> getSides() {
        return sides;
    }

    public DifferentialMetaModel metaModel() {
        return metaModelSupplier.get();
    }

    public LeftOrRightContext<DifferentialList> build(ProgressionModel progressionModel) {

        DifferentialMetaModel metaModel = metaModel();

        long startTime = TimeLog.getTime();
        int typesCount = metaModel.size();
        setProgressionModel(progressionModel, typesCount * 4);

        int index = 0;
        for (Map.Entry<Class<? extends ReferentialDto>, List<String>> entry : metaModel.entriesByNaturalOrder()) {
            processOneType(typesCount, ++index, entry);
        }
        timeLog.log(startTime, "create");
        return getSides().apply(DifferentialModelBuilderSide::build);
    }

    protected void processOneType(int typesCount, int index, Map.Entry<Class<? extends ReferentialDto>, List<String>> entry) {
        long t0 = TimeLog.getTime();
        Class<? extends ReferentialDto> referentialType = entry.getKey();
        Collection<String> businessProperties = entry.getValue();

        DifferentialModelBuilderSide leftSide = sides.left();
        int oldLefCount = leftSide.getDifferentialBuilder().size();
        DifferentialModelBuilderSide rightSide = sides.right();
        int oldRightCount = rightSide.getDifferentialBuilder().size();

        String typeName = Objects.requireNonNull(I18nDecoratorHelper.getType(referentialType));

        log.debug(setMessage(t("observe.referential.diff.loading.type", index, typesCount, typeName)));

        buildForType(referentialType, typeName, businessProperties);

        int newLeftCount = leftSide.getDifferentialBuilder().size();
        int newRightCount = rightSide.getDifferentialBuilder().size();

        log.info(setMessage(String.format("%s → Found %d differences on left and %d on right.", typeName, newLeftCount - oldLefCount, newRightCount - oldRightCount)));

        timeLog.log(t0, "register type", referentialType.getName());
    }

    protected <D extends ReferentialDto> void buildForType(Class<D> dtoType, String typeName, Collection<String> businessProperties) {

        Decorator decorator = DecoratorProvider.get().decorator(locale, dtoType);

        DifferentialModelBuilderSide leftSide = sides.left();
        @SuppressWarnings("unchecked") Set<D> leftSideDtoList = (Set<D>) leftSide.getDtoSupplier().apply(dtoType);
        leftSideDtoList.forEach(r -> r.registerDecorator(decorator));
        String leftLogPrefix = logPrefix(true);
        log.info(setMessage(String.format("%s → %s - Load %d referential.", typeName, leftLogPrefix, leftSideDtoList.size())));

        DifferentialModelBuilderSide rightSide = sides.right();
        @SuppressWarnings("unchecked") Set<D> rightSideDtoList = (Set<D>) rightSide.getDtoSupplier().apply(dtoType);
        rightSideDtoList.forEach(r -> r.registerDecorator(decorator));
        rightSide.getIds().computeIfAbsent(dtoType, k -> new TreeSet<>()).addAll(rightSideDtoList.stream().map(ReferentialDto::getTopiaId).collect(Collectors.toSet()));
        String rightLogPrefix = logPrefix(false);
        log.info(setMessage(String.format("%s → %s - Load %d referential.", typeName, rightLogPrefix, rightSideDtoList.size())));

        boolean onReferential = ReferentialDto.class.isAssignableFrom(dtoType);

        if (leftSideDtoList.isEmpty() && rightSideDtoList.isEmpty()) {
            log.info(String.format("%s - No id to process.", dtoType.getName()));
            return;
        }

        Collection<D[]> diffMap = computeDiffMap(dtoType, leftSideDtoList, rightSideDtoList);

        log.info(String.format("%s - Found %d id(s) to process.", dtoType.getName(), diffMap.size()));

        for (D[] pair : diffMap) {
            D leftSideDto = pair[0];
            D rightSideDto = pair[1];
            if (rightSideDto == null) {
                // only on left side
                leftSide.getDifferentialBuilder().add(newDifferential(dtoType, DifferentialType.ADDED, leftSideDto, null, null, leftLogPrefix));
                continue;
            }
            if (leftSideDto == null) {
                // only on right side
                rightSide.getDifferentialBuilder().add(newDifferential(dtoType, DifferentialType.ADDED, rightSideDto, null, null, rightLogPrefix));
                continue;
            }
            int compareLastUpdateDate = dtoComparator.compare(leftSideDto, rightSideDto);
            if (0 == compareLastUpdateDate) {
                // same version
                continue;
            }

            // something may be modified
            boolean leftSideDtoNewer = compareLastUpdateDate > 0;
            Map<String, Object[]> modifiedProperties = buildDtoModifiedProperties(businessProperties, leftSideDto, rightSideDto, leftSideDtoNewer);
            if (modifiedProperties.isEmpty()) {
                // equals (no real business properties modified), no differential state
                continue;
            }

            DifferentialPropertyList modifications = DifferentialPropertyList.of(modifiedProperties);
            boolean containsEnabledProperty = onReferential && modifiedProperties.containsKey(ReferentialDto.PROPERTY_ENABLED);

            DifferentialType updateType = getDtoUpdateType(containsEnabledProperty, leftSideDtoNewer ? rightSideDto : leftSideDto);
            if (leftSideDtoNewer) {
                // modified on left side
                leftSide.getDifferentialBuilder().add(newDifferential(dtoType, updateType, leftSideDto, rightSideDto, modifications, leftLogPrefix));
            } else {
                // modified on right side
                rightSide.getDifferentialBuilder().add(newDifferential(dtoType, updateType, rightSideDto, leftSideDto, modifications, rightLogPrefix));
            }
        }
    }

    protected <D extends ReferentialDto> Collection<D[]> computeDiffMap(Class<D> dtoType, Set<D> leftSideDtoList, Set<D> rightSideDtoList) {
        Map<String, D[]> diffMap = new TreeMap<>();
        for (D leftSideDto : leftSideDtoList) {
            String topiaId = leftSideDto.getTopiaId();
            @SuppressWarnings("unchecked") D[] pair = (D[]) Array.newInstance(dtoType, 2);
            pair[0] = leftSideDto;
            diffMap.put(topiaId, pair);
        }
        for (D rightSideDto : rightSideDtoList) {
            String topiaId = rightSideDto.getTopiaId();
            diffMap.compute(topiaId, (s, pair) -> {
                if (pair == null) {
                    // not existing on left
                    @SuppressWarnings("unchecked") D[] newPair = (D[]) Array.newInstance(dtoType, 2);
                    pair = newPair;
                }
                pair[1] = rightSideDto;
                return pair;
            });
        }
        return diffMap.values();
    }

    protected <D extends ReferentialDto> Map<String, Object[]> buildDtoModifiedProperties(Collection<String> businessProperties, D leftSideDto, D rightSideDto, boolean leftSideDtoNewer) {
        Map<String, Object[]> modifiedProperties = new LinkedHashMap<>();
        for (String propertyName : businessProperties) {
            Object leftValue = getDtoPropertyValue(leftSideDto, propertyName);
            Object rightValue = getDtoPropertyValue(rightSideDto, propertyName);
            if (Objects.equals(leftValue, rightValue)) {
                continue;
            }
            Object[] pair = new Object[2];
            if (leftSideDtoNewer) {
                pair[0] = rightValue;
                pair[1] = leftValue;
            } else {
                pair[0] = leftValue;
                pair[1] = rightValue;
            }
            modifiedProperties.put(propertyName, pair);
        }
        return modifiedProperties;
    }

    protected <D extends ReferentialDto> Differential newDifferential(Class<D> dtoType, DifferentialType stateType, D thisSideDto, D otherSideDto, DifferentialPropertyList modifiedProperties, String logPrefix) {
        Differential state = new Differential(dtoType, stateType, thisSideDto.toTechnicalLabel(), otherSideDto == null ? null : otherSideDto.toTechnicalLabel(), modifiedProperties);
        log.info(String.format("%s → %s - add %s", dtoType.getName(), logPrefix, state));
        return state;
    }

    protected <D extends ReferentialDto> Object getDtoPropertyValue(D entity, String propertyName) {
        Object result = entity.get(propertyName);
        if (result instanceof Float) {
            result = ((Float) result).doubleValue();
        } else if (result instanceof Date) {
            result = ((Date) result).getTime();
        } else if (result instanceof ReferentialDtoReference) {
            Decorator decorator = DecoratorProvider.get().decorator(locale, result.getClass());
            ((ReferentialDtoReference) result).registerDecorator(decorator);
        }
        return result;
    }

    protected <D extends ReferentialDto> DifferentialType getDtoUpdateType(boolean containsEnabledProperty, D otherSideEntity) {
        return containsEnabledProperty && otherSideEntity.isEnabled() ? DifferentialType.DISABLED : DifferentialType.MODIFIED;
    }

    protected String logPrefix(boolean left) {
        return String.format("[ %s ]", left ? "left " : "right");
    }

    private void setProgressionModel(ProgressionModel progressionModel, int maxSteps) {
        this.progressionModel = progressionModel;
        if (progressionModel != null) {
            progressionModel.setMaximum(maxSteps);
        }
    }

    private String setMessage(String message) {
        if (progressionModel != null) {
            progressionModel.setMessage(message);
        }
        return message;
    }

    static class DifferentialModelBuilderSide {
        private final Function<Class<? extends ReferentialDto>, Set<? extends ReferentialDto>> dtoSupplier;

        private final List<Differential> differentialBuilder;
        private final Map<Class<? extends ReferentialDto>, Set<String>> ids;

        DifferentialModelBuilderSide(Function<Class<? extends ReferentialDto>, Set<? extends ReferentialDto>> dtoSupplier) {
            this.dtoSupplier = Objects.requireNonNull(dtoSupplier);
            this.ids = new LinkedHashMap<>();
            this.differentialBuilder = new LinkedList<>();
        }

        public Function<Class<? extends ReferentialDto>, Set<? extends ReferentialDto>> getDtoSupplier() {
            return dtoSupplier;
        }

        public List<Differential> getDifferentialBuilder() {
            return differentialBuilder;
        }

        public Map<Class<? extends ReferentialDto>, Set<String>> getIds() {
            return ids;
        }

        public DifferentialList build() {
            return new DifferentialList(ids, differentialBuilder);
        }
    }

}
