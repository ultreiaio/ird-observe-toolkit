package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Contient l'ensemble des résultats du call back utilisateur.
 * <p>
 * Created on 12/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalCallbackResults {

    private final Map<Class<? extends ReferentialDto>, UnidirectionalCallbackResult<?>> callbackResults;

    public UnidirectionalCallbackResults() {
        callbackResults = new LinkedHashMap<>();
    }

    public <D extends ReferentialDto> void addCallbackResult(Class<D> referentialName, String idToReplace, String replaceId) {
        UnidirectionalCallbackResult<D> callbackResult = getCallbackResult(referentialName);
        if (callbackResult == null) {
            callbackResult = new UnidirectionalCallbackResult<>(referentialName);
            callbackResults.put(referentialName, callbackResult);
        }
        callbackResult.addId(idToReplace, replaceId);
    }

    @SuppressWarnings("unchecked")
    public <D extends ReferentialDto> UnidirectionalCallbackResult<D> getCallbackResult(Class<D> referentialName) {
        return (UnidirectionalCallbackResult<D>) callbackResults.get(referentialName);
    }

    public <D extends ReferentialDto> boolean containsReferentialName(Class<D> referentialName) {
        return callbackResults.containsKey(referentialName);
    }
}
