package fr.ird.observe.services;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.http.service.ServiceInitializerConfig;
import io.ultreia.java4all.util.Version;

import java.io.File;
import java.util.Locale;

/**
 * Contains the service configuration.
 * <p>
 * Created on 05/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.7
 */
public class ObserveServiceInitializerConfig implements ServiceInitializerConfig {
    /**
     * Application locale used for any translation.
     */
    private final Locale applicationLocale;
    /**
     * Referential locale used for any translation in database.
     */
    private final ReferentialLocale referentialLocale;
    /**
     * Temporary directory used for any internal.
     */
    private final File temporaryDirectoryRoot;
    /**
     * Le temps en millisecondes maximum de tentative de connection http.
     *
     * @since 6.0
     */
    private final int httpTimeout;
    /**
     * Persistence model version.
     */
    private final Version persistenceModelVersion;
    /**
     * Application build version.
     */
    private final Version applicationBuildVersion;
    /**
     * Validation mode.
     */
    private DataSourceValidationMode validationMode;

    public ObserveServiceInitializerConfig(Locale applicationLocale,
                                           ReferentialLocale referentialLocale,
                                           File temporaryDirectoryRoot,
                                           int httpTimeout,
                                           Version persistenceModelVersion,
                                           Version applicationBuildVersion) {
        this.applicationLocale = applicationLocale;
        this.referentialLocale = referentialLocale;
        this.temporaryDirectoryRoot = temporaryDirectoryRoot;
        this.httpTimeout = httpTimeout;
        this.persistenceModelVersion = persistenceModelVersion;
        this.applicationBuildVersion = applicationBuildVersion;
    }

    public Locale getApplicationLocale() {
        return applicationLocale;
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public File getTemporaryDirectoryRoot() {
        return temporaryDirectoryRoot;
    }

    public int getHttpTimeout() {
        return httpTimeout;
    }

    public boolean withApplicationLocale() {
        return getApplicationLocale() != null;
    }

    public boolean withReferentialLocale() {
        return getReferentialLocale() != null;
    }

    public Version getPersistenceModelVersion() {
        return persistenceModelVersion;
    }

    public Version getApplicationBuildVersion() {
        return applicationBuildVersion;
    }

    public DataSourceValidationMode getValidationMode() {
        return validationMode;
    }

    public void setValidationMode(DataSourceValidationMode validationMode) {
        this.validationMode = validationMode;
    }
}
