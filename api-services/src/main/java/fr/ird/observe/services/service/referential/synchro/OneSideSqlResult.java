package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class OneSideSqlResult implements JsonAware {

    /**
     * A prefix to add on some association statements to be able to sort distinguish from other statements.
     */
    public static final String ASSOCIATION_STATEMENT_PREFIX = "$$_";
    private static final Logger log = LogManager.getLogger(OneSideSqlResult.class);

    private final List<String> insertAssociationSqlCode;
    private final List<String> insertSqlCode;
    private final List<String> updateSqlCode;
    private final List<String> updateAssociationSqlCode;
    private final List<String> deleteAssociationSqlCode;
    private final List<String> deleteSqlCode;
    private final List<String> deactivateSqlCode;

    @SuppressWarnings("UnusedReturnValue")
    public static class Builder {

        private final List<String> addAssociationTasksBuilder = new LinkedList<>();
        private final List<String> addTasksBuilder = new LinkedList<>();
        private final List<String> updateTasksBuilder = new LinkedList<>();
        private final List<String> updateAssociationTasksBuilder = new LinkedList<>();
        private final List<String> deleteTasksBuilder = new LinkedList<>();
        private final List<String> deleteAssociationTasksBuilder = new LinkedList<>();
        private final List<String> deactivateTasksBuilder = new LinkedList<>();
        private final Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide;

        public Builder(Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {
            this.idsOnlyExistingOnThisSide = idsOnlyExistingOnThisSide;
        }

        public Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide() {
            return idsOnlyExistingOnThisSide;
        }

        public OneSideSqlResult build() {
            return new OneSideSqlResult(addTasksBuilder,
                                        addAssociationTasksBuilder,
                                        updateTasksBuilder,
                                        updateAssociationTasksBuilder,
                                        deleteTasksBuilder,
                                        deleteAssociationTasksBuilder,
                                        deactivateTasksBuilder);
        }

        public OneSideSqlResult.Builder addInsertStatement(String sql) {
            log.debug(String.format("Add add sql: %s", sql));
            if (sql.startsWith(ASSOCIATION_STATEMENT_PREFIX)) {
                addAssociationTasksBuilder.add(unboxAssociationStatement(sql));
            } else {
                addTasksBuilder.add(sql);
            }
            return this;
        }

        public OneSideSqlResult.Builder addUpdateStatement(String sql) {
            log.debug(String.format("Add update sql: %s", sql));
            if (sql.startsWith(ASSOCIATION_STATEMENT_PREFIX)) {
                updateAssociationTasksBuilder.add(unboxAssociationStatement(sql));
            } else {
                updateTasksBuilder.add(sql);
            }
            return this;
        }

        public OneSideSqlResult.Builder addDeleteStatement(String sql) {
            log.debug(String.format("Add delete sql: %s", sql));
            if (sql.startsWith(ASSOCIATION_STATEMENT_PREFIX)) {
                deleteAssociationTasksBuilder.add(unboxAssociationStatement(sql));
            } else {
                deleteTasksBuilder.add(sql);
            }
            return this;
        }

        public OneSideSqlResult.Builder addDeactivateStatement(String sql) {
            log.debug(String.format("Add deactivate sql: %s", sql));
            deactivateTasksBuilder.add(sql);
            return this;
        }
    }

    public static String unboxAssociationStatement(String statement) {
        if (statement.startsWith(ASSOCIATION_STATEMENT_PREFIX)) {
            return statement.substring(ASSOCIATION_STATEMENT_PREFIX.length());
        }
        return statement;
    }

    private OneSideSqlResult(List<String> insertSqlCode,
                             List<String> insertAssociationSqlCode,
                             List<String> updateSqlCode,
                             List<String> updateAssociationSqlCode,
                             List<String> deleteSqlCode,
                             List<String> deleteAssociationSqlCode,
                             List<String> deactivateSqlCode) {
        this.insertSqlCode = insertSqlCode;
        this.insertAssociationSqlCode = insertAssociationSqlCode;
        this.updateSqlCode = updateSqlCode;
        this.updateAssociationSqlCode = updateAssociationSqlCode;
        this.deleteSqlCode = deleteSqlCode;
        this.deleteAssociationSqlCode = deleteAssociationSqlCode;
        this.deactivateSqlCode = deactivateSqlCode;
    }

    public List<String> getInsertSqlCode() {
        return insertSqlCode;
    }

    public List<String> getUpdateSqlCode() {
        return updateSqlCode;
    }

    public List<String> getDeleteSqlCode() {
        return deleteSqlCode;
    }

    public List<String> getInsertAssociationSqlCode() {
        return insertAssociationSqlCode;
    }

    public List<String> getUpdateAssociationSqlCode() {
        return updateAssociationSqlCode;
    }

    public List<String> getDeleteAssociationSqlCode() {
        return deleteAssociationSqlCode;
    }

    public List<String> getDeactivateSqlCode() {
        return deactivateSqlCode;
    }

    public Optional<SqlScript> toSqlScript(Path scriptPath, OneSideSqlResult oppositeSqlResult) {
        List<String> statements = new LinkedList<>(getDeleteAssociationSqlCode());
        if (oppositeSqlResult != null) {
            statements.addAll(oppositeSqlResult.getInsertSqlCode());
            statements.addAll(oppositeSqlResult.getInsertAssociationSqlCode());
            statements.addAll(oppositeSqlResult.getUpdateSqlCode());
            statements.addAll(oppositeSqlResult.getUpdateAssociationSqlCode());
        }
        statements.addAll(getDeactivateSqlCode());
        statements.addAll(getDeleteSqlCode());
        if (statements.isEmpty()) {
            return Optional.empty();
        }
        if (Files.notExists(scriptPath.getParent())) {
            try {
                Files.createDirectories(scriptPath.getParent());
            } catch (IOException e) {
                throw new IllegalStateException(String.format("Can't create directory: %s", scriptPath.getParent()), e);
            }
        }
        try (BufferedWriter writer = Files.newBufferedWriter(scriptPath)) {
            for (String statement : statements) {
                writer.write(statement.trim());
                writer.newLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException("Can't write to " + scriptPath, e);
        }
        return Optional.of(SqlScript.of(scriptPath));
    }

}

