package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Service;

/**
 * Created on 10/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
@Service
public interface ReferenceService extends ObserveService {

    @Get
    @MethodCredential(Permission.READ_DATA)
    <R extends DataDtoReference> R createData(Class<R> type);

    @Get
    @MethodCredential(Permission.READ_DATA)
    <R extends DataDtoReference> R loadData(Class<R> type, String id, @Nullable String classifier);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <R extends ReferentialDtoReference> R createReferential(Class<R> type);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <R extends ReferentialDtoReference> R loadReferential(Class<R> type, String id);

}
