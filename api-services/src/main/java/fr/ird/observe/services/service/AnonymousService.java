package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConnection;
import fr.ird.observe.datasource.configuration.ObserveDataSourceInformation;
import fr.ird.observe.datasource.security.BabModelVersionException;
import fr.ird.observe.datasource.security.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.datasource.security.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import fr.ird.observe.datasource.security.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.datasource.security.model.DataSourceUserDto;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Set;

/**
 * Created on 17/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
@Service(anonymous = true)
public interface AnonymousService extends ObserveService {

    @Get
    Version getModelVersion();

    @Get
    Version getServerVersion();

    @Get
    ObserveDataSourceInformation checkCanConnect(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException;

    @Get
    ObserveDataSourceInformation checkCanConnectOrBeEmpty(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException;

    @Post
    ObserveDataSourceConnection createEmpty(ObserveDataSourceConfiguration config) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException;

    @Post(useMultiPartForm = true)
    ObserveDataSourceConnection createFromDump(ObserveDataSourceConfiguration config, SqlScript dump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException;

    @Post(useMultiPartForm = true)
    ObserveDataSourceConnection createFromImport(ObserveDataSourceConfiguration config, SqlScript dump, @Nullable SqlScript optionalDump) throws IncompatibleDataSourceCreateConfigurationException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException;

    @Get
    ObserveDataSourceConnection open(ObserveDataSourceConfiguration config) throws DatabaseNotFoundException, DatabaseConnexionNotAuthorizedException, BabModelVersionException;

    @Get
    Set<DataSourceUserDto> getUsers(ObserveDataSourceConfiguration config);

    @Get(timeOut = 100)
    void applySecurity(ObserveDataSourceConfiguration config, Set<DataSourceUserDto> users);

    @Get(timeOut = 100)
    void migrateData(ObserveDataSourceConfiguration config);

    @Get
    String generateHomeId();

    /**
     * @return set of available databases (only available for server service)
     */
    @Get
    Set<String> getAvailableDatabaseNames();
}
