package fr.ird.observe.services.service.referential.differential;
/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Defines a list of differential states from a same side of a source.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class DifferentialList implements JsonAware {

    /**
     * All ids on this side.
     */
    private final Map<Class<? extends ReferentialDto>, Set<String>> allIds;
    /**
     * All registered differential states on this side.
     */
    private final List<Differential> states;

    public DifferentialList(Map<Class<? extends ReferentialDto>, Set<String>> allIds, List<Differential> states) {
        this.allIds = allIds;
        this.states = states;
    }

    public List<Differential> getStates() {
        return states;
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getAllIds() {
        return allIds;
    }

    public Map<Class<? extends ReferentialDto>, DifferentialList> getStatesByType() {
        Map<Class<? extends ReferentialDto>, List<Differential>> result = new LinkedHashMap<>();
        for (Differential s : states) {
            List<Differential> builder = result.computeIfAbsent(s.getDtoType(), t -> new LinkedList<>());
            builder.add(s);
        }
        Map<Class<? extends ReferentialDto>, DifferentialList> builder = new LinkedHashMap<>();
        result.forEach((k, v) -> builder.put(k, new DifferentialList(allIds, v)));
        return builder;
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getNewIds() {
        Map<Class<? extends ReferentialDto>, Set<String>> result = new LinkedHashMap<>();
        states.stream().filter(s -> s.getDifferentialType() == DifferentialType.ADDED).forEach(s -> result.computeIfAbsent(s.getDtoType(), k -> new TreeSet<>()).add(s.getId()));
        return result;
    }
}
