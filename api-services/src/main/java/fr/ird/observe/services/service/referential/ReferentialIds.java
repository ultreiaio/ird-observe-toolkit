package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 26/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.45
 */
public class ReferentialIds implements JsonAware {

    private final Map<Class<? extends ReferentialDto>, Set<String>> ids;

    public static ReferentialIds of(Map<Class<? extends ReferentialDto>, Set<String>> ids) {
        return new ReferentialIds(ids);
    }

    public ReferentialIds(Map<Class<? extends ReferentialDto>, Set<String>> ids) {
        this.ids = Objects.requireNonNull(ids);
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getIds() {
        return ids;
    }

    public int size() {
        return ids.values().stream().mapToInt(Set::size).sum();
    }

    public boolean isEmpty() {
        return ids.isEmpty();
    }

    public Set<Class<? extends ReferentialDto>> keySet() {
        return ids.keySet();
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> without(Map<Class<? extends ReferentialDto>, Set<String>> existingIds) {
        Map<Class<? extends ReferentialDto>, Set<String>> result = new LinkedHashMap<>();
        for (Map.Entry<Class<? extends ReferentialDto>, Set<String>> entry : ids.entrySet()) {
            Class<? extends ReferentialDto> type = entry.getKey();
            Set<String> required = new LinkedHashSet<>(entry.getValue());
            Set<String> existing = existingIds.get(type);
            if (existing != null) {
                required.removeAll(existing);
            }
            if (!required.isEmpty()) {
                result.put(type, required);
            }
        }
        return result;

    }
}
