package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.navigation.tree.NavigationResult;
import fr.ird.observe.navigation.tree.io.ToolkitTreeFlatModel;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelPathRequest;
import fr.ird.observe.navigation.tree.io.request.ToolkitTreeFlatModelRootRequest;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Service;

import java.util.Date;

/**
 * Created on 13/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.0
 */
@Service
public interface NavigationService extends ObserveService {

    @Get
    @MethodCredential
    DataGroupByDtoSet<?, ?> getGroupByDtoSet(ToolkitTreeFlatModelRootRequest request);

    @Get
    @MethodCredential
    NavigationResult getNavigation(ToolkitTreeFlatModelRootRequest request, @Nullable Date timestamp);

    @Get
    @MethodCredential
    ToolkitTreeFlatModel loadSelectionRoot(ToolkitTreeFlatModelRootRequest request);

    @Get
    @MethodCredential
    ToolkitTreeFlatModel loadNavigationRoot(ToolkitTreeFlatModelRootRequest request);

    @Get
    @MethodCredential
    ToolkitTreeFlatModel loadNavigationPath(ToolkitTreeFlatModelPathRequest request);

}
