package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.i18n.I18n;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 08/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.53
 */
public class UsageCountWithLabel extends UsageCount {

    private final Locale locale;
    private final Map<Class<?>, String> labels;

    public UsageCountWithLabel(Locale locale, BusinessProject businessProject, UsageCount parent) {
        super(Objects.requireNonNull(parent));
        this.locale = Objects.requireNonNull(locale);
        labels = new LinkedHashMap<>();
        parent.keySet().forEach(t -> labels.put(t, businessProject.getFullTitle(locale, t)));
        UsageCount tmp = new UsageCount();
        parent.keySet().stream().sorted(Comparator.comparing(labels::get)).forEach(key -> tmp.put(key, get(key)));
        clear();
        putAll(tmp);
    }

    public String getLabelWithCount(Class<?> type) {
        return I18n.l(locale, "observe.Common.usage.label.and.count", getLabel(type), get(type));
    }

    public String getLabel(Class<?> type) {
        return labels.get(type);
    }

    @Override
    public UsageCountWithLabel getOrNullIfEmpty() {
        return (UsageCountWithLabel) super.getOrNullIfEmpty();
    }

}
