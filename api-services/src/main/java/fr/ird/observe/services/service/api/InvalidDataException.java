package fr.ird.observe.services.service.api;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import fr.ird.observe.validation.api.result.ValidationResult;
import io.ultreia.java4all.util.json.JsonAdapter;

/**
 * Created on 06/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.23
 */
public class InvalidDataException extends Exception {

    private final String message;
    private final ValidationResult result;

    public InvalidDataException(String message, ValidationResult result) {
        this.message = message;
        this.result = result;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public ValidationResult getResult() {
        return result;
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<InvalidDataException> {

        @Override
        public InvalidDataException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new InvalidDataException(message, context.deserialize(json.get("result"), ValidationResult.class));
        }

        @Override
        public Class<?> type() {
            return InvalidDataException.class;
        }
    }
}
