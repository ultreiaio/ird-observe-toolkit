package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.UsageCount;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.http.spi.Delete;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created on 24/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
//@ServiceInternal(value = {"D", "R"}, type = MetaModelNodeType.Open)
//@Internal
@Service
public interface OpenableService extends ObserveService {

    static <D extends OpenableDto, R extends DataDtoReference> EmptyChildrenDataReferenceSet<D, R> create(Class<D> dtoType,
                                                                                                          Class<R> referenceType,
                                                                                                          String parentId,
                                                                                                          int initialCount,
                                                                                                          BusinessProject businessProject,
                                                                                                          Supplier<OpenableService> serviceSupplier,
                                                                                                          DecoratorService decoratorService) {
        return new EmptyChildrenDataReferenceSet<>(dtoType,
                                                   referenceType,
                                                   initialCount,
                                                   businessProject.getReferenceComparator(dtoType),
                                                   serviceSupplier,
                                                   decoratorService,
                                                   parentId);
    }

    /**
     * For the given data dto, obtains the list of his brothers.
     *
     * @param dtoType dto type to look for
     * @param id      id
     * @param <D>     dto type to look for
     * @return the list of brothers
     */
    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> List<ToolkitIdLabel> getBrothers(Class<D> dtoType, String id);

    /**
     * For the given data dto, obtains the list of his brothers.
     *
     * @param dtoType  dto type to look for
     * @param parentId parent id
     * @param <D>      dto type to look for
     * @return the list of brothers
     */
    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> List<ToolkitIdLabel> getBrothersFromParent(Class<D> dtoType, String parentId);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto, R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(Class<D> dtoType, String parentId);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto, R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(Class<D> dtoType, String parentId, Date lastUpdate);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> Form<D> loadForm(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> D loadDto(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.READ_DATA)
    <D extends OpenableDto> boolean exists(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.WRITE_DATA)
    <D extends OpenableDto> Form<D> preCreate(Class<D> dtoType, String parentId);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    <D extends OpenableDto> SaveResultDto save(String parentId, D dto) throws ConcurrentModificationException;

    @Post
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> UsageCount getOptionalDependenciesCount(Class<D> dtoType, Set<String> ids);

    @Post
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto> UsageCount getMandatoryDependenciesCount(Class<D> dtoType, Set<String> ids);

    @Post
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto, T extends BusinessDto> Set<ToolkitIdLabel> getOptionalDependencies(Class<D> dtoType, Set<String> ids, Class<T> targetType);

    @Post
    @MethodCredential(Permission.READ_ALL)
    <D extends OpenableDto, T extends BusinessDto> Set<ToolkitIdLabel> getMandatoryDependencies(Class<D> dtoType, Set<String> ids, Class<T> targetType);

    @Write
    @Delete
    @MethodCredential(Permission.WRITE_DATA)
    <D extends OpenableDto> boolean delete(Class<D> dtoType, String id);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    <D extends OpenableDto> Set<String> move(Class<D> dtoType, MoveRequest request) throws ConcurrentModificationException;

}
