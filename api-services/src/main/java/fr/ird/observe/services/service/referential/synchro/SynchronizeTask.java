package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.DifferentialPropertyList;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 14/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class SynchronizeTask {

    /**
     * Type of referential to synchronize.
     */
    private final Class<? extends ReferentialDto> referentialType;
    /**
     * Id of referential to synchronize.
     */
    private final String referentialId;
    /**
     * Optional last update date to set.
     */
    private final Date lastUpdateDate;
    /**
     * Optional id of replace referential, if value is {@code null} then no replace is done.
     */
    private final String replaceReferentialId;
    /**
     * Optional property names to include, if value is {@code null} then include all properties.
     */
    private final Set<String> includedPropertyNames;
    /**
     * Optional property modifications.
     */
    private final DifferentialPropertyList includedProperties;

    public static SynchronizeTask createWithReplace(Class<? extends ReferentialDto> referentialType, String referentialId, Date lastUpdateDate, String replaceReferenceId) {
        return new SynchronizeTask(referentialType, referentialId, lastUpdateDate, replaceReferenceId, null, null);
    }

    public static SynchronizeTask createWithProperties(Class<? extends ReferentialDto> referentialType, String referentialId, Date lastUpdateDate, Set<String> includedPropertyNames, DifferentialPropertyList includedProperties) {
        return new SynchronizeTask(referentialType, referentialId, lastUpdateDate, null, includedPropertyNames, includedProperties);
    }

    public static SynchronizeTask create(Class<? extends ReferentialDto> referentialType, String referentialId, Date lastUpdateDate) {
        return new SynchronizeTask(referentialType, referentialId, lastUpdateDate, null, null, null);
    }

    protected SynchronizeTask(Class<? extends ReferentialDto> referentialType, String referentialId, Date lastUpdateDate, String replaceReferenceId, Set<String> includedPropertyNames, DifferentialPropertyList includedProperties) {
        this.referentialType = Objects.requireNonNull(referentialType);
        this.referentialId = Objects.requireNonNull(referentialId);
        this.lastUpdateDate = lastUpdateDate;
        this.replaceReferentialId = replaceReferenceId;
        this.includedPropertyNames = includedPropertyNames;
        this.includedProperties = includedProperties;
    }

    public Class<? extends ReferentialDto> getReferentialType() {
        return referentialType;
    }

    public String getReferentialId() {
        return referentialId;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public Optional<String> getOptionalReplaceReferentialId() {
        return Optional.ofNullable(replaceReferentialId);
    }

    public Optional<Set<String>> getOptionalIncludedPropertyNames() {
        return Optional.ofNullable(includedPropertyNames);
    }

    public Optional<DifferentialPropertyList> getOptionalIncludedProperties() {
        return Optional.ofNullable(includedProperties);
    }
}
