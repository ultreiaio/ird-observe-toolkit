package fr.ird.observe.services.service.api;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.navigation.tree.io.ToolkitTreeNodeStates;
import fr.ird.observe.navigation.tree.io.request.ToolkitRequestConfig;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import io.ultreia.java4all.http.spi.Delete;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Put;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

import java.util.List;
import java.util.Map;

/**
 * Created on 18/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
@Service
public interface DataEntityService extends ObserveService {

    @Get
    @MethodCredential(Permission.READ_ALL)
    ToolkitTreeNodeStates getOne(Class<? extends DataDto> dtoType, @Nullable ToolkitRequestConfig config, String id);

    @Get
    @MethodCredential(Permission.READ_ALL)
    ToolkitTreeNodeStates getSome(Class<? extends DataDto> dtoType, @Nullable ToolkitRequestConfig config, Map<String, String> filters, @Nullable Map<String, OrderEnum> orders);

    @Get
    @MethodCredential(Permission.READ_ALL)
    default ToolkitTreeNodeStates getSome(Class<? extends DataDto> dtoType, @Nullable ToolkitRequestConfig config, Map<String, String> filters) {
        return getSome(dtoType, config, filters, Map.of());
    }

    @Get
    List<String> generateId(Class<? extends DataDto> dtoType, int number);

    @Post(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    @Write
    ToolkitId create(Class<? extends DataDto> dtoType, String content) throws InvalidDataException;

    @Put(useMultiPartForm = true)
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    @Write
    ToolkitId update(Class<? extends DataDto> dtoType, String id, String content) throws InvalidDataException;

    @Delete
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    @Write
    void delete(Class<? extends DataDto> dtoType, String id);

}
