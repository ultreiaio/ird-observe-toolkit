package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.definition.ReportDefinition;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@Service
public interface ReportService extends ObserveService {

    @Post
    @MethodCredential(Permission.READ_ALL)
    ReportColumnRenderersParameters initColumnRendererParameters(ReportDefinition reportDefinition);

    @Post
    @MethodCredential(Permission.READ_ALL)
    Report populateVariables(Report report, Set<String> tripId);

    @Post
    @MethodCredential(Permission.READ_ALL)
    DataMatrix executeReport(Report report, Set<String> tripId);
}
