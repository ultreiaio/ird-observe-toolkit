package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Defines all requests to produce on one side of the synchronisation process.
 * <p>
 * Maybe executed after to the same side, or to the opposite (depends on task type).
 * <p>
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class OneSideSqlRequest implements JsonAware {

    private static final Logger log = LogManager.getLogger(OneSideSqlRequest.class);

    private final Map<SynchronizeTaskType, List<SynchronizeTask>> tasks;
    private final Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide;
    private transient Set<Class<? extends ReferentialDto>> types;

    public static class Builder {

        private final Map<SynchronizeTaskType, List<SynchronizeTask>> tasksBuilder = new LinkedHashMap<>();
        private final Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide;

        public Builder(Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {
            this.idsOnlyExistingOnThisSide = new LinkedHashMap<>(idsOnlyExistingOnThisSide);
        }

        public OneSideSqlRequest build() {
            return new OneSideSqlRequest(tasksBuilder, idsOnlyExistingOnThisSide);
        }

        public void addTask(SynchronizeTaskType taskType, SynchronizeTask task) {
            log.info(String.format("Add %s task: %s / %s (replace id %s - include property names %s) - last update date: %s", taskType, task.getReferentialType().getName(), task.getReferentialId(), task.getOptionalReplaceReferentialId(), task.getOptionalIncludedPropertyNames(), task.getLastUpdateDate()));
            tasksBuilder.computeIfAbsent(taskType, k -> new LinkedList<>()).add(task);
        }

        public <D extends ReferentialDto> void removeIdOnlyExistOnThisSide(Class<D> type, String id) {
            Set<String> existing = idsOnlyExistingOnThisSide.get(type);
            if (existing != null) {
                existing.remove(id);
            }
        }

        public OneSideSqlRequest.Builder addTasks(SynchronizeTaskType taskType, Map<Class<? extends ReferentialDto>, Set<String>> referentialIds) {
            for (Map.Entry<Class<? extends ReferentialDto>, Set<String>> entry : referentialIds.entrySet()) {
                Class<? extends ReferentialDto> dtoType = entry.getKey();
                Set<String> ids = entry.getValue();
                ids.forEach(id -> addTask(taskType, SynchronizeTask.create(dtoType, id, null)));
                log.info(String.format("Add %s %d referential of type: %s", taskType.name(), ids.size(), dtoType.getName()));
            }
            return this;
        }
    }

    public static OneSideSqlRequest.Builder builder() {
        return new OneSideSqlRequest.Builder(Map.of());
    }

    public static OneSideSqlRequest.Builder builder(Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {
        return new OneSideSqlRequest.Builder(idsOnlyExistingOnThisSide);
    }

    private OneSideSqlRequest(Map<SynchronizeTaskType, List<SynchronizeTask>> tasks,
                              Map<Class<? extends ReferentialDto>, Set<String>> idsOnlyExistingOnThisSide) {
        this.tasks = tasks;
        this.idsOnlyExistingOnThisSide = idsOnlyExistingOnThisSide;
    }

    public boolean isNotEmpty() {
        return !tasks.isEmpty();
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getIdsOnlyExistingOnThisSide() {
        return idsOnlyExistingOnThisSide;
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getNewIds() {
        Map<Class<? extends ReferentialDto>, Set<String>> result = new LinkedHashMap<>();
        List<SynchronizeTask> tasks = this.tasks.get(SynchronizeTaskType.ADD);
        if (tasks != null) {
            tasks.forEach(task -> result.computeIfAbsent(task.getReferentialType(), k -> new TreeSet<>()).add(task.getReferentialId()));
        }
        return result;
    }

    public Map<SynchronizeTaskType, List<SynchronizeTask>> getTasks() {
        return tasks;
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getAddTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.ADD, dtoType);
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getUpdateTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.UPDATE, dtoType);
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getRevertTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.REVERT, dtoType);
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getDeactivateTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.DEACTIVATE, dtoType);
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getDeactivateWithReplacementTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.DEACTIVATE_WITH_REPLACEMENT, dtoType);
    }

    public <D extends ReferentialDto> List<SynchronizeTask> getDeleteTasks(Class<D> dtoType) {
        return getTasks0(SynchronizeTaskType.DELETE, dtoType);
    }

    public Set<Class<? extends ReferentialDto>> getTypes() {
        if (types == null) {
            types = tasks.values().stream().flatMap(List::stream).map(SynchronizeTask::getReferentialType).collect(Collectors.toSet());
        }
        return types;
    }

    private <D extends ReferentialDto> List<SynchronizeTask> getTasks0(SynchronizeTaskType type, Class<D> dtoType) {
        List<SynchronizeTask> tasks = this.tasks.get(type);
        return tasks == null ? List.of() : tasks.stream().filter(t -> dtoType.equals(t.getReferentialType())).collect(Collectors.toList());
    }

}

