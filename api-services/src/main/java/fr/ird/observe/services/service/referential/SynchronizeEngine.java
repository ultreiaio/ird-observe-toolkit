package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import io.ultreia.java4all.util.LeftOrRight;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 15/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class SynchronizeEngine {
    private static final Logger log = LogManager.getLogger(SynchronizeEngine.class);

    /**
     * Where to generate sql scripts.
     */
    private final Path temporaryPath;
    /**
     * Left and right services.
     */
    private final LeftOrRightContext<SynchronizeService> services;

    public SynchronizeEngine(Path temporaryPath, LeftOrRightContext<SynchronizeService> services) {
        this.temporaryPath = Objects.requireNonNull(temporaryPath).resolve(getClass().getName());
        this.services = Objects.requireNonNull(services);
    }

    public LeftOrRightContext<OneSideSqlResult> produceSql(LeftOrRightContext<OneSideSqlRequest> sqlRequest) {
        return sqlRequest.apply(services, SynchronizeService::produceSqlResult);
    }

    public void executeSql(LeftOrRightContext<OneSideSqlResult> sqlResult) {
        String filePrefix = String.format("SynchronizeEngine-%d-", System.nanoTime());
        executeSqlSide(LeftOrRight.LEFT, filePrefix, sqlResult);
        executeSqlSide(LeftOrRight.RIGHT, filePrefix, sqlResult);
    }

    public Optional<SqlScript> toSqlScript(Path scriptPath, LeftOrRight side, LeftOrRightContext<OneSideSqlResult> result) {
        return result.onSameSide(side).toSqlScript(Objects.requireNonNull(scriptPath), result.onOppositeSide(side));
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }


    public LeftOrRightContext<SynchronizeService> getServices() {
        return services;
    }

    protected void executeSqlSide(LeftOrRight side, String filePrefix, LeftOrRightContext<OneSideSqlResult> sqlResult) {
        Path scriptPath = getTemporaryPath().resolve(filePrefix + side+".sql");
        log.info("Generate {} side script at: {}", side, scriptPath);
        Optional<SqlScript> optionalScript = toSqlScript(scriptPath, side, sqlResult);
        optionalScript.ifPresent(s -> executeSql(s, side));
    }

    protected void executeSql(SqlScript sqlResult, LeftOrRight side) {
        log.info("Execute {} side script...", side);
        services.onSameSide(side).applySql(sqlResult, SynchronizeService.ADVANCED_REFERENTIAL_SYNCHRONISATION);
    }
}

