package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Contient toutes les opérations réalisées lors de la synchronisation des référentiels.
 * <p>
 * Created on 05/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalResult implements JsonAware {

    private final Set<Class<? extends ReferentialDto>> referentialNames;
    private final LinkedHashMap<Class<? extends ReferentialDto>, List<String>> referentialAdded;
    private final LinkedHashMap<Class<? extends ReferentialDto>, List<String>> referentialUpdated;
    private final LinkedHashMap<Class<? extends ReferentialDto>, List<String>> referentialReverted;
    private final LinkedHashMap<Class<? extends ReferentialDto>, List<LeftOrRightContext< String>>> referentialReplaced;
    private final LinkedHashMap<Class<? extends ReferentialDto>, List<String>> referentialRemoved;

    public UnidirectionalResult() {
        this.referentialNames = new LinkedHashSet<>();
        this.referentialAdded = new LinkedHashMap<>();
        this.referentialUpdated = new LinkedHashMap<>();
        this.referentialReverted = new LinkedHashMap<>();
        this.referentialReplaced = new LinkedHashMap<>();
        this.referentialRemoved = new LinkedHashMap<>();
    }

    public boolean isEmpty() {
        return referentialNames.isEmpty();
    }

    public Set<Class<? extends ReferentialDto>> getReferentialNames() {
        return referentialNames;
    }

    public List<String> getReferentialAdded(Class<? extends ReferentialDto> referentialName) {
        return referentialAdded.get(referentialName);
    }

    public List<String> getReferentialUpdated(Class<? extends ReferentialDto> referentialName) {
        return referentialUpdated.get(referentialName);
    }

    public List<String> getReferentialReverted(Class<? extends ReferentialDto> referentialName) {
        return referentialReverted.get(referentialName);
    }

    public List<LeftOrRightContext<String>> getReferentialReplaced(Class<? extends ReferentialDto> referentialName) {
        return referentialReplaced.get(referentialName);
    }

    public List<String> getReferentialRemoved(Class<? extends ReferentialDto> referentialName) {
        return referentialRemoved.get(referentialName);
    }

    public void flushRequest(LeftOrRightContext<OneSideSqlRequest> sqlRequest) {
        for (Map.Entry<SynchronizeTaskType, List<SynchronizeTask>> entry : sqlRequest.left().getTasks().entrySet()) {
            SynchronizeTaskType key = entry.getKey();
            Collection<SynchronizeTask> value = entry.getValue();
            if (SynchronizeTaskType.DELETE == key) {
                value.forEach(synchronizeTask -> {
                    synchronizeTask.getOptionalReplaceReferentialId().ifPresent(replaceId -> addReferentialReplaced(synchronizeTask.getReferentialType(), synchronizeTask.getReferentialId(), replaceId));
                    addReferentialRemoved(synchronizeTask.getReferentialType(), synchronizeTask.getReferentialId());
                });
            }
        }
        for (Map.Entry<SynchronizeTaskType, List<SynchronizeTask>> entry : sqlRequest.right().getTasks().entrySet()) {
            SynchronizeTaskType key = entry.getKey();
            Collection<SynchronizeTask> value = entry.getValue();
            switch (key) {
                case ADD:
                    value.forEach(synchronizeTask -> addReferentialAdded(synchronizeTask.getReferentialType(), synchronizeTask.getReferentialId()));
                    break;
                case REVERT:
                    value.forEach(synchronizeTask -> addReferentialReverted(synchronizeTask.getReferentialType(), synchronizeTask.getReferentialId()));
                    break;
                case UPDATE:
                    value.forEach(synchronizeTask -> addReferentialUpdated(synchronizeTask.getReferentialType(), synchronizeTask.getReferentialId()));
                    break;
            }
        }
    }

    private void addReferentialAdded(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialAdded.computeIfAbsent(referentialName, k -> new LinkedList<>()).add(id);
    }

    private void addReferentialUpdated(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialUpdated.computeIfAbsent(referentialName, k -> new LinkedList<>()).add(id);
    }

    private void addReferentialReverted(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialReverted.computeIfAbsent(referentialName, k -> new LinkedList<>()).add(id);
    }

    private void addReferentialRemoved(Class<? extends ReferentialDto> referentialName, String id) {
        referentialNames.add(referentialName);
        referentialRemoved.computeIfAbsent(referentialName, k -> new LinkedList<>()).add(id);
    }

    private void addReferentialReplaced(Class<? extends ReferentialDto> referentialName, String idToReplace, String replaceId) {
        referentialNames.add(referentialName);
        referentialReplaced.computeIfAbsent(referentialName, k -> new LinkedList<>()).add(LeftOrRightContext.of(idToReplace, replaceId));
    }
}
