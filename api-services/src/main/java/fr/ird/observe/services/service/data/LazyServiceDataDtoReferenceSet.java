package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.LazyDataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.services.service.ObserveService;

import java.util.Comparator;
import java.util.function.Supplier;

/**
 * Created on 11/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class LazyServiceDataDtoReferenceSet<D extends DataDto, R extends DataDtoReference, S extends ObserveService> extends LazyDataDtoReferenceSet<D, R> {

    protected final Supplier<S> serviceSupplier;
    protected final DecoratorService decoratorService;

    public LazyServiceDataDtoReferenceSet(Class<D> dtoType, Class<R> referenceType, int initialCount, Comparator<R> comparator, Supplier<S> serviceSupplier, DecoratorService decoratorService) {
        super(dtoType, referenceType, comparator, initialCount);
        this.serviceSupplier = serviceSupplier;
        this.decoratorService = decoratorService;
    }

    protected abstract DataDtoReferenceSet<R> createInitialResult(S service);

    protected abstract UpdatedDataDtoReferenceSet<R> createUpdatedResult(S service, DataDtoReferenceSet<R> previousResult);

    @Override
    protected final DataDtoReferenceSet<R> createDelegate() {
        DataDtoReferenceSet<R> previousResult = getDelegate();
        DataDtoReferenceSet<R> result;
        S service = serviceSupplier.get();
        if (previousResult == null) {
            // first time load all data
            result = createInitialResult(service);
        } else {
            // ask an update of data
            UpdatedDataDtoReferenceSet<R> updateResult = createUpdatedResult(service, previousResult);
            // merge it with previous data
            result = updateResult.mergeTo(previousResult);
        }
        result.sort(getComparator());
        installDecorator(result);
        return result;
    }

    @Override
    protected void installDecorator(DataDtoReferenceSet<R> result) {
        decoratorService.installDecorator(result);
    }
}
