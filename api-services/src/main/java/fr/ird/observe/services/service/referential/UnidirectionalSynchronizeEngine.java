package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ProgressionModel;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.differential.Differential;
import fr.ird.observe.services.service.referential.differential.DifferentialList;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import fr.ird.observe.services.service.referential.differential.DifferentialType;
import fr.ird.observe.services.service.referential.synchro.BothSidesSqlRequestBuilder;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlRequest;
import fr.ird.observe.services.service.referential.synchro.OneSideSqlResult;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTask;
import fr.ird.observe.services.service.referential.synchro.SynchronizeTaskType;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackRequests;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResult;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackResults;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalResult;
import io.ultreia.java4all.util.LeftOrRight;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 28/06/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalSynchronizeEngine {

    private static final Logger log = LogManager.getLogger(UnidirectionalSynchronizeEngine.class);
    private final SynchronizeEngine synchronizeEngine;

    public UnidirectionalSynchronizeEngine(SynchronizeEngine synchronizeEngine) {
        this.synchronizeEngine = Objects.requireNonNull(synchronizeEngine);
    }

    public UnidirectionalSynchronizeContext prepareContext(DifferentialModelBuilder diffsEngine, ProgressionModel progressionModel) {
        if (progressionModel == null) {
            progressionModel = new ProgressionModel();
            progressionModel.addPropertyChangeListener(ProgressionModel.PROPERTY_MESSAGE, evt -> log.info(evt.getNewValue()));
        }
        LeftOrRightContext<DifferentialList> synchronizeDiffs = diffsEngine.build(progressionModel);

        DifferentialList leftDiff = synchronizeDiffs.left();
        DifferentialList rightDiff = synchronizeDiffs.right();

        Map<Class<? extends ReferentialDto>, DifferentialList> leftDiffStates = leftDiff.getStatesByType();
        Map<Class<? extends ReferentialDto>, DifferentialList> rightDiffStates = rightDiff.getStatesByType();
        Set<Class<? extends ReferentialDto>> types = new LinkedHashSet<>(leftDiffStates.keySet());
        types.addAll(rightDiffStates.keySet());

        BothSidesSqlRequestBuilder sqlRequestBuilder = BothSidesSqlRequestBuilder.builder(synchronizeDiffs.apply(DifferentialList::getNewIds));

        Set<Class<? extends ReferentialDto>> replicationOrder = diffsEngine.metaModel().replicationOrder();
        Set<Class<? extends ReferentialDto>> usedTypes = new LinkedHashSet<>();
        UnidirectionalCallbackRequests callbackRequests = new UnidirectionalCallbackRequests();

        // Première étape pour construire les builder de requète et calculer au passage les ids à faire remplacer par l'utilisateur
        for (Class<? extends ReferentialDto> dtoType : replicationOrder) {
            if (types.contains(dtoType)) {
                boolean used = computeReferentialSynchronizeRequestBuilder(sqlRequestBuilder,
                                                                           dtoType,
                                                                           leftDiffStates,
                                                                           rightDiffStates,
                                                                           callbackRequests);
                if (used) {
                    usedTypes.add(dtoType);
                }
            }
        }
        return new UnidirectionalSynchronizeContext(usedTypes, sqlRequestBuilder, callbackRequests);
    }

    public UnidirectionalResult prepareResult(UnidirectionalSynchronizeContext context, UnidirectionalCallbackResults callbackResults) {
        UnidirectionalResult result = new UnidirectionalResult();

        // Second étape pour terminer la construction des builders de requètes et générer le code sql
        boolean needCallback = callbackResults != null;
        BothSidesSqlRequestBuilder requestBuilders = context.getSqlRequestsBuilder();
        for (Class<? extends ReferentialDto> referentialName : context.getUsedTypes()) {
            if (needCallback && callbackResults.containsReferentialName(referentialName)) {
                // l'utilisateur a agit sur ce référentiel, c'est une suppression avec remplacement
                UnidirectionalCallbackResult<?> callbackResult = callbackResults.getCallbackResult(referentialName);
                Map<String, String> ids = callbackResult.getIds();
                for (Map.Entry<String, String> entry : ids.entrySet()) {
                    String idToReplace = entry.getKey();
                    requestBuilders.addTask(LeftOrRight.LEFT, SynchronizeTaskType.DELETE, SynchronizeTask.createWithReplace(referentialName, idToReplace, null, entry.getValue()));
                }
            }
        }
        LeftOrRightContext<OneSideSqlRequest> referentialSynchronizeRequest = requestBuilders.build();
        result.flushRequest(referentialSynchronizeRequest);
        LeftOrRightContext<OneSideSqlResult> generatedSqlRequests = synchronizeEngine.produceSql(referentialSynchronizeRequest);
        Path scriptPath = synchronizeEngine.getTemporaryPath().resolve(String.format("UnidirectionalSynchronizeEngine-%d.sql", System.nanoTime()));

        Optional<SqlScript> topiaSqlScript = synchronizeEngine.toSqlScript(scriptPath,LeftOrRight.LEFT, generatedSqlRequests);
        topiaSqlScript.ifPresent(s -> {
            log.info(String.format("Generate script at: %s", scriptPath));
            context.setSqlScript(s);
            context.setSqlScriptPath(scriptPath);
        });
        return result;
    }

    private <D extends ReferentialDto> boolean computeReferentialSynchronizeRequestBuilder(
            BothSidesSqlRequestBuilder sqlRequestBuilder,
            Class<D> dtoType,
            Map<Class<? extends ReferentialDto>, DifferentialList> leftDiffStates,
            Map<Class<? extends ReferentialDto>, DifferentialList> rightDiffStates,
            UnidirectionalCallbackRequests callbackRequests) {
        boolean used = false;
        List<Differential> leftAddDifferentials = new LinkedList<>();
        DifferentialList leftDifferentials = leftDiffStates.get(dtoType);
        if (leftDifferentials != null) {
            used = true;
            for (Differential differential : leftDifferentials.getStates()) {
                DifferentialType differentialType = differential.getDifferentialType();
                switch (differentialType) {
                    case ADDED:
                        // on conserve ces objets car il faudra les remplacer dans la base locale
                        leftAddDifferentials.add(differential);
                        break;
                    case MODIFIED:
                    case DISABLED:
                        // On revient en arrière sur les référentiels de la base centrale
                        sqlRequestBuilder.addTask(LeftOrRight.LEFT, SynchronizeTaskType.REVERT, createRevertTask(dtoType, differential));
                        break;
                }
            }
        }
        DifferentialList rightDifferentials = rightDiffStates.get(dtoType);
        if (rightDifferentials != null) {
            used = true;
            for (Differential differential : rightDifferentials.getStates()) {
                DifferentialType differentialType = differential.getDifferentialType();
                switch (differentialType) {
                    case ADDED:
                        // Tous les référentiels ajoutés sur la base centrale doivent être copié en local
                        sqlRequestBuilder.addTask(LeftOrRight.RIGHT, SynchronizeTaskType.ADD, createTask(dtoType, differential));
                        break;
                    case MODIFIED:
                    case DISABLED:
                        // Tous les référentiels mises à jour dans la base centrale doivent être copié en local
                        sqlRequestBuilder.addTask(LeftOrRight.RIGHT, SynchronizeTaskType.UPDATE, createTask(dtoType, differential));
                        break;
                }
            }
        }
        // Tous les référentiels non présents dans la base centrale  doivent être supprimés en local
        if (!leftAddDifferentials.isEmpty()) {
            Set<String> idsToRemove = leftAddDifferentials.stream().map(Differential::getId).collect(Collectors.toCollection(LinkedHashSet::new));
            Set<String> blockingIdsToRemove = null;
            if (!idsToRemove.isEmpty()) {
                Set<String> blockingIdsToRemoveFromLocal = filterIdsUsedInLeftSource(dtoType, idsToRemove);
                blockingIdsToRemove = idsToRemove
                        .stream()
                        .filter(blockingIdsToRemoveFromLocal::contains)
                        .collect(Collectors.toSet());
                idsToRemove.removeAll(blockingIdsToRemove);
                idsToRemove.forEach(id -> sqlRequestBuilder.addTask(LeftOrRight.LEFT, SynchronizeTaskType.DELETE, SynchronizeTask.create(dtoType, id, null)));
            }
            boolean needCallback = blockingIdsToRemove != null && !blockingIdsToRemove.isEmpty();
            if (needCallback) {
                Set<ToolkitIdLabel> availableReferenceSet = getRightEnabledReferentialLabelSet(dtoType);
                Set<ToolkitIdLabel> blockingReferentialSetToRemove = getLeftReferentialToDelete(dtoType, blockingIdsToRemove);
                callbackRequests.addCallbackRequest(dtoType, blockingReferentialSetToRemove, availableReferenceSet);
            }
        }
        return used;
    }

    private  <D extends ReferentialDto> Set<ToolkitIdLabel> getLeftReferentialToDelete(Class<D> dtoType, Set<String> blockingIdsToRemove) {
        return synchronizeEngine.getServices().left().getReferentialToDelete(dtoType, blockingIdsToRemove);
    }

    private  <D extends ReferentialDto> Set<String> filterIdsUsedInLeftSource(Class<D> dtoType, Set<String> idsToRemove) {
        return synchronizeEngine.getServices().left().filterIdsUsed(dtoType, idsToRemove);
    }

    private  <D extends ReferentialDto> Set<ToolkitIdLabel> getRightEnabledReferentialLabelSet(Class<D> referentialName) {
        return synchronizeEngine.getServices().right().getEnabledReferentialLabelSet(referentialName);
    }

    private <D extends ReferentialDto> SynchronizeTask createTask(Class<D> dtoType, Differential differential) {
        return SynchronizeTask.create(dtoType, differential.getId(), differential.getThisSideDto().getLastUpdateDate());
    }

    private <D extends ReferentialDto> SynchronizeTask createRevertTask(Class<D> dtoType, Differential differential) {
        ToolkitIdTechnicalLabel otherSideDto = differential.getOptionalOtherSideDto().orElseThrow();
        return SynchronizeTask.create(dtoType, differential.getId(), otherSideDto.getLastUpdateDate());
    }

}
