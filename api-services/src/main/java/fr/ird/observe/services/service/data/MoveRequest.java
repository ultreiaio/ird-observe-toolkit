package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import fr.ird.observe.dto.data.RootOpenableDto;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;
import java.util.StringJoiner;

/**
 * Created on 13/10/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class MoveRequest implements JsonAware {

    /**
     * Id of parent data to move.
     */
    private final ToolkitIdDtoBean oldParentId;
    /**
     * Id of parent data where move.
     */
    private final ToolkitIdDtoBean newParentId;
    /**
     * Ids to move.
     */
    private final Set<String> ids;
    /**
     * To select target node after move operation.
     */
    private final boolean selectTarget;

    public MoveRequest(ToolkitIdDtoBean oldParentId, ToolkitIdDtoBean newParentId, Set<String> ids, boolean selectTarget) {
        this.oldParentId = oldParentId;
        this.newParentId = newParentId;
        this.ids = ids;
        this.selectTarget = selectTarget;
    }

    public Class<? extends BusinessDto> getParentDtoType() {
        return oldParentId.getType();
    }

    public Class<? extends BusinessDto> getParentTargetDtoType() {
        return newParentId.getType();
    }

    public ToolkitIdDtoBean getOldParentId() {
        return oldParentId;
    }

    public ToolkitIdDtoBean getNewParentId() {
        return newParentId;
    }

    public Set<String> getIds() {
        return ids;
    }

    public String getId() {
        return ids.iterator().next();
    }

    public boolean isSelectTarget() {
        return selectTarget;
    }

    public boolean isSingle() {
        return ids.size() == 1;
    }

    public boolean isNewParentIsRootType() {
        return RootOpenableDto.class.isAssignableFrom(getParentTargetDtoType());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MoveRequest.class.getSimpleName() + "[", "]")
                .add("oldParentId='" + oldParentId + "'")
                .add("newParentId='" + newParentId + "'")
                .add("ids=" + ids)
                .toString();
    }
}
