package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Map;
import java.util.TreeMap;

/**
 * Contient les résultats du call back utilisateur pour un type de référentiel donné.
 * <p>
 * Created on 12/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalCallbackResult<D extends ReferentialDto> {

    private final Class<D> referentialName;
    private final Map<String, String> ids = new TreeMap<>();

    UnidirectionalCallbackResult(Class<D> referentialName) {
        this.referentialName = referentialName;
    }

    public Class<D> getReferentialName() {
        return referentialName;
    }

    public Map<String, String> getIds() {
        return ids;
    }

    void addId(String idToReplace, String replaceId) {
        ids.put(idToReplace, replaceId);
    }


}
