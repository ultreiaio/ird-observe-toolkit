package fr.ird.observe.services.service.referential.differential;
/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveDto;
import fr.ird.observe.dto.ToolkitIdTechnicalLabel;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * TODO Rethink this API to use a DifferentialIds from ToPIA, we should do all the work in persistence layer,
 * TODO after this is just a matter of decorating data for service
 * Defines a differential characterized by {@code differentialType} on a referential that exists on {@code thisSource} side.
 * <p>
 * The dto might exists on other side, it depends on {@code differentialType}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class Differential implements ObserveDto {
    /**
     * Type of dto linked to the entity.
     */
    private final Class<? extends ReferentialDto> dtoType;
    /**
     * What is the type of differential on the {@link #thisSideDto}.
     */
    private final DifferentialType differentialType;
    /**
     * Referential on this side on which a modification has been detected.
     */
    private final ToolkitIdTechnicalLabel thisSideDto;
    /**
     * Optional referential on other side, may be null if referential was added...
     */
    private final ToolkitIdTechnicalLabel otherSideDto;
    /**
     * Optional list of modified properties, used only on {@link DifferentialType#MODIFIED} or {@link DifferentialType#DISABLED}, if so {@code null} means use all
     * properties, otherwise update only theses properties; may be null in all other case.
     */
    private final Set<String> modifiedProperties;
    /**
     * Optional list of modified properties, used only on {@link DifferentialType#MODIFIED} or {@link DifferentialType#DISABLED}, if so {@code null} means use all
     * properties, otherwise update only theses properties; may be null in all other case.
     */
    private final DifferentialPropertyList propertiesModification;

    private transient String label;

    public Differential(Class<? extends ReferentialDto> dtoType, DifferentialType differentialType, ToolkitIdTechnicalLabel thisSideDto, ToolkitIdTechnicalLabel otherSideDto, DifferentialPropertyList propertiesModification) {
        this.dtoType = dtoType;
        this.differentialType = Objects.requireNonNull(differentialType);
        this.thisSideDto = Objects.requireNonNull(thisSideDto);
        this.otherSideDto = otherSideDto;
        this.propertiesModification = propertiesModification;
        this.modifiedProperties = propertiesModification == null ? null : propertiesModification.getPropertyNames();
    }

    public DifferentialType getDifferentialType() {
        return differentialType;
    }

    public ToolkitIdTechnicalLabel getThisSideDto() {
        return thisSideDto;
    }

    public String getId() {
        return thisSideDto.getId();
    }

    public Class<? extends ReferentialDto> getDtoType() {
        return dtoType;
    }

    public Optional<ToolkitIdTechnicalLabel> getOptionalOtherSideDto() {
        return Optional.ofNullable(otherSideDto);
    }

    public Optional<Set<String>> getOptionalModifiedProperties() {
        return Optional.ofNullable(modifiedProperties);
    }

    public String getLabel() {
        if (label == null) {
            label = getThisSideDto().toString();
        }
        return label;
    }

    public DifferentialPropertyList getPropertiesModification() {
        return propertiesModification;
    }

    public DifferentialPropertyList getPropertiesModification(Collection<String> propertyNames, boolean flip) {
        if (propertyNames != null) {
            return propertiesModification.of(propertyNames, flip);
        }
        return null;
    }

    @Override
    public String toString() {
        return "Differential{" +
                "differentialType=" + differentialType +
                ", thisSourceDto=" + thisSideDto.getId() +
                (otherSideDto == null ? "" : ", otherSideDto=" + otherSideDto.getId()) +
                (modifiedProperties == null ? "" : ", modifiedProperties=" + modifiedProperties) +
                '}';
    }
}
