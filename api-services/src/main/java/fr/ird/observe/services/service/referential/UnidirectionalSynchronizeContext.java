package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.referential.synchro.BothSidesSqlRequestBuilder;
import fr.ird.observe.services.service.referential.synchro.UnidirectionalCallbackRequests;
import io.ultreia.java4all.util.sql.SqlScript;

import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 12/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalSynchronizeContext {

    private final Set<Class<? extends ReferentialDto>> usedTypes;
    private final BothSidesSqlRequestBuilder sqlRequestsBuilder;
    private final UnidirectionalCallbackRequests callbackRequests;
    private Path sqlScriptPath;
    private SqlScript sqlScript;

    UnidirectionalSynchronizeContext(Set<Class<? extends ReferentialDto>> usedTypes, BothSidesSqlRequestBuilder sqlRequestsBuilder, UnidirectionalCallbackRequests callbackRequests) {
        this.usedTypes = usedTypes;
        this.sqlRequestsBuilder = sqlRequestsBuilder;
        this.callbackRequests = callbackRequests;
    }

    public Set<Class<? extends ReferentialDto>> getUsedTypes() {
        return usedTypes;
    }

    BothSidesSqlRequestBuilder getSqlRequestsBuilder() {
        return sqlRequestsBuilder;
    }

    public UnidirectionalCallbackRequests getCallbackRequests() {
        return callbackRequests;
    }

    public boolean isNeedCallback() {
        return callbackRequests.isNotEmpty();
    }

    public Optional<SqlScript> getSqlScript() {
        return Optional.ofNullable(sqlScript);
    }

    public void setSqlScript(SqlScript sqlScript) {
        this.sqlScript = sqlScript;
    }

    public Path getSqlScriptPath() {
        return sqlScriptPath;
    }

    public void setSqlScriptPath(Path sqlScriptPath) {
        this.sqlScriptPath = sqlScriptPath;
    }

    public void finish(SynchronizeService leftService) {
        Optional<SqlScript> sqlScript = getSqlScript();
        leftService.applySql(sqlScript.orElse(null), SynchronizeService.SIMPLE_REFERENTIAL_SYNCHRONISATION);
    }
}
