package fr.ird.observe.services.service.referential.differential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Contains meta data around how to build differential model.
 * <p>
 * Created on 12/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.x
 */
public class DifferentialMetaModel implements JsonAware {

    public static String toLocation(String modelName) {
        return String.format("META-INF/services/%s/%s.json", Objects.requireNonNull(modelName), DifferentialMetaModel.class.getSimpleName());
    }

    public static Gson newGson() {
        return new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .enableComplexMapKeySerialization()
                .registerTypeAdapter(Class.class, new ClassAdapter())
                .create();
    }

    private final LinkedHashMap<Class<? extends ReferentialDto>, List<String>> entries;

    public DifferentialMetaModel(String modelName) {
        String location = toLocation(Objects.requireNonNull(modelName));
        URL resource = Objects.requireNonNull(DifferentialMetaModel.class).getClassLoader().getResource(Objects.requireNonNull(location));
        try (Reader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(resource).openStream(), StandardCharsets.UTF_8))) {
            this.entries = newGson().fromJson(reader, new TypeToken<LinkedHashMap<Class<? extends ReferentialDto>, List<String>>>() {
            }.getType());
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not load resource: %s (for type: %s)", resource, getClass().getName()), e);
        }
    }

    public int size() {
        return entries.size();
    }

    public Set<Map.Entry<Class<? extends ReferentialDto>, List<String>>> entriesByNaturalOrder() {
        return Collections.unmodifiableSet(entries.entrySet());
    }

    public Set<Class<? extends ReferentialDto>> replicationOrder() {
        return Collections.unmodifiableSet(entries.keySet());
    }
}
