package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Représente une demande du call back utilisateur pour un type de référentiel donné.
 * <p>
 * Created on 12/07/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class UnidirectionalCallbackRequest<D extends ReferentialDto> {

    private final Class<D> dtoType;
    private final Set<ToolkitIdLabel> referentialToReplace;
    private final Set<ToolkitIdLabel> availableReferential;

    UnidirectionalCallbackRequest(Class<D> dtoType,
                                  Set<ToolkitIdLabel> referentialToReplace,
                                  Set<ToolkitIdLabel> availableReferential) {
        this.dtoType = dtoType;
        this.referentialToReplace = new LinkedHashSet<>(referentialToReplace);
        this.availableReferential = new LinkedHashSet<>(availableReferential);
    }

    public Class<D> getDtoType() {
        return dtoType;
    }

    public Set<ToolkitIdLabel> getReferentialToReplace() {
        return referentialToReplace;
    }

    public Set<ToolkitIdLabel> getAvailableReferential() {
        return availableReferential;
    }

}
