package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;

import java.util.Comparator;
import java.util.function.Supplier;

/**
 * Created on 14/01/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
public class EmptyChildrenDataReferenceSet<D extends OpenableDto, R extends DataDtoReference> extends LazyServiceDataDtoReferenceSet<D, R, OpenableService> {

    private final String parentId;

    public EmptyChildrenDataReferenceSet(Class<D> dtoType, Class<R> referenceType, int initialCount, Comparator<R> comparator, Supplier<OpenableService> serviceSupplier, DecoratorService decoratorService, String parentId) {
        super(dtoType, referenceType, initialCount, comparator, serviceSupplier, decoratorService);
        this.parentId = parentId;
    }

    @Override
    protected DataDtoReferenceSet<R> createInitialResult(OpenableService service) {
        return service.getChildren(getDtoType(), parentId);
    }

    @Override
    protected UpdatedDataDtoReferenceSet<R> createUpdatedResult(OpenableService service, DataDtoReferenceSet<R> previousResult) {
        return service.getChildrenUpdate(getDtoType(), parentId, previousResult.getLastUpdate());
    }

    public String getParentId() {
        return parentId;
    }
}
