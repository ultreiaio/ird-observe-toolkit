package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Map;
import java.util.Set;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class MissingReferentialResult implements JsonAware {

    private final Map<Class<? extends ReferentialDto>, Set<String>> missingIds;
    private final SqlScript sqlCode;

    public static MissingReferentialResult of(Map<Class<? extends ReferentialDto>, Set<String>> missingReferentialIds, SqlScript sqlCode) {
        return new MissingReferentialResult(missingReferentialIds, sqlCode);
    }

    private MissingReferentialResult(Map<Class<? extends ReferentialDto>, Set<String>> missingIds, SqlScript sqlCode) {
        this.missingIds = missingIds;
        this.sqlCode = sqlCode;
    }

    public Map<Class<? extends ReferentialDto>, Set<String>> getMissingIds() {
        return missingIds;
    }

    public SqlScript getSqlCode() {
        return sqlCode;
    }
}
