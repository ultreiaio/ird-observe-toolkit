package fr.ird.observe.services.service.referential.synchro;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.LeftOrRight;
import io.ultreia.java4all.util.LeftOrRightContext;

import java.util.Map;
import java.util.Set;

/**
 * Created at 22/04/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class BothSidesSqlRequestBuilder {

    private final LeftOrRightContext<OneSideSqlRequest.Builder> builders;

    private BothSidesSqlRequestBuilder(LeftOrRightContext<Map<Class<? extends ReferentialDto>, Set<String>>> ids) {
        this.builders = ids.apply(OneSideSqlRequest::builder);
    }

    public static BothSidesSqlRequestBuilder builder(LeftOrRightContext<Map<Class<? extends ReferentialDto>, Set<String>>> ids) {
        return new BothSidesSqlRequestBuilder(ids);
    }

    public LeftOrRightContext<OneSideSqlRequest> build() {
        return builders.apply(OneSideSqlRequest.Builder::build);
    }

    public void addTask(LeftOrRight side, SynchronizeTaskType taskType, SynchronizeTask task) {
        OneSideSqlRequest.Builder requestBuilder = taskType.chooseSqlBuilder(builders).apply(side);
        requestBuilder.addTask(taskType, task);

        if (SynchronizeTaskType.ADD == taskType) {

            // le referentiel est à ajouter, il sera donc disponible dans les deux sources
            requestBuilder.removeIdOnlyExistOnThisSide(task.getReferentialType(), task.getReferentialId());

        } else if (SynchronizeTaskType.DELETE == taskType) {

            // le référentiel est supprimer, il ne sera donc plus disponible du tout
            //FIXME Make sure this is ok
            requestBuilder.removeIdOnlyExistOnThisSide(task.getReferentialType(), task.getReferentialId());
        }
    }
}
