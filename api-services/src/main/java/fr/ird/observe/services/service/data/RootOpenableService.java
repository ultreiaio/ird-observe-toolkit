package fr.ird.observe.services.service.data;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.DataGroupByParameter;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.DataDtoReferenceSet;
import fr.ird.observe.dto.reference.UpdatedDataDtoReferenceSet;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.http.spi.Delete;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
//@ServiceInternal(value = {"D", "R"}, type = MetaModelNodeType.RootOpen)
//@Internal
@Service
public interface RootOpenableService extends ObserveService {

    static <D extends RootOpenableDto, R extends DataDtoReference> RootEmptyChildrenDataReferenceSet<D, R> create(Class<D> dtoType,
                                                                                                                  Class<R> referenceType,
                                                                                                                  DataGroupByDto<?> parentProperty,
                                                                                                                  String groupByFlavor,
                                                                                                                  BusinessProject businessProject,
                                                                                                                  Supplier<RootOpenableService> serviceSupplier,
                                                                                                                  DecoratorService decoratorService) {
        return new RootEmptyChildrenDataReferenceSet<>(dtoType,
                                                       referenceType,
                                                       businessProject.getReferenceComparator(dtoType),
                                                       serviceSupplier,
                                                       decoratorService,
                                                       parentProperty,
                                                       groupByFlavor);
    }

    /**
     * Obtains the list of the given data dto matching the given groupBy, and excluding the optional id (if present).
     *
     * @param groupBy parent property name
     * @param id      optional id to exclude from result
     * @return the list of brothers
     */
    @Get
    @MethodCredential(Permission.READ_ALL)
    List<ToolkitIdLabel> getBrothers(DataGroupByParameter groupBy, @Nullable String id);

    @Get
    @MethodCredential(Permission.READ_ALL)
    String getGroupByValue(String groupByName, @Nullable String groupByFlavor, String id);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends RootOpenableDto, F extends DataGroupByDto<D>> F getGroupByDtoValue(Class<F> dtoType, DataGroupByParameter groupBy);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <R extends DataDtoReference> DataDtoReferenceSet<R> getChildren(DataGroupByParameter groupBy);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <R extends DataDtoReference> UpdatedDataDtoReferenceSet<R> getChildrenUpdate(DataGroupByParameter groupBy, Date lastUpdate);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends RootOpenableDto> Form<D> loadForm(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.READ_ALL)
    <D extends RootOpenableDto> D loadDto(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.READ_DATA)
    <D extends RootOpenableDto> boolean exists(Class<D> dtoType, String id);

    @Get
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> Form<D> preCreate(Class<D> dtoType, DataGroupByParameter groupBy);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> SaveResultDto save(D dto) throws ConcurrentModificationException;

    @Write
    @Delete
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> void delete(Class<D> dtoType, String id);

    @Write
    @Delete
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> void deleteLayout(Class<D> dtoType, DeleteLayoutRequest request);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> void moveLayout(Class<D> dtoType, MoveLayoutRequest request);

    @Write
    @Post(useMultiPartForm = true, timeOut = 120)
    @MethodCredential(Permission.WRITE_DATA)
    <D extends RootOpenableDto> void updateQualitativeProperty(Class<D> dtoType, DataGroupByParameter groupBy, Set<String> ids);

    @Post(useMultiPartForm = true, timeOut = 120)
    @MethodCredential(Permission.READ_ALL)
    <D extends RootOpenableDto> MissingReferentialResult computeMissingReferential(Class<D> dtoType, MissingReferentialRequest request);

}

