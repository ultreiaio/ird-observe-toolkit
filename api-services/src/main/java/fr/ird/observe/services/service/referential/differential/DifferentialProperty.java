package fr.ird.observe.services.service.referential.differential;
/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.IdDto;
import io.ultreia.java4all.i18n.I18n;

import java.util.Collection;
import java.util.Date;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Represents a modification on a property.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 7.4.0
 */
public class DifferentialProperty {

    private final String propertyName;
    private final Object oldValue;
    private final Object newValue;

    private transient String propertyNameLabel;
    private transient String oldValueLabel;
    private transient String newValueLabel;

    public static String getModifiedProperty(String propertyName, Object value) {
        if (value == null) {
            return t("observe.Common.undefined");
        }
        if (value instanceof Boolean) {
            Boolean otherSideValue1 = (Boolean) value;
            if (otherSideValue1) {
                return t("boolean.true");
            }
            return t("boolean.false");
        }
        if (value instanceof Collection<?>) {
            Collection<?> value1 = (Collection<?>) value;
            return t("observe.Common.size", value1.size());
        }
        if (value instanceof Long) {
            long value1 = (long) value;
            if (propertyName.toLowerCase().contains("timestamp")) {
                // timestamp (date + time)
                return I18nDecoratorHelper.getTimestampLabel(I18n.getDefaultLocale(), new Date(value1));
            }
            if (propertyName.toLowerCase().contains("date")) {
                // only date
                return I18nDecoratorHelper.getDateLabel(I18n.getDefaultLocale(), new Date(value1));
            }
        }
        return value.toString();
    }

    public DifferentialProperty(String propertyName, Object oldValue, Object newValue) {
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }

    public String getPropertyNameLabel(Class<? extends IdDto> dtoType) {
        if (propertyNameLabel == null) {
            propertyNameLabel = t(I18nDecoratorHelper.getPropertyI18nKey(dtoType, getPropertyName()));
        }
        return propertyNameLabel;
    }

    public String getOldValueLabel() {
        if (oldValueLabel == null) {
            oldValueLabel = getModifiedProperty(getPropertyName(), oldValue);
        }
        return oldValueLabel;
    }

    public String getNewValueLabel() {
        if (newValueLabel == null) {
            newValueLabel = getModifiedProperty(getPropertyName(), newValue);
        }
        return newValueLabel;
    }

    public DifferentialProperty flip() {
        return new DifferentialProperty(propertyName, newValue, oldValue);
    }
}
