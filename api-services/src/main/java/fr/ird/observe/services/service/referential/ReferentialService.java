package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.ConcurrentModificationException;
import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.form.Form;
import fr.ird.observe.dto.reference.ReferenceSetsRequest;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceSet;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.MethodCredential;
import fr.ird.observe.services.service.ObserveService;
import fr.ird.observe.services.service.SaveResultDto;
import fr.ird.observe.services.service.referential.differential.DifferentialMetaModel;
import fr.ird.observe.services.service.referential.differential.DifferentialModelBuilder;
import io.ultreia.java4all.http.spi.Delete;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Nullable;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.java4all.http.spi.Service;
import io.ultreia.java4all.http.spi.Write;
import io.ultreia.java4all.util.LeftOrRightContext;
import io.ultreia.java4all.util.sql.SqlScript;

import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created on 16/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
//@ServiceInternal(value = {"D", "R"},type = MetaModelNodeType.ReferentialType)
//@Internal
@Service
public interface ReferentialService extends ObserveService {

    static DifferentialModelBuilder createDifferentialModelBuilder(Locale locale, Supplier<DifferentialMetaModel> metaModelSupplier, LeftOrRightContext<ReferentialService> referentialServices) {
        return new DifferentialModelBuilder(
                locale,
                metaModelSupplier,
                referentialServices.apply(referentialService -> referentialService::loadDtoList)
        );
    }

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <R extends ReferentialDtoReference> ReferentialDtoReferenceSet<R> getReferenceSet(Class<R> type, @Nullable Date lastUpdateDate);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends BusinessDto> Set<ReferentialDtoReferenceSet<?>> getReferentialReferenceSets(ReferenceSetsRequest<D> request);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> D loadDto(Class<D> type, String id);

    @Post
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> Set<D> loadDtoList(Class<D> type);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> Set<String> loadIds(Class<D> type);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> Form<D> loadForm(Class<D> type, String id);

    @Get
    @MethodCredential(Permission.WRITE_REFERENTIAL)
    <D extends ReferentialDto> Form<D> preCreate(Class<D> type);

    @Write
    @Post
    @MethodCredential(Permission.WRITE_REFERENTIAL)
    <D extends ReferentialDto> SaveResultDto save(D bean) throws ConcurrentModificationException;

    @Write
    @Delete
    @MethodCredential(Permission.WRITE_REFERENTIAL)
    <D extends ReferentialDto> void delete(Class<D> type, String id);

    @Post
    @Write
    @MethodCredential(Permission.ALL)
    <D extends ReferentialDto> void replaceReference(Class<D> beanType, String idToReplace, String replaceId);

    @Post
    @Write
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    <D extends ReferentialDto> void replaceReferenceInData(Class<D> beanType, String idToReplace, String replaceId);

    @Post
    @Write
    @MethodCredential(Permission.ALL)
    <D extends ReferentialDto> void changeId(Class<D> beanType, String id, String newId);

    @Get
    @MethodCredential(Permission.READ_REFERENTIAL)
    <D extends ReferentialDto> boolean exists(Class<D> type, String id);

    @Post
    @MethodCredential(Permission.READ_REFERENTIAL)
    ReferentialIds getReferentialIds();

    @Post(useMultiPartForm = true)
    @Write
    @MethodCredential(Permission.READ_ALL_AND_WRITE_DATA)
    void insertMissingReferential(SqlScript sqlContent);
}
