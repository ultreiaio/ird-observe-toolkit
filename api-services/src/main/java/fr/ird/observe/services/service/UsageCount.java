package fr.ird.observe.services.service;

/*-
 * #%L
 * Toolkit :: API :: Services
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdDtoBean;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * Created on 14/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.41
 */
public class UsageCount extends LinkedHashMap<Class<? extends BusinessDto>, Long> implements JsonAware {

    public static UsageCount count(Function<ToolkitIdDtoBean, UsageCount> usageCountFunction, Set<ToolkitIdDtoBean> ids) {
        UsageCount result = new UsageCount();
        for (ToolkitIdDtoBean id : ids) {
            result.add0(usageCountFunction, id);
        }
        return result.getOrNullIfEmpty();
    }

    public UsageCount() {
    }

    public UsageCount(UsageCount parent) {
        putAll(parent);
    }

    public UsageCount getOrNullIfEmpty() {
        return isEmpty() ? null : this;
    }

    public void merge(UsageCount usageCount) {
        for (Map.Entry<Class<? extends BusinessDto>, Long> entry : usageCount.entrySet()) {
            Class<? extends BusinessDto> key = entry.getKey();
            Long value = entry.getValue();
            merge(key, value, Long::sum);
        }
    }

    protected <X> void add0(Function<X, UsageCount> usageCountFunction, X id) {
        UsageCount usageCount = usageCountFunction.apply(id).getOrNullIfEmpty();
        if (usageCount == null) {
            return;
        }
        merge(usageCount);
    }
}
