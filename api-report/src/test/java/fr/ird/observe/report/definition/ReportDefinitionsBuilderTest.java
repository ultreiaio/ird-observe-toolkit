package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.definition.i18n.ReportI18nNamedStringDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nDefinition;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReportDefinitionsBuilderTest {

    static void assertReports(List<ReportDefinition> reports) {

        Assert.assertNotNull(reports);
        Assert.assertEquals(2, reports.size());

        assertReport1(reports.get(0));
        assertReport2(reports.get(1));

    }

    static void assertReport1(ReportDefinition report) {
        Assert.assertNotNull(report);
        Assert.assertEquals("reportOne", report.getId());
        Assert.assertEquals("Rapport numéro 1", report.getName());
        Assert.assertEquals("Description du rapport numéro 1", report.getDescription());
        Assert.assertEquals(4, report.getColumns());
        Assert.assertEquals("Colonne 1", report.getColumnHeaders()[0]);
        Assert.assertEquals("Colonne 2", report.getColumnHeaders()[1]);
        Assert.assertEquals("Colonne 3", report.getColumnHeaders()[2]);
        Assert.assertEquals("Dernière colonne", report.getColumnHeaders()[3]);

        Assert.assertEquals(0, report.getVariables().length);
        Assert.assertEquals(0, report.getRepeatVariables().length);
        Assert.assertEquals(0, report.getOperations().length);
        Assert.assertEquals(1, report.getRequests().length);

        ReportRequestDefinition request1 = report.getRequests()[0];
        Assert.assertNotNull(request1);
        Assert.assertEquals(0, request1.getX());
        Assert.assertEquals(0, request1.getY());
        Assert.assertEquals(RequestLayout.row, request1.getLayout());
        Assert.assertEquals("Select * from uneTable;", request1.getRequest());
        Optional<ReportI18nDefinition> optionalI18n = report.i18n();
        Assert.assertTrue(optionalI18n.isPresent());
        ReportI18nDefinition i18n = optionalI18n.get();
        Map<String, ReportI18nNamedStringDefinition> translations = i18n.getTranslations();
        Assert.assertNotNull(translations);
        Assert.assertEquals(1, translations.size());
        Assert.assertEquals("oui", i18n.getTranslation(ReferentialLocale.FR, "@vi@"));
        Assert.assertEquals("si", i18n.getTranslation(ReferentialLocale.ES, "@vi@"));
        Assert.assertEquals("yes", i18n.getTranslation(ReferentialLocale.UK, "@vi@"));


    }

    static void assertReport2(ReportDefinition report) {
        Assert.assertNotNull(report);
        Assert.assertEquals("reportTwo", report.getId());
        Assert.assertEquals("Rapport numéro 2", report.getName());
        Assert.assertEquals("Description du rapport numéro 2", report.getDescription());
        Assert.assertEquals(5, report.getColumns());
        Assert.assertEquals("Colonne 1", report.getColumnHeaders()[0]);
        Assert.assertEquals("Colonne 2", report.getColumnHeaders()[1]);
        Assert.assertEquals("Colonne 3", report.getColumnHeaders()[2]);
        Assert.assertEquals("Avant dernière colonne", report.getColumnHeaders()[3]);
        Assert.assertEquals("Total", report.getColumnHeaders()[4]);

        Assert.assertEquals(1, report.getVariables().length);
        ReportVariableDefinition<?> variable = report.getVariables()[0];
        Assert.assertNotNull(variable);
        Assert.assertEquals("groupId", variable.getName());
        Assert.assertEquals(ReferentialDtoReference.class, variable.getType());
        Assert.assertEquals("From SpeciesGroupImpl ge Order By ge.code", variable.getRequest());

        Assert.assertEquals(1, report.getRepeatVariables().length);
        ReportRepeatVariableDefinition<?> repeatVariable = report.getRepeatVariables()[0];
        Assert.assertNotNull(repeatVariable);
        Assert.assertEquals("typeObjetId", repeatVariable.getName());
        Assert.assertEquals(String.class, repeatVariable.getType());
        Assert.assertEquals("Select id from uneTable where group = :groupId order By code;", repeatVariable.getRequest());
        Assert.assertTrue(repeatVariable.isAddNullValue());
        Assert.assertEquals(1, report.getOperations().length);
        ReportOperationDefinition operation = report.getOperations()[0];
        Assert.assertEquals(ReportOperationDefinition.of("SumRow", "FirstOperationParameters"), operation);

        Assert.assertEquals(2, report.getRequests().length);

        ReportRequestDefinition request1 = report.getRequests()[0];
        Assert.assertNotNull(request1);
        Assert.assertEquals(0, request1.getX());
        Assert.assertEquals(0, request1.getY());
        Assert.assertEquals(RequestLayout.row, request1.getLayout());
        Assert.assertEquals("Select * from uneTable where id = :typeObjectId;", request1.getRequest());
        RequestRepeatVariableDefinition repeat1 = request1.getRepeat();
        Assert.assertNotNull(repeat1);
        Assert.assertEquals("typeObjetId", repeat1.getVariableName());
        Assert.assertEquals(RequestLayout.column, repeat1.getLayout());

        ReportRequestDefinition request2 = report.getRequests()[1];
        Assert.assertNotNull(request2);
        Assert.assertEquals(1, request2.getX());
        Assert.assertEquals(0, request2.getY());
        Assert.assertEquals(RequestLayout.row, request2.getLayout());
        Assert.assertEquals("Select * from uneAutreTable where id = :typeObjectId;", request2.getRequest());
        RequestRepeatVariableDefinition repeat2 = request2.getRepeat();
        Assert.assertNotNull(repeat2);
        Assert.assertEquals("typeObjetId", repeat2.getVariableName());
        Assert.assertEquals(RequestLayout.column, repeat2.getLayout());
    }

    @Test
    public void loadDefaultByPath() throws Exception {
        Path basedir = new File("").getAbsoluteFile().toPath();
        Path path = basedir.resolve("src").resolve("test").resolve("resources");
        List<ReportDefinition> reports = DefaultReportDefinitionsBuilder.build(Objects.requireNonNull(path));
        ReportDefinitionsBuilderTest.assertReports(reports);
    }

    @Test
    public void loadDefaultByClassPath() throws Exception {
        URL location = getClass().getResource(ReportDefinitionsBuilder.REPORT_LIST_LOCATION);
        List<ReportDefinition> reports = DefaultByClassPathReportDefinitionsBuilder.build(Objects.requireNonNull(location));
        ReportDefinitionsBuilderTest.assertReports(reports);
    }

}
