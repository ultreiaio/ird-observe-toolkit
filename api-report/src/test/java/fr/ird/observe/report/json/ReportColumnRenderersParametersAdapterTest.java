package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.renderers.HighlightIfAbsoluteDeltaIsPositive;
import fr.ird.observe.report.renderers.HighlightIfEquals18nReferentialValue;
import fr.ird.observe.report.renderers.HighlightIfNumericalValueIsPositive;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created at 19/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class ReportColumnRenderersParametersAdapterTest {

    private static void assertEquals(ReportColumnRenderersParameters expected, ReportColumnRenderersParameters actual) {
        Assert.assertEquals(expected.getColumnRendererParameters(), actual.getColumnRendererParameters());
    }

    @Test
    public void test() {

        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls().setPrettyPrinting();
        JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);
        Gson gson = gsonBuilder.create();

        ReportColumnRenderersParameters expected = new ReportColumnRenderersParameters(List.of(
                new HighlightIfAbsoluteDeltaIsPositive().parseParameters("0|1|0.5f|0.1f"),
                new HighlightIfEquals18nReferentialValue().parseParameters("0,1,2|ok|error"),
                new HighlightIfNumericalValueIsPositive().parseParameters("0|1.0f|2.5f")
        ));

        String json = gson.toJson(expected);
        ReportColumnRenderersParameters actual = gson.fromJson(json, ReportColumnRenderersParameters.class);
        assertEquals(expected, actual);
    }
}
