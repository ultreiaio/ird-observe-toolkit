package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.report.ReportVariable;
import fr.ird.observe.report.definition.ReportVariableDefinition;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * Created at 19/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class ReportVariableAdapterTest {

    private static <V> void assertEquals(ReportVariable<V> expected, ReportVariable<V> actual) {
        Assert.assertEquals(expected.definition(), actual.definition());
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getRequest(), actual.getRequest());
        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getType(), actual.getType());
        Assert.assertEquals(expected.getValues(), actual.getValues());
        Assert.assertEquals(expected.getSelectedValue(), actual.getSelectedValue());
    }

    @Test
    public void test() {
        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls().setPrettyPrinting();
        JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);
        Gson gson = gsonBuilder.create();

        ReportVariable<String> expected = new ReportVariable<>(new ReportVariableDefinition<>(
                "name",
                String.class,
                "Request",
                "Comment"
        ));
        expected.setValues(Set.of("a", "b", "c"));

        String json = gson.toJson(expected);
        Type type = TypeToken.getParameterized(ReportVariable.class, String.class).getType();
        ReportVariable<String> actual = gson.fromJson(json, type);
        Assert.assertEquals(expected, actual);

        expected.setSelectedValue("a");
        json = gson.toJson(expected);
        actual = gson.fromJson(json, type);
        assertEquals(expected, actual);

    }
}
