package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.definition.ReportRepeatVariableDefinition;
import io.ultreia.java4all.util.json.JsonAdapterProvider;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created at 19/09/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.7
 */
public class ReportRepeatVariableAdapterTest {

    private static <V> void assertEquals(ReportRepeatVariable<V> expected, ReportRepeatVariable<V> actual) {
        Assert.assertEquals(expected.definition(), actual.definition());
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getRequest(), actual.getRequest());
        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getType(), actual.getType());
        Assert.assertEquals(expected.getValues(), actual.getValues());
        Assert.assertEquals(expected.isAddNullValue(), actual.isAddNullValue());
    }

    @Test
    public void test() {

        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls().setPrettyPrinting();
        JsonAdapterProvider.offers(gsonBuilder::registerTypeAdapter, gsonBuilder::registerTypeHierarchyAdapter);
        Gson gson = gsonBuilder.create();

        ReportRepeatVariable<String> expected = new ReportRepeatVariable<>(new ReportRepeatVariableDefinition<>(
                "name",
                String.class,
                "Request",
                "Comment",
                false
        ));
        expected.setValues(Set.of("a", "b", "c"));

        String json = gson.toJson(expected);
        ReportRepeatVariable<String> actual = gson.fromJson(json, TypeToken.getParameterized(ReportRepeatVariable.class, String.class).getType());
        assertEquals(expected, actual);
    }
}
