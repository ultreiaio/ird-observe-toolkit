/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
function HighlightIfNumericalValueIsPositive(cell, warningThreshHold, errorThreshHold) {
    if (!cell) {
        return;
    }
    if (!!!cell) {
        return {
            'data-cell-content': "",
            'class': 'gridjs-td cellNull',
        };
    }
    let delta = Math.abs(cell);
    if (delta < warningThreshHold) {
        return;
    }
    return {
        'data-cell-content': cell,
        'class': 'gridjs-td ' + (delta < errorThreshHold ? "cellWarning" : "cellError"),
    };
}

function initHighlightIfNumericalValueIsPositive(column, warningThreshHold, errorThreshHold, result, json) {
    result[column] = {
        name: result [column],
        attributes: cell => HighlightIfNumericalValueIsPositive(cell, warningThreshHold, errorThreshHold)
    };
}
