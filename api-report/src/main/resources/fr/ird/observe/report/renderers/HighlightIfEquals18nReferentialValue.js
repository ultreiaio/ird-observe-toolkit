/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
function HighlightIfEquals18nReferentialValue(cell, labelError, labelOk) {
    if (!!!cell || cell === labelOk) {
        return;
    }
    return {
        'data-cell-content': cell,
        'class': 'gridjs-td ' + (cell === labelError ? "cellError" : "cellWarning"),
    };
}

function initHighlightIfEquals18nReferentialValue(columns, labelError, labelOk, result, json) {
    for (let j = 0; j < columns.length; j++) {
        let column = columns[j];
        result [column] = {
            name: result [column],
            attributes: cell => HighlightIfEquals18nReferentialValue(cell, labelError, labelOk)
        };
    }
}
