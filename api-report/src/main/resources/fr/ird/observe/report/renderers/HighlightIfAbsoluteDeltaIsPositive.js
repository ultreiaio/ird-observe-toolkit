/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
function HighlightIfAbsoluteDeltaIsPositive(cell, row, column2, warningThreshHold, errorThreshHold) {
    if (!cell) {
        return;
    }
    if (!!!cell) {
        return {
            'data-cell-content': "",
            'class': 'gridjs-td cellNull',
        };
    }
    let value2 = row.cells[column2].data;
    if (!!!value2) {
        return;
    }
    if (cell === value2) {
        return;
    }
    let delta = Math.abs(cell - value2);
    if (delta < warningThreshHold) {
        return;
    }
    return {
        'data-cell-content': cell,
        'class': 'gridjs-td ' + (delta < errorThreshHold ? "cellWarning" : "cellError"),
    };
}

function initHighlightIfAbsoluteDeltaIsPositive(column1, column2, warningThreshHold, errorThreshHold, result, json) {
    result[column1] = {
        name: result [column1],
        attributes: (cell, row) => HighlightIfAbsoluteDeltaIsPositive(cell, row, column2, warningThreshHold, errorThreshHold)
    };
    result[column2] = {
        name: result [column2],
        attributes: (cell, row) => HighlightIfAbsoluteDeltaIsPositive(cell, row, column1, warningThreshHold, errorThreshHold)
    };
}
