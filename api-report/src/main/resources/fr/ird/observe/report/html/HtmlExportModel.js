/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
class GridHandler {
    /**
     * Where to store configuration of the grid.
     */
    config;
    rowCount;
    searchOption;
    resizableOption;
    paginationOption;
    paginationSizeOption;
    sortOption;
    gridContainerParent;

    constructor(document, language, columnsSupplier, json) {
        this.gridContainerParent = document.getElementById("wrapperParent");
        this.searchOption = document.getElementById("search");
        this.resizableOption = document.getElementById("resizable");
        this.paginationOption = document.getElementById("pagination");
        this.paginationSizeOption = document.getElementById("paginationSize");
        this.sortOption = document.getElementById("sort");
        json.data.data = this.deserializeJson(json.data);

        this.rowCount = json.data['height'];
        this.searchOption['checked'] = true;
        document.getElementById("rowCount").innerHTML = this.rowCount;
        this.resizableOption['checked'] = true;
        this.sortOption['checked'] = false;
        this.paginationOption['checked'] = false;
        this.paginationSizeOption.value = 20;
        this.paginationSizeOption.disabled = true;
        this.config = {
            language: language,
            columns: columnsSupplier(json),
            data: json.data.data,
            search: this.searchValue(),
            resizable: this.resizableValue(),
            sort: this.sortValue(),
            pagination: this.paginationValue()
        }
    }

    updateGrid() {
        this.gridContainerParent.innerHTML = '<div id="wrapper"></div>';
        setTimeout(() => {
            // noinspection JSUnresolvedReference
            new gridjs.Grid(this.toGridConfig()).render(document.getElementById("wrapper"));
        }, 50);
    }

    toGridConfig() {
        return {
            language: this.config.language,
            data: this.config.data,
            columns: this.deepCopyColumns(this.config.columns),
            search: this.config.search,
            resizable: this.config.resizable,
            sort: this.config.sort,
            pagination: this.config.pagination,
        }
    }

    deepCopyColumns(columns) {
        let result = [];
        let index = 0;
        for (const column of columns) {
            const columnName = column instanceof Object ? column['name'] : column;
            const newColumn = {
                name: columnName,
                formatter: cell => {
                    if (!!!cell) {
                        return cell;
                    }
                    let value = cell === '-' ? '&nbsp;' : cell.replace(/\s/g, '&nbsp;');
                    // noinspection JSUnresolvedReference
                    return value.indexOf("&nbsp;") > -1 ? gridjs.html(value) : value;
                }
            };
            if (column instanceof Object) {
                if (column['attributes'] != null) {
                    newColumn['attributes'] = column['attributes'];
                }
            }
            result[index++] = newColumn;
        }
        return result;
    }

    deserializeJson(json) {
        let height = json.height;
        let width = json.width;
        let data = json.rows;
        let result = new Array(height);
        for (let row = 0; row < height; row++) {
            let cells = data[row].split('||');
            let realRow = new Array(width);
            result[row] = realRow;
            for (let column = 0; column < width; column++) {
                let rowElement = cells[column];
                realRow[column] = rowElement === '$' ? null : rowElement;
            }
        }
        return result;
    }

    searchValue() {
        return this.searchOption.checked;
    }

    resizableValue() {
        return this.resizableOption.checked;
    }

    sortValue() {
        return this.sortOption.checked;
    }

    paginationValue() {
        return this.paginationOption.checked ? {limit: this.paginationSizeOption.value} : false;
    }

    toggleSearch(source) {
        this.config.search = this.searchValue();
        this.updateGrid();
    }

    toggleResizable(source) {
        this.config.resizable = this.resizableValue();
        this.updateGrid();
    }

    toggleSort(source) {
        this.config.sort = this.sortValue();
        this.updateGrid();
    }

    togglePagination(source) {
        let newValue = source.checked;
        if (newValue) {
            this.paginationSizeOption["disabled"] = null;
        } else {
            this.paginationSizeOption.disabled = true;
        }
        this.config.pagination = this.paginationValue();
        this.updateGrid();
    }

    changePaginationSize(source) {
        this.config.pagination = this.paginationValue();
        this.updateGrid();
    }

    init() {

        this.updateGrid();
        let that = this;
        this.searchOption.addEventListener("change", function () {
            that.toggleSearch(this);
        });
        this.resizableOption.addEventListener("change", function () {
            that.toggleResizable(this);
        });
        this.sortOption.addEventListener("change", function () {
            that.toggleSort(this);
        });
        this.paginationOption.addEventListener("change", function () {
            that.togglePagination(this);
        });

        this.paginationSizeOption.addEventListener("change", function () {
            that.changePaginationSize(this);
        });
    }
}
