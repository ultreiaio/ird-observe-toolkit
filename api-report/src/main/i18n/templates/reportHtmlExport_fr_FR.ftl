<#--noinspection JSUnresolvedReference-->
<#-- @ftlvariable name=".data_model" type="fr.ird.observe.report.html.HtmlExportModel" -->
<!DOCTYPE html>
<!--
#%L
 Toolkit :: API :: Report
 %%
 Copyright (C) 2017 - 2024 Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html lang="fr">
<head>
    <link href="https://unpkg.com/gridjs/dist/theme/mermaid.min.css" rel="stylesheet"/>
    <title>Rapport ${.data_model.getReportName()}</title>
    <script src="https://unpkg.com/gridjs/dist/gridjs.umd.js"></script>
    <style>
        ${.data_model.style}
    </style>
    <script type="application/javascript">
        ${.data_model.script}

        <#list .data_model.columnRendererFunctions as key>
        ${key}
        </#list>
    </script>
</head>
<body>
<h1>Rapport ${.data_model.getReportName()}</h1>
<div id="main-tab" class="widget">
    <div class="widget-list" id="tab-configuration">
        <h3>Données sélectionnées :</h3>
        <ul>
            <li>
                <#assign selectDataCount = .data_model.getSelectedCount() />

                <#if selectDataCount == 1> 1 marée <#else> ${selectDataCount} marées</#if>.
                <ul>
                    <#list .data_model.getSelectedLabels() as key, value>
                        <li>
                            ${key}
                            <ul>
                                <#list value as trip>
                                    <li>${trip}</li>
                                </#list>
                            </ul>
                        </li>
                    </#list>
                </ul>
            </li>
        </ul>
        <h3>Rapport sélectionné :</h3>

        <#assign selectedReport = .data_model.selectedReport />
        <p><b>${.data_model.getReportName()}</b></p>
        <ul>
            <#list selectedReport.getVariables() as variable>
                <li>
                    Variable ${.data_model.getVariableName(variable)} : ${variable.selectedValue}.
                </li>
            </#list>
        </ul>
    </div>
    <div class="widget-list" id="tab-result">
        <div class="config-panel">
            <label><input id="search" type="checkbox"/> Recherche</label>|
            <label><input id="resizable" type="checkbox"/> Colonnes redimensionnables</label>|
            <label><input id="sort" type="checkbox"/> Tri des colonnes</label>|
            <label><input id="pagination" type="checkbox"/> Pagination</label>|
            <label>Nombre de lignes par page&nbsp;<input id="paginationSize" type="number"/></label>|
            <label>Nombre de résultat(s)&nbsp;:&nbsp;<b><span id="rowCount"
                                                              style="font-style: italic"></span></b></label>

        </div>
        <div id="wrapperParent">
            <div id="wrapper"></div>
        </div>
    </div>
    <ul class="widget-tabs">
        <li class="widget-tab"><a href="#tab-configuration" class="widget-tab-link">Méta-données</a>
        <li class="widget-tab"><a href="#tab-result" class="widget-tab-link">Résultat</a>
    </ul>
</div>

<script type="application/javascript">
    new GridHandler(
        document,
        {
            'search': {
                'placeholder': '🔍 Recherche...'
            },
            sort: {
                sortAsc: 'Tri ascendant',
                sortDesc: 'Tri descendant',
            },
            pagination: {
                previous: 'Précédent',
                next: 'Suivant',
                navigate: (page, pages) => `Page ${r"${page}"} sur ${r"${pages}"}`,
                page: (page) => `Page ${r"${page}"}`,
                showing: 'Affichage des lignes de',
                of: 'sur',
                to: 'à',
                results: 'lignes.',
            },
            loading: 'Chargement...'
        },
        function (json) {
            let result = !!json["columnNames"] ? json["columnNames"] : [];
            if (result.length === 0) {
                return result;
            }
            let data = json.data;
            <#list .data_model.columnRendererInitCode as  value>
            ${value}</#list>
            return result;
        },
        ${.data_model.json}).init();
</script>
</body>
</html>
