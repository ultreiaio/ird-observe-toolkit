package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.SortedProperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created at 06/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class DefaultReportDefinitionsBuilder extends ReportDefinitionsBuilderPropertiesSupport {
    public static final String EXTENSION = ".report";
    /**
     * The report file location.
     */
    private final Path location;

    /**
     * Cache of detected reports indexed by their reportId.
     */
    private Map<String, Path> reportFiles;

    private DefaultReportDefinitionsBuilder(Path location) {
        this.location = Objects.requireNonNull(location);
    }

    public static List<ReportDefinition> build(Path location) throws IOException {
        try (ReportDefinitionsBuilder builder = new DefaultReportDefinitionsBuilder(location)) {
            return builder.build();
        }
    }

    public static Set<Path> detectReportFiles(Path importPath) {
        Set<Path> reportsList = new TreeSet<>();
        Set<String> reportsNames = new TreeSet<>();
        try (Stream<Path> stream = Files.find(importPath, 5, (f, a) -> a.isRegularFile() && f.toFile().getName().endsWith(EXTENSION)).sorted()) {
            for (Path path : stream.collect(Collectors.toList())) {
                String reportId = fileNameToReportId(path);
                if (!reportsNames.add(reportId)) {
                    throw new IllegalStateException("Found duplicated report name: " + reportId);
                }
                reportsList.add(path);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return reportsList;
    }

    public static String fileNameToReportId(Path file) {
        String name = file.toFile().getName();
        return fileNameToReportId(name);
    }

    public static String fileNameToReportId(String name) {
        return name.substring(0, name.length() - EXTENSION.length());
    }

    @Override
    public Set<String> detectReportNames() {
        return getReportFiles().keySet().stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public ReportDefinition buildReport(String reportName) {
        Map<String, String> properties = new TreeMap<>();
        try (BufferedReader reader = Files.newBufferedReader(Objects.requireNonNull(getReportFiles().get(reportName)))) {
            Properties store = new SortedProperties();
            store.load(reader);
            store.forEach((key, value) -> properties.put((String) key, (String) value));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ReportDefinition result = buildReport(reportName, properties);
        if (!properties.isEmpty()) {
            // if still some properties not used, build failed
            throw new IllegalStateException(String.format("On report %s - there is some unknown properties we can not treat:\n%s", reportName, properties));
        }
        return result;
    }

    @Override
    public void close() {
        if (reportFiles != null) {
            reportFiles.clear();
        }
    }

    public Map<String, Path> getReportFiles() {
        if (reportFiles == null) {
            reportFiles = detectReportFiles(location).stream().collect(Collectors.toMap(DefaultReportDefinitionsBuilder::fileNameToReportId, Function.identity()));
        }
        return reportFiles;
    }
}
