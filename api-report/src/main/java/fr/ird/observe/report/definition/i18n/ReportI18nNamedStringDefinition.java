package fr.ird.observe.report.definition.i18n;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialLocale;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Function;

/**
 * Created at 05/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportI18nNamedStringDefinition extends ReportI18nStringDefinition {

    /**
     * name of this item (will be used to translate report content att runtime.
     */
    private final String name;

    public ReportI18nNamedStringDefinition(String name, String label, String enLabel, String esLabel) {
        super(label, enLabel, esLabel);
        this.name = Objects.requireNonNull(name);
    }

    public static ReportI18nNamedStringDefinition of(String name, Function<ReferentialLocale, String> transformer) {
        return new ReportI18nNamedStringDefinition(name, transformer.apply(ReferentialLocale.FR), transformer.apply(ReferentialLocale.UK), transformer.apply(ReferentialLocale.ES));
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return toString(new StringJoiner(", ", ReportI18nNamedStringDefinition.class.getSimpleName() + "[", "]"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportI18nNamedStringDefinition)) return false;
        ReportI18nNamedStringDefinition that = (ReportI18nNamedStringDefinition) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
