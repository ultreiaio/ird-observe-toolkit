package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.matrix.DataMatrix;
import io.ultreia.java4all.util.matrix.DataMatrixDimension;

import java.awt.Point;
import java.util.List;
import java.util.function.Supplier;

/**
 * Definition of a report request.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public final class ReportRequestDefinition implements WithRequest {

    public static final String TRIP_ID_VARIABLE = "tripId";
    private static final long serialVersionUID = 1L;
    /**
     * Layout of the request.
     */
    private final RequestLayout layout;
    /**
     * Request to execute.
     */
    private final String request;
    /**
     * Location of the result request in result matrix.
     */
    private final Point location;
    /**
     * Optional request repeat variable.
     */
    private final RequestRepeatVariableDefinition repeat;
    /**
     * Optional comment.
     */
    private final String comment;

    public ReportRequestDefinition(RequestLayout layout,
                                   int x,
                                   int y,
                                   String request,
                                   RequestRepeatVariableDefinition repeat, String comment) {
        this.layout = layout;
        this.request = request;
        this.repeat = repeat;
        this.comment = comment;
        this.location = new Point(x, y);
    }

    public RequestLayout getLayout() {
        return layout;
    }

    public int getX() {
        return (int) location.getX();
    }

    public int getY() {
        return (int) location.getY();
    }

    public Point getLocation() {
        return location;
    }

    @Override
    public String getRequest() {
        return request;
    }

    public String getComment() {
        return comment;
    }

    public RequestRepeatVariableDefinition getRepeat() {
        return repeat;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ReportRequestDefinition{");
        sb.append("location=").append(String.format("%d,%d", location.x, location.y));
        sb.append(", layout=").append(layout);
        sb.append(", request='").append(request).append('\'');
        if (withRepeat()) {
            sb.append(", repeat=").append(repeat);
        }
        if (withComment()) {
            sb.append(", comment=").append(comment);
        }
        sb.append('}');
        return sb.toString();
    }

    public DataMatrixDimension computeDimension(List<?> list) {
        int height = 0;
        int width = 0;
        switch (getLayout()) {
            case row:
                // le count de result est le nombre de lignes
                height = list.size();
                if (!list.isEmpty()) {
                    Object o = list.get(0);
                    if (o == null || !o.getClass().isArray()) {
                        // une seule colonne
                        width = 1;
                    } else {
                        width = ((Object[]) o).length;
                    }
                }
                break;
            case column:
                // le count de result est le nombre de colonnes
                width = list.size();
                if (!list.isEmpty()) {
                    Object o = list.get(0);
                    if (o == null || !o.getClass().isArray()) {
                        // une seule ligne
                        height = 1;
                    } else {
                        height = ((Object[]) o).length;
                    }
                }
                break;
        }
        return new DataMatrixDimension(width, height);
    }

    public DataMatrix computeResult(DataMatrixDimension dimension, List<?> list, Supplier<DataMatrix> matrixFactory) {
        DataMatrix result = matrixFactory.get();
        result.setDimension(dimension);
        result.createData();
        // le seul cas différent est le n-* (une ligne correspond à une colonne)
        int y = 0;
        int x = 0;
        switch (getLayout()) {
            case row:
                boolean uniqueColumn = result.getWidth() == 1;
                // les lignes du résultat sont les lignes du tableau
                for (Object row : list) {
                    if (uniqueColumn) {
                        // une seule colonne
                        result.setValue(0, y, row);
                    } else {
                        x = 0;
                        Object[] cells = (Object[]) row;
                        for (Object cell : cells) {
                            result.setValue(x++, y, cell);
                        }
                    }
                    // on passage a la ligne suivante
                    y++;
                }
                break;
            case column:
                // les lignes du résultat sont les colonnes du tableau
                boolean uniqueRow = result.getHeight() == 1;
                for (Object col : list) {
                    if (uniqueRow) {
                        // une seule ligne
                        result.setValue(x, 0, col);
                    } else {
                        y = 0;
                        Object[] cells = (Object[]) col;
                        for (Object cell : cells) {
                            result.setValue(x, y++, cell);
                        }
                    }
                    // on passage a la colonne suivante
                    x++;
                }
                break;
        }
        // on pousse la position du résultat
        result.setX(getX());
        result.setY(getY());
        return result;
    }

    public boolean withRepeat() {
        return repeat != null;
    }

    public boolean withComment() {
        return comment != null;
    }
}
