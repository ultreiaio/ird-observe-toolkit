package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportVariableDefinition;

/**
 * Definition of a variable of a report, that user need to select {@code selectedValue} among the {@code values}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.7
 */
public final class ReportVariable<V> extends ReportVariableSupport<V, ReportVariableDefinition<V>> {

    public final static String PROPERTY_SELECTED_VALUE = "selectedValue";

    private static final long serialVersionUID = 1L;

    /**
     * Selected value for this variable, that will be used to execute report.
     */
    private V selectedValue;

    public ReportVariable(ReportVariableDefinition<V> definition) {
        super(definition);
    }

    public V getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(V selectedValue) {
        this.selectedValue = selectedValue;
    }

    public boolean isSelectedValueFilled() {
        return getSelectedValue() != null;
    }

    public boolean isValuesLoadedAndSelectedValueFilled() {
        return isValuesLoaded() && isSelectedValueFilled();
    }

    @Override
    public void unload() {
        super.unload();
        this.selectedValue = null;
    }

}
