package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Set;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
@AutoService(ReportOperationConsumer.class)
public class SumRow implements ReportOperationConsumer {
    private static final Logger log = LogManager.getLogger(SumRow.class);

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        int x = 0;
        int y = 0;
        int width = incoming.getWidth();
        int height = incoming.getHeight();


        if (!parameters.isEmpty()) {
            String[] split = parameters.split("\\|");
            x = Integer.parseInt(split[0]);
            y = Integer.parseInt(split[1]);
            if (y + 1 >= width) {
                // only one column in row to sum, so skip operation
                return incoming;
            }
        }
        DataMatrix tmpMatrix = createTmpMatrix(width, 0, 1, height);
        for (int row = x; row < height; row++) {
            Object sumRow = getSumRow(row, y, incoming);
            tmpMatrix.setValue(0, row, sumRow);
        }
        DataMatrix merge = DataMatrix.merge(incoming, tmpMatrix);
        if (report.getColumnHeaders() == null) {
            merge.setValue(width, 0, requestExecutor.i18nContext().translate(I18n.n("observe.Common.total")));
        }
        return merge;
    }

    protected Object getSumRow(int row, int y, DataMatrix incoming) {
        float result = 0f;
        int nbColumns = incoming.getWidth();
        for (int col = y; col < nbColumns; col++) {
            Serializable o = incoming.getValue(col, row);
            if (o == null || "null".equals(o)) {
                o = 0;
            }
            double d;
            try {
                d = Double.parseDouble(o.toString());
            } catch (NumberFormatException e) {
                // une des données de la colonne n'est pas un count on sort directement
                log.debug(String.format("Could not convert %s to number", o), e);
                return "-";
            }
            result += d;
        }
        return Numbers.roundFourDigits(result);
    }
}
