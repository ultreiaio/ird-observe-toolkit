package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.definition.WithRequest;
import fr.ird.observe.report.definition.i18n.ReportI18nNamedStringDefinition;
import io.ultreia.java4all.i18n.I18n;

import java.util.Map;
import java.util.Objects;

/**
 * Created at 05/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportRequestExecutorI18nContext {

    private final Map<String, ReportI18nNamedStringDefinition> defaultMapping;

    private final ReferentialLocale referentialLocale;

    public ReportRequestExecutorI18nContext(ReferentialLocale referentialLocale) {
        this.referentialLocale = Objects.requireNonNull(referentialLocale);
        this.defaultMapping = Map.of(
                "@i18nColumnName@", ReportI18nNamedStringDefinition.of("@i18nColumnName@", ReferentialLocale::getColumnName),
                "@i18nLanguage@", ReportI18nNamedStringDefinition.of("@i18nLanguage@", r -> r.getLocale().getLanguage().toLowerCase()),
                "@yes@", ReportI18nNamedStringDefinition.of("@yes@", r -> I18n.l(r.getLocale(), "boolean.true")),
                "@no@", ReportI18nNamedStringDefinition.of("@no@", r -> I18n.l(r.getLocale(), "boolean.false")));
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public String translateRequest(ReportDefinition reportDefinition, WithRequest request) {
        final String result = applyI18nIFNecessary(defaultMapping, request.getRequest());
        return reportDefinition.i18n().map(i18n -> applyI18nIFNecessary(i18n.getTranslations(), result)).orElse(result);
    }

    private String applyI18nIFNecessary(Map<String, ReportI18nNamedStringDefinition> i18n, String incoming) {
        String result = incoming;
        for (Map.Entry<String, ReportI18nNamedStringDefinition> entry : i18n.entrySet()) {
            String k = entry.getKey();
            if (result.contains(k)) {
                result = result.replaceAll(k, entry.getValue().apply(referentialLocale));
            }
        }
        return result;
    }

    public String translateParameter(ReportDefinition reportDefinition, String i18nKey) {
        return reportDefinition.i18n().map(i18n -> i18n.getTranslation(referentialLocale, i18nKey)).orElse(i18nKey);
    }

    public String translate(String i18nKey) {
        return I18n.l(getReferentialLocale().getLocale(), i18nKey);
    }
}
