package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ColumnRendererDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdesktop.swingx.JXTable;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created at 29/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public final class ColumnRendererConsumers {

    private static final Logger log = LogManager.getLogger(ColumnRendererConsumers.class);
    private static ColumnRendererConsumers INSTANCE;
    private final Map<String, ColumnRendererConsumer<?>> consumers;

    private ColumnRendererConsumers() {
        log.info("Initializing... {}}", this);
        consumers = new TreeMap<>();
        for (ColumnRendererConsumer<?> definition : ServiceLoader.load(ColumnRendererConsumer.class)) {
            consumers.put(definition.name(), definition);
        }
        log.info("Initialization done {}}, register {} column renderer consumer(s).", this, consumers.size());
    }

    public static ColumnRendererConsumers get() {
        return INSTANCE == null ? INSTANCE = new ColumnRendererConsumers() : INSTANCE;
    }

    public static void checkParameters(ColumnRendererDefinition definition) {
        get().getConsumer(definition.getName()).parseParameters(definition.getParameters());
    }

    public static ColumnRendererParameters createParameters(ReportRequestExecutor requestExecutor, ColumnRendererDefinition definition) {
        return get().getConsumer(definition.getName()).createParameters(requestExecutor, definition.getParameters());
    }

    public static <P extends ColumnRendererParameters> void consumeSwing(P parameters, JXTable table) {
        get().getConsumer(parameters.name()).consumeSwing(parameters, table);
    }

    public static Set<String> htmlFunctions(ReportColumnRenderersParameters columnRenderers) {
        return columnRenderers.getColumnRendererParameters().stream().map(columnRenderer -> get().getConsumer(columnRenderer.name()).htmlFunctions()).collect(Collectors.toCollection(TreeSet::new));
    }

    public static List<String> htmlInitCode(ReportColumnRenderersParameters columnRenderers) {
        return columnRenderers.getColumnRendererParameters().stream().map(r -> get().getConsumer(r.name()).htmlInitCode(r)).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <P extends ColumnRendererParameters> ColumnRendererConsumer<P> getConsumer(String name) {
        return (ColumnRendererConsumer<P>) Objects.requireNonNull(consumers.get(name), "Could not find column renderer with name: " + name);
    }

}
