package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.ReportVariable;

/**
 * Definition of  a report variable.
 * <p>
 * Created on 19/12/2022.
 *
 * @param <V> type of data values of this variable.
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.23
 */
public final class ReportVariableDefinition<V> extends ReportVariableDefinitionSupport<V> {

    public ReportVariableDefinition(String name, Class<V> type, String request, String comment) {
        super(name, type, request, comment);
    }

    public ReportVariable<?> toVariable() {
        return new ReportVariable<>(this);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportVariableDefinition{");
        sb.append("name='").append(getName()).append('\'');
        sb.append(", type=").append(getType().getName());
        sb.append(", request='").append(getRequest()).append('\'');
        if (withComment()) {
            sb.append(", comment='").append(getComment()).append('\'');
        }
        sb.append('}');
        return sb.toString();
    }
}
