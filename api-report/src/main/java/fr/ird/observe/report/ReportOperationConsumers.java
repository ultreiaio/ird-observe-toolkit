package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportOperationDefinition;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
public class ReportOperationConsumers {

    private static final Logger log = LogManager.getLogger(ReportOperationConsumers.class);
    private static ReportOperationConsumers INSTANCE;
    private final Map<String, ReportOperationConsumer> consumers;

    public static ReportOperationConsumers get() {
        return INSTANCE == null ? INSTANCE = new ReportOperationConsumers() : INSTANCE;
    }

    public static DataMatrix executeReportOperation(ReportRequestExecutor requestExecutor, ReportOperationDefinition operation, Report report, Set<String> tripId, DataMatrix incoming) {
        ReportOperationConsumer consumer = get().getConsumer(operation.getName());
        return consumer.consume(operation.getParameters(), requestExecutor, report, tripId, incoming);
    }

    private ReportOperationConsumers() {
        log.info(String.format("Initializing... %s", this));
        consumers = new TreeMap<>();
        for (ReportOperationConsumer definition : ServiceLoader.load(ReportOperationConsumer.class)) {
            consumers.put(definition.name(), definition);
        }
        log.info(String.format("Initialization done %s, register %d report operation consumer(s).", this, consumers.size()));
    }

    public ReportOperationConsumer getConsumer(String name) {
        return consumers.get(name);
    }
}
