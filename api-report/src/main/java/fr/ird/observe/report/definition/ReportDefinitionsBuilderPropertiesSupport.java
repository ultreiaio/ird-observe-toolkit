package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.ColumnRendererConsumers;
import fr.ird.observe.report.definition.i18n.ReportI18nNamedStringDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nStringArrayDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nStringDefinition;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.SortedProperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created at 07/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public abstract class ReportDefinitionsBuilderPropertiesSupport implements ReportDefinitionsBuilder {
    protected static final String VARIABLE_PREFIX = "variable.";
    protected static final Pattern VARIABLE_NAME_PATTERN = Pattern.compile("variable.(\\d+).name");
    protected static final String VARIABLE_TYPE_FORMAT = "variable.%s.type";
    protected static final String VARIABLE_REQUEST_FORMAT = "variable.%s.request";
    protected static final String VARIABLE_COMMENT_FORMAT = "variable.%s.comment";
    protected static final String REPEAT_VARIABLE_PREFIX = "repeatVariable.";
    protected static final Pattern REPEAT_VARIABLE_NAME_PATTERN = Pattern.compile("repeatVariable.(\\d+).name");
    protected static final String REPEAT_VARIABLE_TYPE_FORMAT = "repeatVariable.%s.type";
    protected static final String REPEAT_VARIABLE_REQUEST_FORMAT = "repeatVariable.%s.request";
    protected static final String REPEAT_VARIABLE_ADD_NULL_FORMAT = "repeatVariable.%s.addNull";
    protected static final String REPEAT_VARIABLE_COMMENT_FORMAT = "repeatVariable.%s.comment";
    protected static final String REQUEST_PREFIX = "request.";
    protected static final Pattern REQUEST_LOCATION_PATTERN = Pattern.compile("request.(\\d+).location");
    protected static final String REQUEST_LAYOUT_FORMAT = "request.%s.layout";
    protected static final String REQUEST_REQUEST_FORMAT = "request.%s.request";
    protected static final String REQUEST_COMMENT_FORMAT = "request.%s.comment";
    protected static final String REQUEST_REPEAT_NAME_FORMAT = "request.%s.repeat.name";
    protected static final String REQUEST_REPEAT_LAYOUT_FORMAT = "request.%s.repeat.layout";
    protected static final String OPERATION_PREFIX = "operations.";
    protected static final Pattern OPERATION_TYPE_PATTERN = Pattern.compile("operations.(\\d+).type");
    protected static final String OPERATION_PARAMETERS_FORMAT = "operations.%s.parameters";
    protected static final String I18n_PREFIX = "i18n.";
    protected static final Pattern I18N_PATTERN = Pattern.compile("i18n\\.([^.]+)");
    protected static final String I18N_LABEL_EN_FORMAT = "i18n.%s.en";
    protected static final String I18N_LABEL_ES_FORMAT = "i18n.%s.es";
    protected static final String COLUMN_RENDERER_PREFIX = "columnRenderers.";
    protected static final Pattern COLUMN_RENDERER_TYPE_PATTERN = Pattern.compile("columnRenderers.(\\d+).type");
    protected static final String COLUMN_RENDERER_PARAMETERS_FORMAT = "columnRenderers.%s.parameters";
    protected static final String REPORT_MODEL_TYPE = "modelType";
    protected static final String REPORT_NAME = "name";
    protected static final String REPORT_DESCRIPTION = "description";
    protected static final String REPORT_ROWS = "rows";
    protected static final String REPORT_COLUMNS = "columns";

    protected String getMandatoryReportValue(String reportName, @SuppressWarnings("SameParameterValue") String propertyName, Map<String, String> reportProperties) {
        log.debug("Looking for mandatory report property: {}.{}", reportName, propertyName);
        String result = getOptionalReportValue(reportName, propertyName, reportProperties).orElse(null);
        return Objects.requireNonNull(result, String.format("Could not find report (%s) property %s", reportName, propertyName)).trim();
    }

    protected ReportI18nStringDefinition getI18nMandatoryReportValue(String reportName, String propertyName, Map<String, String> reportProperties) {
        log.debug("Looking for mandatory report property: {}.{}", reportName, propertyName);
        String result = getOptionalReportValue(reportName, propertyName, reportProperties).orElse(null);
        Objects.requireNonNull(result, String.format("Could not find report (%s) property %s", reportName, propertyName));

        String resultEn = getOptionalReportValue(reportName, propertyName + ".en", reportProperties).orElse(null);
        String resultEs = getOptionalReportValue(reportName, propertyName + ".es", reportProperties).orElse(null);
        return new ReportI18nStringDefinition(result, resultEn, resultEs);
    }

    protected Optional<String> getOptionalReportValue(String reportName, String propertyName, Map<String, String> reportProperties) {
        log.debug("Looking for optional report property: {}.{}", reportName, propertyName);
        String result = reportProperties.remove(propertyName);
        return Optional.ofNullable(result == null || result.trim().isEmpty() ? null : result.trim());
    }

    protected ReportI18nStringArrayDefinition loadHeaders(String reportName, String headersType, Map<String, String> reportProperties) {
        Optional<String> headers = getOptionalReportValue(reportName, headersType, reportProperties);
        String[] label = loadHeaders(headers.orElse(null));
        if (label == null) {
            return null;
        }
        Optional<String> headersEn = getOptionalReportValue(reportName, headersType + ".en", reportProperties);
        String[] labelEn = loadHeaders(headersEn.orElse(null));
        Optional<String> headersEs = getOptionalReportValue(reportName, headersType + ".es", reportProperties);
        String[] labelEs = loadHeaders(headersEs.orElse(null));
        return new ReportI18nStringArrayDefinition(label, labelEn, labelEs);
    }

    protected String[] loadHeaders(String headers) {
        if (headers == null) {
            return null;
        }
        return headers.split("\\s*,\\s*");
    }

    protected String getMandatoryVariableValue(String reportName, int variableNumber, String variableName, String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, variableNumber);
        log.debug("Looking for mandatory variable {} property: {}.{}", variableName, reportName, propertyName);
        String result = getOptionalReportValue(reportName, propertyName, reportProperties).orElse(null);
        return Objects.requireNonNull(result, String.format("Could not find report variable (%s) property %s", variableName, propertyName)).trim();
    }

    protected String getMandatoryRepeatVariableValue(String reportName, int variableNumber, String variableName, String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, variableNumber);
        log.debug("Looking for mandatory repeat variable {} property: {}.{}", variableName, reportName, propertyName);
        String result = getOptionalReportValue(reportName, propertyName, reportProperties).orElse(null);
        return Objects.requireNonNull(result, String.format("Could not find report repeat variable (%s) property %s", variableName, propertyName)).trim();
    }

    protected String getMandatoryRequestValue(String reportName, int requestNumber, String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, requestNumber);
        log.debug("Looking for mandatory property on request {}: {}.{}", requestNumber, reportName, propertyName);
        String result = getOptionalReportValue(reportName, propertyName, reportProperties).orElse(null);
        return Objects.requireNonNull(result, String.format("Could not find report on request %s key %s", requestNumber, propertyName)).trim();
    }

    protected Optional<String> getOptionalRequestValue(String reportName, int requestNumber, @SuppressWarnings("SameParameterValue") String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, requestNumber);
        log.debug("Looking for optional property on request {}: {}.{}", requestNumber, reportName, propertyName);
        return getOptionalReportValue(reportName, propertyName, reportProperties);
    }

    protected Optional<String> getOptionalRepeatVariableValue(String reportName, int variableNumber, String variableName, @SuppressWarnings("SameParameterValue") String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, variableNumber);
        log.debug("Looking for optional repeat variable {} property: {}.{}", variableName, reportName, propertyName);
        return getOptionalReportValue(reportName, propertyName, reportProperties);
    }

    protected Optional<String> getOptionalOperationValue(String reportName, int variableNumber, @SuppressWarnings("SameParameterValue") String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, variableNumber);
        log.debug("Looking for optional operation parameters property: {}.{}", reportName, propertyName);
        return getOptionalReportValue(reportName, propertyName, reportProperties);
    }

    protected Optional<String> getOptionalI18nValue(String reportName, String i18nName, String propertyNamePattern, Map<String, String> reportProperties) {
        String propertyName = String.format(propertyNamePattern, i18nName);
        log.debug("Looking for optional i18n property: {}.{}", reportName, propertyName);
        return getOptionalReportValue(reportName, propertyName, reportProperties);
    }

    protected ReportRequestDefinition[] getRequests(String reportName, Map<String, String> reportProperties) {
        Map<Integer, String> locationByOrder = filterProperties(reportProperties, REQUEST_PREFIX, REQUEST_LOCATION_PATTERN, (name, map) -> {
        });

        List<ReportRequestDefinition> result = new ArrayList<>(locationByOrder.size());
        locationByOrder.forEach((key, location) -> {
            ReportRequestDefinition def = getRequest(reportName, key, location, reportProperties);
            log.debug(String.format("Detects on report %s request %d : %s", reportName, key, def));
            result.add(def);
        });
        return result.toArray(ReportRequestDefinition[]::new);
    }

    protected List<ReportOperationDefinition> getOperations(String reportName, Map<String, String> reportProperties) {

        Map<Integer, String> typesByOrder = filterProperties(reportProperties, OPERATION_PREFIX, OPERATION_TYPE_PATTERN, (name, map) -> {
        });

        List<ReportOperationDefinition> result = new ArrayList<>();
        typesByOrder.forEach((operationNumber, operationType) -> {
            Optional<String> optionalParameters = getOptionalOperationValue(reportName, operationNumber, OPERATION_PARAMETERS_FORMAT, reportProperties);
            ReportOperationDefinition def = ReportOperationDefinition.of(operationType, optionalParameters.orElse(""));
            log.debug(String.format("Detects on report %s operation %d : %s", reportName, operationNumber, def));
            result.add(def);
        });
        return result;
    }

    protected List<ReportI18nNamedStringDefinition> getI18ns(String reportName, Map<String, String> reportProperties) {

        Map<String, String> i18nsByName = filterI18nProperties(reportProperties);

        List<ReportI18nNamedStringDefinition> result = new ArrayList<>();
        i18nsByName.forEach((name, label) -> {
            Optional<String> optionalLabelEn = getOptionalI18nValue(reportName, name, I18N_LABEL_EN_FORMAT, reportProperties);
            Optional<String> optionalLabelEs = getOptionalI18nValue(reportName, name, I18N_LABEL_ES_FORMAT, reportProperties);
            ReportI18nNamedStringDefinition def = new ReportI18nNamedStringDefinition(name, label, optionalLabelEn.orElse(null), optionalLabelEs.orElse(""));
            log.debug(String.format("Detects on report %s i18n %s : %s", reportName, name, def));
            result.add(def);
        });
        return result;
    }

    protected List<ColumnRendererDefinition> getColumnRenderers(String reportName, Map<String, String> reportProperties) {

        Map<Integer, String> typesByOrder = filterProperties(reportProperties, COLUMN_RENDERER_PREFIX, COLUMN_RENDERER_TYPE_PATTERN, (name, map) -> {
        });

        List<ColumnRendererDefinition> result = new ArrayList<>();
        typesByOrder.forEach((operationNumber, operationType) -> {
            Optional<String> optionalParameters = getOptionalOperationValue(reportName, operationNumber, COLUMN_RENDERER_PARAMETERS_FORMAT, reportProperties);
            ColumnRendererDefinition def = ColumnRendererDefinition.of(operationType, optionalParameters.orElse(""));
            ColumnRendererConsumers.checkParameters(def);
            log.debug(String.format("Detects on report %s column renderer %d : %s", reportName, operationNumber, def));
            result.add(def);
        });
        return result;
    }

    protected void validateVariableName(String reportName, String name, Map<Integer, String> variableNamesByOrder) {
        if (variableNamesByOrder.containsValue(name)) {
            throw new IllegalArgumentException(String.format("La variable %s est déjà définie pour le rapport %s", name, reportName));
        }
        if (ReportRequestDefinition.TRIP_ID_VARIABLE.equals(name)) {
            throw new IllegalArgumentException(String.format("La variable %s n'est pas utilisable (c'est une variable réservée) pour le rapport %s", ReportRequestDefinition.TRIP_ID_VARIABLE, reportName));
        }
    }

    protected void validateRepeatVariableName(String reportName, String name, Map<Integer, String> variableNamesByOrder, Set<String> variablesNames) {
        if (variableNamesByOrder.containsValue(name)) {
            throw new IllegalArgumentException(String.format("La variable de répétition %s est déjà définie pour le rapport %s", name, reportName));
        }
        if (ReportRequestDefinition.TRIP_ID_VARIABLE.equals(name)) {
            throw new IllegalArgumentException(String.format("La variable de répétition %s n'est pas utilisable (c'est une variable réservée) pour le rapport %s", name, reportName));
        }
        if (variablesNames.contains(name)) {
            throw new IllegalArgumentException(String.format("La variable de répétition %s est déjà définie par une autre variable pour le rapport %s", name, reportName));
        }
    }

    protected List<ReportVariableDefinition<?>> getVariables(String reportName, Map<String, String> reportProperties) {
        Map<Integer, String> namesByOrder = filterProperties(reportProperties, VARIABLE_PREFIX, VARIABLE_NAME_PATTERN, (name, map) -> validateVariableName(reportName, name, map));
        List<ReportVariableDefinition<?>> result = new ArrayList<>();
        namesByOrder.forEach((variableNumber, variableName) -> {
            String typeStr = getMandatoryVariableValue(reportName, variableNumber, variableName, VARIABLE_TYPE_FORMAT, reportProperties);
            String request = getMandatoryVariableValue(reportName, variableNumber, variableName, VARIABLE_REQUEST_FORMAT, reportProperties);
            String comment = getOptionalRepeatVariableValue(reportName, variableNumber, variableName, VARIABLE_COMMENT_FORMAT, reportProperties).orElse(null);
            Class<?> type = Objects2.forName(typeStr);
            ReportVariableDefinition<?> variable = new ReportVariableDefinition<>(variableName, type, request, comment);
            log.debug(String.format("Detects on report %s variable %d : %s", reportName, variableNumber, variable));
            result.add(variable);
        });
        return result;
    }

    protected List<ReportRepeatVariableDefinition<?>> getRepeatVariables(String reportName, Map<String, String> reportProperties, Set<String> variablesNames) {

        Map<Integer, String> namesByOrder = filterProperties(reportProperties, REPEAT_VARIABLE_PREFIX, REPEAT_VARIABLE_NAME_PATTERN, (name, map) -> validateRepeatVariableName(reportName, name, map, variablesNames));
        List<ReportRepeatVariableDefinition<?>> result = new ArrayList<>();
        namesByOrder.forEach((variableNumber, variableName) -> {
            String typeStr = getMandatoryRepeatVariableValue(reportName, variableNumber, variableName, REPEAT_VARIABLE_TYPE_FORMAT, reportProperties);
            String request = getMandatoryRepeatVariableValue(reportName, variableNumber, variableName, REPEAT_VARIABLE_REQUEST_FORMAT, reportProperties);
            String comment = getOptionalRepeatVariableValue(reportName, variableNumber, variableName, REPEAT_VARIABLE_COMMENT_FORMAT, reportProperties).orElse(null);
            Optional<String> addNullStr = getOptionalRepeatVariableValue(reportName, variableNumber, variableName, REPEAT_VARIABLE_ADD_NULL_FORMAT, reportProperties);
            Class<?> type = Objects2.forName(typeStr);
            boolean addNull = addNullStr.map(Boolean::parseBoolean).orElse(false);
            ReportRepeatVariableDefinition<?> variable = new ReportRepeatVariableDefinition<>(variableName, type, request, comment, addNull);
            log.debug(String.format("Detects on report %s repeat variable %d : %s", reportName, variableNumber, variable));
            result.add(variable);
        });
        return result;
    }

    protected ReportRequestDefinition getRequest(String reportName, int key, String location, Map<String, String> reportProperties) {
        String[] coords = location.split("\\s*,\\s*");
        if (coords.length != 2) {
            throw new IllegalArgumentException(String.format("La définition des coordonées de la requete %d (rapport %s) doit etre de type 'X,Y' mais est : %s", key, reportName, location));
        }
        int x = Integer.parseInt(coords[0]);
        int y = Integer.parseInt(coords[1]);

        String layout = getMandatoryRequestValue(reportName, key, REQUEST_LAYOUT_FORMAT, reportProperties);
        RequestLayout realLayout = RequestLayout.valueOf(layout);

        String request = getMandatoryRequestValue(reportName, key, REQUEST_REQUEST_FORMAT, reportProperties);
        String comment = getOptionalRequestValue(reportName, key, REQUEST_COMMENT_FORMAT, reportProperties).orElse(null);
        Optional<String> repeatName = getOptionalRequestValue(reportName, key, REQUEST_REPEAT_NAME_FORMAT, reportProperties);
        RequestRepeatVariableDefinition repeat = null;
        if (repeatName.isPresent()) {
            String repeatLayout = getMandatoryRequestValue(reportName, key, REQUEST_REPEAT_LAYOUT_FORMAT, reportProperties);
            RequestLayout realRepeatLayout = RequestLayout.valueOf(repeatLayout);
            repeat = new RequestRepeatVariableDefinition(repeatName.get(), realRepeatLayout);
        }
        return new ReportRequestDefinition(realLayout, x, y, request, repeat, comment);
    }

    protected Map<Integer, String> filterProperties(Map<String, String> reportProperties, String prefixKey, Pattern keyMatcher, BiConsumer<String, Map<Integer, String>> validator) {

        Map<Integer, String> namesByOrder = new TreeMap<>();
        Set<String> propertiesToRemove = new TreeSet<>();
        reportProperties.forEach((key, name) -> {
            if (!key.startsWith(prefixKey)) {
                // prefix does not match
                return;
            }
            Matcher matcher = keyMatcher.matcher(key);
            if (!matcher.matches()) {
                // name property does not match
                return;
            }
            Integer number = Integer.parseInt(matcher.group(1));
            validator.accept(name, namesByOrder);
            namesByOrder.put(number, name);
            propertiesToRemove.add(key);
        });
        propertiesToRemove.forEach(reportProperties::remove);
        return namesByOrder;
    }

    protected Map<String, String> filterI18nProperties(Map<String, String> reportProperties) {

        Map<String, String> labelsByName = new TreeMap<>();
        Set<String> propertiesToRemove = new TreeSet<>();
        reportProperties.forEach((key, label) -> {
            if (!key.startsWith(I18n_PREFIX)) {
                // prefix does not match
                return;
            }
            Matcher matcher = I18N_PATTERN.matcher(key);
            if (!matcher.matches()) {
                // name property does not match
                return;
            }
            String i18nName = matcher.group(1);
            labelsByName.put(i18nName, label);
            propertiesToRemove.add(key);
        });
        propertiesToRemove.forEach(reportProperties::remove);
        return labelsByName;
    }

    protected ReportDefinition buildReport(String reportName, Map<String, String> reportProperties) {

        log.debug(String.format("Will build report [%s] with %d properties (%s).", reportName, reportProperties.size(), reportProperties));

        String modelType = getMandatoryReportValue(reportName, REPORT_MODEL_TYPE, reportProperties).toUpperCase();
        ReportI18nStringDefinition name = getI18nMandatoryReportValue(reportName, REPORT_NAME, reportProperties);
        ReportI18nStringDefinition description = getI18nMandatoryReportValue(reportName, REPORT_DESCRIPTION, reportProperties);
        ReportI18nStringArrayDefinition rowHeaders = loadHeaders(reportName, REPORT_ROWS, reportProperties);
        ReportI18nStringArrayDefinition columnHeaders = loadHeaders(reportName, REPORT_COLUMNS, reportProperties);
        List<ReportVariableDefinition<?>> variables = getVariables(reportName, reportProperties);
        Set<String> variablesNames = variables.stream().map(ReportVariableDefinition::getName).collect(Collectors.toSet());
        List<ReportRepeatVariableDefinition<?>> repeatVariables = getRepeatVariables(reportName, reportProperties, variablesNames);
        ReportRequestDefinition[] requests = getRequests(reportName, reportProperties);
        List<ReportOperationDefinition> operations = getOperations(reportName, reportProperties);
        List<ReportI18nNamedStringDefinition> i18n = getI18ns(reportName, reportProperties);
        List<ColumnRendererDefinition> columnRenders = getColumnRenderers(reportName, reportProperties);
        if (!reportProperties.isEmpty()) {
            // if still some properties not used, build failed
            throw new IllegalStateException(String.format("Report [%s] definition has some unknown properties we can not treat:\n%s", reportName, reportProperties));
        }
        return new ReportDefinition(modelType,
                                    reportName,
                                    name,
                                    description,
                                    rowHeaders,
                                    columnHeaders,
                                    operations.toArray(new ReportOperationDefinition[0]),
                                    variables.toArray(new ReportVariableDefinition[0]),
                                    repeatVariables.toArray(new ReportRepeatVariableDefinition[0]),
                                    i18n.isEmpty() ? null : new ReportI18nDefinition(i18n),
                                    columnRenders.toArray(new ColumnRendererDefinition[0]),
                                    requests);
    }

    protected Properties loadProperties(URL location) {
        Properties properties = new SortedProperties();
        try (Reader in = new BufferedReader(new InputStreamReader(location.openStream(), StandardCharsets.UTF_8))) {
            properties.load(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }
}
