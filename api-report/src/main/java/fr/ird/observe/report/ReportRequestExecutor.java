package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ColumnRendererDefinition;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.definition.ReportOperationDefinition;
import fr.ird.observe.report.definition.ReportRequestDefinition;
import fr.ird.observe.report.definition.RequestLayout;
import fr.ird.observe.report.definition.RequestRepeatVariableDefinition;
import fr.ird.observe.report.operations.ExecuteRequests;
import io.ultreia.java4all.util.TimeLog;
import io.ultreia.java4all.util.matrix.DataMatrix;
import io.ultreia.java4all.util.matrix.DataMatrixDimension;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * To execute a request.
 * <p>
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
public interface ReportRequestExecutor {
    Logger log = LogManager.getLogger(ReportRequestExecutor.class);
    TimeLog TIME_LOG = new TimeLog(ReportRequestExecutor.class, 500, 1000);

    /**
     * Check if a variable name is used in given {@code request}.
     *
     * @param request      the request to scan
     * @param variableName the variable name to check
     * @return {@code true} if variable name found in request, {@code false} otherwise
     */
    static boolean isVariableUsed(String request, String variableName) {
        return request.contains(":" + variableName + " ") || request.contains(":" + variableName + ",") || request.endsWith(":" + variableName);
    }

    /**
     * To extract default parameters to use in the given report.
     * <p>
     * Here we only inject:
     * <ul>
     *     <li>{@code tripId} (they are always used in any report requests)</li>
     *     <li>Any selected value of any variable of the report</li>
     * </ul>
     *
     * @param report the report to inspect
     * @param tripId data ids
     * @return the dictionary of parameters
     */
    static Map<String, Object> extractParams(Report report, Set<String> tripId) {
        Map<String, Object> params = new TreeMap<>();
        params.put(ReportRequestDefinition.TRIP_ID_VARIABLE, tripId);
        for (ReportVariable<?> variable : report.getVariables()) {
            String name = variable.getName();
            Object value = variable.getSelectedValue();
            if (value instanceof ReportVariableValueAware) {
                value = ((ReportVariableValueAware<?>) value).getValue();
            }
            params.put(name, value);
        }
        return params;
    }

    ReportRequestExecutorI18nContext i18nContext();

    <R> List<R> executeRequest(String request, Map<String, Object> params);

    <V> Set<V> transformVariableValues(List<?> universe, Class<V> type);

    default Report populateVariables(Report report, Set<String> tripIds) {
        long t00 = TimeLog.getTime();
        Map<String, Set<String>> reportVariableDependencies = report.getVariableDependencies();
        Map<String, ReportVariable<?>> variableByName = report.getVariableByName();
        for (ReportVariable<?> variable : report.getVariables()) {
            String variableName = variable.getName();
            if (variable.isValuesLoaded()) {
                // variable already loaded
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Do not populate variable %s, variable already loaded.", variableName));
                }
                continue;
            }
            Map<String, Object> vars = new TreeMap<>();
            vars.put(ReportRequestDefinition.TRIP_ID_VARIABLE, tripIds);
            Set<String> variableDependenciesNames = reportVariableDependencies.get(variableName);
            if (variableDependenciesNames != null) {
                boolean missingSomeDependencies = false;
                for (String variableDependencyName : variableDependenciesNames) {
                    ReportVariable<?> variableDependency = variableByName.get(variableDependencyName);
                    if (!variableDependency.isValuesLoaded()) {
                        // this dependency variable is not loaded
                        missingSomeDependencies = true;
                        log.info(String.format("Do not populate variable %s, dependency variable %s not loaded.", variableName, variableDependencyName));
                        break;
                    }
                    Object variableDependencySelectedValue = variableDependency.getSelectedValue();
                    if (variableDependencySelectedValue == null) {
                        // this dependency variable has no selected value, can use it
                        missingSomeDependencies = true;
                        log.info(String.format("Do not populate variable %s, dependency variable %s not selected.", variableName, variableDependencyName));
                        break;
                    }
                    // add this variable value
                    vars.put(variableDependencyName, variableDependencySelectedValue);
                }
                if (missingSomeDependencies) {
                    // can not populate this variable, one of his dependency is still not loaded
                    log.info(String.format("Do not populate variable %s, one of his dependency can not be used.", variableName));
                    continue;
                }
            }
            long t0 = TimeLog.getTime();
            log.info(String.format("Will populate variable %s...", variableName));
            populateVariable(report, variable, vars);
            TIME_LOG.log(t0, "populate variable", variableName);
        }
        TIME_LOG.log(t00, "populate variables");
        return report;
    }

    default ReportColumnRenderersParameters initColumnRendererParameters(ReportDefinition definition) {
        long t00 = TimeLog.getTime();
        ColumnRendererDefinition[] columnRenderers = definition.getColumnRenderers();
        List<ColumnRendererParameters> parameters = new ArrayList<>(columnRenderers.length);
        for (ColumnRendererDefinition columnRendererDefinition : columnRenderers) {
            parameters.add(ColumnRendererConsumers.createParameters(this, columnRendererDefinition));
        }
        TIME_LOG.log(t00, "initColumnRendererParameters", definition.getName());
        return new ReportColumnRenderersParameters(parameters);
    }

    default <V> void populateVariable(Report report, ReportVariable<V> variable, Map<String, Object> vars) {
        String hql = i18nContext().translateRequest(report.definition(), variable);
        Class<V> type = variable.getType();
        List<?> universe = executeRequest(hql, vars);
        Set<V> values = transformVariableValues(universe, type);
        variable.setValues(values);
    }

    default DataMatrix executeReport(Report report, Set<String> tripId) {
        if (report == null) {
            // pas de report sélectionné, donc pas de résultat
            return null;
        }
        if (!report.canExecute()) {
            // la requête n'est pas exécutable
            return null;
        }
        // remplissage des variables de répétition si nécessaire
        populateRepeatVariables(report, tripId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Build result for report [%s] on %s", report.getName(), tripId));
        }
        int rows = report.getRows();
        int columns = report.getColumns();
        if (log.isDebugEnabled()) {
            log.debug(String.format("Dimension : [%d,%d]", rows, columns));
        }
        // lancement de la première opération et la matrice de résultat
        DataMatrix result = ReportOperationConsumers.executeReportOperation(this, ExecuteRequests.OPERATION, report, tripId, new DataMatrix());
        // lancement des opérations supplémentaires
        for (ReportOperationDefinition operation : report.getOperations()) {
            DataMatrix tmp = result;
            if (log.isDebugEnabled()) {
                log.debug(String.format("Apply operation %s to %s", operation, tmp));
            }
            result = ReportOperationConsumers.executeReportOperation(this, operation, report, tripId, tmp);
        }
        if (log.isDebugEnabled()) {
            log.debug(String.format("Final result : %s", result));
        }
        return result;
    }

    default DataMatrix executeReportRequest(ReportRequestDefinition request, Report report, Set<String> dataId) {
        long t0 = TimeLog.getTime();
        try {
            Map<String, Object> params = extractParams(report, dataId);
            ReportDefinition reportDefinition = report.definition();
            String hql = i18nContext().translateRequest(reportDefinition, request);
            for (ReportRepeatVariable<?> repeatVariable : report.getRepeatVariables()) {
                if (isVariableUsed(hql, repeatVariable.getName())) {
                    // add this repeat variable values to parameters
                    params.put(repeatVariable.getName(), repeatVariable.computeIndexList());
                }
            }
            return executeReportRequest(reportDefinition, request, params);
        } finally {
            TIME_LOG.log(t0, "executeReportRequest", request.toString());
        }
    }

    default DataMatrix executeReportRequest(ReportRequestDefinition request, Report report, Set<String> dataId, ReportRepeatVariable<?> repeatValues) {
        long t0 = TimeLog.getTime();
        try {
            Map<String, Object> params = extractParams(report, dataId);
            DataMatrix result = new DataMatrix();
            RequestRepeatVariableDefinition requestRepeatVariable = request.getRepeat();
            RequestLayout requestRepeatVariableLayout = requestRepeatVariable.getLayout();
            String repeatVariableName = requestRepeatVariable.getVariableName();
            ReportDefinition reportDefinition = report.definition();
            for (Object repeatValue : repeatValues.getValues()) {
                params.put(repeatVariableName, repeatValue);
                DataMatrix tmp = executeReportRequest(reportDefinition, request, params);
                switch (requestRepeatVariableLayout) {
                    case row:
                        // add current result on the same row (at the right of the current result)
                        tmp.setX(result.getX() + result.getWidth());
                        tmp.setY(result.getY());
                        break;
                    case column:
                        // add current result on the same column (at the bottom of the current result)
                        tmp.setX(result.getX());
                        tmp.setY(result.getY() + result.getHeight());
                        break;
                }
                result = DataMatrix.merge(result, tmp);
            }
            result.setX(request.getX());
            result.setY(request.getY());
            if (log.isDebugEnabled()) {
                log.debug(String.format("Result location  : %s", result.getLocation()));
                log.debug(String.format("Result data      :\n%s", result.getClipboardContent(true, true, false, '\t')));
            }
            return result;
        } finally {
            TIME_LOG.log(t0, "executeReportRequest", request.toString());
        }
    }

    default DataMatrix executeReportRequest(ReportDefinition reportDefinition, ReportRequestDefinition reportRequestDefinition, Map<String, Object> params) {
        // création des paramètres : couples (key, value)
        String request = i18nContext().translateRequest(reportDefinition, reportRequestDefinition);
        if (log.isDebugEnabled()) {
            log.debug("Request          : {}", request);
            log.debug("Available params : {}", params.keySet());
            log.debug("Params to use    : {}", params);
        }
        // lancement de la requête
        List<?> list = executeRequest(request, params);
        if (log.isDebugEnabled()) {
            log.debug("Result size      : {}", list.size());
        }
        // determination des dimensions du résultat
        DataMatrixDimension dimension = reportRequestDefinition.computeDimension(list);
        if (log.isDebugEnabled()) {
            log.debug("Result dimension : {}", dimension);
        }
        // construction du résultat
        DataMatrix result = reportRequestDefinition.computeResult(dimension, list, () -> new DataMatrix(cellValueFormatter()));
        if (log.isDebugEnabled()) {
            log.debug("Result location  : {}", result.getLocation());
            log.debug("Result data      :\n{}", result.getClipboardContent(true, true, false, '\t'));
        }
        return result;
    }

    default void populateRepeatVariables(Report report, Set<String> tripId) {
        long t0 = TimeLog.getTime();
        Map<String, Object> vars = extractParams(report, tripId);
        for (ReportRepeatVariable<?> variable : report.getRepeatVariables()) {
            populateRepeatVariable(report, new TreeMap<>(vars), variable);
        }
        TIME_LOG.log(t0, "populate repeat variables");
    }

    default <R> void populateRepeatVariable(Report report, Map<String, Object> vars, ReportRepeatVariable<R> variable) {
        long t0 = TimeLog.getTime();
        String hql = i18nContext().translateRequest(report.definition(), variable);
        for (ReportRepeatVariable<?> repeatVariable : report.getRepeatVariables()) {
            if (variable.equals(repeatVariable)) {
                continue;
            }
            if (isVariableUsed(hql, repeatVariable.getName())) {
                // add this repeat variable values to parameters
                vars.put(repeatVariable.getName(), repeatVariable.computeIndexList());
            }
        }
        List<R> universe = executeRequest(hql, vars);
        Set<R> values = new LinkedHashSet<>(universe);
        variable.setValues(values);
        TIME_LOG.log(t0, "populate repeat variable", variable.getName());
    }

    Function<Object, String> cellValueFormatter();

    String getReferentialLabel(String id);
}
