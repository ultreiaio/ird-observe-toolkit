package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.definition.i18n.ReportI18nDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nStringArrayDefinition;
import fr.ird.observe.report.definition.i18n.ReportI18nStringDefinition;
import io.ultreia.java4all.util.json.JsonAware;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Definition of a report.
 * <p>
 * This object is immutable and is loaded once for all.
 * <p>
 * Created on 19/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public final class ReportDefinition implements Serializable, JsonAware {
    private static final long serialVersionUID = 1L;
    /**
     * Business model on which the report can be apply.
     */
    private final String modelType;
    /**
     * Id of report.
     */
    private final String id;
    /**
     * Short label of report.
     */
    private final ReportI18nStringDefinition name;
    /**
     * Long label of report.
     */
    private final ReportI18nStringDefinition description;
    /**
     * Labels used for column's header report (can be empty if computed by report requests or operations).
     */
    private final ReportI18nStringArrayDefinition columnHeaders;
    /**
     * Labels used for row's header report (can be empty if computed by report requests or operations).
     */
    private final ReportI18nStringArrayDefinition rowHeaders;
    /**
     * All requests to perform for this report.
     */
    private final ReportRequestDefinition[] requests;
    /**
     * All operations to perform after all requests were performed.
     */
    private final ReportOperationDefinition[] operations;
    /**
     * Optional column renderers.
     */
    private final ColumnRendererDefinition[] columnRenderers;
    /**
     * All variables of this report.
     */
    private final ReportVariableDefinition<?>[] variables;
    /**
     * All repeat variables of this report.
     */
    private final ReportRepeatVariableDefinition<?>[] repeatVariables;
    /**
     * Optional i18n named item used to translate report data at runtime/
     */
    private final ReportI18nDefinition i18n;
    /**
     * Cache of dependencies on variables (key is variable name, value is the required variable names to be able to populate the variable).
     */
    private transient Map<String, Set<String>> variableDependencies;

    public ReportDefinition(String modelType,
                            String id,
                            ReportI18nStringDefinition name,
                            ReportI18nStringDefinition description,
                            ReportI18nStringArrayDefinition rowHeaders,
                            ReportI18nStringArrayDefinition columnHeaders,
                            ReportOperationDefinition[] operations,
                            ReportVariableDefinition<?>[] variables,
                            ReportRepeatVariableDefinition<?>[] repeatVariables,
                            ReportI18nDefinition i18n,
                            ColumnRendererDefinition[] columnRenderers,
                            ReportRequestDefinition... requests) {
        this.modelType = Objects.requireNonNull(modelType);
        this.id = Objects.requireNonNull(id);
        this.name = Objects.requireNonNull(name);
        this.description = Objects.requireNonNull(description);
        this.rowHeaders = rowHeaders;
        this.columnHeaders = columnHeaders;
        this.columnRenderers = columnRenderers;
        this.i18n = i18n;
        this.requests = requests;
        this.operations = operations;
        this.variables = variables;
        this.repeatVariables = repeatVariables;
    }

    public String getModelType() {
        return modelType;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name.getLabel();
    }

    public String getName(ReferentialLocale referentialLocale) {
        return name.apply(referentialLocale);
    }

    public String getDescription() {
        return description.getLabel();
    }

    public String getDescription(ReferentialLocale referentialLocale) {
        return description.apply(referentialLocale);
    }

    public Optional<ReportI18nDefinition> i18n() {
        return Optional.ofNullable(i18n);
    }

    public String[] getColumnHeaders() {
        return columnHeaders == null ? null : columnHeaders.getLabel();
    }

    public String[] getColumnHeaders(ReferentialLocale referentialLocale) {
        return columnHeaders == null ? null : columnHeaders.apply(referentialLocale);
    }

    public String[] getRowHeaders() {
        return rowHeaders == null ? null : rowHeaders.getLabel();
    }

    public String[] getRowHeaders(ReferentialLocale referentialLocale) {
        return rowHeaders == null ? null : rowHeaders.apply(referentialLocale);
    }

    public ReportRequestDefinition[] getRequests() {
        return requests;
    }

    public Map<String, Set<String>> getVariableDependencies() {
        if (variableDependencies == null) {
            this.variableDependencies = computeVariableDependencies();
        }
        return variableDependencies;
    }

    public ReportOperationDefinition[] getOperations() {
        return operations;
    }

    public ColumnRendererDefinition[] getColumnRenderers() {
        return columnRenderers;
    }

    public ReportVariableDefinition<?>[] getVariables() {
        return variables;
    }

    public ReportRepeatVariableDefinition<?>[] getRepeatVariables() {
        return repeatVariables;
    }

    public int getRows() {
        return rowHeaders == null ? -1 : rowHeaders.getLabel().length;
    }

    public int getColumns() {
        return columnHeaders == null ? -1 : columnHeaders.getLabel().length;
    }

    public Report toReport() {
        return new Report(this);
    }

    @Override
    public String toString() {
        return name.getLabel();
    }

    private Map<String, Set<String>> computeVariableDependencies() {
        Map<String, ReportVariableDefinition<?>> variableByNames = Collections.unmodifiableMap(Arrays.stream(variables).collect(Collectors.toMap(ReportVariableDefinition::getName, Function.identity())));
        Set<String> variableNames = variableByNames.keySet();
        Map<String, Set<String>> variableDependencies = new TreeMap<>();
        for (Map.Entry<String, ReportVariableDefinition<?>> entry : variableByNames.entrySet()) {
            String currentVariableName = entry.getKey();
            ReportVariableDefinition<?> variable = entry.getValue();
            String request = variable.getRequest();
            for (String variableName : variableNames) {
                if (currentVariableName.equals(variableName)) {
                    continue;
                }
                if (ReportRequestExecutor.isVariableUsed(request, variableName)) {
                    // this variable depends on variableName
                    variableDependencies.computeIfAbsent(currentVariableName, e -> new TreeSet<>()).add(variableName);
                }
            }
        }
        return Collections.unmodifiableMap(variableDependencies);
    }

}
