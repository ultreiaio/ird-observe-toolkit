package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.ReportVariableSupport;

import java.util.Objects;

/**
 * Definition of  a report variable.
 * <p>
 * Created on 19/12/2022.
 *
 * @param <V> type of data values of this variable.
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.23
 */
public abstract class ReportVariableDefinitionSupport<V> implements WithRequest {
    /**
     * Name of the variable.
     */
    private final String name;
    /**
     * Type of data of this variable.
     */
    private final Class<V> type;
    /**
     * The hql request to get data.
     */
    private final String request;
    /**
     * Optional comment.
     */
    private final String comment;

    public ReportVariableDefinitionSupport(String name, Class<V> type, String request, String comment) {
        this.name = Objects.requireNonNull(name);
        this.type = Objects.requireNonNull(type);
        this.request = Objects.requireNonNull(request);
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public Class<V> getType() {
        return type;
    }


    @Override
    public String getRequest() {
        return request;
    }

    public String getComment() {
        return comment;
    }

    public boolean withComment() {
        return comment != null;
    }

    public abstract ReportVariableSupport<?, ?> toVariable();

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !getClass().equals(o.getClass())) return false;
        ReportVariableDefinitionSupport<?> that = (ReportVariableDefinitionSupport<?>) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(name);
    }
}
