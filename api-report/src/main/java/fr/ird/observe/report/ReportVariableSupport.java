package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportVariableDefinitionSupport;
import fr.ird.observe.report.definition.WithRequest;

import java.util.Objects;
import java.util.Set;

/**
 * Report variable support to share common code with both implementations.
 * <p>
 * Created on 14/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ReportVariable
 * @see ReportRepeatVariable
 * @since 9.0.22
 */
public abstract class ReportVariableSupport<V, D extends ReportVariableDefinitionSupport<V>> implements WithRequest {

    public final static String PROPERTY_DEFINITION = "definition";
    public final static String PROPERTY_VALUES = "values";

    /**
     * Definition of this variable.
     */
    private final D definition;
    /**
     * Values loaded for this variable.
     */
    private Set<V> values;

    protected ReportVariableSupport(D definition) {
        this.definition = definition;
    }

    public final D definition() {
        return definition;
    }

    public final String getName() {
        return definition().getName();
    }

    public final String getComment() {
        return definition().getComment();
    }

    public final Class<V> getType() {
        return definition().getType();
    }

    @Override
    public final String getRequest() {
        return definition().getRequest();
    }

    public final Set<V> getValues() {
        return values;
    }

    public void setValues(Set<V> values) {
        this.values = values;
    }

    public final boolean isValuesLoaded() {
        return values != null && !values.isEmpty();
    }

    public void unload() {
        this.values = null;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !getClass().equals(o.getClass())) return false;
        ReportVariableSupport<?, ?> that = (ReportVariableSupport<?, ?>) o;
        return Objects.equals(definition(), that.definition());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(definition());
    }
}
