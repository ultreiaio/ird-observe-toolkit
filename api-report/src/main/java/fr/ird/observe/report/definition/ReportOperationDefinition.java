package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;

import java.util.Objects;

/**
 * Definition of  a report operation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public final class ReportOperationDefinition implements JsonAware {

    /**
     * Name of the operation, this is the fully qualified name of the ReportOperationConsumer.
     */
    private final String name;
    /**
     * Optional parameters of this operation.
     */
    private final String parameters;

    public static ReportOperationDefinition of(String name) {
        return of(name, "");
    }

    public static ReportOperationDefinition of(String name, String parameters) {
        return new ReportOperationDefinition(name, parameters);
    }

    public ReportOperationDefinition(String name, String parameters) {
        this.name = Objects.requireNonNull(name);
        this.parameters = Objects.requireNonNull(parameters);
    }

    public String getName() {
        return name;
    }

    public String getParameters() {
        return parameters;
    }

    public boolean withParameters() {
        return !parameters.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportOperationDefinition)) return false;
        ReportOperationDefinition that = (ReportOperationDefinition) o;
        return name.equals(that.name) && parameters.equals(that.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parameters);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportOperationDefinition{");
        sb.append("name='").append(name).append('\'');
        if (!parameters.isEmpty()) {
            sb.append(", parameters='").append(parameters).append('\'');
        }
        sb.append('}');
        return sb.toString();
    }

}
