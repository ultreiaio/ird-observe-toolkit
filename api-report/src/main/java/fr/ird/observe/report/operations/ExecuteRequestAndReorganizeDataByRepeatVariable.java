package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To execute a query and then reorganize it using repeat variables position (on x axis and y axis).
 * <p>
 * Created on 15/11/2022.
 * <p>
 * Parameters format is
 * <code>repeatVariableForRow|repeatVariableFormColum|request</code>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
@AutoService(ReportOperationConsumer.class)
public class ExecuteRequestAndReorganizeDataByRepeatVariable implements ReportOperationConsumer {

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        if (parameters.isEmpty()) {
            throw new IllegalStateException(String.format("parameters of operation %s can't not be null (format: repeatVariableForRow|repeatVariableFormColum|request)", getClass().getName()));
        }
        String[] split = parameters.split("\\|");
        if (split.length != 3) {
            throw new IllegalStateException(String.format("parameters of operation %s (value %s) has bad format (format: repeatVariableForRow|repeatVariableFormColum|request)", getClass().getName(), parameters));
        }
        String repeatVariableForColumName = split[1].trim();
        ReportRepeatVariable<?> repeatVariableForColumn = report.getRepeatVariable(repeatVariableForColumName);
        if (!repeatVariableForColumn.isValuesLoaded()) {
            return incoming;
        }
        String repeatVariableForRowName = split[0].trim();
        ReportRepeatVariable<?> repeatVariableForRow = report.getRepeatVariable(repeatVariableForRowName);
        if (!repeatVariableForRow.isValuesLoaded()) {
            return incoming;
        }
        String request = split[2].trim();

        List<String> columnIdList = repeatVariableForColumn.computeIndexList();
        List<String> rowIdList = repeatVariableForRow.computeIndexList();

        Map<String, Object> queryParameters = computeQueryParameters(report,
                                                                     tripId,
                                                                     request,
                                                                     repeatVariableForRowName,
                                                                     rowIdList,
                                                                     repeatVariableForColumName,
                                                                     columnIdList);

        DataMatrix result = createTmpMatrix(incoming.getWidth(), 0, columnIdList.size(), rowIdList.size()).setValue(0);

        List<Object[]> rows = requestExecutor.executeRequest(request, queryParameters);
        reorganize(result, rows, rowIdList, columnIdList);
        return DataMatrix.merge(incoming, result);
    }

    private Map<String, Object> computeQueryParameters(Report report,
                                                       Set<String> tripId,
                                                       String request,
                                                       String repeatVariableForRow,
                                                       List<String> rowIdList,
                                                       String repeatVariableFormColum,
                                                       List<String> columnIdList) {
        Map<String, Object> queryParameters = ReportRequestExecutor.extractParams(report, tripId);

        if (ReportRequestExecutor.isVariableUsed(request, repeatVariableForRow)) {
            // add this variable to queryParameters
            queryParameters.put(repeatVariableForRow, rowIdList);
        }
        if (ReportRequestExecutor.isVariableUsed(request, repeatVariableFormColum)) {
            // add this variable to queryParameters
            queryParameters.put(repeatVariableFormColum, columnIdList);
        }
        return queryParameters;
    }

    private void reorganize(DataMatrix result, List<Object[]> rows, List<String> rowIdList, List<String> columnIdList) {
        for (Object[] row : rows) {
            String rowId = String.valueOf(row[0]);
            int rowIndex = rowIdList.indexOf(rowId);
            String columnId = String.valueOf(row[1]);
            int columnIndex = columnIdList.indexOf(columnId);
            Number count = (Number) row[2];
            result.setValue(columnIndex, rowIndex, count);
        }
    }
}
