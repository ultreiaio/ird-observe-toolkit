package fr.ird.observe.report.renderers;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.ColumnRendererConsumer;
import fr.ird.observe.report.ColumnRendererParameters;
import io.ultreia.java4all.util.SingletonSupplier;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.Component;
import java.util.Objects;

import static fr.ird.observe.report.renderers.HighlightIfNumericalValueIsPositive.Parameters;

/**
 * Created at 29/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@SuppressWarnings("rawtypes")
@AutoService(ColumnRendererConsumer.class)
public class HighlightIfNumericalValueIsPositive implements ColumnRendererConsumer<Parameters> {

    private final static SingletonSupplier<String> HTML_FUNCTION = ColumnRendererConsumer.htmlFunctions(HighlightIfNumericalValueIsPositive.class);

    public int parametersCount() {
        return 3;
    }

    public String parametersSyntax() {
        return "column|warningThreshHold|errorThreshHold";
    }

    @Override
    public Parameters parseParameters(String parameters) {
        String[] split = pareParametersSyntax(parameters);
        int column = Integer.parseInt(split[0]);
        double warningThreshHold = Double.parseDouble(split[1]);
        double errorThreshHold = Double.parseDouble(split[2]);
        return new Parameters(column, warningThreshHold, errorThreshHold);
    }


    @Override
    public void consumeSwing(Parameters parameters, JXTable table) {
        ColumnRendererConsumer.addHighLighters(table,
                                               new ParametersHighlightPredicate(parameters, null),
                                               new ParametersHighlightPredicate(parameters, true),
                                               new ParametersHighlightPredicate(parameters, false));
    }

    @Override
    public String htmlFunctions() {
        return HTML_FUNCTION.get();
    }

    @Override
    public String htmlInitCode(Parameters parameters) {
        return String.format("init%s(%d, %f, %f, result, data);\n", parameters.name(), parameters.getColumn(), parameters.getWarningThreshHold(), parameters.getErrorThreshHold());
    }

    public static final class Parameters implements ColumnRendererParameters {
        private final int column;
        private final double warningThreshHold;
        private final double errorThreshHold;

        Parameters(int column, double warningThreshHold, double errorThreshHold) {
            this.column = column;
            this.warningThreshHold = warningThreshHold;
            this.errorThreshHold = errorThreshHold;
        }

        @Override
        public String name() {
            return HighlightIfNumericalValueIsPositive.class.getSimpleName();
        }

        public int getColumn() {
            return column;
        }


        public double getErrorThreshHold() {
            return errorThreshHold;
        }

        public double getWarningThreshHold() {
            return warningThreshHold;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Parameters)) return false;
            Parameters that = (Parameters) o;
            return getColumn() == that.getColumn() && Double.compare(getWarningThreshHold(), that.getWarningThreshHold()) == 0 && Double.compare(getErrorThreshHold(), that.getErrorThreshHold()) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(getColumn(), getWarningThreshHold(), getErrorThreshHold());
        }
    }

    static class ParametersHighlightPredicate implements HighlightPredicate {
        private final Parameters parameters;
        private final Boolean warning;

        ParametersHighlightPredicate(Parameters parameters, Boolean warning) {
            this.parameters = parameters;
            this.warning = warning;
        }

        @Override
        public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
            int column = adapter.convertColumnIndexToModel(adapter.column);
            if (column != parameters.getColumn()) {
                return false;
            }
            Object value1 = adapter.getValue();
            if (value1 == null || value1.toString().equals("-")) {
                return warning == null;
            }
            if (warning == null) {
                return false;
            }
            double d1 = Double.parseDouble(value1.toString());
            double delta = Math.abs(d1);
            if (delta < parameters.getWarningThreshHold()) {
                return false;
            }
            boolean onError = delta >= parameters.getErrorThreshHold();
            return warning != onError;
        }
    }
}
