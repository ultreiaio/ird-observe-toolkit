package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Basic contract to build report definitions.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public interface ReportDefinitionsBuilder extends AutoCloseable {
    Logger log = LogManager.getLogger(ReportDefinitionsBuilder.class);
    String REPORT_LIST_LOCATION = "/META-INF/report/embedded.list";
    String REPORT_PACKAGE_LOCATION = "META-INF/report/";

    static String reportLocation(String path) {
        return REPORT_PACKAGE_LOCATION + path;
    }

    default List<ReportDefinition> build() throws IOException {
        Set<String> reportNames = detectReportNames();
        log.debug(String.format("Detected report names: %s", reportNames));
        List<ReportDefinition> reports = reportNames.stream().map(reportName -> {
            log.debug(String.format("Build report: %s", reportName));
            return buildReport(reportName);
        }).collect(Collectors.toList());
        log.info(String.format("Found %d definition(s) of report.", reports.size()));
        return reports;
    }

    Set<String> detectReportNames();

    ReportDefinition buildReport(String reportName);

    @Override
    void close();
}
