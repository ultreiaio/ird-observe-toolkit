package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;
import org.jdesktop.swingx.JXTable;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created at 29/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportColumnRenderersParameters implements JsonAware {
    /**
     * All column renderer parameters of this report.
     */
    private final List<ColumnRendererParameters> columnRendererParameters;

    public ReportColumnRenderersParameters(List<ColumnRendererParameters> columnRendererParameters) {
        this.columnRendererParameters = Collections.unmodifiableList(Objects.requireNonNull(columnRendererParameters));
    }

    public List<ColumnRendererParameters> getColumnRendererParameters() {
        return columnRendererParameters;
    }

    public void consumeColumnRenderersSwing(JXTable table) {
        getColumnRendererParameters().forEach(r -> ColumnRendererConsumers.consumeSwing(r, table));
    }
}
