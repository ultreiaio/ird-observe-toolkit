package fr.ird.observe.report.tools;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.report.definition.DefaultReportDefinitionsBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created at 07/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class PersistenceComputeReportListFileRunner implements Runnable {
    private static final Logger log = LogManager.getLogger(PersistenceComputeReportListFileRunner.class);
    private Path importPath;
    private Path reportListPath;

    public Path getImportPath() {
        return importPath;
    }

    public void setImportPath(Path importPath) {
        this.importPath = importPath;
    }

    public Path getReportListPath() {
        return reportListPath;
    }

    public void setReportListPath(Path reportListPath) {
        this.reportListPath = reportListPath;
    }

    @Override
    public void run() {
        Path importPath = getImportPath();
        Set<Path> reportFiles = DefaultReportDefinitionsBuilder.detectReportFiles(importPath);
        Map<String, String> result = new TreeMap<>();
        for (Path path : reportFiles) {
            String reportId = DefaultReportDefinitionsBuilder.fileNameToReportId(path);
            String location = importPath.relativize(path).toString();
            String oldValue = result.put(reportId, location);
            if (oldValue != null) {
                throw new IllegalStateException(String.format("Found a duplicated report %s at: %s", reportId, location));
            }
        }
        log.info("Detected {} report(s).", result.size());
        log.info("Write reports list to {}", getReportListPath());
        ObserveUtil.createDirectories(getReportListPath().getParent());
        try (BufferedWriter writer = Files.newBufferedWriter(getReportListPath())) {
            for (String location : result.values().stream().sorted().collect(Collectors.toList())) {
                writer.write(location);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
