package fr.ird.observe.report.definition.i18n;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created at 05/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportI18nStringArrayDefinition implements ReportI18nObjectDefinition<String[]> {

    /**
     * Default french label.
     */
    private final String[] label;
    /**
     * Optional english label (if null, then will be use the [@link {@link #label} instead).
     */
    private final String[] enLabel;
    /**
     * Optional spanish label (if null, then will be use the [@link {@link #label} instead).
     */
    private final String[] esLabel;

    public ReportI18nStringArrayDefinition(String[] label, String[] enLabel, String[] esLabel) {
        this.label = Objects.requireNonNull(label);
        this.enLabel = enLabel;
        this.esLabel = esLabel;
    }

    @Override
    public String[] getLabel() {
        return label;
    }

    @Override
    public String[] getEnLabel() {
        return enLabel == null ? label : enLabel;
    }

    @Override
    public String[] getEsLabel() {
        return esLabel == null ? label : esLabel;
    }

    @Override
    public String toString() {
        StringJoiner builder = new StringJoiner(", ", ReportI18nStringArrayDefinition.class.getSimpleName() + "[", "]")
                .add("label='" + Arrays.toString(label) + "'");
        if (enLabel != null) {
            builder.add("enLabel='" + Arrays.toString(enLabel) + "'");
        }
        if (esLabel != null) {
            builder.add("enLabel='" + Arrays.toString(esLabel) + "'");
        }
        return builder
                .toString();
    }
}
