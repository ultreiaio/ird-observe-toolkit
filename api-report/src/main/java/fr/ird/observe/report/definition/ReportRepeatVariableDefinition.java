package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.ReportRepeatVariable;

/**
 * Created on 19/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public final class ReportRepeatVariableDefinition<V> extends ReportVariableDefinitionSupport<V> {

    private static final long serialVersionUID = 1L;

    /**
     * Should we add to values the null one? (we can not deal this case sometimes just by a hql request)
     */
    private final boolean addNullValue;

    public ReportRepeatVariableDefinition(String name, Class<V> type, String request, String comment, boolean addNullValue) {
        super(name, type, request, comment);
        this.addNullValue = addNullValue;
    }

    public boolean isAddNullValue() {
        return addNullValue;
    }

    public ReportRepeatVariable<?> toVariable() {
        return new ReportRepeatVariable<>(this);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportRepeatVariableDefinition{");
        sb.append("name='").append(getName()).append('\'');
        sb.append(", type=").append(getType().getName());
        if (isAddNullValue()) {
            sb.append(", addNullValue");
        }
        sb.append(", request='").append(getRequest()).append('\'');
        if (withComment()) {
            sb.append(", comment='").append(getComment()).append('\'');
        }
        sb.append('}');
        return sb.toString();
    }
}
