package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.definition.ReportOperationDefinition;
import fr.ird.observe.report.definition.ReportRequestDefinition;
import fr.ird.observe.report.definition.RequestRepeatVariableDefinition;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
@AutoService(ReportOperationConsumer.class)
public class ExecuteRequests implements ReportOperationConsumer {
    public static final ReportOperationDefinition OPERATION = ReportOperationDefinition.of("ExecuteRequests");

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        ReportRequestDefinition[] requests = report.getRequests();
        DataMatrix[] requestResults = new DataMatrix[requests.length];
        int i = 0;
        for (ReportRequestDefinition request : requests) {
            RequestRepeatVariableDefinition repeatVariable = request.getRepeat();
            DataMatrix result;
            if (repeatVariable == null) {
                // requête simple sans repetition
                result = requestExecutor.executeReportRequest(request, report, tripId);
            } else {
                // on a une requête avec repetition
                String repeatVariableName = repeatVariable.getVariableName();
                ReportRepeatVariable<?> repeat = report.getRepeatVariable(repeatVariableName);
                result = requestExecutor.executeReportRequest(request, report, tripId, repeat);
            }
            requestResults[i++] = result;
        }
        int rows = report.getRows();
        int columns = report.getColumns();
        return DataMatrix.merge(rows, columns, requestResults);
    }

}
