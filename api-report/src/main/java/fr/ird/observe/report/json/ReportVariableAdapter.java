package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import fr.ird.observe.report.ReportVariable;
import fr.ird.observe.report.definition.ReportVariableDefinition;
import io.ultreia.java4all.util.json.JsonAdapter;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(JsonAdapter.class)
public class ReportVariableAdapter<V> extends ReportVariableSupportAdapter<V, ReportVariableDefinition<V>, ReportVariable<V>> {

    @Override
    public Class<?> type() {
        return ReportVariable.class;
    }

    @Override
    protected void deserialize(JsonDeserializationContext context, JsonObject jsonObject, ReportVariable<V> variable) {
        super.deserialize(context, jsonObject, variable);
        deserializeSelectedValue(context, jsonObject, variable);
    }

    @Override
    public void serialize(ReportVariable<V> src, JsonSerializationContext context, JsonObject jsonObject) {
        super.serialize(src, context, jsonObject);
        if (src.getSelectedValue() != null) {
            jsonObject.add(ReportVariable.PROPERTY_SELECTED_VALUE, context.serialize(src.getSelectedValue()));
        }
    }

    protected void deserializeSelectedValue(JsonDeserializationContext context, JsonObject jsonObject, ReportVariable<V> variable) {
        JsonElement json = jsonObject.get(ReportVariable.PROPERTY_SELECTED_VALUE);
        if (json != null) {
            V selectedValue = deserializeValue(context, variable.getType(), json);
            variable.setSelectedValue(selectedValue);
        }
    }
}
