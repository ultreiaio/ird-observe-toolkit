package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.util.SortedProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created at 07/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class DefaultByClassPathReportDefinitionsBuilder extends ReportDefinitionsBuilderPropertiesSupport {
    private static final Logger log = LogManager.getLogger(DefaultByClassPathReportDefinitionsBuilder.class);
    /**
     * The report list location.
     */
    private final URL location;
    /**
     * Cache of report location indexed by their reportId.
     */
    private Properties reportsList;

    private DefaultByClassPathReportDefinitionsBuilder(URL location) {
        this.location = Objects.requireNonNull(location);
    }

    public static List<ReportDefinition> build(URL location) throws IOException {
        try (ReportDefinitionsBuilder builder = new DefaultByClassPathReportDefinitionsBuilder(location)) {
            return builder.build();
        }
    }

    @Override
    public Set<String> detectReportNames() {
        Properties reportsList = getReportsList();
        Set<String> reportNames = new TreeSet<>();
        Enumeration<Object> keys = reportsList.keys();
        while (keys.hasMoreElements()) {
            String reportName = (String) keys.nextElement();
            log.debug(String.format("Discover a new report name: %s", reportName));
            boolean add = reportNames.add(reportName);
            if (!add) {
                throw new IllegalStateException("Found duplicated report name: " + reportName);
            }
        }
        return reportNames;
    }

    @Override
    public ReportDefinition buildReport(String reportName) {
        String reportLocation = ReportDefinitionsBuilder.reportLocation(getReportsList().getProperty(reportName));
        URL url = getClass().getClassLoader().getResource(reportLocation);
        Properties properties = loadProperties(Objects.requireNonNull(url, String.format("Report %s location %s not found", reportName, reportLocation)));
        Map<String, String> reportProperties = new TreeMap<>();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            String key = (String) entry.getKey();
            String definition = (String) entry.getValue();
            reportProperties.put(key, definition);
        }
        ReportDefinition result = buildReport(reportName, reportProperties);
        if (!reportProperties.isEmpty()) {
            // if still some properties not used, build failed
            throw new IllegalStateException(String.format("There is some unknown properties we can not treat:\n%s", properties));
        }
        return result;
    }

    @Override
    public void close() {
        if (reportsList != null) {
            reportsList.clear();
            reportsList = null;
        }
    }

    public Properties getReportsList() {
        if (reportsList == null) {
            reportsList = new SortedProperties();
            for (String location : ObserveUtil.loadResourceContent(location).split("\\s*\\n\\s*")) {
                int i = location.lastIndexOf("/");
                String reportId = DefaultReportDefinitionsBuilder.fileNameToReportId(location.substring(i+1));
                reportsList.setProperty(reportId, location);
            }
        }
        return reportsList;
    }
}
