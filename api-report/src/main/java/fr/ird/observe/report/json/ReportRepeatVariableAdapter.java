package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.definition.ReportRepeatVariableDefinition;
import io.ultreia.java4all.util.json.JsonAdapter;

/**
 * Created on 14/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.22
 */
@AutoService(JsonAdapter.class)
public class ReportRepeatVariableAdapter<V> extends ReportVariableSupportAdapter<V, ReportRepeatVariableDefinition<V>, ReportRepeatVariable<V>> {

    @Override
    public Class<?> type() {
        return ReportRepeatVariable.class;
    }

}
