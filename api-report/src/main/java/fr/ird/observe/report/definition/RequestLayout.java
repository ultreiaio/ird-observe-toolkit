package fr.ird.observe.report.definition;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Request layout.
 * <p>
 * Created on 19/12/2022.
 * *
 * * @author Tony Chemit - dev@tchemit.fr
 * * @since 9.0.23
 */
public enum RequestLayout {
    /**
     * lorsque les résultats de la requète sont des lignes du résultat global.
     */
    row,
    /**
     * lorsque les résultats de la requète sont des colonnes du résultat global.
     */
    column
}
