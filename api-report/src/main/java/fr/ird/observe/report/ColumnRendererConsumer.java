package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ObserveUtil;
import io.ultreia.java4all.util.SingletonSupplier;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * To consume a {@link fr.ird.observe.report.definition.ColumnRendererDefinition}.
 * <p>
 * Created at 29/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public interface ColumnRendererConsumer<P extends ColumnRendererParameters> {

    static void addHighLighters(JXTable table, HighlightPredicate nullPredicate, HighlightPredicate warningPredicate, HighlightPredicate errorPredicate) {
        if (nullPredicate != null) {
            table.addHighlighter(new ColorHighlighter(nullPredicate, Color.GRAY, Color.BLACK));
        }
        table.addHighlighter(new ColorHighlighter(errorPredicate, Color.RED, Color.BLACK));
        table.addHighlighter(new ColorHighlighter(warningPredicate, Color.ORANGE, Color.BLACK));
    }

    static SingletonSupplier<String> htmlFunctions(Class<?> type) {
        String resourceName = type.getSimpleName() + ".js";
        URL url = Objects.requireNonNull(type.getResource(resourceName), "Could not find resource: " + resourceName);
        return ObserveUtil.loadResourceContentSupplier(url, ObserveUtil.removeJavaLicense());
    }

    static String loadResourceContent(URL url) {
        try (InputStream in = url.openStream()) {
            String content = new String(in.readAllBytes(), StandardCharsets.UTF_8);
            return content.substring(content.indexOf("*/") + 2).trim();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return name of the column renderer, must be unique
     */
    default String name() {
        return getClass().getSimpleName();
    }

    int parametersCount();

    String parametersSyntax();

    default String[] pareParametersSyntax(String parameters) {
        if (parameters == null || (parameters = parameters.trim()).isEmpty()) {
            throw new IllegalStateException(String.format("parameters of column renderer %s can't not be null (format: %s)", getClass().getName(), parametersSyntax()));
        }
        String[] split = parameters.split("\\s*\\|\\s*");
        if (split.length != parametersCount()) {
            throw new IllegalStateException(String.format("parameters of column renderer %s must have %d parameters (format: %s), but was: %s", getClass().getName(), parametersCount(), parametersSyntax(), parameters));
        }
        return split;
    }

    P parseParameters(String parameters);

    default P createParameters(ReportRequestExecutor requestExecutor, String parameters) {
        return parseParameters(parameters);
    }

    void consumeSwing(P parameters, JXTable table);

    String htmlFunctions();

    String htmlInitCode(P parameters);
}
