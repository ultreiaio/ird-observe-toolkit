package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.dto.referential.WithI18n;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRepeatVariable;
import fr.ird.observe.report.ReportRequestExecutor;
import fr.ird.observe.report.ReportRequestExecutorI18nContext;
import fr.ird.observe.report.definition.ReportDefinition;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * To compute dynamic headers (for report with no column headers defined).
 * <p>
 * You can set as parameters an array of fixed column names + a repeat variable to complete the header.
 * <p>
 * <b>Note:</b> The {@code null} value will be added at the last column (TODO maybe should we be able to customize this in the operation parameters...)
 * <p>
 * Example:
 * <code>Col1,Col2,Col3|repeatVariableName</code>
 * <p>
 * Created on 15/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
@AutoService(ReportOperationConsumer.class)
public class ComputeDynamicHeader implements ReportOperationConsumer {
    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        if (parameters.isEmpty()) {
            throw new IllegalStateException(String.format("parameters of operation %s can't not be null (format: col1,col2,...|repeatVariableName)", getClass().getName()));
        }
        String[] split = parameters.split("\\|");
        if (split.length != 2) {
            throw new IllegalStateException(String.format("parameters of operation %s (value %s) has bad format (format: col1,col2,...|repeatVariableName)", getClass().getName(), parameters));
        }
        String[] fixedColumnNames = split[0].split("\\s*,\\s*");
        String repeatVariableName = split[1].trim();
        ReportRepeatVariable<Object> repeatVariable = report.getRepeatVariable(repeatVariableName);
        Set<?> repeatVariableValues = repeatVariable.getValues();
        ReportRequestExecutorI18nContext i18nContext = requestExecutor.i18nContext();
        ReferentialLocale referentialLocale = i18nContext.getReferentialLocale();
        ReportDefinition reportDefinition = report.definition();
        List<String> columnNames = new ArrayList<>(fixedColumnNames.length);
        for (String fixedColumnName : fixedColumnNames) {
            columnNames.add(i18nContext.translateParameter(reportDefinition, fixedColumnName));
        }
        boolean useNullRepeatVariableValue = false;
        for (Object repeatVariableValue : repeatVariableValues) {
            if (repeatVariableValue == null) {
                useNullRepeatVariableValue = true;
            } else {
                if (repeatVariableValue instanceof WithI18n) {
                    columnNames.add(((WithI18n) repeatVariableValue).getLabel(referentialLocale));
                } else {
                    columnNames.add(repeatVariableValue.toString());
                }
            }
        }
        if (useNullRepeatVariableValue || repeatVariable.isAddNullValue()) {
            columnNames.add(i18nContext.translate(I18n.n("observe.Common.none")));
        }
        int columnIndex = 0;
        DataMatrix tmpMatrix = createTmpMatrix(0, 0, columnNames.size(), 1);
        for (String columnName : columnNames) {
            tmpMatrix.setValue(columnIndex++, 0, columnName);
        }
        incoming.setY(1);
        return DataMatrix.merge(incoming, tmpMatrix);
    }
}
