package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.definition.ReportDefinition;
import fr.ird.observe.report.definition.ReportOperationDefinition;
import fr.ird.observe.report.definition.ReportRepeatVariableDefinition;
import fr.ird.observe.report.definition.ReportRequestDefinition;
import fr.ird.observe.report.definition.ReportVariableDefinition;
import io.ultreia.java4all.util.json.JsonAware;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Report implements Serializable, JsonAware {

    private static final Logger log = LogManager.getLogger(Report.class);
    private static final long serialVersionUID = 1L;
    /**
     * Report definition.
     */
    private final ReportDefinition definition;
    /**
     * All variables of this report.
     */
    private final ReportVariable<?>[] variables;
    /**
     * All repeat variables of this report.
     */
    private final ReportRepeatVariable<?>[] repeatVariables;
    /**
     * Cache of variables of this report indexed by their name.
     */
    private transient Map<String, ReportVariable<?>> variableByName;
    /**
     * Cache of repeat variables of this report indexed by their name.
     */
    private transient Map<String, ReportRepeatVariable<?>> repeatVariableByName;

    public Report(ReportDefinition definition) {
        this.definition = Objects.requireNonNull(definition);
        ReportVariableDefinition<?>[] definitionVariables = definition.getVariables();
        int variablesLength = definitionVariables.length;
        this.variables = new ReportVariable[variablesLength];
        for (int i = 0; i < variablesLength; i++) {
            ReportVariableDefinition<?> definitionVariable = definitionVariables[i];
            ReportVariable<?> variable = definitionVariable.toVariable();
            this.variables[i] = variable;
        }
        ReportRepeatVariableDefinition<?>[] definitionRepeatVariables = definition.getRepeatVariables();
        int repeatVariablesLength = definitionRepeatVariables.length;
        this.repeatVariables = new ReportRepeatVariable[repeatVariablesLength];
        for (int i = 0; i < repeatVariablesLength; i++) {
            ReportRepeatVariableDefinition<?> definitionRepeatVariable = definitionRepeatVariables[i];
            this.repeatVariables[i] = definitionRepeatVariable.toVariable();
        }
    }

    public ReportDefinition definition() {
        return definition;
    }

    public String getModelType() {
        return definition().getModelType();
    }

    public String getId() {
        return definition().getId();
    }

    public String getName() {
        return definition().getName();
    }

    public String getName(ReferentialLocale referentialLocale) {
        return definition().getName(referentialLocale);
    }

    public String getDescription() {
        return definition().getDescription();
    }

    public String getDescription(ReferentialLocale referentialLocale) {
        return definition().getDescription(referentialLocale);
    }

    public int getRows() {
        return definition().getRows();
    }

    public int getColumns() {
        return definition().getColumns();
    }

    public String[] getColumnHeaders() {
        return definition().getColumnHeaders();
    }

    public String[] getColumnHeaders(ReferentialLocale referentialLocale) {
        return definition().getColumnHeaders(referentialLocale);
    }

    public String[] getRowHeaders() {
        return definition().getRowHeaders();
    }

    public String[] getRowHeaders(ReferentialLocale referentialLocale) {
        return definition().getRowHeaders(referentialLocale);
    }

    public ReportRequestDefinition[] getRequests() {
        return definition().getRequests();
    }

    public ReportOperationDefinition[] getOperations() {
        return definition().getOperations();
    }

    public Map<String, Set<String>> getVariableDependencies() {
        return definition().getVariableDependencies();
    }

    public ReportVariable<?>[] getVariables() {
        return variables;
    }

    public Map<String, ReportVariable<?>> getVariableByName() {
        if (variableByName == null) {
            this.variableByName = Collections.unmodifiableMap(Arrays.stream(variables).collect(Collectors.toMap(ReportVariable::getName, Function.identity())));
        }
        return variableByName;
    }

    public ReportVariable<?> getVariable(String variableName) {
        return getVariableByName().get(variableName);
    }

    public ReportRepeatVariable<?>[] getRepeatVariables() {
        return repeatVariables;
    }

    public Map<String, ReportRepeatVariable<?>> getRepeatVariableByName() {
        if (repeatVariableByName == null) {
            this.repeatVariableByName = Collections.unmodifiableMap(Arrays.stream(repeatVariables).collect(Collectors.toMap(ReportRepeatVariable::getName, Function.identity())));
        }
        return repeatVariableByName;
    }

    @SuppressWarnings("unchecked")
    public <V> ReportRepeatVariable<V> getRepeatVariable(String name) {
        return (ReportRepeatVariable<V>) getRepeatVariableByName().get(name);
    }

    @Override
    public String toString() {
        return definition().toString();
    }

    /**
     * @return {@code true} if the report can be executed (means are variables (if any) are loaded and selected, {@code false} otherwise.
     */
    public boolean canExecute() {
        for (ReportVariable<?> variable : getVariables()) {
            if (variable.isValuesLoaded() && variable.isSelectedValueFilled()) {
                continue;
            }
            log.info(String.format("variable %s is not loaded, nor selected value filled", variable.getName()));
            return false;
        }
        return true;
    }

    /**
     * @return {@code true} if we need to init some colum renderer parameters, {@code false} otherwise.
     */
    public boolean needInitColumnRendererParameters() {
        return definition().getColumnRenderers().length > 0;
    }

    /**
     * @return {@code true} if there is still some variables to populate, {@code false} otherwise.
     */
    public boolean needPopulate() {
        for (ReportVariable<?> variable : getVariables()) {
            if (variable.isValuesLoaded()) {
                continue;
            }
            log.info(String.format("variable %s is not loaded, nor selected value filled", variable.getName()));
            return true;
        }
        return false;
    }

    /**
     * @return {@code true} if report contains some variables, {@code false} otherwise.
     */
    public boolean isVariableRequired() {
        return variables.length > 0;
    }

    /**
     * Unload all variables and repeat variables.
     */
    public void unload() {
        for (ReportVariable<?> variable : variables) {
            variable.unload();
        }
        for (ReportRepeatVariable<?> repeatVariable : repeatVariables) {
            repeatVariable.unload();
        }
    }

    /**
     * To unload some variables if they have at least one dependency on a variable that is not loaded, nor selected
     */
    public void unloadVariablesIfNecessary() {
        Map<String, Set<String>> variableDependencyNamesMap = getVariableDependencies();
        Map<String, ReportVariable<?>> variablesMap = getVariableByName();
        for (ReportVariable<?> variable : variables) {
            if (!variable.isValuesLoaded()) {
                // this variable is not loaded, so can not unload it
                continue;
            }
            Set<String> variableDependencyNames = variableDependencyNamesMap.get(variable.getName());
            if (variableDependencyNames == null) {
                // this variable has no dependency, no need to unload it
                continue;
            }
            boolean needToUnload = false;
            for (String variableDependencyName : variableDependencyNames) {
                ReportVariable<?> variableDependency = variablesMap.get(variableDependencyName);
                if (variableDependency.isValuesLoadedAndSelectedValueFilled()) {
                    // this dependency variable can be used, so do not unload it
                    continue;
                }
                // found a dependency that can not be used, so will need to unload the variable
                needToUnload = true;
                break;
            }
            if (needToUnload) {
                variable.unload();
            }
        }
    }

    /**
     * To unload some variables if they have at least one dependency on a variable that is not loaded, nor selected
     *
     * @param dependencyVariableName the dependency variable name that has changed
     */
    public void unloadVariablesIfNecessary(String dependencyVariableName) {
        Map<String, Set<String>> variableDependencyNamesMap = getVariableDependencies();
        for (ReportVariable<?> variable : variables) {
            if (!variable.isValuesLoaded()) {
                // this variable is not loaded (so can not unload it)
                continue;
            }
            Set<String> variableDependencyNames = variableDependencyNamesMap.get(variable.getName());
            if (variableDependencyNames == null) {
                // this variable has no dependency
                continue;
            }
            if (variableDependencyNames.contains(dependencyVariableName)) {
                // as the dependency has changed, we can simply here unloaded this variable
                variable.unload();
            }
        }
    }

}
