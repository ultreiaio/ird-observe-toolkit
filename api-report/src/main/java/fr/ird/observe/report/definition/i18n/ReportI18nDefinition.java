package fr.ird.observe.report.definition.i18n;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialLocale;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created at 08/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class ReportI18nDefinition {

    /**
     * All translations available for a report.
     */
    private final Map<String, ReportI18nNamedStringDefinition> translations;

    public ReportI18nDefinition(List<ReportI18nNamedStringDefinition> translations) {
        this.translations = Collections.unmodifiableMap(Objects.requireNonNull(translations).stream().collect(Collectors.toMap(k -> String.format("@%s@", k.getName()), Function.identity())));
    }

    public String getTranslation(ReferentialLocale referentialLocale, String id) {
        Optional<ReportI18nNamedStringDefinition> definition = getDefinition(id);
        return definition.map(d -> d.apply(referentialLocale)).orElse(null);
    }

    private Optional<ReportI18nNamedStringDefinition> getDefinition(String id) {
        return Optional.ofNullable(translations.get(id));
    }

    public Map<String, ReportI18nNamedStringDefinition> getTranslations() {
        return translations;
    }

}
