package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Set;

/**
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
@AutoService(ReportOperationConsumer.class)
public class SumColumn implements ReportOperationConsumer {
    private static final Logger log = LogManager.getLogger(SumColumn.class);

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        int x = 0;
        int y = 0;
        int height = incoming.getHeight();
        int width = incoming.getWidth();
        if (!parameters.isEmpty()) {
            String[] split = parameters.split("\\|");
            x = Integer.parseInt(split[0]);
            y = Integer.parseInt(split[1]);
            if (x + 1 >= height) {
                // only one row in column to sum, so skip operation
                return incoming;
            }
        }

        DataMatrix tmpMatrix = createTmpMatrix(0, height, width, 1);
        for (int column = y; column < width; column++) {
            Object sumColumn = getSumColumn(column, x, incoming);
            tmpMatrix.setValue(column, 0, sumColumn);
        }
        DataMatrix merge = DataMatrix.merge(incoming, tmpMatrix);
        if (report.getRowHeaders() == null) {
            merge.setValue(0, height, requestExecutor.i18nContext().translate(I18n.n("observe.Common.total")));
        }
        return merge;

    }

    protected Object getSumColumn(int column, int x, DataMatrix incoming) {
        float result = 0f;
        int nbRows = incoming.getHeight();
        for (int row = x; row < nbRows; row++) {
            Serializable o = incoming.getValue(column, row);
            if (o == null || "null".equals(o)) {
                o = 0;
            }
            double d;
            try {
                d = Double.parseDouble(o.toString());
            } catch (NumberFormatException e) {
                log.debug(String.format("Could not convert %s to number", o), e);
                return "-";
            }
            result += d;
        }
        return Numbers.roundFourDigits(result);
    }
}
