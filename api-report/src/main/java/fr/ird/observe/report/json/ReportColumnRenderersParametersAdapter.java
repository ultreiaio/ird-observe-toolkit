package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.report.ColumnRendererParameters;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created at 01/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@AutoService(JsonAdapter.class)
public class ReportColumnRenderersParametersAdapter implements JsonDeserializer<ReportColumnRenderersParameters>, JsonSerializer<ReportColumnRenderersParameters>, JsonAdapter {

    private static final String TYPE = "type";
    private static final String VALUE = "value";

    @Override
    public Class<?> type() {
        return ReportColumnRenderersParameters.class;
    }

    @Override
    public ReportColumnRenderersParameters deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonArray columnRendererParametersJson = json.getAsJsonArray();
        List<ColumnRendererParameters> list = new ArrayList<>(columnRendererParametersJson.size());
        for (JsonElement itemJson : columnRendererParametersJson) {
            JsonObject asJsonObject = itemJson.getAsJsonObject();
            String key = context.deserialize(asJsonObject.get(TYPE), String.class);
            Class<ColumnRendererParameters> itemType = Objects2.forName(key);
            list.add(context.deserialize(asJsonObject.get(VALUE), itemType));
        }
        return new ReportColumnRenderersParameters(list);
    }

    @Override
    public JsonElement serialize(ReportColumnRenderersParameters src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray result = new JsonArray();
        for (ColumnRendererParameters columnRendererParameter : src.getColumnRendererParameters()) {
            JsonObject item = new JsonObject();
            item.addProperty(TYPE, columnRendererParameter.getClass().getName());
            item.add(VALUE, context.serialize(columnRendererParameter));
            result.add(item);
        }
        return result;
    }
}
