package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.lang.Numbers;
import io.ultreia.java4all.util.matrix.DataMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Set;

/**
 * To subtract two columns values and place it in a new column.
 * <p>
 * Created on 15/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.1.6
 */
@AutoService(ReportOperationConsumer.class)
public class SubtractColum implements ReportOperationConsumer {
    private static final Logger log = LogManager.getLogger(SubtractColum.class);

    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        if (parameters.isEmpty()) {
            throw new IllegalStateException(String.format("parameters of operation %s can't not be null (format: column1|column2|targetColumn)", getClass().getName()));
        }
        String[] split = parameters.trim().split("\\|");
        if (split.length != 3) {
            throw new IllegalStateException(String.format("parameters of operation %s must have three parameters (format: column1|column2|targetColumn), but was: %s", getClass().getName(), parameters.trim()));
        }
        int column1 = Integer.parseInt(split[0]);
        int column2 = Integer.parseInt(split[1]);
        int targetColumn = Integer.parseInt(split[2]);

        int height = incoming.getHeight();

        DataMatrix tmpMatrix = createTmpMatrix(targetColumn, 0, 1, height);
        for (int row = 0; row < height; row++) {
            Object sumRow = getSubtract(row, column1, column2, incoming);
            tmpMatrix.setValue(0, row, sumRow);
        }
        return DataMatrix.merge(incoming, tmpMatrix);
    }

    protected Object getSubtract(int row, int column1, int column2, DataMatrix incoming) {

        Double v1 = getValue(row, column1, incoming);
        Double v2 = getValue(row, column2, incoming);
        if (v1 == null && v2 == null) {
            return null;
        }
        if (v1 == null) {
            v1 = 0d;
        }
        if (v2 == null) {
            v2 = 0d;
        }
        float result = (float) (v1 - v2);
        return Numbers.roundFourDigits(result);
    }

    protected Double getValue(int row, int column, DataMatrix incoming) {
        Serializable o = incoming.getValue(column, row);
        if (o == null || "null".equals(o)) {
            return null;
        }
        try {
            return Double.parseDouble(o.toString());
        } catch (NumberFormatException e) {
            log.debug(String.format("Could not convert %s to number", o), e);
            return null;
        }
    }
}
