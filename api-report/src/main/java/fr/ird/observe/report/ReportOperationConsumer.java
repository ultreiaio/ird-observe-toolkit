package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportOperationDefinition;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;

/**
 * To consumer a {@link ReportOperationDefinition}.
 * <p>
 * For each operation, create a such object and register it by {@link java.util.ServiceLoader} mechanism.
 * <p>
 * Created on 21/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.44
 */
public interface ReportOperationConsumer {

    /**
     * @return name of the operation, must be unique
     */
    default String name() {
        return getClass().getSimpleName();
    }

    DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming);

    default DataMatrix createTmpMatrix(int positionX, int positionY, int width, int height) {
        return DataMatrix.create(positionX, positionY, width, height);
    }
}
