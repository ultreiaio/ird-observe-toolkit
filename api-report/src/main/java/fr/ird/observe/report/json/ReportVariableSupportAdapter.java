package fr.ird.observe.report.json;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import fr.ird.observe.report.ReportVariableSupport;
import fr.ird.observe.report.definition.ReportVariableDefinitionSupport;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

/**
 * Shared code for any {@link ReportVariableSupport} implementation.
 * <p>
 * Created on 14/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.22
 */
public abstract class ReportVariableSupportAdapter<V, D extends ReportVariableDefinitionSupport<V>, R extends ReportVariableSupport<V, D>> implements JsonAdapter, JsonSerializer<R>, JsonDeserializer<R> {

    @Override
    public JsonElement serialize(R src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("V", context.serialize(src.definition().getType()));
        jsonObject.add("D", context.serialize(src.definition().getClass()));
        serialize(src, context, jsonObject);
        return jsonObject;
    }

    protected void serialize(R src, JsonSerializationContext context, JsonObject jsonObject) {
        jsonObject.add(ReportVariableSupport.PROPERTY_DEFINITION, context.serialize(src.definition()));
        jsonObject.add(ReportVariableSupport.PROPERTY_VALUES, context.serialize(src.getValues()));
    }

    @Override
    public final R deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Class<V> vType = context.deserialize(jsonObject.get("V"), Class.class);
        Class<D> dType = context.deserialize(jsonObject.get("D"), Class.class);
        Type definitionType = TypeToken.getParameterized(dType, vType).getType();

        D definition = context.deserialize(jsonObject.get(ReportVariableSupport.PROPERTY_DEFINITION), definitionType);
        @SuppressWarnings("unchecked") R variable = (R) definition.toVariable();

        deserialize(context, jsonObject, variable);
        return variable;
    }

    protected void deserialize(JsonDeserializationContext context, JsonObject jsonObject, R variable) {
        deserializeValues(context, jsonObject, variable);
    }

    protected void deserializeValues(JsonDeserializationContext context, JsonObject jsonObject, R variable) {
        JsonElement valuesJson = jsonObject.get(ReportVariableSupport.PROPERTY_VALUES);
        if (valuesJson != null) {
            Class<V> type = variable.getType();
            JsonArray jsonArray = valuesJson.getAsJsonArray();
            Set<V> values = new HashSet<>(jsonArray.size());
            for (JsonElement valueJson : jsonArray) {
                V value = deserializeValue(context, type, valueJson);
                values.add(value);
            }
            variable.setValues(values);
        }
    }

    protected V deserializeValue(JsonDeserializationContext context, Class<V> variableType, JsonElement valueJson) {
        return valueJson == null ? null : context.deserialize(valueJson, variableType);
    }
}
