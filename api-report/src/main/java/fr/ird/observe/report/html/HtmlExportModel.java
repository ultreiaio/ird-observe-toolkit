package fr.ird.observe.report.html;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ObserveUtil;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.report.ColumnRendererConsumers;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportColumnRenderersParameters;
import fr.ird.observe.report.ReportVariableSupport;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.SingletonSupplier;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The model used to generate data file in html export.
 * <p>
 * Created at 15/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@GenerateTemplate(template = "reportHtmlExport.ftl")
public class HtmlExportModel {
    private static SingletonSupplier<String> SCRIPT_CONTENT;
    private static SingletonSupplier<String> STYLE_CONTENT;
    /**
     * Referential local used.
     */
    private final ReferentialLocale referentialLocale;
    /**
     * Selected report.
     */
    private final transient Report selectedReport;
    private final transient Set<String> columnRendererFunctions;
    private final transient List<String> columnRendererInitCode;
    private final List<String> columnNames;
    private final List<String> rowNames;
    private final DataMatrix data;
    private final boolean withColumnHeader;
    private final boolean withRowHeader;
    private final transient String json;
    private final int selectedCount;
    private final Map<String, List<String>> selectedLabels;

    public HtmlExportModel(Gson gson,
                           ReferentialLocale referentialLocale,
                           Report selectedReport,
                           List<String> columnNames,
                           List<String> rowNames,
                           DataMatrix data,
                           boolean withColumnHeader,
                           boolean withRowHeader,
                           ReportColumnRenderersParameters reportColumnRenderersParameters,
                           int selectedCount,
                           Map<String, List<String>> selectedLabels) {
        this.referentialLocale = Objects.requireNonNull(referentialLocale);
        this.selectedReport = selectedReport;
        this.columnNames = columnNames;
        this.rowNames = rowNames;
        this.data = data;
        this.withColumnHeader = withColumnHeader;
        this.withRowHeader = withRowHeader;
        this.selectedCount = selectedCount;
        this.selectedLabels = selectedLabels;
        if (reportColumnRenderersParameters != null) {
            this.columnRendererFunctions = ColumnRendererConsumers.htmlFunctions(reportColumnRenderersParameters);
            this.columnRendererInitCode = ColumnRendererConsumers.htmlInitCode(reportColumnRenderersParameters);
        } else {
            this.columnRendererFunctions = Set.of();
            this.columnRendererInitCode = List.of();
        }
        this.json = gson.toJson(this);
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public String getReportName() {
        return getSelectedReport().getName(getReferentialLocale());
    }

    public String getVariableName(ReportVariableSupport<?, ?> variable) {
        return I18n.l(getReferentialLocale().getLocale(), I18nDecoratorHelper.getType(variable.getType()));

    }

    public String getScript() {
        if (SCRIPT_CONTENT == null) {
            String resourceName = getClass().getSimpleName() + ".js";
            URL url = Objects.requireNonNull(getClass().getResource(resourceName), "Could not find resource: " + resourceName);
            SCRIPT_CONTENT = ObserveUtil.loadResourceContentSupplier(url, ObserveUtil.removeJavaLicense());
        }
        return SCRIPT_CONTENT.get();
    }

    public String getStyle() {
        if (STYLE_CONTENT == null) {
            String resourceName = getClass().getSimpleName() + ".css";
            URL url = Objects.requireNonNull(getClass().getResource(resourceName), "Could not find resource: " + resourceName);
            STYLE_CONTENT = ObserveUtil.loadResourceContentSupplier(url, ObserveUtil.removeJavaLicense());
        }
        return STYLE_CONTENT.get();
    }

    public String getJson() {
        return json;
    }

    public Set<String> getColumnRendererFunctions() {
        return columnRendererFunctions;
    }

    public List<String> getColumnRendererInitCode() {
        return columnRendererInitCode;
    }

    public Report getSelectedReport() {
        return selectedReport;
    }

    public int getSelectedCount() {
        return selectedCount;
    }

    public Map<String, List<String>> getSelectedLabels() {
        return selectedLabels;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public List<String> getRowNames() {
        return rowNames;
    }

    public DataMatrix getData() {
        return data;
    }

    public boolean isWithColumnHeader() {
        return withColumnHeader;
    }

    public boolean isWithRowHeader() {
        return withRowHeader;
    }
}
