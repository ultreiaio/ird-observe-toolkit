package fr.ird.observe.report;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.report.definition.ReportRepeatVariableDefinition;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Definition of a repeatable variable of a report, user has no action possible on it.
 * <p>
 * They are used to perform report requests in two ways:
 * <ul>
 *     <li>first usage: use it as a discriminant in a report request, it means that the request will execute for each of his values</li>
 *     <li>second usage: for some special operations, we use the hole values of the repeatable variable</li>
 * </ul>
 * <p>
 * Created on 14/12/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.23
 */
public final class ReportRepeatVariable<V> extends ReportVariableSupport<V, ReportRepeatVariableDefinition<V>> {

    private static final long serialVersionUID = 1L;

    public ReportRepeatVariable(ReportRepeatVariableDefinition<V> definition) {
        super(definition);
    }

    public boolean isAddNullValue() {
        return definition().isAddNullValue();
    }

    @Override
    public void setValues(Set<V> values) {
        if (values != null && isAddNullValue()) {
            values.add(null);
        }
        super.setValues(values);
    }

    public List<String> computeIndexList() {
        //FIXME This is already done by setValues method? Why using a "null" instead of null value?
        Set<V> tmp = new LinkedHashSet<>(getValues());
        boolean useNullValue = tmp.remove(null);
        List<String> result = tmp.stream().map(s -> s instanceof ToolkitId ? ((ToolkitId) s).getId() : String.valueOf(s)).collect(Collectors.toList());
        if (useNullValue || isAddNullValue()) {
            result.add("null");
        }
        return result;
    }
}
