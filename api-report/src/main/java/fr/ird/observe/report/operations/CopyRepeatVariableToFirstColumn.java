package fr.ird.observe.report.operations;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.referential.WithI18n;
import fr.ird.observe.report.Report;
import fr.ird.observe.report.ReportOperationConsumer;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.util.matrix.DataMatrix;

import java.util.Set;

/**
 * To copy all values of a repeat variable to the first column.
 * <p>
 * If repeat variables values are {@link WithI18n}, then will use the {@link WithI18n#getLabel2()}.
 * <p>
 * TODO: Be able to use the correct locale to translate with correct label.
 * <p>
 * Created on 15/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
@AutoService(ReportOperationConsumer.class)
public class CopyRepeatVariableToFirstColumn implements ReportOperationConsumer {
    @Override
    public DataMatrix consume(String parameters, ReportRequestExecutor requestExecutor, Report report, Set<String> tripId, DataMatrix incoming) {
        if (parameters.isEmpty()) {
            throw new IllegalStateException(String.format("parameters of operation %s can't not be null (format: repeatVariableName)", getClass().getName()));
        }
        String repeatVariableName = parameters.trim();
        Set<?> repeatVariableValues = report.getRepeatVariable(repeatVariableName).getValues();
        DataMatrix tmpMatrix = createTmpMatrix(0, 0, 1 + incoming.getWidth(), repeatVariableValues.size());
        int index = 0;
        for (Object value : repeatVariableValues) {
            if (value instanceof WithI18n) {
                value = ((WithI18n) value).getLabel2();
            }
            tmpMatrix.setValue(0, index++, value);
        }
        incoming.setX(1);
        return DataMatrix.merge(incoming, tmpMatrix);
    }
}
