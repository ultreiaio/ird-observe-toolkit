package fr.ird.observe.report.renderers;

/*-
 * #%L
 * Toolkit :: API :: Report
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.report.ColumnRendererConsumer;
import fr.ird.observe.report.ColumnRendererParameters;
import fr.ird.observe.report.ReportRequestExecutor;
import io.ultreia.java4all.util.SingletonSupplier;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;

import java.awt.Color;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import static fr.ird.observe.report.renderers.HighlightIfEquals18nReferentialValue.Parameters;

/**
 * Created at 02/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@SuppressWarnings("rawtypes")
@AutoService(ColumnRendererConsumer.class)
public class HighlightIfEquals18nReferentialValue implements ColumnRendererConsumer<Parameters> {
    private final static SingletonSupplier<String> HTML_FUNCTION = ColumnRendererConsumer.htmlFunctions(HighlightIfEquals18nReferentialValue.class);

    @Override
    public int parametersCount() {
        return 3;
    }

    @Override
    public String parametersSyntax() {
        return "column1,column2,...,columnN|idError|idOk";
    }

    @Override
    public Parameters parseParameters(String parameters) {
        String[] split = pareParametersSyntax(parameters);
        String[] split2 = split[0].trim().split("\\s*,\\s*");
        Set<Integer> columns = new TreeSet<>();
        for (String s : split2) {
            columns.add(Integer.parseInt(s));
        }
        String idError = split[1];
        String idOk = split[2];
        return new Parameters(columns, idError, idOk);
    }

    @Override
    public Parameters createParameters(ReportRequestExecutor requestExecutor, String parameters) {
        Parameters result = ColumnRendererConsumer.super.createParameters(requestExecutor, parameters);
        String labelError = requestExecutor.getReferentialLabel(result.getIdError());
        String labelOk = requestExecutor.getReferentialLabel(result.getIdOk());
        return result.setLabel(labelError, labelOk);
    }

    @Override
    public void consumeSwing(Parameters parameters, JXTable table) {
        table.addHighlighter(new ColorHighlighter((renderer, adapter) -> {
            Object value = adapter.getValue();
            int column = adapter.convertColumnIndexToModel(adapter.column);
            return parameters.getColumns().contains(column) && Objects.equals(value, parameters.getLabelError());
        }, Color.RED, Color.BLACK));

        table.addHighlighter(new ColorHighlighter((renderer, adapter) -> {
            Object value = adapter.getValue();
            int column = adapter.convertColumnIndexToModel(adapter.column);
            return parameters.getColumns().contains(column) && !Objects.equals(value, parameters.getLabelError()) && !Objects.equals(value, parameters.getLabelOk());
        }, Color.ORANGE, Color.BLACK));
    }

    @Override
    public String htmlFunctions() {
        return HTML_FUNCTION.get();
    }

    @Override
    public String htmlInitCode(Parameters parameters) {
        return String.format("init%s(%s, '%s', '%s', result, data);\n", parameters.name(), parameters.getColumns(), parameters.getLabelError(), parameters.getLabelOk());
    }

    public static final class Parameters implements ColumnRendererParameters {
        private final Set<Integer> columns;
        private final String idError;
        private final String idOk;
        private final String labelError;
        private final String labelOk;

        public Parameters(Set<Integer> columns, String idError, String idOk, String labelError, String labelOk) {
            this.columns = columns;
            this.idError = idError;
            this.idOk = idOk;
            this.labelError = labelError;
            this.labelOk = labelOk;
        }

        public Parameters(Set<Integer> columns, String idError, String idOk) {
            this.columns = columns;
            this.idError = idError;
            this.idOk = idOk;
            this.labelError = null;
            this.labelOk = null;
        }

        @Override
        public String name() {
            return HighlightIfEquals18nReferentialValue.class.getSimpleName();
        }

        public Set<Integer> getColumns() {
            return columns;
        }

        public String getIdError() {
            return idError;
        }

        public String getIdOk() {
            return idOk;
        }

        public String getLabelError() {
            return labelError;
        }

        public String getLabelOk() {
            return labelOk;
        }

        public Parameters setLabel(String labelError, String labelOk) {
            return new Parameters(columns, idError, idOk, labelError, labelOk);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Parameters)) return false;
            Parameters that = (Parameters) o;
            return Objects.equals(getColumns(), that.getColumns()) && Objects.equals(getIdError(), that.getIdError()) && Objects.equals(getIdOk(), that.getIdOk()) && Objects.equals(getLabelError(), that.getLabelError()) && Objects.equals(getLabelOk(), that.getLabelOk());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getColumns(), getIdError(), getIdOk(), getLabelError(), getLabelOk());
        }
    }
}
