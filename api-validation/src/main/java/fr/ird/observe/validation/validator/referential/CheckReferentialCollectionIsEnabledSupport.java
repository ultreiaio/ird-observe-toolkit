package fr.ird.observe.validation.validator.referential;

/*
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;

/**
 * Created by tchemit on 17/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class CheckReferentialCollectionIsEnabledSupport<O, F extends ReferentialDtoReference> extends SkipableFieldValidatorSupport<O, Collection<F>> {

    private static final Logger log = LogManager.getLogger(CheckReferentialCollectionIsEnabledSupport.class);
    private final NuitonValidatorScope scope;

    public CheckReferentialCollectionIsEnabledSupport(String fieldName, Function<O, Collection<F>> fieldFunction, NuitonValidatorScope scope) {
        super(fieldName, fieldFunction, (o, v) -> !Objects.equals(((ValidationContextSupport) v).getConfiguration().getValidationDisabledReferentialScope(), Objects.requireNonNull(scope).name()));
        this.scope = scope;
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String fieldName = Objects.requireNonNull(getFieldName());
        Collection<F> fieldValue = getField(object);
        if (fieldValue == null || fieldName.isEmpty()) {
            return;
        }
        int referencePosition = 1;
        for (F o : fieldValue) {
            validateOne(validationContext, messagesCollector, o, referencePosition++);
        }
    }

    private void validateOne(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, F fieldValue, int referencePosition) {
        if (fieldValue.isEnabled()) {
            return;
        }
        if (fieldValue.decorator().isEmpty()) {
            DecoratorService decoratorService = ((ValidationContextSupport) validationContext).getDecoratorService();
            decoratorService.installDecorator(fieldValue);
        }
        String referenceValue = fieldValue.toString();
        log.debug(String.format("[%s] Found a referential (%s) disabled.", scope, referenceValue));
        addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.multiple.referential.disabled"), referenceValue, referencePosition);
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends FieldValidatorSupport.GeneratorSupport {

        public Generator() {
            super(CheckReferentialCollectionIsEnabledSupport.class, true, true, false, false);
        }

        @Override
        public boolean accept(Class<?> validationType) {
            return getValidatorType().isAssignableFrom(validationType);
        }

    }
}
