package fr.ird.observe.validation.validator.referential;

/*
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.function.Function;

/**
 * Created by tchemit on 17/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class CheckReferentialIsEnabledSupport<O, F extends ReferentialDtoReference> extends SkipableFieldValidatorSupport<O, F> {

    private static final Logger log = LogManager.getLogger(CheckReferentialIsEnabledSupport.class);
    private final NuitonValidatorScope scope;

    public CheckReferentialIsEnabledSupport(String fieldName, Function<O, F> fieldFunction, NuitonValidatorScope scope) {
        super(fieldName, fieldFunction, (o, v) -> !Objects.equals(((ValidationContextSupport) v).getConfiguration().getValidationDisabledReferentialScope(), Objects.requireNonNull(scope).name()));
        this.scope = scope;
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        F fieldValue = getField(object);
        if (fieldValue == null) {
            return;
        }
        validateOne(validationContext, messagesCollector, fieldValue);
    }

    private void validateOne(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, F fieldValue) {
        if (fieldValue.isEnabled()) {
            return;
        }
        String referenceType = I18nDecoratorHelper.getType(validationContext.getLocale(), fieldValue.getReferenceType());
        log.debug(String.format("[%s] Found a referential (%s) disabled.", scope, fieldValue));
        addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.single.referential.disabled"), referenceType);
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends FieldValidatorSupport.GeneratorSupport {

        public Generator() {
            super(CheckReferentialIsEnabledSupport.class, true, true, false, false);
        }

        @Override
        public boolean accept(Class<?> validationType) {
            return getValidatorType().isAssignableFrom(validationType);
        }
    }

}
