package fr.ird.observe.validation.validator;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.reference.ReferentialDtoReferenceAware;
import fr.ird.observe.dto.referential.ReferentialDto;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static io.ultreia.java4all.i18n.I18n.n;
import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Ask a comment if some of referential used require one (see {@link ReferentialDto#isNeedComment()}.
 * <p>
 * Created by tchemit on 18/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CommentNeededDtoValidator<O> extends FieldValidatorSupport<O, String> {

    private final PropertyAccessor<O, ?>[] getters;

    @SafeVarargs
    public CommentNeededDtoValidator(String fieldName, Function<O, String> fieldFunction, PropertyAccessor<O, ?>... getters) {
        super(fieldName, fieldFunction);
        this.getters = getters;
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String commentValue = getField(object);
        if (commentValue != null && !commentValue.isEmpty()) {
            return;
        }
        for (PropertyAccessor<O, ?> getter : getters) {
            Object fieldValue = getter.apply(object);
            if (fieldValue instanceof Collection) {
                boolean needComment = false;
                for (Object o : (Collection<?>) fieldValue) {
                    needComment = validateOne((ReferentialDtoReferenceAware) o);
                    if (needComment) {
                        break;
                    }
                }
                if (needComment) {
                    addError(validationContext, messagesCollector, object, n("observe.Common.validation.comment.needed.for.multiple"), getter.getPropertyName());
                }
            } else {
                boolean needComment = validateOne((ReferentialDtoReferenceAware) fieldValue);
                if (needComment) {
                    addError(validationContext, messagesCollector, object, n("observe.Common.validation.comment.needed.for.single"), getter.getPropertyName());
                }
            }
        }
    }

    private boolean validateOne(ReferentialDtoReferenceAware fieldValue) {
        return fieldValue != null && fieldValue.isNeedComment();
    }

    private void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, O object, String messageKey, String propertyName) {
        String propertyLabel = t(I18nDecoratorHelper.getPropertyI18nKey(object.getClass(), propertyName));
        addMessage(validationContext, messagesCollector, messageKey, propertyLabel);
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(CommentNeededDtoValidator.class, true, true, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            String fqn = importManager.addImport(PropertyAccessor.class);
            String[] propertyNames = validatorDefinition.getParameter("propertyNames").split("\\s*,\\s*");
            for (String propertyName : propertyNames) {
                String getterName = ExpressionResolver.guessGetterName(key.getBeanType(), propertyName);
                result.add(String.format("%s.of(%s, %s::%s)", fqn, ExpressionResolver.escapeString(propertyName), key.getBeanType().getSimpleName(), getterName));
            }
        }
    }
}
