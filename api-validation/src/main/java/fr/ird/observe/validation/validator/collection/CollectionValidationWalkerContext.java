package fr.ird.observe.validation.validator.collection;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiPredicate;

/**
 * Created on 10/03/2022.
 *
 * @param <C> type of object in collection
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public class CollectionValidationWalkerContext<D extends ToolkitId, C extends ToolkitId> {

    protected final D container;
    protected final Collection<C> collection;
    protected final BiPredicate<CollectionValidationWalkerContext<D, C>, C> usePredicate;
    protected final BiPredicate<CollectionValidationWalkerContext<D, C>, C> validPredicate;
    protected final int size;
    protected C editing;
    protected int index = -1;
    protected C current;
    protected C previous;

    public static <D extends ToolkitId, C extends ToolkitId> CollectionValidationWalkerContext<D, C> validate(D container, Collection<C> collection, C skip, BiPredicate<CollectionValidationWalkerContext<D, C>, C> usePredicate, BiPredicate<CollectionValidationWalkerContext<D, C>, C> validPredicate) {
        return create(container, collection, usePredicate, validPredicate).map(c -> c.setEditing(skip).validate()).orElse(null);
    }

    public static <D extends ToolkitId, C extends ToolkitId> CollectionValidationWalkerContext<D, C> validate(D container, Collection<C> collection, BiPredicate<CollectionValidationWalkerContext<D, C>, C> usePredicate, BiPredicate<CollectionValidationWalkerContext<D, C>, C> validPredicate) {
        return create(container, collection, usePredicate, validPredicate).map(CollectionValidationWalkerContext::validate).orElse(null);
    }

    public static <D extends ToolkitId, C extends ToolkitId> Optional<CollectionValidationWalkerContext<D, C>> create(D container, Collection<C> collection, BiPredicate<CollectionValidationWalkerContext<D, C>, C> usePredicate, BiPredicate<CollectionValidationWalkerContext<D, C>, C> validPredicate) {
        if (collection == null || collection.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new CollectionValidationWalkerContext<>(container, collection, usePredicate, validPredicate));
    }

    public CollectionValidationWalkerContext(D container, Collection<C> collection, BiPredicate<CollectionValidationWalkerContext<D, C>, C> usePredicate, BiPredicate<CollectionValidationWalkerContext<D, C>, C> validPredicate) {
        this.container = container;
        this.collection = Objects.requireNonNull(collection);
        this.size = collection.size();
        this.usePredicate = usePredicate;
        this.validPredicate = validPredicate;
    }

    public CollectionValidationWalkerContext<D, C> setEditing(C editing) {
        this.editing = editing;
        return this;
    }

    public CollectionValidationWalkerContext<D, C> validate() {
        for (C current : collection) {
            current = getCurrent(current);
            boolean use = addCurrent(current, usePredicate);
            if (use) {
                boolean valid = validPredicate.test(this, current);
                if (!valid) {
                    return this;
                }
            }
        }
        return null;
    }

    private C getCurrent(C current) {
        if (editing != null && Objects.equals(current.getId(), editing.getId())) {
            // use editing one as current
            return editing;
        }
        return current;
    }

    public boolean addCurrent(C current, BiPredicate<CollectionValidationWalkerContext<D, C>, C> predicate) {
        index++;
        if (editing != null) {
            if (!Objects.equals(current.getId(), editing.getId())) {
                boolean use = predicate.test(this, current);
                if (use) {
                    this.previous = this.current;
                    this.current = current;
                }
                // always return false, since we don't want to validate this entry
                return false;
            }
        }
        boolean use = predicate.test(this, current);
        if (use) {
            this.previous = this.current;
            this.current = current;
        }
        return use;
    }

    public D getContainer() {
        return container;
    }

    public C getCurrent() {
        return current;
    }

    public int getIndex() {
        return index;
    }

    public int getHumanIndex() {
        return index + 1;
    }

    public C getPrevious() {
        return previous;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFirst() {
        return index == 0;
    }

    public boolean isLast() {
        return index == size - 1;
    }
}
