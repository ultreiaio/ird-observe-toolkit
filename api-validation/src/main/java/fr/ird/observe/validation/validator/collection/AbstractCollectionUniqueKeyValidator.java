package fr.ird.observe.validation.validator.collection;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Abstract collection unique key validator that will be extended by generation to stop using costing ognl usage.
 * <p>
 * Compile time is always a better choice that runtime :)
 * <p>
 * Created on 30/10/2022.
 *
 * @param <O> type of object to validate inside the collection.
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.17
 */
public abstract class AbstractCollectionUniqueKeyValidator<O, F> extends SkipableFieldValidatorSupport<O, Collection<F>> {

    private final String validationMessageKey;

    public AbstractCollectionUniqueKeyValidator(String fieldName, Function<O, Collection<F>> fieldFunction, String validationMessageKey) {
        this(fieldName, fieldFunction, null, validationMessageKey);
    }

    public AbstractCollectionUniqueKeyValidator(String fieldName, Function<O, Collection<F>> fieldFunction, BiFunction<O, NuitonValidationContext, Boolean> skipFunction, String validationMessageKey) {
        super(fieldName, fieldFunction, skipFunction);
        this.validationMessageKey = validationMessageKey;
    }

    protected abstract String computeUniqueKey(F collectionValue);

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        Collection<F> collection = getField(object);
        if (collection == null || collection.size() < 2) {
            // can't have doubloon values
            return;
        }
        Set<String> uniqueKeys = new LinkedHashSet<>();
        try {
            for (F collectionValue : collection) {
                String uniqueKey = computeUniqueKey(collectionValue);
                if (!uniqueKeys.add(uniqueKey)) {
                    // found a duplicated entry
                    int firstBadIndex = uniqueKeys.size() + 1;
                    Decorated decorated = (Decorated) collectionValue;
                    if (decorated.decorator().isEmpty()) {
                        DecoratorService decoratorService = ((ValidationContextSupport) validationContext).getDecoratorService();
                        decoratorService.installDecorator(decorated);
                    }
                    String firstBadRow = collectionValue.toString();
                    addMessage(validationContext, messagesCollector, validationMessageKey, firstBadIndex, firstBadRow);
                    return;
                }
            }

        } finally {
            uniqueKeys.clear();
        }
    }

    protected void addComponentValue(StringBuilder builder, IdDto value) {
        builder.append("|").append(Optional.ofNullable(value).map(IdDto::getId).orElse("null"));
    }

    protected void addComponentValue(StringBuilder builder, Date value) {
        builder.append("|").append(Optional.ofNullable(value).map(d -> d.getTime() + "").orElse("null"));
    }

    protected void addComponentValue(StringBuilder builder, Enum<?> value) {
        builder.append("|").append(Optional.ofNullable(value).map(Enum::name).orElse("null"));
    }

    protected void addComponentValue(StringBuilder builder, Number value) {
        builder.append("|").append(Optional.ofNullable(value).map(Number::toString).orElse("null"));
    }

    protected void addComponentValue(StringBuilder builder, int value) {
        builder.append("|").append(value);
    }

    protected void addComponentValue(StringBuilder builder, String value) {
        builder.append("|").append(value);
    }

    protected void addComponentValue(StringBuilder builder, long value) {
        builder.append("|").append(value);
    }

    protected void addComponentValue(StringBuilder builder, double value) {
        builder.append("|").append(value);
    }

    protected void addComponentValue(StringBuilder builder, float value) {
        builder.append("|").append(value);
    }

}
