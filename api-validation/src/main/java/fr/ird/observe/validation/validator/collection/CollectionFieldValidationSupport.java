package fr.ird.observe.validation.validator.collection;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitId;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public abstract class CollectionFieldValidationSupport<O extends ToolkitId, F, C extends ToolkitId> extends SkipableFieldValidatorSupport<O, F> {
    public CollectionFieldValidationSupport(String fieldName,
                                            Function<O, F> fieldFunction,
                                            BiFunction<O, NuitonValidationContext, Boolean> skipFunction) {
        this(fieldName, fieldFunction, null, null, skipFunction);
    }

    public CollectionFieldValidationSupport(String fieldName,
                                            Function<O, F> fieldFunction,
                                            String messageKey,
                                            MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                                            BiFunction<O, NuitonValidationContext, Boolean> skipFunction) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction);
    }

    protected abstract Collection<C> getCollection(NuitonValidationContext validationContext, O object);

    protected boolean processEntry(CollectionValidationWalkerContext<O, C> context, C current) {
        return true;
    }

    protected abstract boolean validateEntry(CollectionValidationWalkerContext<O, C> context, C current);

    protected abstract void addError(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, CollectionValidationWalkerContext<O, C> context, O object);

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        Collection<C> collection = getCollection(validationContext, object);
        CollectionValidationWalkerContext<O, C> context = CollectionValidationWalkerContext.validate(object, collection, this::processEntry, this::validateEntry);
        if (context != null) {
            addError(validationContext, messagesCollector, context, object);
        }
    }
}
