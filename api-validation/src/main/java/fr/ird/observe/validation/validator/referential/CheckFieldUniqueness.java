package fr.ird.observe.validation.validator.referential;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.validation.ValidationContextSupport;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;

/**
 * Created by tchemit on 19/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CheckFieldUniqueness<O extends ReferentialDto, F> extends FieldValidatorSupport<O, F> {

    private Decorator decorator;

    public CheckFieldUniqueness(String fieldName, Function<O, F> fieldFunction) {
        super(fieldName, fieldFunction);
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String fieldName = getFieldName();
        String id = object.getId();
        Collection<?> collection = ((ValidationContextSupport) validationContext).getEditingReferentielList();
        F code = getField(object);
        if (code == null) {
            return;
        }
        for (Object o : collection) {
            ReferentialDtoReference reference = (ReferentialDtoReference) o;
            if (Objects.equals(reference.getId(), id)) {
                continue;
            }
            if (Objects.equals(reference.get(fieldName), code)) {
                if (decorator == null) {
                    DecoratorService decoratorService = ((ValidationContextSupport) validationContext).getDecoratorService();
                    decorator = decoratorService.getDecoratorByType(reference.getClass());
                }
                String badReferentialValue = decorator.decorate(reference);
                addMessage(validationContext, messagesCollector, I18n.n("observe.referential.Referential.validation.field.unique"), "'" + code + "'", badReferentialValue);
                return;
            }
        }
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(CheckFieldUniqueness.class, true, true, false, false);
        }
    }

}
