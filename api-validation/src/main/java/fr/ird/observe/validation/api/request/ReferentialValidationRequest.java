package fr.ird.observe.validation.api.request;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.referential.ReferentialDto;

import java.util.Set;

/**
 * Created on 22/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ReferentialValidationRequest extends ValidationRequestSupport {

    /**
     * All referential types to validate.
     */
    private Set<Class<? extends ReferentialDto>> referentialTypes;

    public Set<Class<? extends ReferentialDto>> getReferentialTypes() {
        return referentialTypes;
    }

    public void setReferentialTypes(Set<Class<? extends ReferentialDto>> referentialTypes) {
        this.referentialTypes = referentialTypes;
    }
}
