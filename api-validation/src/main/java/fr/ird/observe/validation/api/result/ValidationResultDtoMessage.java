package fr.ird.observe.validation.api.result;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Objects;

/**
 * This is a referential validation message.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidationResultDtoMessage {

    private final String fieldName;
    private final NuitonValidatorScope scope;
    private final String message;
    private Class<?> type;

    public ValidationResultDtoMessage(NuitonValidatorScope scope, String fieldName, String message) {
        Objects.requireNonNull(scope);
        Objects.requireNonNull(fieldName);
        Objects.requireNonNull(message);
        this.scope = scope;
        this.fieldName = fieldName;
        this.message = message;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ValidationResultDtoMessage{fieldName='%s', scope=%s, message='%s'}", fieldName, scope, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationResultDtoMessage that = (ValidationResultDtoMessage) o;
        return Objects.equals(fieldName, that.fieldName) &&
                scope == that.scope &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName, scope, message);
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }
}
