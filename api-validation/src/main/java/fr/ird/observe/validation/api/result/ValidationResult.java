package fr.ird.observe.validation.api.result;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import io.ultreia.java4all.util.json.JsonAware;

import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by tchemit on 06/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ValidationResult implements JsonAware {

    private final Set<ValidationResultDto> nodes;

    public boolean withError() {
        for (ValidationResultDto node : nodes) {
            if (node.withError()) {
                return true;
            }
        }
        return false;
    }

    ValidationResult(Set<ValidationResultDto> nodes) {
        this.nodes = nodes;
    }

    public Set<ValidationResultDto> getNodes() {
        return nodes;
    }

    public void applyOnNodes(Consumer<ValidationResultDto> nodeConsumer) {
        for (ValidationResultDto node : nodes) {
            node.apply(nodeConsumer);
        }
    }
}
