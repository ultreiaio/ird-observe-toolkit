package fr.ird.observe.validation.api.result;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToToolkitIdLabel;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdLabel;

import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 30/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ValidationResultBuilder implements AutoCloseable {
    /**
     * All root nodes.
     */
    private final Set<ValidationResultDto> rootNodes;
    /**
     * All nodes indexed by the underlying data id.
     */
    private final Map<String, ValidationResultDto> nodesById;

    public static ValidationResultBuilder create() {
        return new ValidationResultBuilder();
    }

    protected ValidationResultBuilder() {
        this.nodesById = new TreeMap<>();
        this.rootNodes = new LinkedHashSet<>();
    }

    public <T extends ToToolkitIdLabel & ToolkitId> void addMessages(Deque<? extends T> paths, LinkedList<ValidationResultDtoMessage> messages) {
        if (messages == null || messages.isEmpty()) {
            return;
        }
        if (paths.isEmpty()) {
            // this cas can never, ever happen!
            throw new IllegalStateException("Can't add messages from empty paths");
        }
        ValidationResultDto node = null;
        for (T path : paths) {
            node = getNode(node, path);
        }
        Objects.requireNonNull(node).addMessages(messages);
    }

    public ValidationResult build() {
        return new ValidationResult(new LinkedHashSet<>(rootNodes));
    }

    public void addResult(ValidationResult result) {
        this.rootNodes.addAll(result.getNodes());
    }

    @Override
    public void close() {
        rootNodes.clear();
        nodesById.clear();
    }

    private <T extends ToToolkitIdLabel & ToolkitId> ValidationResultDto getNode(ValidationResultDto parentNode, T path) {
        String id = path.getId();
        ValidationResultDto node = nodesById.get(id);
        if (node == null) {
            // create it
            ToolkitIdLabel datum = path.toLabel();
            datum.setTopiaVersion(path.getTopiaVersion());
            node = new ValidationResultDto(parentNode, datum);
            // add it to nodes cache
            nodesById.put(id, node);
            if (parentNode == null) {
                // add it also a root node
                rootNodes.add(node);
            }
        }
        return node;
    }

}
