package fr.ird.observe.validation.validator.referential;

/*
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.reference.ReferentialDtoReference;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.function.Function;

/**
 * Created by tchemit on 17/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CheckReferentialIsEnabledOnErrorScope<O, F extends ReferentialDtoReference> extends CheckReferentialIsEnabledSupport<O, F> {
    public CheckReferentialIsEnabledOnErrorScope(String fieldName, Function<O, F> fieldFunction) {
        super(fieldName, fieldFunction, NuitonValidatorScope.ERROR);
    }

}
