package fr.ird.observe.validation.api.request;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

/**
 * Configuration used for any {@link ValidationRequestSupport}.
 * <p>
 * Created on 22/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class ValidationRequestConfigurationSupport implements JsonAware {

    private boolean validationUseDisabledReferential;

    public final boolean isValidationUseDisabledReferential() {
        return validationUseDisabledReferential;
    }

    public final void setValidationUseDisabledReferential(boolean validationUseDisabledReferential) {
        this.validationUseDisabledReferential = validationUseDisabledReferential;
    }

    public String getValidationDisabledReferentialScope() {
        return (isValidationUseDisabledReferential() ? NuitonValidatorScope.WARNING : NuitonValidatorScope.ERROR).name();
    }
}
