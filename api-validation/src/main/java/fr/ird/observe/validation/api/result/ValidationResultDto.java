package fr.ird.observe.validation.api.result;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Represents a validation result node.
 * <p>
 * Created on 29/07/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.2.0
 */
public class ValidationResultDto {
    /**
     * The underlying data.
     */
    private final ToolkitIdLabel datum;
    /**
     * Optional children.
     */
    private LinkedList<ValidationResultDto> children;
    /**
     * Optional messages for the underlying data.
     */
    private LinkedList<ValidationResultDtoMessage> messages;

    public ValidationResultDto(ValidationResultDto parent, ToolkitIdLabel datum) {
        this.datum = Objects.requireNonNull(datum);
        if (parent != null) {
            parent.addChildren(this);
        }
    }

    public ToolkitIdLabel getDatum() {
        return datum;
    }

    public LinkedList<ValidationResultDtoMessage> getMessages() {
        return messages;
    }

    public int getChildCount() {
        return children == null ? 0 : children.size();
    }

    public LinkedList<ValidationResultDto> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValidationResultDto)) return false;
        ValidationResultDto that = (ValidationResultDto) o;
        return Objects.equals(datum.getId(), that.datum.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(datum.getId());
    }

    public void apply(Consumer<ValidationResultDto> nodeConsumer) {
        nodeConsumer.accept(this);
        if (children != null) {
            for (ValidationResultDto child : children) {
                child.apply(nodeConsumer);
            }
        }
    }

    void addChildren(ValidationResultDto node) {
        if (children == null) {
            children = new LinkedList<>();
        }
        children.add(node);
    }

    boolean withError() {
        if (messages != null) {
            for (ValidationResultDtoMessage message : messages) {
                NuitonValidatorScope scope = message.getScope();
                if (scope == NuitonValidatorScope.ERROR || scope == NuitonValidatorScope.FATAL) {
                    return true;
                }
            }
        }
        if (children != null) {
            for (ValidationResultDto child : children) {
                if (child.withError()) {
                    return true;
                }
            }
        }
        return false;
    }

    void addMessages(LinkedList<ValidationResultDtoMessage> messages) {
        if (this.messages == null) {
            this.messages = new LinkedList<>();
        }
        this.messages.addAll(messages);
    }
}
