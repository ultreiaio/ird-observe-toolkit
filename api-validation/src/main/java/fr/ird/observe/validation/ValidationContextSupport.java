package fr.ird.observe.validation;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.decoration.DecoratorService;
import fr.ird.observe.dto.IdDto;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.navigation.id.IdProject;
import fr.ird.observe.validation.api.request.ValidationRequestConfigurationSupport;
import io.ultreia.java4all.util.Dates;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * Created on 19/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public abstract class ValidationContextSupport implements NuitonValidationContext, Closeable {

    public static final String CREATE_VALIDATION_CONTEXT = "create";
    public static final String UPDATE_VALIDATION_CONTEXT = "update";
    private static final Logger log = LogManager.getLogger(ValidationContextSupport.class);

    private final Map<String, Object> cache;
    private final ValidationRequestConfigurationSupport configuration;
    private final DecoratorService decoratorService;
    private List<?> editReferentielList;

    protected ValidationContextSupport(ValidationRequestConfigurationSupport configuration, DecoratorService decoratorService) {
        this.configuration = configuration;
        this.decoratorService = decoratorService;
        this.cache = new TreeMap<>();
    }

    public abstract IdProject getSelectModel();

    public final DecoratorService getDecoratorService() {
        return decoratorService;
    }

    public final ReferentialLocale getReferentialLocale() {
        return decoratorService.getReferentialLocale();
    }

    @Override
    public Locale getLocale() {
        return getReferentialLocale().getLocale();
    }

    public ValidationRequestConfigurationSupport getConfiguration() {
        return configuration;
    }

    public Date getNow() {
        return new Date();
    }

    public Date getEndOfDay() {
        //FIXME:Performance Il faudrait cacher cette valeur
        return Dates.getEndOfDay(getNow());
    }

    public void add(IdDto dto) {
        getCache().put(dto.getId(), dto);
    }

    public void remove(String id) {
        getCache().remove(id);
    }

    public Map<String, Object> getCache() {
        return cache;
    }

    public void reset() {
        getCache().clear();
        editReferentielList = null;
    }

    public final List<?> getEditingReferentielList() {
        return editReferentielList;
    }

    public final void setEditingReferentielList(List<?> referentielList) {
        this.editReferentielList = referentielList;
    }

    protected <D extends IdDto> D getDto(Function<String, D> function, String id) {
        if (id == null) {
            return null;
        }
        @SuppressWarnings("unchecked")
        D result = (D) getCache().get(id);
        if (result == null) {
            result = function.apply(id);
            if (result != null) {
                log.info(String.format("Put '%s' into cache.", id));
                getCache().put(id, result);
            }
        }
        return result;
    }

    protected Set<String> getIds(Function<String, Set<String>> function, String id) {
        if (id == null) {
            return null;
        }
        @SuppressWarnings("unchecked")
        Set<String> result = (Set<String>) getCache().get(id);
        if (result == null) {
            result = function.apply(id);
            if (result != null) {
                log.info(String.format("Put '%s' into cache.", id));
                getCache().put(id, result);
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    protected <D> Function<String, D> function() {
        return s -> (D) getCache().get(s);
    }

    @Override
    public final void close() {
    }
}
