package fr.ird.observe.validation.api.request;

/*-
 * #%L
 * Toolkit :: API :: Validation
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Set;

/**
 * Created on 30/08/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.1.0
 */
public abstract class ValidationRequestSupport implements JsonAware {

    private Set<NuitonValidatorScope> scopes;
    private String validationContext;

    public Set<NuitonValidatorScope> getScopes() {
        return scopes;
    }

    public void setScopes(Set<NuitonValidatorScope> scopes) {
        this.scopes = scopes;
    }

    public String getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(String validationContext) {
        this.validationContext = validationContext;
    }
}
