package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.decoration.DecoratorDefinition;

import java.util.Locale;
import java.util.Set;

/**
 * Created on 22/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class ToolkitIdLabelDecoratorDefinition extends DecoratorDefinition<ToolkitIdLabel, ToolkitIdLabelDecoratorRenderer> {

    private final Class<? extends BusinessDto> dtoType;

    public ToolkitIdLabelDecoratorDefinition(Class<? extends BusinessDto> dtoType, String classifier, Set<String> codeProperties, String... properties) {
        super(ToolkitIdLabel.class, dtoType.getName() + (classifier == null ? "" : ("-" + classifier)), DEFAULT_CONTEXT_SEPARATOR, DEFAULT_RENDERER_SEPARATOR, new ToolkitIdLabelDecoratorRenderer(codeProperties), properties);
        this.dtoType = dtoType;
    }

    public Class<? extends BusinessDto> dtoType() {
        return dtoType;
    }

    @Override
    public String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (contextCount()) {
            case 1:
                return decoratorForContextCountEquals1(locale, source, rendererSeparator, index);
            case 2:
                return decoratorForContextCountEquals2(locale, source, rendererSeparator, index);
            case 3:
                return decoratorForContextCountEquals3(locale, source, rendererSeparator, index);
            case 4:
                return decoratorForContextCountEquals4(locale, source, rendererSeparator, index);
            case 5:
                return decoratorForContextCountEquals5(locale, source, rendererSeparator, index);
            case 6:
                return decoratorForContextCountEquals6(locale, source, rendererSeparator, index);
            case 7:
                return decoratorForContextCountEquals7(locale, source, rendererSeparator, index);
            case 8:
                return decoratorForContextCountEquals8(locale, source, rendererSeparator, index);
            case 9:
                return decoratorForContextCountEquals9(locale, source, rendererSeparator, index);
            case 10:
                return decoratorForContextCountEquals10(locale, source, rendererSeparator, index);
            case 11:
                return decoratorForContextCountEquals11(locale, source, rendererSeparator, index);
            case 12:
                return decoratorForContextCountEquals12(locale, source, rendererSeparator, index);
            case 13:
                return decoratorForContextCountEquals13(locale, source, rendererSeparator, index);
            case 14:
                return decoratorForContextCountEquals14(locale, source, rendererSeparator, index);
            case 15:
                return decoratorForContextCountEquals15(locale, source, rendererSeparator, index);
            case 16:
                return decoratorForContextCountEquals16(locale, source, rendererSeparator, index);
            case 17:
                return decoratorForContextCountEquals17(locale, source, rendererSeparator, index);
            case 18:
                return decoratorForContextCountEquals18(locale, source, rendererSeparator, index);
            case 19:
                return decoratorForContextCountEquals19(locale, source, rendererSeparator, index);
            case 20:
                return decoratorForContextCountEquals20(locale, source, rendererSeparator, index);
            default:
                throw new IllegalStateException("Context count is too high, can only manage from 1 to 20, but was " + contextCount());
        }
    }

    @Override
    public String decorateContext(Locale locale, ToolkitIdLabelDecoratorRenderer renderer, ToolkitIdLabel source, int index) {
        ensureContextIndex(index);
        return source.getText(index);
    }

}
