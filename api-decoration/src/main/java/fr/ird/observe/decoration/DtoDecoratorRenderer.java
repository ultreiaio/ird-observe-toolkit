package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.reference.ReferentialDtoReference;

import java.util.Locale;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class DtoDecoratorRenderer<O extends BusinessDto> extends JavaBeanDecoratorRenderer<O> {

    public DtoDecoratorRenderer(Class<O> type) {
        super(type);
    }

    public String label(Locale locale, ReferentialDtoReference source) {
        return source == null ? onNullValue(locale, null) : source.getLabel();
    }

    public String labelOrUnknown(Locale locale, ReferentialDtoReference source) {
        return source == null ? onUnknownValue(locale, null) : source.getLabel();
    }
}
