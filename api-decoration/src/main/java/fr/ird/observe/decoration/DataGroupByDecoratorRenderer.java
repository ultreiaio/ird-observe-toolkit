package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.data.DataGroupBy;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.i18n.I18n;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created on 23/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class DataGroupByDecoratorRenderer<O extends DataGroupBy<?>> extends JavaBeanDecoratorRenderer<O> {
    protected static final Set<String> CODE_PROPERTIES = Set.of(DataGroupBy.PROPERTY_FILTER_TEXT);

    protected static final Set<Locale> NOT_DATE_NATURAL_SORT_ORDER = ReferentialLocale.initNotDateNaturalSortOrder();

    public static boolean isLocaleNotDateNaturalSortOrder(Locale locale) {
        return NOT_DATE_NATURAL_SORT_ORDER.contains(locale);
    }

    private final boolean temporal;

    public DataGroupByDecoratorRenderer(DataGroupByDefinition<?, O> definition) {
        super(definition.getContainerType());
        if (definition.isQualitative()) {
            // qualitative data can not follow String natural order, they are referential, so use to code strategy
            setCodeProperties(CODE_PROPERTIES);
        }
        this.temporal = definition.isTemporal();
    }

    public String decorateDefinitionLabel(Locale locale, O source) {
        return onNullValue("definitionLabel", locale, source.getDefinitionLabel(locale));
    }

    public String decorateFilterText(Locale locale, O source) {
        String filterText = source.getFilterText();
        if (source.isNull()) {
            // the service just gave us the i18n key
            filterText = I18n.l(locale, filterText);
        } else {
            if (temporal) {
                // use correct locale to display the temporal value
                filterText = translateTemporal(locale, source.getFilterValue());
            }
        }
        return onNullValue(DataGroupBy.PROPERTY_FILTER_TEXT, locale, filterText);
    }

    @Override
    protected String sortByCodeFunction(String code) {
        int i = code.indexOf("-");
        return super.sortByCodeFunction(i == -1 ? code : code.substring(0, i - 1));
    }

    @Override
    protected void defaultSort(String propertyName, DecoratorDefinition<O, ?> definition, Locale locale, int pos, List<O> dataList) {
        if (!dataList.isEmpty() && DataGroupBy.PROPERTY_FILTER_TEXT.equals(propertyName) && temporal && isLocaleNotDateNaturalSortOrder(locale)) {
            // Need to translate to a natural sort order (says english one)
            sortByNotDateNaturalSortOrderTemporal(dataList);
        } else {
            super.defaultSort(propertyName, definition, locale, pos, dataList);
        }
    }

    protected void sortByNotDateNaturalSortOrderTemporal(List<O> dataList) {
        Multimap<String, O> dataToDate = ArrayListMultimap.create();
        for (O data : dataList) {
            String o = data.getFilterText();
            dataToDate.put(o == null ? null : translateTemporalToNaturalSortOrder(o), data);
        }
        sortByString(dataList, dataToDate);
    }

    protected String translateTemporal(Locale locale, String value) {
        if (isLocaleNotDateNaturalSortOrder(locale)) {
            List<String> split = Arrays.asList(value.split("-"));
            switch (split.size()) {
                case 1:
                    return value;
                case 2:
                case 3:
                    Collections.reverse(split);
                    return String.join("/", split);
                default:
                    throw new IllegalStateException("Can't manage here date: " + value);
            }
        }
        return value;
    }

    protected String translateTemporalToNaturalSortOrder(String value) {
        List<String> split = Arrays.asList(value.split("/"));
        switch (split.size()) {
            case 1:
                return value;
            //FIXME We may left pad day and mouth components ?
            case 2:
            case 3:
                Collections.reverse(split);
                return String.join("-", split);
            default:
                throw new IllegalStateException("Can't manage here date: " + value);
        }
    }
}
