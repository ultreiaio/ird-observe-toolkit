package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.stats.StatisticValue;
import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.bean.definition.JavaBeanPropertyDefinition;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.lang.Strings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 02/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public class JavaBeanDecoratorRenderer<O extends JavaBean> extends DefaultDecoratorRenderer<O> {

    private JavaBeanDefinition javaBeanDefinition;

    private Set<String> codeProperties = Set.of("code");

    public JavaBeanDecoratorRenderer(Class<O> type) {
        super(type);
    }

    public Set<String> getCodeProperties() {
        return codeProperties;
    }

    public void setCodeProperties(Set<String> codeProperties) {
        this.codeProperties = Objects.requireNonNull(codeProperties);
    }

    public String stat(Locale locale, StatisticValue source) {
        return source == null ? onNullValue(locale, null) : source.getValueWithSymbol();
    }

    @Override
    public void sort(DecoratorDefinition<O, ?> definition, Locale locale, int pos, List<O> dataList) {
        String propertyName = definition.properties().get(pos);
        if (getCodeProperties().contains(propertyName)) {
            sortByCode(propertyName, dataList);
            return;
        }
        if (isDateOrTimestamp(propertyName)) {
            // sort on date, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy)
            sortByDate(propertyName, dataList);
            return;
        }
        JavaBeanPropertyDefinition<JavaBean, Object> propertyDefinition = getJavaBeanDefinition().readProperty(propertyName);
        if (Number.class.isAssignableFrom(propertyDefinition.type()) ||
                int.class.isAssignableFrom(propertyDefinition.type()) ||
                float.class.isAssignableFrom(propertyDefinition.type()) ||
                long.class.isAssignableFrom(propertyDefinition.type())) {
            sortByCode(propertyName, dataList);
            return;
        }
        if (StatisticValue.class.isAssignableFrom(propertyDefinition.type())) {
            sortByStat(propertyName, dataList);
            return;
        }
        defaultSort(propertyName, definition, locale, pos, dataList);
    }

    @Override
    public Comparator<O> getComparator(DecoratorDefinition<O, ?> definition, Locale locale, int pos) {
        String propertyName = definition.properties().get(pos);
        if (getCodeProperties().contains(propertyName)) {
            return Comparator.comparing(d -> {
                Object context = d.get(propertyName);
                if (context == null) {
                    return null;
                }
                String text = String.valueOf(context);
                return sortByCodeFunction(text);
            });
        }
        if (isTemporal(propertyName)) {
            // sort on date, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy)
            return Comparator.comparing(d -> ((Date) d.get(propertyName)).getTime());
        }
        JavaBeanPropertyDefinition<JavaBean, Object> propertyDefinition = getJavaBeanDefinition().readProperty(propertyName);
        if (Number.class.isAssignableFrom(propertyDefinition.type()) ||
                int.class.isAssignableFrom(propertyDefinition.type()) ||
                float.class.isAssignableFrom(propertyDefinition.type()) ||
                long.class.isAssignableFrom(propertyDefinition.type())) {
            return Comparator.comparing(d -> d.get(propertyName));
        }
        if (StatisticValue.class.isAssignableFrom(propertyDefinition.type())) {
            return Comparator.comparingLong(d -> ((StatisticValue) d.get(propertyName)).getValue());
        }
        return Comparator.comparing(d -> definition.decorate(locale, d, pos));
    }

    protected void defaultSort(String propertyName, DecoratorDefinition<O, ?> definition, Locale locale, int pos, List<O> dataList) {
        super.sort(definition, locale, pos, dataList);
    }

    protected void sortByDate(String propertyName, List<O> dataList) {
        Multimap<Long, O> dataToDate = ArrayListMultimap.create();
        for (O data : dataList) {
            Date o = data.get(propertyName);
            if (o == null) {
                dataToDate.put(null, data);
            } else {
                dataToDate.put(o.getTime(), data);
            }
        }
        sortByLong(dataList, dataToDate);
    }

    protected void sortByCode(String propertyName, List<O> dataList) {
        Multimap<String, O> dataToDate = ArrayListMultimap.create();
        for (O data : dataList) {
            Object o = data.get(propertyName);
            dataToDate.put(o == null ? null : o.toString(), data);
        }
        sortByCode(dataList, dataToDate);
    }

    protected void sortByStat(String propertyName, List<O> dataList) {
        Multimap<Long, O> dataToDate = ArrayListMultimap.create();
        for (O data : dataList) {
            StatisticValue o = data.get(propertyName);
            dataToDate.put(o == null ? null : o.getValue(), data);
        }
        sortByLong(dataList, dataToDate);
    }

    protected void sortByLong(List<O> dataList, Multimap<Long, O> dataToDate) {
        List<Long> dates = new ArrayList<>(dataToDate.keySet());
        boolean withNull = dates.removeIf(Objects::isNull);
        dates.sort(Long::compareTo);
        if (withNull) {
            dates.add(0, null);
        }
        dataList.clear();
        for (Long date : dates) {
            dataList.addAll(dataToDate.get(date));
        }
    }

    protected void sortByCode(List<O> dataList, Multimap<String, O> dataToDate) {
        Multimap<String, O> dataToDate2 = ArrayListMultimap.create();
        for (Map.Entry<String, Collection<O>> entry : dataToDate.asMap().entrySet()) {
            String o = entry.getKey();
            if (o != null) {
                o = sortByCodeFunction(o);
            }
            dataToDate2.putAll(o, entry.getValue());
        }
        sortByString(dataList, dataToDate2);
    }

    protected String sortByCodeFunction(String code) {
        return Strings.leftPad(code, 6, "0");
    }

    protected void sortByString(List<O> dataList, Multimap<String, O> dataToDate) {
        List<String> dates = new ArrayList<>(dataToDate.keySet());
        boolean withNull = dates.removeIf(Objects::isNull);
        dates.sort(String::compareTo);
        if (withNull) {
            dates.add(0, null);
        }
        dataList.clear();
        for (String date : dates) {
            dataList.addAll(dataToDate.get(date));
        }
    }

    protected JavaBeanDefinition getJavaBeanDefinition() {
        if (javaBeanDefinition == null) {
            javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(type()).orElseThrow();
        }
        return javaBeanDefinition;
    }

}
