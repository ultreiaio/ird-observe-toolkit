package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.ToolkitIdLabel;
import fr.ird.observe.dto.data.DataGroupByDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import fr.ird.observe.dto.reference.DataGroupByDtoSet;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.dto.reference.DtoReferenceCollection;
import fr.ird.observe.dto.referential.ReferentialLocale;
import io.ultreia.java4all.decoration.Decorated;
import io.ultreia.java4all.decoration.Decorator;
import io.ultreia.java4all.decoration.DecoratorProvider;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class DecoratorService {

    public static final String LONG_CLASSIFIER = "Long";
    public static final String WITH_STATS_CLASSIFIER = "WithStats";
    public static final String FOR_SEARCH_CLASSIFIER = "ForSearch";
    private static DecoratorProvider PROVIDER;
    private ReferentialLocale referentialLocale;

    public static synchronized DecoratorProvider provider() {
        return PROVIDER == null ? PROVIDER = DecoratorProvider.get() : PROVIDER;
    }

    public DecoratorService(ReferentialLocale referentialLocale) {
        this.referentialLocale = Objects.requireNonNull(referentialLocale);
    }

    public ReferentialLocale getReferentialLocale() {
        return referentialLocale;
    }

    public void setReferentialLocale(ReferentialLocale referentialLocale) {
        this.referentialLocale = Objects.requireNonNull(referentialLocale);
    }

    public <O> Optional<Decorator> optionalDecoratorByType(Class<O> type, String classifier) {
        return provider().optionalDecorator(referentialLocale.getLocale(), type, classifier);
    }

    public <O> Decorator getDecoratorByType(Class<O> type) {
        return provider().decorator(referentialLocale.getLocale(), type);
    }

    public <O> Decorator getDecoratorByType(Class<O> type, String classifier) {
        return provider().decorator(referentialLocale.getLocale(), type, classifier);
    }

    public <O> Decorator getToolkitIdLabelDecoratorByType(Class<O> type) {
        return provider().decorator(referentialLocale.getLocale(), ToolkitIdLabel.class, type.getName());
    }

    @SuppressWarnings("unchecked")
    public <O extends Decorated> void installDecorator(O data) {
        installDecorator((Class<O>) data.getClass(), data);
    }

    public <O extends Decorated> void installDecorator(Class<O> type, O data) {
//        if (data.decorator().isEmpty()) {
        Decorator decorator = getDecoratorByType(type);
        data.registerDecorator(decorator);
//        }
    }

    public <O extends Decorated> void installDecorator(Class<O> type, Stream<O> stream) {
        Decorator decorator = getDecoratorByType(type);
        stream.forEach(r -> r.registerDecorator(decorator));
    }

    public <O extends Decorated> void installDecorator(Class<O> type, String classifier, Stream<O> stream) {
        Decorator decorator = getDecoratorByType(type, classifier);
        stream.forEach(r -> r.registerDecorator(decorator));
    }

    public <O extends Decorated> void installToolkitIdLabelDecorator(Class<O> type, ToolkitIdLabel data) {
//        if (data.decorator().isEmpty()) {
        Decorator decorator = getToolkitIdLabelDecoratorByType(type);
        data.registerDecorator(decorator);
//        }
    }

    public <O extends Decorated> void installToolkitIdLabelDecorator(Class<O> type, Stream<ToolkitIdLabel> stream) {
        Decorator decorator = getToolkitIdLabelDecoratorByType(type);
        stream.forEach(r -> r.registerDecorator(decorator));
    }

    public <D extends RootOpenableDto, R extends DataGroupByDto<D>> void installDecorator(DataGroupByDtoSet<D, R> referenceSet) {
        installDecorator(referenceSet.getType(), referenceSet.stream());
    }

    public <D extends RootOpenableDto, R extends DataGroupByDto<D>> void installDecoratorForSearch(DataGroupByDtoSet<D, R> referenceSet) {
        installDecorator(referenceSet.getType(), FOR_SEARCH_CLASSIFIER, referenceSet.stream());
    }

    public <R extends DtoReference> void installDecorator(DtoReferenceCollection<R> referenceSet) {
        installDecorator(referenceSet.getType(), referenceSet.stream());
    }

    public <R extends DtoReference> void installDecorator(DtoReferenceCollection<R> referenceSet, String classifier) {
        installDecorator(referenceSet.getType(), classifier, referenceSet.stream());
    }
}
