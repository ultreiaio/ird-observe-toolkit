package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataGroupBy;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.decoration.DecoratorProvider;
import io.ultreia.java4all.decoration.DecoratorProviderInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created on 23/10/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.16
 */
public class DecoratorProviderInitializerHelper {
    private static final Logger log = LogManager.getLogger(DecoratorProviderInitializerHelper.class);

    public static void init(DecoratorProviderInitializer initializer,
                            DecoratorProvider provider,
                            Supplier<List<DecoratorDefinition<?, ?>>> listSupplier,
                            Supplier<Stream<? extends DataGroupByDefinition<?, ?>>> groupByDefinitionsSupplier) {
        log.info("Initializing... {}", initializer);

        List<DecoratorDefinition<?, ?>> list = listSupplier.get();

        int count = DecoratorProviderInitializerHelper.registerGroupByDecorators(provider, list, groupByDefinitionsSupplier.get());
        log.info("Initialization done {}, register {} DataGroupByDecoratorDefinition(s).", initializer, count);

        int index = DecoratorProviderInitializerHelper.registerToolkitLabels(provider, initializer.codeProperties(), list);
        log.info("Initialization done {}, register {} ToolkitIdLabelDecoratorDefinition(s).", initializer, index);
    }

    public static int registerGroupByDecorators(DecoratorProvider provider, List<DecoratorDefinition<?, ?>> list, Stream<? extends DataGroupByDefinition<?, ?>> defs) {
        AtomicInteger index = new AtomicInteger();
        defs.forEach(definition -> {
            DataGroupByDecoratorDefinition<?> decoratorDefinition = registerGroupByDefinition(provider, definition);
            list.add(decoratorDefinition);
            index.incrementAndGet();
            DataGroupByDecoratorDefinitionForSearch<?> decoratorDefinitionForSearch = registerGroupByDefinitionForSearch(provider, definition);
            list.add(decoratorDefinitionForSearch);
            index.incrementAndGet();
        });
        return index.get();
    }

    public static int registerToolkitLabels(DecoratorProvider provider, Set<String> codeProperties, List<DecoratorDefinition<?, ?>> list) {
        AtomicInteger index = new AtomicInteger();
        list.forEach(definition -> {
            @SuppressWarnings("unchecked") DecoratorDefinition<? extends BusinessDto, ?> typedDefinition = (DecoratorDefinition<? extends BusinessDto, ?>) definition;
            DecoratorProviderInitializerHelper.registerToolkitLabelDefinition(provider, codeProperties, typedDefinition);
            index.incrementAndGet();
        });
        return index.get();
    }

    static <O, F extends DataGroupBy<O>> DataGroupByDecoratorDefinition<F> registerGroupByDefinition(DecoratorProvider provider, DataGroupByDefinition<O, F> type) {
        DataGroupByDecoratorDefinition<F> definition = new DataGroupByDecoratorDefinition<>(type);
        provider.addDefinition(definition);
        return definition;
    }

    static <O, F extends DataGroupBy<O>> DataGroupByDecoratorDefinitionForSearch<F> registerGroupByDefinitionForSearch(DecoratorProvider provider, DataGroupByDefinition<O, F> type) {
        DataGroupByDecoratorDefinitionForSearch<F> definition = new DataGroupByDecoratorDefinitionForSearch<>(type);
        provider.addDefinition(definition);
        return definition;
    }

    static void registerToolkitLabelDefinition(DecoratorProvider provider, Set<String> codeProperties, DecoratorDefinition<? extends BusinessDto, ?> definition) {
        Class<? extends BusinessDto> type = definition.type();
        List<String> properties = definition.properties();
        provider.addDefinition(new ToolkitIdLabelDecoratorDefinition(type, definition.classifier(), codeProperties, properties.toArray(new String[0])));
    }


}
