package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.referential.I18nReferentialHelper;
import fr.ird.observe.dto.referential.WithI18n;
import io.ultreia.java4all.decoration.DecoratorRenderer;
import io.ultreia.java4all.i18n.I18n;

import java.util.Date;
import java.util.Locale;

/**
 * Created on 20/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class DefaultDecoratorRenderer<O> extends DecoratorRenderer<O> {

    public DefaultDecoratorRenderer(Class<O> type) {
        super(type);
    }

    @Override
    public String onNullValue(String propertyName, Locale locale, Object value) {
        if (value == null) {
            return I18n.l(locale, "observe.Common.none");
        }
        return value.toString();
    }

    public String onUnit(Locale locale, String value) {
        if (value == null) {
            return I18n.l(locale, "observe.Common.no.unit");
        }
        return value;
    }

    public String onHomeId(Locale locale, String value) {
        if (value == null || value.isEmpty()) {
            return I18n.l(locale, "observe.Common.no.homeId");
        }
        return value;
    }

    @Override
    public String onNullValue(Locale locale, Object value) {
        if (value == null) return I18n.l(locale, "observe.Common.none");
        return value.toString();
    }

    public String label(Locale locale, WithI18n source) {
        return source == null ? onNullValue(locale, null) : I18nReferentialHelper.getLabel(locale, source);
    }

    public String labelOrUnknown(Locale locale, WithI18n source) {
        return source == null ? onUnknownValue(locale, null) : I18nReferentialHelper.getLabel(locale, source);
    }

    public static String onUnknownValue(Locale locale, Object value) {
        if (value == null) return I18n.l(locale, "observe.Common.unknown");
        return value.toString();
    }

    public String noCode(Locale locale, Object value) {
        if (value == null || value.toString().isBlank()) return I18n.l(locale, "observe.Common.nocode");
        return value.toString();
    }

    public String date(Locale locale, Date time) {
        return time == null ? onNullValue(locale, null) : I18nDecoratorHelper.getDateLabel(locale, time);
    }

    public String niceDate(Locale locale, Date time) {
        if (time == null) {
            return I18n.l(locale, "observe.Common.no.date");
        }
        return date(locale, time);
    }

    public String niceTime(Locale locale, Date time) {
        if (time == null) {
            return I18n.l(locale, "observe.Common.no.time");
        }
        return time(locale, time);
    }

    public String time(Locale locale, Date time) {
        return time == null ? onNullValue(locale, null) : I18nDecoratorHelper.getTimeLabel(locale, time);
    }

    public String timestamp(Locale locale, Date time) {
        return time == null ? onNullValue(locale, null) : I18nDecoratorHelper.getTimestampLabel(locale, time);
    }

    protected boolean isDateOrTimestamp(String propertyName) {
        return isDate(propertyName) || isTimestamp(propertyName);
    }

    protected boolean isDate(String propertyName) {
        return propertyName.toLowerCase().contains("date");
    }

    protected boolean isTimestamp(String propertyName) {
        return propertyName.toLowerCase().contains("timestamp");
    }

    protected boolean isTemporal(String propertyName) {
        return isDate(propertyName) || isTimestamp(propertyName) || propertyName.toLowerCase().contains("time");
    }

}
