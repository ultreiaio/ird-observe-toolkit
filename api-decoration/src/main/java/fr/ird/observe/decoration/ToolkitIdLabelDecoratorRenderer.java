package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.dto.ToolkitIdLabel;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created on 22/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.39
 */
public class ToolkitIdLabelDecoratorRenderer extends JavaBeanDecoratorRenderer<ToolkitIdLabel> {
    private static final Logger log = LogManager.getLogger(ToolkitIdLabelDecoratorRenderer.class);

    public ToolkitIdLabelDecoratorRenderer(Set<String> codeProperties) {
        super(ToolkitIdLabel.class);
        setCodeProperties(codeProperties);
    }

    @Override
    public void sort(DecoratorDefinition<ToolkitIdLabel, ?> definition, Locale locale, int pos, List<ToolkitIdLabel> dataList) {
        String propertyName = definition.properties().get(pos);
        if (getCodeProperties().contains(propertyName)) {
            sortByCode(pos, dataList);
            return;
        }
        if (isDate(propertyName)) {
            // sort on date, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy)
            sortByDate(pos, I18nDecoratorHelper.newDateFormat(locale), dataList);
            return;
        }
        if (isTimestamp(propertyName)) {
            // sort on timestamp, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy HH:mm)
            sortByDate(pos, I18nDecoratorHelper.newTimestampFormat(locale), dataList);
            return;
        }
        // always sort as using natural order from numbers
        sortByCode(pos, dataList);
    }

    @Override
    public Comparator<ToolkitIdLabel> getComparator(DecoratorDefinition<ToolkitIdLabel, ?> definition, Locale locale, int pos) {
        String propertyName = definition.properties().get(pos);
        if (isDate(propertyName)) {

            // sort on date, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy)
            SimpleDateFormat dateFormat = I18nDecoratorHelper.newDateFormat(locale);
            return Comparator.comparingLong(d -> {
                String o = d.getText(pos);
                try {
                    return dateFormat.parse(o).getTime();
                } catch (ParseException e) {
                    return 0;
                }

            });
        }
        if (isTimestamp(propertyName)) {
            // sort on timestamp, can not use toString render to sort (wrong order when using date pattern dd/MM/yyyy HH:mm)
            SimpleDateFormat dateFormat = I18nDecoratorHelper.newTimestampFormat(locale);
            return Comparator.comparingLong(d -> {
                String o = d.getText(pos);
                try {
                    return dateFormat.parse(o).getTime();
                } catch (ParseException e) {
                    return 0;
                }

            });
        }
        return Comparator.comparing(d -> {
            String o = d.getText(pos);
            return sortByCodeFunction(o);

        });
    }

    protected void sortByDate(int pos, SimpleDateFormat dateFormat, List<ToolkitIdLabel> dataList) {
        Multimap<Long, ToolkitIdLabel> dataToDate = ArrayListMultimap.create();
        for (ToolkitIdLabel data : dataList) {
            String o = data.getText(pos);
            if (o == null) {
                dataToDate.put(null, data);
            } else {

                try {
                    dataToDate.put(dateFormat.parse(o).getTime(), data);
                } catch (ParseException e) {
                    log.error("Could not parse date: " + o, e);
                    dataToDate.put(null, data);
                }
            }
        }
        sortByLong(dataList, dataToDate);
    }

    protected void sortByCode(int pos, List<ToolkitIdLabel> dataList) {
        Multimap<String, ToolkitIdLabel> dataToDate = ArrayListMultimap.create();
        for (ToolkitIdLabel data : dataList) {
            String o = data.getText(pos);
            dataToDate.put(o, data);
        }
        sortByCode(dataList, dataToDate);
    }

}
