package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import fr.ird.observe.dto.reference.DtoReference;
import fr.ird.observe.spi.module.BusinessProject;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.decoration.DecoratorKey;
import io.ultreia.java4all.decoration.DecoratorProvider;
import io.ultreia.java4all.decoration.DecoratorProviderInitializer;
import io.ultreia.java4all.util.SingletonSupplier;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 22/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class DtoDecoratorProviderInitializer implements DecoratorProviderInitializer {

    protected abstract BusinessProject businessProject();

    @Override
    public void init(DecoratorProvider provider) {
        SingletonSupplier<List<DecoratorDefinition<?, ?>>> listSupplier = SingletonSupplier.of(() -> {
            Map<DecoratorKey<?>, DecoratorDefinition<?, ?>> definitions = provider.definitions();
            return new LinkedList<>(definitions.values()).stream().filter(d -> d.classifier() == null && BusinessDto.class.isAssignableFrom(d.type()) && !DtoReference.class.isAssignableFrom(d.type())).collect(Collectors.toList());
        });
        SingletonSupplier<Stream<? extends DataGroupByDefinition<?, ?>>> groupByDefinitionsSupplier = SingletonSupplier.of(() -> businessProject().getGroupByDefinitionsByModuleNames().values().stream().flatMap(List::stream));
        DecoratorProviderInitializerHelper.init(this, provider, listSupplier, groupByDefinitionsSupplier);
    }

}
