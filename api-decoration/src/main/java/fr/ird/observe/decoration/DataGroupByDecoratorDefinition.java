package fr.ird.observe.decoration;

/*-
 * #%L
 * Toolkit :: API :: Decoration
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.DataGroupBy;
import fr.ird.observe.dto.data.DataGroupByDefinition;
import io.ultreia.java4all.decoration.DecoratorDefinition;

import java.util.Locale;

/**
 * Created on 02/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class DataGroupByDecoratorDefinition<O extends DataGroupBy<?>> extends DecoratorDefinition<O, DataGroupByDecoratorRenderer<O>> {

    public DataGroupByDecoratorDefinition(DataGroupByDefinition<?, O> definition) {
        super(definition.getContainerType(), null, DEFAULT_CONTEXT_SEPARATOR, DEFAULT_RENDERER_SEPARATOR, new DataGroupByDecoratorRenderer<>(definition), "definitionLabel", DataGroupBy.PROPERTY_FILTER_TEXT);
    }

    @Override
    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        return decoratorForContextCountEquals2(locale, source, rendererSeparator, index);
    }

    @Override
    public final String decorateContext(Locale locale, DataGroupByDecoratorRenderer<O> renderer, O source, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return renderer.decorateDefinitionLabel(locale, source);
            case 1:
                return renderer.decorateFilterText(locale, source);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }


}
