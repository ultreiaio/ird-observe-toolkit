package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.application.context.ApplicationComponent;
import io.ultreia.java4all.application.context.ApplicationContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

public class TestSupportWithConfig {

    @BeforeClass
    public static void beforeClass() throws IOException {
        Path basedir = new File(new File("").getAbsolutePath()).toPath();
        File templateDirectory = basedir
                .resolve("src")
                .resolve("test")
                .resolve("i18n")
                .resolve("templates")
                .toFile();
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
        new ApplicationContext() {
            @Override
            protected void registerComponents(Map<Class<?>, ApplicationComponent<?>> componentMap) {
                TestConfigApplicationComponent component = (TestConfigApplicationComponent) componentMap.get(TestConfig.class);
                component.get().setTemplatesDirectory(templateDirectory);
                super.registerComponents(componentMap);
            }
        };
    }


    @AfterClass
    public static void afterClass() {
        if (ApplicationContext.isInit()) {
            ApplicationContext.get().close();
        }
    }
}
