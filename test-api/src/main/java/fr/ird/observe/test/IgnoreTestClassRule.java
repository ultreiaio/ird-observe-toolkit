package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Created on 02/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.11
 */
public class IgnoreTestClassRule implements TestRule {

    private final String name;
    private final boolean ignore;
    private final String propertyName;

    public IgnoreTestClassRule(String name, String propertyName) {
        this.name = name;
        this.ignore = Boolean.parseBoolean(System.getProperty(propertyName, "false"));
        this.propertyName = propertyName;
    }

    public boolean isIgnore() {
        return ignore;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        Assume.assumeFalse(String.format("Skip %s tests... (via system property: %s)", name, propertyName), ignore);
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                base.evaluate();
            }
        };
    }
}
