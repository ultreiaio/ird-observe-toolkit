package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.split.DataBaseBuilder;
import io.ultreia.java4all.util.sql.SqlScriptWriter;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum DatabaseName implements DataBaseBuilder {
    empty(DataBaseBuilder.EMPTY),
    referential(DataBaseBuilder.REFERENTIAL),
    data(DataBaseBuilder.DATA);

    private final DataBaseBuilder databaseBuilder;

    DatabaseName(DataBaseBuilder databaseBuilder) {
        this.databaseBuilder = databaseBuilder;
    }

    @Override
    public void generate(Path sourcePath, SqlScriptWriter writer) throws IOException {
        databaseBuilder.generate(sourcePath, writer);
    }

}
