package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseServerNameConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.conf.JdbcConfiguration;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;
import org.junit.Assume;

import java.lang.reflect.AnnotatedElement;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 29/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveTestConfiguration {

    public static final String TCK_CACHE_PATH = "observetest.tck.cache.path";
    public static final String MODEL_VERSION = "observetest.model.version";
    public static final String PREVIOUS_VERSION = "observetest.previous.model.version";
    public static final String FIRST_VERSION = "observetest.first.model.version";
    public static final String H2_LOGIN = "observetest.h2.login";
    public static final String H2_PASSWORD = "observetest.h2.password";
    public static final String PG_LOGIN = "observetest.pg.login";
    public static final String PG_PASSWORD = "observetest.pg.password";
    public static final String PG_URL = "observetest.pg.url";
    public static final String WEB_LOGIN = "observetest.web.login";
    public static final String WEB_PASSWORD = "observetest.web.password";
    public static final String WEB_URL = "observetest.web.url";
    public static final String WEB_DATABASE = "observetest.web.database";
    public static final String OBSTUNA_ADMIN_LOGIN = "observetest.obstuna.admin.login";
    public static final String OBSTUNA_TECHNICIEN_LOGIN = "observetest.obstuna.technicien.login";
    public static final String OBSTUNA_UTILISATEUR_LOGIN = "observetest.obstuna.utilisateur.login";
    public static final String OBSTUNA_REFERENTIEL_LOGIN = "observetest.obstuna.referentiel.login";

    public static Version getModelVersion() {
        return getTestPropertyAsVersion(MODEL_VERSION);
    }

    public static Version getPreviousModelVersion() {
        return getTestPropertyAsVersion(PREVIOUS_VERSION);
    }

    public static Version getFirstModelVersion() {
        return getTestPropertyAsVersion(FIRST_VERSION);
    }

    public static Version getTestPropertyAsVersion(String propertyName) {
        return Version.valueOf(ToolkitFixtures.getTestProperties().getProperty(propertyName));
    }

    public static String getTestPropertyAsString(String propertyName) {
        return ToolkitFixtures.getTestProperties().getProperty(propertyName);
    }

    public static URL getTestPropertyAsUrl(String propertyName) {
        String property = null;
        try {
            property = ToolkitFixtures.getTestProperties().getProperty(propertyName);
            return new URL(property);
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Can't init with url: " + property);
        }
    }

    public static char[] getTestPropertyAsCharArray(String propertyName) {
        return ToolkitFixtures.getTestProperties().getProperty(propertyName).toCharArray();
    }

    public static DatabaseUrlConfiguration getDatabaseUrlConfigurationAnnotation(AnnotatedElement annotatedElement) {
        return annotatedElement.getAnnotation(DatabaseUrlConfiguration.class);
    }

    public static DatabaseLoginConfiguration getDatabaseLoginConfigurationAnnotation(AnnotatedElement annotatedElement) {
        return annotatedElement.getAnnotation(DatabaseLoginConfiguration.class);
    }

    public static DatabasePasswordConfiguration getDatabasePasswordConfigurationAnnotation(AnnotatedElement annotatedElement) {
        return annotatedElement.getAnnotation(DatabasePasswordConfiguration.class);
    }

    public static DatabaseServerNameConfiguration getDatabaseServerNameConfigurationAnnotation(AnnotatedElement annotatedElement) {
        return annotatedElement.getAnnotation(DatabaseServerNameConfiguration.class);
    }

    public static DatabaseNameConfiguration getDatabaseNameConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {
        DatabaseNameConfiguration[] annotations = testClass.getAnnotationsByType(DatabaseNameConfiguration.class);
        for (DatabaseNameConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    public static DatabaseVersionConfiguration getDatabaseVersionConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {
        DatabaseVersionConfiguration[] annotations = testClass.getAnnotationsByType(DatabaseVersionConfiguration.class);
        for (DatabaseVersionConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    public static CopyDatabaseConfiguration getCopyDatabaseConfigurationAnnotation(AnnotatedElement testClass, DatabaseClassifier classifier) {
        CopyDatabaseConfiguration[] annotations = testClass.getAnnotationsByType(CopyDatabaseConfiguration.class);
        for (CopyDatabaseConfiguration annotation : annotations) {
            if (classifier.equals(annotation.classifier())) {
                return annotation;
            }
        }
        return null;
    }

    public static JdbcConfiguration pgConfiguration() {
        return new JdbcConfigurationBuilder().forDatabase(ObserveTestConfiguration.getTestPropertyAsString(ObserveTestConfiguration.PG_URL),
                                                          ObserveTestConfiguration.getTestPropertyAsString(ObserveTestConfiguration.PG_LOGIN),
                                                          ObserveTestConfiguration.getTestPropertyAsString(ObserveTestConfiguration.PG_PASSWORD)
        );
    }


    public static void injectCredentials() {

        Path credentialPath = Path.of(ObserveTestConfiguration.getTestPropertyAsString(ToolkitFixtures.TEST_CREDENTIALS_KEY));
        Assume.assumeTrue("Skip, no credential file found", Files.exists(credentialPath));
        ToolkitFixtures.injectCredentials(credentialPath,
                                          ObserveTestConfiguration.PG_URL,
                                          ObserveTestConfiguration.PG_LOGIN,
                                          ObserveTestConfiguration.PG_PASSWORD);
    }
}
