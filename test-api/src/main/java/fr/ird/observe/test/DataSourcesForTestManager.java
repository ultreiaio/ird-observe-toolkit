package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assume;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 26/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataSourcesForTestManager {

    private static final Logger log = LogManager.getLogger(DataSourcesForTestManager.class);

    /**
     * Root path of shared databases.
     */
    private static Path sharedDatabasesRootPath;

    /**
     * Root path of tck databases.
     */
    private static Path tckCachePath;

    public static void clearCache(Path path) throws IOException {
        log.info(String.format("Clear tck cache databases directory: %s", path));
        if (Files.exists(path)) {
            Files.newDirectoryStream(path).forEach(DataSourcesForTestManager::deleteDirectory);
            Files.delete(path);
        }
    }

    private static void deleteDirectory(Path directory) {
        try {
            log.debug(String.format("Delete directory: %s", directory));
            Files.walk(directory).filter(f -> !f.equals(directory)).forEach(f -> {
                try {
                    if (Files.isDirectory(f)) {
                        deleteDirectory(f);
                        return;
                    }
                    log.debug(String.format("Delete file: %s", f));
                    Files.delete(f);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });
            Files.delete(directory);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public Path getTckCachePath() {
        if (tckCachePath == null) {
            tckCachePath = Path.of(ObserveTestConfiguration.getTestPropertyAsString(ObserveTestConfiguration.TCK_CACHE_PATH));
        }
        return tckCachePath;
    }

    private Path getSharedDatabasesRootPath() {
        if (sharedDatabasesRootPath == null) {
            sharedDatabasesRootPath = ToolkitFixtures.getCommonsDir().toPath();
        }
        return sharedDatabasesRootPath;
    }

    public SqlScript getCache(Version dbVersion, String dbName) {
        Path scriptPath = getTckCachePath().resolve(dbVersion.getVersion()).resolve(dbName + ".sql.gz");
        if (Files.notExists(scriptPath)) {
            //noinspection ConstantConditions
            Assume.assumeFalse(String.format("Could not find tck database at: %s", scriptPath), true);
        }
        return SqlScript.of(scriptPath);
    }

    private Path getDatabasePath(Path rootPath, Version dbVersion, String dbName) {
        return rootPath.resolve(dbVersion.getValidName()).resolve(dbName);
    }

    public ObserveDataSourceConfigurationTopiaH2 createSharedDataSourceConfigurationH2(Version dbVersion, String dbName, String login, char[] password) {
        Path databasePath = getDatabasePath(getSharedDatabasesRootPath(), dbVersion, dbName);
        return createDataSourceConfigurationH2(databasePath.toFile(), dbVersion, dbName, login, password);
    }

    public ObserveDataSourceConfigurationTopiaH2 createDataSourceConfigurationH2(File localDbFile, Version dbVersion, String dbName, String login, char[] password) {
        ObserveDataSourceConfigurationTopiaH2 configurationTopiaH2 = new ObserveDataSourceConfigurationTopiaH2();
        log.debug("db directory: " + localDbFile);
        configurationTopiaH2.setLabel("Commons database#" + dbName);
        configurationTopiaH2.setLogin(login);
        configurationTopiaH2.setPassword(password);
        configurationTopiaH2.setDirectory(localDbFile);
        configurationTopiaH2.setDbName("obstuna");
        configurationTopiaH2.setAutoMigrate(true);
        configurationTopiaH2.setModelVersion(dbVersion);
        return configurationTopiaH2;
    }

    public void clear() throws IOException {
        Path path = getSharedDatabasesRootPath();
        log.info(String.format("Clear share databases directory: %s", path));
        if (Files.exists(path)) {
            Files.newDirectoryStream(path).forEach(DataSourcesForTestManager::deleteDirectory);
        }
    }

    public void clearCache() throws IOException {
        clearCache(getTckCachePath());
    }
}
