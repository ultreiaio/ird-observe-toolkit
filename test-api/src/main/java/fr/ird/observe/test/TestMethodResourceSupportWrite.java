package fr.ird.observe.test;

/*
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseServerNameConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.lang.reflect.Method;

/**
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class TestMethodResourceSupportWrite<A extends TestClassResourceSupport> implements TestRule {

    private static final Logger log = LogManager.getLogger(TestMethodResourceSupportWrite.class);

    protected final A testClassResource;
    protected Method testClassMethod;
    private File testDirectory;
    private String login;
    private char[] password;
    private String url;
    private Class<?> testClass;
    private String methodName;
    private String dbName;
    private String serverDbName;
    private Version dbVersion;

    protected TestMethodResourceSupportWrite(A testClassResource) {
        this.testClassResource = testClassResource;
    }

    @Override
    public final Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    public String getDbName() {
        return dbName;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public File getTestDirectory() {
        return testDirectory;
    }

    public String getLogin() {
        return login;
    }

    public char[] getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }

    public String getServerDbName() {
        return serverDbName;
    }

    public String getMethodName() {
        return methodName;
    }

    protected void before(Description description) throws Throwable {

        testClass = description.getTestClass();
        // with some parametrized tests, we can have some spaces in methods
        methodName = description.getMethodName().replaceAll("\\s*", "");
        log.debug("Starts " + testClass.getName() + "::" + methodName);

        testDirectory = testClassResource.initTemporaryDirectoryRoot(getMethodName());

        DatabaseLoginConfiguration databaseLogin = ObserveTestConfiguration.getDatabaseLoginConfigurationAnnotation(testClass);
        if (databaseLogin == null) {
            login = testClassResource.getLogin();
        } else {
            login = ObserveTestConfiguration.getTestPropertyAsString(databaseLogin.value());
        }

        DatabasePasswordConfiguration databasePassword = ObserveTestConfiguration.getDatabasePasswordConfigurationAnnotation(testClass);
        if (databasePassword == null) {
            password = testClassResource.getPassword();
        } else {
            password = ObserveTestConfiguration.getTestPropertyAsCharArray(databasePassword.value());
        }

        testClassMethod = testClass.getMethod(methodName.contains("[") ? methodName.substring(0, methodName.indexOf("[")) : methodName);

        DatabaseNameConfiguration databaseNameConfiguration = ObserveTestConfiguration.getDatabaseNameConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        if (databaseNameConfiguration != null) {
            dbName = databaseNameConfiguration.value().name();
        } else {
            dbName = testClassResource.getDbName();
        }
        if (Strings.isNullOrEmpty(dbName)) {
            dbName = null;
        }

        DatabaseVersionConfiguration databaseVersionConfiguration = ObserveTestConfiguration.getDatabaseVersionConfigurationAnnotation(testClassMethod, testClassResource.getClassifier());
        if (databaseVersionConfiguration == null) {
            dbVersion = testClassResource.getDbVersion();
        } else {
            dbVersion = ObserveTestConfiguration.getTestPropertyAsVersion(databaseVersionConfiguration.value());
        }

        DatabaseUrlConfiguration databaseUrl = ObserveTestConfiguration.getDatabaseUrlConfigurationAnnotation(testClassMethod);
        if (databaseUrl == null) {
            url = testClassResource.getUrl();
        } else {
            url = ObserveTestConfiguration.getTestPropertyAsString(databaseUrl.value());
        }

        DatabaseServerNameConfiguration databaseServerNameConfiguration = ObserveTestConfiguration.getDatabaseServerNameConfigurationAnnotation(testClassMethod);
        if (databaseServerNameConfiguration == null) {
            serverDbName = testClassResource.getServerDbName();
        } else {
            serverDbName = ObserveTestConfiguration.getTestPropertyAsString(databaseServerNameConfiguration.value());
        }
    }

    protected void after(Description description) {
        log.debug(String.format("Ends %s::%s", testClass.getName(), methodName));
        testClassResource.setTemporaryDirectoryRoot(null);
    }

}
