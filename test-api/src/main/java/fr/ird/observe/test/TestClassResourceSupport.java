package fr.ird.observe.test;

/*-
 * #%L
 * Toolkit :: Test API
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.spi.DatabaseClassifier;
import fr.ird.observe.test.spi.DatabaseLoginConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import fr.ird.observe.test.spi.DatabasePasswordConfiguration;
import fr.ird.observe.test.spi.DatabaseServerNameConfiguration;
import fr.ird.observe.test.spi.DatabaseUrlConfiguration;
import fr.ird.observe.test.spi.DatabaseVersionConfiguration;
import io.ultreia.java4all.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 29/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class TestClassResourceSupport implements TestRule {

    private static final Logger log = LogManager.getLogger(TestClassResourceSupport.class);
    protected final DataSourcesForTestManager dataSourcesForTestManager;
    private final DatabaseClassifier classifier;
    protected Path temporaryDirectoryRoot;
    private File testDirectory;
    private String login;
    private char[] password;
    private String dbName;
    private Version dbVersion;
    private String url;
    private String serverDbName;

    protected TestClassResourceSupport(DatabaseClassifier classifier) {
        this.classifier = classifier;
        dataSourcesForTestManager = new DataSourcesForTestManager();
    }

    public DataSourcesForTestManager getDataSourcesForTestManager() {
        return dataSourcesForTestManager;
    }

    public final void setTemporaryDirectoryRoot(Path temporaryDirectoryRoot) {
        this.temporaryDirectoryRoot = temporaryDirectoryRoot;
    }

    public Path getTemporaryDirectoryRoot() {
        return temporaryDirectoryRoot;
    }

    public String getLogin() {
        return login;
    }

    public char[] getPassword() {
        return password;
    }

    public String getDbName() {
        return dbName;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

    public DatabaseClassifier getClassifier() {
        return classifier;
    }

    public File getTestDirectory() {
        return testDirectory;
    }

    public String getServerDbName() {
        return serverDbName;
    }

    @Override
    public final Statement apply(final Statement base, final Description description) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    protected void before(Description description) throws Throwable {
        Class<?> testClass = description.getTestClass();
        log.debug(String.format("Starts %s", testClass.getName()));
        testDirectory = ToolkitFixtures.getTestBasedir(testClass).toPath().getParent().resolve(testClass.getName()).toFile();
        DatabaseLoginConfiguration databaseLogin = ObserveTestConfiguration.getDatabaseLoginConfigurationAnnotation(testClass);
        if (databaseLogin != null) {
            login = ObserveTestConfiguration.getTestPropertyAsString(databaseLogin.value());
        }

        DatabasePasswordConfiguration databasePassword = ObserveTestConfiguration.getDatabasePasswordConfigurationAnnotation(testClass);
        if (databasePassword != null) {
            password = ObserveTestConfiguration.getTestPropertyAsCharArray(databasePassword.value());
        }
        DatabaseNameConfiguration databaseNameConfiguration = ObserveTestConfiguration.getDatabaseNameConfigurationAnnotation(testClass, classifier);
        if (databaseNameConfiguration != null) {
            dbName = databaseNameConfiguration.value().name();
        }
        DatabaseVersionConfiguration databaseVersionConfiguration = ObserveTestConfiguration.getDatabaseVersionConfigurationAnnotation(testClass, classifier);
        if (databaseVersionConfiguration != null) {
            dbVersion = ObserveTestConfiguration.getTestPropertyAsVersion(databaseVersionConfiguration.value());
        }
        DatabaseUrlConfiguration databaseUrl = ObserveTestConfiguration.getDatabaseUrlConfigurationAnnotation(testClass);
        if (databaseUrl != null) {
            url = ObserveTestConfiguration.getTestPropertyAsString(databaseUrl.value());
        }
        DatabaseServerNameConfiguration databaseServerNameConfiguration = ObserveTestConfiguration.getDatabaseServerNameConfigurationAnnotation(testClass);
        if (databaseServerNameConfiguration != null) {
            serverDbName = ObserveTestConfiguration.getTestPropertyAsString(databaseServerNameConfiguration.value());
        }
    }

    protected void after(Description description) throws IOException {
        Class<?> testClass = description.getTestClass();
        log.debug(String.format("Ends %s", testClass.getName()));
        setTemporaryDirectoryRoot(null);
    }

    public File newFile(String methodName) {
        return new File(testDirectory, methodName.replace("[", "").replace("]", ""));
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public File initTemporaryDirectoryRoot(String name) throws IOException {
        File testDirectory = newFile(name);

        Path temporaryDirectoryRoot = new File(testDirectory, "tmp").toPath();

        Files.createDirectories(temporaryDirectoryRoot);
        setTemporaryDirectoryRoot(temporaryDirectoryRoot);
        return testDirectory;
    }

}
