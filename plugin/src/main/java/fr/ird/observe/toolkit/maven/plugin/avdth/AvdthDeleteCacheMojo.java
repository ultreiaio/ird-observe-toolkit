package fr.ird.observe.toolkit.maven.plugin.avdth;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

/**
 * Created on 29/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
@Mojo(name = "avdth-delete-cache", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE)
public class AvdthDeleteCacheMojo extends PersistenceMojoSupport {

    @Parameter(property = "runner.avdthRootPath", defaultValue = "${avdth.cache.path}", required = true)
    private File avdthRootPath;

    @Override
    protected DeleteAvdthCache createRunner() {
        DeleteAvdthCache runner = new DeleteAvdthCache();
        runner.setAvdthRootPath(avdthRootPath.toPath());
        return runner;
    }
}
