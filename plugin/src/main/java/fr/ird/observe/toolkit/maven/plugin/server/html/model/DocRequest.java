package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import fr.ird.observe.toolkit.maven.plugin.server.html.TemplateModelBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.topia.persistence.filter.EntityFilterConsumer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.18
 */
@SuppressWarnings("unused")
public class DocRequest extends DocElement implements WithValidation {
    private static final Logger log = LogManager.getLogger(DocRequest.class);

    private final Requests type;
    private final String dataScope;
    DocElement relativeParent;
    private List<String> parameters;
    private List<String> headerDefinitions;
    private String payloadContent;
    private String parametersContent;
    private String headersContent;
    private String exampleContent = "";
    private String requestPrototypeContent;
    private String exampleRequestContent;
    private String exampleResponseContent;
    private String validationContent = "";
    private String definitionContent = "";
    private String filterContent = "";
    private String orderContent = "";
    private List<FilterProperty> filterConsumers;
    private Set<String> filterOrders;
    private String naturalOrders;
    private String withIdInUrlContent;

    public static String getRelativeName(DocElement relativeParent, DocElement request) {
        if (relativeParent == null) {
            return request.getName();
        }
        StringBuilder result = new StringBuilder(request.getName());
        while ((request = request.getParent()) != relativeParent) {
            result.insert(0, request.getName() + "/");
        }
        return result.toString();
    }

    public DocRequest(DocElement parent, Requests type) {
        this(parent, type, null);
    }

    public DocRequest(DocElement parent, Requests type, String dataScope) {
        this(parent.getPath(), type, dataScope);
        setParent(parent);
    }

    public DocRequest(String parentPath, Requests type, String dataScope) {
        this.type = Objects.requireNonNull(type);
        this.dataScope = dataScope;
        setName(type.name());
        setPath(parentPath.replace("common/common", "common") + type.getQuery());
    }

    public String getDataScope() {
        return dataScope;
    }

    public String getFilterContent() {
        return filterContent;
    }

    public void setFilterContent(String filterContent) {
        this.filterContent = filterContent;
    }

    public String getOrderContent() {
        return orderContent;
    }

    public void setOrderContent(String orderContent) {
        this.orderContent = orderContent;
    }

    public String getDefinitionContent() {
        return definitionContent;
    }

    public void setDefinitionContent(String definitionContent) {
        this.definitionContent = definitionContent;
    }

    public String getParametersContent() {
        return parametersContent;
    }

    public void setParametersContent(String parametersContent) {
        this.parametersContent = parametersContent;
    }

    public String getHeadersContent() {
        return headersContent;
    }

    public void setHeadersContent(String headersContent) {
        this.headersContent = headersContent;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getHeaders() {
        return type.getHeaders();
    }

    public List<String> getHeaderDefinitions() {
        return headerDefinitions;
    }

    public void setHeaderDefinitions(List<String> headerDefinitions) {
        this.headerDefinitions = headerDefinitions;
    }

    @SuppressWarnings("unused")
    public String getRelativeName() {
        return getRelativeName(relativeParent, this);
    }

    @SuppressWarnings("unused")
    public String getParentRelativeName() {
        DocElement parent = getParent();
        if (parent instanceof DocProject) {
            return "None";
        }
        return getRelativeName(relativeParent, parent).replaceAll("/", ".");
    }

    public String getMethod() {
        return type.getMethod();
    }

    public String getTitle() {
        return type.getTitle(getParent().getName());
    }

    @Override
    public List<DocRequest> getRequests() {
        return Collections.singletonList(this);
    }

    public void setRelativeParent(DocElement relativeParent) {
        this.relativeParent = relativeParent;
    }

    public boolean withContent() {
        return type.needBody();
    }

    public String getExampleContent() {
        return exampleContent;
    }

    public void setExampleContent(String exampleContent) {
        this.exampleContent = exampleContent;
    }

    public String getPayloadContent() {
        return payloadContent;
    }

    public void setPayloadContent(String payloadContent) {
        this.payloadContent = payloadContent;
    }

    public String getValidationContent() {
        return validationContent;
    }

    public void setValidationContent(String validationContent) {
        this.validationContent = validationContent;
    }

    @Override
    public void storeValidation(Path requestPath) {
    }

    public String getRequestPrototypeContent() {
        return requestPrototypeContent;
    }

    public void setRequestPrototypeContent(String requestPrototypeContent) {
        this.requestPrototypeContent = requestPrototypeContent;
    }

    public String getExampleResponseContent() {
        return exampleResponseContent;
    }

    public void setExampleResponseContent(String exampleResponseContent) {
        this.exampleResponseContent = exampleResponseContent;
    }

    public String getExampleRequestContent() {
        return exampleRequestContent;
    }

    public void setExampleRequestContent(String exampleRequestContent) {
        this.exampleRequestContent = exampleRequestContent;
    }

    public void setFilterConsumers(List<FilterProperty> filterConsumers) {
        this.filterConsumers = filterConsumers;
    }

    public List<FilterProperty> getFilterConsumers() {
        return filterConsumers;
    }

    public void setFilterOrders(Set<String> filterOrders) {
        Set<String> tmp = new LinkedHashSet<>(filterOrders);
        tmp.remove("id");
        this.filterOrders = tmp;
    }

    public Set<String> getFilterOrders() {
        return filterOrders;
    }

    public String getNaturalOrders() {
        return naturalOrders;
    }

    public void setNaturalOrders(List<String> naturalOrders) {
        StringBuilder builder = new StringBuilder();
        for (String naturalOrder : naturalOrders) {
            builder.append(", orders.").append(naturalOrder).append("=ASC");
        }
        this.naturalOrders = builder.substring(2);
    }

    public String getPathClean() {
        String path = getPath();
        if (path.endsWith("?")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }

    public Map<String, Object> parametersMap() {
        String requestContent = getPath();
        boolean withParameters = !requestContent.endsWith("?");
        Map<String, Object> parameters = new LinkedHashMap<>();
        if (withParameters) {
            int beginIndex = requestContent.indexOf("?");
            String queryPart = requestContent.substring(beginIndex + 1);
            for (String keyValue : queryPart.split("&")) {
                String[] split = keyValue.split("=");
                String value = split[1];
                String key = split[0];
                if (value.equals("XXX")) {
                    value = "${_" + key + "}";
                }
                parameters.put(key, value);
            }
        }
        return parameters;
    }

    public Path resolveRequestPrototype(Path path) {
        return path.resolve(type.name() + "-request.json");
    }

    public Path resolveExampleRequest(Path path) {
        return path.resolve(type.name() + "-example-request.json");
    }

    public Path resolveExampleResponse(Path path) {
        return path.resolve(type.name() + "-example-response.json");
    }

    public void addFilter(DtoEntityContext<?, ?, ?, ?> spi, Mustache filterTemplate) {
        EntityFilterConsumer<?> entityFilterConsumer = spi.newFilterConsumer(ReferentialLocale.UK);
        setFilterConsumers(FilterProperty.create(entityFilterConsumer.consumers()));
        String filterContent = TemplateHelper.render(filterTemplate, this);
        setFilterContent(filterContent);
    }

    public void addOrder(DtoEntityContext<?, ?, ?, ?> spi, Mustache orderTemplate, Mustache requestDefinitionTemplate) {
        EntityFilterConsumer<?> entityFilterConsumer = spi.newFilterConsumer(ReferentialLocale.UK);
        setFilterOrders(entityFilterConsumer.authorizedOrders());
        setNaturalOrders(entityFilterConsumer.naturalOrders());
        String orderContent = TemplateHelper.render(orderTemplate, this);
        setOrderContent(orderContent);
    }

    public void addExample(Path fixturesPath) {
        Path requestPath = resolveRequestPrototype(fixturesPath);
        String requestContent = loadContent(requestPath);
        setRequestPrototypeContent(requestContent);

        Path exampleRequestPath = resolveExampleRequest(fixturesPath);
        String exampleRequestContent = loadContent(exampleRequestPath);
        setExampleRequestContent(exampleRequestContent);

        Path exampleResponsePath = resolveExampleResponse(fixturesPath);
        String exampleResponseContent = loadContent(exampleResponsePath);
        setExampleResponseContent(exampleResponseContent);
    }

    public String getWithIdInUrlContent() {
        return withIdInUrlContent;
    }

    public void setWithIdInUrlContent(String withIdInUrlContent) {
        this.withIdInUrlContent = withIdInUrlContent;
    }

    @Override
    public void fill(TemplateModelBuilder builder) {
        setExampleContent(builder.exampleContent);

        Map<String, String> headers = type.getHeaders();
        if (headers.size() > 0) {
            List<String> headersList = headers.keySet().stream().map(builder.headersContent::get).collect(Collectors.toList());
            setHeaderDefinitions(headersList);
            String headersContent = TemplateHelper.render(builder.headersTemplate, this);
            setHeadersContent(headersContent);
        }
        if (type.witIdInUrl()) {
            setWithIdInUrlContent(builder.idInUrlContent);
        }
        if (withContent()) {
            setPayloadContent(builder.payloadContent);
        }
        Map<String, Object> parameters = parametersMap();
        if (parameters.size() > 0) {
            List<String> parametersList = parameters.keySet().stream().map(builder.parametersContent::get).collect(Collectors.toList());
            setParameters(parametersList);
            String parametersContent = TemplateHelper.render(builder.parametersTemplate, this);
            setParametersContent(parametersContent);
        }
        String requestDefinitionContent = TemplateHelper.render(builder.requestDefinitionTemplate, this);
        setParametersContent(requestDefinitionContent);

        super.fill(builder, getParent().getClass().getSimpleName() + "Request");

        if (withContent()) {
            String contextName = type.name().toLowerCase();
            setDefinitionContent(builder.definitionRequestContent.replaceAll("\\$\\{contextName}", contextName));
        }
    }

    public String loadContent(Path responsePath) {
        if (responsePath != null) {
            try {
                return IOUtils.resourceToString(responsePath.toString(), StandardCharsets.UTF_8);
            } catch (IOException e) {
                log.warn("Could not find fixture: " + responsePath);
            }
        }
        return null;
    }

    public void setIdInPath(String id) {
        String encodeId = ToolkitId.encodeId(id);
        setPath(getPath().replace("{id}", encodeId));
    }

    public void setNumberInPath(int  number) {
        setPath(getPath().replace("{number}", number+""));
    }

    public boolean needAuthenticationToken() {
        return type.needAuthenticationToken();
    }

    public boolean needBody() {
        return type.needBody();
    }
}
