package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.DataSourcesForTestManager;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 28/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.58
 */
public class DeleteTckCache extends PersistenceRunner {

    private Path tckRootPath;

    public void setTckRootPath(Path tckRootPath) {
        this.tckRootPath = tckRootPath;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(tckRootPath);
    }

    @Override
    public void run() {
        try {
            DataSourcesForTestManager.clearCache(tckRootPath);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't delete tck cache at: %s", tckRootPath), e);
        }
    }
}

