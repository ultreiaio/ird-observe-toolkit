package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

/**
 * Created on 28/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.58
 */
@Mojo(name = "tck-delete-cache", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE)
public class TckDeleteCacheMojo extends PersistenceMojoSupport {

    @Parameter(property = "runner.tckRootPath", defaultValue = "${tck.cache.path}", required = true)
    private File tckRootPath;

    @Override
    protected DeleteTckCache createRunner() {
        DeleteTckCache runner = new DeleteTckCache();
        runner.setTckRootPath(tckRootPath.toPath());
        return runner;
    }
}
