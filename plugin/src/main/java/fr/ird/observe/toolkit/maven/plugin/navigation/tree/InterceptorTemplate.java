package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class InterceptorTemplate {

    public static final String INTERCEPTOR_CASE = "           case \"%1$s\":\n" +
            "                provider.intercept((%1$s) node);\n" +
            "                break;\n";
    public static final String INTERCEPTOR_METHOD = "" +
            "   default void intercept(%1$s node) {\n" +
            "    }\n\n";
    private final GenerateTreeModelSupport generator;
    private final Set<String> done = new TreeSet<>();
    protected StringBuilder interceptorCaseBuilder = new StringBuilder();
    protected StringBuilder interceptorMethodsBuilder = new StringBuilder();

    public InterceptorTemplate(GenerateTreeModelSupport generator) {
        this.generator = generator;
    }

    public String getPackageName() {
        return generator.getPackage().replace(".navigation.", ".spi.navigation.");
    }

    public String getClassSimpleName(MetaModelSupport descriptor) {
        return descriptor.getName() + generator.getSuffix() + "Interceptor";
    }

    public String generateContent(String packageName, String className) {
        return String.format(generator.getInterceptorTemplate()
                , packageName
                , new Date()
                , className
                , interceptorCaseBuilder.substring(1).trim()
                , interceptorMethodsBuilder.substring(1).trim()
                , getClass().getName());
    }

    public void register(TreeNodeLink link) {
        String childName = generator.getNodeFullyQualifiedName(link.getTargetClassName());
        if (done.add(childName)) {
            interceptorCaseBuilder.append(" ").append(String.format(INTERCEPTOR_CASE, childName));
            interceptorMethodsBuilder.append(" ").append(String.format(INTERCEPTOR_METHOD, childName));
        }
    }
}
