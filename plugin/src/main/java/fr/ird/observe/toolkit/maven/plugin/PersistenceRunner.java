package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.entities.ObserveTopiaConfigurationFactory;
import fr.ird.observe.test.DatabaseName;
import io.ultreia.java4all.util.ServiceLoaders;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.Zips;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.TopiaApplicationContextFactory;
import org.nuiton.topia.persistence.TopiaConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
public abstract class PersistenceRunner extends MojoRunnable {

    protected Path sourceDirectory;
    protected Version modelVersion;
    protected Version previousModelVersion;

    public void setSourceDirectory(Path sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    public void setModelVersion(Version modelVersion) {
        this.modelVersion = modelVersion;
    }

    public void setPreviousModelVersion(Version previousModelVersion) {
        this.previousModelVersion = previousModelVersion;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(sourceDirectory);
        Objects.requireNonNull(modelVersion);
    }

    public Version getModelVersion() {
        return modelVersion;
    }

    public ObserveDataSourceConfigurationTopiaH2 createConfiguration() {
        Path localDbFile = getTemporaryPath().resolve("db").resolve(modelVersion.toString()).resolve("H2");
        return createConfiguration(localDbFile);
    }

    public ObserveDataSourceConfigurationTopiaH2 createConfiguration(String classifier) {
        Path localDbFile = getTemporaryPath().resolve("db").resolve(modelVersion.toString()).resolve(classifier);
        return createConfiguration(localDbFile);
    }

    public ObserveDataSourceConfigurationTopiaH2 createConfiguration(Path localDbFile) {
        ObserveDataSourceConfigurationTopiaH2 configurationTopiaH2 = new ObserveDataSourceConfigurationTopiaH2();
        configurationTopiaH2.setLabel("Script creation: " + localDbFile.toFile().getName());
        configurationTopiaH2.setLogin("sa");
        configurationTopiaH2.setPassword("sa".toCharArray());
        configurationTopiaH2.setDirectory(localDbFile.toFile());
        configurationTopiaH2.setDbName("obstuna");
        configurationTopiaH2.setAutoMigrate(false);
        configurationTopiaH2.setModelVersion(modelVersion);
        configurationTopiaH2.setTemporaryDirectory(getTemporaryPath());
        return configurationTopiaH2;
    }

    protected TopiaApplicationContext<?> createTopiaApplicationContext(ObserveDataSourceConfigurationTopiaH2 configuration) {
        TopiaConfiguration topiaConfigurationExtension = ObserveTopiaConfigurationFactory.create(configuration);
        TopiaApplicationContextFactory<?, ?> applicationContextFactory = ServiceLoaders.loadUniqueService(TopiaApplicationContextFactory.class);
        return applicationContextFactory.create(topiaConfigurationExtension);
    }

    protected Path explodeArchive(Path archiveFile) {
        Path exploded = getTemporaryPath().resolve("exploded");
        if (Files.notExists(exploded)) {
            try {
                Files.createDirectories(exploded);
                try (InputStream inputStream = Files.newInputStream(archiveFile)) {
                    Zips.uncompress(inputStream, exploded.toFile());
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return explodeArchive(archiveFile,exploded);
    }

    protected Path explodeArchive(Path archiveFile, Path target) {
        if (Files.notExists(target)) {
            try {
                Files.createDirectories(target);
                try (InputStream inputStream = Files.newInputStream(archiveFile)) {
                    Zips.uncompress(inputStream, target.toFile());
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return target;
    }

    protected void createForVersion(Version version, Path exploded, Path targetPath, DatabaseName... databaseNames) {
        Path sourcePath = exploded.resolve(version.toString());
        Path versionTargetPath = targetPath.resolve(version.toString());

        try {
            Files.createDirectories(versionTargetPath);
            for (DatabaseName databaseName : databaseNames) {
                generate(databaseName, sourcePath, versionTargetPath);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void generate(DatabaseName database, Path sourcePath, Path targetPath) throws IOException {
        try (SqlScriptWriter scriptWriter = SqlScriptWriter.builder(targetPath.resolve(database.name() + ".sql.gz")).gzip().keepCommentLine().keepEmptyLine().build()) {
            database.generate(sourcePath, scriptWriter);
        }
    }
}
