package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.split.PersistenceScriptHelper;
import fr.ird.observe.spi.split.ScriptSplitter;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.ultreia.java4all.util.sql.SqlScript;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
public class ExtractScriptRunner extends PersistenceRunner {
    private static final Logger log = LogManager.getLogger(ExtractScriptRunner.class);

    private Path scriptFile;
    private Path targetPath;
    private Boolean forH2;

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(scriptFile);
        Objects.requireNonNull(targetPath);
        Objects.requireNonNull(forH2);
    }

    public void setScriptFile(Path scriptFile) {
        this.scriptFile = scriptFile;
    }

    public void setTargetPath(Path targetPath) {
        this.targetPath = targetPath;
    }

    public void setForH2(boolean forH2) {
        this.forH2 = forH2;
    }

    @Override
    public void run() {
        try {
            getLog().info(String.format("Will split script: %s", scriptFile));
            SqlScript script = SqlScript.of(scriptFile);

            String scriptName = scriptFile.toFile().getName().replace(".sql", "");
            log.info(String.format("Loading %s.", scriptName));

            ScriptSplitter.SplitResult splitResult = ScriptSplitter.create(forH2, script).splitSchema();

            PersistenceScriptHelper.write(targetPath, splitResult.getParent(), "%s.sql", "", true);
            String basename = String.format("%s-%%s.sql", scriptName);
            ScriptSplitter.SplitNotSchemaResultPair splitNotSchemaResultPair = ScriptSplitter.splitDataAndReferential(splitResult.getNotSchema());
            PersistenceScriptHelper.writeScript(targetPath, basename, "data", splitNotSchemaResultPair.getData());
            PersistenceScriptHelper.writeScript(targetPath, basename, "referential", splitNotSchemaResultPair.getReferential());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}
