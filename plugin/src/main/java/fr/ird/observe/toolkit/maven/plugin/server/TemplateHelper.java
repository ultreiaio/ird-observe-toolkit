package fr.ird.observe.toolkit.maven.plugin.server;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.spi.json.JsonHelper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Created on 02/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
public class TemplateHelper {

    private final DefaultMustacheFactory mf;

    public static String render(Mustache template, Object model) {
        try (StringWriter writer = new StringWriter(200)) {
            template.execute(writer, model);
            return writer.toString();
        } catch (IOException e) {
            throw new IllegalStateException("Can't render template: " + template, e);
        }
    }

    public static void anonymizeAuthenticationToken(Map<String, Object> parameters) {
        parameters.replace("authenticationToken", "XXX");
    }

    public static void removeTopiaFields(Map<String, Object> content) {
        JsonHelper.removeProperties(content, ToolkitId.PROPERTY_TOOLKIT_TOPIA_ID, ToolkitId.PROPERTY_TOOLKIT_CREATE_DATE, ToolkitId.PROPERTY_TOOLKIT_LAST_UPDATE_DATE);
    }

    public static void reduceResult(Map<?, ?> content) {
        for (Map.Entry<?, ?> entry : content.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Collection) {
                Collection<?> references = (Collection<?>) value;
                reduceResult(references);
            } else if (value instanceof Map) {
                reduceResult((Map<?, ?>) value);
            }
        }
    }

    public static void reduceResult(Collection<?> references) {
        if (references == null) {
            return;
        }
        Iterator<?> iterator = references.iterator();
        if (iterator.hasNext()) {
            Object next = iterator.next();
            if (next instanceof Map) {
                reduceResult((Map<?, ?>) next);
            }
            if (iterator.hasNext()) {
                next = iterator.next();
                if (next instanceof Map) {
                    reduceResult((Map<?, ?>) next);
                }
                while (iterator.hasNext()) {
                    iterator.next();
                    iterator.remove();
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static void reduceForCreate(Map<String, Object> content) {
        removeTopiaFields(content);
        for (Map.Entry<?, ?> entry : content.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Collection) {
                Collection<?> references = (Collection<?>) value;
                reduceForCreate(references);
            } else if (value instanceof Map) {
                reduceForCreate((Map<String, Object>) value);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static void reduceForCreate(Collection<?> references) {
        if (references == null) {
            return;
        }
        for (Object reference : references) {
            if (reference instanceof Map) {
                reduceForCreate((Map<String, Object>) reference);
            } else if (reference instanceof Collection) {
                reduceForCreate((Collection<?>) reference);
            }
        }
    }

    public TemplateHelper() {
        mf = new DefaultMustacheFactory("templates");
    }

    public Mustache getMustache(String suffix) {
        return mf.compile(suffix + ".mustache");
    }

    public String loadFragment(String name) {
        Mustache responseTemplate = getMustache("fragment/" + name);
        return render(responseTemplate, this);
    }

    public void loadParameter(Map<String, String> parametersContent, String name) {
        parametersContent.put(name, loadFragment("parameter/" + name));
    }

    public void loadBreadcrumb(Map<String, Mustache> breadCrumbTemplates, Class<?> name) {
        loadBreadcrumb(breadCrumbTemplates, name, null);
        loadBreadcrumb(breadCrumbTemplates, name, "Request");
    }

    public void loadBreadcrumb(Map<String, Mustache> breadCrumbTemplates, Class<?> name, String suffix) {
        String s = name.getSimpleName() + (suffix == null ? "" : suffix);
        breadCrumbTemplates.put(s, getMustache("fragment/breadcrumb/" + s));
    }

    public void loadPage(Map<String, Mustache> pagesTemplates, Class<?> name) {
        String s = name.getSimpleName();
        pagesTemplates.put(s, getMustache("page/" + s));
    }

    public void loadHeader(Map<String, Mustache> headerTemplates, Class<?> name) {
        loadHeader(headerTemplates, name, null);
        loadHeader(headerTemplates, name, "Request");
    }

    public void loadHeader(Map<String, Mustache> headerTemplates, Class<?> name, String suffix) {
        String s = name.getSimpleName() + (suffix == null ? "" : suffix);
        headerTemplates.put(s, getMustache("fragment/header/" + s));
    }

}
