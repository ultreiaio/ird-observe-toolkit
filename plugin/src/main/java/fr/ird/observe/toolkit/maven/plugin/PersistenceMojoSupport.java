package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.util.Version;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.util.Objects;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
public abstract class PersistenceMojoSupport extends ExecuteRunnerMojoSupport {
    /**
     * The version which fires the script.
     */
    @Parameter(property = "runner.acceptingVersion")
    private String acceptingVersion;
    /**
     * The current model version.
     */
    @Parameter(property = "runner.modelVersion", defaultValue = "${persistence.model.version}", required = true)
    private String modelVersion;

    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        if (acceptingVersion != null && !Objects.equals(modelVersion, acceptingVersion)) {
            getLog().warn(String.format("Skip script, accepting version %s does not match model version: %s", acceptingVersion, modelVersion));
            return;
        }
        super.doAction(getterFile, classLoader);
    }

    public Version getModelVersion() {
        return Version.valueOf(modelVersion);
    }

    @Override
    protected void prepare(MojoRunnable runner, I18nKeySet getterFile, ClassLoader classLoader) {
        if (runner instanceof PersistenceRunner) {
            prepare((PersistenceRunner) runner);
        }
        super.prepare(runner, getterFile, classLoader);
    }

    protected void prepare(PersistenceRunner runner) {
        runner.setSourceDirectory(getResourcesRoot().toPath());
        runner.setModelVersion(getModelVersion());
    }
}
