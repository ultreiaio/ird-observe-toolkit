package fr.ird.observe.toolkit.maven.plugin.fixtures;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.DataSourcesForTestManager;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import io.ultreia.java4all.util.Version;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
@Mojo(name = "fixtures-generate", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyCollection = ResolutionScope.COMPILE)
public class FixturesGenerateMojo extends ExecuteRunnerMojoSupport {

    private final DataSourcesForTestManager dataSourcesForTestManager = new DataSourcesForTestManager();

    @Parameter(property = "runner.tckDbName", defaultValue = "data", required = true)
    private DatabaseName tckDbName;

    @Parameter(property = "runner.modelVersion", defaultValue = "${persistence.model.version}", required = true)
    private String modelVersion;

    @Parameter(property = "runner.tckRootPath", required = true)
    private File tckRootPath;

    @Override
    protected FixturesRunner createRunner() {
        FixturesRunner runner = new FixturesRunner();
        runner.setTckRootPath(tckRootPath.toPath());
        runner.setApplicationVersion(Version.removeSnapshot(Version.valueOf(getProject().getVersion())));
        runner.setModelVersion(Version.valueOf(modelVersion));
        runner.setDataSourcesForTestManager(dataSourcesForTestManager);
        runner.setTckDbName(tckDbName);
        return runner;
    }
}
