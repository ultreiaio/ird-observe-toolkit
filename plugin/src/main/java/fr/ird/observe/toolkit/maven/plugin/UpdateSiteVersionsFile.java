package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 11/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.40
 */
@Mojo(name = "update-site-versions", threadSafe = true, defaultPhase = LifecyclePhase.SITE)
public class UpdateSiteVersionsFile extends ToolboxMojoSupport {

    /**
     * To activate verbose mode.
     */
    @Parameter(property = "updateSiteVersions.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;
    /**
     * To skip the goal.
     */
    @Parameter(property = "updateSiteVersions.skip", defaultValue = "false")
    private boolean skip;

    @Parameter(property = "updateSiteVersions.checkoutDirectory", defaultValue = "${scmpublish.checkoutDirectory}", required = true)
    private File checkoutDirectory;

    @Parameter(property = "updateSiteVersions.siteDirectory", defaultValue = "${scmpublish.content}", required = true)
    private File siteDirectory;

    @Parameter(property = "updateSiteVersions.buildDate", defaultValue = "${buildDate}", required = true)
    private String buildDate;

    @Parameter(property = "updateSiteVersions.version", defaultValue = "${project.version}", required = true)
    private String version;

    @Override
    protected void doAction() throws Exception {

        boolean releaseVersion = !version.endsWith("-SNAPSHOT");

        getLog().info(String.format("Add version: %s (build date: %s)", version, buildDate));

        Path versionsFile = checkoutDirectory.toPath().resolve("versions.json");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        Map<String, String> oldVersions = gson.fromJson(Files.readString(versionsFile), (Type) Object.class);
        Map<String, String> newVersions = new LinkedHashMap<>();

        newVersions.put(version, buildDate);
        for (Map.Entry<String, String> entry : oldVersions.entrySet()) {
            String currentVersion = entry.getKey();
            if (currentVersion.equals(version)) {
                continue;
            }
            if (releaseVersion && currentVersion.equals(version + "-SNAPSHOT")) {
                deleteDirectory(currentVersion);
                continue;
            }
            newVersions.put(currentVersion, entry.getValue());
        }

        String newVersionsContent = gson.toJson(newVersions);
        getLog().info(String.format("Updated versions:\n%s", newVersionsContent));
        getLog().info(String.format("Update file: %s", versionsFile));
        Files.write(versionsFile, newVersionsContent.getBytes(StandardCharsets.UTF_8), StandardOpenOption.TRUNCATE_EXISTING);

    }

    private void deleteDirectory(String path) throws IOException {
        Path directory = checkoutDirectory.toPath().resolve(path);
        if (Files.exists(directory)) {
            getLog().info(String.format("Delete directory: %s", directory));
            FileUtils.deleteDirectory(directory.toFile());
        }
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public boolean isSkip() {
        return skip;
    }

}
