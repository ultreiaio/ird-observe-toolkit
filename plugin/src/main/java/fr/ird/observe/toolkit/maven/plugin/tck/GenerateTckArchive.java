package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.datasource.request.CreateDatabaseRequest;
import fr.ird.observe.spi.split.PersistenceScriptHelper;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.Zips;
import io.ultreia.java4all.util.sql.SqlScript;
import io.ultreia.java4all.util.sql.SqlScriptWriter;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.jdbc.JdbcHelperH2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class GenerateTckArchive extends PersistenceRunner {

    private Path archiveFile;
    private Path targetArchiveFile;

    public void setArchiveFile(Path archiveFile) {
        this.archiveFile = archiveFile;
    }

    public void setTargetArchiveFile(Path targetArchiveFile) {
        this.targetArchiveFile = targetArchiveFile;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(archiveFile);
        Objects.requireNonNull(targetArchiveFile);
        Objects.requireNonNull(previousModelVersion);
    }

    @Override
    public void run() {

        Path exploded = explodeArchive(archiveFile);
        Path sources = getTemporaryPath().resolve("sources");
        createForVersion(previousModelVersion, exploded, sources, DatabaseName.data);
        Path archive = getTemporaryPath().resolve("archive");
        try {
            generateTckScripts(exploded, sources, archive);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't generate tck scripts to: %s", archive), e);
        }
        try {
            generateTckArchive(archive, targetArchiveFile);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't generate tck archive to: %s", targetArchiveFile), e);
        }
    }


    public static void backupSane(TopiaApplicationContext<?> applicationContext, Path backupFile, boolean compress) {
        try (SqlScriptWriter scriptWriter = newWriter(backupFile, compress)) {
            JdbcHelperH2 helper = applicationContext.newJdbcHelperH2();
            CreateDatabaseRequest request = CreateDatabaseRequest
                    .builder(!applicationContext.getConfiguration().isH2Configuration(), Version.valueOf(applicationContext.getModelVersion()))
                    .addGeneratedSchema()
                    .addVersionTable()
                    .addStandaloneTables()
                    .addAllData()
                    .build();
            SqlScript sqlScript = applicationContext.getSqlService().consume(request);
            helper.copy(sqlScript, scriptWriter);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't backup to: %s", backupFile), e);
        }
    }

    protected static SqlScriptWriter newWriter(Path backupFile, boolean compress) {
        if (compress) {
            return SqlScriptWriter.builder(backupFile).keepCommentLine().keepEmptyLine().gzip().build();
        }
        return SqlScriptWriter.builder(backupFile).keepCommentLine().keepEmptyLine().build();
    }

    private void generateTckScripts(Path exploded, Path sources, Path archive) throws IOException {

        ObserveDataSourceConfigurationTopiaH2 configuration = createConfiguration("tck");
        Path exportPath = archive.resolve(modelVersion.getVersion());
        Path backupFile = getTemporaryPath().resolve("tck-backup.sql");

        try (TopiaApplicationContext<?> applicationContext = createTopiaApplicationContext(configuration)) {

            log.info(String.format("Load referential and data database for version %s", previousModelVersion));

            applicationContext.executeSqlStatements(SqlScript.of(sources.resolve(previousModelVersion.getVersion()).resolve(DatabaseName.data.name() + ".sql.gz")));

            log.info(String.format("Migrate from version %s to %s", previousModelVersion, modelVersion));
            applicationContext.migrate();

            // Load optional extra data script (to add new data if any for this version)
            Path dataPath = exploded.resolve(modelVersion.getVersion()).resolve("data-extra.sql");
            if (Files.exists(dataPath)) {
            log.info(String.format("Load extra data script for version %s (%s)", modelVersion, dataPath));
                applicationContext.executeSqlStatements(SqlScript.of(dataPath));
            }

            log.info(String.format("Do backup to %s", backupFile));
            backupSane(applicationContext, backupFile, false);

        }
        log.info(String.format("Generate tck scripts to %s", exportPath));
        PersistenceScriptHelper.generateTckScripts(true, backupFile, exportPath);
    }

    private void generateTckArchive(Path archive, Path targetFile) throws IOException {
        List<Path> files;
        try (Stream<Path> stream = Files.find(archive, 2, (path, basicFileAttributes) ->
                Files.isRegularFile(path) && path.toFile().getName().endsWith(".sql"))) {
            files = stream.collect(Collectors.toList());
        }
        log.info(String.format("Generate tck archive with %d script(s) to %s", files.size(), targetFile));
        Files.deleteIfExists(targetFile);
        if (Files.notExists(targetFile.getParent())) {
            Files.createDirectories(targetFile.getParent());
        }
        Zips.compressFiles(targetFile.toFile(), archive.toFile(), files.stream().map(Path::toFile).collect(Collectors.toList()));
    }
}
