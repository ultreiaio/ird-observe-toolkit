package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.util.Version;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 26/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.57
 */
@Mojo(name = "tck-create-databases", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE)
public class TckCreateDatabasesMojo extends PersistenceMojoSupport {

    @Parameter(property = "runner.previousModelVersion", defaultValue = "${persistence.previous.model.version}", required = true)
    private String previousModelVersion;

    @Parameter(property = "runner.archiveFile", defaultValue = "${tck.cache.path}/${tck.filename}", required = true)
    private File archiveFile;

    @Parameter(property = "runner.tckRootPath", defaultValue = "${tck.cache.path}", required = true)
    private File tckRootPath;

    @Parameter(property = "runner.skipIfExists", defaultValue = "false")
    private boolean skipIfExists;

    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        Path resolve = tckRootPath.toPath().resolve(getModelVersion().getVersion());
        if (skipIfExists && Files.exists(resolve)) {
            getLog().info(String.format("Skip - files already at: %s", resolve));
            return;
        }
        super.doAction(getterFile, classLoader);
    }

    @Override
    protected CreateTckDatabases createRunner() {
        CreateTckDatabases runner = new CreateTckDatabases();
        runner.setPreviousModelVersion(Version.valueOf(previousModelVersion));
        runner.setArchiveFile(archiveFile.toPath());
        return runner;
    }

    @Override
    protected void prepare(PersistenceRunner runner) {
        super.prepare(runner);
        runner.setSourceDirectory(tckRootPath.toPath());
    }
}

