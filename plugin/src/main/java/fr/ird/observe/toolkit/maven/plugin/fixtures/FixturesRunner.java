package fr.ird.observe.toolkit.maven.plugin.fixtures;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.dto.referential.ReferentialLocale;
import fr.ird.observe.services.ObserveServiceInitializer;
import fr.ird.observe.services.ObserveServiceInitializerConfig;
import fr.ird.observe.services.ObserveServiceMainFactory;
import fr.ird.observe.services.service.AnonymousService;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.test.DataSourcesForTestManager;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import io.ultreia.java4all.util.ServiceLoaders;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.json.adapters.DateAdapter;
import io.ultreia.java4all.util.sql.SqlScript;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class FixturesRunner extends MojoRunnable {
    private Path tckRootPath;
    private Version applicationVersion;
    private DatabaseName tckDbName;
    private Version modelVersion;
    private DataSourcesForTestManager dataSourcesForTestManager;

    public void setApplicationVersion(Version applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public void setTckDbName(DatabaseName tckDbName) {
        this.tckDbName = tckDbName;
    }

    public void setModelVersion(Version modelVersion) {
        this.modelVersion = modelVersion;
    }

    public DatabaseName getTckDbName() {
        return tckDbName;
    }

    public Version getModelVersion() {
        return modelVersion;
    }

    public Version getApplicationVersion() {
        return applicationVersion;
    }

    public void setTckRootPath(Path tckRootPath) {
        this.tckRootPath = tckRootPath;
    }

    public void setDataSourcesForTestManager(DataSourcesForTestManager dataSourcesForTestManager) {
        this.dataSourcesForTestManager = dataSourcesForTestManager;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(applicationVersion);
        Objects.requireNonNull(modelVersion);
        Objects.requireNonNull(tckDbName);
        Objects.requireNonNull(tckRootPath);
    }

    @Override
    public void run() {

        try (ObserveServiceMainFactory serviceMainFactory = new ObserveServiceMainFactory()) {
            try {
                dataSourcesForTestManager = new DataSourcesForTestManager();
                ObserveServiceInitializer serviceInitializer = createDataSourceConfiguration(
                        dataSourcesForTestManager,
                        serviceMainFactory,
                        modelVersion,
                        getTckDbName().name(),
                        null
                );

                getLog().info(String.format("Will generate at %s", tckRootPath));
                BusinessProject businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);

                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateAdapter())
                        .setPrettyPrinting().serializeNulls().disableHtmlEscaping().create();

                FixturesModel model = FixturesModel.create(getLog(), tckRootPath.resolve("fixtures"), getModelVersion(), getApplicationVersion()).build(businessProject);
                FixturesTemplate template = new FixturesTemplate(model, gson, serviceMainFactory, serviceInitializer);
                template.generate();

            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public ObserveServiceInitializer createDataSourceConfiguration(
            DataSourcesForTestManager dataSourcesForTestManager,
            ObserveServiceMainFactory serviceMainFactory,
            Version dbVersion,
            String dbName,
            File targetPath
    ) throws Exception {
        ObserveServiceInitializerConfig serviceInitializerConfig = new ObserveServiceInitializerConfig(
                Locale.FRENCH,
                ReferentialLocale.FR,
                getTemporaryPath().toFile(),
                600,
                dbVersion,
                applicationVersion);

        String login = "sa";
        char[] password = new char[]{'s', 'a'};
        ObserveDataSourceConfigurationTopiaH2 sharedDatabaseConfiguration = dataSourcesForTestManager.createSharedDataSourceConfigurationH2(dbVersion, dbName, login, password);
        File sharedDatabaseFile = sharedDatabaseConfiguration.getDatabaseFile();
        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration;
        boolean sharedDatabaseExist = sharedDatabaseFile.exists();
        if (!sharedDatabaseExist) {
            log.info(String.format("Create shared database: %s/%s to %s", dbVersion.toString(), dbName, sharedDatabaseFile));
            sharedDatabaseConfiguration.setTemporaryDirectory(getTemporaryPath());
            ObserveServiceInitializer serviceInitializer = new ObserveServiceInitializer(serviceInitializerConfig, sharedDatabaseConfiguration, null);

            try {
                SqlScript script = dataSourcesForTestManager.getCache(dbVersion, dbName);
                serviceMainFactory.newService(serviceInitializer, AnonymousService.class).createFromDump(sharedDatabaseConfiguration, script);
            } finally {
                serviceMainFactory.newService(serviceInitializer, DataSourceService.class).close();
            }
        }
        if (targetPath == null) {
            dataSourceConfiguration = sharedDatabaseConfiguration;
        } else {
            // Use a copy
            dataSourceConfiguration = dataSourcesForTestManager.createDataSourceConfigurationH2(targetPath, dbVersion, dbName, login, password);
            File databaseFileTarget = dataSourceConfiguration.getDatabaseFile();
            log.info(String.format("Copy database: %s/%s to %s", dbVersion.toString(), dbName, databaseFileTarget));
            Files.createDirectories(databaseFileTarget.toPath().getParent());
            Files.copy(sharedDatabaseFile.toPath(), databaseFileTarget.toPath());
        }
        dataSourceConfiguration.setModelVersion(dbVersion);
        dataSourceConfiguration.setTemporaryDirectory(getTemporaryPath());
        return new ObserveServiceInitializer(serviceInitializerConfig, dataSourceConfiguration, null);
    }
}
