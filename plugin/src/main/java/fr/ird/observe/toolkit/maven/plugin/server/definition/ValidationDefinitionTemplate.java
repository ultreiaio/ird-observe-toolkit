package fr.ird.observe.toolkit.maven.plugin.server.definition;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.module.BusinessDataPackage;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.validation.api.NuitonFieldValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class ValidationDefinitionTemplate {

    private final Log log;
    private final Path targetDirectory;
    private final Gson gson;
    private final Map<Class<?>, List<NuitonValidatorModel<?>>> validatorsByType;
    boolean verbose;
    private int count;

    public ValidationDefinitionTemplate(Log log, Path targetDirectory, Map<Class<?>, List<NuitonValidatorModel<?>>> validatorsByType) {
        this.log = log;
        this.targetDirectory = targetDirectory;
        this.gson = NuitonValidatorModelHelper.creatGson();
        this.validatorsByType = validatorsByType;
    }

    public static Pair<String, String> generateTypeDefinitionContent(Map<Class<?>, List<NuitonValidatorModel<?>>> validatorsByType, Gson gson, Class<? extends BusinessDto> dtoType) {
        List<NuitonValidatorModel<?>> validators = validatorsByType.get(dtoType);

        List<NuitonValidatorModel<?>> createErrors = validators.stream().filter(d -> Objects.equals(d.getContext(), "create") && d.getScopes().contains(NuitonValidatorScope.ERROR)).collect(Collectors.toList());
        List<NuitonValidatorModel<?>> createWarnings = validators.stream().filter(d -> Objects.equals(d.getContext(), "create") && d.getScopes().contains(NuitonValidatorScope.WARNING)).collect(Collectors.toList());

        List<NuitonValidatorModel<?>> updateErrors = validators.stream().filter(d -> Objects.equals(d.getContext(), "update") && d.getScopes().contains(NuitonValidatorScope.ERROR)).collect(Collectors.toList());
        List<NuitonValidatorModel<?>> updateWarnings = validators.stream().filter(d -> Objects.equals(d.getContext(), "update") && d.getScopes().contains(NuitonValidatorScope.WARNING)).collect(Collectors.toList());

        Object createReduceErrors = reduceValidators(createErrors, createWarnings);

        Object updateReduceErrors = reduceValidators(updateErrors, updateWarnings);

        String createFileContent = gson.toJson(createReduceErrors);

        String updateFileContent = gson.toJson(updateReduceErrors);
        return Pair.of(createFileContent, updateFileContent);
    }

    private static Object reduceValidators(List<NuitonValidatorModel<?>> errors, List<NuitonValidatorModel<?>> warnings) {
        Map<String, Object> result = new LinkedHashMap<>();
        Set<String> fieldNames = errors.stream().flatMap(d -> d.getFieldNames(NuitonValidatorScope.ERROR).stream()).filter(f -> !f.isEmpty()).flatMap(Set::stream).collect(Collectors.toSet());
        fieldNames.addAll(warnings.stream().flatMap(d -> d.getFieldNames(NuitonValidatorScope.WARNING).stream()).filter(f -> !f.isEmpty()).flatMap(Set::stream).collect(Collectors.toSet()));
        List<String> sortedFieldNames = new ArrayList<>(fieldNames);
        sortedFieldNames.sort(String::compareTo);
        for (String fieldName : sortedFieldNames) {
            Map<String, Object> fieldObject = new LinkedHashMap<>();
            fill(fieldName, errors, NuitonValidatorScope.ERROR, fieldObject);
            fill(fieldName, warnings, NuitonValidatorScope.WARNING, fieldObject);
            if (!fieldObject.isEmpty()) {
                result.put(fieldName, fieldObject);
            }
        }
        return result;
    }

    private static void fill(String fieldName, List<NuitonValidatorModel<?>> errors, NuitonValidatorScope scope, Map<String, Object> fieldObject) {
        List<String> fieldErrors = new ArrayList<>();
        for (NuitonValidatorModel<?> validator : errors) {
            List<NuitonFieldValidatorModel> fields = validator.getFieldsByScope().get(scope);
            for (NuitonFieldValidatorModel field : fields) {
                String name = field.getFieldName();
                if (name.equals(fieldName)) {
                    fieldErrors.addAll(field.getComments());
                }
            }
        }
        if (!fieldErrors.isEmpty()) {
            fieldObject.put(scope.name().toLowerCase() + "s", fieldErrors);
        }
    }

    public void generate(BusinessProject businessProject) throws IOException {
        count++;
        BusinessProjectVisitor visitor = new BusinessProjectVisitor() {

            @Override
            public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage, Class<? extends DataDto> dtoType) {
                Class<?> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);
                if (mainDtoType != dtoType && !(EditableDto.class.isAssignableFrom(dtoType))) {
                    return;
                }
                Path path = BusinessProject.getDtoVariablePath(targetDirectory, module, subModule, dtoType);
                try {
                    generateDefinition(path, dtoType);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }

            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                Path path;
                if (subModule.getName().equals(module.getName())) {
                    path = BusinessProject.getDtoVariablePath(targetDirectory, module, dtoType);
                } else {
                    path = BusinessProject.getDtoVariablePath(targetDirectory, module, subModule, dtoType);
                }
                try {
                    generateDefinition(path, dtoType);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        };
        businessProject.accept(visitor);
        log.info(String.format("Generate %d definition(s).", count));
    }

    protected void generateDefinition(Path path, Class<? extends BusinessDto> dtoType) throws IOException {
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        count++;
        if (verbose) {
            log.info("generate definition at: " + path);
        }
        Pair<String, String> pair = generateTypeDefinitionContent(validatorsByType, gson, dtoType);
        String createFileContent = pair.getLeft();
        String updateFileContent = pair.getRight();

        Files.write(path.resolve(DtoEntityContext.VALIDATION_CREATE_CLASSIFIER), createFileContent.getBytes(StandardCharsets.UTF_8));
        Files.write(path.resolve(DtoEntityContext.VALIDATION_UPDATE_CLASSIFIER), updateFileContent.getBytes(StandardCharsets.UTF_8));
    }

}
