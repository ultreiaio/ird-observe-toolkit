package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;

import java.beans.Introspector;
import java.util.Date;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.4
 */
public class ToParentIdProviderTemplate {
    public static final String ID_PROVIDER_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.dto.ToolkitIdDtoBean;\n" +
            "import fr.ird.observe.dto.ToolkitParentIdDtoBean;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.Objects;\n\n" +
            "@Generated(value = \"%6$s\", date = \"%2$s\")\n" +
            "public interface %3$s extends fr.ird.observe.spi.navigation.parent.ToParentIdProvider {\n" +
            "\n" +
            "    static ToolkitParentIdDtoBean getParent(%3$s provider, ToolkitIdDtoBean shortId) {\n" +
            "        String id = Objects.requireNonNull(shortId.getId());\n" +
            "        switch (shortId.getType().getName()) {\n" +
            "            %4$s\n" +
            "        }\n" +
            "        return null;\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    default ToolkitParentIdDtoBean getParent(ToolkitIdDtoBean shortId) {\n" +
            "        return getParent(this, shortId);\n" +
            "    }\n\n" +
            "    %5$s\n" +
            "}";
    public static final String ID_CASE = "           case \"%1$s\":\n" +
            "                return provider.get%2$sParent(id);\n";
    public static final String ID_METHOD = "" +
            "   ToolkitParentIdDtoBean get%1$sParent(String %2$s);\n\n";
    private final GenerateToParentIdProvider generator;
    StringBuilder idCaseBuilder = new StringBuilder();
    StringBuilder idMethodsBuilder = new StringBuilder();

    public ToParentIdProviderTemplate(GenerateToParentIdProvider generator) {
        this.generator = generator;
    }

    public void register(ParentProjectModel projectModel, String node) {

        String simpleName = ProjectPackagesDefinition.getSimpleName(node);

        String getterName = projectModel.getterName(node);
        idCaseBuilder.append(" ").append(String.format(ID_CASE, generator.getDtoFullyQualifiedName(node), getterName));
        idMethodsBuilder.append(" ").append(String.format(ID_METHOD, getterName, Introspector.decapitalize(simpleName) + "Id"));
    }

    public String getPackageName() {
        return "fr.ird.observe.spi.navigation.parent";
    }

    public String getClassSimpleName(MetaModelSupport descriptor) {
        return descriptor.getName() + "ToParentIdProvider";
    }

    public String generateContent(String packageName, String className) {
        Date now = new Date();
        return String.format(ID_PROVIDER_MODEL
                , packageName
                , now
                , className
                , idCaseBuilder.substring(1).trim()
                , idMethodsBuilder.substring(1).trim()
                , getClass().getName());
    }
}
