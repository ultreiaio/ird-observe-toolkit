package fr.ird.observe.toolkit.maven.plugin.navigation.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.4
 */
public class BuildChildrenInterceptorTemplate {

    public static final String ID_PROVIDER_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.tree.ToolkitTreeNode;\n\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"%6$s\", date = \"%2$s\")\n" +
            "public abstract class %3$s extends NavigationTreeNodeChildrenBuilderSupport implements %4$s {\n\n" +
            "    public %3$s(boolean recursive, boolean canWriteData) {\n" +
            "        super(recursive, canWriteData);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final void intercept(ToolkitTreeNode node) {\n" +
            "        intercept0(node, n -> %4$s.intercept(this, n));\n" +
            "    }\n\n" +
            "    %5$s\n" +
            "}";
    public static final String ADD_TABLE_CHILDREN = "            addTableChildren(node, entity.get%1$s(), true, new %2$s());\n";
    public static final String ADD_SIMPLE_CHILDREN = "            addSimpleChildren(node, entity, true, new %1$s());\n";
    public static final String ADD_OPEN_LIST_CHILDREN = "            addOpenList(node, entity.get%1$s(), true, new %2$s());\n";
    public static final String ADD_ROOT_OPEN_CHILDREN = "            entity.get%1$s().forEach(e -> addRootOpen(node, e, true, new %2$s()));\n";
    public static final String ADD_OPEN_CHILDREN = "            entity.get%1$s().forEach(e -> addOpen(node, e, true, new %2$s()));\n";
    public static final String ADD_MULTIPLE_EDIT_CHILDREN = "            entity.get%1$s().forEach(e -> addEdit(node, e, true, new %2$s()));\n";
    public static final String ADD_SINGLE_EDIT_CHILDREN = "            java.util.Optional.ofNullable(entity.get%1$s()).ifPresent(e -> addEdit(node, e, true, new %2$s()));\n";

    public static final String INTERCEPT_METHOD = "" +
            "    @Override\n" +
            "    public void intercept(%1$s node) {\n" +
            "        %2$s entity = node.getState($$_DATA, true);\n" +
            "        if (entity != null && entity.isPersisted()) {\n" +
            "            %3$s\n" +
            "        }\n" +
            "    }\n\n";
    private final GeneratePersistence generator;
    private final String suffix;
    private final String packageName;
    StringBuilder idMethodsBuilder = new StringBuilder();

    public BuildChildrenInterceptorTemplate(GeneratePersistence generator) {
        this.generator = generator;
        this.suffix = "NavigationTreeNode";
        this.packageName = "fr.ird.observe.navigation.tree.navigation";
    }


    public void register(TreeProjectModel projectModel, TreeNodeModel node) {
        if (node.isRoot() || node.isLeaf() || node.isReferential()) {
            return;
        }

        String contentBuilder = computeAddContent(projectModel, node);

        String nodeFullyQualifiedName = getNodeFullyQualifiedName(node.getClassName());

        String entityType;
        if (node.isOpenList()) {
            entityType = generator.getEntityFullyQualifiedName(node.getParentClassName());
        } else {
            entityType = generator.getEntityFullyQualifiedName(node.getClassName());
        }
        idMethodsBuilder.append(String.format(INTERCEPT_METHOD, nodeFullyQualifiedName, entityType, contentBuilder));
    }

    private String computeAddContent(TreeProjectModel projectModel, TreeNodeModel node) {
        StringBuilder contentBuilder = new StringBuilder(" ");
        for (TreeNodeLink link : node.getChildren()) {
            String propertyName = link.getCapitalizePropertyName();

            TreeNodeModel targetNode = projectModel.getNode(link.getTargetClassName()).orElse(null);
            if (targetNode == null) {
                continue;
            }

            String nodeFullyQualifiedName = getNodeFullyQualifiedName(targetNode.getClassName());
            TreeNodeLink parentNodeLink = projectModel.getNodeLink(node).orElse(null);

            switch (targetNode.getNodeType()) {
                case OpenList:
                    contentBuilder.append(String.format(ADD_OPEN_LIST_CHILDREN, propertyName, nodeFullyQualifiedName));
                    break;
                case RootOpen:
                    contentBuilder.append(String.format(ADD_ROOT_OPEN_CHILDREN, propertyName, nodeFullyQualifiedName));
                    break;
                case Open:
                    contentBuilder.append(String.format(ADD_OPEN_CHILDREN, Objects.requireNonNull(parentNodeLink).getCapitalizePropertyName(), nodeFullyQualifiedName));
                    break;
                case Edit:
                    if (link.getMultiplicity() == MetaModelNodeLinkMultiplicity.MANY) {
                        contentBuilder.append(String.format(ADD_MULTIPLE_EDIT_CHILDREN, propertyName, nodeFullyQualifiedName));
                    } else {
                        contentBuilder.append(String.format(ADD_SINGLE_EDIT_CHILDREN, propertyName, nodeFullyQualifiedName));
                    }
                    break;
                case Table:
                    contentBuilder.append(String.format(ADD_TABLE_CHILDREN, propertyName, nodeFullyQualifiedName));
                    break;
                case Simple:
                    contentBuilder.append(String.format(ADD_SIMPLE_CHILDREN, nodeFullyQualifiedName));
                    break;
            }
        }
        return contentBuilder.substring(1).trim();
    }

    public String getPackageName() {
        return "fr.ird.observe.spi.navigation.tree.navigation";
    }

    public String getClassSimpleName(MetaModelSupport descriptor) {
        return descriptor.getName() + "NavigationTreeNodeBuildChildrenInterceptor";
    }

    public String generateContent(String modelName, String packageName, String className) {
        Date now = new Date();
        String interceptorName = modelName + "NavigationTreeNodeInterceptor";
        return String.format(ID_PROVIDER_MODEL
                , packageName
                , now
                , className
                , interceptorName
                , idMethodsBuilder.substring(1).trim()
                , getClass().getName());

    }

    public String getSuffix() {
        return suffix;
    }

    public String getPackage() {
        return packageName;
    }

    protected final String getNodeFullyQualifiedName(String node) {
        return getPackage() + "." + node + getSuffix();
    }

    protected final String getNodePackageName(TreeNodeModel node) {
        String packageName = node.getPackageName();
        return getPackage() + (packageName.isEmpty() ? "" : ("." + packageName));
    }

}
