package fr.ird.observe.toolkit.maven.plugin.server.definition;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import io.ultreia.java4all.util.ServiceLoaders;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class ValidationDefinitionRunner extends MojoRunnable {
    private Path targetDirectory;

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(targetDirectory);
    }

    @Override
    public void run() {
        getLog().info(String.format("Will generate at %s", targetDirectory));
        try {
            BusinessProject businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);

            List<NuitonValidatorModel<?>> validatorModels;
            try {
                validatorModels = new NuitonValidatorModelHelper().readAll(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            Path rootPath = targetDirectory
                    .resolve("fr")
                    .resolve("ird")
                    .resolve("observe")
                    .resolve("entities");

            ValidationDefinitionTemplate template = new ValidationDefinitionTemplate(getLog(), rootPath, getValidatorsByType(validatorModels));
            template.verbose = isVerbose();
            template.generate(businessProject);
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate model", e);
        }
    }

    public Map<Class<?>, List<NuitonValidatorModel<?>>> getValidatorsByType(List<NuitonValidatorModel<?>> validatorsManager) {
        Map<Class<?>, List<NuitonValidatorModel<?>>> validatorsByType = new LinkedHashMap<>();
        validatorsManager.forEach(validator -> validatorsByType.computeIfAbsent(validator.getType(), k -> new LinkedList<>()).add(validator));
        return validatorsByType;
    }
}
