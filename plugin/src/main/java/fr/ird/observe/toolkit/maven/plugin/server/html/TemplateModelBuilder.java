package fr.ird.observe.toolkit.maven.plugin.server.html;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import com.google.gson.Gson;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.module.BusinessDataPackage;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocModule;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocPackage;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocProject;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocType;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.Requests;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;

import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
@SuppressWarnings("UnusedReturnValue")
public class TemplateModelBuilder implements BusinessProjectVisitor {

    public static final String config = "&config.loadReferential=XXX&config.recursive=XXX&config.prettyPrint=XXX&config.serializeNulls=XXX&referentialLocale=XXX";
    public static final String orders = "&orders=XXX";
    public static final String filters = "&filters=XXX" + orders + "&config.caseSensitive=XXX";
    public static final Path INIT_FIXTURES_PATH = Path.of("/fixtures/init");
    public static final String REFERENTIAL_FIXTURES_PATH = "/fixtures/referential";
    public final String footer;
    public final String requestContent;
    public final Mustache headersTemplate;
    public final Mustache requestDefinitionTemplate;
    public final Mustache parametersTemplate;
    public final String exampleContent;
    public final String idInUrlContent;
    public final String payloadContent;
    public final Map<String, String> headersContent;
    public final Map<String, String> parametersContent;
    public final Gson gson;
    public final String definitionRequestContent;
    public final String validationRequestContent;
    public final Mustache filterTemplate;
    public final Mustache orderTemplate;
    public final String definitionTypeContent;
    public final Mustache availableRequestsTemplate;
    public final String validationTypeContent;
    private final Map<String, Mustache> headerTemplates;
    private final Map<String, Mustache> breadCrumbTemplates;
    private DocModule currentReferentialModule;
    private DocModule currentDataModule;
    private DocPackage currentReferentialPackage;
    private DocPackage currentDataPackage;
    private TemplateModel result;
    private String moduleName;
    private String packageName;
    private BusinessProject businessProject;

    //FIXME Remove filter.id
    public TemplateModelBuilder(TemplateHelper templateHelper) {
        this.filterTemplate = templateHelper.getMustache("fragment/filterContent");
        this.orderTemplate = templateHelper.getMustache("fragment/orderContent");
        this.definitionRequestContent = templateHelper.loadFragment("definitionRequestContent");
        this.definitionTypeContent = templateHelper.loadFragment("definitionTypeContent");
        this.validationRequestContent = templateHelper.loadFragment("validationRequestContent");
        this.validationTypeContent = templateHelper.loadFragment("validationTypeContent");
        footer = templateHelper.loadFragment("footer");
        requestContent = templateHelper.loadFragment("requestContent");
        breadCrumbTemplates = new TreeMap<>();
        templateHelper.loadBreadcrumb(breadCrumbTemplates, DocProject.class);
        templateHelper.loadBreadcrumb(breadCrumbTemplates, DocModule.class);
        templateHelper.loadBreadcrumb(breadCrumbTemplates, DocPackage.class);
        templateHelper.loadBreadcrumb(breadCrumbTemplates, DocType.class);
        headerTemplates = new TreeMap<>();
        templateHelper.loadHeader(headerTemplates, DocProject.class);
        templateHelper.loadHeader(headerTemplates, DocModule.class);
        templateHelper.loadHeader(headerTemplates, DocPackage.class);
        templateHelper.loadHeader(headerTemplates, DocType.class);
        headersContent = new TreeMap<>();
        templateHelper.loadParameter(headersContent, "authenticationToken");
        templateHelper.loadParameter(headersContent, "Content-Type");
        parametersContent = new TreeMap<>();
        templateHelper.loadParameter(parametersContent, "referentialLocale");
        templateHelper.loadParameter(parametersContent, "config.loadReferential");
        templateHelper.loadParameter(parametersContent, "config.recursive");
        templateHelper.loadParameter(parametersContent, "config.prettyPrint");
        templateHelper.loadParameter(parametersContent, "config.serializeNulls");
        templateHelper.loadParameter(parametersContent, "config.modelVersion");
        templateHelper.loadParameter(parametersContent, "config.login");
        templateHelper.loadParameter(parametersContent, "config.password");
        templateHelper.loadParameter(parametersContent, "config.databaseName");
        templateHelper.loadParameter(parametersContent, "config.caseSensitive");
        templateHelper.loadParameter(parametersContent, "filters");
        templateHelper.loadParameter(parametersContent, "orders");
        availableRequestsTemplate = templateHelper.getMustache("fragment/availableRequests");
        parametersTemplate = templateHelper.getMustache("fragment/parameters");
        headersTemplate = templateHelper.getMustache("fragment/headers");
        requestDefinitionTemplate = templateHelper.getMustache("fragment/requestDefinition");
        exampleContent = templateHelper.loadFragment("exampleContent");
        idInUrlContent = templateHelper.loadFragment("idInUrlContent");
        payloadContent = templateHelper.loadFragment("payloadContent");
        this.gson = NuitonValidatorModelHelper.creatGson();
    }

    public TemplateModel build(BusinessProject project) {
        try {
            project.accept(this);
            return result;
        } finally {
            result = null;
        }
    }

    @Override
    public void enterProject(BusinessProject project) {
        this.businessProject = project;
        this.result = new TemplateModel();
        result.referentialProject.fill(this);
        result.dataProject.fill(this);
        result.initProject.fill(this);
        // Get all referential in project
        result.getReferentialProject().addRequest(this,
                                                  Requests.GetAllForProject,
                                                  "project",
                                                  null,
                                                  "Get all referential available in project.",
                                                  Path.of(REFERENTIAL_FIXTURES_PATH));

        result.getInitProject().addRequest(this,
                                           Requests.Ping,
                                           "connexion",
                                           null,
                                           "Ping server.", INIT_FIXTURES_PATH);
        result.getInitProject().addRequest(this,
                                           Requests.Open,
                                           "connexion",
                                           null,
                                           "Connect to server.", INIT_FIXTURES_PATH);
        result.getInitProject().addRequest(this,
                                           Requests.Close,
                                           "connexion",
                                           null,
                                           "Close connexion.", INIT_FIXTURES_PATH);
        result.getInitProject().addRequest(this,
                                           Requests.Information,
                                           "connexion",
                                           null,
                                           "Get information about connexion.", INIT_FIXTURES_PATH);

    }

    @Override
    public void enterModule(BusinessModule module) {
        boolean withReferential = !module.getReferentialTypes().isEmpty();
        moduleName = module.getName().toLowerCase();
        if (withReferential) {
            currentReferentialModule = result.referentialProject.addModule(this, moduleName);
            // Get all referential in module
            currentReferentialModule.addRequest(this,
                                                Requests.GetAllForModule,
                                                "module",
                                                moduleName,
                                                String.format("Get all referential available in module <b>%s</b>.", moduleName),
                                                BusinessProject.getDtoModulePath(Path.of("/fixtures"), module, false));
        }
        boolean withData = !module.getDataTypes().isEmpty();
        if (withData) {
            currentDataModule = result.dataProject.addModule(this, moduleName);
        }
    }

    @Override
    public void enterSubModuleDataPackage(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage) {
        packageName = subModule.getName().toLowerCase();
        currentDataPackage = currentDataModule.addPackage(this, packageName);
    }

    @Override
    public void enterSubModuleReferentialPackage(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage) {
        packageName = subModule.getName().toLowerCase();
        currentReferentialPackage = currentReferentialModule.addPackage(this, packageName);
        // Get all referential in package
        currentReferentialPackage.addRequest(this,
                                             Requests.GetAllForPackage,
                                             "package",
                                             String.format("%s.%s", moduleName, packageName),
                                             String.format("Get all referential available in package <b>%s.%s</b>.", moduleName, packageName),
                                             BusinessProject.getDtoPackagePath(Path.of("/fixtures"), module, subModule, false));
    }

    @Override
    public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage, Class<? extends DataDto> dtoType) {
        Class<?> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);
        if (mainDtoType != dtoType && !EditableDto.class.isAssignableFrom(dtoType)) {
            return;
        }
        String simpleName = dtoType.getSimpleName().replace("Dto", "");
        DocType currentDataType = currentDataPackage.addType(this, simpleName);
        DataDtoEntityContext<? extends DataDto, DataDtoReference, DataEntity, ?> spi = PersistenceBusinessProject.fromDataDto(dtoType);
        currentDataType.addSimpleType(this, module, subModule, dtoType, spi);
    }

    @Override
    public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
        String simpleName = dtoType.getSimpleName().replace("Dto", "");
        DocType currentReferentialType = currentReferentialPackage.addType(this, simpleName);
        ReferentialDtoEntityContext<? extends ReferentialDto, ReferentialDtoReference, ReferentialEntity, ?> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
        currentReferentialType.addSimpleType(this, module, subModule, dtoType, spi);
    }

    @Override
    public void exitSubModuleDataPackage(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage) {
        currentDataPackage.finalizeContainer(this);
    }

    @Override
    public void exitSubModuleReferentialPackage(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage) {
        currentReferentialPackage.finalizeContainer(this);
    }

    @Override
    public void exitModule(BusinessModule module) {
        if (currentDataModule != null) {
            currentDataModule.finalizeContainer(this);
        }
        if (currentReferentialModule != null) {
            currentReferentialModule.finalizeContainer(this);
        }
    }

    @Override
    public void exitProject(BusinessProject project) {
        result.getInitProject().finalizeContainer(this);
        result.getReferentialProject().finalizeContainer(this);
        result.getDataProject().finalizeContainer(this);
    }

    public Mustache header(String name) {
        return headerTemplates.get(name);
    }

    public Mustache breadCrumb(String name) {
        return breadCrumbTemplates.get(name);
    }
}
