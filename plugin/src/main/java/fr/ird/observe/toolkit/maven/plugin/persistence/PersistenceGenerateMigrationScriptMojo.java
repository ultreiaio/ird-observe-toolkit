package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
@Mojo(name = "persistence-generate-migration-script", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyCollection = ResolutionScope.COMPILE)
public class PersistenceGenerateMigrationScriptMojo extends PersistenceMojoSupport {

    /**
     * Location of json script to execute.
     */
    @Parameter(property = "runner.scriptFile", defaultValue = "${project.basedir}/src/main/migration-v${persistence.model.version}.json", required = true)
    private File scriptFile;

    @Override
    protected GenerateMigrationScriptRunner createRunner() throws IOException {

        String scriptFileContent = Files.readString(scriptFile.toPath());
        MigrationScriptTask script = new GsonBuilder().create().fromJson(scriptFileContent, MigrationScriptTask.class);

        GenerateMigrationScriptRunner runner = new GenerateMigrationScriptRunner();
        runner.setTask(script);
        return runner;
    }

}
