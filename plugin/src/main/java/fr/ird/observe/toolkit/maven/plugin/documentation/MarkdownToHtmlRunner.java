package fr.ird.observe.toolkit.maven.plugin.documentation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.RenderMarkdown;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class MarkdownToHtmlRunner extends MojoRunnable {
    private Path extraDirectory;
    private Path targetDirectory;

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    public void setExtraDirectory(Path extraDirectory) {
        this.extraDirectory = extraDirectory;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(targetDirectory);
    }

    @Override
    public void run() {

        runForPath(targetDirectory);
        if (extraDirectory!=null) {
            runForPath(extraDirectory);

        }
    }

    private void runForPath(Path targetDirectory) {
        try (Stream<Path> pathStream = Files.walk(targetDirectory)) {
            pathStream.forEach(path -> {
                if (Files.isRegularFile(path) && path.toFile().getName().endsWith(".md")) {
                    processPath(path);
                }
            });
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void processPath(Path path) {
        try {
            getLog().info(String.format("Detect file to transform: %s", path));
            String template = Files.readString(path, StandardCharsets.UTF_8);
            String content = RenderMarkdown.renderContent(template);
            Path targetPath = path.getParent().resolve(path.toFile().getName().replace(".md", ".html"));
            getLog().info(String.format("Generate to: %s", targetPath));
            Files.writeString(targetPath, content, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


}
