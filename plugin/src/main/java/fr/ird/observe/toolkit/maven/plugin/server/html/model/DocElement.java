package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import fr.ird.observe.toolkit.maven.plugin.server.html.TemplateModelBuilder;

import java.util.List;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.18
 */
public abstract class DocElement {

    private String footer;
    private String requestContent;
    private String headerContent;
    private String breadcrumbContent;
    private String name;
    private String path;
    private DocElement parent;
    private String availableRequests = "";

    public String getHeaderContent() {
        return headerContent;
    }

    public void setHeaderContent(String headerContent) {
        this.headerContent = headerContent;
    }

    public String getBreadcrumbContent() {
        return breadcrumbContent;
    }

    public void setBreadcrumbContent(String breadcrumbContent) {
        this.breadcrumbContent = breadcrumbContent;
    }

    public String getFooter() {
        return footer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public DocElement getParent() {
        return parent;
    }

    public void setParent(DocElement parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public abstract List<DocRequest> getRequests();

    public boolean referential() {
        return getPath().contains("/referential");
    }

    public String getAvailableRequests() {
        return availableRequests;
    }

    public void setAvailableRequests(String availableRequests) {
        this.availableRequests = availableRequests;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public void fill(TemplateModelBuilder builder) {
        fill(builder, null);
    }

    public void fill(TemplateModelBuilder builder, String templatePath) {
        if (templatePath == null) {
            templatePath = getClass().getSimpleName();
        }
        setFooter(builder.footer);
        setRequestContent(builder.requestContent);
        String headerContent = TemplateHelper.render(builder.header(templatePath), this);
        setHeaderContent(headerContent);
        String breadCrumbContent = TemplateHelper.render(builder.breadCrumb(templatePath), this);
        setBreadcrumbContent(breadCrumbContent);
    }


    public void finalizeContainer(TemplateModelBuilder builder) {
        String availableRequests = TemplateHelper.render(builder.availableRequestsTemplate, this);
        setAvailableRequests(availableRequests);
    }
}
