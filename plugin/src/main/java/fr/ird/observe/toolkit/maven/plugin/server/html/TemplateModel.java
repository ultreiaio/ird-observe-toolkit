package fr.ird.observe.toolkit.maven.plugin.server.html;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocProject;

import java.util.LinkedList;
import java.util.List;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
public class TemplateModel {
    final DocProject referentialProject;
    final DocProject dataProject;
    final DocProject initProject;
    final List<String> requests;

    public TemplateModel() {
        this.initProject = new DocProject("init");
        this.dataProject = new DocProject("data");
        this.referentialProject = new DocProject("referential");
        this.requests = new LinkedList<>();
    }

    public DocProject getReferentialProject() {
        return referentialProject;
    }

    public DocProject getDataProject() {
        return dataProject;
    }

    public DocProject getInitProject() {
        return initProject;
    }

    public List<String> getRequests() {
        return requests;
    }

}
