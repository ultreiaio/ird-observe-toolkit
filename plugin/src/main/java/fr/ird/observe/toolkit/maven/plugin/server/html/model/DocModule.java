package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.server.html.TemplateModelBuilder;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1.0
 */
public class DocModule extends DocContainer {

    final List<DocPackage> packages = new LinkedList<>();

    public DocModule(DocProject parent, String name) {
        super(parent, name);
    }

    @Override
    public DocProject getParent() {
        return (DocProject) super.getParent();
    }

    @Override
    protected List<DocRequest> getChildrenRequests() {
        return getPackages().stream().flatMap(m -> m.getRequests().stream()).collect(Collectors.toList());
    }

    public List<DocPackage> getPackages() {
        return packages;
    }

    public DocPackage getPackage(String name) {
        Optional<DocPackage> module = packages.stream().filter(m -> name.equals(m.getName())).findFirst();
        DocPackage result;
        if (module.isEmpty()) {
            result = new DocPackage(this, name);
            packages.add(result);
        } else {
            result = module.get();
        }
        return result;
    }

    public DocPackage addPackage(TemplateModelBuilder builder, String packageName) {
        DocPackage result = getPackage(packageName);
        result.fill(builder);
        return result;
    }

    public DocExtraRequest addRequest(TemplateModelBuilder builder, Requests type, String dataScope, String data, String title, Path fixturesPath) {
        DocExtraRequest request = addExtraRequest(type, dataScope, data, title, fixturesPath);
        request.fill(builder);
        return request;
    }


}
