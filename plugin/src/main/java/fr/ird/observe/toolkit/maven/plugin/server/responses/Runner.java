package fr.ird.observe.toolkit.maven.plugin.server.responses;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 23/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.18
 */
public class Runner extends MojoRunnable {

    private Path targetDirectory;

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(targetDirectory);
    }

    @Override
    public void run() {
        getLog().info(String.format("Will generate at %s", targetDirectory));
        try {
            Template template = new Template(getLog(), targetDirectory);
            template.init();
            template.generate();
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate model", e);
        }
    }

}
