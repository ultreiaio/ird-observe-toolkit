package fr.ird.observe.toolkit.maven.plugin.server.responses;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.dto.ToolkitId;
import fr.ird.observe.dto.ToolkitIdBean;
import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created on 08/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public class RequestModel {
    private final String url;
    private final String method;
    private final Map<String, Object> parameters = new LinkedHashMap<>();
    private transient Map<String, Object> realParameters = new LinkedHashMap<>();
    private transient Path path;

    public RequestModel(RequestModel model) {
        this.method = model.getMethod();
        this.url = model.getUrl();
        this.path = model.getPath();
        if (model.getRealParameters() != null) {
            parameters.putAll(model.getRealParameters());
            TemplateHelper.anonymizeAuthenticationToken(parameters);
        }
    }

    public String getMethod() {
        return method;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public Map<String, Object> getRealParameters() {
        return realParameters;
    }

    public String getUrl() {
        return url;
    }

    public RequestModel setPath(Path path) {
        this.path = path;
        return this;
    }

    public Path getResponsePath() {
        return path.getParent().resolve("example-response.json");
    }

    public Path getRequestPath() {
        return path.getParent().resolve("example-request.json");
    }

    public Path getContentPath() {
        return path.getParent().resolve("content.json");
    }

    public Object invoke(Gson gson, Properties variables) throws IOException {
        StringBuilder request = new StringBuilder(variables.getProperty("_host"));
        request.append(getUrl()).append("?");
        Iterator<Map.Entry<String, Object>> iterator = getParameters().entrySet().iterator();
        if (realParameters == null) {
            realParameters = new LinkedHashMap<>();
        }
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String key = entry.getKey();
            String value;
            if (!(entry.getValue() instanceof String)) {
                continue;
            }
            String realKey = (String) entry.getValue();
            if (realKey.startsWith("$")) {
                realKey = Template.unboxVariable(realKey);
                value = variables.getProperty(realKey);
            } else {
                value = realKey;
            }
            if (value != null) {
                value = value.replaceAll("#", "%23");
                realParameters.put(key, value);
                request.append(key).append("=").append(value);
            }
            if (iterator.hasNext()) {
                request.append("&");
            }
        }
        URL url = new URL(request.toString());
        String content = IOUtils.toString(url, StandardCharsets.UTF_8);
        return gson.fromJson(content, Object.class);
    }

    public void resolveParameters(Properties variables) {
        Iterator<Map.Entry<String, Object>> iterator = getParameters().entrySet().iterator();
        if (realParameters == null) {
            realParameters = new LinkedHashMap<>();
        }
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String key = entry.getKey();
            String value;
            if (!(entry.getValue() instanceof String)) {
                continue;
            }
            String realKey = (String) entry.getValue();
            if (realKey.startsWith("$")) {
                realKey = Template.unboxVariable(realKey);
                value = variables.getProperty(realKey);
                if (value == null) {
                    Object o = variables.get(realKey);
                    realParameters.put(key, o);
                    continue;
                }
            } else {
                value = realKey;
            }
            value = ToolkitId.encodeId(value);
            realParameters.put(key, value);
        }
    }

    public void storeContent(Gson gson, Gson gsonWithNoNull, Map<?, ?> content, ToolkitIdBean result, Properties variables) throws IOException {
        String requestContent = gson.toJson(content);
        Path path = getContentPath();
        Files.deleteIfExists(path);
        Files.write(path, requestContent.getBytes(StandardCharsets.UTF_8));
        variables.put("_content", content);
        resolveParameters(variables);
        variables.remove("_content");
        storeResponse(gson, gsonWithNoNull, result);
    }

    void storeRequest(Gson gson) throws IOException {
        Path path = getRequestPath();
        Files.deleteIfExists(path);
        Files.write(path, gson.toJson(new RequestModel(this)).getBytes(StandardCharsets.UTF_8));
    }

    void storeResponse(Gson gson, Gson gsonWithNoNull, Object apply) throws IOException {
        storeRequest(gsonWithNoNull);
        Path path = getResponsePath();
        Files.deleteIfExists(path);
        if (apply instanceof Map) {
            @SuppressWarnings("unchecked") Map<String, Object> response = (Map<String, Object>) apply;
            TemplateHelper.anonymizeAuthenticationToken(response);
        }
        Files.write(path, gson.toJson(apply).getBytes(StandardCharsets.UTF_8));
    }

    public String getParameter(String id) {
        return (String) getParameters().get(id);
    }

    public Path getPath() {
        return path;
    }

}
