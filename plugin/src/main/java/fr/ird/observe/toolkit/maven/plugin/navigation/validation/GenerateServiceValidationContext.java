package fr.ird.observe.toolkit.maven.plugin.navigation.validation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateServiceValidationContext extends GenerateValidationModelSupport {

    public static final String METHOD_TEMPLATE = "" +
            "    @Override\n" +
            "    protected final Function<String, %1$s> %2$s() {\n" +
            "        return function();\n" +
            "    }\n\n";

    //FIXME Parametrized more stuff in ProjectPackageDefinition or extends it for the hole project used api
    public static final String PROJECT_TEMPLATE = "package %1$s;\n\n" +
            "import fr.ird.observe.dto.validation.DtoValidationContext;\n" +
            "import fr.ird.observe.dto.validation.ValidationRequestConfiguration;\n" +
            "import fr.ird.observe.navigation.id.Project;\n" +
            "import fr.ird.observe.services.ObserveServicesProvider;\n" +
            "import fr.ird.observe.decoration.DecoratorService;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.function.Function;\n\n" +
            "@Generated(value = \"%2$s\", date = \"%3$s\")\n" +
            "public abstract class %4$s extends DtoValidationContext {\n\n" +
            "    private final ObserveServicesProvider servicesProvider;\n\n" +
            "    protected GeneratedServiceValidationContext(ValidationRequestConfiguration configuration, DecoratorService decoratorService, Project selectModel, ObserveServicesProvider servicesProvider) {\n" +
            "        super(configuration, decoratorService, selectModel);\n" +
            "        this.servicesProvider = servicesProvider;\n" +
            "    }\n\n" +
            "    public final ObserveServicesProvider getServicesProvider() {\n" +
            "        return servicesProvider;\n" +
            "    }\n\n" +
            "%5$s" +
            "}";

    protected String generateMethod(String dtoFullyQualifiedName, String moduleName, String typePrefix, String capitalizeModuleName) {
        return String.format(METHOD_TEMPLATE,
                             dtoFullyQualifiedName,
                             moduleName + typePrefix + "Function");
    }

    @Override
    protected String getPackage() {
        return "fr.ird.observe.spi.validation";
    }

    protected String getClassSimpleName() {
        return "GeneratedServiceValidationContext";
    }

    @Override
    public String getProjectTemplate() {
        return PROJECT_TEMPLATE;
    }

}
