package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.filter.EntityFilterProperty;
import org.nuiton.topia.persistence.filter.FilterOperation;

import java.beans.Introspector;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 05/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
public class FilterProperty {

    private final String propertyKey;
    private final Class<?> type;
    private final String propertyType;
    private final String propertyDescription;
    private final String propertyFormat;
    private final Set<FilterOperation> flavors;

    public static List<FilterProperty> create(Map<String, EntityFilterProperty<?>> consumers) {
        List<FilterProperty> builder = new LinkedList<>();
        consumers.forEach((k, v) -> {
            if (!EntityFilterProperty.ID.equals(k)) {
                builder.add(FilterProperty.of(k, v));
            }
        });
        return builder;
    }

    public static FilterProperty of(String key, EntityFilterProperty<?> value) {
        Class<?> type = value.getType();
        String propertyType = type.getSimpleName();
        if (value.isPrimitive()) {
            propertyType = Introspector.decapitalize(propertyType);
        }
        if (key.endsWith("_id")) {
            propertyType = "Id";
        }
        return new FilterProperty(key, type, propertyType);
    }

    public FilterProperty(String propertyKey, Class<?> type, String propertyType) {
        this.propertyKey = propertyKey;
        this.type = type;
        this.propertyType = propertyType;
        String propertyFormat = "";
        Set<FilterOperation> flavors = null;
        String propertyDescription = null;
        switch (propertyType) {
            case "Id":
                propertyDescription = "Search on exact id, <b>CASE SENSITIVE</b>";
                propertyFormat = "<code>id</code>";
                flavors = EntityFilterProperty.FLAVORS_OBJECT;
                break;
            case "String":
                propertyDescription = "Search on a exact or partial text, <b>CASE INSENSITIVE</b>";
                propertyFormat = "<code>exact text</code> for an equality search, <code>%part of text%</code> for pattern search";
                flavors = EntityFilterProperty.FLAVORS_OBJECT;
                break;
            case "Boolean":
                propertyFormat = "<code>false|true</code>";
                flavors = EntityFilterProperty.FLAVORS_OBJECT;
                break;
            case "boolean":
                propertyFormat = "<code>false|true</code>";
                flavors = EntityFilterProperty.FLAVORS_EQUALS;
                break;
            case "Integer":
            case "Long":
                propertyFormat = "<code>[0-9]+</code>";
                flavors = EntityFilterProperty.FLAVORS_COMPARABLE;
                break;
            case "long":
            case "int":
            case "integer":
                propertyFormat = "<code>[0-9]+</code>";
                flavors = EntityFilterProperty.FLAVORS_PRIMITIVE;
                break;
            case "Float":
            case "Double":
                propertyFormat = "<code>[0-9]*[.[0-9]+]</code>";
                flavors = EntityFilterProperty.FLAVORS_COMPARABLE;
                break;
            case "float":
            case "double":
                propertyFormat = "<code>[0-9]*[.[0-9]+]</code>";
                flavors = EntityFilterProperty.FLAVORS_PRIMITIVE;
                break;
            case "Date":
                propertyFormat = "<code>yyyy-mm-dd</code>";
                flavors = EntityFilterProperty.FLAVORS_COMPARABLE;
                break;
            case "Time":
                propertyFormat = "<code>hh:mm</code>";
                flavors = EntityFilterProperty.FLAVORS_COMPARABLE;
                break;
            case "Timestamp":
                propertyFormat = "<code>yyyy-mm-dd hh:mm:ss MMMM</code>";
                flavors = EntityFilterProperty.FLAVORS_COMPARABLE;
                break;
        }
        if (Enum.class.isAssignableFrom(type)) {
            @SuppressWarnings({"unchecked", "rawtypes"}) EnumSet<?> enumSet = EnumSet.allOf((Class) type);
            flavors = EntityFilterProperty.FLAVORS_OBJECT;
            propertyFormat = "<code>" + enumSet.stream().map(Enum::name).collect(Collectors.joining("|")) + "</code>";
        }
        this.flavors = Objects.requireNonNull(flavors, String.format("No flavors for %s.%s", propertyType, propertyKey));
        this.propertyDescription = propertyDescription == null ? String.format("See documentation for filter type: <code>%s</code>.", propertyType) : propertyDescription;
        this.propertyFormat = propertyFormat;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getPropertyDescription() {
        return propertyDescription;
    }

    public String getPropertyFormat() {
        return propertyFormat;
    }

    public String getPropertyFlavors() {
        return flavors.stream().map(FilterOperation::flavor).collect(Collectors.joining(", ", "[", "]"));
    }

    public Class<?> getType() {
        return type;
    }
}
