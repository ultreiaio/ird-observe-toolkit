package fr.ird.observe.toolkit.maven.plugin.documentation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

/**
 * To scan for any {@code markdown} resources to transform to html.
 * <p>
 * Created on 19/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
@Mojo(name = "documentation-generate-html", threadSafe = true, defaultPhase = LifecyclePhase.PROCESS_CLASSES)
public class DocumentationGenerateHtmlMojo extends ExecuteRunnerMojoSupport {
    /**
     * Java source root directory.
     */
    @Parameter(property = "runner.extraRoot")
    private File extraRoot;

    @Override
    protected MarkdownToHtmlRunner createRunner() throws IOException {
        MarkdownToHtmlRunner runner = new MarkdownToHtmlRunner();
        runner.setTargetDirectory(getCompileRoot().toPath());
        if (extraRoot != null) {
            runner.setExtraDirectory(extraRoot.toPath());
        }
        return runner;
    }
}
