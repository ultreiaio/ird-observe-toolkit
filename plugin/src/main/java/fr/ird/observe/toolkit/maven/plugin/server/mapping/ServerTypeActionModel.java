package fr.ird.observe.toolkit.maven.plugin.server.mapping;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.x
 */
public class ServerTypeActionModel {
    final String comment;
    final String simpleName;
    final String moduleName;
    final Map<String, String> actions = new LinkedHashMap<>();

    ServerTypeActionModel(String entityType, String simpleName, String moduleName) {
        this.simpleName = simpleName;
        this.moduleName = moduleName;
        this.comment = String.format(entityType.contains(".data") ? "# Data : %1$s" : "# Referential : %1$s", entityType);
    }

    ServerTypeActionModel() {
        this.simpleName = null;
        this.moduleName = null;
        this.comment = null;
    }

    int actionPrefix() {
        return actions.keySet().stream().mapToInt(String::length).max().orElseThrow();
    }

    public String generate(int actionPrefix) {
        StringBuilder result = new StringBuilder("\n");
        if (comment != null) {
            result.append(comment);
        }
        for (Map.Entry<String, String> entry : actions.entrySet()) {
            result.append(String.format("\n%-" + actionPrefix + "s %s", entry.getKey(), entry.getValue()));
        }
        return result.append("\n").toString();
    }
}
