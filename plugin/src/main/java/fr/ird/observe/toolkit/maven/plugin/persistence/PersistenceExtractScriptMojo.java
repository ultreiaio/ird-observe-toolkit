package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
@Mojo(name = "persistence-extract-script", threadSafe = true, requiresProject = false)
public class PersistenceExtractScriptMojo extends PersistenceMojoSupport {
    /**
     * Name of script to split.
     */
    @Parameter(property = "runner.scriptName", required = true)
    private String scriptName;
    /**
     * Path of script to split.
     */
    @Parameter(property = "runner.scriptPath", required = true)
    private File scriptPath;
    /**
     * Path of scripts to generate (by default to scriptPath/extract-scriptName)
     */
    @Parameter(property = "runner.targetFile", defaultValue = "${runner.scriptPath}/extract-${runner.scriptName}", required = true)
    private File targetPath;
    /**
     * For h2 database?.
     */
    @Parameter(property = "runner.forH2", defaultValue = "true")
    private boolean forH2;

    public PersistenceExtractScriptMojo() {
        // no need of temporary database
        setUseTemporaryPath(false);
    }

    @Override
    protected ExtractScriptRunner createRunner() throws IOException {
        Path scriptFile = scriptPath.toPath().resolve(scriptName);
        if (Files.notExists(scriptFile)) {
            throw new IllegalStateException(String.format("Script %s does not exist.", scriptFile));
        }
        Path dir = targetPath.toPath();
        Files.createDirectories(dir);
        ExtractScriptRunner runner = new ExtractScriptRunner();
        runner.setScriptFile(scriptFile);
        runner.setForH2(forH2);
        runner.setTargetPath(targetPath.toPath());
        return runner;
    }
}
