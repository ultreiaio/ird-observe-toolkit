package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public abstract class MojoRunnable implements Runnable {

    protected I18nKeySet getterFile;
    protected Log log;
    private boolean force;
    private boolean verbose;
    private Path temporaryPath;

    public static MojoRunnable of(Runnable runner) {
        return new MojoRunnable() {
            @Override
            public void run() {
                Objects.requireNonNull(runner).run();
            }
        };
    }

    public I18nKeySet getGetterFile() {
        return getterFile;
    }

    public void setGetterFile(I18nKeySet getterFile) {
        this.getterFile = getterFile;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void init() {
        Objects.requireNonNull(temporaryPath);
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }

    public void setTemporaryPath(Path temporaryPath) {
        this.temporaryPath = temporaryPath;
    }

    public void prepare(MojoRunnable parentRunner) {
        setLog(parentRunner.log);
        setGetterFile(parentRunner.getterFile);
        setForce(parentRunner.isForce());
        setVerbose(parentRunner.isVerbose());
        setTemporaryPath(parentRunner.getTemporaryPath());
    }


    protected Path store(Path targetFile, String content) throws IOException {
        createDirectories(targetFile.getParent());
        log.debug(String.format("Will generate at %s", targetFile));
        Files.write(targetFile, content.getBytes(StandardCharsets.UTF_8));
        return targetFile;
    }

    protected void createDirectories(Path targetDirectory) {
        try {
            if (Files.notExists(targetDirectory)) {
                Files.createDirectories(targetDirectory);
            }
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't create directory %s", targetDirectory), e);
        }
    }

}
