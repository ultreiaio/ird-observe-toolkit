package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created at 12/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class RenameForeignKeyTask {
    private static final String RENAME_FOREIGN_KEY = "-- Rename foreign key %1$s.%2$s.%3$s → %4$s\n" +
            "ALTER TABLE %1$s.%2$s ADD COLUMN %4$s VARCHAR(255);\n" +
            "ALTER TABLE %1$s.%2$s ADD CONSTRAINT fk_%1$s_%2$s_%4$s FOREIGN KEY (%4$s) REFERENCES %1$s.%4$s;\n" +
            "CREATE INDEX idx_%1$s_%2$s_%4$s ON %1$s.%2$s(%4$s);\n" +
            "UPDATE %1$s.%2$s SET %4$s = REPLACE(set, '.%3$s', '.%4$s');\n" +
            "ALTER TABLE %1$s.%2$s ALTER COLUMN %4$s SET NOT NULL;\n" +
            "ALTER TABLE %1$s.%2$s DROP COLUMN %3$s%5$s;";
    private static final String RENAME_FOREIGN_KEY_WITH_IDX = "-- Rename foreign key %1$s.%2$s.%3$s → %4$s (and %3$s_idx → %4$s_idx)\n" +
            "ALTER TABLE %1$s.%2$s ADD COLUMN %4$s VARCHAR(255);\n" +
            "ALTER TABLE %1$s.%2$s ADD COLUMN %4$s_idx INTEGER;\n" +
            "ALTER TABLE %1$s.%2$s ADD CONSTRAINT fk_%1$s_%2$s_%4$s FOREIGN KEY (%4$s) REFERENCES %1$s.%4$s;\n" +
            "CREATE INDEX idx_%1$s_%2$s_%4$s ON %1$s.%2$s(%4$s);\n" +
            "UPDATE %1$s.%2$s SET %4$s = REPLACE(set, '.%3$s', '.%4$s'), %4$s_idx = %3$s_idx;\n" +
            "ALTER TABLE %1$s.%2$s ALTER COLUMN %4$s SET NOT NULL;\n" +
            "ALTER TABLE %1$s.%2$s ALTER COLUMN %4$s_idx SET NOT NULL;\n" +
            "ALTER TABLE %1$s.%2$s DROP COLUMN %3$s%5$s;\n" +
            "ALTER TABLE %1$s.%2$s DROP COLUMN %3$s_idx%5$s;";
    private final String schemaName;
    private final String tableName;
    private final boolean useIndex;

    public RenameForeignKeyTask(String schemaName, String tableName, boolean useIndex) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.useIndex = useIndex;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public boolean isUseIndex() {
        return useIndex;
    }

    public String toSql(boolean h2, String oldColumnName, String newColumnName) {
        return String.format(isUseIndex() ? RENAME_FOREIGN_KEY_WITH_IDX : RENAME_FOREIGN_KEY, getSchemaName(), getTableName(), oldColumnName, newColumnName, h2 ? "" : " CASCADE");
    }
}
