package fr.ird.observe.toolkit.maven.plugin.server.mapping;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 19/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class Runner extends MojoRunnable {
    public static final String PACKAGE = "fr.ird.observe.entities";
    private String modelName;
    private Path targetDirectory;

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(modelName);
        Objects.requireNonNull(targetDirectory);
    }

    @Override
    public void run() {

//        NavigationNodeDescriptor descriptor = NavigationNodeDescriptor.getRootDescriptor();
//        NavigationNodesBuilder builder = new NavigationNodesBuilder(getLog());
//        List<NodeModel> models = builder.build(descriptor);
//        getLog().info(String.format("Detect %d navigation node(s) to generate.\n%s", models.size(), builder.getDebugTree()));
        getLog().info(String.format("Will generate at %s", targetDirectory));

        Template template = new Template(getLog(), targetDirectory);
        try {
            template.generateMapping();
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate mapping.", e);
        }
    }

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}

