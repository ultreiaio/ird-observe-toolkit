package fr.ird.observe.toolkit.maven.plugin.navigation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 31/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class NavigationGenerateSupport extends MojoRunnable {

    protected BusinessProject businessProject;
    protected String dtoRootPackage;
    private Path targetDirectory;
    private ProjectPackagesDefinition packagesDefinition;

    protected abstract void generate(Path targetDirectory, String dtoRootPackage, MetaModelSupport descriptor) throws IOException;

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(targetDirectory);
        packagesDefinition = initProjectPackageDefinition();
    }


    protected ProjectPackagesDefinition initProjectPackageDefinition() {
        return ProjectPackagesDefinition.of(Thread.currentThread().getContextClassLoader());
    }

    @Override
    public void run() {

        dtoRootPackage = packagesDefinition.getDtoRootPackage();
        String name = packagesDefinition.getName();
        getLog().info(String.format("Load meta-model: %s", name));
        MetaModelSupport descriptor = new MetaModelSupport(Thread.currentThread().getContextClassLoader(), name);
        try {
            generate(targetDirectory, packagesDefinition.getDtoRootPackage(), descriptor);
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate model", e);
        }
    }

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    public final String getDtoFullyQualifiedName(String node) {
        return dtoRootPackage + "." + node + "Dto";
    }

    public final String getEntityFullyQualifiedName(String node) {
        return packagesDefinition.getEntityRootPackage() + "." + node;
    }

    public final Path getSpiFile(Path targetDirectory, String packageName, String classSimpleName) {
        String path = packageName + "." + classSimpleName;
        return targetDirectory.resolve(path.replaceAll("\\.", "/") + ".java");
    }

}

