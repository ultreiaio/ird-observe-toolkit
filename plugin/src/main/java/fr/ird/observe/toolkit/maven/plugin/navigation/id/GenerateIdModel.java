package fr.ird.observe.toolkit.maven.plugin.navigation.id;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.id.IdModuleModel;
import fr.ird.observe.spi.navigation.model.id.IdNodeModel;
import fr.ird.observe.spi.navigation.model.id.IdProjectModel;
import fr.ird.observe.toolkit.maven.plugin.navigation.NavigationGenerateSupport;
import io.ultreia.java4all.lang.Strings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Edit id model contains only root open and open nodes.
 * <p>
 * Created on 31/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateIdModel extends NavigationGenerateSupport {

    public static final String PACKAGE = "fr.ird.observe.navigation.id";
    public static final String NODE_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.spi.module.ObserveBusinessProject;\n" +
            "import fr.ird.observe.navigation.id.IdNode;\n" +
            "import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;\n" +
            "import java.util.function.Supplier;\n" +
            "import javax.annotation.Generated;\n\n" +
            "@GenerateJavaBeanDefinition\n" +
            "@Generated(value = \"fr.ird.observe.toolkit.maven.plugin.navigation.id.GenerateIdModel\", date = \"%2$s\")\n" +
            "public class %3$s extends IdNode<%4$s> {\n\n" +
            "    public %3$s() {\n" +
            "        super(%5$s);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final int getLevel() {\n" +
            "        return %6$d;\n" +
            "    }\n\n" +
            "%7$s" +
            "%8$s" +
            "    @Override\n" +
            "    protected Supplier<ObserveBusinessProject> getProjectSupplier() {\n" +
            "        return ObserveBusinessProject::get;\n" +
            "    }\n}\n";


    public static final String MODULE_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.spi.module.ObserveBusinessProject;\n" +
            "import fr.ird.observe.navigation.id.IdNode;\n" +
            "import fr.ird.observe.navigation.id.IdModule;\n" +
            "import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;\n" +
            "import java.util.function.Supplier;\n" +
            "import javax.annotation.Generated;\n\n" +
            "@GenerateJavaBeanDefinition\n" +
            "@Generated(value = \"fr.ird.observe.toolkit.maven.plugin.navigation.id.GenerateIdModel\", date = \"%2$s\")\n" +
            "public class Module extends IdModule {\n\n" +
            "    public Module() {\n" +
            "        super(new %3$s());\n" +
            "    }\n\n" +
            "%4$s" +
            "    @Override\n" +
            "    protected Supplier<ObserveBusinessProject> getProjectSupplier() {\n" +
            "        return ObserveBusinessProject::get;\n" +
            "    }\n}\n";
    public static final String PROJECT_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.id.IdAggregateModel;\n" +
            "import fr.ird.observe.navigation.id.IdProject;\n" +
            "import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;\n" +
            "import com.google.auto.service.AutoService;\n" +
            "import javax.annotation.Generated;\n\n" +
            "@GenerateJavaBeanDefinition\n" +
            "@AutoService(value = {IdAggregateModel.class, IdProject.class})\n" +
            "@Generated(value = \"fr.ird.observe.toolkit.maven.plugin.navigation.id.GenerateIdModel\", date = \"%2$s\")\n" +
            "public class Project extends IdProject {\n\n" +
            "%3$s" +
            "    public Project() {\n" +
            "        super(%4$s);\n" +
            "        %5$s\n" +
            "    }\n\n" +
            "%6$s" +
            "}\n";

    public static final String PARENT_MODEL = "" +
            "    @Override\n" +
            "    public final %1$s getParent() {\n" +
            "        return (%1$s) super.getParent();\n" +
            "    }\n\n";
    public static final String CAPABILITY_MODEL = "" +
            "    public final %1$s get%2$s() {\n" +
            "        return getNode(%3$d);\n" +
            "    }\n\n";
    public static final String CAPABILITY_CONSTRUCTOR_MODEL = "" +
            "              new %1$s()%2$s";
    public static final String CAPABILITY_FIELD_MODEL = "" +
            "    private final transient %1$s %2$s;\n\n";
    public static final String CAPABILITY_FIELD_INIT_MODEL = "" +
            "        this.%1$s = forModelType(%2$s.class).orElse(null);\n";
    public static final String GET_MODULE = "" +
            "    public final %1$s get%2$s() {\n" +
            "        return %3$s;\n" +
            "    }\n\n";

    @Override
    protected final void generate(Path targetDirectory, String dtoRootPackage, MetaModelSupport descriptor) throws IOException {
        IdProjectModel model = getProjectModel(descriptor);
        Path rootTargetDirectory = getRootTargetDirectory(targetDirectory);
        createDirectories(rootTargetDirectory);

        getLog().info(String.format("Will generate at %s", rootTargetDirectory));
        getLog().info(String.format("Found %d module(s).", model.getModules().size()));
        getLog().info(String.format("Found %d node(s).", model.getNodes().size()));

        generateProjectClass(getProjectFile(rootTargetDirectory), model);
        for (IdModuleModel module : model.getModules()) {
            generateModuleClass(getModuleFile(rootTargetDirectory, module), module);
        }
        for (IdNodeModel node : model.getNodes()) {
            generateNodeClass(getNodeFile(rootTargetDirectory, node), node);
        }
    }

    protected String getPackage() {
        return PACKAGE;
    }

    public String getNodeTemplate() {
        return NODE_MODEL;
    }

    public String getModuleTemplate() {
        return MODULE_MODEL;
    }

    public String getProjectTemplate() {
        return PROJECT_MODEL;
    }

    protected IdProjectModel getProjectModel(MetaModelSupport descriptor) {
        return descriptor.getIdProjectModel();
    }

    protected Path getRootTargetDirectory(Path targetDirectory) {
        return targetDirectory.resolve(getPackage().replaceAll("\\.", File.separator));
    }

    protected void generateProjectClass(Path targetFile, IdProjectModel model) throws IOException {
        if (isVerbose()) {
            getLog().info("Will generate project at: " + targetFile);
        }
        String content = generateProjectContent(model);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    protected void generateModuleClass(Path targetFile, IdModuleModel module) throws IOException {
        if (isVerbose()) {
            getLog().info("Will generate module  at: " + targetFile);
        }
        String content = generateModuleContent(module);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    protected void generateNodeClass(Path targetFile, IdNodeModel node) throws IOException {
        if (isVerbose()) {
            getLog().info("Will generate node    at: " + targetFile);
        }
        String content = generateNodeContent(node);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    protected String generateProjectContent(IdProjectModel model) {
        StringBuilder fieldBuilder = new StringBuilder();
        StringBuilder constructorBuilder = new StringBuilder();
        StringBuilder constructor2Builder = new StringBuilder();
        StringBuilder capabilityBuilder = new StringBuilder();
        List<IdModuleModel> modules = model.getModules();
        Iterator<IdModuleModel> iterator = modules.iterator();
        while (iterator.hasNext()) {
            IdModuleModel module = iterator.next();
            String moduleName = module.getClassName();
            boolean hasNext = iterator.hasNext();
            String moduleFqn = getModuleFullyQualifiedName(moduleName);
            fieldBuilder.append(String.format(CAPABILITY_FIELD_MODEL, moduleFqn, moduleName));
            constructorBuilder.append(String.format(CAPABILITY_CONSTRUCTOR_MODEL, moduleFqn, hasNext ? ",\n" : ""));
            constructor2Builder.append(String.format(CAPABILITY_FIELD_INIT_MODEL, moduleName, moduleFqn));
            capabilityBuilder.append(String.format(GET_MODULE, moduleFqn, Strings.capitalize(moduleName), moduleName));
        }
        return String.format(getProjectTemplate()
                , getPackage()
                , new Date()
                , fieldBuilder
                , constructorBuilder.toString().trim()
                , constructor2Builder.toString().trim()
                , capabilityBuilder
        );
    }

    protected String generateModuleContent(IdModuleModel model) {
        String moduleName = model.getClassName();
        Map<String, String> nodes = model.getNodes();
        StringBuilder capabilityBuilder = new StringBuilder();
        int index = 0;
        String firstNode = null;
        for (Map.Entry<String, String> entry : nodes.entrySet()) {
            String nodeName = getNodeFullyQualifiedName(entry.getValue());
            if (firstNode == null) {
                firstNode = nodeName;
            }
            String getterName = Strings.capitalize(entry.getKey());
            capabilityBuilder.append(String.format(CAPABILITY_MODEL, nodeName, getterName, index++));
        }
        return String.format(getModuleTemplate()
                , getPackage() + "." + moduleName
                , new Date()
                , firstNode
                , capabilityBuilder
        );
    }

    protected String generateNodeContent(IdNodeModel model) {
        StringBuilder constructorBuilder = new StringBuilder();
        StringBuilder capabilityBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = model.getNodes().entrySet().iterator();
        int order = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String propertyName = entry.getKey();
            String nodeType = entry.getValue();
            String nodeName = getNodeFullyQualifiedName(nodeType);
            boolean hasNext = iterator.hasNext();
            constructorBuilder.append(String.format(CAPABILITY_CONSTRUCTOR_MODEL, nodeName, hasNext ? ",\n" : ""));
            capabilityBuilder.append(String.format(CAPABILITY_MODEL, nodeName, Strings.capitalize(propertyName), order++));
        }
        int level = model.getLevel();
        String nodeName = getNodeFullyQualifiedName(model.getClassName());
        int indexOf = nodeName.lastIndexOf(".");
        return String.format(getNodeTemplate()
                , nodeName.substring(0, indexOf)
                , new Date()
                , nodeName.substring(indexOf + 1)
                , getDtoFullyQualifiedName(model.getType())
                , constructorBuilder.toString().trim()
                , level
                , level == -1 ? "" : String.format(PARENT_MODEL, getNodeFullyQualifiedName(model.getParentClassName()))
                , capabilityBuilder
        );
    }

    protected Path getProjectFile(Path targetDirectory) {
        return targetDirectory.resolve("Project.java");
    }

    protected Path getModuleFile(Path targetDirectory, IdModuleModel module) {
        return targetDirectory.resolve(module.getClassName()).resolve("Module.java");
    }

    protected Path getNodeFile(Path targetDirectory, IdNodeModel node) {
        String path = node.getClassName() + "Node";
        return targetDirectory.resolve(path.replaceAll("\\.", "/") + ".java");
    }

    protected String getNodeFullyQualifiedName(String node) {
        return getPackage() + "." + node + "Node";
    }

    protected String getModuleFullyQualifiedName(String module) {
        return getPackage() + "." + module + ".Module";
    }

}
