package fr.ird.observe.toolkit.maven.plugin.navigation.validation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.id.IdModuleModel;
import fr.ird.observe.spi.navigation.model.id.IdProjectModel;
import fr.ird.observe.toolkit.maven.plugin.navigation.NavigationGenerateSupport;
import io.ultreia.java4all.lang.Strings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Edit id model contains only root open and open nodes.
 * <p>
 * Created on 31/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class GenerateValidationModelSupport extends NavigationGenerateSupport {

    @Override
    protected final void generate(Path targetDirectory, String dtoRootPackage, MetaModelSupport descriptor) throws IOException {
        IdProjectModel model = descriptor.getIdProjectModel();
        Path rootTargetDirectory = getRootTargetDirectory(targetDirectory);
        createDirectories(rootTargetDirectory);

        getLog().info(String.format("Found %d module(s).", model.getModules().size()));
        getLog().info(String.format("Found %d node(s).", model.getNodes().size()));

        String projectFileName = getClassSimpleName();

        Path projectFile = getProjectFile(rootTargetDirectory, projectFileName);
        getLog().info(String.format("Will generate at %s", projectFile));
        String content = generateProjectContent(model, projectFileName);
        getLog().debug("Content:\n" + content);
        store(projectFile, content);
    }

    protected abstract String getPackage();

    public abstract String getProjectTemplate();

    protected abstract String generateMethod(String dtoFullyQualifiedName, String moduleName, String typePrefix, String capitalizeModuleName);

    protected abstract String getClassSimpleName();

    protected String generateProjectContent(IdProjectModel model, String projectFileName) {
        List<String> methods = new LinkedList<>();
        for (IdModuleModel module : model.getModules()) {
            String moduleName = module.getClassName();
            String capitalizeModuleName = Strings.capitalize(moduleName);
            for (Map.Entry<String, String> entry : module.getNodes().entrySet()) {
                String type = entry.getValue();
                String dtoFullyQualifiedName = getDtoFullyQualifiedName("data." + type);
                String typePrefix = Strings.capitalize(entry.getKey());
                String method = generateMethod(dtoFullyQualifiedName, moduleName, typePrefix, capitalizeModuleName);
                methods.add(method);
            }
        }
        return String.format(getProjectTemplate(),
                             getPackage(),
                             getClass().getName(),
                             new Date(),
                             projectFileName,
                             String.join("", methods));
    }

    protected Path getRootTargetDirectory(Path targetDirectory) {
        return targetDirectory.resolve(getPackage().replaceAll("\\.", File.separator));
    }

    protected Path getProjectFile(Path targetDirectory, String projectFileName) {
        return targetDirectory.resolve(projectFileName + ".java");
    }

}
