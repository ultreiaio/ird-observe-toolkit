package fr.ird.observe.toolkit.maven.plugin.service;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.services.service.MethodCredential;
import io.ultreia.java4all.http.spi.SpiHelper;
import io.ultreia.java4all.http.spi.model.ImportManager;
import io.ultreia.java4all.http.spi.model.MethodDescription;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created on 26/12/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.3
 */
@SuppressWarnings("unused")
public class ServiceLocalMethodDescriptionImpl implements MethodDescription {

    private final String name;
    private final Method method;
    private final String returnType;
    private final String returnInvocation;
    private final List<Class<?>> exceptions;
    private final List<String> parameterNames;
    private final List<String> parameterTypes;
    private final List<String> nullableParameterNames;
    private final Permission methodeCredentials;
    private final boolean write;
    private final boolean noTransaction;

    public <S> ServiceLocalMethodDescriptionImpl(ImportManager importManager, Class<S> serviceType, boolean anonymous, Method method, Map<String, String> currentMapping) {
        this.name = method.getName();
        this.method = method;

        String returnTypePrefix = importManager.getGenericDefinitionOfReturnType(method);
        String simpleReturnType = importManager.importReturnType(method, currentMapping);
        this.returnType = returnTypePrefix + simpleReturnType;

        parameterNames = new LinkedList<>();
        parameterTypes = new LinkedList<>();
        nullableParameterNames = new LinkedList<>();
        for (java.lang.reflect.Parameter parameter : method.getParameters()) {
            String name = parameter.getName();
            parameterNames.add(name);
            parameterTypes.add(importManager.importParameterType(parameter, currentMapping));
            if (SpiHelper.nullable(parameter)) {
                nullableParameterNames.add(name);
            }
        }
        this.returnInvocation = returnType.contains("void") ? "" : "return ";
        this.exceptions = MethodDescription.getExceptions(method, importManager);
        this.methodeCredentials = Optional.ofNullable(method.getAnnotation(MethodCredential.class)).map(MethodCredential::value).orElse(null);
        this.write = SpiHelper.write(method);
        this.noTransaction = anonymous;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getReturnType() {
        return returnType;
    }

    @Override
    public String getParametersDefinition() {
        return null;
    }

    @Override
    public List<Class<?>> getExceptions() {
        return exceptions;
    }

    @Override
    public boolean isWrite() {
        return write;
    }

    public Permission getMethodeCredentials() {
        return methodeCredentials;
    }

    public String getParametersInvocation() {
        return null;
    }

    public String getReturnInvocation() {
        return returnInvocation;
    }

    public List<String> getParameterNames() {
        return parameterNames;
    }

    public List<String> getParameterTypes() {
        return parameterTypes;
    }

    public boolean isNullableParameterName(String parameterName) {
        return nullableParameterNames.contains(parameterName);
    }

    public boolean isNoTransaction() {
        return noTransaction;
    }
}
