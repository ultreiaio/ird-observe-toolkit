package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;

/**
 * Created on 01/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateSelectionModel extends GenerateTreeModelSupport {
    public static final String TYPE = "selection";
    public static final String NODE_TEMPLATE = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.tree.selection.SelectionTreeNode;\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"%5$s\", date = \"%2$s\")\n" +
            "public class %3$s extends SelectionTreeNode {\n\n" +
            "    public %3$s() {\n" +
            "        super(new %3$sBean());\n" +
            "    }\n\n" +
            "%4$s" +
            "}\n";
    public static final String BEAN_TEMPLATE = "" +
            "package %1$s;\n\n" +
            "import com.google.common.collect.ImmutableMap;\n" +
            "import fr.ird.observe.spi.module.ObserveBusinessProject;\n" +
            "import fr.ird.observe.navigation.tree.NavigationNodeType;\n" +
            "import fr.ird.observe.navigation.tree.ToolkitTreeNodeBeanState;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.Map;\n" +
            "import java.util.function.Supplier;\n\n" +
            "@Generated(value = \"%6$s\", date = \"%2$s\")\n" +
            "public class %3$s extends fr.ird.observe.navigation.tree.selection.bean.%4$sSelectionTreeNodeBean {\n\n" +
            "    @Override\n" +
            "    protected final Map<ToolkitTreeNodeBeanState<?>, Object> defaultStates() {\n" +
            "        return %5$s;\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected Supplier<ObserveBusinessProject> getProjectSupplier() {\n" +
            "        return ObserveBusinessProject::get;\n" +
            "    }\n}\n";
    public static final String INTERCEPTOR_TEMPLATE = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.tree.ToolkitTreeNode;\n" +
            "import fr.ird.observe.spi.navigation.tree.ToolkitTreeNodeInterceptor;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.Objects;\n\n" +
            "@Generated(value = \"%6$s\", date = \"%2$s\")\n" +
            "public interface %3$s extends ToolkitTreeNodeInterceptor {\n" +
            "\n" +
            "    static void intercept(%3$s provider, ToolkitTreeNode node) {\n" +
            "        switch (Objects.requireNonNull(node.getClass().getName())) {\n" +
            "            %4$s\n" +
            "        }\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    default void intercept(ToolkitTreeNode node) {\n" +
            "        intercept(this, node);\n" +
            "    }\n\n" +
            "    %5$s\n" +
            "}";
    public static final String TREE_MODEL_TEMPLATE = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.tree.selection.SelectionTreeConfig;\n" +
            "import fr.ird.observe.navigation.tree.selection.SelectionTreeModelSupport;\n" +
            "import fr.ird.observe.spi.module.ObserveBusinessProject;\n" +
            "import javax.annotation.Generated;\n" +
            "@Generated(value = \"%4$s\", date = \"%2$s\")\n" +
            "public class %3$s extends SelectionTreeModelSupport<RootSelectionTreeNode> {\n\n" +
            "    public %3$s() {\n" +
            "        super(ObserveBusinessProject.get(), new SelectionTreeConfig(), RootSelectionTreeNode::new);\n" +
            "    }\n\n" +
            "}\n";
    public static final String TREE_TEMPLATE = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.navigation.tree.selection.SelectionTreeSupport;\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"%4$s\", date = \"%2$s\")\n" +
            "public class %3$s extends SelectionTreeSupport<RootSelectionTreeNode, %3$sModel> {\n\n" +
            "    public %3$s() {\n" +
            "        super(new %3$sModel());\n" +
            "    }\n" +
            "}\n";

    public GenerateSelectionModel() {
        super(TYPE);
    }

    @Override
    public String getNodeTemplate() {
        return NODE_TEMPLATE;
    }

    @Override
    public String getBeanTemplate() {
        return BEAN_TEMPLATE;
    }

    @Override
    public String getInterceptorTemplate() {
        return INTERCEPTOR_TEMPLATE;
    }

    @Override
    public String getTreeModelTemplate() {
        return TREE_MODEL_TEMPLATE;
    }

    @Override
    public String getTreeTemplate() {
        return TREE_TEMPLATE;
    }

    @Override
    public TreeProjectModel getProjectModel(MetaModelSupport descriptor) {
        return descriptor.getTreeSelectionProjectModel();
    }
}
