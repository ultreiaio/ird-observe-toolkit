package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Extends this mojo to get a runner managed by maven.
 * <p>
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public abstract class ExecuteRunnerMojoSupport extends ToolboxMojoSupport {

    /**
     * To activate verbose mode.
     */
    @Parameter(property = "runner.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;
    /**
     * To skip the goal.
     */
    @Parameter(property = "runner.skip", defaultValue = "false")
    private boolean skip;
    /**
     * Java source root directory.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/java", required = true)
    private File sourceRoot;
    /**
     * Resources root directory.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/resources", required = true)
    private File resourcesRoot;
    /**
     * Filtered resources root directory.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/filtered-resources", required = true)
    private File filteredResourcesRoot;
    /**
     * Web resources root directory.
     */
    @Parameter(defaultValue = "${project.basedir}/src/main/webResources", required = true)
    private File webResourcesRoot;
    /**
     * Compile classes directory.
     */
    @Parameter(defaultValue = "${project.build.directory}/classes", required = true)
    private File compileRoot;
    /**
     * Java generated root directory.
     */
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/java", required = true)
    private File targetRoot;
    /**
     * Optional i18n getter file.
     */
    @Parameter(property = "runner.i18n")
    private String i18n;
    /**
     * To add project class path to the plugin class path.
     */
    @Parameter(property = "runner.addProjectClassPath", defaultValue = "true")
    private boolean addProjectClassPath;
    /**
     * To add Java generated source root directory to class-path.
     */
    @Parameter(property = "runner.addTargetRootSource", defaultValue = "false")
    private boolean addTargetRootSource;
    private boolean useTemporaryPath = true;
    private Path temporaryPath;

    protected abstract MojoRunnable createRunner() throws IOException;

    @Override
    protected final void doAction() throws Exception {
        I18nKeySet getterFile = null;
        I18nModule i18nModule = null;
        if (i18n != null) {
            i18nModule = I18nModule.forGetter(getProject().getProperties());
            getterFile = i18nModule.getModuleKeySet(i18n);
        }
        if (useTemporaryPath) {
            Path path = getCompileRoot().toPath();
            temporaryPath = path.getParent().resolve("tmp-" + System.nanoTime());
            Files.createDirectories(temporaryPath);
            System.setProperty("java.io.tmpdir", temporaryPath.toString());
        } else {
            String property = System.getProperty("java.io.tmpdir");
            temporaryPath = new File(property).toPath();
        }
        getLog().debug(String.format("Temporary path: %s", temporaryPath));
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            URLClassLoader urlClassLoader = initClassLoader(getProject(), compileRoot == null ? sourceRoot : compileRoot, false, false, true, true, addProjectClassPath);
            Thread.currentThread().setContextClassLoader(urlClassLoader);
            doAction(getterFile, urlClassLoader);
            if (addTargetRootSource) {
                addCompileSourceRoots(targetRoot);
            }
        } finally {
            if (getterFile != null) {
                i18nModule.storeModuleKeySet(getterFile);
            }
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }

    public void setUseTemporaryPath(boolean useTemporaryPath) {
        this.useTemporaryPath = useTemporaryPath;
    }

    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        MojoRunnable runner = createRunner();
        prepare(runner, getterFile, classLoader);
        getLog().info("Will execute runner: " + runner);
        runner.run();
    }

    protected void prepare(MojoRunnable runner, I18nKeySet getterFile, ClassLoader classLoader) {
        runner.setLog(getLog());
        if (getterFile != null) {
            runner.setGetterFile(getterFile);
        }
        runner.setTemporaryPath(getTemporaryPath());
        runner.setVerbose(isVerbose());
        runner.init();
    }

    @Override
    public final boolean isVerbose() {
        return verbose;
    }

    @Override
    public final void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public final boolean isSkip() {
        return skip;
    }

    public File getSourceRoot() {
        return sourceRoot;
    }

    public File getResourcesRoot() {
        return resourcesRoot;
    }

    public File getFilteredResourcesRoot() {
        return filteredResourcesRoot;
    }

    public File getWebResourcesRoot() {
        return webResourcesRoot;
    }

    public File getCompileRoot() {
        return compileRoot;
    }

    public File getTargetRoot() {
        return targetRoot;
    }
}

