package fr.ird.observe.toolkit.maven.plugin.navigation.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;
import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.lang.Strings;

import java.util.Date;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.4
 */
public class ToParentIdProviderTemplate {

    public static final String ID_PROVIDER_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.dto.ToolkitParentIdDtoBean;\n" +
            "import fr.ird.observe.entities.ObserveTopiaPersistenceContext;\n\n" +
            "import javax.annotation.Generated;\n\n" +
            "@Generated(value = \"%5$s\", date = \"%2$s\")\n" +
            "public abstract class %3$sImpl implements %3$s {\n\n" +
            "    protected abstract ObserveTopiaPersistenceContext getPersistenceContext();\n\n" +
            "    %4$s\n" +
            "}";
    public static final String SINGLE_METHOD = "" +
            "   @Override\n" +
            "    public ToolkitParentIdDtoBean get%1$sParent(String id) {\n" +
            "        return %2$s.%3$s.getParentId(getPersistenceContext(), id);\n" +
            "    }\n\n";
    private final GeneratePersistence generator;
    StringBuilder idMethodsBuilder = new StringBuilder();

    public ToParentIdProviderTemplate(GeneratePersistence generator) {
        this.generator = generator;
    }

    public void register(BusinessProject businessProject, ParentProjectModel projectModel, String node) {

        String simpleName = ProjectPackagesDefinition.getSimpleName(node);

        String getterName = projectModel.getterName(node);
        String packageName = ProjectPackagesDefinition.getPackageName(node);

        Class<? extends BusinessDto> dtoType = Objects2.forName(generator.getDtoFullyQualifiedName(node));
        Class<? extends BusinessDto> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);

        String spi = "SPI";
        if (!dtoType.equals(mainDtoType)) {
            spi = Strings.convertToConstantName(simpleName + "_SPI");
            packageName = mainDtoType.getPackageName().replace(BusinessDto.class.getPackageName() + ".", "");
            simpleName = mainDtoType.getSimpleName().replace("Dto", "");
        }

        String entityQualifiedName = String.format("fr.ird.observe.entities.%s.%s", packageName, simpleName);
        idMethodsBuilder.append(" ").append(String.format(SINGLE_METHOD, getterName, entityQualifiedName, spi));

    }

    public String getPackageName() {
        return "fr.ird.observe.spi.navigation.parent";
    }

    public String getClassSimpleName(MetaModelSupport descriptor) {
        return descriptor.getName() + "ToParentIdProviderImpl";
    }

    public String generateContent(String packageName, String className) {
        Date now = new Date();
        return String.format(ID_PROVIDER_MODEL
                , packageName
                , now
                , className.substring(0, className.length() - 4) // we use the version without Impl in template
                , idMethodsBuilder.substring(1).trim()
                , getClass().getName());
    }
}
