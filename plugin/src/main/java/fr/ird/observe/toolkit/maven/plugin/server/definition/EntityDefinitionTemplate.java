package fr.ird.observe.toolkit.maven.plugin.server.definition;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.ird.observe.dto.data.DataDto;
import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.reference.DataDtoReference;
import fr.ird.observe.dto.reference.ReferentialDtoReference;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.entities.Entity;
import fr.ird.observe.entities.data.DataEntity;
import fr.ird.observe.entities.referential.ReferentialEntity;
import fr.ird.observe.spi.PersistenceBusinessProject;
import fr.ird.observe.spi.ProjectPackagesDefinition;
import org.nuiton.topia.persistence.TagValues;
import fr.ird.observe.spi.context.DataDtoEntityContext;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.context.ReferentialDtoEntityContext;
import fr.ird.observe.spi.doc.EntityToDescription;
import fr.ird.observe.spi.module.BusinessDataPackage;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessProjectVisitor;
import fr.ird.observe.spi.module.BusinessReferentialPackage;
import fr.ird.observe.spi.module.BusinessSubModule;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class EntityDefinitionTemplate {

    private final Log log;
    private final Path targetDirectory;
    private final Gson gson;
    private final Date now;
    boolean verbose;
    int count = 0;

    public EntityDefinitionTemplate(Log log, Path targetDirectory, Gson gson) {
        this.log = log;
        this.targetDirectory = targetDirectory;
        this.gson = gson;
        this.now = new Date();

    }

    public void generate(BusinessProject businessProject) throws IOException {
        count = 0;
        TagValues persistenceTagValues = TagValues.persistence(businessProject.getName(), ProjectPackagesDefinition::cleanType);
        Map<Pair<String, String>, String> dateTagValues = persistenceTagValues.getAttributeTagValues("hibernateAttributeType.java.util.Date");

        BusinessProjectVisitor visitor = new BusinessProjectVisitor() {

            @Override
            public void enterSubModuleDataType(BusinessModule module, BusinessSubModule subModule, BusinessDataPackage dataPackage, Class<? extends DataDto> dtoType) {
                Class<?> mainDtoType = businessProject.getMapping().getMainDtoType(dtoType);
                if (mainDtoType != dtoType && !(EditableDto.class.isAssignableFrom(dtoType))) {
                    return;
                }
                Path path = BusinessProject.getDtoVariablePath(targetDirectory, module, subModule, dtoType);
                DataDtoEntityContext<? extends DataDto, DataDtoReference, DataEntity, ?> spi = PersistenceBusinessProject.fromDataDto(dtoType);
                try {
                    generateDefinition(path, spi, dateTagValues);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }

            @Override
            public void enterSubModuleReferentialType(BusinessModule module, BusinessSubModule subModule, BusinessReferentialPackage referentialPackage, Class<? extends ReferentialDto> dtoType) {
                Path path;
                if (subModule.getName().equals(module.getName())) {
                    path = BusinessProject.getDtoVariablePath(targetDirectory, module, dtoType);
                } else {
                    path = BusinessProject.getDtoVariablePath(targetDirectory, module, subModule, dtoType);
                }
                ReferentialDtoEntityContext<? extends ReferentialDto, ReferentialDtoReference, ReferentialEntity, ?> spi = PersistenceBusinessProject.fromReferentialDto(dtoType);
                try {
                    generateDefinition(path, spi, dateTagValues);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        };
        businessProject.accept(visitor);
        log.info(String.format("Generate %d definition(s).", count));
    }

    protected void generateDefinition(Path path, DtoEntityContext<?, ?, ?, ?> spi, Map<Pair<String, String>, String> dateTagValues) throws IOException {
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        if (verbose) {
            log.info("generate definition at: " + path);
        }
        count++;
        Entity newEntity = spi.newEntity(now);
        Map<String, String> dateAttributes = getDateFlavors(spi, dateTagValues);
        Map<String, String> description = EntityToDescription.generate(newEntity, dateAttributes);
        description.remove(Entity.PROPERTY_LAST_UPDATE_DATE);
        Path descriptionPath = path.resolve(DtoEntityContext.DEFINITION_CLASSIFIER);
        String json = gson.toJson(description);
        Files.write(descriptionPath, json.getBytes(StandardCharsets.UTF_8));
    }

    private Map<String, String> getDateFlavors(DtoEntityContext<?, ?, ?, ?> spi, Map<Pair<String, String>, String> dateTagValues) {
        Map<String,String> dateAttributes = new LinkedHashMap<>();
        String entityName = spi.toEntityType().getName();
        for (Map.Entry<Pair<String, String>, String> pairStringEntry : dateTagValues.entrySet()) {
            if (pairStringEntry.getKey().getKey().equals(entityName)) {
                dateAttributes.put(pairStringEntry.getKey().getValue(), Strings.capitalize(pairStringEntry.getValue()));
            }
        }
        return dateAttributes;
    }
}
