package fr.ird.observe.toolkit.maven.plugin.server.html;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import io.ultreia.java4all.util.ServiceLoaders;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1.0
 */
public class Runner extends MojoRunnable {
    public static final String PACKAGE = "fr.ird.observe.entities";
    private Path targetDirectory;

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(targetDirectory);
    }

    @Override
    public void run() {
        getLog().info(String.format("Will generate at %s", targetDirectory));
        try {
            BusinessProject businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);
            TemplateHelper templateHelper = new TemplateHelper();

            TemplateModelBuilder modelBuilder = new TemplateModelBuilder(templateHelper);
            TemplateModel model = modelBuilder.build(businessProject);
            new Template(getLog(), targetDirectory, templateHelper).generate(model);
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate model", e);
        }
    }

}


