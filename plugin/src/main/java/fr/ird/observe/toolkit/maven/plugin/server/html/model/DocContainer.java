package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 01/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.34
 */
public abstract class DocContainer extends DocElement {

    private final List<DocRequest> extraRequests = new LinkedList<>();

    public DocContainer(DocElement parent, String name) {
        setName(name);
        setParent(parent);
        if (parent == null) {
            setPath(name);
        } else {
            setPath(parent.getPath() + "/" + name);
        }
    }

    public DocContainer(String path, String name) {
        setName(name);
        setPath(path + "/" + name);
    }

    protected abstract List<DocRequest> getChildrenRequests();

    @Override
    public List<DocRequest> getRequests() {
        ArrayListMultimap<String, DocRequest> map = ArrayListMultimap.create();
        extraRequests.forEach(r -> {
            r.setRelativeParent(null);
            map.put(r.getDataScope(), r);
        });
        List<DocRequest> collect = getChildrenRequests();
        collect.forEach(r -> {
            r.setRelativeParent(this);
            map.put(r.getDataScope(), r);
        });
        LinkedList<DocRequest> result = new LinkedList<>();
        Set<String> dataScopes = map.keySet();
        retainsRequests("project", map, result);
        retainsRequests("module", map, result);
        retainsRequests("package", map, result);
        retainsRequests("type", map, result);
        retainsRequests("connexion", map, result);
        for (String dataScope : dataScopes) {
            result.addAll(map.get(dataScope));
        }
        return result;
    }

    protected void retainsRequests(String dataScope, ArrayListMultimap<String, DocRequest> map, LinkedList<DocRequest> result) {
        List<DocRequest> docRequests = map.removeAll(dataScope);
        if (!docRequests.isEmpty()) {
            result.addAll(docRequests);
        }
    }

    public final List<DocRequest> getExtraRequests() {
        return extraRequests;
    }

    public final DocExtraRequest addExtraRequest(Requests type, String dataScope, String data, String title, Path fixturesPath) {
        DocExtraRequest request = new DocExtraRequest(this, type, dataScope, data);
        if (title != null) {
            request.setTitle(title);
        }
        extraRequests.add(request);
        request.addExample(Objects.requireNonNull(fixturesPath));
        return request;
    }

}
