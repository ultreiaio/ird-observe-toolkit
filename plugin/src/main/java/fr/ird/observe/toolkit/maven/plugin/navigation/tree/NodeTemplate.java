package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelNodeLinkMultiplicity;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import io.ultreia.java4all.lang.Strings;

import java.util.Date;
import java.util.Objects;

import static fr.ird.observe.toolkit.maven.plugin.navigation.tree.GenerateTreeModelSupport.BEAN;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class NodeTemplate {
    public static final String GET_USER_OBJECT = "" +
            "    @Override\n" +
            "    public final %1$s getUserObject() {\n" +
            "        return (%1$s) super.getUserObject();\n" +
            "    }\n\n";
    public static final String PARENT_MODEL = "" +
            "    @Override\n" +
            "    public final %1$s getParent() {\n" +
            "        return (%1$s) super.getParent();\n" +
            "    }\n\n";
    public static final String CAPABILITY_SINGLE = "" +
            "    public final %1$s get%2$s() {\n" +
            "        return find(%1$s.class);\n" +
            "    }\n\n";
    public static final String CAPABILITY_MULTIPLE = "" +
            "    public final %1$s get%2$s(fr.ird.observe.dto.ToolkitIdDtoBean parameter) {\n" +
            "        return find(%1$s.class, parameter);\n" +
            "    }\n\n";
    public static final String DEFAULT_CAPABILITY_MULTIPLE = "" +
            "    public final %1$s get%2$s(fr.ird.observe.dto.ToolkitIdDtoBean parameter) {\n" +
            "        return get%2$s().getChildren(parameter);\n" +
            "    };\n\n";

    private final GenerateTreeModelSupport generator;

    public NodeTemplate(GenerateTreeModelSupport generator) {
        this.generator = generator;
    }

    public String generateContent(TreeProjectModel project, TreeNodeModel node) {
        StringBuilder capabilityBuilder = new StringBuilder();
        String beanSimpleName = generator.getNodeFullyQualifiedName(node.getClassName()) + BEAN;
        capabilityBuilder.append(String.format(GET_USER_OBJECT, beanSimpleName));
        if (!node.isRoot()) {
            if (!node.withMultipleParent()) {
                // unique parent
                String parentClassName = node.getParentClassName();
                String parentName = generator.getNodeFullyQualifiedName(parentClassName);
                capabilityBuilder.append(String.format(PARENT_MODEL, parentName));
            }
        }
        for (TreeNodeLink link : node.getChildren()) {
            String type = generator.getNodeFullyQualifiedName(link.getTargetClassName());
            String propertyName = link.getPropertyName();
            String getterName = Strings.capitalize(propertyName);
            if (link.getMultiplicity() == MetaModelNodeLinkMultiplicity.MANY) {
                capabilityBuilder.append(String.format(CAPABILITY_MULTIPLE, type, getterName));
            } else {
                capabilityBuilder.append(String.format(CAPABILITY_SINGLE, type, getterName));
                TreeNodeModel realNode = project.getNode(link.getTargetClassName()).orElse(null);
                if (Objects.requireNonNull(realNode).isOpenList()) {
                    capabilityBuilder.append(String.format(DEFAULT_CAPABILITY_MULTIPLE, type.replace(TreeNodeModel.LIST, ""), getterName));
                }
            }
        }
        return String.format(generator.getNodeTemplate()
                , generator.getNodePackageName(node)
                , new Date()
                , node.getSimpleName() + generator.getSuffix()
                , capabilityBuilder
                , getClass().getName());
    }
}
