package fr.ird.observe.toolkit.maven.plugin.server.html;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import fr.ird.observe.toolkit.maven.plugin.server.TemplateHelper;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocElement;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocExtraRequest;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocModule;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocPackage;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocProject;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocRequest;
import fr.ird.observe.toolkit.maven.plugin.server.html.model.DocType;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import org.apache.maven.plugin.logging.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1.0
 */
public class Template extends fr.ird.observe.toolkit.maven.plugin.server.Template {

    private final Map<String, Mustache> pageTemplates;

    public Template(Log log, Path targetDirectory, TemplateHelper templateHelper) {
        super(log, "", "", targetDirectory);
        pageTemplates = new TreeMap<>();
        templateHelper.loadPage(pageTemplates, DocProject.class);
        templateHelper.loadPage(pageTemplates, DocModule.class);
        templateHelper.loadPage(pageTemplates, DocPackage.class);
        templateHelper.loadPage(pageTemplates, DocType.class);
        templateHelper.loadPage(pageTemplates, DocRequest.class);
        pageTemplates.put(DocExtraRequest.class.getSimpleName(), pageTemplates.get(DocRequest.class.getSimpleName()));

        I18n.init(new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration()), Locale.UK);
    }

    public void generate(TemplateModel templateModel) throws IOException {
        Path docDirectory = targetDirectory.resolve("doc").resolve("api").resolve("public");

        Path requestPath = docDirectory.resolve("requests.text");

        generate(templateModel, templateModel.getInitProject(), docDirectory.resolve("init"));
        generate(templateModel, templateModel.getReferentialProject(), docDirectory.resolve("referential"));
        generate(templateModel, templateModel.getDataProject(), docDirectory.resolve("data"));

        Files.deleteIfExists(requestPath);
        int length = requestPath.getParent().toFile().getAbsolutePath().length() + 1;
        Files.write(requestPath, templateModel.getRequests().stream().map(s -> s.substring(length)).collect(Collectors.toList()));
    }

    protected void generate(TemplateModel templateModel, DocProject docProject, Path rootDirectory) throws IOException {
        generate(rootDirectory, docProject);
        for (DocModule module : docProject.getModules()) {
            Path moduleFile = rootDirectory.resolve(module.getName());
            generate(moduleFile, module);
            addRequests(templateModel, moduleFile, module.getExtraRequests());
            for (DocPackage aPackage : module.getPackages()) {
                Path packageFile = moduleFile.resolve(aPackage.getName());
                generate(packageFile, aPackage);
                addRequests(templateModel, packageFile, aPackage.getExtraRequests());
                for (DocType type : aPackage.getTypes()) {
                    Path typeFile = packageFile.resolve(type.getName());
                    generate(typeFile, type);
                    for (DocRequest request : type.getRequests()) {
                        String requestName = request.getName();
                        Path requestPath = typeFile.resolve(requestName);
                        addRequest(templateModel, request, requestPath);
                        generate(requestPath, request);
                    }
                }
            }
        }
        addRequests(templateModel, rootDirectory, docProject.getExtraRequests());
    }

    protected void addRequests(TemplateModel templateModel, Path rootDirectory, Collection<DocRequest> requests) throws IOException {
        for (DocRequest request : requests) {
            String requestName = request.getName();
            Path requestPath = rootDirectory.resolve(requestName);
            addRequest(templateModel, request, requestPath);
            generate(requestPath, request);
        }
    }

    protected void addRequest(TemplateModel templateModel, DocRequest request, Path requestPath) throws IOException {
        Path requestFile = requestPath.resolve("request.json");
        templateModel.requests.add(requestFile.toFile().getAbsolutePath());
        if (Files.notExists(requestPath)) {
            Files.createDirectories(requestPath);
        }
        String requestContent = request.getRequestPrototypeContent();
        Files.deleteIfExists(requestPath.resolve("response.json"));
        Files.deleteIfExists(requestFile);
        if (requestContent != null) {
            Files.write(requestFile, requestContent.getBytes(StandardCharsets.UTF_8));
        }
        String exampleRequestContent = request.getExampleRequestContent();
        if (exampleRequestContent != null) {
            Path requestExampleFile = requestPath.resolve("example-request.json");
            Files.deleteIfExists(requestExampleFile);
            Files.write(requestExampleFile, exampleRequestContent.getBytes(StandardCharsets.UTF_8));
        }
        String exampleResponseContent = request.getExampleResponseContent();
        if (exampleResponseContent != null) {
            Path responseExampleFile = requestPath.resolve("example-response.json");
            Files.deleteIfExists(responseExampleFile);
            Files.write(responseExampleFile, exampleResponseContent.getBytes(StandardCharsets.UTF_8));
        }
    }

    protected void addValidation(Path requestPath, DocType request) throws IOException {
        request.storeValidation(requestPath);
    }

    protected void generate(Path targetPath, DocElement model) throws IOException {
        if (!Files.exists(targetPath)) {
            Files.createDirectories(targetPath);
        }
        Mustache mustache = pageTemplates.get(model.getClass().getSimpleName());
        if (model instanceof DocType) {
            Files.write(targetPath.resolve("definition.json"), ((DocType) model).getTypeDefinition().getBytes(StandardCharsets.UTF_8));
        }
        try (BufferedWriter writer = Files.newBufferedWriter(targetPath.resolve("index.html"))) {
            mustache.execute(writer, model);
        }
        if (model instanceof DocType) {
            addValidation(targetPath, (DocType) model);
        }
    }


}
