package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import fr.ird.observe.toolkit.maven.plugin.server.html.TemplateModelBuilder;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 03/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.21
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = "toolkit.doc.api.public.@CLASS_SIMPLE_NAME@.@NAME@")
public enum Requests {
    //FIXME Replace this a generated model from inspected service methods.
    GetAllForProject("GET", "?" + TemplateModelBuilder.config.substring(1), true, false),
    GetAllForModule("GET", "?" + TemplateModelBuilder.config.substring(1), true, false),
    GetAllForPackage("GET", "?" + TemplateModelBuilder.config.substring(1), true, false),
    GetAll("GET", "/all?" + TemplateModelBuilder.orders.substring(1) + TemplateModelBuilder.config, true, false),
    GetSome("GET", "?" + TemplateModelBuilder.filters.substring(1) + TemplateModelBuilder.config, true, false),
    GetOne("GET", "/{id}?" + TemplateModelBuilder.config.substring(1), true, false),
    GenerateId("GET", "/generateId/{number}?", false, false),
    Create("POST", "?", true, true),
    Update("PUT", "/{id}?", true, true),
    Delete("DELETE", "/{id}?", true, false),
    Ping("GET", "/ping?", false, false),
    Open("GET", "/open?config.modelVersion=XXX&config.login=XXX&config.password=XXX&config.databaseName=XXX&referentialLocale=XXX", false, false),
    Close("GET", "/close?", true, false),
    Information("GET", "/information?", true, false);

    private final String method;
    private final String query;
    private final boolean needAuthenticationToken;
    private final boolean needPayload;
    private final boolean witIdInUrl;
    private final Map<String, String> headers;

    Requests(String method, String query, boolean needAuthenticationToken, boolean needPayload) {
        this.method = method;
        this.query = query;
        this.needAuthenticationToken = needAuthenticationToken;
        this.needPayload = needPayload;
        this.headers = new LinkedHashMap<>();
        if (needAuthenticationToken) {
            headers.put("authenticationToken", "XXX");
        }
        if (needPayload) {
            headers.put("Content-Type", "application/json");
        }
        this.witIdInUrl = query.startsWith("/{id}");
    }

    public boolean witIdInUrl() {
        return witIdInUrl;
    }

    public boolean needAuthenticationToken() {
        return needAuthenticationToken;
    }

    public boolean needBody() {
        return needPayload;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getMethod() {
        return method;
    }

    public String getQuery() {
        return query;
    }

    public String getTitle(String type) {
        return I18n.t(RequestsI18n.getLabelKey(this), type);
    }
}
