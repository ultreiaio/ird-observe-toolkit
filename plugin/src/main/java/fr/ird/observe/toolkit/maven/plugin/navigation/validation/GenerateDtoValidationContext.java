package fr.ird.observe.toolkit.maven.plugin.navigation.validation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateDtoValidationContext extends GenerateValidationModelSupport {

    public static final String METHOD_TEMPLATE = "" +
            "    protected abstract Function<String, %1$s> %2$s();\n\n" +
            "    public final %1$s %3$s() {\n" +
            "        return getDto(%2$s(), %4$s.getId());\n" +
            "    }\n\n";

    public static final String PROJECT_TEMPLATE = "package %1$s;\n\n" +
            "import fr.ird.observe.dto.validation.ValidationRequestConfiguration;\n" +
            "import fr.ird.observe.navigation.id.Project;\n" +
            "import fr.ird.observe.decoration.DecoratorService;\n\n" +
            "import fr.ird.observe.validation.ValidationContextSupport;\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.function.Function;\n\n" +
            "@Generated(value = \"%2$s\", date = \"%3$s\")\n" +
            "public abstract class %4$s extends ValidationContextSupport {\n\n" +
            "    private final Project selectModel;\n\n" +
            "    protected %4$s(ValidationRequestConfiguration configuration, DecoratorService decoratorService, Project selectModel) {\n" +
            "        super(configuration, decoratorService);\n" +
            "        this.selectModel = selectModel;\n" +
            "    }\n\n" +
            "    public final Project getSelectModel() {\n" +
            "        return selectModel;\n" +
            "    }\n\n" +
            "%5$s" +
            "}";

    protected String getClassSimpleName() {
        return "GeneratedDtoValidationContextSupport";
    }

    protected String generateMethod(String dtoFullyQualifiedName, String moduleName, String typePrefix, String capitalizeModuleName) {
        return String.format(METHOD_TEMPLATE,
                             dtoFullyQualifiedName,
                             moduleName + typePrefix + "Function",
                             "getCurrent" + capitalizeModuleName + typePrefix,
                             "getSelectModel().get" + capitalizeModuleName + "().get" + typePrefix + "()");
    }

    @Override
    protected String getPackage() {
        return "fr.ird.observe.dto.validation";
    }

    @Override
    public String getProjectTemplate() {
        return PROJECT_TEMPLATE;
    }

}
