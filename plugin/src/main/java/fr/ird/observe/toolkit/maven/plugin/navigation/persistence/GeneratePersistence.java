package fr.ird.observe.toolkit.maven.plugin.navigation.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.ProjectPackagesDefinition;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import fr.ird.observe.spi.navigation.parent.ParentLink;
import fr.ird.observe.spi.navigation.parent.ParentProjectModel;
import fr.ird.observe.toolkit.maven.plugin.navigation.NavigationGenerateSupport;
import io.ultreia.java4all.util.ServiceLoaders;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created on 04/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GeneratePersistence extends NavigationGenerateSupport {

    @Override
    protected ProjectPackagesDefinition initProjectPackageDefinition() {
        businessProject = ServiceLoaders.loadUniqueService(BusinessProject.class);
        return businessProject.getPackagesDefinition();
    }

    @Override
    protected void generate(Path targetDirectory, String dtoRootPackage, MetaModelSupport descriptor) throws IOException {
        generateToParentIdProvider(targetDirectory, descriptor);
        generateBuildChildInterceptor(targetDirectory, descriptor);
    }

    private void generateBuildChildInterceptor(Path targetDirectory, MetaModelSupport descriptor) throws IOException {
        BuildChildrenInterceptorTemplate template = new BuildChildrenInterceptorTemplate(this);

        TreeProjectModel projectModel = descriptor.getTreeNavigationProjectModel();
        for (TreeNodeModel node : projectModel.getNodes()) {
            template.register(projectModel, node);
        }

        String packageName = template.getPackageName();
        String classSimpleName = template.getClassSimpleName(descriptor);

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        getLog().info(String.format("Will generate BuildChildrenInterceptor    at: %s", targetFile));
        String content = template.generateContent(descriptor.getName(), packageName, classSimpleName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private void generateToParentIdProvider(Path targetDirectory, MetaModelSupport descriptor) throws IOException {
        ToParentIdProviderTemplate template = new ToParentIdProviderTemplate(this);

        ParentProjectModel projectModel = descriptor.getParentProjectModel();
        for (ParentLink className : projectModel.getLinks()) {
            template.register(businessProject, projectModel, className.getClassName());
        }

        String packageName = template.getPackageName();
        String classSimpleName = template.getClassSimpleName(descriptor);

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        getLog().info(String.format("Will generate ToParentIdProvider    at: %s", targetFile));
        String content = template.generateContent(packageName, classSimpleName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }
}
