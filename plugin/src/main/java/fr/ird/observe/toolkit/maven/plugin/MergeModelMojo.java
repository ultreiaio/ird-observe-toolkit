package fr.ird.observe.toolkit.maven.plugin;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.persistence.MergeModels;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
@Mojo(name = "merge-model", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyCollection = ResolutionScope.COMPILE)
public class MergeModelMojo extends ExecuteRunnerMojoSupport {

    @Parameter(property = "runner.modelName", required = true)
    private String modelName;

    @Parameter(property = "runner.packageName", required = true)
    private String packageName;

    @Override
    protected MojoRunnable createRunner() {
        MergeModels runner = new MergeModels();
        runner.setModelName(modelName);
        runner.setPackageName(packageName);
        runner.setSourceDirectory(getCompileRoot().toPath());
        runner.setTargetDirectory(getTargetRoot().toPath());
        return runner;
    }
}
