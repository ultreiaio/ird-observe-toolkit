package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.migration.ByMajorMigrationVersionResource;
import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.util.Version;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
@Mojo(name = "tck-migrate-databases", threadSafe = true, defaultPhase = LifecyclePhase.PROCESS_CLASSES)
public class TckMigrateDatabasesMojo extends PersistenceMojoSupport {

    @Parameter(property = "runner.modelVersion", defaultValue = "${persistence.previous.model.version}", required = true)
    private String previousModelVersion;

    @Parameter(property = "runner.archiveFile", defaultValue = "${tck.cache.path}/${tck.filename}", required = true)
    private File archiveFile;

    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        ByMajorMigrationVersionResource.setClassLoader(classLoader);
        ByMajorMigrationVersionResource.setForTck(true);
        try {
            super.doAction(getterFile, classLoader);
        } finally {
            ByMajorMigrationVersionResource.setClassLoader(null);
            ByMajorMigrationVersionResource.setForTck(false);
        }
    }

    @Override
    protected MigrateTckDatabases createRunner() {
        MigrateTckDatabases runner = new MigrateTckDatabases();
        runner.setPreviousModelVersion(Version.valueOf(previousModelVersion));
        runner.setTemporaryPath(getTemporaryPath());
        runner.setModelVersion(getModelVersion());
        runner.setArchiveFile(archiveFile.toPath());
        return runner;
    }
}

