package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;

/**
 * Created at 12/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class RenameForeignKeyTasks {
    private final String prefix;
    private final String oldColumnName;
    private final String newColumnName;
    private List<RenameForeignKeyTask> tasks = List.of();

    public RenameForeignKeyTasks(String prefix, String oldColumnName, String newColumnName) {
        this.prefix = prefix;
        this.oldColumnName = oldColumnName;
        this.newColumnName = newColumnName;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getOldColumnName() {
        return oldColumnName;
    }

    public String getNewColumnName() {
        return newColumnName;
    }

    public List<RenameForeignKeyTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<RenameForeignKeyTask> tasks) {
        this.tasks = tasks;
    }

    public List<String> toSql(boolean h2) {
        List<String> statements = new LinkedList<>();
        for (RenameForeignKeyTask task : getTasks()) {
            statements.add(task.toSql(h2, getOldColumnName(), getNewColumnName()));
        }
        return statements;
    }
}
