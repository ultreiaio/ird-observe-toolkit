package fr.ird.observe.toolkit.maven.plugin.server;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

/**
 * Created on 06/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public abstract class Template {

    protected final Date now;
    protected final Log log;
    protected final String prefix;
    protected final String suffix;
    protected final Path targetDirectory;

    public Template(Log log, String prefix, String suffix, Path targetDirectory) {
        this.log = log;
        this.prefix = prefix;
        this.suffix = suffix;
        this.targetDirectory = targetDirectory;
        this.now = new Date();
    }

    public Path store(Path targetDirectory, String packageName, String simpleName, String content) throws IOException {

        Path targetFile = targetDirectory.resolve(packageName.replaceAll("\\.", File.separator)).resolve(simpleName + ".java");
        if (Files.notExists(targetFile.getParent())) {
            Files.createDirectories(targetFile.getParent());
        }
        log.debug(String.format("Will generate at %s", targetFile));
        Files.write(targetFile, content.getBytes(StandardCharsets.UTF_8));
        return targetFile;
    }
}
