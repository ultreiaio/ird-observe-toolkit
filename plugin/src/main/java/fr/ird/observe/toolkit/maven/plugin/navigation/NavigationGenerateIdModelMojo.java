package fr.ird.observe.toolkit.maven.plugin.navigation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import fr.ird.observe.toolkit.maven.plugin.navigation.id.GenerateIdModel;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Created on 19/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
@Mojo(name = "navigation-generate-id-model", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyCollection = ResolutionScope.COMPILE)
public class NavigationGenerateIdModelMojo extends ExecuteRunnerMojoSupport {

    @Override
    protected MojoRunnable createRunner() {
        GenerateIdModel runner = new GenerateIdModel();
        runner.setTargetDirectory(getTargetRoot().toPath());
        return runner;
    }
}
