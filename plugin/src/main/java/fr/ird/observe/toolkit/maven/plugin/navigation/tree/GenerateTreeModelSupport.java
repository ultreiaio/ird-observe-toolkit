package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import fr.ird.observe.toolkit.maven.plugin.navigation.NavigationGenerateSupport;
import io.ultreia.java4all.lang.Strings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 01/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public abstract class GenerateTreeModelSupport extends NavigationGenerateSupport {

    public static final String BEAN = "Bean";
    private final NodeTemplate nodeTemplate;
    private final BeanTemplate beanTemplate;
    private final InterceptorTemplate interceptorTemplate;
    private final TreeModelTemplate treeModelTemplate;
    private final TreeTemplate treeTemplate;
    private final ToDtoMappingTemplate toDtoMappingTemplate;
    private final String type;
    private final String suffix;
    private final String packageName;
    private Path resourcesRoot;

    protected GenerateTreeModelSupport(String type) {
        this.type = type;
        this.suffix = Strings.capitalize(type) + "TreeNode";
        this.packageName = "fr.ird.observe.navigation.tree." + type;
        this.nodeTemplate = new NodeTemplate(this);
        this.beanTemplate = new BeanTemplate(this);
        this.treeModelTemplate = new TreeModelTemplate(this);
        this.treeTemplate = new TreeTemplate(this);
        this.interceptorTemplate = new InterceptorTemplate(this);
        this.toDtoMappingTemplate = new ToDtoMappingTemplate(this);
    }

    public void setResourcesRoot(Path resourcesRoot) {
        this.resourcesRoot = resourcesRoot;
    }

    public abstract String getNodeTemplate();

    public abstract String getTreeModelTemplate();

    public abstract String getTreeTemplate();

    public abstract String getBeanTemplate();

    public abstract String getInterceptorTemplate();

    public abstract TreeProjectModel getProjectModel(MetaModelSupport descriptor);

    public final String getType() {
        return type;
    }

    public final String getSuffix() {
        return suffix;
    }

    public final String getPackage() {
        return packageName;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(resourcesRoot);
    }

    @Override
    protected final void generate(Path targetDirectory, String dtoRootPackage, MetaModelSupport descriptor) throws IOException {
        TreeProjectModel model = getProjectModel(descriptor);
        Path rootTargetDirectory = getRootTargetDirectory(targetDirectory);
        createDirectories(rootTargetDirectory);

        getLog().info(String.format("Found %d node(s).", model.getNodes().size()));
        getLog().info(String.format("Will generate at %s", rootTargetDirectory));

        for (TreeNodeModel node : model.getNodes()) {
            generateNodeClass(getNodeFile(rootTargetDirectory, node), model, node);
            generateBeanClass(getBeanFile(rootTargetDirectory, node), model, node);
            registerPath(model, node);
        }
        generateToDtoMappingClass(targetDirectory, descriptor);
        generateInterceptorClass(targetDirectory, descriptor);
        generateTreeModel(targetDirectory);
        generateTree(targetDirectory);
    }

    protected void registerPath(TreeProjectModel project, TreeNodeModel model) {
        toDtoMappingTemplate.register(project, model);
        for (TreeNodeLink link : model.getChildren()) {
            interceptorTemplate.register(link);
        }
    }

    protected final String getNodeFullyQualifiedName(String node) {
        return getPackage() + "." + node + getSuffix();
    }

    protected final String getNodePackageName(TreeNodeModel node) {
        String packageName = node.getPackageName();
        return getPackage() + (packageName.isEmpty() ? "" : ("." + packageName));
    }

    private void generateNodeClass(Path targetFile, TreeProjectModel model, TreeNodeModel node) throws IOException {
        if (isVerbose()) {
            getLog().info("Will generate node    at: " + targetFile);
        }
        String content = nodeTemplate.generateContent(model, node);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private void generateBeanClass(Path targetFile, TreeProjectModel model, TreeNodeModel node) throws IOException {
        if (isVerbose()) {
            getLog().info("Will generate bean    at: " + targetFile);
        }
        String content = beanTemplate.generateContent(model, node);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private void generateToDtoMappingClass(Path targetDirectory, MetaModelSupport descriptor) throws IOException {
        String packageName = toDtoMappingTemplate.getPackageName();
        String classSimpleName = toDtoMappingTemplate.getClassSimpleName(descriptor);

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        String jsonFileName = classSimpleName +".json";
        Path jsonTargetFile = resourcesRoot.resolve("META-INF").resolve("mapping").resolve(jsonFileName);
        if (isVerbose()) {
            getLog().info("Will generate ToDtoMapping at: " + targetFile);
        }

        String content = toDtoMappingTemplate.generateContent(packageName, classSimpleName, jsonFileName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
        String jsonContent = toDtoMappingTemplate.generateMappingContent();
        store(jsonTargetFile, jsonContent);

    }

    private void generateInterceptorClass(Path targetDirectory, MetaModelSupport descriptor) throws IOException {
        String packageName = interceptorTemplate.getPackageName();
        String classSimpleName = interceptorTemplate.getClassSimpleName(descriptor);

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        if (isVerbose()) {
            getLog().info("Will generate interceptor at: " + targetFile);
        }
        String content = interceptorTemplate.generateContent(packageName, classSimpleName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private void generateTreeModel(Path targetDirectory) throws IOException {
        String packageName = treeModelTemplate.getPackageName();
        String classSimpleName = treeModelTemplate.getClassSimpleName();

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        if (isVerbose()) {
            getLog().info("Will generate tree model at: " + targetFile);
        }
        String content = treeModelTemplate.generateContent(packageName, classSimpleName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private void generateTree(Path targetDirectory) throws IOException {
        String packageName = treeTemplate.getPackageName();
        String classSimpleName = treeTemplate.getClassSimpleName();

        Path targetFile = getSpiFile(targetDirectory, packageName, classSimpleName);
        if (isVerbose()) {
            getLog().info("Will generate tree at: " + targetFile);
        }
        String content = treeTemplate.generateContent(packageName, classSimpleName);
        getLog().debug("Content:\n" + content);
        store(targetFile, content);
    }

    private Path getRootTargetDirectory(Path targetDirectory) {
        return targetDirectory.resolve(getPackage().replaceAll("\\.", File.separator));
    }

    private Path getNodeFile(Path targetDirectory, TreeNodeModel node) {
        String path = node.getClassName() + getSuffix();
        return targetDirectory.resolve(path.replaceAll("\\.", "/") + ".java");
    }

    private Path getBeanFile(Path targetDirectory, TreeNodeModel node) {
        String path = node.getClassName() + getSuffix() + BEAN;
        return targetDirectory.resolve(path.replaceAll("\\.", "/") + ".java");
    }
}
