package fr.ird.observe.toolkit.maven.plugin.navigation.tree;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.gson.GsonBuilder;
import fr.ird.observe.spi.navigation.model.MetaModelSupport;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeLink;
import fr.ird.observe.spi.navigation.model.tree.TreeNodeModel;
import fr.ird.observe.spi.navigation.model.tree.TreeProjectModel;
import io.ultreia.java4all.lang.Strings;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created on 03/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class ToDtoMappingTemplate {
    public static final String PATH_MAPPING_MODEL = "" +
            "package %1$s;\n\n" +
            "import fr.ird.observe.spi.navigation.tree.DtoToToolkitTreePathMapping;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.function.Supplier;\n\n" +
            "@Generated(value = \"%6$s\", date = \"%2$s\")\n" +
            "public class %3$s extends DtoToToolkitTreePathMapping {\n\n" +
            "    private static %3$s INSTANCE;\n\n" +
            "    public static %3$s get() {\n" +
            "        return INSTANCE == null ? INSTANCE = new %3$s() : INSTANCE;\n" +
            "    }\n\n" +
            "    protected %3$s() {\n" +
            "        super(\"%4$s\");\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public Supplier<%5$s> rootSupplier() {\n" +
            "        return %5$s::new;\n" +
            "    }\n\n" +
            "}";

    private final GenerateTreeModelSupport generator;
    private final ArrayListMultimap<String, String> pathMapping = ArrayListMultimap.create();

    public ToDtoMappingTemplate(GenerateTreeModelSupport generator) {
        this.generator = generator;
    }

    public String getPackageName() {
        return "fr.ird.observe.spi.navigation.tree." + generator.getType();
    }

    public String getClassSimpleName(MetaModelSupport descriptor) {
        return descriptor.getName() + "DtoTo" + Strings.capitalize(generator.getType()) + "TreePathMapping";
    }

    public String generateMappingContent() {
        TreeMap<String, Collection<String>> sorted = new TreeMap<>();
        pathMapping.asMap().entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey()).forEach(e -> sorted.put(e.getKey(), e.getValue()));
        return new GsonBuilder().setPrettyPrinting().create().toJson(sorted);
    }

    public String generateContent(String packageName, String className, String modelName) {
        String rootNodeType = generator.getNodeFullyQualifiedName("Root");
        return String.format(PATH_MAPPING_MODEL
                , packageName
                , new Date()
                , className
                , modelName
                , rootNodeType
                , getClass().getName());
    }

    public void register(TreeProjectModel project, TreeNodeModel node) {
        String dtoType = node.getType();
        if (node.isRoot() || node.isReferentialPackage() || node.isOpenList()) {
            return;
        }
        String key = generator.getDtoFullyQualifiedName(dtoType);
        if (node.isReferentialType()) {
            TreeNodeLink nodeLink = project.getNodeLink(node).orElseThrow();
            TreeNodeModel parentNode = project.getNode(node.getParentClassName()).orElseThrow();
            TreeNodeLink parentNodeLink = project.getNodeLink(parentNode).orElseThrow();
            pathMapping.put(key, String.format("/%s/%s", parentNodeLink.getPropertyName(), nodeLink.getPropertyName()));
            return;
        }
        String simpleName = node.getSimpleName();
        if (node.isRootOpenFilter()) {
            TreeNodeLink nodeLink = project.getNodeLink(node).orElseThrow();
            pathMapping.put(key, String.format("/%s:${%s}", nodeLink.getPropertyName(), simpleName));
            return;
        }
        if (node.isRootOpen()) {
            for (String parentClassName : node.getParentClassNames()) {
                TreeNodeModel parentNode = project.getNode(parentClassName).orElseThrow();
                if (parentNode.isRoot()) {
                    TreeNodeLink nodeLink = parentNode.getChildren(node.getClassName()).orElseThrow();
                    pathMapping.put(key, String.format("/%s:${%s}", nodeLink.getPropertyName(), simpleName));
                } else {
                    extendsParentPaths(parentNode, key, String.format("${%s}", simpleName));
                }
            }
            return;
        }
        if (node.isOpen()) {
            String parentClassName = node.getParentClassName();
            TreeNodeModel listParentNode = project.getNode(parentClassName).orElseThrow();
            TreeNodeLink nodeLink = project.getNodeLink(listParentNode).orElseThrow();
            TreeNodeModel parentNode = project.getNode(listParentNode.getParentClassName()).orElseThrow();
            extendsParentPaths(parentNode, key, String.format("%s/${%s}", nodeLink.getPropertyName(), simpleName));
            return;
        }
        if (node.isEdit()) {
            String parentClassName = node.getParentClassName();
            TreeNodeModel parentNode = project.getNode(parentClassName).orElseThrow();
            TreeNodeLink nodeLink = project.getNodeLink(node).orElseThrow();
            extendsParentPaths(parentNode, key, String.format("%s:${%s}", nodeLink.getPropertyName(), simpleName));
            return;
        }
        String parentClassName = node.getParentClassName();
        TreeNodeModel parentNode = project.getNode(parentClassName).orElseThrow();
        TreeNodeLink nodeLink = project.getNodeLink(node).orElseThrow();
        extendsParentPaths(parentNode, key, nodeLink.getPropertyName());
    }


    private void extendsParentPaths(TreeNodeModel parentNode, String key, String newPath) {
        String parentKey = generator.getDtoFullyQualifiedName(parentNode.getClassName());
        List<String> parentPaths = pathMapping.entries().stream().filter(e -> parentKey.equals(e.getKey())).map(Map.Entry::getValue).collect(Collectors.toList());
        parentPaths.forEach(parentPath -> pathMapping.put(key, String.format("%s/%s", parentPath, newPath)));
    }
}
