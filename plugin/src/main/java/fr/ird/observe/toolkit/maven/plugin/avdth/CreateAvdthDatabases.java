package fr.ird.observe.toolkit.maven.plugin.avdth;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;

import java.nio.file.Path;
import java.util.Objects;

/**
 * To create avdth databases from the avdth archive.
 * <p>
 * Created on 29/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
public class CreateAvdthDatabases extends PersistenceRunner {
    protected Path archiveFile;

    public void setArchiveFile(Path archiveFile) {
        this.archiveFile = archiveFile;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(archiveFile);
    }

    @Override
    public void run() {
        Path exploded = explodeArchive(archiveFile, sourceDirectory);
        log.info(String.format("Load avdth databases for version %s to %s", modelVersion, exploded));
    }
}
