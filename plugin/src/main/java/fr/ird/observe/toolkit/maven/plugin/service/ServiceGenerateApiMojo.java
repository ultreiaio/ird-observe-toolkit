package fr.ird.observe.toolkit.maven.plugin.service;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.IOException;

/**
 * Created on 21/01/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
@Mojo(name = "service-generate-api", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class ServiceGenerateApiMojo extends ExecuteRunnerMojoSupport {
    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        super.doAction(getterFile, classLoader);
        addCompileSourceRoots(getTargetRoot());
    }

    @Override
    protected ServiceGenerateApiRunner createRunner() throws IOException {
        ServiceGenerateApiRunner runner = new ServiceGenerateApiRunner();
        runner.setSourceDirectory(getSourceRoot().toPath());
        runner.setTargetDirectory(getTargetRoot().toPath());
        return runner;
    }
}
