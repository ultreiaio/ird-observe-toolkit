package fr.ird.observe.toolkit.maven.plugin.server.html.model;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.BusinessDto;
import fr.ird.observe.dto.referential.ReferentialDto;
import fr.ird.observe.spi.context.DtoEntityContext;
import fr.ird.observe.spi.module.BusinessModule;
import fr.ird.observe.spi.module.BusinessProject;
import fr.ird.observe.spi.module.BusinessSubModule;
import fr.ird.observe.toolkit.maven.plugin.server.html.TemplateModelBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.jaxx.runtime.util.FileUtil;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 21/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1.0
 */
public class DocType extends DocElement implements WithValidation {
    private static final Logger log = LogManager.getLogger(DocType.class);

    private final List<DocRequest> requests = new LinkedList<>();
    private String typeDefinition;
    private String definitionContent;
    private String createValidationContent;
    private String updateValidationContent;
    private String createErrorValidationContent;
    private String createWarningValidationContent;
    private String updateErrorValidationContent;
    private String updateWarningValidationContent;
    private String validationContent = "";

    public DocType(DocPackage parent, String name) {
        setParent(parent);
        setName(name);
        setPath(parent.getPath() + "/" + name);
    }

    public String getTypeDefinition() {
        return typeDefinition;
    }

    public void setTypeDefinition(String typeDefinition) {
        this.typeDefinition = typeDefinition;
    }

    public String getDefinitionContent() {
        return definitionContent;
    }

    public void setDefinitionContent(String definitionContent) {
        this.definitionContent = definitionContent;
    }

    @Override
    public List<DocRequest> getRequests() {
        requests.forEach(r -> r.setRelativeParent(null));
        return requests;
    }

    public DocRequest addRequest(Requests type) {
        DocRequest request = new DocRequest(this, type, "type");
        requests.add(request);
        return request;
    }

    public Path resolveContent(Path path) {
        return path.resolve("content.json");
    }

    public Path resolveCreate(Path path) {
        return path.resolve("create.json");
    }

    @Override
    public String getValidationContent() {
        return validationContent;
    }

    @Override
    public void setValidationContent(String validationContent) {
        this.validationContent = validationContent;
    }

    public String getCreateValidationContent() {
        return createValidationContent;
    }

    public void setCreateValidationContent(String createErrorValidationContent) {
        this.createValidationContent = createErrorValidationContent;
    }

    public String getUpdateValidationContent() {
        return updateValidationContent;
    }

    public void setUpdateValidationContent(String updateValidationContent) {
        this.updateValidationContent = updateValidationContent;
    }

    public String getUpdateWarningValidationContent() {
        return updateWarningValidationContent;
    }

    public void setUpdateWarningValidationContent(String updateWarningValidationContent) {
        this.updateWarningValidationContent = updateWarningValidationContent;
    }

    public String getUpdateErrorValidationContent() {
        return updateErrorValidationContent;
    }

    public void setUpdateErrorValidationContent(String updateErrorValidationContent) {
        this.updateErrorValidationContent = updateErrorValidationContent;
    }

    public String getCreateWarningValidationContent() {
        return createWarningValidationContent;
    }

    public void setCreateWarningValidationContent(String createWarningValidationContent) {
        this.createWarningValidationContent = createWarningValidationContent;
    }

    public String getCreateErrorValidationContent() {
        return createErrorValidationContent;
    }

    public void setCreateErrorValidationContent(String createErrorValidationContent) {
        this.createErrorValidationContent = createErrorValidationContent;
    }

    @Override
    public void storeValidation(Path requestPath) throws IOException {
        WithValidation.store(getCreateValidationContent(), requestPath.resolve("validation-create.json"));
        WithValidation.store(getUpdateValidationContent(), requestPath.resolve("validation-update.json"));
        WithValidation.store(getCreateErrorValidationContent(), requestPath.resolve("validation-create-error.json"));
        WithValidation.store(getCreateWarningValidationContent(), requestPath.resolve("validation-create-warning.json"));
        WithValidation.store(getUpdateErrorValidationContent(), requestPath.resolve("validation-update-error.json"));
        WithValidation.store(getUpdateWarningValidationContent(), requestPath.resolve("validation-update-warning.json"));
    }

    public void addSimpleType(TemplateModelBuilder builder,
                              BusinessModule module,
                              BusinessSubModule subModule,
                              Class<? extends BusinessDto> dtoType,
                              DtoEntityContext<?, ?, ?, ?> spi) {
        Path fixturesPath = BusinessProject.getDtoVariablePath(Path.of("/fixtures"), module, subModule, dtoType);

        addDescription(spi, builder.definitionTypeContent);
        addValidation(dtoType, spi, builder.validationTypeContent);
        boolean referential = ReferentialDto.class.isAssignableFrom(dtoType);
        if (referential) {
            DocRequest getAllRequest = addTypeRequest(builder, Requests.GetAll, fixturesPath, false);
            getAllRequest.addOrder(spi, builder.orderTemplate, builder.requestDefinitionTemplate);
        }
        DocRequest getRequest = addTypeRequest(builder, Requests.GetSome, fixturesPath, false);
        getRequest.addFilter(spi, builder.filterTemplate);
        getRequest.addOrder(spi, builder.orderTemplate, builder.requestDefinitionTemplate);
        addTypeRequest(builder, Requests.GetOne, fixturesPath, false);
        addTypeRequest(builder, Requests.GenerateId, fixturesPath, false);
        //noinspection StatementWithEmptyBody
        if (!referential && !"TripDto".equals(dtoType.getSimpleName())) {
            //FIXME One day we will be able to expose all types
        } else {
            addTypeRequest(builder, Requests.Create, fixturesPath, spi.getValidationCreateContent() != null);
            addTypeRequest(builder, Requests.Update, fixturesPath, spi.getValidationUpdateContent() != null);
            addTypeRequest(builder, Requests.Delete, fixturesPath, false);
        }
        finalizeContainer(builder);
    }

    private void addDescription(DtoEntityContext<?, ?, ?, ?> spi, String definitionTypeContent) {
        String description = spi.getDefinitionContent();
        setTypeDefinition(description);
        setDefinitionContent(definitionTypeContent);
    }

    private void addValidation(Class<? extends BusinessDto> dtoType, DtoEntityContext<?, ?, ?, ?> spi, String validationTypeContent) {
        Path directory = FileUtil.getFileFromFQN(Path.of("/").toFile(), dtoType.getName()).toPath().getParent();
        String createErrorFileContent = getValidator(directory, dtoType, "create", "error");
        String updateErrorFileContent = getValidator(directory, dtoType, "update", "error");
        String createWarningFileContent = getValidator(directory, dtoType, "create", "warning");
        String updateWarningFileContent = getValidator(directory, dtoType, "update", "warning");
        String createFileContent = spi.getValidationCreateContent();
        String updateFileContent = spi.getValidationUpdateContent();
        setCreateValidationContent(createFileContent);
        setUpdateValidationContent(updateFileContent);
        setCreateErrorValidationContent(createErrorFileContent);
        setCreateWarningValidationContent(createWarningFileContent);
        setUpdateErrorValidationContent(updateErrorFileContent);
        setUpdateWarningValidationContent(updateWarningFileContent);

        if (createFileContent == null && updateFileContent == null) {
            return;
        }
        setValidationContent(validationTypeContent);
    }

    private DocRequest addTypeRequest(TemplateModelBuilder builder, Requests type, Path fixturesPath, boolean addValidation) {
        DocRequest request = addRequest(type);
        request.fill(builder);
        request.addExample(fixturesPath);
        if (request.withContent()) {
            String contextName = type.name().toLowerCase();
            if (addValidation) {
                request.setValidationContent(builder.validationRequestContent.replaceAll("\\$\\{contextName}", contextName));
            }
        }
        return request;
    }

    private String getValidator(Path directory, Class<? extends BusinessDto> dtoType, String context, String scope) {
        Path path = directory.resolve(String.format("%s-%s-%s-validation.json", dtoType.getSimpleName(), context, scope));
        try {
            String location = path.toString().substring(1);
            return IOUtils.resourceToString(location, StandardCharsets.UTF_8, Thread.currentThread().getContextClassLoader());
        } catch (IOException e) {
            log.warn("Can't load description at: {}", path);
            return null;
        }
    }
}
