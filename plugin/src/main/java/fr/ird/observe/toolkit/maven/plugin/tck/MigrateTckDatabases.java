package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.sql.SqlScript;
import org.nuiton.topia.persistence.TopiaApplicationContext;

import java.nio.file.Path;
import java.util.Objects;

/**
 * Created on 22/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.55
 */
public class MigrateTckDatabases extends PersistenceRunner {
    protected Path archiveFile;

    public void setArchiveFile(Path archiveFile) {
        this.archiveFile = archiveFile;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(previousModelVersion);
        Objects.requireNonNull(archiveFile);
    }

    @Override
    public void run() {

        Path exploded = explodeArchive(archiveFile);
        Path sources = getTemporaryPath().resolve("sources");

        migrateVersion(sources, exploded, Version.valueOf("7.6"), DatabaseName.empty, DatabaseName.referential);
        migrateVersion(sources, exploded, Version.valueOf("8.0"), DatabaseName.values());
        migrateVersion(sources, exploded, Version.valueOf("9.0"), DatabaseName.values());
        migrateVersion(sources, exploded, previousModelVersion, DatabaseName.values());
    }

    protected void migrateVersion(Path sources, Path exploded, Version version, DatabaseName... databaseNames) {
        createForVersion(version, exploded, sources, databaseNames);
        for (DatabaseName databaseName : databaseNames) {
            migrateDatabase(databaseName, version, sources);
        }
    }

    protected void migrateDatabase(DatabaseName tckDbName, Version previousModelVersion, Path sources) {

        Path dbPath = sources.resolve(previousModelVersion.getVersion());

        ObserveDataSourceConfigurationTopiaH2 configuration = createConfiguration(getTemporaryPath().resolve("db").resolve(previousModelVersion.getValidName()).resolve(tckDbName.name()));

        String sqlFilename = tckDbName.name() + ".sql.gz";
        Path backupFile = getTemporaryPath().resolve(previousModelVersion.getValidName() + "-backup-" + sqlFilename.replace(".gz", ""));
        try (TopiaApplicationContext<?> applicationContext = createTopiaApplicationContext(configuration)) {

            log.info(String.format("[%S] migrate from version %s to %s", tckDbName, previousModelVersion, modelVersion));
            applicationContext.executeSqlStatements(SqlScript.of(dbPath.resolve(sqlFilename)));
            applicationContext.migrate();

            log.info(String.format("[%S] backup to %s", tckDbName, backupFile));
            GenerateTckArchive.backupSane(applicationContext, backupFile, false);
        }
        checkDatabase(tckDbName, previousModelVersion, backupFile);
    }

    protected void checkDatabase(DatabaseName tckDbName, Version previousModelVersion, Path backupFile) {

        ObserveDataSourceConfigurationTopiaH2 checkConfiguration = createConfiguration(getTemporaryPath().resolve("db").resolve(previousModelVersion.getValidName() + "-" + modelVersion.getValidName()).resolve(tckDbName.name()));
        try (TopiaApplicationContext<?> applicationContext = createTopiaApplicationContext(checkConfiguration)) {

            log.info(String.format("[%S] Check - loading new database with version %s", tckDbName, modelVersion));

            applicationContext.executeSqlStatements(SqlScript.of(backupFile));
            Path checkBackupFile = getTemporaryPath().resolve("check-backup-" + backupFile.toFile().getName().replace(".sql.gz", "-check.sql"));
            applicationContext.newJdbcHelperH2().backup(checkBackupFile.toFile(), false);
            log.info(String.format("[%S] Check - backup to %s", tckDbName, checkBackupFile));
        }
    }

}
