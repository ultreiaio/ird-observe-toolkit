package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import fr.ird.observe.toolkit.templates.io.TagValuesExtractor;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 01/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.17
 */
public class MergeModels extends MojoRunnable {

    private Path sourceDirectory;
    private Path targetDirectory;
    private String packageName;
    private String modelName;

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(sourceDirectory);
        Objects.requireNonNull(targetDirectory);
        Objects.requireNonNull(packageName);
        Objects.requireNonNull(modelName);
    }

    @Override
    public void run() {

        String classifier;
        if (packageName.contains(".dto")) {
            classifier = "dto";
        } else if (packageName.contains(".entities")) {
            classifier = "persistence";
        } else {
            throw new IllegalStateException("Can't manage packageName: " + packageName);
        }

        Path resourceDirectory = TagValuesExtractor.getModelPath(sourceDirectory.resolve("ya"), modelName, classifier);
        getLog().info(String.format("[%s] Will get models from %s", classifier, resourceDirectory));

        List<Path> files;
        try {
            files = Files.walk(resourceDirectory, 1)
                    .filter(p -> p.toFile().getName().endsWith(".model"))
                    .sorted(Comparator.comparing(p -> p.toFile().getName()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalStateException("Can't get models files from: " + resourceDirectory, e);
        }
        getLog().info(String.format("[%s] Found %d model file(s)", classifier, files.size()));

        Path targetPath = TagValuesExtractor.getModelPath(targetDirectory.resolve("yo"), modelName, classifier).getParent().resolve(classifier + ".model");
        if (Files.notExists(targetPath.getParent())) {
            try {
                Files.createDirectories(targetPath.getParent());
            } catch (IOException e) {
                throw new IllegalStateException("Can't create target directory: " + targetPath.getParent(), e);
            }
        }
        getLog().info(String.format("[%s] Merge to %s", classifier, targetPath));
        try (OutputStream writer = Files.newOutputStream(targetPath)) {
            for (Path file : files) {
                writer.write('\n');
                writer.write('\n');
                Files.copy(file, writer);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Can't generate model: " + targetPath, e);
        }
    }

    public void setSourceDirectory(Path sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    public void setTargetDirectory(Path targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
