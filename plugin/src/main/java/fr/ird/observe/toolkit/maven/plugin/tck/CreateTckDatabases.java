package fr.ird.observe.toolkit.maven.plugin.tck;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;

import java.nio.file.Path;
import java.util.Objects;

/**
 * To create tck databases from the tck archive.
 * <p>
 * Created on 26/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.57
 */
public class CreateTckDatabases extends PersistenceRunner {
    protected Path archiveFile;

    public void setArchiveFile(Path archiveFile) {
        this.archiveFile = archiveFile;
    }

    @Override
    public void init() {
        super.init();
        Objects.requireNonNull(archiveFile);
    }

    @Override
    public void run() {
        Path exploded = explodeArchive(archiveFile);
        log.info(String.format("Load tck databases for version %s to %s", modelVersion, sourceDirectory));
        createForVersion(modelVersion, exploded, sourceDirectory, DatabaseName.values());
    }
}
