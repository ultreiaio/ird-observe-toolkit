package fr.ird.observe.toolkit.maven.plugin.navigation.validation;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.data.EditableDto;
import fr.ird.observe.dto.data.OpenableDto;
import fr.ird.observe.dto.data.RootOpenableDto;
import io.ultreia.java4all.lang.Objects2;

/**
 * Created on 29/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.64
 */
public class GenerateClientValidationContext extends GenerateValidationModelSupport {

    public static final String METHOD_TEMPLATE = "" +
            "    @Override\n" +
            "    protected final Function<String, %1$s> %2$s() {\n" +
            "        return id -> getServicesProvider().%3$s().loadDto(%1$s.class, id);\n" +
            "    }\n\n";

    public static final String PROJECT_TEMPLATE = "package %1$s;\n\n" +
            "import fr.ird.observe.dto.validation.DtoValidationContext;\n" +
            "import fr.ird.observe.dto.validation.ValidationRequestConfiguration;\n" +
            "import fr.ird.observe.navigation.id.Project;\n" +
            "import fr.ird.observe.services.ObserveServicesProvider;\n" +
            "import fr.ird.observe.decoration.DecoratorService;\n\n" +
            "import javax.annotation.Generated;\n" +
            "import java.util.function.Function;\n\n" +
            "@Generated(value = \"%2$s\", date = \"%3$s\")\n" +
            "public abstract class %4$s extends DtoValidationContext {\n\n" +
            "    protected %4$s(ValidationRequestConfiguration configuration, DecoratorService decoratorService, Project selectModel) {\n" +
            "        super(configuration, decoratorService, selectModel);\n" +
            "    }\n\n" +
            "    public abstract ObserveServicesProvider getServicesProvider();\n\n" +
            "%5$s" +
            "}";

    @Override
    protected String getPackage() {
        return "fr.ird.observe.client.datasource.validation";
    }

    protected String getClassSimpleName() {
        return "GeneratedClientValidationContext";
    }

    @Override
    public String getProjectTemplate() {
        return PROJECT_TEMPLATE;
    }

    protected String generateMethod(String dtoFullyQualifiedName, String moduleName, String typePrefix, String capitalizeModuleName) {
        Class<Object> type = Objects2.forName(dtoFullyQualifiedName);
        String serviceMethod;

        if (RootOpenableDto.class.isAssignableFrom(type)) {
            serviceMethod = "getRootOpenableService";
        } else if (OpenableDto.class.isAssignableFrom(type)) {
            serviceMethod = "getOpenableService";
        } else if (EditableDto.class.isAssignableFrom(type)) {
            serviceMethod = "getEditableService";
        } else {
            throw new IllegalStateException("Can't manage type: " + type);
        }
        return String.format(METHOD_TEMPLATE,
                             dtoFullyQualifiedName,
                             moduleName + typePrefix + "Function",
                             serviceMethod);
    }
}
