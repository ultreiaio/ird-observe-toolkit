package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.report.definition.ReportDefinitionsBuilder;
import fr.ird.observe.report.tools.PersistenceComputeReportListFileRunner;
import fr.ird.observe.toolkit.maven.plugin.ExecuteRunnerMojoSupport;
import fr.ird.observe.toolkit.maven.plugin.MojoRunnable;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

/**
 * Created at 07/12/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
@Mojo(name = "persistence-compute-report-list-file", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class PersistenceComputeReportListFileMojo extends ExecuteRunnerMojoSupport {


    /**
     * Where to load reports.
     */
    @Parameter(property = "runner.reports.path", defaultValue = "${project.basedir}/src/main/resources/META-INF/report", required = true)
    private File source;
    /**
     * Where to write reduced report file.
     */
    @Parameter(property = "runner.report.list.file", defaultValue = "${project.basedir}/src/main/resources" + ReportDefinitionsBuilder.REPORT_LIST_LOCATION, required = true)
    private File reportListPath;

    @Override
    protected MojoRunnable createRunner() throws IOException {
        PersistenceComputeReportListFileRunner runner = new PersistenceComputeReportListFileRunner();
        runner.setImportPath(source.toPath());
        runner.setReportListPath(reportListPath.toPath());
        return MojoRunnable.of(runner);
    }
}
