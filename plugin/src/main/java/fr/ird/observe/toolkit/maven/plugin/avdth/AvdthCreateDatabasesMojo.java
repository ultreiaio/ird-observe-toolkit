package fr.ird.observe.toolkit.maven.plugin.avdth;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.toolkit.maven.plugin.PersistenceMojoSupport;
import fr.ird.observe.toolkit.maven.plugin.PersistenceRunner;
import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created on 29/06/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.4.0
 */
@Mojo(name = "avdth-create-databases", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE)
public class AvdthCreateDatabasesMojo extends PersistenceMojoSupport {

    @Parameter(property = "runner.archiveFile", defaultValue = "${avdth.cache.path}/${avdth.filename}", required = true)
    private File archiveFile;

    @Parameter(property = "runner.avdthRootPath", defaultValue = "${avdth.cache.path}/data", required = true)
    private File avdthRootPath;

    @Parameter(property = "runner.skipIfExists", defaultValue = "false")
    private boolean skipIfExists;

    @Override
    protected void doAction(I18nKeySet getterFile, ClassLoader classLoader) throws IOException {
        Path resolve = avdthRootPath.toPath().resolve(getModelVersion().getVersion());
        if (skipIfExists && Files.exists(resolve)) {
            getLog().info(String.format("Skip - files already at: %s", resolve));
            return;
        }
        super.doAction(getterFile, classLoader);
    }

    @Override
    protected CreateAvdthDatabases createRunner() {
        CreateAvdthDatabases runner = new CreateAvdthDatabases();
        runner.setArchiveFile(archiveFile.toPath());
        return runner;
    }

    @Override
    protected void prepare(PersistenceRunner runner) {
        super.prepare(runner);
        runner.setSourceDirectory(avdthRootPath.toPath());
    }
}

