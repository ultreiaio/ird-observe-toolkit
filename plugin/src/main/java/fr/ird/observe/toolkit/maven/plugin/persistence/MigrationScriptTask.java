package fr.ird.observe.toolkit.maven.plugin.persistence;

/*-
 * #%L
 * Toolkit :: Maven plugin
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

/**
 * Created on 24/11/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.56
 */
public class MigrationScriptTask {

    private List<ExtractSchemaTask> schemaTasks = List.of();
    private List<ExtractTableTask> tableTasks = List.of();
    private List<DropTableTasks> dropTableTasks = List.of();
    private List<RenameForeignKeyTasks> renameForeignKeyTasks = List.of();

    public List<ExtractSchemaTask> getSchemaTasks() {
        return schemaTasks;
    }

    public void setSchemaTasks(List<ExtractSchemaTask> schemaTasks) {
        this.schemaTasks = schemaTasks;
    }

    public List<ExtractTableTask> getTableTasks() {
        return tableTasks;
    }

    public void setTableTasks(List<ExtractTableTask> tableTasks) {
        this.tableTasks = tableTasks;
    }

    public List<DropTableTasks> getDropTableTasks() {
        return dropTableTasks;
    }

    public void setDropTableTasks(List<DropTableTasks> dropTableTasks) {
        this.dropTableTasks = Objects.requireNonNull(dropTableTasks);
    }

    public List<RenameForeignKeyTasks> getRenameForeignKeyTasks() {
        return renameForeignKeyTasks;
    }

    public void setRenameForeignKeyTasks(List<RenameForeignKeyTasks> renameForeignKeyTasks) {
        this.renameForeignKeyTasks = renameForeignKeyTasks;
    }
}
