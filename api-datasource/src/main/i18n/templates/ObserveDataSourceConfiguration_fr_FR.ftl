<#--
 #%L
 Toolkit :: API :: DataSource
 %%
 Copyright (C) 2017 - 2024 Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#macro dataSourceConfiguration model>
    <#if model.local>
        <@localDataSourceConfiguration model/>
    <#elseif model.remote>
        <@remoteDataSourceConfiguration model/>
    <#elseif model.server>
        <@serverDataSourceConfiguration model />
    </#if>
</#macro>
<#macro localDataSourceConfiguration model>
<#-- @ftlvariable name="model" type="fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaH2" -->
    <ul>
        <li><strong>Utilisateur : </strong>${model.login}</li>
        <li><strong>Mot de passe : </strong>*****</li>
        <li><strong>Emplacement de la base : </strong>${model.directory.absolutePath}</li>
    </ul>
</#macro>

<#macro remoteDataSourceConfiguration model>
<#-- @ftlvariable name="model" type="fr.ird.observe.datasource.configuration.topia.ObserveDataSourceConfigurationTopiaPG" -->
    <ul>
        <li><strong>URL jdbc : </strong>${model.url}</li>
        <li><strong>Utilisateur : </strong>${model.login}</li>
        <li><strong>Mot de passe : </strong>*****</li>
        <li><strong>SSL mode: </strong>${model.useSsl?then('Oui', 'Non')}</li>
    </ul>
</#macro>
<#macro serverDataSourceConfiguration model>
<#-- @ftlvariable name="model" type="fr.ird.observe.datasource.configuration.rest.ObserveDataSourceConfigurationRest" -->
    <ul>
        <li><strong>URL du serveur : </strong>${model.url}</li>
        <li><strong>Utilisateur : </strong>${model.login}</li>
        <li><strong>Mot de passe : </strong>*****</li>
        <#if model.databaseName??>
            <li><strong>Base de données : </strong>${model.databaseName}</li>
        </#if>
    </ul>
</#macro>
<#macro dataSourceInformationRights dataSourceInformation>
<#-- @ftlvariable name="dataSourceInformation" type="fr.ird.observe.datasource.configuration.ObserveDataSourceInformation" -->
    <h3>Droits</h3>
    <#if dataSourceInformation??>
        <ul>
            <li>
                <em>Référentiel : </em>
                ${dataSourceInformation.referentialPermissionLabel}
            </li>
            <li>
                <em>Données observateur : </em>
                ${dataSourceInformation.dataPermissionLabel}
            </li>
        </ul>
        <#if dataSourceInformation.version??>
            <h3>Version</h3>
            <ul>
                <li>v <strong>${dataSourceInformation.version}</strong></li>
            </ul>
        </#if>
    <#else>
        NO DATA SOURCE INFORMATION
    </#if>
</#macro>
