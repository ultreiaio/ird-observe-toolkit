<#-- @ftlvariable name=".data_model" type="fr.ird.observe.datasource.configuration.ObserveDataSourceInformation" -->
<#--
 #%L
 Toolkit :: API :: DataSource
 %%
 Copyright (C) 2017 - 2024 Ultreia.io
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>
Droits :

<strong> &bull; Référentiel : </strong>
${.data_model.referentialPermissionLabel}

<strong> &bull; Données observateur : </strong>
${.data_model.dataPermissionLabel}

<Strong> &bull; Version : </Strong>
v ${.data_model.version}
</body>
</html>
