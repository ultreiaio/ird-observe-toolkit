package fr.ird.observe.datasource.security.model;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.DataSourceValidationMode;
import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.bean.AbstractJavaBean;

import java.util.Objects;
import java.util.function.Predicate;

public class DataSourceUserDto extends AbstractJavaBean implements ObserveDto, Comparable<DataSourceUserDto> {

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_ROLE = "role";
    public static final String PROPERTY_API_ACCESS = "apiAccess";
    public static final String PROPERTY_VALIDATION_MODE = "validationMode";
    /**
     * User name.
     */
    protected String name;
    /**
     * User role.
     */
    protected DataSourceUserRole role;
    /**
     * Api access (by default keep only the client one to avoid any migration in existing configurations).
     */
    protected DataSourceApiAccess apiAccess = DataSourceApiAccess.CLIENT;
    /**
     * Validation (by default none to avoid any migration in existing configurations).
     */
    protected DataSourceValidationMode validationMode = DataSourceValidationMode.NONE;

    public static Predicate<DataSourceUserDto> newRolePredicate(DataSourceUserRole role) {
        return o -> Objects.equals(role, o.getRole());
    }

    @Override
    public int compareTo(DataSourceUserDto o) {
        return getName().compareTo(o.getName());
    }

    @Override
    public String toString() {
        return String.format("DataSourceUserDto{name='%s', role=%s}", name, role);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldValue = getName();
        this.name = name;
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    public DataSourceUserRole getRole() {
        return role;
    }

    public void setRole(DataSourceUserRole role) {
        DataSourceUserRole oldValue = getRole();
        this.role = role;
        firePropertyChange(PROPERTY_ROLE, oldValue, role);
    }

    public DataSourceApiAccess getApiAccess() {
        return apiAccess;
    }

    public void setApiAccess(DataSourceApiAccess apiAccess) {
        DataSourceApiAccess oldValue = this.apiAccess;
        this.apiAccess = apiAccess;
        firePropertyChange(PROPERTY_API_ACCESS, oldValue, apiAccess);
    }

    public DataSourceValidationMode getValidationMode() {
        return validationMode;
    }

    public void setValidationMode(DataSourceValidationMode validationMode) {
        DataSourceValidationMode oldValue = this.validationMode;
        this.validationMode = validationMode;
        firePropertyChange(PROPERTY_VALIDATION_MODE, oldValue, validationMode);
    }
}
