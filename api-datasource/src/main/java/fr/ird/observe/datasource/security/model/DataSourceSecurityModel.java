package fr.ird.observe.datasource.security.model;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Data source security model.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
@GenerateJavaBeanDefinition
public class DataSourceSecurityModel extends AbstractJavaBean {

    public static final String PROPERTY_ADMINISTRATOR = "administrator";
    public static final String PROPERTY_ROLE = "role";
    public static final String PROPERTY_ASSIGNED = "assigned";
    protected final Set<DataSourceUserDto> users;
    protected DataSourceUserDto administrator;

    public DataSourceSecurityModel() {
        users = new HashSet<>();
    }

    public DataSourceUserDto getAdministrator() {
        return administrator;
    }

    public Set<DataSourceUserDto> getUsersByRole(DataSourceUserRole role) {
        return users.stream()
                .filter(DataSourceUserDto.newRolePredicate(role))
                .collect(Collectors.toSet());
    }

    public List<String> getUserNamesByRole(DataSourceUserRole role) {
        return users.stream()
                .filter(DataSourceUserDto.newRolePredicate(role))
                .map(DataSourceUserDto::getName)
                .collect(Collectors.toList());
    }

    public List<String> getDataUserNames() {
        return getUserNamesByRole(DataSourceUserRole.USER);
    }

    public List<String> getReferentialUserNames() {
        return getUserNamesByRole(DataSourceUserRole.REFERENTIAL);
    }

    public List<String> getDataEntryOperatorUserNames() {
        return getUserNamesByRole(DataSourceUserRole.DATA_ENTRY_OPERATOR);
    }

    public List<String> getTechnicalUserNames() {
        return getUserNamesByRole(DataSourceUserRole.TECHNICAL);
    }

    public List<String> getUnusedUserNames() {
        return getUserNamesByRole(DataSourceUserRole.UNUSED);
    }

    public Set<DataSourceUserDto> getUsers() {
        return users;
    }

    public Set<DataSourceUserDto> getUsersWithoutAdministrator() {
        Predicate<DataSourceUserDto> predicate = DataSourceUserDto.newRolePredicate(DataSourceUserRole.ADMINISTRATOR);
        return users.stream().filter(u -> !predicate.test(u)).collect(Collectors.toSet());
    }

    public void init(Collection<DataSourceUserDto> users) {
        this.users.clear();
        this.users.addAll(users);

        Optional<DataSourceUserDto> optionalAdministrator = users.stream().filter(DataSourceUserDto.newRolePredicate(DataSourceUserRole.ADMINISTRATOR)).findFirst();
        if (optionalAdministrator.isPresent()) {
            this.administrator = optionalAdministrator.get();
        } else {
            throw new IllegalStateException("No administrator found");
        }
        firePropertyChange(PROPERTY_ROLE, getUsers());
        firePropertyChange(PROPERTY_ADMINISTRATOR, getAdministrator());
        firePropertyChange(PROPERTY_ASSIGNED, getAssigned());
    }

    public void setRole(DataSourceUserDto user, DataSourceUserRole role, boolean fire) {
        user.setRole(role);
        if (fire) {
            if (role != null) {
                firePropertyChange(role.name(), getUsersByRole(role));
            }
            firePropertyChange(PROPERTY_ASSIGNED, getAssigned());
        }
    }

    public void setRole(Iterable<DataSourceUserDto> users, DataSourceUserRole role) {
        for (DataSourceUserDto user : users) {
            setRole(user, role, false);
        }
        firePropertyChange(role.name(), getUsersByRole(role));
        firePropertyChange(PROPERTY_ASSIGNED, getAssigned());
    }

    public int getAssigned() {
        int countNotAssigned = (int) users.stream().filter(DataSourceUserDto.newRolePredicate(null)).count();
        return users.size() - countNotAssigned;
    }

}
