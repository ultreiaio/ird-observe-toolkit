package fr.ird.observe.datasource;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

/**
 * Created on 07/11/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1.7
 */
public class SqlHelper {

    public static final int DEFAULT_COMMENT_LENGTH = 8192;

    public static Function<String, String> escapeComment(boolean postgres) {
        return postgres ? pgEscapeString(DEFAULT_COMMENT_LENGTH) : h2EscapeString(DEFAULT_COMMENT_LENGTH);
    }

    public static Function<String, String> escapeString(boolean postgres, int maxLength) {
        return postgres ? pgEscapeString(maxLength) : h2EscapeString(maxLength);
    }

    public static Function<String, String> h2EscapeString(int maxLength) {
        return s -> SqlHelper.h2EscapeString(s, maxLength);
    }

    public static Function<String, String> pgEscapeString(int maxLength) {
        return s -> SqlHelper.pgEscapeString(s, maxLength);
    }

    public static String h2EscapeString(String txt, int maxLength) {
        return txt == null ? "NULL" : String.format("STRINGDECODE(%s)", escapeNotNullString(txt, maxLength));
    }

    public static String pgEscapeString(String txt, int maxLength) {
        return txt == null ? "NULL" : String.format("E%s", escapeNotNullString(txt, maxLength));
    }

    public static String escapeNotNullString(String txt, int maxLength) {
        String trim = Objects.requireNonNull(txt).trim();
        if (trim.length() > maxLength) {
            trim = trim.substring(0, maxLength - 1);
        }
        return "'" + escapeSingleQuote(trim) + "'";
    }

    public static String escapeSingleQuote(String string) {
        return string.replaceAll("'", "''");
    }

    public static boolean needEscapeString(String string) {
        return string != null && !string.contains("'");
    }

    public static String escapeString(String string) {
        if (string == null) {
            return "NULL";
        }
        String trim = string.trim();
        if (trim.isEmpty()) {
            return "NULL";
        }
        return "'" + escapeSingleQuote(trim) + "'";
    }
}
