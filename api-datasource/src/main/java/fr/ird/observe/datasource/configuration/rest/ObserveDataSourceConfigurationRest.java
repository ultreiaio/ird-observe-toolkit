package fr.ird.observe.datasource.configuration.rest;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import io.ultreia.java4all.util.Version;

import java.util.StringJoiner;

/**
 * Configuration d'une source de données Rest non connectée.
 * <p>
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationRest extends ObserveDataSourceConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Un nom optionnel de base à utiliser, si rien n'est préciser, on utilisera la base par défaut du serveur.
     */
    private String databaseName;

    public static ObserveDataSourceConfigurationRest create(String label,
                                                            String url,
                                                            String login,
                                                            char[] password,
                                                            String databaseName,
                                                            Version dbVersion) {
        ObserveDataSourceConfigurationRest result = new ObserveDataSourceConfigurationRest();
        result.setLabel(label);
        result.setUrl(url);
        result.setLogin(login);
        result.setPassword(password);
        result.setDatabaseName(databaseName);
        result.setModelVersion(dbVersion);
        return result;
    }

    public ObserveDataSourceConfigurationRest() {
        super.setConnectMode(DataSourceConnectMode.SERVER);
        super.setCanMigrate(false);
    }

    @Override
    public void setCanMigrate(boolean canMigrate) {
    }

    @Override
    public void setConnectMode(DataSourceConnectMode connectMode) {
    }

    @Override
    public ObserveDataSourceConfigurationRest clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationRest) super.clone();
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ObserveDataSourceConfigurationRest.class.getSimpleName() + "[", "]")
                .add("label='" + getLabel() + "'")
                .add("url='" + getUrl() + "'")
                .add("login='" + getLogin() + "'")
                .add("optionalDatabaseName='" + getDatabaseName() + "'")
                .add("modelVersion=" + getModelVersion())
                .add("autoMigrate=" + isAutoMigrate())
                .toString();
    }
}
