package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * Describes a type of data source.
 * <p>
 * Created on 01/03/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.6
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_DESCRIPTION_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_CONFIG, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_CONFIG_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_USE, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_USE_PATTERN),
})
public enum DataSourceConnectMode {
    /**
     * For a local data source (always h2).
     */
    LOCAL,
    /**
     * For a remote data source (for the moment only postgres but will then accept any jdbc with remote access)
     */
    REMOTE,
    /**
     * For a server data source (always rest).
     */
    SERVER;

    private final String iconPath;
    private Icon icon;

    DataSourceConnectMode() {
        this.iconPath = "action.connexion-type-" + name().toLowerCase();
    }

    public Icon getIcon() {
        if (icon == null) {
            icon = UIManager.getIcon(iconPath);
        }
        return icon;
    }

    public String getLabel() {
        return DataSourceConnectModeI18n.getLabel(this);
    }

    public String getDescription() {
        return DataSourceConnectModeI18n.getDescription(this);
    }

    public String getConfig() {
        return DataSourceConnectModeI18n.getConfig(this);
    }

    public String getUse() {
        return DataSourceConnectModeI18n.getUse(this);
    }
}
