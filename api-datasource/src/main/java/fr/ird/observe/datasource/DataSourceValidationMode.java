package fr.ird.observe.datasource;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Defines the validation mode we can apply on public API for create or update operations.
 * <p>
 * Created on 06/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.23
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_LABEL_PATTERN)
public enum DataSourceValidationMode {
    /**
     * No validation.
     */
    NONE,
    /**
     * Level 1 validation (no disabled referential check).
     */
    PERMISSIVE,
    /**
     * Full validation (with disabled referential check).
     */
    STRONG;

    public String getLabel() {
        return DataSourceValidationModeI18n.getLabel(this);
    }

}
