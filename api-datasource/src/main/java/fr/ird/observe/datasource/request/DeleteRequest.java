package fr.ird.observe.datasource.request;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;
import java.util.StringJoiner;

/**
 * To drop some entities.
 * <p>
 * Created on 23/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public class DeleteRequest extends SqlServiceRequest {
    /**
     * Type of data to delete.
     */
    private final String dataType;
    /**
     * Optional id of the parent of data to delete. It can be used in some callbacks if the operation.
     * <p>
     * <b>Note:</b> not required if data is an entry point, or for a referential.
     * </p>
     */
    private final String parentId;
    /**
     * Ids of data to delete.
     */
    private final Set<String> dataIds;

    public DeleteRequest(boolean postgres, String dataType, String parentId, String... dataIds) {
        super(postgres);
        this.dataType = dataType;
        this.parentId = parentId;
        this.dataIds = Set.of(dataIds);
    }

    public String getDataType() {
        return dataType;
    }

    public String getParentId() {
        return parentId;
    }

    public Set<String> getDataIds() {
        return dataIds;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]")
                .add("postgres=" + isPostgres())
                .add("dataType=" + getDataType())
                .add("parentId=" + getParentId())
                .add("dataIds=" + getDataIds())
                .toString();
    }
}
