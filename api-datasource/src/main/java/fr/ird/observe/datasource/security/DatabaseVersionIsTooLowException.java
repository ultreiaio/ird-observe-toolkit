package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Locale;

/**
 * the version of database is lesser than required version
 * a migration is possible (if you have the credentials...)
 * <p>
 * Created on 26/04/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.19
 */
public class DatabaseVersionIsTooLowException extends BabModelVersionException {

    public DatabaseVersionIsTooLowException(Locale locale, Version requiredVersion, Version databaseModelVersion) {
        this(I18n.l(locale, "observe.services.topia.error.database.version.less.than.model.version", databaseModelVersion, requiredVersion), requiredVersion, databaseModelVersion);
    }

    public DatabaseVersionIsTooLowException(String message, Version requiredVersion, Version dbVersion) {
        super(message, requiredVersion, dbVersion);
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<DatabaseVersionIsTooLowException> {

        @Override
        public DatabaseVersionIsTooLowException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new DatabaseVersionIsTooLowException(message, context.deserialize(json.get("requiredVersion"), Version.class), context.deserialize(json.get("dbVersion"), Version.class));
        }

        @Override
        public Class<?> type() {
            return DatabaseVersionIsTooLowException.class;
        }
    }
}
