package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationAndConnectionDto implements ObserveDataSourceConfigurationAndConnection {

    private ObserveDataSourceConfiguration configuration;
    private ObserveDataSourceConnection connection;

    public ObserveDataSourceConfigurationAndConnectionDto(ObserveDataSourceConfiguration configuration, ObserveDataSourceConnection connection) {
        this.configuration = configuration;
        this.connection = connection;
    }

    @Override
    public ObserveDataSourceConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public ObserveDataSourceConnection getConnection() {
        return connection;
    }

    @Override
    public void setConfiguration(ObserveDataSourceConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void setConnection(ObserveDataSourceConnection connection) {
        this.connection = connection;
    }

}
