package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;

import java.util.Locale;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BabModelVersionException extends Exception {

    private static final long serialVersionUID = 1L;
    private final Version requiredVersion;
    private final Version dbVersion;

    public static void createAndThrow(Locale applicationLocale, Version requestVersion, Version dbVersion) throws BabModelVersionException {
        if (dbVersion.after(requestVersion)) {
            // the version of database is greater than application version
            // no migration is possible
            throw new DatabaseVersionIsTooHighException(applicationLocale, requestVersion, dbVersion);
        } else {

            // the version of database is lesser than application version
            // a migration is possible (if you have the credentials...)
            throw new DatabaseVersionIsTooLowException(applicationLocale, requestVersion, dbVersion);
        }
    }

    protected BabModelVersionException(String message, Version requiredVersion, Version dbVersion) {
        super(message);
        this.requiredVersion = requiredVersion;
        this.dbVersion = dbVersion;
    }

    public Version getRequiredVersion() {
        return requiredVersion;
    }

    public Version getDbVersion() {
        return dbVersion;
    }

}
