package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.WithPermission;
import io.ultreia.java4all.application.template.spi.GenerateTemplate;
import io.ultreia.java4all.bean.AbstractJavaBean;
import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateTemplate(template = "ObserveDataSourceInformation.ftl")
@GenerateJavaBeanDefinition
public class ObserveDataSourceInformation extends AbstractJavaBean implements JsonAware, WithPermission {
    /**
     * Permissions associated with data source connection.
     */
    private final Permission permission;
    /**
     * Minimum version from where we can migrate.
     */
    private final Version minimumVersion;
    /**
     * data source version.
     */
    private final Version version;
    /**
     * List of available versions to migrate.
     */
    private final List<Version> migrations;
    /**
     * Is user owner of the data source?
     */
    private final boolean owner;
    /**
     * Is user super-user of the data source?
     */
    private final boolean superUser;

    public ObserveDataSourceInformation(Permission permission, boolean owner, boolean superUser, Version minimumVersion, Version version, List<Version> migrations) {
        this.permission = permission;
        this.owner = owner;
        this.superUser = superUser;
        this.minimumVersion = minimumVersion;
        this.version = version;
        this.migrations = migrations;
    }

    @Override
    public Permission getCredentials() {
        return permission;
    }

    public boolean isOwner() {
        return owner;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public Version getMinimumVersion() {
        return minimumVersion;
    }

    public Version getVersion() {
        return version == null ? Version.VZERO : version;
    }

    public List<Version> getMigrations() {
        return migrations;
    }

    public ObserveDataSourceInformation toPermission(Permission permission) {
        return new ObserveDataSourceInformation(permission, owner, superUser, minimumVersion, version, migrations);
    }
}
