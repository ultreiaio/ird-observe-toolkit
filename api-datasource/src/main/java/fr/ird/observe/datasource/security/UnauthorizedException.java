package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Locale;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(Locale locale, String className, String methodName) {
        this(I18n.l(locale, "observe.service.error.method.unauthorized", className, methodName));
    }

    public UnauthorizedException(String message) {
        super(message);
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<UnauthorizedException> {

        @Override
        public UnauthorizedException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new UnauthorizedException(getMessage(json));
        }

        @Override
        public Class<?> type() {
            return UnauthorizedException.class;
        }
    }
}
