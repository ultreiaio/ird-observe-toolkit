package fr.ird.observe.datasource.request;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.SqlHelper;
import io.ultreia.java4all.util.json.JsonAware;

import java.util.function.Function;

/**
 * User sql request to apply on sql model.
 * <p>
 * Created on 23/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.67
 */
public abstract class SqlServiceRequest implements JsonAware {

    /**
     * Is script for Postgres?
     */
    private final boolean postgres;

    protected SqlServiceRequest(boolean postgres) {
        this.postgres = postgres;
    }

    public boolean isPostgres() {
        return postgres;
    }

    public Function<String, String> commentFormat() {
        return SqlHelper.escapeComment(isPostgres());
    }
}
