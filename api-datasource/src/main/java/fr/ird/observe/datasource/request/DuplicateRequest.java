package fr.ird.observe.datasource.request;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * To duplicate entities which are entry point.
 * <p>
 * Created on 15/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class DuplicateRequest extends SqlServiceRequest {

    /**
     * Type fo data to replicate.
     */
    private final String dataType;
    /**
     * Id of data to duplicate.
     */
    private final String id;
    /**
     * New id to use for duplicated data.
     */
    private final String newId;

    public DuplicateRequest(boolean postgres, String dataType, String id, String newId) {
        super(postgres);
        this.dataType = dataType;
        this.id = id;
        this.newId = newId;
    }

    public String getDataType() {
        return dataType;
    }

    public String getId() {
        return id;
    }

    public String getNewId() {
        return newId;
    }
}
