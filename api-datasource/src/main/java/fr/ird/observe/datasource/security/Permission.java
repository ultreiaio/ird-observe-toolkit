package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created on 21/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public enum Permission {
    /**
     * No permission.
     */
    NONE,
    /**
     * To read referential.
     */
    READ_REFERENTIAL(0),
    /**
     * To read data.
     */
    READ_DATA(1),
    /**
     * To read and write referential.
     */
    WRITE_REFERENTIAL(READ_REFERENTIAL, 2),
    /**
     * To read and write data.
     */
    WRITE_DATA(READ_DATA, 3),
    /**
     * To read on both referential and data.
     */
    READ_ALL(READ_REFERENTIAL, READ_DATA),
    /**
     * To read on both referential and data, plus write on data.
     */
    READ_ALL_AND_WRITE_DATA(READ_REFERENTIAL, WRITE_DATA),
    /**
     * To read and write on both referential and data.
     */
    ALL(WRITE_REFERENTIAL, WRITE_DATA);

    private final int[] modifiers;
    private final int modifier;

    public static Permission valueOf(Collection<Permission> permissions) {
        int modifier = reduce(permissions.stream());
        return valueOf(modifier);
    }

    public static Permission valueOf(int modifier) {
        for (Permission value : values()) {
            if (value.modifier == modifier) {
                return value;
            }
        }
        throw new IllegalStateException(String.format("Can't find permission for modifier: %d", modifier));
    }

    private static int reduce(Stream<Permission> stream) {
        return reduce(stream.mapToInt(Permission::getModifier));
    }

    private static int reduce(IntStream stream) {
        return stream.reduce(0, (a, b) -> a | b);
    }

    Permission() {
        this(new int[0]);
    }

    Permission(Permission permission, int modifier) {
        this(IntStream.concat(Arrays.stream(permission.modifiers), IntStream.of(1 << modifier)).toArray());
    }

    Permission(Permission permission, Permission permission2) {
        this(IntStream.concat(Arrays.stream(permission.modifiers), Arrays.stream(permission2.modifiers)).toArray());
    }

    Permission(int modifiers) {
        this(IntStream.of(1 << modifiers).toArray());
    }

    Permission(int[] modifiers) {
        this.modifiers = modifiers;
        this.modifier = reduce(Arrays.stream(modifiers));
//        System.out.printf("%s → %d (%s)%n", name(), modifier, Arrays.toString(modifiers));
    }

    /**
     * @param permission permission to test
     * @return {@code true} if the invoker permission contains the given {@code permission}, {@code false} otherwise.
     */
    public boolean contains(Permission permission) {
        return modifier >= Objects.requireNonNull(permission, "Can't test a null permission").modifier
                && Arrays.stream(permission.modifiers).allMatch(this::match);
    }

    public int getModifier() {
        return modifier;
    }

    private boolean match(int modifier) {
        return (this.modifier & modifier) != 0;
    }
}
