package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;

/**
 * Created on 21/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public interface WithPermission {

    Permission getCredentials();

    /**
     * @return {@code true} si on peut lire les référentiels, {@code false} sinon.
     */
    default boolean canReadReferential() {
        return canExecute(Permission.READ_REFERENTIAL);
    }

    default boolean canReadAll() {
        return canReadReferential() && canReadData();
    }

    default String getReferentialPermissionLabel() {
        String result;
        if (canWriteReferential()) {
            result = I18n.t("observe.common.Permission.read") + " / " + I18n.t("observe.common.Permission.write");
        } else if (canReadReferential()) {
            result = I18n.t("observe.common.Permission.read");
        } else {
            result = I18n.t("observe.common.Permission.none");
        }
        return result;
    }

    default String getDataPermissionLabel() {
        String result;
        if (canWriteData()) {
            result = I18n.t("observe.common.Permission.read") + " / " + I18n.t("observe.common.Permission.write");
        } else if (canReadData()) {
            result = I18n.t("observe.common.Permission.read");
        } else {
            result = I18n.t("observe.common.Permission.none");
        }
        return result;
    }

    /**
     * @return {@code true} si on peut écrire les référentiels, {@code false} sinon.
     */
    default boolean canWriteReferential() {
        return canExecute(Permission.WRITE_REFERENTIAL);
    }

    /**
     * @return {@code true} si on peut lire des données, {@code false} sinon.
     */
    default boolean canReadData() {
        return canExecute(Permission.READ_DATA);
    }

    /**
     * @return {@code true} si on peut écrire des données, {@code false} sinon.
     */
    default boolean canWriteData() {
        return canExecute(Permission.WRITE_DATA);
    }

    default boolean canExecute(Permission methodeCredentials) {
        return methodeCredentials == null || getCredentials().contains(methodeCredentials);
    }

    default boolean canNotExecute(Permission methodeCredentials) {
        return !canExecute(methodeCredentials);
    }
}
