package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;

import java.util.Locale;

/**
 * Pour signifier qu'on a demandé une resources non existente.
 * Created on 18/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataFileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DataFileNotFoundException(Locale locale, String id) {
        this(I18n.l(locale, "observe.services.topia.error.dataFile.notFound", id));
    }

    public DataFileNotFoundException(String message) {
        super(message);
    }
}
