package fr.ird.observe.datasource.configuration.topia;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.datasource.security.DatabaseNotFoundException;
import io.ultreia.java4all.util.Version;

import java.io.File;
import java.util.Locale;

import static io.ultreia.java4all.i18n.I18n.l;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationTopiaH2 extends ObserveDataSourceConfiguration {

    /**
     * H2 database url.
     */
    private static final String H2_LOCAL_URL =
            "jdbc:h2:file:%s;" +
                    // on peut aussi utiliser file, socket
                    "FILE_LOCK=file;" +
                    //1 or 2 is needed to restore avec crash
                    // 0: logging is disabled (faster),
                    // 1: logging of the data is enabled, but logging of the index
                    // changes is disabled (default), 2: logging of both data and index
                    // changes are enabled
                    "LOG=0;" +
                    // on peut aussi utiliser hsqldb, mysql ou postgresql
                    "MODE=postgresql;" +
                    //"MODE=hsqldb;" +
                    // Sets the default lock timeout (in milliseconds) in this
                    // database that is used for the new sessions.
                    "DEFAULT_LOCK_TIMEOUT=100;" +
                    // -1: the database is never closed until the close delay is set to
                    // some other rev or SHUTDOWN is called., 0: no delay (default; the
                    // database is closed if the last connection to it is closed)., n:
                    // the database is left open for n second after the last connection
                    // is closed.
                    "DB_CLOSE_DELAY=0;" +
                    // 0: no locking (should only be used for testing),
                    // 1: table level locking (default),
                    // 2: table level locking with garbage collection (if the
                    // application does not close all connections).
                    // LOCK_MODE 3 (READ_COMMITTED). Table level locking, but only when
                    // writing (no read locks).
                    "LOCK_MODE=3;" +
                    // Levels: 0=off, 1=error, 2=info, 3=debug.
                    "TRACE_LEVEL_FILE=0;" +
                    // on system.out: 0=off, 1=error, 2=info, 3=debug.
                    "TRACE_LEVEL_SYSTEM_OUT=0;" +
                    // maximumn cache to improve performance...
                    "CACHE_SIZE=65536;" +
                    // avoid timeout on reading tables (see http://stackoverflow.com/questions/4162557/timeout-error-trying-to-lock-table-in-h2)
                    "MVCC=true";

    private static final long serialVersionUID = 1L;
    /**
     * Data base directory.
     */
    private File directory;
    /**
     * Data base name.
     */
    private String dbName;
    /**
     * Database file.
     */
    private transient File databaseFile;
    /**
     * Lock file.
     */
    private transient File lockFile;

    public static ObserveDataSourceConfigurationTopiaH2 create(String label,
                                                               File directory,
                                                               String databaseName,
                                                               String username,
                                                               char[] password,
                                                               Version dbVersion) {
        ObserveDataSourceConfigurationTopiaH2 result = new ObserveDataSourceConfigurationTopiaH2();
        result.setLabel(label);
        result.setDbName(databaseName);
        result.setLogin(username);
        result.setPassword(password);
        result.setDirectory(directory);
        result.setModelVersion(dbVersion);
        return result;
    }

    public static ObserveDataSourceConfigurationTopiaH2 createTemporaryConfiguration(File tmpDirectory, Version version) {
        File dbDirectory = new File(tmpDirectory, "tmp-db-" + System.nanoTime());
        ObserveDataSourceConfigurationTopiaH2 result = create(
                "temporary h2 database",
                dbDirectory,
                "tmpDb",
                "sa",
                "sa".toCharArray(),
                version);
        result.setTemporaryDirectory(tmpDirectory.toPath());
        return result;
    }

    public ObserveDataSourceConfigurationTopiaH2() {
        super.setConnectMode(DataSourceConnectMode.LOCAL);
    }

    @Override
    public void setConnectMode(DataSourceConnectMode connectMode) {
    }

    @Override
    public void setUrl(String url) {
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
        lockFile = databaseFile = null;
        url = null;
    }

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
        lockFile = databaseFile = null;
        url = null;
    }

    public File getDatabaseFile() {
        if (databaseFile == null && (directory != null && dbName != null)) {
            databaseFile = new File(directory, dbName + ".mv.db");
        }
        return databaseFile;
    }

    public File getLockFile() {
        if (lockFile == null && (directory != null && dbName != null)) {
            lockFile = new File(directory, dbName + ".lock.db");
        }
        return lockFile;
    }

    @Override
    public String getUrl() {
        if (url == null && (directory != null && dbName != null)) {
            String dbPath = new File(getDirectory(), getDbName()).getPath();
            url = String.format(H2_LOCAL_URL, dbPath);
        }
        return url;
    }

    @Override
    public ObserveDataSourceConfigurationTopiaH2 clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationTopiaH2) super.clone();
    }

    public String checkConfiguration(Locale applicationLocale) throws DatabaseNotFoundException {

        // On vérifie que le fichier de la base existe
        File databaseFile = getDatabaseFile();

        if (!databaseFile.exists()) {
            throw new DatabaseNotFoundException(applicationLocale);
        }

        // On vérifier que la base n'est pas déjà en cours d'utilisation
        File databaseLockFile = getLockFile();
        if (databaseLockFile.exists()) {

            return l(applicationLocale, "observe.services.topia.error.h2.database.locked");
            // FIXME even if file is lock we still authorize to connect,
            //throw new DatabaseConnexionNotAuthorizedException(message, dataSourceConfiguration);
        }
        return null;
    }
}
