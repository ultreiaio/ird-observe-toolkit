package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.security.Permission;
import fr.ird.observe.datasource.security.WithPermission;
import fr.ird.observe.dto.ObserveDto;
import io.ultreia.java4all.http.service.DataSourceConnection;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Représente une source de donnée une fois la connexion acceptée.
 * <p>
 * Created on 04/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConnection implements Serializable, ObserveDto, WithPermission, WithDataSourceConnectMode, DataSourceConnection {
    /**
     * Data source url.
     */
    private final String url;
    /**
     * Unique connection token.
     */
    private final String authenticationToken;
    /**
     * Data source information related to this connection.
     */
    private final ObserveDataSourceInformation dataSourceInformation;
    /**
     * Type of the datasource.
     */
    private final DataSourceConnectMode connectMode;

    public ObserveDataSourceConnection(DataSourceConnectMode connectMode, String url, String authenticationToken, ObserveDataSourceInformation dataSourceInformation) {
        this.connectMode = connectMode;
        this.url = Objects.requireNonNull(url);
        this.authenticationToken = Objects.requireNonNull(authenticationToken);
        this.dataSourceInformation = Objects.requireNonNull(dataSourceInformation);
    }

    @Override
    public DataSourceConnectMode getConnectMode() {
        return connectMode;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    @Override
    public Permission getCredentials() {
        return dataSourceInformation.getCredentials();
    }

    public ObserveDataSourceInformation getDataSourceInformation() {
        return dataSourceInformation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveDataSourceConnection)) return false;
        ObserveDataSourceConnection that = (ObserveDataSourceConnection) o;
        return Objects.equals(authenticationToken, that.getAuthenticationToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(authenticationToken);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ObserveDataSourceConnection.class.getSimpleName() + "[", "]")
                .add("authenticationToken='" + authenticationToken + "'")
                .add("dataSourceInformation=" + dataSourceInformation)
                .toString();
    }

    //FIXME FreeMarker can not manage with interface default methods :(
    @Override
    public boolean isLocal() {
        return WithDataSourceConnectMode.super.isLocal();
    }

    @Override
    public boolean isRemote() {
        return WithDataSourceConnectMode.super.isRemote();
    }

    @Override
    public boolean isServer() {
        return WithDataSourceConnectMode.super.isServer();
    }

    public ObserveDataSourceConnection toPermission(Permission permission) {
        return new ObserveDataSourceConnection(connectMode, url, authenticationToken, dataSourceInformation.toPermission(permission));
    }
}
