package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumerations;

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * Describes how to create a new data base.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
@TranslateEnumerations({
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_LABEL_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_DESCRIPTION, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_DESCRIPTION_PATTERN),
        @TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_CONFIG, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_CONFIG_PATTERN),
})
public enum DataSourceCreateMode {
    /**
     * créer une nouvelle base locale vide
     */
    EMPTY,
    /**
     * créer une nouvelle base a partir du dump embarque
     */
    IMPORT_INTERNAL_DUMP,
    /**
     * créer une nouvelle base a partir d'un dump sur fs
     */
    IMPORT_EXTERNAL_DUMP,
    /**
     * créer une nouvelle base vide et importer le referentiel via une base locale.
     */
    IMPORT_LOCAL_STORAGE,
    /**
     * créer une nouvelle base vide et importer le referentiel via une base distante.
     */
    IMPORT_REMOTE_STORAGE,
    /**
     * créer une nouvelle base vide et importer le referentiel via un serveur distant.
     */
    IMPORT_SERVER_STORAGE;

    private final String iconPath;
    private Icon icon;

    DataSourceCreateMode() {
        this.iconPath = "action.storage-create-mode-" + name();
    }

    public String getLabel() {
        return DataSourceCreateModeI18n.getLabel(this);
    }

    public String getDescription() {
        return DataSourceCreateModeI18n.getDescription(this);
    }

    public String getConfig() {
        return DataSourceCreateModeI18n.getConfig(this);
    }

    public Icon getIcon() {
        if (icon == null) {
            icon = UIManager.getIcon(iconPath);
        }
        return icon;
    }

}
