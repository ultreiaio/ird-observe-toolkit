package fr.ird.observe.datasource.configuration;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import io.ultreia.java4all.http.service.DataSourceConfiguration;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Version;
import io.ultreia.java4all.util.json.JsonAware;
import io.ultreia.java4all.util.sql.conf.JdbcConfigurationBuilder;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfiguration implements Serializable, Cloneable, JsonAware, WithDataSourceConnectMode, DataSourceConfiguration {

    /**
     * L'URL du serveur à utiliser.
     */
    protected String url;
    /**
     * Le libellé de la source de données.
     */
    private String label;
    /**
     * Le login de l'utilisateur.
     */
    private String login;

    /**
     * Le mot de passe de l'utilisateur.
     */
    private char[] password;

    /**
     * La version de la base demandée
     */
    private Version modelVersion;

    /**
     * Temporary directory.
     */
    private Path temporaryDirectory;

    /**
     * Type of the datasource.
     */
    private DataSourceConnectMode connectMode;

    //FIXME deplacer ça dans l'objet de création ou d'ouverture de base
    /**
     * Est-ce que la base doit être migrée à son ouverture?
     */
    private boolean canMigrate;

    private boolean autoMigrate;

    /**
     * Pour logger les requêtes sql
     **/
    private boolean traceSql;

    public ObserveDataSourceConnection toConnection(String token, ObserveDataSourceInformation dataSourceInformation) {
        return new ObserveDataSourceConnection(getConnectMode(), getUrl(), token, dataSourceInformation);
    }

    public boolean isH2Configuration() {
        // can not use the **url** field since it may be null and rebuild by method **getUrl()**
        return JdbcConfigurationBuilder.isH2Url(getUrl());
    }

    public boolean isPGConfiguration() {
        return JdbcConfigurationBuilder.isPostgreSqlUrl(getUrl());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObserveDataSourceConfiguration)) return false;
        ObserveDataSourceConfiguration that = (ObserveDataSourceConfiguration) o;
        return Objects.equals(getUrl(), that.getUrl()) &&
                Objects.equals(getLogin(), that.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUrl(), getLogin());
    }

    public String getLabel() {
        return I18n.t(label);
    }

    @Override
    public DataSourceConnectMode getConnectMode() {
        return connectMode;
    }

    public void setConnectMode(DataSourceConnectMode connectMode) {
        this.connectMode = connectMode;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char... password) {
        this.password = password;
    }

    public Version getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(Version modelVersion) {
        this.modelVersion = modelVersion;
    }

    public Path getTemporaryDirectory() {
        return temporaryDirectory;
    }

    public void setTemporaryDirectory(Path temporaryDirectory) {
        this.temporaryDirectory = temporaryDirectory;
    }

    public boolean isCanMigrate() {
        return canMigrate;
    }

    public void setCanMigrate(boolean canMigrate) {
        this.canMigrate = canMigrate;
    }

    public boolean isAutoMigrate() {
        return autoMigrate;
    }

    public void setAutoMigrate(boolean autoMigrate) {
        this.autoMigrate = autoMigrate;
    }

    public boolean isTraceSql() {
        return traceSql;
    }

    public void setTraceSql(boolean traceSql) {
        this.traceSql = traceSql;
    }

    @Override
    public ObserveDataSourceConfiguration clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfiguration) super.clone();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ObserveDataSourceConfiguration.class.getSimpleName() + "[", "]")
                .add("label='" + getLabel() + "'")
                .add("url='" + getUrl() + "'")
                .add("login='" + getLogin() + "'")
                .add("modelVersion=" + getModelVersion())
                .add("autoMigrate=" + isAutoMigrate())
                .toString();
    }

    //FIXME FreeMarker can not manage with interface default methods :(
    @Override
    public boolean isLocal() {
        return WithDataSourceConnectMode.super.isLocal();
    }

    @Override
    public boolean isRemote() {
        return WithDataSourceConnectMode.super.isRemote();
    }

    @Override
    public boolean isServer() {
        return WithDataSourceConnectMode.super.isServer();
    }

}
