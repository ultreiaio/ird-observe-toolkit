package fr.ird.observe.datasource.configuration.topia;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.datasource.configuration.DataSourceConnectMode;
import fr.ird.observe.datasource.configuration.ObserveDataSourceConfiguration;
import io.ultreia.java4all.util.Version;

/**
 * Created on 19/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObserveDataSourceConfigurationTopiaPG extends ObserveDataSourceConfiguration {

    private static final long serialVersionUID = 1L;

    /**
     * Est-ce que l'on doit utiliser une connexion ssl ?
     */
    private boolean useSsl;

    public static ObserveDataSourceConfigurationTopiaPG create(String label,
                                                               String url,
                                                               String username,
                                                               char[] password,
                                                               boolean useSsl,
                                                               Version dbVersion) {

        ObserveDataSourceConfigurationTopiaPG result = new ObserveDataSourceConfigurationTopiaPG();
        result.setLabel(label);
        result.setUrl(url);
        result.setLogin(username);
        result.setPassword(password);
        result.setUseSsl(useSsl);
        result.setModelVersion(dbVersion);
        return result;
    }

    public ObserveDataSourceConfigurationTopiaPG() {
        super.setConnectMode(DataSourceConnectMode.REMOTE);
    }

    @Override
    public void setConnectMode(DataSourceConnectMode connectMode) {
    }

    public boolean isUseSsl() {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }

    @Override
    public ObserveDataSourceConfigurationTopiaPG clone() throws CloneNotSupportedException {
        return (ObserveDataSourceConfigurationTopiaPG) super.clone();
    }

}
