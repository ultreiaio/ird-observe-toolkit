package fr.ird.observe.datasource;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.dto.I18nDecoratorHelper;
import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * Defines which API user can access.
 * Created on 06/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.23
 */
@TranslateEnumeration(name = I18nDecoratorHelper.I18N_CONSTANT_LABEL, pattern = I18nDecoratorHelper.I18N_CONSTANT_STORAGE_LABEL_PATTERN)
public enum DataSourceApiAccess {
    /**
     * Access only to public API.
     */
    PUBLIC,
    /**
     * Access only to client API.
     */
    CLIENT,
    /**
     * Access to both API.
     */
    ALL;

    public String getLabel() {
        return DataSourceApiAccessI18n.getLabel(this);
    }

}
