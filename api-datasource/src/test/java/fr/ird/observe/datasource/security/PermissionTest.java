package fr.ird.observe.datasource.security;

/*-
 * #%L
 * Toolkit :: API :: DataSource
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created on 21/11/2020.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 8.0.1
 */
public class PermissionTest implements WithPermission {

    private Permission credentials;

    @Override
    public Permission getCredentials() {
        return credentials;
    }

    @Test
    public void testNo() {
        credentials = Permission.NONE;
        assertCredential(false, false, false, false);
    }

    @Test
    public void testReadReferential() {
        credentials = Permission.READ_REFERENTIAL;
        assertCredential(true, false, false, false);
    }

    @Test
    public void testReadData() {
        credentials = Permission.READ_DATA;
        assertCredential(false, false, true, false);
    }

    @Test
    public void testWriteReferential() {
        credentials = Permission.WRITE_REFERENTIAL;
        assertCredential(true, true, false, false, Permission.READ_REFERENTIAL);
    }

    @Test
    public void testWriteData() {
        credentials = Permission.WRITE_DATA;
        assertCredential(false, false, true, true, Permission.READ_DATA);
    }

    @Test
    public void testReadAll() {
        credentials = Permission.READ_ALL;
        assertCredential(true, false, true, false, Permission.READ_DATA, Permission.READ_REFERENTIAL);
    }

    @Test
    public void testReadAllAndWriteData() {
        credentials = Permission.READ_ALL_AND_WRITE_DATA;
        assertCredential(true, false, true, true, Permission.READ_REFERENTIAL, Permission.READ_DATA, Permission.READ_ALL, Permission.WRITE_DATA);
    }

    @Test
    public void testAll() {
        credentials = Permission.ALL;
        assertCredential(true, true, true, true, Permission.values());
    }

    void assertCredential(boolean readReferential, boolean writeReferential, boolean readData, boolean writeData, Permission... permissions) {
        Assert.assertEquals(readReferential, canReadReferential());
        Assert.assertEquals(writeReferential, canWriteReferential());
        Assert.assertEquals(readData, canReadData());
        Assert.assertEquals(writeData, canWriteData());
        List<Permission> acceptedPermissions = new ArrayList<>(Arrays.asList(permissions));
        acceptedPermissions.add(Permission.NONE);
        acceptedPermissions.add(getCredentials());
        for (Permission permission : Permission.values()) {
            if (acceptedPermissions.contains(permission)) {
                Assert.assertTrue(credentials + " should contains " + permission, credentials.contains(permission));
            } else {
                Assert.assertFalse(credentials + " should NOT contains " + permission, credentials.contains(permission));
            }
        }
    }
}
