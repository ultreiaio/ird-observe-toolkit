# Abstract

This module exposes the **Data source** API:

  * configuration API
  * security API (to generate and apply proper rights on databases)
  * request API on data source (high level operations on data source)