package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServerDatabase {

    private String name;
    private String url;
    private boolean defaultDatabase;
    private LinkedHashSet<ServerRole> roles;

    public ServerDatabase() {
        roles = new LinkedHashSet<>();
    }

    public ServerDatabase(String name, String url, boolean defaultDatabase, LinkedHashSet<ServerRole> roles) {
        this.name = name;
        this.url = url;
        this.defaultDatabase = defaultDatabase;
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefaultDatabase() {
        return defaultDatabase;
    }

    public void setDefaultDatabase(boolean defaultDatabase) {
        this.defaultDatabase = defaultDatabase;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LinkedHashSet<ServerRole> getRoles() {
        return roles;
    }

    public void setRoles(LinkedHashSet<ServerRole> roles) {
        this.roles = roles;
    }

    public Optional<ServerRole> getDatabaseRoleByLogin(String login) {
        return roles.stream().filter(r -> Objects.equals(login, r.getLogin())).findFirst();
    }
}
