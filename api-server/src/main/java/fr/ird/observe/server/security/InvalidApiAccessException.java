package fr.ird.observe.server.security;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Locale;

/**
 * Created on 06/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.23
 */
public class InvalidApiAccessException extends SecurityExceptionSupport {

    private static final long serialVersionUID = 1L;

    protected final DataSourceApiAccess requiredDataSourceApiAccess;
    protected final DataSourceApiAccess actualDataSourceApiAccess;

    public InvalidApiAccessException(Locale locale, DataSourceApiAccess requiredDataSourceApiAccess, DataSourceApiAccess actualDataSourceApiAccess) {
        this(I18n.l(locale, "observe.ui.datasource.storage.error.rest.apiAccess.invalid", requiredDataSourceApiAccess, actualDataSourceApiAccess), requiredDataSourceApiAccess, actualDataSourceApiAccess);
    }

    public InvalidApiAccessException(String message, DataSourceApiAccess requiredDataSourceApiAccess, DataSourceApiAccess actualDataSourceApiAccess) {
        super(message);
        this.requiredDataSourceApiAccess = requiredDataSourceApiAccess;
        this.actualDataSourceApiAccess = actualDataSourceApiAccess;
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<InvalidApiAccessException> {

        @Override
        public InvalidApiAccessException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new InvalidApiAccessException(message,
                                                 context.deserialize(json.get("requiredDataSourceApiAccess"), DataSourceApiAccess.class),
                                                 context.deserialize(json.get("actualDataSourceApiAccess"), DataSourceApiAccess.class));
        }

        @Override
        public Class<?> type() {
            return InvalidApiAccessException.class;
        }
    }
}

