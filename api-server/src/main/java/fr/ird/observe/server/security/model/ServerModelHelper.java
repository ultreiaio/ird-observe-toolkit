package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.DataSourceValidationMode;
import io.ultreia.java4all.lang.Strings;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public class ServerModelHelper {
    private static final YamlStorage<ServerModel> STORAGE = new YamlStorage<>(ServerModel.class, "server") {

        @Override
        protected void initConfig(YamlConfig config) {
            config.setPropertyElementType(ServerModel.class, "roles", ServerRole.class);
            config.setPropertyElementType(ServerModel.class, "databases", ServerDatabase.class);
            config.setPropertyElementType(ServerModel.class, "users", ServerUser.class);
            config.setPropertyElementType(ServerDatabase.class, "roles", ServerRole.class);
            config.setPropertyElementType(ServerUser.class, "permissions", ServerUserPermission.class);
        }

        @Override
        protected YamlWriterExt<ServerModel> createYamlWriter(Writer writer) {
            return new YamlWriterExt<>(writer, createConfig()) {
                private final Map<String, ServerRole> roleMapping = new TreeMap<>();
                private final Map<String, ServerDatabase> databasesMapping = new TreeMap<>();

                @Override
                protected void initWriter(ServerModel o) {
                    o.getRoles().forEach(i -> {
                        String login = i.getLogin();
                        roleMapping.put(login, i);
                        setAlias(i, String.format("role-%s", login));
                        i.setLogin(null);
                    });
                    o.getDatabases().forEach(i -> {
                        String name = i.getName();
                        databasesMapping.put(name, i);
                        setAlias(i, String.format("db-%s", name));
                        i.setName(null);
                    });
                }

                @Override
                protected void endWriter(ServerModel o) {
                    roleMapping.forEach((k, v) -> v.setLogin(k));
                    databasesMapping.forEach((k, v) -> v.setName(k));
                }
            };
        }

        @Override
        protected ServerModel load(YamlReaderExt yamlReader) throws YamlException {
            ServerModel load = super.load(yamlReader);
            for (Map.Entry<String, Object> entry : yamlReader.getAnchors().entrySet()) {
                String alias = entry.getKey();
                Object o = entry.getValue();
                if (o instanceof ServerRole) {
                    ServerRole o1 = (ServerRole) o;
                    if (Strings.isEmpty(o1.getLogin())) {
                        String login = alias.substring(alias.indexOf("-") + 1);
                        o1.setLogin(login);
                    }
                }
                if (o instanceof ServerDatabase) {
                    ServerDatabase o1 = (ServerDatabase) o;
                    if (Strings.isEmpty(o1.getName())) {
                        String name = alias.substring(alias.indexOf("-") + 1);
                        o1.setName(name);
                    }
                }
            }
            load.postLoad();
            return load;
        }

        @Override
        public void store(Writer writer, ServerModel o) {
            o.preWrite();
            try (StringWriter stringWriter = new StringWriter()) {
                super.store(stringWriter, o);
                String content = stringWriter.toString();
                fixIndentation(content, writer);
            } catch (IOException e) {
                throw new IllegalStateException("Could not store object: " + o, e);
            }

        }
    };

    public static void fixIndentation(String content, Writer writer) throws IOException {
        for (String line : content.split("\\n")) {
            writer.write(line.replace("- &role", "  - &role")
                                 .replace("  login:", "    login:")
                                 .replace("  defaultRole:", "    defaultRole:")
                                 .replace("  password:", "    password:")
                                 .replace("- &db", "  - &db")
                                 .replace("  name:", "    name:")
                                 .replace("  url:", "    url:")
                                 .replace("  defaultDatabase:", "    defaultDatabase:")
                                 .replace("  roles:", "    roles:")
                                 .replace("  - *role", "      - *role")
                                 .replace("- login:", "  - login:")
                                 .replace("  permissions: ", "    permissions:")
                                 .replace("  defaultApiAccess:", "    defaultApiAccess:")
                                 .replace("  defaultValidationMode:", "    defaultValidationMode:")
                                 .replace("  - database:", "      - database:")
                                 .replace("    role:", "        role:")
                                 .replace("    apiAccess:", "        apiAccess:")
                                 .replace("    validationMode:", "        validationMode:")
            );
            writer.write("\n");
        }
    }

    public static ServerModel load(File file) {
        return STORAGE.load(file);
    }

    public static ServerModel load(String content) {
        return STORAGE.load(content);
    }

    public static void store(File file, ServerModel o) {
        STORAGE.store(file, o);
    }

    public static void store(Writer writer, ServerModel o) {
        STORAGE.store(writer, o);
    }

    public static String toString(ServerModel o) {
        try (StringWriter writer = new StringWriter()) {
            store(writer, o);
            return writer.toString();
        } catch (IOException e) {
            throw new IllegalStateException("Can't write model", e);
        }
    }

    public static void validate(ServerModel serverModel) throws InvalidServerModelException {
        Set<String> availableRoleLoginSet = validateRoles(serverModel.getRoles());
        Set<String> availableDatabaseNameSet = validateDatabases(serverModel.getDatabases(), availableRoleLoginSet);
        validateUsers(serverModel.getUsers(), availableRoleLoginSet, availableDatabaseNameSet);
    }

    static Set<String> validateRoles(LinkedHashSet<ServerRole> roles) throws InvalidServerModelException {
        if (roles == null || roles.isEmpty()) {
            throw new InvalidServerModelException("No role defined");
        }
        // unique role login
        Set<String> roleLoginSet = new LinkedHashSet<>();
        for (ServerRole role : roles) {
            String login = role.getLogin();
            if (Strings.isEmpty(login)) {
                throw new InvalidServerModelException("Found a role with no login defined");
            }
            if (Strings.isEmpty(role.getPassword())) {
                throw new InvalidServerModelException(String.format("User %s has no password defined", login));
            }
            if (!roleLoginSet.add(login)) {
                throw new InvalidServerModelException(String.format("Not unique role login found: %s", login));
            }
        }
        return roleLoginSet;
    }

    static Set<String> validateDatabases(LinkedHashSet<ServerDatabase> databases, Set<String> availableRoleLoginSet) throws InvalidServerModelException {
        if (databases == null || databases.isEmpty()) {
            throw new InvalidServerModelException("No database defined");
        }
        // need one default database, or none
        ServerDatabase defaultDatabase = null;
        // unique database name
        Set<String> databaseNameSet = new LinkedHashSet<>();
        for (ServerDatabase database : databases) {
            String databaseName = database.getName();
            if (Strings.isEmpty(databaseName)) {
                throw new InvalidServerModelException("Found a database with no name defined");
            }
            if (Strings.isEmpty(database.getUrl())) {
                throw new InvalidServerModelException(String.format("Database %s has no url defined", databaseName));
            }
            if (database.getRoles() == null || database.getRoles().isEmpty()) {
                throw new InvalidServerModelException(String.format("Database %s has no roles defined", databaseName));
            }
            if (!databaseNameSet.add(databaseName)) {
                throw new InvalidServerModelException(String.format("Not unique database name found: %s", databaseName));
            }
            if (database.isDefaultDatabase()) {
                if (defaultDatabase != null) {
                    throw new InvalidServerModelException(String.format("Found two default databases: %s and %s", defaultDatabase.getName(), databaseName));
                }
                defaultDatabase = database;
            }
            // unique database login name
            Set<String> databaseLoginSet = new LinkedHashSet<>();
            for (ServerRole role : database.getRoles()) {
                String roleLogin = role.getLogin();
                if (Strings.isEmpty(roleLogin)) {
                    throw new InvalidServerModelException(String.format("In database %s, found a role with no login defined", databaseName));
                }
                if (Strings.isEmpty(role.getPassword())) {
                    throw new InvalidServerModelException(String.format("In database %s, found a role %s with no password defined", databaseName, roleLogin));
                }
                if (!availableRoleLoginSet.contains(roleLogin)) {
                    throw new InvalidServerModelException(String.format("Database %s, role %s is not defined in roles section.", databaseName, roleLogin));
                }
                if (!databaseLoginSet.add(roleLogin)) {
                    throw new InvalidServerModelException(String.format("Database %s contains a doubloon login: %s", databaseName, roleLogin));
                }
            }
        }
//        if (defaultDatabase == null) {
//            throw new InvalidServerModelException("No default databases defined");
//        }
        return databaseNameSet;
    }

    static void validateUsers(LinkedHashSet<ServerUser> users, Set<String> availableRoleLoginSet, Set<String> availableDatabaseNameSet) throws InvalidServerModelException {
        if (users == null || users.isEmpty()) {
            throw new InvalidServerModelException("No user defined");
        }
        // unique user permission role login
        Set<String> userLoginSet = new LinkedHashSet<>();
        for (ServerUser user : users) {
            String login = user.getLogin();
            if (Strings.isEmpty(login)) {
                throw new InvalidServerModelException("Found a user with no login defined");
            }
            if (Strings.isEmpty(user.getPassword())) {
                throw new InvalidServerModelException(String.format("User %s has no password defined", login));
            }
            if (user.getPermissions() == null || user.getPermissions().isEmpty()) {
                throw new InvalidServerModelException(String.format("User %s has no permission defined", login));
            }
            if (!userLoginSet.add(login)) {
                throw new InvalidServerModelException(String.format("Not unique user login found: %s", login));
            }
            // unique user permission database name
            Set<String> userPermissionDatabaseNameSet = new LinkedHashSet<>();
            for (ServerUserPermission permission : user.getPermissions()) {
                ServerDatabase userDatabase = permission.getDatabase();
                if (userDatabase == null) {
                    throw new InvalidServerModelException(String.format("User %s, found a permission with no database name defined", login));
                }
                ServerRole userPermission = permission.getRole();
                String userDatabaseName = userDatabase.getName();
                if (userPermission == null) {
                    throw new InvalidServerModelException(String.format("User %s, found a permission %s with no role defined", login, userDatabaseName));
                }
                DataSourceApiAccess apiAccess = permission.getApiAccess();
                if (apiAccess == null) {
                    throw new InvalidServerModelException(String.format("User %s, found a permission %s with no api access defined", login, userDatabaseName));
                }
                DataSourceValidationMode validationMode = permission.getValidationMode();
                if (validationMode == null) {
                    throw new InvalidServerModelException(String.format("User %s, found a permission %s with no validation mode defined", login, userDatabaseName));
                }
                if (!userPermissionDatabaseNameSet.add(userDatabaseName)) {
                    throw new InvalidServerModelException(String.format("User %s contains with a doubloon database permission: %s", login, userDatabaseName));
                }
                if (!availableDatabaseNameSet.contains(userDatabaseName)) {
                    throw new InvalidServerModelException(String.format("User %s, permission database %s not found in databases section", login, userDatabaseName));
                }
                String userPermissionLogin = userPermission.getLogin();
                if (!availableRoleLoginSet.contains(userPermissionLogin)) {
                    throw new InvalidServerModelException(String.format("User %s, permission role %s not found in roles section", login, userPermissionLogin));
                }
            }
        }
    }

    private ServerModelHelper() {
    }
}
