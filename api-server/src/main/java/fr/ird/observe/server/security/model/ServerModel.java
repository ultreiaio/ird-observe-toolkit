package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.DataSourceValidationMode;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created on 08/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public class ServerModel {

    private LinkedHashSet<ServerRole> roles;
    private LinkedHashSet<ServerDatabase> databases;
    private LinkedHashSet<ServerUser> users;

    public ServerModel() {
    }

    public ServerModel(Collection<ServerRole> roles, Collection<ServerDatabase> databases, Collection<ServerUser> users) {
        this.roles = new LinkedHashSet<>(roles);
        this.databases = new LinkedHashSet<>(databases);
        this.users = new LinkedHashSet<>(users);
    }

    public LinkedHashSet<ServerRole> getRoles() {
        return roles;
    }

    public void setRoles(LinkedHashSet<ServerRole> roles) {
        this.roles = roles;
    }

    public LinkedHashSet<ServerDatabase> getDatabases() {
        return databases;
    }

    public void setDatabases(LinkedHashSet<ServerDatabase> databases) {
        this.databases = databases;
    }

    public LinkedHashSet<ServerUser> getUsers() {
        return users;
    }

    public void setUsers(LinkedHashSet<ServerUser> users) {
        this.users = users;
    }

    public Optional<ServerDatabase> getDatabaseByName(String databaseName) {
        return databases.stream().filter(d -> Objects.equals(databaseName, d.getName())).findFirst();
    }

    public Optional<ServerUser> getUserByLogin(String login) {
        return users.stream().filter(u -> Objects.equals(login, u.getLogin())).findFirst();
    }

    public Optional<ServerDatabase> getDefaultDatabase() {
        return databases.stream().filter(ServerDatabase::isDefaultDatabase).findFirst();
    }

    public void postLoad() {
        // add user default values to his permissions
        if (users != null) {
            for (ServerUser user : users) {
                ServerRole defaultRole = user.getDefaultRole();
                DataSourceApiAccess defaultApiAccess = user.getDefaultApiAccess();
                DataSourceValidationMode defaultValidationMode = user.getDefaultValidationMode();
                for (ServerUserPermission permission : user.getPermissions()) {
                    if (defaultRole != null && permission.getRole() == null) {
                        permission.setRole(defaultRole);
                    }
                    if (permission.getApiAccess() == null) {
                        permission.setApiAccess(defaultApiAccess);
                    }
                    if (permission.getValidationMode() == null) {
                        permission.setValidationMode(defaultValidationMode);
                    }
                }
            }
        }
    }

    public void preWrite() {
        // extract user default values from his permissions
        if (users != null) {
            for (ServerUser user : users) {
                ServerRole defaultRole = user.getDefaultRole();
                DataSourceApiAccess defaultApiAccess = user.getDefaultApiAccess();
                DataSourceValidationMode defaultValidationMode = user.getDefaultValidationMode();
                Map<String, ServerRole> rolesUsed = new LinkedHashMap<>();
                Set<DataSourceApiAccess> apiAccessUsed = new LinkedHashSet<>();
                Set<DataSourceValidationMode> validationModeUsed = new LinkedHashSet<>();
                int permissionsCount = user.getPermissions().size();
                for (ServerUserPermission permission : user.getPermissions()) {
                    ServerRole permissionRole = permission.getRole();
                    if (permissionRole != null) {
                        if (Objects.equals(defaultRole, permissionRole)) {
                            permission.setRole(null);
                        } else {
                            rolesUsed.put(permissionRole.getLogin(), permissionRole);
                        }
                    }
                    DataSourceApiAccess permissionApiAccess = permission.getApiAccess();
                    if (permissionApiAccess != null) {
                        if (Objects.equals(defaultApiAccess, permissionApiAccess)) {
                            permission.setApiAccess(null);
                        } else {
                            apiAccessUsed.add(permissionApiAccess);
                        }
                    }
                    DataSourceValidationMode permissionValidationMode = permission.getValidationMode();
                    if (permissionValidationMode != null) {
                        if (Objects.equals(defaultValidationMode, permissionValidationMode)) {
                            permission.setValidationMode(null);
                        } else {
                            validationModeUsed.add(permissionValidationMode);
                        }
                    }
                }
                if (permissionsCount > 1) {
                    if (defaultRole == null && rolesUsed.size() == 1) {
                        defaultRole = rolesUsed.values().iterator().next();
                        user.setDefaultRole(defaultRole);
                        user.getPermissions().forEach(permission -> permission.setRole(null));
                    }
                    if (apiAccessUsed.size() == 1) {
                        defaultApiAccess = apiAccessUsed.iterator().next();
                        user.setDefaultApiAccess(defaultApiAccess);
                        user.getPermissions().forEach(permission -> permission.setApiAccess(null));
                    }
                    if (validationModeUsed.size() == 1) {
                        defaultValidationMode = validationModeUsed.iterator().next();
                        user.setDefaultValidationMode(defaultValidationMode);
                        user.getPermissions().forEach(permission -> permission.setValidationMode(null));
                    }
                }
            }
        }
    }
}
