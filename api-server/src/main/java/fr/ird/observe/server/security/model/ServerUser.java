package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.DataSourceValidationMode;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServerUser extends ServerRole {

    /**
     * Optional default role (if set all permissions with no role will use this one).
     */
    private ServerRole defaultRole;
    /**
     * Optional Api access (if set all permissions with no apiAccess will use this one).
     * <p>
     * (by default keep only the client one to avoid any migration in existing configurations)
     */
    private DataSourceApiAccess defaultApiAccess = DataSourceApiAccess.CLIENT;
    /**
     * Optional validation model (if set all permissions with no validationMode will use this one).
     * <p>
     * (by default strong to be safe).
     */
    private DataSourceValidationMode defaultValidationMode = DataSourceValidationMode.STRONG;

    private LinkedHashSet<ServerUserPermission> permissions;

    public ServerUser() {
    }

    public ServerUser(String login, String password, ServerRole defaultRole, Collection<ServerUserPermission> permissions) {
        super(login, password);
        this.defaultRole = defaultRole;
        this.permissions = new LinkedHashSet<>(permissions);
    }

    public ServerRole getDefaultRole() {
        return defaultRole;
    }

    public void setDefaultRole(ServerRole defaultRole) {
        this.defaultRole = defaultRole;
    }

    public DataSourceApiAccess getDefaultApiAccess() {
        return defaultApiAccess;
    }

    public void setDefaultApiAccess(DataSourceApiAccess defaultApiAccess) {
        this.defaultApiAccess = defaultApiAccess;
    }

    public DataSourceValidationMode getDefaultValidationMode() {
        return defaultValidationMode;
    }

    public void setDefaultValidationMode(DataSourceValidationMode defaultValidationMode) {
        this.defaultValidationMode = defaultValidationMode;
    }

    public LinkedHashSet<ServerUserPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(LinkedHashSet<ServerUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Optional<ServerUserPermission> getPermissionByDatabaseName(String databaseName) {
        return permissions.stream().filter(p -> Objects.equals(p.getDatabase().getName(), databaseName)).findFirst();
    }
}
