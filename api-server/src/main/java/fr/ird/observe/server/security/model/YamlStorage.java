package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;
import java.util.Objects;

/**
 * FIXME Move this to a new module api-server-model?
 * <p>
 * Defines a Yaml storage.
 * <p>
 * Created on 08/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public abstract class YamlStorage<O> {
    private static final Logger log = LogManager.getLogger(YamlStorage.class);

    private final String label;
    private final Class<O> type;

    protected static class YamlWriterExt<O> extends YamlWriter {

        public YamlWriterExt(Writer writer, YamlConfig config) {
            super(writer, config);
        }

        protected void initWriter(O o) {
        }

        protected void endWriter(O o) {
        }
    }

    protected static class YamlReaderExt extends YamlReader {

        private final Map<String, Object> anchors;

        public YamlReaderExt(Reader reader, YamlConfig config) {
            super(reader, config);
            anchors = getSuperAnchors();
        }

        public Map<String, Object> getAnchors() {
            return anchors;
        }

        @SuppressWarnings("unchecked")
        private Map<String, Object> getSuperAnchors() {
            try {
                Field field = YamlReader.class.getDeclaredField("anchors");
                field.setAccessible(true);
                return (Map<String, Object>) field.get(this);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static YamlConfig createDefaultConfig() {
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.writeConfig.setIndentSize(2);
        yamlConfig.writeConfig.setWriteRootTags(false);
        yamlConfig.writeConfig.setKeepBeanPropertyOrder(true);
        yamlConfig.writeConfig.setAutoAnchor(true);
        return yamlConfig;
    }

    public YamlStorage(Class<O> type, String label) {
        this.label = Objects.requireNonNull(label);
        this.type = Objects.requireNonNull(type);
    }

    protected abstract void initConfig(YamlConfig config);

    public O load(File file) {
        log.info(String.format("Loading %s from file: %s", label, file));
        try (Reader fileReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {
            return load(fileReader);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not read %s from file: %s", label, file), e);
        }
    }

    public O load(String content) {
        log.info(String.format("Loading %s from content (length: %s)", label, content.length()));
        try (Reader reader = new StringReader(content)) {
            return load(reader);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not read %s from file: %s", label, content), e);
        }
    }

    public O load(Reader reader) throws Exception {
        YamlReaderExt yamlReader = new YamlReaderExt(reader, createConfig());
        return load(yamlReader);
    }

    protected O load(YamlReaderExt yamlReader) throws YamlException {
        return yamlReader.read(type);
    }

    public void store(File file, O o) {
        log.info(String.format("Store %s to %s", label, file));
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8)) {
            store(writer, o);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not write %s to file: %s", label, file), e);
        }
    }

    public void store(Writer writer, O o) {
        YamlWriterExt<O> yamlWriter = createYamlWriter(writer);
        try {
            yamlWriter.initWriter(o);
            try {
                yamlWriter.write(o);
            } finally {
                try {
                    yamlWriter.close();
                } finally {
                    yamlWriter.endWriter(o);
                }
            }
        } catch (YamlException e) {
            throw new IllegalStateException("Could not store object: " + o, e);
        }
    }

    protected YamlWriterExt<O> createYamlWriter(Writer writer) {
        return new YamlWriterExt<>(writer, createConfig());
    }

    protected YamlConfig createConfig() {
        YamlConfig config = createDefaultConfig();
        initConfig(config);
        return config;
    }
}
