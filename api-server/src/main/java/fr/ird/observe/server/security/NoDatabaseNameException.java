package fr.ird.observe.server.security;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Locale;
import java.util.Set;

/**
 * When server model does not defined a default database name and user try to connect without specify one
 * on his connexion configuration.
 * <p>
 * Created on 26/08/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.7
 */
public class NoDatabaseNameException extends SecurityExceptionSupport {

    private static final long serialVersionUID = 1L;

    public NoDatabaseNameException(Locale locale, Set<String> availableDatabaseNames) {
        this(I18n.l(locale, "observe.ui.datasource.storage.error.rest.database.name.required", availableDatabaseNames));
    }

    public NoDatabaseNameException(String message) {
        super(message);
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<NoDatabaseNameException> {

        @Override
        public NoDatabaseNameException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new NoDatabaseNameException(message);
        }

        @Override
        public Class<?> type() {
            return NoDatabaseNameException.class;
        }
    }
}
