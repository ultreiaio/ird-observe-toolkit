package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.datasource.DataSourceApiAccess;
import fr.ird.observe.datasource.DataSourceValidationMode;

import java.util.Objects;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServerUserPermission {
    /**
     * Database name.
     */
    private ServerDatabase database;
    /**
     * Role name.
     */
    private ServerRole role;
    /**
     * Api access.
     */
    private DataSourceApiAccess apiAccess;
    /**
     * Validation mode.
     */
    private DataSourceValidationMode validationMode;

    public ServerUserPermission() {
    }

    public ServerUserPermission(ServerDatabase database, ServerRole role, DataSourceApiAccess apiAccess, DataSourceValidationMode validationMode) {
        this.database = database;
        this.role = role;
        if (apiAccess != null) {
            this.apiAccess = apiAccess;
        }
        if (validationMode != null) {
            this.validationMode = validationMode;
        }
    }

    public ServerDatabase getDatabase() {
        return database;
    }

    public void setDatabase(ServerDatabase database) {
        this.database = database;
    }

    public ServerRole getRole() {
        return role;
    }

    public void setRole(ServerRole role) {
        this.role = role;
    }

    public DataSourceApiAccess getApiAccess() {
        return apiAccess;
    }

    public void setApiAccess(DataSourceApiAccess apiAccess) {
        this.apiAccess = apiAccess;
    }

    public DataSourceValidationMode getValidationMode() {
        return validationMode;
    }

    public void setValidationMode(DataSourceValidationMode validationMode) {
        this.validationMode = validationMode;
    }

    public boolean rejectApiAccess(DataSourceApiAccess apiAccess) {
        if (this.apiAccess == null) {
            return false;
        }
        if (Objects.equals(this.apiAccess, apiAccess)) {
            return false;
        }
        return this.apiAccess != DataSourceApiAccess.ALL && apiAccess != DataSourceApiAccess.ALL;
    }

}
