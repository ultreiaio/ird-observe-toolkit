package fr.ird.observe.server.security;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import fr.ird.observe.spi.json.ThrowableAdapterSupport;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.json.JsonAdapter;

import java.util.Locale;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UnknownObserveWebUserForDatabaseException extends SecurityExceptionSupport {

    private static final long serialVersionUID = 1L;

    public UnknownObserveWebUserForDatabaseException(Locale locale, String databaseName, String role) {
        this(I18n.l(locale, "observe.ui.datasource.storage.error.rest.database.unknownForUser", databaseName, role));
    }

    public UnknownObserveWebUserForDatabaseException(String message) {
        super(message);
    }

    @AutoService(JsonAdapter.class)
    public static class GsonAdapter extends ThrowableAdapterSupport<UnknownObserveWebUserForDatabaseException> {

        @Override
        public UnknownObserveWebUserForDatabaseException create(JsonObject json, String message, JsonDeserializationContext context) throws JsonParseException {
            return new UnknownObserveWebUserForDatabaseException(message);
        }

        @Override
        public Class<?> type() {
            return UnknownObserveWebUserForDatabaseException.class;
        }
    }
}
