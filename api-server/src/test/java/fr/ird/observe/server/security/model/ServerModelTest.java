package fr.ird.observe.server.security.model;

/*-
 * #%L
 * Toolkit :: API :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.config.ConfigResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 08/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.62
 */
public class ServerModelTest {

    public static final ConfigResource RESOURCE = new ConfigResource("/fixtures-security.yml");
    protected File configFile;

    protected static void assertServerUser(ServerUser user, String expectedLogin, String expectedPassword, int expectedPermissionsCount) {
        Assert.assertNotNull(user);
        Assert.assertEquals(expectedLogin, user.getLogin());
        Assert.assertEquals(expectedPassword, user.getPassword());
        Assert.assertNotNull(user.getPermissions());
        Assert.assertEquals(expectedPermissionsCount, user.getPermissions().size());
    }

    protected static void assertServerUserPermission(ServerUserPermission user, String expectedDatabaseName, String expectedRoleLogin) {
        Assert.assertNotNull(user);
        Assert.assertEquals(expectedDatabaseName, user.getDatabase().getName());
        Assert.assertEquals(expectedRoleLogin, user.getRole().getLogin());
    }

    @Before
    public void setUp() throws IOException {
        configFile = Files.createTempDirectory("security.yml").toFile();
    }

    @Test(expected = InvalidServerModelException.class)
    public void testEmpty() throws InvalidServerModelException {
        ServerModel model = new ServerModel();
        ServerModelHelper.validate(model);
    }

    @Test
    public void testLoadBean() {

        RESOURCE.copyResource(configFile);

        ServerModel model = ServerModelHelper.load(configFile);
        Assert.assertNotNull(model);
        List<ServerDatabase> databasesSet = new LinkedList<>(model.getDatabases());
        Assert.assertNotNull(databasesSet);
        Assert.assertEquals(2, databasesSet.size());

        {
            ServerDatabase database = databasesSet.get(0);
            Assert.assertNotNull(database);
            Assert.assertEquals("production", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(4, database.getRoles().size());
        }
        {
            ServerDatabase database = databasesSet.get(1);
            Assert.assertNotNull(database);
            Assert.assertEquals("test", database.getName());
            Assert.assertNotNull(database.getRoles());
            Assert.assertEquals(5, database.getRoles().size());
        }
        List<ServerUser> usersSet = new LinkedList<>(model.getUsers());
        Assert.assertNotNull(usersSet);
        Assert.assertEquals(2, usersSet.size());
        {
            ServerUser user = usersSet.get(0);
            assertServerUser(user, "user1", "password1", 2);
            Iterator<ServerUserPermission> iterator = user.getPermissions().iterator();
            assertServerUserPermission(iterator.next(), "production", "admin");
            assertServerUserPermission(iterator.next(), "test", "admin");
        }
        {
            ServerUser user = usersSet.get(1);
            assertServerUser(user, "user2", "password2", 1);
            Iterator<ServerUserPermission> iterator = user.getPermissions().iterator();
            assertServerUserPermission(iterator.next(), "test", "referentiel");
        }
    }

    @Test
    public void testStoreBean() throws Exception {
        RESOURCE.copyResource(configFile);
        List<String> expectedContent;
        try {
            expectedContent = Files.readAllLines(configFile.toPath());
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Could not read resource %s to %s", this, configFile), e);
        }
        ServerRole adminRole, technicienRole, referentialRote;
        ServerDatabase production, test;
        List<ServerRole> roleSet = List.of(
                adminRole = new ServerRole("admin", "passwordAdmin"),
                technicienRole = new ServerRole("technicien", "passwordTechnicien"),
                new ServerRole("utilisateur", "passwordUtilisateur"),
                referentialRote = new ServerRole("referentiel", "passwordReferentiel"),
                new ServerRole("test", "passwordTest"));
        ServerModel databases = new ServerModel(roleSet, List.of(
                production = new ServerDatabase("production", "jdbc:postgresql://localhost:5432/production", true, roleSet.stream().filter(r -> !r.getLogin().equals("test")).collect(Collectors.toCollection(LinkedHashSet::new))),
                test = new ServerDatabase("test", "jdbc:postgresql://localhost:5432/test", false, new LinkedHashSet<>(roleSet))),
                                                List.of(
                                                        new ServerUser("user1", "password1", adminRole, List.of(
                                                                new ServerUserPermission(production, null, null, null),
                                                                new ServerUserPermission(test, null, null, null)
                                                        )),
                                                        new ServerUser("user2", "password2", null, List.of(
                                                                new ServerUserPermission(test, referentialRote, null, null)
                                                        ))
                                                ));
        ServerModelHelper.store(configFile, databases);

        List<String> fileContent = Files.readAllLines(configFile.toPath());
        Assert.assertEquals(expectedContent.size(), fileContent.size());
        for (int i = 0; i < fileContent.size(); i++) {
            String actual = fileContent.get(i);
            String expected = expectedContent.get(i);
            Assert.assertEquals(expected.trim(), actual.trim());
        }
    }
}

