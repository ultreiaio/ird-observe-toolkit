package org.debux.webmotion.server.handler.converter;

/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import io.ultreia.java4all.util.json.JsonAware;
import org.apache.commons.beanutils.converters.AbstractConverter;

/**
 * Created on 07/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JsonAwareDtoConverter<TT extends JsonAware> extends AbstractConverter {

    protected final Gson gson;
    private final Class<TT> type;

    public JsonAwareDtoConverter(Gson gson, Class<TT> type) {
        this.gson = gson;
        this.type = type;
    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) {
        if (type.isAssignableFrom(type)) {
            String gsonContent = value.toString();
            return gson.fromJson(gsonContent, type);
        }
        throw conversionException(type, value);
    }

    @Override
    protected Class<TT> getDefaultType() {
        return type;
    }
}
