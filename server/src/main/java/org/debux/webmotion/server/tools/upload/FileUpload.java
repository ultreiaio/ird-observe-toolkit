package org.debux.webmotion.server.tools.upload;

/*-
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.fileupload2.core.AbstractFileUpload;
import org.apache.commons.fileupload2.core.DiskFileItem;
import org.apache.commons.fileupload2.core.DiskFileItemFactory;
import org.apache.commons.fileupload2.core.FileItemInputIterator;
import org.apache.commons.fileupload2.core.FileUploadException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created at 24/11/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.3.0
 */
public class FileUpload extends AbstractFileUpload<HttpServletRequest, DiskFileItem, DiskFileItemFactory> {

    public static final Set<String> ACCEPTED_METHOD = Set.of("post","put");
    public FileUpload(DiskFileItemFactory fileItemFactory) {
        setFileItemFactory(Objects.requireNonNull(fileItemFactory));
    }

    public static boolean isMultipartContent(final HttpServletRequest request) {
        return ACCEPTED_METHOD.contains(request.getMethod().toLowerCase()) && AbstractFileUpload.isMultipartContent(new RequestContext(request));
    }

    @Override
    public FileItemInputIterator getItemIterator(HttpServletRequest request) throws IOException {
        return super.getItemIterator(new RequestContext(request));
    }

    @Override
    public Map<String, List<DiskFileItem>> parseParameterMap(HttpServletRequest request) throws FileUploadException {
        return super.parseParameterMap(new RequestContext(request));
    }

    @Override
    public List<DiskFileItem> parseRequest(HttpServletRequest request) throws FileUploadException {
        return super.parseRequest(new RequestContext(request));
    }
}
