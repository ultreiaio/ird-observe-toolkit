/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.call;

import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.mapping.Rule;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Information used to execute the user request on reflection java. The executor
 * contains information on method to execute. It is used to call method for
 * error, action and filter.
 *
 * @author julien
 */
public class Executor {

    /**
     * Class
     */
    protected Class<? extends WebMotionController> clazz;

    /**
     * Method
     */
    protected Method method;

    /**
     * Instance
     */
    protected WebMotionController instance;

    /**
     * Parameters
     */
    protected Map<String, Object> parameters;

    /**
     * The parameters injected by the injector.
     */
    protected List<String> protectedParameters;

    /**
     * Rule uses to create the Executor
     */
    protected Rule rule;

    public Executor() {
        this.parameters = new LinkedHashMap<>();
        this.protectedParameters = new LinkedList<>();
    }

    public Executor copy() {
        Executor result = new Executor();
        result.setClazz(getClazz());
        result.setMethod(getMethod());
        result.setRule(getRule());
        return result;
    }


    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public WebMotionController getInstance() {
        return instance;
    }

    public List<String> getProtectedParameters() {
        return protectedParameters;
    }

    public void setProtectedParameters(List<String> protectedParameters) {
        this.protectedParameters = protectedParameters;
    }

    public void setInstance(WebMotionController instance) {
        this.instance = instance;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Class<? extends WebMotionController> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends WebMotionController> clazz) {
        this.clazz = clazz;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

}
