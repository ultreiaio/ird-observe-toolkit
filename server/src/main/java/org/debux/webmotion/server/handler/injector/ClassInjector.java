package org.debux.webmotion.server.handler.injector;

/*-
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonParseException;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClassInjector implements ExecutorParametersInjectorHandler.Injector {

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {
        if (!type.equals(Class.class)) {
            return null;
        }
        Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);
        Objects.requireNonNull(parameterTree, String.format("Parameter %s could not be found, need compile with parameters option!", name));
        String className = ((String[]) parameterTree.getValue())[0];
        className = className.replace("\"", "");
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(String.format("Class not found: %s", className), e);
        }
    }
}
