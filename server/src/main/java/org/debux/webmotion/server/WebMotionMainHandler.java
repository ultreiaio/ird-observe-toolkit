/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.call.HttpContext.ErrorData;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.handler.ActionExecuteRenderHandler;
import org.debux.webmotion.server.handler.ActionFinderHandler;
import org.debux.webmotion.server.handler.ActionMethodFinderHandler;
import org.debux.webmotion.server.handler.ErrorFinderHandler;
import org.debux.webmotion.server.handler.ErrorMethodFinderHandler;
import org.debux.webmotion.server.handler.ExecutorInstanceCreatorHandler;
import org.debux.webmotion.server.handler.ExecutorMethodInvokerHandler;
import org.debux.webmotion.server.handler.ExecutorParametersConvertorHandler;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.handler.FilterFinderHandler;
import org.debux.webmotion.server.handler.FilterMethodFinderHandler;
import org.debux.webmotion.server.handler.ParametersExtractorHandler;
import org.debux.webmotion.server.handler.ParametersMultipartHandler;
import org.debux.webmotion.server.mapping.Config;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.mapping.Rule;
import org.debux.webmotion.server.tools.HttpUtils;
import org.debux.webmotion.server.tools.SingletonFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Get chains handlers to process user request when the request is action or
 * error type.
 * <p>
 * It is possible to extends to :
 * <ul>
 * <li>Add new handlers in chains, with extending of getActionHandlers or getErrorHandlers</li>
 * <li>Parameters handlers, with extending of init</li>
 * <li>Modify handler creation, with extending of getHandler</li>
 * </ul>
 *
 * @author julien
 */
public class WebMotionMainHandler implements WebMotionHandler {

    private static final Logger log = LogManager.getLogger(WebMotionMainHandler.class);
    /**
     * All handlers are a singleton
     */
    protected SingletonFactory<WebMotionHandler> factory;


    /**
     * All handlers use to process an action
     */
    protected List<WebMotionHandler> actionHandlers;

    /**
     * All handlers use to process an error
     */
    protected List<WebMotionHandler> errorHandlers;

    /**
     * All handlers use to process an executor
     */
    protected List<WebMotionHandler> executorHandlers;

    @Override
    public void handlerCreated(Mapping mapping, ServerContext context) {
        factory = context.getHandlers();
    }

    @Override
    public void handlerInitialized(Mapping mapping, ServerContext context) {
        initHandlers(mapping, context);
        initExtensions(mapping, context);
    }

    /**
     * Init all handlers to process the request
     *
     * @param mapping mapping
     * @param context context
     */
    protected void initHandlers(Mapping mapping, ServerContext context) {
        List<Class<? extends WebMotionHandler>> actionClasses = getActionHandlers();
        actionHandlers = initHandlers(mapping, context, actionClasses);

        List<Class<? extends WebMotionHandler>> errorClasses = getErrorHandlers();
        errorHandlers = initHandlers(mapping, context, errorClasses);

        List<Class<? extends WebMotionHandler>> executorClasses = getExecutorHandlers();
        executorHandlers = initHandlers(mapping, context, executorClasses);
    }

    /**
     * Chain init methods on the handlers
     *
     * @param mapping mapping
     * @param context context
     * @param classes classes
     * @return list of handlers
     */
    protected List<WebMotionHandler> initHandlers(Mapping mapping, ServerContext context, List<Class<? extends WebMotionHandler>> classes) {
        List<WebMotionHandler> handlers = new ArrayList<>(classes.size());

        for (Class<? extends WebMotionHandler> clazz : classes) {
            WebMotionHandler handler = getHandler(mapping, context, clazz);
            handler.handlerInitialized(mapping, context);
            handlers.add(handler);
        }

        return handlers;
    }

    /**
     * Use to create handlers other WebMotionHandlerFactory
     *
     * @param mapping mapping
     * @param context context
     * @param clazz   clazz
     * @return handler
     */
    public WebMotionHandler getHandler(Mapping mapping, ServerContext context, Class<? extends WebMotionHandler> clazz) {
        WebMotionHandler handler = factory.get(clazz);
        if (handler == null) {
            handler = factory.createInstance(clazz);
            handler.handlerCreated(mapping, context);
        }
        return handler;
    }

    /**
     * Init handler factory for extension
     *
     * @param mapping mapping
     * @param context context
     */
    protected void initExtensions(Mapping mapping, ServerContext context) {
        List<Mapping> extensionsRules = mapping.getExtensionsRules();
        for (Mapping extensionMapping : extensionsRules) {

            Config extensionConfig = extensionMapping.getConfig();
            String className = extensionConfig.getMainHandler();

            WebMotionHandler mainHandler = factory.get(className);
            if (mainHandler == null) {
                mainHandler = factory.createInstance(className);
                mainHandler.handlerCreated(extensionMapping, context);
            }
            mainHandler.handlerInitialized(extensionMapping, context);
        }
    }

    @Override
    public void handlerDestroyed(Mapping mapping, ServerContext context) {
        for (WebMotionHandler handler : actionHandlers) {
            handler.handlerDestroyed(mapping, context);
        }
        for (WebMotionHandler handler : errorHandlers) {
            handler.handlerDestroyed(mapping, context);
        }
        for (WebMotionHandler handler : executorHandlers) {
            handler.handlerDestroyed(mapping, context);
        }

        List<Mapping> extensionsRules = mapping.getExtensionsRules();
        for (Mapping extensionMapping : extensionsRules) {

            Config extensionConfig = extensionMapping.getConfig();
            String className = extensionConfig.getMainHandler();

            WebMotionHandler mainHandler = factory.get(className);
            if (mainHandler != null) {
                mainHandler.handlerDestroyed(extensionMapping, context);
            }
        }

        factory.remove(getClass());
    }

    @Override
    public void handle(Mapping mapping, Call call) {
        handleExtension(mapping, call);

        Rule rule = call.getRule();
        if (rule == null) {
            // Not process in extension
            handleExecutors(mapping, call);
        }
    }

    /**
     * Begin by search in extension if a rule is available.
     *
     * @param mapping TODO
     * @param call    TODO
     */
    protected void handleExtension(Mapping mapping, Call call) {
        HttpContext context = call.getContext();

        // Determine the extension is used
        String url = context.getUrl();
        log.debug("url = " + url);

        List<Mapping> extensionsRules = mapping.getExtensionsRules();
        for (Mapping extensionMapping : extensionsRules) {

            String path = extensionMapping.getExtensionPath();
            log.debug("path = " + path);
            if ("/".equals(path) || HttpUtils.find("^" + path + "(/|$)", url)) {

                context.addExtensionPath(path);

                Config newConfig = extensionMapping.getConfig();
                String className = newConfig.getMainHandler();

                WebMotionHandler mainHandler = factory.get(className);
                mainHandler.handle(extensionMapping, call);

                context.removeExtensionPath(path);

                // Stop if the first handler process the request
                Rule rule = call.getRule();
                if (rule != null) {
                    break;
                }
            }
        }
    }

    /**
     * Next find a rule in current mapping.
     *
     * @param mapping TODO
     * @param call    TODO
     */
    protected void handleExecutors(Mapping mapping, Call call) {
        // Add handlers used during executor invoker
        call.setExecutorHandlers(executorHandlers);

        // Determine if the request contains an errors
        HttpContext context = call.getContext();
        if (context.isError()) {
            ErrorData errorData = context.getErrorData();
            log.error(String.format("Error %d : %s on %s", errorData.getStatusCode(), errorData.getMessage(), errorData.getRequestUri()), errorData.getException());
            chainHandlers(errorHandlers, mapping, call);
        } else {
            chainHandlers(actionHandlers, mapping, call);
        }
    }

    /**
     * Chain handlers
     *
     * @param handlers TODO
     * @param mapping  TODO
     * @param call     TODO
     */
    protected void chainHandlers(List<WebMotionHandler> handlers, Mapping mapping, Call call) {
        for (WebMotionHandler handler : handlers) {
            handler.handle(mapping, call);
        }
    }

    /**
     * @return list of {@link WebMotionHandler} that will be processed for action handling
     */
    public List<Class<? extends WebMotionHandler>> getActionHandlers() {
        return List.of(ParametersMultipartHandler.class,
                       ActionFinderHandler.class,
                       FilterFinderHandler.class,
                       ParametersExtractorHandler.class,
                       ActionExecuteRenderHandler.class,
                       ActionMethodFinderHandler.class,
                       FilterMethodFinderHandler.class,
                       ExecutorMethodInvokerHandler.class
        );
    }

    /**
     * @return list of {@link WebMotionHandler} that will be processed for error handling
     */
    public List<Class<? extends WebMotionHandler>> getErrorHandlers() {
        return List.of(
                ParametersMultipartHandler.class,
                ErrorFinderHandler.class,
                ActionExecuteRenderHandler.class,
                ErrorMethodFinderHandler.class,
                ExecutorMethodInvokerHandler.class
        );
    }

    /**
     * @return list of {@link WebMotionHandler} that will be processed for executor
     */
    public List<Class<? extends WebMotionHandler>> getExecutorHandlers() {
        return List.of(
                ExecutorInstanceCreatorHandler.class,
                ExecutorParametersInjectorHandler.class,
                ExecutorParametersConvertorHandler.class
        );
    }

}
