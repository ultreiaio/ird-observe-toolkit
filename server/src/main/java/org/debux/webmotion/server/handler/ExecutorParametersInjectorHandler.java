/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.handler;

import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.Executor;
import org.debux.webmotion.server.tools.upload.FileProgressListener;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.call.HttpContext.ErrorData;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Config;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.mapping.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The handler injects in executor parameters from the follow type :
 * <ul>
 * <li>Mapping</li>
 * <li>Config</li>
 * <li>Call</li>
 * <li>HttpContext</li>
 * <li>HttpSession</li>
 * <li>HttpServletRequest or ServletRequest</li>
 * <li>HttpServletResponse or ServletResponse</li>
 * <li>ServerContext</li>
 * <li>ServletContext</li>
 * <li>ErrorData</li>
 * <li>Exception</li>
 * <li>FileProgressListener</li>
 * </ul>
 * <p>
 * You can add injector in server context.
 *
 * @author jruchaud
 */
public class ExecutorParametersInjectorHandler extends AbstractHandler implements WebMotionHandler {

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static final Optional<Object> EMPTY = Optional.empty();

    protected List<Injector> injectors;

    @Override
    public void handlerCreated(Mapping mapping, ServerContext context) {
        injectors = context.getInjectors();
        addBasicInjectors(context);
    }

    @Override
    public void handle(Mapping mapping, Call call) {
        Executor executor = call.getCurrent();

        Method executorMethod = executor.getMethod();
        Parameter[] methodParameters = executorMethod.getParameters();

        Map<String, Object> parameters = executor.getParameters();
        List<String> protectedParameters = executor.getProtectedParameters();

        // Search a value with a type
        for (Parameter param : methodParameters) {
            String name = param.getName();
            Class<?> type = param.getType();
            Type generic = param.getParameterizedType();
            Object inject = getValueFromInjectors(mapping, call, name, type, generic);
            boolean foundNullValue = EMPTY.equals(inject);
            if (foundNullValue) {
                inject = null;
                protectedParameters.add(name);
            } else if (inject != null) {
                protectedParameters.add(name);
            }
            parameters.put(name, inject);

        }
    }

    protected Object getValueFromInjectors(Mapping mapping, Call call, String name, Class<?> type, Type generic) {
        for (Injector injector : injectors) {
            Object inject = injector.getValue(mapping, call, name, type, generic);
            if (inject != null) {
                return inject;
            }
        }
        return null;
    }

    /**
     * Use to determine if inject the value in the parameter.
     */
    public interface Injector {

        /**
         * Get the value to inject.
         *
         * @param mapping mapping
         * @param call    call
         * @param name    name
         * @param type    type
         * @param generic of type
         * @return value
         */
        Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic);
    }

    /**
     * Add All basic injectors.
     *
     * @param context incoming context
     */
    protected void addBasicInjectors(ServerContext context) {
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (Mapping.class.isAssignableFrom(type)) {
                        return mapping;
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (Config.class.isAssignableFrom(type)) {
                        return mapping.getConfig();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (Properties.class.isAssignableFrom(type)) {
                        return call.getContext().getServerContext().getMapping().getProperties();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (Call.class.isAssignableFrom(type)) {
                        return call;
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (HttpContext.class.isAssignableFrom(type)) {
                        return call.getContext();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (HttpSession.class.isAssignableFrom(type)) {
                        return call.getContext().getSession();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (ServletRequest.class.isAssignableFrom(type)) {
                        return call.getContext().getRequest();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (ServletResponse.class.isAssignableFrom(type)) {
                        return call.getContext().getResponse();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (ServerContext.class.isAssignableFrom(type)) {
                        return call.getContext().getServerContext();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (ServletContext.class.isAssignableFrom(type)) {
                        return call.getContext().getServletContext();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (ErrorData.class.isAssignableFrom(type)) {
                        return call.getContext().getErrorData();
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    HttpContext context1 = call.getContext();
                    ErrorData errorData = context1.getErrorData();
                    Throwable cause = errorData.getCause();

                    if (type.isInstance(cause)) {
                        return cause;
                    }
                    return null;
                });
        context.addInjector(
                (mapping, call, name, type, generic) -> {
                    if (FileProgressListener.class.isAssignableFrom(type)) {
                        return call.getContext().getSession().getAttribute(FileProgressListener.SESSION_ATTRIBUTE_NAME);
                    }
                    return null;
                });
    }

}
