/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.mapping;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DataConfiguration;
import org.apache.commons.configuration.MapConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.LinkedHashMap;

/**
 * Contains all user configuration. Manage key with value. You can get the value
 * directly in good type. The key "include" add a properties file, it is search
 * in classpath, user home and system configuration.
 *
 * @author julien
 */
public class Properties extends DataConfiguration {

    private static final Logger log = LogManager.getLogger(Properties.class);

    public Properties() {
        super(new CompositeConfiguration());
    }

    public void addItem(PropertiesItem item) {
        ((CompositeConfiguration) configuration).addConfiguration(item);
    }

    public void addProperties(Properties properties) {
        ((CompositeConfiguration) configuration).addConfiguration(properties);
    }

    public static class PropertiesItem extends CompositeConfiguration {
        protected String name;

        public PropertiesItem(String name) {
            this.name = name;
            includeConfiguration(name + ".properties");

            LinkedHashMap<String, Object> map = new LinkedHashMap<>();
            MapConfiguration mapConfiguration = new MapConfiguration(map);
            addConfiguration(mapConfiguration, true);
        }

        @Override
        public void addPropertyDirect(String key, Object value) {
            if ("include".equals(key)) {
                String fileName = (String) value;
                includeConfiguration(fileName);
            } else {
                super.addPropertyDirect(key, value);
            }
        }

        protected void includeConfiguration(String fileName) {
            PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
            addConfiguration(propertiesConfiguration);

            try {
//                // Search in system configuration path
//                String systemConfigurationPath = ReflectionUtils.getSystemConfigurationPath();
//                String fileNameSystem = systemConfigurationPath + File.separator + fileName;
//                File fileSystem = new File(fileNameSystem);
//                if (fileSystem.exists() && fileSystem.canRead()) {
//                    propertiesConfiguration.load(fileSystem);
//                }

//                // Search in user configuration path
//                String userConfigurationPath = ReflectionUtils.getUserConfigurationPath();
//                String fileNameUser = userConfigurationPath + File.separator + fileName;
//                File fileUser = new File(fileNameUser);
//                if (fileUser.exists() && fileUser.canRead()) {
//                    propertiesConfiguration.load(fileUser);
//                }

                // Search in classpath
                ClassLoader classLoader = Properties.class.getClassLoader();
                URL url = classLoader.getResource(fileName);
                if (url != null) {
                    propertiesConfiguration.load(url);
                }

            } catch (ConfigurationException ce) {
                log.error("Error during read properties file " + fileName, ce);
            }
        }
    }
}
