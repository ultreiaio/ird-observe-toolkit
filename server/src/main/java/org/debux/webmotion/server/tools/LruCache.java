/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.tools;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Basic implementation LRU cache.
 * @param <K> key type
 * @param <V> value type
 */
public class LruCache<K, V> extends LinkedHashMap<K, V> {
    /** Max key in cache */
    protected int max;

    public LruCache(int maxEntries) {
        super(maxEntries + 1, 1.0f, true);
        this.max = maxEntries;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return super.size() > max;
    }
    
}
