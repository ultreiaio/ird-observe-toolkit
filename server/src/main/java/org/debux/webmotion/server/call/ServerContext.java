/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.call;

import io.ultreia.java4all.lang.Objects2;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.fileupload2.core.DiskFileItemFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.WebMotionException;
import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler.Injector;
import org.debux.webmotion.server.mapping.Config;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.mapping.Rule;
import org.debux.webmotion.server.parser.DefaultMappingParser;
import org.debux.webmotion.server.parser.MappingChecker;
import org.debux.webmotion.server.parser.MappingParser;
import org.debux.webmotion.server.tools.HttpUtils;
import org.debux.webmotion.server.tools.ReflectionUtils;
import org.debux.webmotion.server.tools.SingletonFactory;

import javax.servlet.ServletContext;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * WebMotionServerContext contains all global information like factories, mbeans, ...
 * The user can store the own attributes. The server context is an attributes of ServletContext.
 *
 * @author julien
 */
@SuppressWarnings("unused")
public class ServerContext {

    /* ServerContext name to store attributes */
    public static final String ATTRIBUTE_SERVER_CONTEXT = "wm.server.context";

    /**
     * Factory of controllers
     */
    protected SingletonFactory<WebMotionController> controllers;

    /**
     * Factory of handlers
     */
    protected SingletonFactory<WebMotionHandler> handlers;
    /**
     * Contain injector use in ExecutorParametersInjectorHandler
     */
    protected List<Injector> injectors;

    /**
     * Bean utils use in handler
     */
    protected BeanUtilsBean beanUtil;

    /**
     * Convert utils use in handler
     */
    protected ConvertUtilsBean converter;

    /**
     * Current mapping
     */
    protected Mapping mapping;

    /**
     * The main handler is call the first
     */
    protected WebMotionHandler mainHandler;

    /**
     * User attributes
     */
    protected Map<String, Object> attributes;

    /**
     * Current servlet context
     */
    protected ServletContext servletContext;

    /**
     * Secret key use in encrypt cookie value
     */
    protected String secret;

    /**
     * Main mapping file name to parse
     */
    protected String[] mappingFileNames = {"/mapping"};

    /**
     * Parsers for mapping file
     */
    protected String[] mappingParsers = {DefaultMappingParser.class.getName()};

    /**
     * Absolute path on webapp
     */
    protected String webappPath;

    /**
     * Listeners on server
     */
    protected List<WebMotionServerListener> listeners;

    /**
     * Current exclude paths
     */
    protected String[] excludePaths = {};
    /**
     * Temporary path used
     */
    protected Path temporaryPath;
    /**
     * Use to read multi-parts.
     *
     * @see org.debux.webmotion.server.handler.ParametersMultipartHandler
     */
    protected DiskFileItemFactory fileItemFactory;

    /**
     * Initialize the context.
     *
     * @param servletContext servlet context
     */
    public void contextInitialized(ServletContext servletContext) {
        this.servletContext = servletContext;
        this.attributes = new HashMap<>();
        this.handlers = new SingletonFactory<>();
        this.controllers = new SingletonFactory<>();
        this.injectors = new ArrayList<>();
        this.beanUtil = BeanUtilsBean.getInstance();
        this.converter = beanUtil.getConvertUtils();

        this.webappPath = servletContext.getRealPath("/");

        // Read the mapping in the current project
        MappingParser[] parsers = getMappingParsers();
        for (MappingParser parser : parsers) {
            mapping = parser.parse(mappingFileNames);
            if (mapping != null) {
                break;
            }
        }

        if (mapping == null) {
            throw new WebMotionException(String.format("No mapping found for %s in %s", Arrays.toString(mappingFileNames), Arrays.toString(mappingParsers)));
        }

        // Fire onStart
        listeners = new ArrayList<>();
        onStartServerListener(mapping);

        // Load mapping
        loadMapping();

        // Check mapping
        checkMapping();
    }

    /**
     * Destroy the context.
     */
    public void contextDestroyed() {
        if (mainHandler != null) {
            mainHandler.handlerDestroyed(mapping, this);
        }
        // Fire onStop
        if (listeners != null) {
            for (WebMotionServerListener listener : listeners) {
                listener.onStop(this);
            }
        }
    }

    /**
     * Load the mapping
     */
    public void loadMapping() {
        // Create the handler factory
        Config config = mapping.getConfig();
        String className = config.getMainHandler();

        mainHandler = handlers.getInstance(className);

        // Read secret otherwise generate it
        secret = config.getSecret();
        if (secret == null) {
            secret = HttpUtils.generateSecret();
        } else if (secret.length() < Config.SERVER_SECRET_MIN_SIZE) {
            throw new IllegalStateException("The secret key is too short, it is generated");
        }

        // Init handlers
        mainHandler.handlerCreated(mapping, this);
        mainHandler.handlerInitialized(mapping, this);
    }

    /**
     * Search in mapping all server listeners and fire onStart
     *
     * @param mapping mapping
     */
    protected void onStartServerListener(Mapping mapping) {
        Config config = mapping.getConfig();
        String serverListenerClassNames = config.getServerListener();
        if (serverListenerClassNames != null && !serverListenerClassNames.isEmpty()) {
            List<Mapping> extensions = mapping.getExtensionsRules();
            // Split name by comma
            String[] serverListenerClassName = serverListenerClassNames.split("\\s*,\\s*");
            for (String className : serverListenerClassName) {

                // Create an instance
                try {
                    Class<WebMotionServerListener> serverListenerClass = Objects2.forName(className);
                    WebMotionServerListener serverListener = Objects2.newInstance(serverListenerClass);
                    serverListener.onStart(mapping, this);
                    for (Mapping extensionMapping : extensions) {
                        serverListener.onStart(extensionMapping, this);
                    }
                    listeners.add(serverListener);
                } catch (Exception iae) {
                    throw new WebMotionException("Error during create server listener " + className, iae);
                }
            }
        }
    }

    /**
     * Check the mapping and extensions
     */
    public void checkMapping() {
        MappingChecker mappingChecker = new MappingChecker();
        mappingChecker.checkMapping(this, mapping);
        mappingChecker.print();
    }

    /**
     * @return the instance of mapping parsers
     */
    protected MappingParser[] getMappingParsers() {
        MappingParser[] parsers = new MappingParser[mappingParsers.length];
        int index = 0;
        for (String className : mappingParsers) {
            Class<MappingParser> aClass = Objects2.forName(className);
            MappingParser parser = Objects2.newInstance(aClass);
            parsers[index++] = parser;
        }
        return parsers;
    }

    /**
     * Set the parsers for the mapping file.
     *
     * @param parsers class names
     */
    public void setMappingParsers(String[] parsers) {
        this.mappingParsers = parsers;
    }

    /**
     * @return factory contains all controllers
     */
    public SingletonFactory<WebMotionController> getControllers() {
        return controllers;
    }

    /**
     * @return factory contains all handlers
     */
    public SingletonFactory<WebMotionHandler> getHandlers() {
        return handlers;
    }

    /**
     * @return all injectors register
     */
    public List<Injector> getInjectors() {
        return injectors;
    }

    /**
     * Set all injectors.
     *
     * @param injectors TODO
     */
    public void setInjectors(List<Injector> injectors) {
        this.injectors = injectors;
    }

    /**
     * Add a injector.
     *
     * @param injector TODO
     */
    public void addInjector(Injector injector) {
        injectors.add(injector);
    }

    /**
     * @return bean utils
     */
    public BeanUtilsBean getBeanUtil() {
        return beanUtil;
    }

    /**
     * Set bean utils use for conversion.
     *
     * @param beanUtil TODO
     */
    public void setBeanUtil(BeanUtilsBean beanUtil) {
        this.beanUtil = beanUtil;
    }

    /**
     * @return converter utils
     */
    public ConvertUtilsBean getConverter() {
        return converter;
    }

    /**
     * Set converter utils.
     *
     * @param converter TODO
     */
    public void setConverter(ConvertUtilsBean converter) {
        this.converter = converter;
    }

    /**
     * Add a converter.
     *
     * @param converter TODO
     * @param clazz     TODO
     */
    public void addConverter(Converter converter, Class<?> clazz) {
        this.converter.register(converter, clazz);
    }

    /**
     * @return attributes store in server context
     */
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    /**
     * Add an attribute in server context.
     *
     * @param name  TODO
     * @param value TODO
     */
    public void setAttribute(String name, Object value) {
        attributes.put(name, value);
    }

    /**
     * Get attribute by name.
     *
     * @param name attribute name
     * @return attribute value
     */
    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    /**
     * @return the main handler instance configure in the mapping
     */
    public WebMotionHandler getMainHandler() {
        return mainHandler;
    }

    /**
     * @return the root mapping
     */
    public Mapping getMapping() {
        return mapping;
    }

    /**
     * @return servlet context
     */
    public ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * @return secret use for security
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set the secret manually.
     *
     * @param secret TODO
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * @return the mapping file name use to read the mapping.
     */
    public String[] getMappingFileNames() {
        return mappingFileNames;
    }

    /**
     * Set the mapping file name use to read the mapping.
     *
     * @param mappingFileNames TODO
     */
    public void setMappingFileNames(String[] mappingFileNames) {
        this.mappingFileNames = mappingFileNames;
    }

    /**
     * @return absolute path on webapp
     */
    public String getWebappPath() {
        return webappPath;
    }

    /**
     * @return current path exclude in filter
     */
    public String[] getExcludePaths() {
        return excludePaths;
    }

    /**
     * Set exclude path in filter
     *
     * @param excludePaths TODO
     */
    public void setExcludePaths(String[] excludePaths) {
        this.excludePaths = excludePaths;
    }

    public Path getTemporaryPath() {
        return temporaryPath;
    }

    public void setTemporaryPath(Path temporaryPath) {
        this.temporaryPath = temporaryPath;
    }

    public synchronized DiskFileItemFactory getFileItemFactory() {
        if (fileItemFactory == null) {
            if (getTemporaryPath() == null) {
                throw new IllegalStateException("No temporary path set in the ServerContext.");
            }
            fileItemFactory = DiskFileItemFactory.builder().setBufferSize(1024 * 1024 /* 1Mb */).setPath(getTemporaryPath()).get();
        }
        return fileItemFactory;
    }

    private static final Logger log = LogManager.getLogger(ServerContext.class);

    public void loadExecutors(Mapping mapping, Stream<? extends Rule> actionStream) {
        actionStream.filter(r -> r.getAction().isAction()).forEach(rule -> {
            String fullName = rule.getAction().getFullName();
            String className = rule.getAction().getClassName();
            Class<? extends WebMotionController> clazz = Objects2.forName(className);
            String methodName = rule.getAction().getMethodName();
            Method method = ReflectionUtils.getMethod(clazz, methodName);
            Executor executor = new Executor();
            executor.setClazz(clazz);
            executor.setMethod(method);
            executor.setRule(rule);
            rule.getAction().setExecutorCache(executor);
            log.debug(String.format("Adding executor cache to %s", fullName));
        });
    }

}
