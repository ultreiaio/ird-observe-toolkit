/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.tools;

import io.ultreia.java4all.lang.Objects2;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple singleton factory, maybe that two threads creates the instance, but it is not a problem.
 */
public class SingletonFactory<T> {
    protected Map<Class<? extends T>, T> singletons;

    public SingletonFactory() {
        singletons = new HashMap<>();
    }

    public T get(Class<? extends T> clazz) {
        return singletons.get(clazz);
    }

    public T get(String clazzName) {
        Class<T> clazz = Objects2.forName(clazzName);
        return get(clazz);
    }

    public T remove(Class<? extends T> clazz) {
        return singletons.remove(clazz);
    }

    public T remove(String clazzName) {
        Class<T> clazz = Objects2.forName(clazzName);
        return singletons.remove(clazz);
    }

    public T createInstance(String clazzName) {
        Class<T> clazz = Objects2.forName(clazzName);
        return createInstance(clazz);
    }

    public T createInstance(Class<? extends T> clazz) {
        T instance = Objects2.newInstance(clazz);
        singletons.put(clazz, instance);
        return instance;
    }

    public T getInstance(String clazzName) {
        Class<T> clazz = Objects2.forName(clazzName);
        return getInstance(clazz);
    }

    public T getInstance(Class<? extends T> clazz) {
        T instance = get(clazz);
        if (instance == null) {
            instance = createInstance(clazz);
        }
        return instance;
    }

}
