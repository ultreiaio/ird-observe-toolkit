/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.render;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Config;
import org.debux.webmotion.server.mapping.Mapping;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * The render represents result for user request. It contains the necessary
 * elements to create the http response. In the {@code WebMotionAction} class, there are
 * utility methods to instantiated correctly a render. The render can be a
 * view (template), an URL (referer), an action, a model (json, jsonp, xml) or
 * directly a content with specified mime-type.
 *
 * @author julien
 */
public abstract class Render {

    private static final Logger log = LogManager.getLogger(Render.class);

    /**
     * Default encoding uses to write the response
     */
    public static String DEFAULT_ENCODING = "UTF-8";

    /**
     * Default model name uses when neither name is given
     */
    public static String DEFAULT_MODEL_NAME = "model";

    /**
     * Execute the render and store a state as executed.
     *
     * @param mapping incoming mapping
     * @param call    incoming call
     * @throws IOException      TODO
     * @throws ServletException TODO
     */
    public void exec(Mapping mapping, Call call) throws IOException, ServletException {
        create(mapping, call);
        complete(mapping, call);
    }

    /**
     * Call after the render is create. If the request is async, call complete method
     * on AsyncContext.
     *
     * @param mapping incoming mapping
     * @param call    incoming call
     */
    public void complete(Mapping mapping, Call call) {
        if (call.isAsync()) {
            HttpContext context = call.getContext();
            HttpServletRequest request = context.getRequest();
            AsyncContext asyncContext = request.getAsyncContext();
            asyncContext.complete();
        }
    }

    /**
     * Use to do the render.
     *
     * @param mapping incoming mapping
     * @param call    incoming call
     * @throws IOException      TODO
     * @throws ServletException TODO
     */
    public abstract void create(Mapping mapping, Call call) throws IOException, ServletException;

    /**
     * Get the url to the view.
     *
     * @param mapping incoming mapping
     * @param call    incoming call
     * @param view    incoming view
     * @return view path
     */
    protected String getViewPath(Mapping mapping, Call call, String view) {
        String path = "/" + view;
        Config config = mapping.getConfig();
        String packageName = config.getPackageViews().replaceAll("\\.", "/");
        if (!packageName.isEmpty()) {
            path = "/" + packageName + path;
        }

        log.debug("path = " + path);
        return path;
    }

    /**
     * Add model in attribute
     *
     * @param call  incoming call
     * @param model incoming model
     */
    protected void addModel(Call call, Map<String, Object> model) {
        HttpContext context = call.getContext();
        HttpServletRequest request = context.getRequest();

        if (model != null) {
            for (Map.Entry<String, Object> entry : model.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                request.setAttribute(key, value);
            }
        }
    }

    /**
     * Add model in parameter
     *
     * @param url   incoming url
     * @param model incoming model
     * @return result url
     */
    protected String addModel(String url, Map<String, Object> model) {
        StringBuilder path = new StringBuilder(url);
        if (model != null) {
            String separator = "?";
            if (path.toString().contains("?")) {
                separator = "&";
            }
            for (Map.Entry<String, Object> entry : model.entrySet()) {
                Object value = entry.getValue();
                if (value != null) {
                    String key = entry.getKey();
                    path.append(separator).append(key).append("=").append(value);
                    separator = "&";
                }
            }
        }
        return path.toString();
    }

}
