/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.handler;

import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.Executor;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Action;
import org.debux.webmotion.server.mapping.FilterRule;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.mapping.Rule;

import java.util.List;

/**
 * Find the filter class represent by name given in mapping.
 *
 * @author julien
 */
public class FilterMethodFinderHandler extends AbstractHandler implements WebMotionHandler {

    @Override
    public void handlerInitialized(Mapping mapping, ServerContext context) {
        context.loadExecutors(mapping, mapping.getFilterRules().stream());
    }

    @Override
    public void handle(Mapping mapping, Call call) {
        Rule rule = call.getRule();
        if (rule != null) {
            List<Executor> filters = call.getFilters();
            for (FilterRule filterRule : call.getFilterRules()) {
                Action action = filterRule.getAction();
                Executor executor = action.newExecutor();
                filters.add(executor);
            }
        }
    }
}
