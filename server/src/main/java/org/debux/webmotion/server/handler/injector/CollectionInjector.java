package org.debux.webmotion.server.handler.injector;

/*-
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.debux.webmotion.server.call.Call;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * To inject any collection using gson object.
 */
@SuppressWarnings("rawtypes")
public class CollectionInjector extends JsonInjector<Collection> {

    public CollectionInjector(Gson gson) {
        super(Collection.class, gson);
    }

    @Override
    protected Collection buildValue(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree) {
        boolean onString = ((ParameterizedType) generic).getActualTypeArguments()[0].equals(String.class);
        Collection<Object> result;
        if (Set.class.isAssignableFrom(type)) {
            result = new LinkedHashSet<>();
        } else {
            result = new LinkedList<>();
        }
        String[] values = (String[]) parameterTree.getValue();
        String value = values[0];
        if (!value.isEmpty()) {
            if (onString) {
                if (!value.contains("[")) {
                    // use values
                    Collections.addAll(result, values);
                } else {
                    // use only first value which was encoded as an array of string
                    Collection<String> o = gson.fromJson(value, generic);
                    if (o != null) {
                        result.addAll(o);
                    }
                }
            } else {
                Collection<?> o = gson.fromJson(value, generic);
                if (o != null) {
                    result.addAll(o);
                }
            }
        }
        return result;
    }
}
