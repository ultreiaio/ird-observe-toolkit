/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.tools.upload;

import org.apache.commons.fileupload2.core.ProgressListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The listener is created for multi-part form to send the progress. It is
 * stored in session.
 *
 * @author julien
 */
public class FileProgressListener implements ProgressListener {

    private static final Logger log = LogManager.getLogger(FileProgressListener.class);
    /**
     * Attribute name where the listener is stored
     */
    public static String SESSION_ATTRIBUTE_NAME = "fileProgressListener";

    /**
     * Current bytes read in request
     */
    protected long bytesRead;

    /**
     * Total file size
     */
    protected long contentLength;

    /**
     * Number of item read
     */
    protected int items;

    @Override
    public void update(long bytesRead, long contentLength, int items) {
        this.bytesRead = bytesRead;
        this.contentLength = contentLength;
        this.items = items;

        log.debug("FileProgressListener : "
                          + bytesRead + " bytesRead, "
                          + contentLength + " contentLength, "
                          + items + " items");
    }

    public long getBytesRead() {
        return bytesRead;
    }

    public long getContentLength() {
        return contentLength;
    }

    public int getItems() {
        return items;
    }

}
