/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server;

import org.debux.webmotion.server.call.ServerContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Listener use to initialized the WebMotion ServerContext.
 * 
 * @author julien
 */
public class WebMotionServletContextListener implements ServletContextListener {
    
    /** Current application context */
    protected ServerContext serverContext;
        
    /** Filter parameter to configure mapping file name by default is mapping */
    protected final static String PARAM_MAPPING_FILE_NAME = "wm.mapping.file.name";
            
    /** Filter parameter to configure parsers for mapping file name by default is only @DefaultMappingParser */
    protected final static String PARAM_MAPPING_PARSERS = "wm.mapping.parsers";
            
    /** Filter parameter to configure excludes path for WebMotion. The value is separated by comma */
    protected final static String PARAM_EXCLUDE_PATHS = "wm.exclude.paths";
            
    @Override
    public void contextInitialized(ServletContextEvent event) {
        serverContext = new ServerContext();
        ServletContext servletContext = event.getServletContext();
        servletContext.setAttribute(ServerContext.ATTRIBUTE_SERVER_CONTEXT, serverContext);
        
        // Get file name mapping in context param
        String mappingFileNameParam = servletContext.getInitParameter(PARAM_MAPPING_FILE_NAME);
        if (mappingFileNameParam != null && !mappingFileNameParam.isEmpty()) {
            serverContext.setMappingFileNames(mappingFileNameParam.split(","));
        }
        
        // Get file name for parsers in context param
        String parsersParam = servletContext.getInitParameter(PARAM_MAPPING_PARSERS);
        if (parsersParam != null && !parsersParam.isEmpty()) {
            serverContext.setMappingParsers(parsersParam.split(","));
        }
        
        // Get exclude path in context param
        String excludePathsParam = servletContext.getInitParameter(PARAM_EXCLUDE_PATHS);
        if (excludePathsParam != null && !excludePathsParam.isEmpty()) {
            serverContext.setExcludePaths(excludePathsParam.split(","));
        }

        serverContext.contextInitialized(servletContext);
    }

    
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        serverContext.contextDestroyed();
    }
    
}
