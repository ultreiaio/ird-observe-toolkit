/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.handler;

import io.ultreia.java4all.lang.Strings;
import org.apache.commons.fileupload2.core.DiskFileItem;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.WebMotionException;
import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.call.UploadFile;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.tools.upload.FileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * Extract parameter in request, when it is a multipart request. Use apache
 * FileUpload to process.
 *
 * @author julien
 */
public class ParametersMultipartHandler extends AbstractHandler implements WebMotionHandler {

    private static final Logger log = LogManager.getLogger(ParametersMultipartHandler.class);

    @Override
    public void handle(Mapping mapping, Call call) {
        HttpContext context = call.getContext();
        HttpServletRequest request = context.getRequest();

        Map<String, Object> extractParameters = call.getExtractParameters();

        boolean isMultipart = FileUpload.isMultipartContent(request);
        if (isMultipart) {
            FileUpload upload = new FileUpload(context.getServerContext().getFileItemFactory());

//            HttpSession session = request.getSession();
//            if (session != null) {
//                FileProgressListener listener = new FileProgressListener();
//                upload.setProgressListener(listener);
//                session.setAttribute(FileProgressListener.SESSION_ATTRIBUTE_NAME, listener);
//                call.setFileUploadRequest(true);
//            }

            try {
                List<DiskFileItem> items = upload.parseRequest(request);
                for (DiskFileItem item : items) {
                    String fieldName = item.getFieldName();

                    log.debug("Multipart: {} - size: {} - in memory? {} - location: {}", fieldName, Strings.convertMemory(item.getSize()), item.isInMemory(), item.getPath());
                    if (item.isFormField()) {
                        String fieldValue;
                        try {
                            fieldValue = item.getString(StandardCharsets.UTF_8);
                        } catch (UnsupportedEncodingException e) {
                            fieldValue = item.getString();
                        }

                        String[] values = (String[]) extractParameters.get(fieldName);
                        if (values == null) {
                            values = new String[]{fieldValue};
                            extractParameters.put(fieldName, values);

                        } else {
                            values = ArrayUtils.add(values, fieldValue);
                            extractParameters.put(fieldName, values);
                        }

                    } else {
                        UploadFile uploadFile = getUploadFile(item);

                        extractParameters.put(fieldName, uploadFile);
                    }
                }
            } catch (IOException fue) {
                throw new WebMotionException("Error during upload file on server", fue);
            }

        } else {
            Map<String, String[]> parameters = context.getParameters();
            extractParameters.putAll(parameters);
        }
    }

    private static UploadFile getUploadFile(DiskFileItem item) {
        UploadFile uploadFile = new UploadFile();

        File file = item.getPath().toFile();
        uploadFile.setFile(file);

        String fileName = item.getName();
        uploadFile.setName(fileName);

        long fileSize = item.getSize();
        uploadFile.setSize(fileSize);

        String fileType = item.getContentType();
        uploadFile.setContentType(fileType);
        return uploadFile;
    }
}
