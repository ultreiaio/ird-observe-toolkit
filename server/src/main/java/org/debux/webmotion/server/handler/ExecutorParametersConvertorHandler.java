/*
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.debux.webmotion.server.handler;

import io.ultreia.java4all.lang.Objects2;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.debux.webmotion.server.WebMotionException;
import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.Call.ParameterTree;
import org.debux.webmotion.server.call.Executor;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.call.UploadFile;
import org.debux.webmotion.server.mapping.Mapping;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Store in the call object, all parameters converted depending action method
 * invoked. Use apache ConvertUtilsBean to process.
 * <p>
 * All following conversion is possible :
 * <ul>
 * <li>java.lang.BigDecimal (no default value)</li>
 * <li>java.lang.BigInteger (no default value)</li>
 * <li>boolean and java.lang.Boolean (default to false)</li>
 * <li>byte and java.lang.Byte (default to zero)</li>
 * <li>char and java.lang.Character (default to a space)</li>
 * <li>java.lang.Class (no default value)</li>
 * <li>double and java.lang.Double (default to zero)</li>
 * <li>float and java.lang.Float (default to zero)</li>
 * <li>int and java.lang.Integer (default to zero)</li>
 * <li>long and java.lang.Long (default to zero)</li>
 * <li>short and java.lang.Short (default to zero)</li>
 * <li>java.lang.String (default to null)</li>
 * <li>java.io.File (no default value)</li>
 * <li>java.net.URL (no default value)</li>
 * <li>java.sql.Date (no default value) (string format [yyyy-MM-dd])</li>
 * <li>java.sql.Time (no default value) (string format [HH:mm:ss])</li>
 * <li>java.sql.Timestamp (no default value) (string format [yyyy-MM-dd HH:mm:ss.fffffffff])</li>
 * <li>POJO (no default value)</li>
 * <li>java.util.Map (no default value)</li>
 * <li>java.util.Set (no default value)</li>
 * <li>Arrays (no default value)</li>
 * </ul>
 * <p>
 * You can add injector in server context.
 *
 * @author julien
 */
public class ExecutorParametersConvertorHandler extends AbstractHandler implements WebMotionHandler {

    protected BeanUtilsBean beanUtil;
    protected ConvertUtilsBean converter;
    protected PropertyUtilsBean propertyUtils;

    @Override
    public void handlerCreated(Mapping mapping, ServerContext context) {
        beanUtil = context.getBeanUtil();
        converter = context.getConverter();
        propertyUtils = beanUtil.getPropertyUtils();
    }

    @Override
    public void handle(Mapping mapping, Call call) {
        Executor executor = call.getCurrent();

        Method executorMethod = executor.getMethod();
        List<String> protectedParameters = executor.getProtectedParameters();
        // Save object in call
        Map<String, Object> convertedParameters = executor.getParameters();

        Call.ParameterTree parameterTree = call.getParameterTree();
        Map<String, List<Call.ParameterTree>> parameterArray = parameterTree.getArray();
        Map<String, Call.ParameterTree> parameterObject = parameterTree.getObject();

        for (Parameter param : executorMethod.getParameters()) {
            String name = param.getName();
            Class<?> type = param.getType();
            Type genericType = param.getParameterizedType();
            if (protectedParameters.contains(name)) {
                continue;
            }
            boolean done = false;
            try {
                if (parameterArray != null) {
                    List<Call.ParameterTree> array = parameterArray.get(name);
                    if (array != null) {
                        Object value = convert(array, type, genericType);
                        convertedParameters.put(name, value);
                        done = true;
                    }
                }
                if (!done && parameterObject != null) {
                    Call.ParameterTree object = parameterObject.get(name);
                    if (object == null
                            && !Collection.class.isAssignableFrom(type)
                            && !Map.class.isAssignableFrom(type)
                            && !UploadFile.class.isAssignableFrom(type)
                            && !File.class.isAssignableFrom(type)
                            && !type.isArray()
                            && converter.lookup(type) == null) {
                        object = parameterTree;
                    }
                    if (object != null) {
                        Object value = convert(object, type, genericType);
                        convertedParameters.put(name, value);
                        done = true;
                    }
                }
                if (!done) {
                    convertedParameters.put(name, null);
                }
            } catch (WebMotionException e) {
                throw e;
            } catch (Exception e) {
                throw new WebMotionException(String.format("Error during converting parameter %s before invoke the method", name), e);
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected Object convert(List<ParameterTree> parameterTrees, Class<?> type, Type genericType) throws Exception {
        Object result = null;

        if (type.isArray()) {
            Class<?> componentType = type.getComponentType();

            Object[] tabConverted = (Object[]) Array.newInstance(componentType, parameterTrees.size());

            int index = 0;
            for (ParameterTree parameterTree : parameterTrees) {
                Object objectConverted = convert(parameterTree, componentType, null);
                tabConverted[index] = objectConverted;
                index++;
            }

            result = tabConverted;

        } else if (Collection.class.isAssignableFrom(type)) {

            Collection<Object> instance;
            if (type.isInterface()) {
                if (List.class.isAssignableFrom(type)) {
                    instance = new ArrayList<>();

                } else if (Set.class.isAssignableFrom(type)) {
                    instance = new HashSet<>();

                } else if (SortedSet.class.isAssignableFrom(type)) {
                    instance = new TreeSet<>();

                } else {
                    instance = new ArrayList<>();
                }
            } else {
                instance = (Collection) Objects2.newInstance(type);
            }

            Class<?> convertType = String.class;
            if (genericType instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericType;
                convertType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            }

            for (ParameterTree parameterTree : parameterTrees) {
                Object converted = convert(parameterTree, convertType, null);
                instance.add(converted);
            }

            result = instance;
        }

        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected Object convert(ParameterTree parameterTree, Class<?> type, Type genericType) throws Exception {
        Object result = null;

        if (parameterTree == null) {
            return null;
        }

        if (genericType == null) {
            genericType = type.getGenericSuperclass();
        }

        Map<String, List<ParameterTree>> parameterArray = parameterTree.getArray();
        Map<String, ParameterTree> parameterObject = parameterTree.getObject();
        Object value = parameterTree.getValue();

        Converter lookup = converter.lookup(type);
        if (lookup != null) {

            // converter found, use it
            result = lookup.convert(type, value);
            return result;
        }

        // Manage enums
        if (type.isEnum()) {
            Object name = value == null ? null : ((Object[]) value)[0];
            if (name != null) {
                result = Enum.valueOf((Class) type, name.toString());
            }

            // Manage collection
        } else if (Collection.class.isAssignableFrom(type)) {

            Collection<Object> instance;
            if (type.isInterface()) {
                if (List.class.isAssignableFrom(type)) {
                    instance = new ArrayList<>();

                } else if (Set.class.isAssignableFrom(type)) {
                    instance = new HashSet<>();

                } else if (SortedSet.class.isAssignableFrom(type)) {
                    instance = new TreeSet<>();

                } else {
                    instance = new ArrayList<>();
                }
            } else {
                instance = (Collection) Objects2.newInstance(type);
            }

            Class<?> convertType = String.class;
            if (genericType instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericType;
                convertType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            }

            if (parameterObject != null) {
                for (Map.Entry<String, ParameterTree> entry : parameterObject.entrySet()) {
                    ParameterTree object = entry.getValue();
                    Object converted = convert(object, convertType, null);
                    instance.add(converted);
                }
            } else {
                Object[] tab = (Object[]) value;
                for (Object object : tab) {
                    Object converted = converter.convert(object, convertType);
                    instance.add(converted);
                }
            }

            result = instance;

            // Manage map
        } else if (Map.class.isAssignableFrom(type)) {
            Map<Object, Object> instance;
            if (type.isInterface()) {
                if (SortedMap.class.isAssignableFrom(type)) {
                    instance = new TreeMap<>();

                } else {
                    instance = new HashMap<>();
                }
            } else {
                instance = (Map) Objects2.newInstance(type);
            }

            Class<?> convertKeyType = String.class;
            Class<?> convertValueType = String.class;
            if (genericType instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericType;
                convertKeyType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                convertValueType = (Class<?>) parameterizedType.getActualTypeArguments()[1];
            }

            for (Map.Entry<String, ParameterTree> entry : parameterObject.entrySet()) {
                String mapKey = entry.getKey();
                ParameterTree mapValue = entry.getValue();

                Object convertedKey = converter.convert(mapKey, convertKeyType);
                Object convertedValue = convert(mapValue, convertValueType, null);

                instance.put(convertedKey, convertedValue);
            }

            result = instance;

            // Manage simple object
        } else if (type.isArray()) {
            Class<?> componentType = type.getComponentType();

            if (parameterObject != null) {
                Object[] tabConverted = (Object[]) Array.newInstance(componentType, parameterObject.size());
                result = tabConverted;

                int index = 0;
                for (Map.Entry<String, ParameterTree> entry : parameterObject.entrySet()) {
                    ParameterTree object = entry.getValue();
                    Object objectConverted = convert(object, componentType, null);
                    tabConverted[index] = objectConverted;
                    index++;
                }

            } else {
                Object[] tab = (Object[]) value;
                Object[] tabConverted = (Object[]) Array.newInstance(componentType, tab.length);
                result = tabConverted;

                for (int index = 0; index < tab.length; index++) {
                    Object object = tab[index];
                    Object objectConverted = converter.convert(object, componentType);
                    tabConverted[index] = objectConverted;
                }
            }

        } else if (value instanceof UploadFile) {
            if (File.class.isAssignableFrom(type)) {
                UploadFile uploadFile = (UploadFile) value;
                result = uploadFile.getFile();
            } else {
                result = value;
            }

            // Manage simple object
        } else {
            Object instance = Objects2.newInstance(type);
            boolean one = false;

            if (parameterObject != null) {
                for (Map.Entry<String, ParameterTree> attributeEntry : parameterObject.entrySet()) {
                    String attributeName = attributeEntry.getKey();
                    ParameterTree attributeValue = attributeEntry.getValue();

                    boolean writeable = propertyUtils.isWriteable(instance, attributeName);
                    if (writeable) {
                        one = true;

                        Field field = FieldUtils.getField(type, attributeName, true);
                        Class<?> attributeType = field.getType();

                        genericType = field.getGenericType();
                        Object attributeConverted = convert(attributeValue, attributeType, genericType);
                        beanUtil.setProperty(instance, attributeName, attributeConverted);
                    }
                }
            }

            if (parameterArray != null) {
                for (Map.Entry<String, List<ParameterTree>> entry : parameterArray.entrySet()) {
                    String attributeName = entry.getKey();
                    List<ParameterTree> attributeValues = entry.getValue();

                    boolean writeable = propertyUtils.isWriteable(instance, attributeName);
                    if (writeable) {
                        one = true;

                        Field field = FieldUtils.getField(type, attributeName, true);
                        Class<?> attributeType = field.getType();

                        genericType = field.getGenericType();
                        Object attributeConverted = convert(attributeValues, attributeType, genericType);
                        beanUtil.setProperty(instance, attributeName, attributeConverted);
                    }
                }
            }
            if (one) {
                result = instance;
            }
        }
        return result;
    }

}
