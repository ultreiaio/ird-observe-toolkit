package org.debux.webmotion.server.handler.injector;

/*-
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 07/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 9.0.0
 */
public abstract class JsonInjector<O> implements ExecutorParametersInjectorHandler.Injector {

    private static final Logger log = LogManager.getLogger(JsonInjector.class);
    protected final Gson gson;
    private final Class<O> type;

    public JsonInjector(Class<O> type, Gson gson) {
        this.gson = gson;
        this.type = type;
    }

    protected abstract O buildValue(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree);

    @Override
    public final O getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {
        if (!this.type.isAssignableFrom(type)) {
            return null;
        }
        Map<String, Call.ParameterTree> parameterTreeObject = call.getParameterTree().getObject();
        if (parameterTreeObject == null) {
            log.warn(String.format("No value found for %s (type: %s)", name, type.getName()));
            return null;
        }
        Call.ParameterTree parameterTree;
        parameterTree = parameterTreeObject.get(name);
        if (parameterTree == null) {
            return buildNullValue(call, name, type, generic);
        }
        Objects.requireNonNull(parameterTree, String.format("Could not get parameterTree for mapping: %s, call: %s - name: %s, type: %s", mapping, call, name, type));
        Object value = parameterTree.getValue();
        if (value == null) {
            return buildValueFormParameters(call, name, type, generic, parameterTree);
        }
        return buildValue(call, name, type, generic, parameterTree);
    }

    protected O buildValueFormParameters(Call call, String name, Class<?> type, Type generic, Call.ParameterTree parameterTree) {
        return null;
    }

    protected O buildNullValue(Call call, String name, Class<?> type, Type generic) {
        throw new NullPointerException(String.format("By default do not accept null parameterTree for parameter: %s with type: %s", name, type.getName()));
    }

}
