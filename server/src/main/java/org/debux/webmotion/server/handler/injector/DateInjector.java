package org.debux.webmotion.server.handler.injector;

/*-
 * #%L
 * Toolkit :: Server
 * %%
 * Copyright (C) 2017 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonParseException;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DateInjector implements ExecutorParametersInjectorHandler.Injector {

    protected final String datePattern;

    public DateInjector(String datePattern) {
        this.datePattern = datePattern;
    }

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {
        Object result = null;
        if (!type.equals(Date.class)) {
            return null;
        }
        Map<String, Call.ParameterTree> object = call.getParameterTree().getObject();
        if (object != null) {
            Call.ParameterTree parameterTree = object.get(name);
            if (parameterTree != null) {
                String dateString = ((String[]) parameterTree.getValue())[0];
                SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
                try {
                    result = dateFormat.parse(dateString);
                } catch (ParseException e) {
                    throw new JsonParseException("no parse to date from " + dateString, e);
                }
            } else {
                result = ExecutorParametersInjectorHandler.EMPTY;
            }
        }
        return result;
    }
}
